<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
       <loc>https://dextraclass.com/</loc>
       <lastmod>2020-11-04T12:16:06+01:00</lastmod>
	   <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/open</loc>
        <lastmod>2020-11-04T12:16:07+01:00</lastmod>
        <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/password/reset</loc>
       	<lastmod>2020-11-04T12:16:11+01:00</lastmod>
	    <priority>0.8</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/register</loc>
        <lastmod>2020-11-04T12:16:12+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/home</loc>
        <lastmod>2020-11-04T12:16:12+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/how-to-access</loc>
        <lastmod>2020-11-04T12:16:15+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/contact</loc>
        <lastmod>2020-11-04T12:16:13+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/articles</loc>
        <lastmod>2020-11-04T12:16:13+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/help</loc>
        <lastmod>2020-11-04T12:16:14+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/privacy</loc>
        <lastmod>2020-11-04T12:16:15+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
    <sitemap>
        <loc>https://dextraclass.com/login</loc>
        <lastmod>2020-11-04T12:16:15+01:00</lastmod>
	    <priority>1.0</priority>
    </sitemap>
</sitemapindex>
