@extends('frontend.layouts.app')

@section('content')
<section class="register-banner"></section>
<section class="register-wrapper">
    <div class="container">
        <div class="register-card-wrapper">
            <div class="register-card">
                <div class="register-card-header">
                    <h1 class="heading">Login</h1>
                    @if (Session::get('newUser'))
                        <p>Welcome new user! Kindly login</p>
                    @endif
                </div>
                @includeif('auth.forms.login-form')
            </div>
        </div>
    </div>
</section>

@endsection
