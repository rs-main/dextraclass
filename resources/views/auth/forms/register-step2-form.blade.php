@section('scripts')
<style>
    .page {
        display: table;
        width: 100%;
    }

    .t-cell{
        display: table-cell;
        padding: 5px;
    }

    .t-cell {
        width: 50px;
    }


    @media (max-width: 765px) {
        .t-cell{
            padding: 5px;
        }
    }

    .otp{
        text-align: center;
    }

</style>
@endsection
<div class="successfully-activated" style="display:none">
    <div class="register-card-header text-center successful">
        <h1 class="heading">Activated.!</h1>
    </div>
    <div class="register-card-body text-center successful">
        <i class="far fa-thumbs-up"></i>

    </div>
    <div class="register-card-footer">
        <p class="login-text mt-4">Your account was successfully activated.</p>
    </div>
</div>

<div class="hide-activate-account">
    <form method="post" id="registerForm">
        {{ csrf_field() }}
        <div class="register-card-body">
            <div class="ajax-error" style="display:none"></div>
            <div class="form-group mb-3">
                <input type="hidden" id="phone_code" name="phone_code" value="{{ $sessionUser->userData->country }}"/>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> +{{ $sessionUser->userData->country }} </span>
                    </div>
                    <input type="hidden" name="old_mobile_number" id="old_mobile_number" value="{{$sessionUser->userData->mobile}}">
                    <input type="text" id="mobile" name="mobile" class="mobile-number-input form-control {{ $errors->has('mobile') ? 'is-invalid' : '' }}" value="{{ old('mobile', $sessionUser->userData->mobile) }}" placeholder="{{ __('Mobile') }}" data-validation="required length number" data-validation-length="10-15" readonly="readonly">
                </div>

                @if($errors->has('mobile'))
                <div class="invalid-feedback" style="display:block">{{ $errors->first('mobile') }}</div>
                @else
                <small class="form-text">{{ "Your Mobile number on record" }} <a href='javascript:void(0)' class="change-mobile-number">change</a></small>
                @endif
            </div>

            <div class="page ">

                <div class="t-cell">
                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_1" id="code_1" maxlength="1" placeholder="{{ __('x') }}" required/>


                </div>
                <div class="t-cell">
                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_2" id="code_2" maxlength="1" placeholder="{{ __('x') }}" required/>

                </div>
                <div class="t-cell">
                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_3" id="code_3" maxlength="1" placeholder="{{ __('x') }}" required/>

                </div>
                <div class="t-cell">
                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_4" id="code_4" maxlength="1" placeholder="{{ __('x') }}" required/>

                </div>
                <div class="t-cell">
                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_5" id="code_5" maxlength="1" placeholder="{{ __('x') }}" required/>

                </div>
                <div class="t-cell">
                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_6" id="code_6" maxlength="1" placeholder="{{ __('x') }}" required/>

                </div>

            </div>


{{--            <div class="row ">--}}

{{--                <div class="col-sm-2">--}}
{{--                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_1" id="code_1"  maxlength="1" placeholder="{{ __('x') }}"  required />--}}

{{--                </div>--}}
{{--                <div class="col-sm-2">--}}
{{--                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_2" id="code_2" maxlength="1" placeholder="{{ __('x') }}" required />--}}

{{--                </div>--}}
{{--                <div class="col-sm-2">--}}
{{--                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_3" id="code_3" maxlength="1" placeholder="{{ __('x') }}" required />--}}

{{--                </div>--}}
{{--                <div class="col-sm-2">--}}
{{--                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_4" id="code_4" maxlength="1" placeholder="{{ __('x') }}" required />--}}

{{--                </div>--}}
{{--                <div class="col-sm-2">--}}
{{--                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_5" id="code_5" maxlength="1" placeholder="{{ __('x') }}" required />--}}

{{--                </div>--}}
{{--                <div class="col-sm-2">--}}
{{--                    <input type="text" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }} otp" name="code_6" id="code_6" maxlength="1" placeholder="{{ __('x') }}" required />--}}

{{--                </div>--}}

{{--            </div>--}}

            <div class="mb-3">
                @if ($errors->has('otp'))
                <div class="invalid-feedback" style="display:block">{!! $errors->first('otp') !!}</div>
                @else
                <small class="form-text">{{ "Enter verification code" }}</small>
                @endif
            </div>

            {{--            <div class="form-group mb-3">--}}
{{--                <input type="password" name="otp" id="otp" class="form-control {{ $errors->has('otp') ? 'is-invalid' : '' }}" placeholder="{{ __('x x x x x x') }}" data-validation="required length" data-validation-length="6">--}}

{{--                @if ($errors->has('otp'))--}}
{{--                <div class="invalid-feedback" style="display:block">{!! $errors->first('otp') !!}</div>--}}
{{--                @else--}}
{{--                <small class="form-text">{{ "Enter verification code" }}</small>--}}
{{--                @endif--}}
{{--            </div>--}}

			 <div class="form-group mb-3">

				<h4 class=" text-center text-info countdown">@if($diffMinutes  != '00:00') Time Left - {{Config::get('constants.OTP_TIME_LEFT')}} @endif </h4>

            </div>
        </div>
        <input type="hidden" id="user_id" value="{{$sessionUser->id}}">

        <div class="register-card-footer">
            <button type="button" class="btn btn-primary w-100 otp-ajax-submit">{{ __('ACTIVATE') }}</button>
            <button type="button" class="btn btn-primary w-100 reSendOtp" style="display:none;">{{ __('REQUEST CODE') }}</button>
            <input type="hidden" name="mobile_number_type" id="mobile_number_type" value="old">
            <p class="login-text mt-4">or <a href="javascript:void(0)" class="reSendOtp">Request code again</a><br> if you don't receive it after 5 mins</p>
        </div>
    <div class="register-card-skip">
            <a href="{{ route('registerStep', 3) }}">or Skip to activate later</a>
        </div>
    </form>
</div>

<script>
    $(document).ready(function (){
        $('.form-control').keyup(function(){
            if($(this).val().length==$(this).attr("maxlength")){
                $(this).closest('div').next().find('.form-control').focus();
                //$(this).next(".form-control").focus();
            }
        });

        $('.form-control').keyup(function(e){
            var key = event.keyCode || event.charCode;
            if( key == 8 || key == 46 ){
                $(this).closest('div').prev().find('.form-control').focus();
            }
        });
    })
</script>
