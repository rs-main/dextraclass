<script>

    let loginMainClassObj = {
        parentUrl:'{{URL::current()}}',
        jsId : {
            activeTabInput: "#activeTabInput",
            LoginBtn      : "#loginBtn",
            loginPopUpDiv : "#loginPopUpDiv",
            returnUrl     : "#returnUrl",
            reloadIt      : "#reloadIt",
            userName      : "#username",
            userPassword  : "#password"
        },
        status : {
            success     : 200
        },
        jsClass : {
            navItem  : ".nav-item"
        },
        extra : {
            url : {
                loginUrl  : '{{route("frontend.ajaxLogin")}}'
            }
        }
    }
</script>

<form>
	{{ csrf_field() }}

    <input type="hidden" id="returnUrl" value="">
    <input type="hidden" id="reloadIt" value="">

	<div class="register-card-body">
		@includeif('backend.message')
		<div class="form-group mb-3">
			<input name="username" class="form-control" id="username" {{ $errors->has('username') ? 'is-invalid' : '' }}" value="{{ old('username') }}" placeholder="{{ __('Username') }}">
			@if ($errors->has('username'))
				<div class="invalid-feedback">
					{{ $errors->first('username') }}
				</div>
			@endif
		</div>

		<div class="form-group mb-3">
			<input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('Password') }}">
			@if ($errors->has('password'))
				<div class="invalid-feedback">
					{{ $errors->first('password') }}
				</div>
			@endif
		</div>
		<div class="row">
			<!--<div class="col-12">
					<div class="icheck-primary">
							<input type="checkbox" name="remember_me" id="remember_me">
							<label for="remember_me">{ __('Remember Me') }}</label>
					</div>
			</div>-->
		</div>
	</div>

	<div class="register-card-footer">
		<button type="submit" id="loginBtn" class="btn btn-primary btn-block btn-flat" onclick="loginMainObj.loginUser()">
			{{ __('Login') }}
		</button>
		<p class="mt-2 mb-2 text-center">or register if you dont have an account</p>

		<p class="mb-0">
			<a href="{{ route('register') }}" class="btn btn-secondary btn-block btn-flat">{{__('Register')}}</a>
		</p>
		<p class="text-left mt-3">
			<a href="{{ route('password.update') }}" >
				{{ __('Forgot Your Password?') }}
			</a>
		</p>
	</div>
</form>

<script src="{{ asset('js/login_main.js') }}"></script>
