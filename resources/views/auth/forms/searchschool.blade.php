<ul id="school-list">
	@foreach($schools as $school)
	<li data-school-id="{{$school->id}}" data-school-name="{{$school->school_name}}" class="select-school">{{$school->school_name}}</li>
	@endforeach
</ul>

<!-- <option class="d-none" value="">School name</option>
@foreach($schools as $school)
<option value="{{$school->id}}">{{$school->school_name}}</option>
@endforeach -->
