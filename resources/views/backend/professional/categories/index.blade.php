@extends('backend.layouts.layout-2')

@section('scripts')
    @php
    @endphp


    <script>
        let category_form = $("#category_upload_form");
        let update_form = $("#category_update_form");

        $(".edit-category").on("click",function (e){
            e.preventDefault();
            $(".category_name").val($(this).data("name"));
            $(".description").val($(this).data("description"));
            $(".industry_id").val($(this).data("industry"));
            let category_id = ($(this).data("id"));
            update_form.attr("action", `/admin/professional/professional-categories/update/${category_id}`);
            $(".category-title").text("Edit " + $(this).data("name"))
        })

        $("#create-category").on("click",function (){
            category_form.attr("action", "{{route("backend.professional.categories.store")}}");
            $(".category-title").text("Add New Category")
            category_form.trigger("reset");
        })

        $(".add-new-course").on("click",function (){
            $(".category-title").text("Add New Course")
            $("#category_id").val($(this).data("id"));
            $("#category_name").val($(this).data("name"));
        })

    </script>
@endsection

@section('content')
    @includeif('backend.message')
    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Categories</div>

        <a href="#" class="btn btn-primary rounded-pill d-block" id="create-category" data-toggle="modal" data-target="#add-category-modal">
            <span class="ion ion-md-add"></span>&nbsp;Create Category</a>
    </h4>

    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered table-striped sortable">
                <thead>
                <tr>
                    <th>No.</th>
                    <th style="width: 20%; vertical-align: middle;" rowspan="2">
                        <i class="ion ion-ios-navigate"></i>&nbsp;
                        Category Name</th>
                    <th data-mainsort="3" style="text-align: center;">No. of Courses</th>
                    <th data-mainsort="3" style="text-align: center;">No. of Videos</th>
                    <th data-mainsort="3" style="text-align: center;">Action</th>
                </tr>
                </thead>
                <tbody>

                @php $i = $categories->firstItem();; @endphp
                @foreach($categories as $category)
                <tr>
                    <td style="text-align: center">{{$i++}}</td>
                    <td style="text-align: center">{{$category->name}}</td>
                    <td style="text-align: center">
                        {{\App\ProfessionalSchoolCategory::CategoryCoursesCount($category->id)}}
                    </td>
                    <td style="text-align: center">{{\App\ProfessionalCategory::getCategoryVideosCount($category->id)}}</td>
                    <td class='form-inline align-middle'>
                        <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;"
                           href ="#"
                           class="btn btn-primary btn-xs md-btn-flat article-tooltip edit-category"
                           data-id="{{$category->id}}"
                           data-industry="{{$category->industry_id}}"
                           data-name="{{$category->name}}"
                           data-description="{{$category->description}}"
                           title="Edit" data-toggle="modal"
                           data-target="#edit-category-modal"><i class="ion ion-md-create"></i></a>
                        {{--                            @role('admin')--}}
                        <form method="POST" action="{{route('backend.professional.categories.destroy', $category->id)}}" style="display: inline-block;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

                        </form>
                        <small>
                            <a data-id="{{$category->id}}" data-name="{{$category->name}}" class="add-new-course" href="#" data-toggle="modal" data-target="#add-new-course-modal"> Add New Course</a>
                        </small>

{{--                        <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.questions.show', $category->id)}}"--}}
{{--                           class="btn btn-warning btn-xs md-btn-flat article-tooltip" title="View question details"><i class="ion ion-md-eye"></i></a>--}}

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div style="text-align: center">
            <ul class="pagination-custom list-unstyled list-inline">
                {!! $categories->render() !!}
            </ul>
        </div>
    </div>

    <div class="modal fade" id="add-category-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title category-title" id="lineModalLabel">Add New Category </h3>
                </div>
                <form action="{{route("backend.professional.categories.store")}}" method="post" id="category_upload_form">

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="industry">Industry</label>
                            <select class="form-control" name="industry_id" id="industry_id">

                                @foreach($industries as $industry)
                                    <option value="{{$industry->id}}">{{$industry->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="industry">Category</label>
                            <input type="text" name="name" id="category_name" class="form-control" required placeholder="Category name"/>
                        </div>

                        <div class="form-group">
                            <label for="industry">Description</label>
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="submit" id="add-industry" class="btn btn-success btn-hover-green" data-action="save" role="button">Create Category</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-category-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title category-title" id="lineModalLabel">Edit </h3>
                </div>
                <form  method="post" id="category_update_form">

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="industry">Industry</label>
                            <select class="form-control industry_id" required name="industry_id" id="industry_id">

                                @foreach($industries as $industry)
                                    <option value="{{$industry->id}}">{{$industry->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="industry">Category</label>
                            <input type="text" name="name" id="category_name" class="form-control category_name" required placeholder="Category name"/>
                        </div>

                        <div class="form-group">
                            <label for="industry">Description</label>
                            <textarea name="description" id="description" required class="form-control description"></textarea>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="submit" id="update-category" class="btn btn-success btn-hover-green" data-action="save" role="button">Update Category</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-new-course-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title category-title" id="lineModalLabel">Add New Course </h3>
                </div>
                <form action="{{route('backend.professional.courses.store')}}" method="post" id="add_course_form">

                    <div class="modal-body">

                        <input type="hidden" name="category_modal" />

                        <input type="hidden" name="category_id" id="category_id">

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2 text-sm-right">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" placeholder="Course Name" value="{{ old('name') }}" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2 text-sm-right">Description</label>
                            <div class="col-sm-10">
                                <textarea required name="description" class="form-control" placeholder="Course Description">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2 text-sm-right">Type</label>
                            <div class="col-sm-10">
                                <input type="text" name="type" class="form-control" placeholder="Course type" value="professional" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2 text-sm-right">Image</label>
                            <div class="col-sm-10">
                                <input type="file" name="display_image" class="form-control" value="professional">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2 text-sm-right"></label>
                            <div class="col-sm-10">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" name="status" value="1" @if(old('status')) checked @endif class="custom-control-input">
                                    <span class="custom-control-label">Active</span>
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="submit" id="update-category" class="btn btn-success btn-hover-green" data-action="save" role="button">Add Course</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
