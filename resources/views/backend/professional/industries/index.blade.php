@extends('backend.layouts.layout-2')

@section('scripts')

    <script>
        $(".edit-industry").on("click",function(){
            $(".modal-title").text("Edit Industry");
            let id = $(this).data("id");
            let name = $(this).data("name");
            $("#name").val(name);
            const update_url = "{{route("backend.professional.industries.update",1)}}";
            $("#industry-modal-form").attr("action", `/admin/professional/industries/update/${id}`)
            $("#add-industry").text("Update")
        })

        $("#add-new-industry").on("click",function (){
            $("#industry-modal-form").attr("action", "{{route("backend.professional.industries.store")}}")
            $(".modal-title").text("Add New Industry");
            $("#industry-modal-form").trigger("reset");
            $("#add-industry").text("Add New")
        })

        $(".add-category").on("click",function(){
            let id = $(this).data("id");
            $("#industry_id").val(id);
            $("#industry_name").val($(this).data("name"));
        })
    </script>

@endsection

@section('content')
    @includeif('backend.message')
    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Industries</div>

        <a href="#" class="btn btn-primary rounded-pill d-block" id="add-new-industry" data-toggle="modal" data-target="#add-industry-modal">
            <span class="ion ion-md-add"></span>&nbsp;Create Industry</a>
    </h4>

    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered table-striped sortable">
                <thead>
                <tr>
                    <th>No.</th>
                    <th style="width: 20%; vertical-align: middle;" rowspan="2">
                        <i class="ion ion-ios-navigate"></i>&nbsp; Industry</th>
{{--                    <th colspan="4" data-mainsort="3" style="text-align: center;">Results</th>--}}
                    <th data-mainsort="3" style="text-align: center;">No. of Categories</th>
                    <th data-mainsort="3" style="text-align: center;">No. of Courses</th>
                    <th data-mainsort="3" style="text-align: center;">No. of Videos</th>
                    <th data-mainsort="3" style="text-align: center;">Action</th>
{{--                    <th data-defaultsort="disabled"></th>--}}
                </tr>
                </thead>
                <tbody>

                @php $i = $industries->firstItem() @endphp
                @foreach($industries as $industry)
                <tr>
                    <td>{{$i++}}</td>
                    <td style="text-align: center">{{$industry->name}}</td>
                    <td style="text-align: center">
                        <a href="{{route("backend.professional.categories.index",["industry_id" => $industry->id ])}}">{{$industry->category_counts}}</a>
                    </td>
                    <td style="text-align: center">{{\App\Industry::getIndustryCourses($industry->id)}}</td>
                    <td style="text-align: center">{{\App\Industry::getIndustryVideosCount($industry->id)}}</td>

                    <td class='form-inline align-middle'>
                        <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;"
                           class="btn btn-primary btn-xs md-btn-flat article-tooltip edit-industry"
                           title="Edit" href="#" id="edit-new-industry"
                           data-id="{{$industry->id}}"
                           data-name="{{$industry->name}}"
                           data-toggle="modal" data-target="#add-industry-modal">
                            <i class="ion ion-md-create"></i></a>
                        {{--                            @role('admin')--}}
                        <form method="POST" action="{{route('backend.professional.industries.destroy', $industry->id)}}" style="display: inline-block;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

                        </form>
                        <small><a href="#" data-name="{{$industry->name}}" data-id="{{$industry->id}}"  data-toggle="modal" data-target="#add-category-modal" class="add-category">Add New Category</a></small>

{{--                        <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.questions.show', $industry->id)}}"--}}
{{--                           class="btn btn-warning btn-xs md-btn-flat article-tooltip" title="View question details"><i class="ion ion-md-eye"></i></a>--}}

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div style="text-align: center">
            <ul class="pagination-custom list-unstyled list-inline">
                {!! $industries->render() !!}
            </ul>
        </div>
    </div>

    <div class="modal fade" id="add-industry-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">Add New Industry </h3>
                </div>
                <form id="industry-modal-form" action="{{route("backend.professional.industries.store")}}" method="post" >

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="industry">Industry</label>
                            <input type="text" name="name" id="name" class="form-control" required placeholder="Industry name"/>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="submit" id="add-industry" class="btn btn-success btn-hover-green" data-action="save" role="button">Create Industry</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-category-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title category-title" id="lineModalLabel">Add New Category </h3>
                </div>

                <form action="{{route("backend.professional.categories.store")}}" method="post" id="category_upload_form">

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="industry">Industry</label>
                            <input type="text" id="industry_name"  class="form-control" readonly>
                        </div>

                        <input type="hidden" name="industry_id" id="industry_id">

                        <div class="form-group">
                            <label for="industry">Category</label>
                            <input type="text" name="name" id="category_name" class="form-control" required placeholder="Category name"/>
                        </div>

                        <input type="hidden" value="industry_modal" name="industry_modal">

                        <div class="form-group">
                            <label for="industry">Description</label>
                            <textarea name="description" id="description" class="form-control" required></textarea>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="submit" id="add-industry" class="btn btn-success btn-hover-green" data-action="save" role="button">Create Category</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
