@extends('backend.layouts.layout-2')

@section('scripts')

<script type="text/javascript">
    $(function () {

        $("#school").on("change", function () {
            //var school_id = $(this).val();
            var school_id = $('#school option:selected').attr('data-id');

            $.ajax({
                type: "POST",
                url: '{{ route("ajax.school.stdfiltercourses") }}',
                data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    $("#school_course").html(data);
                }
            });

        });

        $("#school_course").on("change", function () {
            // var school_course = $('#school_course').val();
            var school_course = $('#school_course option:selected').attr('data-id');
            if (school_course) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.stdfiltercourseclasses") }}',
                    data: {'course_id': school_course, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#class").html(data);
                    }
                });
            }
        });

        $("#class").on("change", function () {
            var class_id = $('#class option:selected').attr('data-id');

            if (class_id) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.filterclasssubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#subject").html(data);
                    }
                });
            }
        });

        var table = $('#video-list').DataTable({
            "columns": [
                null,
                null,
                {"orderable": false},
                {"orderable": false},
                {"orderable": false},
                {"orderable": false},
                {"orderable": false},
                null,
                {"orderable": false}
            ],
            dom: 'lrtip',
        });
        $('#video_title').on('keyup', function () {
            //alert('gdfgfd');
            regExSearch = this.value;
            table.column(6).search(regExSearch, true, false).draw();
            // table.search(this.value, true, false).draw();
        });
        $('#school').on('change', function () {
            //alert('gdfgfd');
            regExSearch = this.value + '\\s*$';
            table.column(2).search(regExSearch, true, false).draw();
            //table.search(this.value, true, false).draw();
        });

        $('#school_course').on('change', function () {
            //alert('gdfgfd');
            regExSearch = this.value + '\\s*$';
            table.column(3).search(regExSearch, true, false).draw();
            //table.search(this.value, true, false).draw();
        });

        $('#class').on('change', function () {
            regExSearch = this.value + '\\s*$';
            table.column(4).search(regExSearch, true, false).draw();
            table.search(this.value, true, false).draw();
        });

        $('#subject').on('change', function () {
            regExSearch = this.value + '\\s*$';
            table.column(5).search(regExSearch, true, false).draw();
            table.search(this.value, true, false).draw();
        });

        $(".edit-video").on("click",function (){

        })

    });
</script>
@endsection

@section('content')
@includeif('backend.message')
<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>Videos</div>
    <a href="{{route('backend.professional.videos.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Video</a>
</h4>


    <!-- Content -->
    <div class="container-fluid flex-grow-1 container-p-y">

        <h4 class="d-flex flex-wrap justify-content-between align-items-center w-100 font-weight-bold pt-2 mb-4">
{{--            <div class="col-12 col-md px-0 pb-2">Courses</div>--}}
            <div class="col-12 col-md-3 px-0 pb-2">
                <form>
                    <input type="text" class="form-control" name="q"
                           placeholder="Search..." value="{{Request::query("q")}}">
                </form>
            </div>
        </h4>

        <ul class="nav container-m-nx bg-lighter container-p-x py-1 mb-4">

            @foreach($professional_courses as $course)
                <li class="nav-item">
                    <a class="nav-link {{Request::query("course") == $course->id  ? ' text-body font-weight-bold pl-0' : 'text-muted' }}" href="{{("?course=".$course->id)}}">{{$course->name}}</a>
                </li>
            @endforeach
        </ul>

        <div class="row">

            @if(sizeof($videos)>0)
            @foreach($videos as $video)

            <div class="col-sm-6 col-xl-4">
                <div class="card mb-4">
                    <div class="w-100">
                        <a href="javascript:void(0)" class="card-img-top d-block ui-rect-60 ui-bg-cover"
                           style="background-image: url( {{ !empty($video->display_image) ? ("/uploads/videos/".$video->display_image) : "'/img/bg/no-image-available.jpg'" }})">
                            <div class="d-flex justify-content-between align-items-end ui-rect-content p-3">
                                <div class="flex-shrink-1">
                                    @foreach(explode(",",$video->keywords) as $keyword)
                                    <span class="badge badge-primary">{{$keyword}}</span>
                                    @endforeach
                                </div>
{{--                                <div class="text-big"><div class="badge badge-dark font-weight-bold">--}}
{{--                                        GHS15.99--}}
{{--                                    </div></div>--}}
                            </div>
                        </a>
                    </div>
                    <div class="card-body">
                        <h5 class="mb-3"><a href="javascript:void(0)" class="text-body">{{$video->topic_name}}</a></h5>
                        <p class="text-muted mb-3">
                           {{$video->description}}
                        </p>
                        <div class="media">
                            <div class="media-body">

{{--                                <div class="ui-stars">--}}
{{--                                    <div class="ui-star filled">--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                    </div>--}}
{{--                                    <div class="ui-star filled">--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                    </div>--}}
{{--                                    <div class="ui-star filled">--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                    </div>--}}
{{--                                    <div class="ui-star filled">--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                    </div>--}}
{{--                                    <div class="ui-star half-filled">--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                        <i class="ion ion-md-star"></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                --}}
{{--                                <a href="javascript:void(0)" class="text-muted small">55</a>--}}
                                <i class="fa fa-eye">  {{$video->total_views == Null ? "0" : $video->total_views}}</i>
                            </div>
                            <div class="text-muted small">
{{--                                <i class="ion ion-md-time text-primary"></i>--}}
                                <span><a href="{{route("backend.professional.videos.edit",$video->uuid)}}">
                                        <i class="fa fa-edit fa-2x btn edit-video"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach
            @else

            <div class="alert alert-info" style="width: 100%">
                <p class="text-center">No Records Found!</p>
            </div>

            @endif

        </div>

        <nav>
            <ul class="pagination justify-content-center">
                {!! $videos->render() !!}
            </ul>
        </nav>

    </div>
    <!-- / Content -->

<div class="modal fade" id="edit-video-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title category-title" id="lineModalLabel">Edit Video </h3>
            </div>

            <form action="{{route("backend.professional.categories.store")}}" method="post" id="category_upload_form">

                <div class="modal-body">

                    <div class="form-group">
                        <label for="industry">Industry</label>
                        <input type="text" id="industry_name"  class="form-control" readonly>
                    </div>

                    <input type="hidden" name="industry_id" id="industry_id">

                    <div class="form-group">
                        <label for="industry">Category</label>
                        <input type="text" name="name" id="category_name" class="form-control" required placeholder="Category name"/>
                    </div>

                    <input type="hidden" value="industry_modal" name="industry_modal">

                    <div class="form-group">
                        <label for="industry">Description</label>
                        <textarea name="description" id="description" class="form-control" required></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                        </div>

                        <div class="btn-group" role="group">
                            <button type="submit" id="add-industry" class="btn btn-success btn-hover-green" data-action="save" role="button">Create Category</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
