@extends('backend.layouts.layout-2')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/dropzone/dropzone.css')}}">
<style>
    .dz-message {
        margin: 3rem 0;
    }
</style>
    <script>
        setTimeout(function (){
          $("#school_course").val("{{$video->course_id}}")
          $("#topic_id").val("{{$video->topic_id}}")
          $("#description").text("{{$video->description}}")
          $("#keywords").val("{{$video->keywords}}")
       },1000);

    </script>
@endsection

@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Videos /</span> Edit Video(Take A Course)
</h4>

@includeif('backend.message')
<form action="{{route('backend.professional.videos.update',$video->id)}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">Video General Details</div>
                <hr class="border-light m-0">
                <div class="card-body">

                    <input type="hidden" name="school" value="{{$school?$school->id:null}}" >

                    <div class="form-group course_wrapper">
                        <label>Course</label>
                        <select name="course" id="school_course" class="custom-select" required>
                            <option value="" disabled selected="">Select Course</option>
                            @foreach($professional_courses as $category)
                                    @if(sizeof($category["course_category"]) > 0)
                                    <optgroup label="{{strtoupper($category["name"])}}">
                                        @foreach($category["course_category"] as $course)
                                        <option value="{{ isset($course["course"])? $course["course"]["id"]: ""}}" >{{isset($course["course"])? $course["course"]["name"] : ""}}</option>
                                        @endforeach
                                    </optgroup>
                                    @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Choose Topic</label>
                        <select name="topic_id" id="topic_id" class="custom-select" required>
                            @foreach($topics as $topic)
                                <option value="{{$topic->id}}">{{$topic->topic_name}}</option>
                            @endforeach
                        </select>

                        <span class="float-right btn" data-toggle="modal" data-target="#add-topic-modal">
                            <small>Add new topic</small></span>
                    </div>


                    <div class="form-group">
                        <label>Video Description</label>
                        <textarea class="form-control" id="description" name="description" rows="3" required>{{old('description')}}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Display Image</label>
                        <input type="file" value="{{old('display_image')}}" name="display_image" id="display_image" class="form-control" />
                    </div>

                    <div class="form-group">
                        <label>Keywords</label>
                        <input type="text" value="{{$video->keywords}}" name="keywords" id="keywords"
                               data-role="tagsinput" class="form-control" />
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3">Status</label>
                        <div class="col-sm-6">
                            <label class="switcher switcher-lg switcher-success">
                                <input type="checkbox" name="status" value="1" class="switcher-input status">
                                <span class="switcher-indicator">
                                    <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                                    <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                                </span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Upload Note (If any)</label>
                        <input type="hidden" name="note_file" id="uplodedNoteFile" value="{{old('note_file')}}"/>
                        <div id="noteFileUpload" class="dropzone">
                            <div class="dz-message needsclick" >
                                Drop files here or click to upload
                                <span class="note needsclick"><small class="form-text text-muted">Allowed file types: jpeg, png, pdf, doc, docx, ppt, pptx, zip and up to 50 MB</small></span>
                            </div>
                            <div class="fallback">
                                <input type="file" name="notefile">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="card mb-4">
                <div class="card-header">Video Details</div>
                <hr class="border-light m-0">
                <div class="card-body">
                    <div class="form-inline mb-4">
                        <label class="custom-control custom-radio justify-content-start mr-2">
                            <input name="video_type" type="radio" class="custom-control-input video_type" value="url" required="">
                            <span class="custom-control-label">Video by URL</span>
                        </label>
                        <label class="custom-control custom-radio justify-content-start mr-2">
                            <input name="video_type" type="radio" class="custom-control-input video_type" value="file" >
                            <span class="custom-control-label">Video by File</span>
                        </label>
                    </div>

                    <div class="form-group video_url_section" style="display: none;">
                        <label>Video URL</label>
                        <input type="url" value="{{$video->video_url}}" placeholder="Video URL" class="form-control" name="video_url" />
                        <small class="form-text text-muted">Example - https://vimeo.com/{video_id} </small>
                    </div>


                    <div class="form-group">
                        <label>Upload Video for mobile</label>
                        <input type="hidden" name="video_file" id="uploadedVideoFile" value=""/>
                        <div id="videoFileUpload" class="dropzone">
                            <div class="dz-message needsclick" >
                                Drop files here or click to upload
                                <span class="note needsclick"><small class="form-text text-muted">Allowed file types: .mp4 .webm .wmv .avi .flv .mov and up to 300 MB</small></span>
                            </div>
                            <div class="fallback">
                                <input type="file" name="videofile">
                            </div>
                        </div>
                    </div>

                </div>
                <hr class="border-light m-0">
                <div class="card-footer d-block text-center">
                    <button type="submit" class="btn btn-primary" id="smt">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" id="add-topic-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">Add New Topic </h3>
            </div>
            <form action="{{route("backend.professional.videos.topic.store")}}" method="post" >

                <input type="hidden" name="video_id" value="{{$video->uuid}}"/>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="industry">Topic Name</label>
                        <input type="text" name="topic_name" class="form-control" required placeholder="Topic name" />
                    </div>

                    <div class="form-group">
                        <label for="industry">Weight(Order)</label>
                        <input type="number" name="weight" class="form-control" required placeholder="Topic weight"/>
                    </div>

                    <input type="hidden" value="ajax_request">
                    <input type="hidden" value="videos">

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                        </div>

                        <div class="btn-group" role="group">
                            <button type="submit" id="add-topic" class="btn btn-success btn-hover-green" data-action="save" role="button">Add Topic</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@php
$url = '';$size = 0;
if(old('note_file')) {
    $url = Storage::disk('s3')->url(old('note_file'));
    $size = Storage::disk('s3')->size(old('note_file'));
}
@endphp
@endsection

@section('scripts')

<script src="{{asset('assets/vendor/libs/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script src="{{asset('assets/vendor/libs/dropzone/dropzone.js')}}"></script>
@role('admin|subadmin')
<script>
    var category_id = "{{$institute->id}}";
    var school_id = "{{old('school')}}";
{{--    var school_id = "{{$institute->id}}";--}}
</script>
@endrole
@role('school')
<script>
    var category_id = "{{$category_id}}";
    var school_id = "{{$school_id}}";
</script>
@endrole
<script>
    $(document).ready(function () {

        $('#keywords').tagsinput({ tagClass: 'badge badge-secondary' });

        $('.period_field_group').hide();

        if(category_id){
            $("#institute_type").val(category_id).trigger('change');
        }

        $(".video_type[value=url]").prop( "checked", true );
        $(".video_url_section").show();
        $('.status').prop( "checked", true );
        $('.status').prop( "disabled", true );


        $(".video_type").click(function() {

            var type = $(this).val();

            if(type == 'url') {
                $(".video_url_section").show();
                $('.status').prop( "checked", true );
                $('.status').prop( "disabled", false );
            } else {
                $('input[name=video_url]').val('');
                $(".video_url_section").hide();
                $('.status').prop( "checked", false );
                $('.status').prop( "disabled", true );
            }

        });



    /********* Upload Note file using dropzone *******************/
    // Dropzone class:
        $("div#noteFileUpload").dropzone({
            url: "{{route('ajax.dropzone.upload.note')}}",
            acceptedFiles: ".jpg,.jpeg.png,.pdf,.doc,.docm,.docx,.docx,.dot,.xls,.xlsb,.ppt",
            maxFilesize: 500000, /* you can upload only 50mb  */
            maxFiles: 1,
            paramName: "notefile",
            addRemoveLinks: true,
            accept: function (file, done) {
                $("#smt").attr("disabled", "disabled");
                if (this.files.length > 1) {
                    done("Sorry you can not upload any media.");
                }
                else {
                    done();
                }
            },
            init: function () {
                var mydropzone = this;
                if("{{$url}}" != ''){
                    var mockFile = { name: "{{basename(old('note_file'))}}",size: '{{$size}}' };
                    mydropzone.options.addedfile.call(mydropzone, mockFile);
                    mydropzone.options.thumbnail.call(mydropzone, mockFile, "{{$url}}");
                }
                this.on("maxfilesexceeded", function(file){
                    this.removeFile(file);
                });

                mydropzone.on("success", function (file, response) {
                    $('#uplodedNoteFile').val(response.savefilename);
                    $("#smt").removeAttr("disabled");
                });

                this.on("removedfile", function (file) {
                  if (this.files.length == 0){
                     $("#uplodedNoteFile").val('');
                  }
                });

            },
            sending: function (file, xhr, formData) {
                formData.append('_token', "{{ csrf_token() }}");
            }

    });

        // Dropzone class:
        $("div#videoFileUpload").dropzone({
            url: "{{route('ajax.dropzone.upload.video')}}",
            acceptedFiles: ".mp4,.webm,.wmv,.avi,.flv,.mov ",
            maxFilesize: 500, /* you can upload only 500mb  */
            maxFiles: 1,
            paramName: "videofile",
            timeout: 3600000,
            addRemoveLinks: true,
            accept: function (file, done) {
                $("#smt").attr("disabled", "disabled");
                if (this.files.length > 1) {
                    done("Sorry you can not upload anymore media.");
                }
                else {
                    done();
                }
            },
            init: function () {
                var mydropzone = this;
                if("{{$url}}" != ''){
                    var mockFile = { name: "{{basename(old('video_file'))}}",size: '{{$size}}' };
                    mydropzone.options.addedfile.call(mydropzone, mockFile);
                    mydropzone.options.thumbnail.call(mydropzone, mockFile, "{{$url}}");
                }
                this.on("maxfilesexceeded", function(file){
                    this.removeFile(file);
                });

                mydropzone.on("success", function (file, response) {

                    $('#uploadedVideoFile').val(response.savefilename);
                    $("#smt").removeAttr("disabled");
                    $('.dz-nopreview').hide();
                });

                this.on("removedfile", function (file) {
                    if (this.files.length == 0){
                        $("#uploadedVideoFile").val('');
                    }
                });

                mydropzone.on('canceled', function (){
                    $('.dz-error-message').html("Upload timed out");
                });

                mydropzone.on("totaluploadprogress", function(progress) {
                    if(progress == 100){
                        $('.progress').hide();
                        $('.dz-nopreview')
                            .html('<div class="text-center"><img src="{{url("images/LoaderIcon.gif")}}"> </div>')
                    }
                });
            },
            sending: function (file, xhr, formData) {
                formData.append('_token', "{{ csrf_token() }}");
            }

        });

    });
</script>

@stop
