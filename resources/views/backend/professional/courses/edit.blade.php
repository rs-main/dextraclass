@extends('backend.layouts.layout-2')

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Courses /</span> Edit Course(Take A Course)
    </h4>
	<div class="card mb-4">
        <h6 class="card-header">
            Edit Course(Take A Course)
        </h6>
        <div class="card-body">
			@includeif('backend.message')
            <form action="{{route('backend.professional.courses.update',$course->course_id)}}" method = "post">
			@csrf

            <input type="hidden" name="school_id" id="school-id" value="{{$school_id}}">

			<div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Category</label>
                    <div class="col-sm-10">
                        <select name="category_id" id="category_id" class="custom-select" required>
						<option value="">Select Category</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
					</select>
					</div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Course Name</label>
                    <div class="col-sm-10">
                        <input type="text" require name="name" placeholder="Course Name" value="{{ $course->name }}" class="form-control" required>
                    </div>
                </div>

				<div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Course Description</label>
                    <div class="col-sm-10">
                        <textarea required name="description" class="form-control"
                                  placeholder="Course Description">{{ $course->description }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Course Type</label>
                    <div class="col-sm-10">
                        <input type="text" name="type" class="form-control" value="professional" placeholder="Course type" {{ old('type') }} readonly>
                    </div>
                </div>

				<div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="status" value="1" @if(old('status')) checked @endif class="custom-control-input">
                        <span class="custom-control-label">Active</span>
                    </label>
                </div>
            </div>


                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
						<a href = "{{route('backend.professional.courses.index')}}" class="btn btn-danger mr-2">Cancel</a>
						<button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
<!--<script src="{{ mix('/assets/vendor/libs/flatpickr/flatpickr.js') }}"></script>-->

<script>
    $(document).ready(function () {
        const category_id = "{{$course->category_id}}";
        $("#category_id").val(category_id);
});
</script>
@stop
