@extends('backend.layouts.layout-3')

@section('content')

    <h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Quiz /</span> Quiz Details
</h4>

<div class="card mb-4">

    <h3 class="card-header">
        {{$quiz->title}}

        <a href="javascript:void(0)" onclick="window.history.go(-1); return false;" class="btn btn-primary rounded-pill d-block detail-back-btn">Back</a>

    </h3>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6 col-xl-7">
                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-2"><strong>School</strong></div>
                    <div class="col-sm-6 col-xl-9">
                        {{$quiz->school_details($quiz->subject_id)}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-2"><strong>Class</strong></div>
                    <div class="col-sm-6 col-xl-9">{{$quiz->class_details($quiz->subject_id)}}</div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-2"><strong>Subject</strong></div>
                    <div class="col-sm-6 col-xl-9">{{$quiz->subject_details($quiz->subject_id)}}</div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-2"><strong>Shown After</strong></div>
                    @if($quiz->lesson_id != null)
                        <div class="col-sm-6 col-xl-9">{{$quiz->topic_details($quiz->lesson_id)}}</div>
                    @else
                        Not added to a lesson
                    @endif
                </div>

                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-2"><strong>Pass Mark</strong></div>
                    <div class="col-sm-6 col-xl-9">{{$quiz->pass_mark}}%</div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-xl-3 mb-2"><strong>Active</strong></div>
                    <div class="col-sm-6 col-xl-9"><span class="badge {{($quiz->status) ? 'badge-success' : 'badge-danger' }}">{{($quiz->status) ? 'Active' : 'Inactive' }}</span></div>
                </div>

            </div>

            <div class="col-sm-6 col-xl-5">
                <h6>Questions</h6>
                <ul class="list-group">
                    @foreach($questions as $key => $question)
                        <li class="list-group-item">
                            {{$question->questions->question}}
                            
                        </li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>

@endsection
