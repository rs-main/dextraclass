@extends('backend.layouts.layout-2')

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Quiz /</span> Create Quiz
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Create Quiz
        </h6>
        <div class="card-body">
            @includeif('backend.message')
            <form method = "post" id="createQuiz">
                @csrf
                <div id="errorDiv">
                    <!-- error will be shown here -->
                </div>

                <div class="form-group row">
                    <label for="answer1" class="col-form-label col-sm-2 text-sm-right">Quiz Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="q_name" id="q_name" placeholder="Quiz name" class="form-control" required>
                        <div class="error" id="q_name_error">
                            Enter quiz name
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="answer1" class="col-form-label col-sm-2 text-sm-right">Pass Mark</label>
                    <div class="col-sm-10">
                        <input type="number" name="pass_mark" id="pass_mark" placeholder="Pass mark %" class="form-control" min="1" max="100" required>
                        <div class="error" id="pass_mark_error">
                            Enter pass mark
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Institution Type</label>
                    <div class="col-sm-10">
                        <select name="institute_type" id="institute_type" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Select Institution Type</option>
                            @foreach($institutes as $id => $type)
                                <option value="{{$id}}" >{{$type}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="institution_error">
                            Select institution
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">School</label>
                    <div class="col-sm-10">
                        <select name="school_name" id="school_name" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Select School</option>
                        </select>
                        <div class="error" id="school_error">
                            Select school
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Course</label>
                    <div class="col-sm-10">
                        <select name="course_name" id="course_name" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Select Course</option>
                        </select>
                        <div class="error" id="course_error">
                            Select course
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Class</label>
                    <div class="col-sm-10">
                        <select name="class_name" id="class_name" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Choose Class</option>
                        </select>
                        <div class="error" id="class_error">
                            Select class
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Subject</label>
                    <div class="col-sm-10">
                        <select name="subject_name" id="subject_name" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Select Subject</option>
                        </select>
                        <div class="error" id="subject_error">
                            Select subject
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="question" class="col-form-label col-sm-2 text-sm-right">Add Question</label>
                    <div class="col-sm-8">
                        <select name="question" id="question" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Select Question</option>
                        </select>
                        <div class="error" id="question_error">
                            Add questions
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary" id="addQuestion"><span class="fa fa-plus-circle"></span> &nbsp; Add</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <div id="questionDiv">

                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Status</label>
                    <div class="col-sm-10">
                        <label class="switcher switcher-lg switcher-success">
                            <input type="checkbox" name="status" value="1" class="switcher-input" checked="">
                            <span class="switcher-indicator">
                            <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                            <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                        </span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <a href = "{{route('backend.quiz.index')}}" class="btn btn-danger mr-2">Cancel</a>
                        <button type="button" class="btn btn-primary" id="submitQuestion">Submit</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
@endsection
@section('scripts')
    <style>
        .error{
            color: red;
            display: none;
        }
    </style>
    <script>
        var option = '';
        var category_id = "{{old('institute_type')}}";
        var school_id = "{{old('school_name')}}";
        var question_id = "{{old('question')}}";
        var questions = [];
        var num = 1;
    </script>
    <script>
        $(document).ready(function () {
            $("#institute_type").on("change", function () {
                var category_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#institution_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.category.schools") }}',
                    data: {'category': category_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#school_name").html(data.schools);
                        if(school_id){
                            $("#school_name").val(school_id).trigger('change');
                        }
                    }
                });
            });

            $("#school_name").on("change", function () {
                var school_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#school_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courses") }}',
                    data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#course_name").html(data);
                        if(course_id){
                            $("#course_name").val(course_id).trigger('change');
                        }
                    }
                });
            });

            $("#course_name").on("change", function () {
                var course_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#course_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courseclasses") }}',
                    data: {'course_id': course_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#class_name").html(data);
                        if(class_id){
                            $("#class_name").val(class_id).trigger('change');
                        }
                    }
                });
            });

            $("#class_name").on("change", function () {
                var class_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#class_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.classsubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#subject_name").html(data);
                        if(subject_id){
                            $("#subject_name").val(subject_id).trigger('change');
                        }
                    }
                });
            });


            if(category_id){
                $("#institute_type").val(category_id).trigger('change');
            }

            $('#subject_name').on('change',function () {
                var subject_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.questions") }}',
                    data: {'subject_id': subject_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#question").html(data.questions);
                        if(question_id){
                            $("#question").val(question_id).trigger('change');
                        }
                    }
                });
            })

            $('#question').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#question_error').fadeOut();
            })

            $('#addQuestion').on('click', function () {
                if(!$('#question').val()){
                    $('#question').addClass('is-invalid');
                    $('#question_error').fadeIn(500)
                }
                else{
                    var value = $('#question').val();
                    if(!questions.includes(value)){
                        $(document).trigger('questionAdded');
                        questions.push(value);
                        var question = $('#question').find(":selected").text();
                        $('#questionDiv').append('<div class="alert alert-dark-primary alert-dismissible fade show"><button type="button" data-id="'+num+'" class="close rm" data-dismiss="alert">×</button>'+question+'</div>');
                        num = num + 1;
                    }
                    else{
                        commonObj.messages('error', 'Question already added')
                    }

                }
            })

            $(document).on('questionAdded', function(e) {

                $('.rm').on('click', function () {
                    var id = $(this).data('id');
                    id = parseInt(id);
                    id = id - 1;
                    questions.splice(id, 1);
                    num = num - 1;
                })
            })

            $('#q_name').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#q_name_error').fadeOut();
            })

            $('#pass_mark').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#pass_mark_error').fadeOut();
            })


            $('#submitQuestion').on('click', function () {
                var institution = $('#institute_type').val();
                var school = $('#school_name').val();
                var course = $('#course_name').val();
                var class_name = $('#class_name').val();
                var subject = $('#subject_name').val();
                var pass_mark = $('#pass_mark').val();
                var quiz_name = $('#q_name').val();
                $('.error').fadeOut();

                var error = false;
                if(!institution){
                    $('#institution_error').fadeIn(500);
                    $('#institute_type').addClass('is-invalid');
                    error = true;
                }
                if(!school){
                    $('#school_error').fadeIn(500);
                    $('#school_name').addClass('is-invalid');
                    error = true;
                }
                if(!course){
                    $('#course_error').fadeIn(500);
                    $('#course_name').addClass('is-invalid');
                    error = true;
                }
                if(!class_name){
                    $('#class_error').fadeIn(500);
                    $('#class_name').addClass('is-invalid');
                    error = true;
                }
                if(!subject){
                    $('#subject_error').fadeIn(500);
                    $('#subject_name').addClass('is-invalid');
                    error = true;
                }
                if(questions === undefined || questions.length == 0){
                    $('#question_error').fadeIn(500);
                    $('#question').addClass('is-invalid');
                    error = true;
                }
                if(!pass_mark){
                    $('#pass_mark_error').fadeIn(500);
                    $('#pass_mark').addClass('is-invalid');
                    error = true;

                }
                if(!quiz_name || quiz_name.trim() == ''){
                    $('#q_name_error').fadeIn(500);
                    $('#q_name').addClass('is-invalid');
                    error = true;
                }

                if(parseInt(pass_mark) == 0 || parseInt(pass_mark) > 100){
                    $('#pass_mark_error').fadeIn(500);
                    $('#pass_mark').addClass('is-invalid');
                    error = true;
                }

                if(!error){

                    var data = {name: quiz_name, marks: pass_mark, active_status: 1, school_id: school, course_id: course, class_id: class_name, subject_id: subject, questions: questions}

                    var errorDiv = $('#errorDiv'), btn = $('#submitQuestion'), btnHtml = 'Submit';
                    $.ajax({
                        url: '{{URL('api/add-quiz')}}',
                        type: 'POST',
                        data: data,
                        beforeSend: function () {

                            errorDiv.fadeOut().html('');
                            btn.html('<span class="spinner-border" role="status" aria-hidden="true"></span> &nbsp; submitting...')

                        },
                        success: function (data) {
                            console.log(data);
                            if(data.statusCode == 200){
                                var d = data.data;

                                errorDiv.fadeIn(1000, function () {
                                    errorDiv.html('<div class="alert alert-dark-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Question added successfully</div>');
                                    $('#createQuestion').trigger('reset');
                                    window.location = d.url;
                                })
                            }
                            else{
                                errorDiv.fadeIn(1000, function () {
                                    errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Try again later</div>')
                                })
                            }

                        },
                        complete: function () {
                            btn.html(btnHtml);
                        },
                        error: function (error) {
                            errorDiv.fadeIn(1000, function () {
                                errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Kindly try again later</div>')
                            })
                        }
                    })
                }

            })
        });
    </script>
@stop
