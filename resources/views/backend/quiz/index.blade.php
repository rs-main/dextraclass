@extends('backend.layouts.layout-2')

@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('#quiz-list').dataTable(
                {
                    "columns": [
                        null,
                        null,
                        { "orderable": false },
                        { "orderable": false },
                        { "orderable": false },
                        { "orderable": false }
                    ],
                    initComplete: function () {
                        this.api().columns([3]).every( function () {
                            var column = this;
                            var select = $('<select class="custom-select"><option value="">All</option></select>')
                                .appendTo( $(column.header()) )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                } );

                            column.data().unique().sort().each( function ( d, j ) {
                                select.append( '<option value="'+d+'">'+d+'</option>' )
                            } );
                        } );
                    }
                }
            );

            $('.remove-quiz-form').on("submit",function (e){
                e.preventDefault();

                let id = $(this).data("id");
                let url = $(this).attr("action");
                let errorDiv = $('#errorDiv');

                $.post(url)
                    .then(function (response) {
                        errorDiv.fadeIn(1000, function () {
                            errorDiv.html('<div class="alert alert-dark-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Removed successfully!</div>');
                            window.location = '/admin/quiz';
                        })
                        console.log(response);
                    })
                    .catch(function (error) {
                        errorDiv.fadeIn(1000, function () {
                            errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>failed!</div>');
                        })
                        console.log(error);
                    });
            });
        });
    </script>
@endsection
@section('content')
    @includeif('backend.message')

    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Quizzes</div>
        <a href="{{route('backend.quiz.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Quiz</a>
    </h4>

    <div id="errorDiv">
        <!-- error will be shown here -->
    </div>

    <div class="card">
        <div class="card-datatable table-responsive">
            <table id="quiz-list" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th style="min-width: 8rem" class="align-top">Name</th>
                    <th style="min-width: 8rem" class="align-top">
                        School
                    </th>
                    <th style="min-width: 8rem" class="align-top">
                        Class
                    </th>
                    <th style="min-width: 8rem" class="align-top">
                        Subject
                    </th>

                    <th class="align-top">Status</th>
                    <th class="align-top">Action</th>
                </tr>
                </thead>

                <tbody>
                @if($questions&& !empty($questions))

                    @foreach($questions as $question)
                        <tr>
                            <td>{{$question->title}}</td>
                            <td>{{$question->school_details($question->subject_id)}}</td>
                            <td>{{$question->class_details($question->subject_id)}}</td>
                            <td>{{$question->subject_details($question->subject_id)}}</td>
                            <td class='text-align align-middle '><button class="btn {{$question->status ? 'btn-success':'btn-danger'}}">{{$question->status ? 'Active':'Disabled'}}</button></td>

                            <td class='form-inline align-middle d-flex-inline'>
                                <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.quiz.edit', $question->id)}}" class="btn btn-primary btn-xs md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>
{{--                                @role('admin')--}}

                                <form method="POST" action="{{route('backend.quiz.remove', $question->id)}}" style="display: inline-block;" class="remove-quiz-form" data-id="{{$question->id}}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button  type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip remove-quiz" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

                                </form>
{{--                                @endrole--}}
                                <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.quiz.show', $question->id)}}" class="btn btn-warning btn-xs md-btn-flat article-tooltip" title="View quiz details"><i class="ion ion-md-eye"></i></a>

                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">
                            <div class="alert alert-info">
                                <p class="text-center">No Records Found!</p>
                            </div>
                        </td>
                    </tr>
                @endif
                </tbody>

            </table>
        </div>
    </div>

@endsection
