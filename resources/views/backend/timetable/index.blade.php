@extends('backend.layouts.layout-2')

@section("styles")

    <link rel="stylesheet" href="{{asset("assets/css/backend.minf700.css?v=1.0.1")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/%40fortawesome/fontawesome-free/css/all.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/line-awesome/dist/line-awesome/css/line-awesome.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/remixicon/fonts/remixicon.css")}}">    <!-- Fullcalender CSS -->
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/core/main.css")}}' />
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/daygrid/main.css")}}' />
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/timegrid/main.css")}}' />
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/list/main.css")}}' />

    <style>
        .fc-day-grid-event,.fc-time-grid-event{
            background-color: #f4a965 !important;
        }

    </style>

@endsection

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Time Tables /</span>
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Time Table
        </h6>

        <div class="card-body">
            @includeif('backend.message')

            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-block card-stretch">
                        <div class="card-body">
                            <div id="calendar1" class="calendar-s"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="date-event" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="popup text-left">
                                <h4 class="mb-3">Add Schedule</h4>
                                <form action="http://iqonic.design/" id="submit-schedule">
                                    <div class="content create-workform row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label" for="schedule-title">Class</label>
                                                {{--                                                <input class="form-control" placeholder="Enter Title" type="text" name="title" id="schedule-title" required />--}}

                                                <select class="form-control" id="schedule-title" required>
                                                    @foreach(\App\Models\Classes::all() as $class)
                                                        <option value="{{$class->id}}" >{{$class->class_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="form-label" for="schedule-title">Subject</label>
{{--                                                <input class="form-control" placeholder="Enter Title" type="text" name="title" id="schedule-title" required />--}}

                                                <select class="form-control" id="schedule-title" required>
                                                    @foreach(\App\Models\Subject::all() as $subject)
                                                    <option value="{{$subject->id}}" >{{$subject->subject_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="schedule-start-date">Start Date</label>
                                                <input class="form-control basicFlatpickr date-input"
                                                       placeholder="2020-06-20"
                                                       type="datetime-local" name="title" id="schedule-start-date" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="schedule-end-date">End Date</label>
                                                <input class="form-control basicFlatpickr date-input" placeholder="2020-06-20" type="text" name="title" id="schedule-end-date" required />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input class="form-control" type="color" name="title" id="schedule-color" required />
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-4">
                                            <div class="d-flex flex-wrap align-items-ceter justify-content-center">
                                                <button class="btn btn-primary mr-4" data-dismiss="modal">Cancel</button>
                                                <button class="btn btn-outline-primary" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"
            integrity="sha512-L0nhmpoPbaeBdKIrCVBfMoCvCRhWkaM7Ton8zlfQOtLKIf7sDFZHgkesCgMDGypvAlmxUYqxqaDZDnGH/WOeuA=="
            crossorigin="anonymous"></script>

    <script src='{{asset("assets/vendor/fullcalendar/core/main.js")}}'></script>
    <script src='{{asset("assets/vendor/fullcalendar/daygrid/main.js")}}'></script>
    <script src='{{asset("assets/vendor/fullcalendar/timegrid/main.js")}}'></script>
    <script src='{{asset("assets/vendor/fullcalendar/list/main.js")}}'></script>
    <script src='{{asset("assets/vendor/fullcalendar/interaction/main.js")}}'></script>

    <script>

        let calendar1;
        if (jQuery('#calendar1').length) {
            document.addEventListener('DOMContentLoaded', function () {
                var calendarEl = document.getElementById('calendar1');

                calendar1 = new FullCalendar.Calendar(calendarEl, {
                    selectable: true,
                    plugins: ["timeGrid", "dayGrid", "list", "interaction"],
                    timeZone: "UTC",
                    defaultView: "dayGridMonth",
                    contentHeight: "auto",
                    eventLimit: true,
                    dayMaxEvents: 4,
                    header: {
                        left: "prev,next today",
                        center: "title",
                        right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
                    },
                    dateClick: function (info) {
                        $('#schedule-start-date').val(info.dateStr)
                        $('#schedule-end-date').val(info.dateStr)
                        $('#date-event').modal('show')

                        // console.log(info)
                    },
                    events: [
                        {
                            title: 'Meeting',
                            start: moment(new Date(), 'YYYY-MM-DD').add(-6, 'days').format('YYYY-MM-DD') + 'T05:30:00.000Z',
                            color: '#15ca92'
                        },


                        {
                            title: 'Click for Google',
                            url: 'http://google.com/',
                            start: moment(new Date(), 'YYYY-MM-DD').add(0, 'days').format('YYYY-MM-DD') + 'T06:30:00.000Z',
                            color: '#4731b6'
                        },
                        {
                            groupId: '999',
                            title: 'Repeating Event',
                            start: moment(new Date(), 'YYYY-MM-DD').add(0, 'days').format('YYYY-MM-DD') + 'T07:30:00.000Z',
                            color: '#5baa73'
                        },
                    ]
                });
                calendar1.render();
            });
            $(document).on("submit", "#submit-schedule", function (e) {
                e.preventDefault()
                const title = $(this).find('#schedule-title').val()
                const startDate = moment(new Date($(this).find('#schedule-start-date').val()), 'YYYY-MM-DD').format('YYYY-MM-DD') + 'T05:30:00.000Z'
                const endDate = moment(new Date($(this).find('#schedule-end-date').val()), 'YYYY-MM-DD').format('YYYY-MM-DD') + 'T05:30:00.000Z'
                const color = $(this).find('#schedule-color').val()
                console.log(startDate, endDate, color)
                const event = {
                    title: title,
                    start: startDate || '2020-12-22T02:30:00',
                    end: endDate || '2020-12-12T14:30:00',
                    color: color || '#7858d7'
                }
                $(this).closest('#date-event').modal('hide')
                calendar1.addEvent(event)


            })
        }

        // Enable all tooltips
        $('[data-toggle="tooltip"]').tooltip();
        // quill
        if (jQuery("#editor").length) {
            new Quill('#editor', {theme: 'snow'});
        }
        // With Tooltip
        if (jQuery("#quill-toolbar").length) {
            new Quill('#quill-toolbar', { modules: { toolbar: '#quill-tool' }, placeholder: 'Compose an epic...', theme: 'snow' });
            // Can control programmatically too
            $('.ql-italic').mouseover();
            setTimeout(function() {
                $('.ql-italic').mouseout();
            }, 2500);
        }

        const readURL = function(input) {
            if (input.files && input.files[0]) {
                const reader = new FileReader();

                reader.onload = function (e) {
                    $('.profile-pic').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@stop
