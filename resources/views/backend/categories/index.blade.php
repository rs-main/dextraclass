@extends('backend.layouts.layout-2')

@section('scripts')

<script type="text/javascript">
    $(function () {
        $('#category-list').dataTable(
			{
            "columns": [
              null,
			  null,
             { "orderable": false }
            ]
          }
		);
    });
</script>
@endsection

@section('content')

@includeif('backend.message')

<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>Institutions</div>
    <a href="{{route('backend.categories.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Institution</a>
</h4>

<div class="card">
    <div class="card-datatable table-responsive">
        <table id="category-list" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="min-width: 18rem">Institution Name</th>
					<th>Status</th>
                    <th class="align-top">Action</th>
                </tr>
            </thead>
            <tbody>
                @php $i=0; @endphp
                @foreach($categories as $category)
                <tr>
                    <td>{{$category->name}}</td>
					<td class='text-align align-middle'><button class="btn {{$category->status ? 'btn-success' : 'btn-danger'}}">{{$category->status ? 'Active':'Disabled'}}</button></td>
                    <td class='form-inline align-middle'>
                        <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.categories.edit', $category->id)}}" class="btn btn-primary btn-xs md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>
                        @role('admin')
						<form method="POST" action="{{route('backend.categories.destroy', $category->id)}}" style="display: inline-block;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" onclick="return confirm('You are about to delete this record?')" class="btn btn-danger btn-xs md-btn-flat article-tooltip" title="Remove"><i class="ion ion-md-close"></i></button>

                        </form>
						@endrole
						</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
