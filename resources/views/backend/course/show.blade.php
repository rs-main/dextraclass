@extends('backend.layouts.layout-3')

@section("styles")

{{--    <link rel="stylesheet" href="{{asset("assets/css/backend.minf700.css?v=1.0.1")}}">--}}
    <link rel="stylesheet" href="{{asset("assets/vendor/%40fortawesome/fontawesome-free/css/all.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/line-awesome/dist/line-awesome/css/line-awesome.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/vendor/remixicon/fonts/remixicon.css")}}">    <!-- Fullcalender CSS -->
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/core/main.css")}}' />
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/daygrid/main.css")}}' />
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/timegrid/main.css")}}' />
    <link rel='stylesheet' href='{{asset("assets/vendor/fullcalendar/list/main.css")}}' />

<style>
    .fc-day-grid-event,.fc-time-grid-event{
        background-color: #f4a965 !important;
    }


</style>

    <style>
        .modal-dialog-full-width {
            width: 100% !important;
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            max-width:none !important;

        }

        .modal-content-full-width  {
            height: auto !important;
            min-height: 100% !important;
            border-radius: 0 !important;
            background-color: #ececec !important
        }

        .modal-header-full-width  {
            border-bottom: 1px solid #9ea2a2 !important;
        }

        .modal-footer-full-width  {
            border-top: 1px solid #9ea2a2 !important;
        }
    </style>
@endsection

@section('content')

   <!-- Content -->
          <div class="container-fluid flex-grow-1 container-p-y">

            <!-- Header -->
            <div class="container-m-nx container-m-ny bg-white mb-4">
			<div class="row">
			<div class="col-md-10">
              <div class="media col-md-10 col-lg-8 col-xl-7 py-5 ml-3">
			 <!--  @if(isset($course->school->logo) && $course->school->logo != 'noimage.jpg')
						<img class="school_logo mb-2 d-block rounded-circle" src='{{url("uploads/schools")}}/{{$course->school->logo}}'  /><br />
			   @endif -->

                <div class="media-body ml-1">
                  <h4 class="font-weight-bold mb-4">{{$course->name}}</h4>
				  <div class="row mb-2">
                      <div class="col-md-3 text-muted">School Name:</div>
                      <div class="col-md-9">
                       <a href="{{route('backend.school.show',$course->school_id)}}" class="text-body">{{$course->school->school_name}}</a>
                      </div>
                    </div>

				</div>

              </div>
			  </div>
			 <div class="col-md-2 ml-10 mt-5">
			<a href="javascript:void(0)" onclick="window.history.go(-1); return false;" class="btn btn-primary rounded-pill d-block detail-back-btn">Back</a>
			</div>
			 </div>
              <hr class="m-0">
            </div>
            <!-- Header -->


       @includeif('backend.message')

		<div class="row">
            <div class="col">
			 <div class="card mb-8">
			 <div class="card-header row ml-0 mr-0">

		<div class="col-md-9"><strong>Classes</strong></div>
		 <div class="col-md-3"><a href="#" class="btn btn-primary rounded-pill d-block" data-toggle="modal" data-target="#createClass"><span class="ion ion-md-add"></span>&nbsp;Create Class</a></div>

		</div>

                  <div class="card-body">
                   <div class="card-datatable table-responsive">
        <table id="classes-list" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Class Name</th>
					<th>Course</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
           <tbody>
                   @php $i=0; @endphp
					@if(!$course->classes->isEmpty())
					@foreach($course->classes as $class)
					<tr>
                     <td>{{ ++$i }}</td>
                    <td>{{$class->class_name}}</td>
					<td>{{$course->name}}</td>
                    <td>{{$class->status ? 'Active':'Disabled'}}</td>
                    <td>
                        <a href ="#" data-attr-id="{{$class->id}}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip edit-class-link" title="Edit"><i class="ion ion-md-create"></i></a>
                        @role('admin')
						<form method="POST" action="{{route('backend.classes.destroy', $class->id)}}" style="display: inline-block;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
							<input type="hidden" name="course" value="{{$course->id}}">
							<input type="hidden" name="ajax_request" value="1">
                            <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" title="Remove"><i class="ion ion-md-close"></i></button>

                        </form>
						@endrole
						<a href ="{{route('backend.classes.show', $class->id)}}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" title="View school details"><i class="ion ion-md-eye"></i></a>
{{--						<a data-school_id="{{$course->school->id}}" data-class_id="{{$class->id}}" data-class="{{$class->class_name}}" data-toggle="modal" data-target="#exampleModalPreview"--}}
{{--                           href ="#" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip class-calendar"--}}
{{--                           title="View school details">--}}
{{--                            <i class="fa fa-calendar" aria-hidden="true"></i>--}}
{{--                        </a>--}}

                        <a data-school_id="{{$course->school->id}}" data-class_id="{{$class->id}}" data-class="{{$class->class_name}}"
                           data-toggle="modal" data-target="#periodModal"
                           href ="#" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip class-calendar period-calendar"
                           title="View school details">
                            <i class="fa fa-clock" aria-hidden="true"></i>
                        </a>
                    </td>
                     </tr>
                    @endforeach
                  @else
                  <tr>
                      <td colspan="5"><p class="text-center">No Record Found!</p></td>
				  </tr>
                  @endif
                  </tbody>
{{--			</tbody>--}}
            </thead>
        </table>
    </div>
                  </div>

                </div>
			</div>


          </div>

	</div>

<!-- create period modal -->
<div class="modal" id="createClass">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Create Class</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form action="{{route('backend.classes.store')}}" method = "post">
            @csrf

			<input type="hidden" name="course" value="{{$course->id}}">
			<input type="hidden" name="ajax_request" value="1">

			 <div class="form-group row">
                <label class="col-form-label col-sm-3 text-sm-right">Class Name</label>
                <div class="col-sm-9">
                    <input type="text" name="class_name" placeholder="Class Name" class="form-control" required>
                </div>
            </div>


           <div class="form-group row">
                <label class="col-form-label col-sm-3 text-sm-right"></label>
                <div class="col-sm-9">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="status" value="1" class="custom-control-input">
                        <span class="custom-control-label">Active</span>
                    </label>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-10 ml-sm-auto">
                    <button data-dismiss="modal" class="btn btn-danger mr-2">Cancel</button> <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
      </div>

    </div>
  </div>
</div>


@include("backend.course.partials.modals.edit-class-modal")

@include("backend.course.partials.modals.calendar-modal")

@include("backend.course.partials.modals.date-event-modal")

@include("backend.course.partials.modals.period-modal")

   <!-- / Content -->
@endsection

@section('scripts')
<!--<script src="{{ mix('/assets/vendor/libs/flatpickr/flatpickr.js') }}"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"
        integrity="sha512-L0nhmpoPbaeBdKIrCVBfMoCvCRhWkaM7Ton8zlfQOtLKIf7sDFZHgkesCgMDGypvAlmxUYqxqaDZDnGH/WOeuA=="
        crossorigin="anonymous"></script>

<script src='{{asset("assets/vendor/fullcalendar/core/main.js")}}'></script>
<script src='{{asset("assets/vendor/fullcalendar/daygrid/main.js")}}'></script>
<script src='{{asset("assets/vendor/fullcalendar/timegrid/main.js")}}'></script>
<script src='{{asset("assets/vendor/fullcalendar/list/main.js")}}'></script>
<script src='{{asset("assets/vendor/fullcalendar/interaction/main.js")}}'></script>

<script>

    let startTime = "";
    let endTime = "";
    let fullStartDateTime = "";
    let fullEndDateTime = "";
    let calendar1;
    function renderCalendar(calendarEvents) {
        const calendarEl = document.getElementById('calendar1');

        calendar1 = new FullCalendar.Calendar(calendarEl, {
            selectable: true,
            plugins: ["timeGrid", "dayGrid", "list", "interaction"],
            timeZone: "UTC",
            defaultView: "dayGridMonth",
            contentHeight: "auto",
            eventLimit: true,
            dayMaxEvents: 4,
            header: {
                left: "prev,next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
            },
            dateClick: function (info) {
                $('#date-event').modal('show')

                let today = new Date();
                let hour = today.getHours().toString().length < 2 ? "0"+today.getHours() : today.getHours();
                let time = hour+ ":" + today.getMinutes();
                let end_time = endTime;
                let date_1 = (info.dateStr).split("-");
                let year = date_1[0];
                let month = date_1[1];
                let day = date_1[2];
                fullStartDateTime = `${year}-${month}-${day}T`;
                fullEndDateTime = `${year}-${month}-${day}T`;

                $('#schedule-start-date').val(`${year}-${month}-${day}T${time}`)
                $("#schedule-end-date").val(`${year}-${month}-${day}T${end_time}`)
            },
            events: calendarEvents
        });

        console.log(calendarEvents);
        calendar1.render();
    }

    function fetchSubjectsForClass(class_id) {
        $.ajax({
            type: "POST",
            url: '{{ route("ajax.school.classsubjects") }}',
            data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
            success: function (data) {
                $("#subject_name").html(data);
            }
        });
    }

    function fetchCalendarEvents(class_id) {
        fetch(`/admin/class-calendar/${class_id}`)
            .then(response => response.json())
            .then(data => {
                let calendarEvents = [];
                let data_array = data.data;
                data_array.forEach((item, index) => {
                    calendarEvents[index] = JSON.parse(item.data);
                })
                fetchPeriodsForClass(class_id);
                renderCalendar(calendarEvents);
            }).catch(function (error) {
            console.log(error);
        });
    }

    function submitCalendarItem(class_id,class_name,school_id) {
        if (jQuery('#calendar1').length) {
            $(document).off().on("submit", "#submit-schedule", function (e) {
                e.preventDefault()
                const title = $(this).find('#schedule-title').val()
                const subject_id = $(this).find('#subject_name').val()
                const subject_name = $(this).find('#subject_name option:selected').text()
                const startDate = ($(this).find('#schedule-start-date').val())
                const endDate = $(this).find('#schedule-end-date').val();
                const color = $(this).find('#schedule-color').val()
                console.log(startDate, endDate, color, subject_name)
                const event = {
                    title: subject_name,
                    start: startDate,
                    end: endDate,
                    color: color
                }
                $(this).closest('#date-event').modal('hide')
                calendar1.addEvent(event);

                $.ajax({
                    type: "POST",
                    url: '{{ route("backend.time-tables.store") }}',
                    data: {
                        'class_id': class_id,
                        'subject_id': subject_id,
                        'school_id': school_id,
                        'start_date_time': startDate,
                        'end_date_time': endDate,
                        'start_date_string': startDate,
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        // fetchCalendarEvents(class_id)
                    }
                });
            })
        }
    }

    $(function () {
        $('#classes-list').dataTable(
            {
                "order": [[ 0, "desc" ]],
                "columns": [
                    null,
                    null,
                    { "orderable": false },
                    null,
                    { "orderable": false }
                ]
            }
        );
    });


    $(".class-calendar").off().on('click', function (){
        let class_id = $(this).data("class_id");
        let class_name = $(this).data("class");
        let school_id = $(this).data("school_id");

        $("#class_title").text(class_name);
        fetchSubjectsForClass(class_id);
        submitCalendarItem(class_id,class_name,school_id);
        fetchCalendarEvents(class_id);
    })

    function fetchPeriodsForClass(class_id){
        $.get(`/admin/class-period-options/${class_id}`,function (data){
            console.log(data);
            $(".period_option").html(data);
        })
    }

    $(".period_option").on("change",function(){
       let optionVal = $(this).val()
       let optionText = $('.period_option option:selected').text()
        console.log(optionVal, " Text " + optionText);
       startTime = $(".period_option option:selected").data("start");
       endTime =   $(".period_option option:selected").data("end");

       console.log((new Date(`${fullStartDateTime}${endTime}`)))

        console.log(`${fullStartDateTime}${startTime}`)
        console.log(`${fullEndDateTime}${endTime}`)

        $('#schedule-start-date').val(`${fullStartDateTime}${startTime}`)
        $("#schedule-end-date").val(`${fullEndDateTime}${endTime}`)
       console.log("end " + endTime + " start " +startTime);
    });

    $(document).ready(function () {
        $(".edit-class-link").on("click", function () {
            var class_id = $(this).attr("data-attr-id");
            $(".edit_class").html("Loading...");
            $("#editClass").modal();
            if(class_id) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("backend.classes.edit_ajax") }}',
                    data: {'class_id' : class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $(".edit_class").html(data);
                        return true;
                    }
                });
            }
        });

        $(".period-calendar").off().on("click",function (){
            let $_this = $(this);
            let class_id = $_this.data("class_id");

            console.log($_this.data("class_id"));
            createPeriodObj.loadPeriods(class_id)

        });
    });

</script>



@stop
