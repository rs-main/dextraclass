<script>
    let createPeriodClassObj = {
        jsId : {
            periodTableBody : "#period-table-body",
            periodTableRow : "#period-table-row"
        },
        jsClass : {

        }
    }

    class createPeriodClass {
        constructor(external) {

            this.ext = external;
        }

        addPeriod() {
            var ids = this.ext.jsId;
            var jsClass = this.ext.jsClass;

            console.log(commonObj.cloneAndAppend(ids.periodTableRow, ids.periodTableBody));
        }

        loadPeriods(class_id){
            var ids = this.ext.jsId;
            var jsClass = this.ext.jsClass;

            const data = { class_id: class_id };

            fetch("{{url("/admin/time-table-class-periods")}}", {
                method: 'POST', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            })
                .then(response => response.json())
                .then(data => {
                    console.log('Success:', data);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        }

    }

    var createPeriodObj = new createPeriodClass(createPeriodClassObj);

</script>
