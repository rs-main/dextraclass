<div class="modal fade right" id="periodModal" tabindex="-1" role="dialog" aria-labelledby="periodModalLabel" aria-hidden="true">
    <div class="modal-dialog-full-width modal-dialog momodel modal-fluid" role="document">
        <div class="modal-content-full-width modal-content ">
            <div class=" modal-header-full-width   modal-header text-center">
                <h5 class="modal-title w-100" id="exampleModalPreviewLabel"><span id="class_title"> </span></h5>
                <button type="button" class="close " data-dismiss="modal" aria-label="Close">
                    <span style="font-size: 1.3em;" aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-block card-stretch">
                            <div class="card-body">
                                <div id="period1">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">Period</th>
                                            <th scope="col">Mon</th>
                                            <th scope="col">Tue</th>
                                            <th scope="col">Wed</th>
                                            <th scope="col">Thur</th>
                                            <th scope="col">Fri</th>
                                        </tr>
                                        </thead>
                                        <tbody id="period-table-body">

                                        @foreach(\App\Models\Period::whereClassId(1)->get() as $period)
                                        <tr id="period-table-row">
                                            <th scope="row">{{$period->title}}</th>
                                            <td>
                                                <button data-period-id="{{$period->id}}" data-period-title="{{$period->title}}" class="btn btn-secondary btn-sm edit-button">Edit</button>
                                            </td>

                                            <td>
                                                <button data-period-id="{{$period->id}}" data-period-title="{{$period->title}}" class="btn btn-secondary btn-sm edit-button">Edit</button>
                                            </td>

                                            <td>
                                                <button data-period-id="{{$period->id}}" data-period-title="{{$period->title}}" class="btn btn-secondary btn-sm edit-button">Edit</button>
                                            </td>

                                            <td>
                                                <button data-period-id="{{$period->id}}" data-period-title="{{$period->title}}" class="btn btn-secondary btn-sm edit-button">Edit</button>
                                            </td>

                                            <td>
                                                <button data-period-id="{{$period->id}}" data-period-title="{{$period->title}}"
                                                        class="btn btn-secondary btn-sm edit-button">Edit</button>
                                            </td>

                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                    <div class="float-right">
                                        <a href="javascript:void(0)" class="btn btn-primary rounded-pill d-block" data-toggle="modal" data-target="#createPeriodModal"><span class="ion ion-md-add"></span> Add period</a>
                                    </div>

                                    @include('backend.course.partials.modals.create-period-modal')

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{--               <div class="modal-footer-full-width  modal-footer">--}}
            {{--                   <button type="button" class="btn btn-danger btn-md btn-rounded" data-dismiss="modal">Close</button>--}}
            {{--                   <button type="button" class="btn btn-primary btn-md btn-rounded">Save changes</button>--}}
            {{--               </div>--}}

        </div>
    </div>
</div>

@include('backend.course.partials.scripts')
