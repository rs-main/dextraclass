<!-- Modal -->
<div class="modal fade" id="createPeriodModal" tabindex="-1" role="dialog" aria-labelledby="createPeriodModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create period</h5>
                <button type="button" class="close" data-toggle="modal" data-target="#createPeriodModal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Period name</label>
                    <input class="form-control" value="" type="text" name="period_name" id="period_name" placeholder="Period name">
                </div>
                <div class="form-group">
                    <label>Start</label>
                    <input class="form-control" value="" type="text" name="period_start_date" id="start_date1" placeholder="Start Date">
                </div>
                <div class="form-group">
                    <label>End</label>
                    <input class="form-control" value="" type="text" name="period_end_date" id="start_date1" placeholder="End Date">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary rounded-pill" data-target="#createPeriodModal" data-toggle="modal">Close</button>
                <button type="button" class="btn btn-primary rounded-pill" onclick="createPeriodObj.addPeriod()">Add</button>
            </div>
        </div>
    </div>
</div>


