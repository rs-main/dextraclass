<!-- Modal -->
<div class="modal" id="date-event" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="popup text-left">
                    <h4 class="mb-3">Add Schedule</h4>
                    <form action="/" id="submit-schedule">
                        <div class="content create-workform row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label" for="schedule-title">Period</label>

                                    <select name="subject_id" id="periods" class="custom-select q_input period_option" required>
                                        <option value="" selected="" disabled="" class="d-none">Select Periods</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label" for="schedule-title">Subject</label>

                                    <select name="subject_id" id="subject_name" class="custom-select q_input" required>
                                        <option value="" selected="" disabled="" class="d-none">Select Subject</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="schedule-start-date">Start Date</label>
                                    <input class="form-control basicFlatpickr date-input"
                                           placeholder="2020-06-20"
                                           type="datetime-local" name="title" id="schedule-start-date" required  readonly/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="schedule-end-date">End Date</label>
                                    <input class="form-control basicFlatpickr date-input"
                                           placeholder="2020-06-20" type="datetime-local"
                                           name="title" id="schedule-end-date" required readonly />
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="title" id="schedule-color"   />
                                </div>
                            </div>
                            <div class="col-md-12 mt-4">
                                <div class="d-flex flex-wrap align-items-ceter justify-content-center">
                                    <button class="btn btn-primary mr-4" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-outline-primary" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
