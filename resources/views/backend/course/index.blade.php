@extends('backend.layouts.layout-2')

@section('scripts')

<script type="text/javascript">

</script>
@endsection

@section('content')

@includeif('backend.message')

<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>Courses</div>
    <a href="{{route('backend.course.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create course</a>
</h4>

<div class="card">
    <div class="card-datatable table-responsive">
        <table id="course-list" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="min-width: 18rem" class="align-top">Course Name</th>
                    <th style="min-width: 18rem" class="align-top">Category</th>
                    <th style="min-width: 18rem">Industry</th>
					<th class="align-top">Status</th>
                    <th style="min-width: 9rem"  class="align-top">Action</th>
                </tr>
            </thead>
            <tbody>
                @php $i=0; @endphp
                @foreach($courses as $course)
                <tr>
                    <td>{{$course->name}}</td>
                    <td>{{$course->school->school_name}}</td>
                    <td class='text-align align-middle '><button class="btn {{$course->status ? 'btn-success':'btn-danger'}}">{{$course->status ? 'Active':'Disabled'}}</button></td>
                    <td class='form-inline align-middle'>
						@if($course->school->school_category != config("constants.BASIC_SCHOOL"))
							<a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.course.edit', $course->id)}}" class="btn btn-primary btn-xs md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>
							@role('admin')
							<form method="POST" action="{{route('backend.course.destroy', $course->id)}}" style="display: inline-block;">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}

								<button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn btn-danger btn-xs md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px; padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

							</form>
							@endrole
						@endif
						<a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.course.show', $course->id)}}" class="btn btn-warning btn-xs md-btn-flat article-tooltip" title="View course details"><i class="ion ion-md-eye"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
