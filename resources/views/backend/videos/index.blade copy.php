@extends('backend.layouts.layout-2')

@section('scripts')

<script type="text/javascript">
    $(function () {

        $("#school").on("change", function () {
            //var school_id = $(this).val();
            var school_id = $('#school option:selected').attr('data-id');

            $.ajax({
                type: "POST",
                url: '{{ route("ajax.school.stdfiltercourses") }}',
                data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    $("#school_course").html(data);
                }
            });

        });

        $("#school_course").on("change", function () {
            // var school_course = $('#school_course').val();
            var school_course = $('#school_course option:selected').attr('data-id');
            if (school_course) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.stdfiltercourseclasses") }}',
                    data: {'course_id': school_course, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#class").html(data);
                    }
                });
            }
        });

        $("#class").on("change", function () {
            var class_id = $('#class option:selected').attr('data-id');

            if (class_id) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.filterclasssubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#subject").html(data);
                    }
                });
            }
        });

        var table = $('#video-list').DataTable({
            "columns": [
                null,
                null,
                {"orderable": false},
                {"orderable": false},
                {"orderable": false},
                {"orderable": false},
                {"orderable": false},
                null,
                {"orderable": false}
            ],
            dom: 'lrtip',
        });
        $('#video_title').on('keyup', function () {
            //alert('gdfgfd');
            regExSearch = this.value;
            table.column(6).search(regExSearch, true, false).draw();
            // table.search(this.value, true, false).draw();
        });
        $('#school').on('change', function () {
            //alert('gdfgfd');
            regExSearch = this.value + '\\s*$';
            table.column(2).search(regExSearch, true, false).draw();
            //table.search(this.value, true, false).draw();
        });

        $('#school_course').on('change', function () {
            //alert('gdfgfd');
            regExSearch = this.value + '\\s*$';
            table.column(3).search(regExSearch, true, false).draw();
            //table.search(this.value, true, false).draw();
        });

        $('#class').on('change', function () {
            //alert('gdfgfd');
            regExSearch = this.value + '\\s*$';
            table.column(4).search(regExSearch, true, false).draw();
            table.search(this.value, true, false).draw();
        });

        $('#subject').on('change', function () {
            //alert('gdfgfd');
            regExSearch = this.value + '\\s*$';
            table.column(5).search(regExSearch, true, false).draw();
            table.search(this.value, true, false).draw();
        });

    });
</script>
@endsection

@section('content')
@includeif('backend.message')
<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>Videos</div>
    <a href="{{route('backend.videos.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Video</a>
</h4>

{{--<div class="card">--}}

{{--    <div class="card-datatable table-responsive">--}}
{{--        <table id="video-list" class="table table-striped table-bordered">--}}
{{--            <thead>--}}
{{--                <tr>--}}
{{--                    <th class="align-top">Date</th>--}}
{{--                    <th class="align-top" style="min-width: 6rem">Period</th>--}}
{{--                    <th class="align-top" style="min-width: 6rem">--}}
{{--                        School--}}
{{--                        @role('admin|subadmin')--}}
{{--                        <select name="school" id="school" class="custom-select">--}}
{{--                            <option value="" selected="">All</option>--}}
{{--                            @foreach($schools as $id => $type)--}}
{{--                            <option value="{{$type}}" data-id="{{$id}}">{{$type}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        @endrole--}}
{{--                    </th>--}}
{{--                    <th class="align-top" style="min-width: 13rem">--}}
{{--                        Course--}}
{{--                        @role('admin|subadmin')--}}
{{--                        <select name="course" id="school_course" class="custom-select">--}}
{{--                            <option value="" selected="">All</option>--}}
{{--                        </select>--}}
{{--                        @endrole--}}
{{--                        @if(Auth::user()->hasRole('school') && empty($classes))--}}
{{--                        <select name="course" id="school_course" class="custom-select">--}}
{{--                            <option value="" selected="">All</option>--}}
{{--                            @foreach($courses as $course)--}}
{{--                            <option value="{{$course->name}}" data-id="{{$course->id}}">{{$course->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        @endif--}}
{{--                    </th>--}}
{{--                    <th class="align-top">--}}
{{--                        Class--}}
{{--                        <select name="class" id="class" class="custom-select">--}}
{{--                            <option value="" selected="">All</option>--}}
{{--                            @foreach($classes as $id => $type)--}}
{{--                            <option value="{{$type}}" data-id="{{$id}}">{{$type}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </th>--}}
{{--                    <th class="align-top">--}}
{{--                        Subject--}}
{{--                        <select name="subject" id="subject" class="custom-select">--}}
{{--                            <option value="" selected="">All</option>--}}
{{--                        </select>--}}
{{--                    </th>--}}
{{--                    <th style="min-width: 15rem" class="align-top">Title--}}
{{--                    <input type="text" name="video_title" id="video_title" class="form-control">--}}
{{--                    </th>--}}
{{--                    <th class="align-top">Status</th>--}}
{{--                    <th class="align-top" style="min-width: 7rem"> Action</th>--}}
{{--                </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}
{{--                @php $i=0; @endphp--}}
{{--                @foreach($videos as $video)--}}
{{--                <tr>--}}
{{--                    <td>{{$video->playOn()}}</td>--}}
{{--                    <td>{{$video->period->title}}</td>--}}
{{--                    <td>{{$video->school->school_name}}</td>--}}
{{--                    <td>{{$video->course->name}}</td>--}}
{{--                    <td>{{$video->classDetail->class_name}}</td>--}}
{{--                    <td>{{$video->subject->subject_name}}</td>--}}
{{--                    <td>--}}
{{--                        <p><strong>{{$video->getTitleAttribute()}}</strong></p>--}}
{{--                        <p>{{$video->getSubTitleAttribute()}}</p>--}}
{{--                    </td>--}}
{{--                    <td class='text-align align-middle '><button class="btn {{$video->status ? 'btn-success':'btn-danger'}}">{{$video->status ? 'Active':'Disabled'}}</button></td>--}}
{{--                    <td class='text-align align-middle'>--}}
{{--                        <a href ="{{route('backend.videos.show', $video->uuid)}}" class="btn-warning  btn-xs  md-btn-flat article-tooltip " style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" title="View video details"><i class="ion ion-md-eye"></i></a>--}}
{{--                        <a href ="{{route('backend.videos.edit', $video->uuid)}}" class="btn-primary  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" title="Edit"><i class="ion ion-md-create"></i></a>--}}
{{--                        @if($repoted_count[$video->id] > 0)<a href ="{{route('backend.videos.show', $video->uuid)}}" title="Reported By"><i class="fas fa-flag" style="color:red"></i> {{$repoted_count[$video->id]}}</a>@endif--}}
{{--                        @role('admin')--}}
{{--                        <form method="POST" action="{{route('backend.videos.destroy', $video->id)}}" style="display: inline-block;">--}}
{{--                            {{ csrf_field() }}--}}
{{--                            {{ method_field('DELETE') }}--}}

{{--                            <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>--}}
{{--                        </form>--}}
{{--                        @endrole--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--                @endforeach--}}
{{--            </tbody>--}}
{{--        </table>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="card">
    <div class="table-responsive">
        <table class="table card-table table-striped table-bordered">
            <thead>
            <tr>
                <th class="align-top" style="min-width: 11rem" >Date</th>
                <th class="align-top" style="min-width: 8rem">Period</th>
                <th class="align-top" style="min-width: 11rem">
                    School
                    @role('admin|subadmin')
                    <select name="school" id="school" class="custom-select">
                        <option value="" selected="">All</option>
                        @foreach($schools as $id => $type)
                            <option value="{{$type}}" data-id="{{$id}}">{{$type}}</option>
                        @endforeach
                    </select>
                    @endrole
                </th>
                <th class="align-top" style="min-width: 8rem">
                    Course
                    @role('admin|subadmin')
                    <select name="course" id="school_course" class="custom-select">
                        <option value="" selected="">All</option>
                        @foreach($courses as $course)
                            <option value="{{$course->name}}" data-id="{{$course->id}}">{{$course->name}}</option>
                        @endforeach
                    </select>
                    @endrole
                    @if(Auth::user()->hasRole('school') && empty($classes))
                        <select name="course" id="school_course" class="custom-select">
                            <option value="" selected="">All</option>
                            @foreach($courses as $course)
                                <option value="{{$course->name}}" data-id="{{$course->id}}">{{$course->name}}</option>
                            @endforeach
                        </select>
                    @endif
                </th>
                <th class="align-top">
                    Class
                    <select name="class" id="class" class="custom-select">
                        <option value="" selected="">All</option>
                        @foreach($classes as $id => $type)
                            <option value="{{$type}}" data-id="{{$id}}">{{$type}}</option>
                        @endforeach
                    </select>
                </th>
                <th class="align-top">
                    Subject
                    <select name="subject" id="subject" class="custom-select">
                        <option value="" selected="">All</option>
                    </select>
                </th>
                <th style="min-width: 15rem" class="align-top">Title
                    <input type="text" name="video_title" id="video_title" class="form-control">
                </th>
                <th class="align-top">Status</th>
                <th class="align-top" style="min-width: 11rem"> Action</th>
            </tr>
            </thead>
            <tbody>
            @php $i=0; @endphp
            @foreach($videos as $video)
                <tr>
                    <td>{{$video->playOn()}}</td>
                    <td>{{$video->period->title}}</td>
                    <td>{{$video->school->school_name}}</td>
                    <td>{{$video->course->name}}</td>
                    <td>{{$video->classDetail->class_name}}</td>
                    <td>{{$video->subject->subject_name}}</td>
                    <td>
                        <p><strong>{{$video->getTitleAttribute()}}</strong></p>
                        <p>{{$video->getSubTitleAttribute()}}</p>
                    </td>
                    <td class='text-align align-middle '><button class="btn {{$video->status ? 'btn-success':'btn-danger'}}">{{$video->status ? 'Active':'Disabled'}}</button></td>
                    <td class='text-align align-middle'>
                        <a href ="{{route('backend.videos.show', $video->uuid)}}" class="btn-warning  btn-xs  md-btn-flat article-tooltip " style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" title="View video details"><i class="ion ion-md-eye"></i></a>
                        <a href ="{{route('backend.videos.edit', $video->uuid)}}" class="btn-primary  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" title="Edit"><i class="ion ion-md-create"></i></a>
                        @if($repoted_count[$video->id] > 0)<a href ="{{route('backend.videos.show', $video->uuid)}}" title="Reported By"><i class="fas fa-flag" style="color:red"></i> {{$repoted_count[$video->id]}}</a>@endif
                        @role('admin')
                        <form method="POST" action="{{route('backend.videos.destroy', $video->id)}}" style="display: inline-block;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>
                        </form>
                        @endrole
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


</div>
<div class="card">
    <div class="card-body">
        {{$videos}}
    </div>
</div>

@endsection
