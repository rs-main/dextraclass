@extends('backend.layouts.layout-2')

@section('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
            integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg=="
            crossorigin="anonymous"></script>

    <script type="text/javascript">

        $(function() {
            $('.subject_select').each(function() {
                $(this)
                    .wrap('<div class="position-relative"></div>')
                    .select2({
                        placeholder: 'Select value',
                        dropdownParent: $(this).parent()
                    });
            })
        });

    </script>

    <style>

        .tt-menu {
            width: 422px;
            margin: 12px 0;
            padding: 8px 0;
            background-color: #fff;
            border: 1px solid #ccc;
            border: 1px solid rgba(0, 0, 0, 0.2);
            -webkit-border-radius: 8px;
            -moz-border-radius: 8px;
            border-radius: 8px;
            -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        }

        .tt-suggestion {
            padding: 3px 20px;
            font-size: 18px;
            line-height: 24px;
            font-family: "Poppins";
            text-transform: lowercase;
        }

        .tt-suggestion:hover {
            cursor: pointer;
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion.tt-cursor {
            color: #fff;
            background-color: #0097cf;
        }

        .tt-suggestion p {
            margin: 0;
        }

        .gist {
            font-size: 14px;
        }

        .empty-message {
            padding: 5px 10px;
            text-align: center;
        }

        .twitter-typeahead{
            width: 97%;
        }
        .tt-dropdown-menu{
            width: 102%;
        }
        input.typeahead.tt-query{ /* This is optional */
            width: 300px !important;
        }

    </style>

    <script type="text/javascript">

        let subject_id = null;
        let school_id = null;
        let course_id = null;
        let class_id = null;

        $('#subject_select').on('change', function () {

            let selected = $(this).find('option:selected');
             subject_id  =     selected.data('subject');
             school_id   =     selected.data('school');
             class_id    =     selected.data('class');
             course_id   =     selected.data('course');

             $("#course").val(course_id);
             $("#subject_id").val(subject_id);
             $("#class").val(class_id);
             $("#school").val(school_id);

            if(subject_id) {
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.subject.topics") }}',
                    data: {'subject_id' : subject_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#topic").html(data);
                    }
                });
            }

            $.ajax({
                type: "POST",
                url: '{{ route("ajax.school.tutors") }}',
                data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    $("#tutor").html(data);
                    // if(tutor_id){
                    //     $("#tutor").val(tutor_id);
                    // }
                }
            });

            $("#course").val(course_id);
            $("#subject_id").val(subject_id);
            $("#class").val(class_id);
            $("#school").val(school_id);

        });

    </script>
@endsection

@section('styles')
<link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/libs/dropzone/dropzone.css')}}">
<style>
    .dz-message {
        margin: 3rem 0;
    }
</style>
@endsection

@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Open School Videos /</span> Create Video
</h4>

@includeif('backend.message')
<form action="{{route('backend.open.video.store')}}" method = "post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">Video General Details</div>
                <hr class="border-light m-0">
                <div class="card-body">
                    <div class="form-group">
                        <label>Subject</label>
                        <select name="course" id="subject_select" class="custom-select subject_select" required>
                            <option value="" disabled selected="">Select Subject</option>
                            @foreach($classes_with_subjects as $class)
                                @if(sizeof($class["subject"]) > 0)
                                    <optgroup label="{{strtoupper($class["class_name"])}}">
                                        @foreach($class["subject"] as $subject_object)
                                            <option
                                                data-school="{{$class["course"]["school_id"]}}"
                                                data-course="{{$class["course_id"]}}"
                                                data-class="{{$class["id"]}}"
                                                data-subject="{{$subject_object["id"]}}"
                                                value="{{  $subject_object["id"]}}" >
                                                {{ $subject_object["subject_name"] }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endif
                            @endforeach
                        </select>

                    </div>

                    <input type="hidden" name="course" id="course" />
                    <input type="hidden" name="subject_id" id="subject_id" />
                    <input type="hidden" name="class" id="class" />
                    <input type="hidden" name="school" id="school" />

                    <div class="form-group">
                        <label>Topic</label>
                        <select name="topic" id="topic" class="custom-select" required>
                            <option value="" selected="" disabled="">{{old("topic")}}Choose Topic</option>
                        </select>
                    </div>

                    <div class="form-group course_wrapper">
                        <label>Tutor</label>
                        <select name="tutor" id="tutor" class="custom-select" required>
                            <option value="" disabled selected="">Select Tutor</option>
                        </select>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="card mb-4">
                <div class="card-header">Video Details</div>
                <hr class="border-light m-0">
                <div class="card-body">
                    <div class="form-inline mb-4">
                        <label class="custom-control custom-radio justify-content-start mr-2">
                            <input name="video_type" type="radio" class="custom-control-input video_type" value="url" required="">
                            <span class="custom-control-label">Video by URL</span>
                        </label>
                        <label class="custom-control custom-radio justify-content-start mr-2">
                            <input name="video_type" type="radio" class="custom-control-input video_type" value="file" >
                            <span class="custom-control-label">Video by File</span>
                        </label>
                    </div>

                    <div class="form-group video_url_section" style="display: none;">
                        <label>Video URL</label>
                        <input type="url" value="{{old('video_url')}}" placeholder="Video URL" class="form-control" name="video_url" />
                        <small class="form-text text-muted">Example - https://vimeo.com/{video_id} </small>
                    </div>


{{--                    <a href="#" class="btn btn-primary rounded-pill" id="upload" data-toggle="modal" data-target="#upload-modal">--}}
{{--                        <span class="fas fa-upload"></span>&nbsp;Upload Video for Mobile</a>--}}
{{--                    <br>--}}
{{--                    <br>--}}

{{--                    <div class="form-group">--}}
{{--                        <label>Upload Video for mobile</label>--}}
{{--                        <input type="hidden" name="video_file" id="uploadedVideoFile" value=""/>--}}
{{--                        <div id="videoFileUpload" class="dropzone">--}}
{{--                            <div class="dz-message needsclick" >--}}
{{--                                Drop files here or click to upload--}}
{{--                                <span class="note needsclick"><small class="form-text text-muted">Allowed file types: .mp4 .webm .wmv .avi .flv .mov and up to 300 MB</small></span>--}}
{{--                            </div>--}}
{{--                            <div class="fallback">--}}
{{--                                <input type="file" name="videofile">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}


                    <div class="form-group">
                        <label>Upload Video for mobile</label>
                        <input type="hidden" name="video_file" id="uploadedVideoFile" value=""/>
                        <div id="videoFileUpload" class="dropzone">
                            <div class="dz-message needsclick" >
                                Drop files here or click to upload
                                <span class="note needsclick"><small class="form-text text-muted">Allowed file types: .mp4 .webm .wmv .avi .flv .mov and up to 300 MB</small></span>
                            </div>
                            <div class="fallback">
                                <input type="file" name="videofile">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Upload Note (If any)</label>
                        <input type="hidden" name="note_file" id="uplodedNoteFile" value="{{old('note_file')}}"/>
                        <div id="noteFileUpload" class="dropzone">
                          <div class="dz-message needsclick" >
                            Drop files here or click to upload
                            <span class="note needsclick"><small class="form-text text-muted">Allowed file types: jpeg, png, pdf, doc, docx, ppt, pptx, zip and up to 50 MB</small></span>
                          </div>
                          <div class="fallback">
                              <input type="file" name="notefile">
                          </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Video Description</label>
                        <textarea class="form-control" name="description" rows="3" required>{{old('description')}}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Keywords</label>
                        <input type="text" value="{{old('keywords')}}" name="keywords" id="keywords" data-role="tagsinput" class="form-control" />
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3">Status</label>
                        <div class="col-sm-6">
                            <label class="switcher switcher-lg switcher-success">
                                <input type="checkbox" name="status" value="1" class="switcher-input status">
                                <span class="switcher-indicator">
                                    <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                                    <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <hr class="border-light m-0">
                <div class="card-footer d-block text-center">
                    <button type="submit" class="btn btn-primary" id="smt">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>

</div>



@php
$url = '';$size = 0;
if(old('note_file')) {
    $url = Storage::disk('s3')->url(old('note_file'));
    $size = Storage::disk('s3')->size(old('note_file'));
}
@endphp
@endsection

@section('scripts')

<script src="{{asset('assets/vendor/libs/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
<script src="{{asset('assets/vendor/libs/dropzone/dropzone.js')}}"></script>
@role('admin|subadmin')
<script>
    var category_id = "{{old('institute_type')}}";
    var school_id = "{{old('school')}}";
</script>
@endrole
@role('school')
<script>
    var category_id = "{{$category_id}}";
    var school_id = "{{$school_id}}";
</script>
@endrole
<script>
    $(document).ready(function () {

        $('#keywords').tagsinput({ tagClass: 'badge badge-secondary' });

        $('.period_field_group').hide();

        $(".video_type[value=url]").prop( "checked", true );
        $(".video_url_section").show();
        $('.status').prop( "checked", true );
        $('.status').prop( "disabled", true );

        $(".video_type").click(function() {

            var type = $(this).val();

            if(type == 'url') {
                $(".video_url_section").show();
                $('.status').prop( "checked", true );
                $('.status').prop( "disabled", false );
            } else {
                $('input[name=video_url]').val('');
                $(".video_url_section").hide();
                $('.status').prop( "checked", false );
                $('.status').prop( "disabled", true );
            }
        });

    /********* Upload Note file using dropzone *******************/
    // Dropzone class:
        $("div#noteFileUpload").dropzone({
            url: "{{route('ajax.dropzone.upload.note')}}",
            acceptedFiles: ".jpg,.jpeg.png,.pdf,.doc,.docm,.docx,.docx,.dot,.xls,.xlsb,.ppt",
            maxFilesize: 500000, /* you can upload only 50mb  */
            maxFiles: 1,
            paramName: "notefile",
            addRemoveLinks: true,
            accept: function (file, done) {
                $("#smt").attr("disabled", "disabled");
                if (this.files.length > 1) {
                    done("Sorry you can not upload any media.");
                }
                else {
                    done();
                }
            },
            init: function () {
                var mydropzone = this;
                if("{{$url}}" != ''){
                    var mockFile = { name: "{{basename(old('note_file'))}}",size: '{{$size}}' };
                    mydropzone.options.addedfile.call(mydropzone, mockFile);
                    mydropzone.options.thumbnail.call(mydropzone, mockFile, "{{$url}}");
                }
                this.on("maxfilesexceeded", function(file){
                    this.removeFile(file);
                });

                mydropzone.on("success", function (file, response) {
                    $('#uplodedNoteFile').val(response.savefilename);
                    $("#smt").removeAttr("disabled");
                });

                this.on("removedfile", function (file) {
                  if (this.files.length == 0){
                     $("#uplodedNoteFile").val('');
                  }
                });
            },
            sending: function (file, xhr, formData) {
                formData.append('_token', "{{ csrf_token() }}");
            }
    });

        // Dropzone class:
        $("div#videoFileUpload").dropzone({
            url: "{{route('ajax.dropzone.upload.video')}}",
            acceptedFiles: ".mp4,.webm,.wmv,.avi,.flv,.mov ",
            maxFilesize: 500, /* you can upload only 500mb  */
            maxFiles: 1,
            paramName: "videofile",
            timeout: 3600000,
            addRemoveLinks: true,
            accept: function (file, done) {
                $("#smt").attr("disabled", "disabled");
                if (this.files.length > 1) {
                    done("Sorry you can not upload anymore media.");
                }
                else {
                    done();
                }
            },
            init: function () {
                var mydropzone = this;
                if("{{$url}}" != ''){
                    var mockFile = { name: "{{basename(old('video_file'))}}",size: '{{$size}}' };
                    mydropzone.options.addedfile.call(mydropzone, mockFile);
                    mydropzone.options.thumbnail.call(mydropzone, mockFile, "{{$url}}");
                }
                this.on("maxfilesexceeded", function(file){
                    this.removeFile(file);
                });

                mydropzone.on("success", function (file, response) {

                    $('#uploadedVideoFile').val(response.savefilename);
                    $("#smt").removeAttr("disabled");
                    $('.dz-nopreview').hide();
                });

                this.on("removedfile", function (file) {
                    if (this.files.length == 0){
                        $("#uploadedVideoFile").val('');
                    }
                });

                mydropzone.on('canceled', function (){
                    $('.dz-error-message').html("Upload timed out");
                });

                mydropzone.on("totaluploadprogress", function(progress) {
                    if(progress == 100){
                        $('.progress').hide();
                        $('.dz-nopreview')
                            .html('<div class="text-center"><img src="{{url("images/LoaderIcon.gif")}}"> </div>')
                    }
                });
            },
            sending: function (file, xhr, formData) {
                formData.append('_token', "{{ csrf_token() }}");
            }

        });

    });
</script>

@stop
