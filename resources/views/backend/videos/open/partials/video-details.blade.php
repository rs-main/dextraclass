<div class="col-xl-6">
    <div class="card mb-4">
        <div class="card-header">Video Details</div>
        <hr class="border-light m-0">
        <div class="card-body">
            <div class="form-inline mb-4">
                <label class="custom-control custom-radio justify-content-start mr-2">
                    <input name="video_type" type="radio" class="custom-control-input video_type" checked value="url" required="">
                    <span class="custom-control-label">Video by URL</span>
                </label>

            </div>

            <div class="form-group video_url_section">
                <label>Video URL</label>
                <input type="url" value="{{old('video_url')}}" placeholder="Video URL" class="form-control" name="video_url" />
                {{--                        <small class="form-text text-muted">Example - https://vimeo.com/{video_id} </small>--}}
                <small class="form-text text-muted">Example - https://video.desafrica.com?m={video_id} </small>
            </div>

            <br>

            <div class="form-group">
                <label>Upload Video for mobile</label>
                <input type="hidden" name="video_file" id="uploadedVideoFile" value=""/>
                <div id="videoFileUpload" class="dropzone">
                    <div class="dz-message needsclick" >
                        Drop files here or click to upload
                        <span class="note needsclick"><small class="form-text text-muted">Allowed file types: .mp4 .webm .wmv .avi .flv .mov and up to 300 MB</small></span>
                    </div>
                    <div class="fallback">
                        <input type="file" name="videofile">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Upload Note (If any)</label>
                <input type="hidden" name="note_file" id="uplodedNoteFile" value="{{old('note_file')}}"/>
                <div id="noteFileUpload" class="dropzone">
                    <div class="dz-message needsclick" >
                        Drop files here or click to upload
                        <span class="note needsclick"><small class="form-text text-muted">Allowed file types: jpeg, png, pdf, doc, docx, ppt, pptx, zip and up to 50 MB</small></span>
                    </div>
                    <div class="fallback">
                        <input type="file" name="notefile">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Video Description</label>
                <textarea class="form-control" name="description" rows="3" required>{{old('description')}}</textarea>
            </div>

            <div class="form-group">
                <label>Keywords</label>
                <input type="text" value="{{old('keywords')}}" name="keywords" id="keywords" data-role="tagsinput" class="form-control" />
            </div>

            <div class="form-group row">
                <label class="col-sm-3">Status</label>
                <div class="col-sm-6">
                    <label class="switcher switcher-lg switcher-success">
                        <input type="checkbox" name="status" value="1" class="switcher-input status">
                        <span class="switcher-indicator">
                                    <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                                    <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                                </span>
                    </label>
                </div>
            </div>
        </div>
        <hr class="border-light m-0">
        <div class="card-footer d-block text-center">
            <button type="submit" class="btn btn-primary" id="smt">Submit</button>
        </div>
    </div>
</div>
