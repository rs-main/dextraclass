@extends('backend.layouts.layout-2')

@section('scripts')

    <script src="{{asset("assets/vendor/libs/datatables/datatables.js")}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(function () {

            const table = $('#school-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('backend.schools.data') }}",
                columns: [
                    {data: 'school_name', name: 'school_name'},
                    {data: 'institution', name: 'institution'},
                    {data: 'featured', name: 'featured'},
                    {data: 'locked', name: 'locked'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });

    </script>

@endsection

<style>
    .yes{
        background-color: #02BC77;
        color: white;
        padding: 3px 5px;
        border-radius: 4px;
    }

    .no{
        background-color: #d9534f;
        color: white;
        padding: 2px 5px;
        border-radius: 4px;
    }

</style>
@section('content')

@includeif('backend.message')

<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>Schools</div>
	@role('admin|subadmin')
		<a href="{{route('backend.school.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create school</a>
	@endrole
	</h4>

<div class="card">
    <div class="card-datatable table-responsive">
        <table id="school-list" class="table table-striped table-bordered">
            <thead>
                <tr>
{{--                    <th>No.</th>--}}
                    <th style="min-width: 18rem" class="align-top">School Name</th>
                    <th style="min-width: 18rem" class="align-top">Institution</th>
					<th class="align-top">Featured</th>
					<th class="align-top">Locked</th>
					<th class="align-top">Status</th>
                    <th class="align-top">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection
