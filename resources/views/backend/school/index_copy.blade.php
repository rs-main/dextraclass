@extends('backend.layouts.layout-2')

@section('scripts')

    <script src="{{asset("assets/vendor/libs/datatables/datatables.js")}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(function () {

            const table = $('#streams-data').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('backend.schools.data') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'school_name', name: 'school_name'},
                    {data: 'institution', name: 'institution'},
                    {data: 'featured', name: 'featured'},
                    {data: 'locked', name: 'locked'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });

    </script>

@endsection

<style>
    .yes{
        background-color: #02BC77;
        color: white;
        padding: 3px 5px;
        border-radius: 4px;
    }

    .no{
        background-color: #d9534f;
        color: white;
        padding: 2px 5px;
        border-radius: 4px;
    }

</style>
@section('content')

@includeif('backend.message')

<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>Schools</div>
	@role('admin|subadmin')
		<a href="{{route('backend.school.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create school</a>
	@endrole
	</h4>

<div class="card">
    <div class="card-datatable table-responsive">
        <table id="school-list" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th style="min-width: 18rem" class="align-top">School Name</th>
                    <th style="min-width: 18rem" class="align-top">Institution</th>
					<th class="align-top">Featured</th>
					<th class="align-top">Locked</th>
					<th class="align-top">Status</th>
                    <th class="align-top">Action</th>
                </tr>
            </thead>
            <tbody>
                @php $i=0; @endphp
                @foreach($schools as $school)
                <tr>
                    <td>{{$school->school_name}}</td>
                    <td>{{$school->category? $school->category->name : ""}}</td>
                    <td class='text-align align-middle'><span class="{{$school->featured ? 'yes':'no'}}">{{$school->featured ? 'Yes':'No'}}</span></td>
                    <td class='text-align align-middle'><span class="{{$school->is_locked ? 'yes':'no'}}">{{$school->is_locked ? 'Yes':'No'}}</span></td>
                    <td class='text-align align-middle'><button class="btn {{$school->status ? 'btn-success':'btn-danger'}}">{{$school->status ? 'Active':'Disabled'}}</button></td>
                    <td class='form-inline align-middle'>

                        <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.school.edit', $school->id)}}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>

                        @role('admin')
							<form method="POST" action="{{route('backend.school.destroy', $school->id)}}" style="display: inline-block;">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}

								<button style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" type="submit" onclick="return confirm('You are about to delete this record?')" class="btn btn-danger btn-xs md-btn-flat article-tooltip" title="Remove"><i class="ion ion-md-close"></i></button>

							</form>
						@endrole

						<a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.school.show', $school->id)}}" class="btn btn-primary btn-xs md-btn-flat article-tooltip" title="View school details">
                            <i class="ion ion-md-eye"></i></a>

						<!-- <a href ="{{route('backend.course.create', $school->id)}}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" title="Create school course"><i class="ion ion-md-add"></i></a> -->
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div style="text-align: center">
        <ul class="pagination-custom list-unstyled list-inline">
            {!! $schools->render() !!}
        </ul>
    </div>

</div>
@endsection
