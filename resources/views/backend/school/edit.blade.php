@extends('backend.layouts.layout-2')

@section("styles")
    <style>
        .margin-right-10{
            margin-right: 10px;
        }

        .tabbable-panel {
            border:1px solid #eee;
            padding: 10px;
        }

        /* Default mode */
        .tabbable-line > .nav-tabs {
            border: none;
            margin: 0px;
        }
        .tabbable-line > .nav-tabs > li {
            margin-right: 2px;
        }
        .tabbable-line > .nav-tabs > li > a {
            border: 0;
            margin-right: 0;
            color: #737373;
        }
        .tabbable-line > .nav-tabs > li > a > i {
            color: #a6a6a6;
        }
        .tabbable-line > .nav-tabs > li.open, .tabbable-line > .nav-tabs > li:hover {
            border-bottom: 4px solid #fbcdcf;
        }
        .tabbable-line > .nav-tabs > li.open > a, .tabbable-line > .nav-tabs > li:hover > a {
            border: 0;
            background: none !important;
            color: #333333;
        }
        .tabbable-line > .nav-tabs > li.open > a > i, .tabbable-line > .nav-tabs > li:hover > a > i {
            color: #a6a6a6;
        }
        .tabbable-line > .nav-tabs > li.open .dropdown-menu, .tabbable-line > .nav-tabs > li:hover .dropdown-menu {
            margin-top: 0px;
        }
        .tabbable-line > .nav-tabs > li.active {
            border-bottom: 4px solid #f3565d;
            position: relative;
        }
        .tabbable-line > .nav-tabs > li.active > a {
            border: 0;
            color: #333333;
        }
        .tabbable-line > .nav-tabs > li.active > a > i {
            color: #404040;
        }
        .tabbable-line > .tab-content {
            margin-top: -3px;
            background-color: #fff;
            border: 0;
            border-top: 1px solid #eee;
            padding: 15px 0;
        }
        .portlet .tabbable-line > .tab-content {
            padding-bottom: 0;
        }

        /* Below tabs mode */

        .tabbable-line.tabs-below > .nav-tabs > li {
            border-top: 4px solid transparent;
        }
        .tabbable-line.tabs-below > .nav-tabs > li > a {
            margin-top: 0;
        }
        .tabbable-line.tabs-below > .nav-tabs > li:hover {
            border-bottom: 0;
            border-top: 4px solid rgba(52, 143, 240,0.5);
            /*border-top: 4px solid #fbcdcf;*/
        }
        .tabbable-line.tabs-below > .nav-tabs > li.active {
            margin-bottom: -2px;
            border-bottom: 0;
            border-top: 4px solid #348FF1;
            /*border-top: 4px solid #f3565d;*/
        }
        .tabbable-line.tabs-below > .tab-content {
            margin-top: -10px;
            border-top: 0;
            border-bottom: 1px solid #eee;
            padding-bottom: 15px;
        }

        .tabbable-line > .nav-tabs > li.active {
            border-bottom: 4px solid #24ABF2;
            position: relative;
        }

        .tabbable-line > .nav-tabs > li:hover {
            border-bottom: 4px solid #24ABF2;
            position: relative;
        }
    </style>
@endsection

@section("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
            integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg==" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyAXI-1JdxHZTy3nb700Mpp1X21pzqAmU_c&callback=initAutocomplete&region=Gh" async defer></script>

    @include("backend.school.partials.location_scripts")

    <script>

        $(".nav-tabs li").on("click",function (){
            $(".nav-tabs li").removeClass("active");
            console.log($(this).addClass("active"));
            console.log($(this).children());
        })

        setTimeout(function (){
            console.log("School Gender {{$school->gender}}" );
            console.log("School Status {{$school->boarding_status}}" );
            $("#school_gender").val("{{$school->gender}}")
{{--            selectElement('school_gender', {{$school->gender}})--}}
            $("#boarding_status").val("{{$school->boarding_status}}")
            $("#sid_status").val("{{$school->sid_status}}")
            $("#track").val("{{$school->track}}")
        },2000);

        function selectElement(id, valueToSelect) {
            let element = document.getElementById(id);
            element.value = valueToSelect;
        }

        function editLocation(status){
            if (status === "start") {
                $("#edit-location-block").show();
                $("#edit-button").hide();
                $("#stop-edit-button").show();
                $("#location_change_status").val("true");
            }else{
                $("#edit-location-block").hide();
                $("#edit-button").show();
                $("#stop-edit-button").hide();
                $("#location_change_status").val("false");
            }
        }
    </script>
@endsection



@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Schools /</span> Edit School
</h4>

<div class="card mb-4">
{{--    <h6 class="card-header">--}}
{{--        Edit School--}}
{{--    </h6>--}}

    <div class="tabbable-panel">
        @includeif('backend.message')
        <form action="{{route('backend.school.update', $school->id)}}" method = "post" enctype="multipart/form-data">
            {{--        <form action="" method = "post" enctype="multipart/form-data">--}}
            @csrf
        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#tab_default_1" data-toggle="tab">
                        <h6 class="card-header">
                            Basic
                        </h6>
                    </a>
                </li>

                <li>
                    <a href="#tab_default_2" data-toggle="tab">
                        <h6 class="card-header">
                            SID Info
                        </h6>
                    </a>
                </li>

                <li>
                    <a href="#tab_default_3" data-toggle="tab">
                        <h6 class="card-header">
                            CMS
                        </h6>
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_default_1">
                    <div class="card-body">
                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Institute Type</label>
                                <div class="col-sm-10">
                                    <select name="school_category" class="custom-select" disabled required>
                                        <option value="">Select Institute Type</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" @if($school->school_category == $category->id) selected="selected" @endif>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="school_category" value="{{$school->school_category}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">School Code</label>
                                <div class="col-sm-10">
                                    <input type="text" placeholder="Assigned school code" class="form-control" name="school_code" value="{{$school->school_code}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="school_name" placeholder="School Name" class="form-control" value="{{$school->school_name}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Short Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="short_name" placeholder="School Short Name" value="{{$school->short_name}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Description</label>
                                <div class="col-sm-10">
                                    <textarea name="description" class="form-control" placeholder="School Description">{{$school->description}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Crest</label>
                                <div class="col-sm-10">
                                    @if(isset($school->logo) && $school->logo != 'noimage.jpg')
                                        <img class="school_logo mb-2" src='{{url("uploads/schools/$school->logo")}}' /><br />
                                    @endif
                                    <input type="file" id="school_logo" name="school_logo">

                                    <small class="form-text mb-4">
                                        .jpg .png .bmp  |  Size max - 2mb<br>
                                    </small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Gender</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="gender" id="school_gender">
                                        <option value="boys">Boys</option>
                                        <option value="girls">Girls</option>
                                        <option value="mixed">Mixed</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">School Status</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="boarding_status" required id="boarding_status">
                                        <option value="day">Day</option>
                                        <option value="boarding">Boarding</option>
                                        <option value="day-boarding">Day/Boarding</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Location</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="location_id" id="location" disabled>
                                        <option value="{{$school->location_id}}">{{$school->location_name}}</option>
                                    </select>
                                </div>

                                <div class="col-sm-2">
                                    <button class="btn btn-success " type="button" id="edit-button" onclick="editLocation('start')">edit</button>
                                    <button class="btn btn-danger " style="display: none" type="button" id="stop-edit-button" onclick="editLocation('close')">cancel</button>
                                </div>
                            </div>

                            <div id="edit-location-block" style="display: none">

                                <input type="hidden" id="location_change_status" name="location_change_status" value="false"/>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2 text-sm-right">Location Type</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="location_type" id="location-type" required>
                                            <option>Choose Location Type</option>
                                            <option value="ghana-post-code">Ghana Post Code</option>
                                            <option value="current-location">Pick Current Location</option>
                                            <option value="manual">Manually Type</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row" style="display: none;" id="manual-location">
                                    <label class="col-form-label col-sm-2 text-sm-right">Location</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="location_id" id="location">
                                            <option value="">select location</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row" id="address_box" style="display: none;">
                                    <label class="col-form-label col-sm-2 text-sm-right">Address</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="address" onFocus="geolocate()" id="autocomplete" class="form-control" autocomplete="false" />
                                    </div>
                                </div>

                                <div class="form-group row ghana-post-row" style="display: none;">
                                    <label class="col-form-label col-sm-2 text-sm-right">Ghana Post Code</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="ghana_post_code"
                                               id="ghana_post_code" placeholder="Your Ghana Post Code" onfocusout="translateCode()"/>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-form-label col-sm-2 text-sm-right">Location Details</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="street_name"
                                               id="street_name" placeholder="Street name" readonly/>
                                        <small><i> School's location</i></small>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="district"
                                               id="district" placeholder="District" readonly/>
                                        <small style="padding-left: 10px;"><i>District's name</i></small>

                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="region"
                                               id="region" placeholder="Region" readonly/>
                                        <small style="padding-left: 10px;"><i>Region's name</i></small>

                                    </div>

                                    {{--                <div class="col-md-2">--}}
                                    {{--                    <input type="text" class="form-control" name="coordinates"--}}
                                    {{--                           id="coordinates" placeholder="Coordinates" readonly/>--}}
                                    {{--                </div>--}}

                                    <input type="hidden" name="coordinates"  id="coordinates"/>

                                </div>

                                <div class="row">
                                    <p id="demo" style="margin-left: 20px;"></p>
                                </div>

                                <input type="hidden" name="address_line_1" id="address_line_1" />

                                <input type="hidden" name="google_place_id" id="google_place_id" />

                                <input type="hidden" name="place_identifier" id="place_details" />

                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Ownership</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="ownership" id="ownership">
                                        <option value="private">Private</option>
                                        <option value="public">Public</option>
                                    </select>
                                </div>
                            </div>

                            @role('admin|subadmin')
                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Theme</label>
                                <div class="col-sm-10">
                                    <select name="theme" class="custom-select" required>
                                        <option value="">Select Theme</option>
                                        @foreach(config('constants.themes') as $theme)
                                            <option value="{{$theme}}" @if($school->theme == $theme) selected @endif>{{ucwords($theme)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right" style="margin-top:-10px">Programmes</label>

                                <div class="courses_container d-inline-flex" style="margin-left: 12px">
                                    <div class="sm-2 margin-right-10">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="agriculture" {{ $school->agriculture ? "checked" : ""}}  class="custom-control-input">
                                            <span class="custom-control-label">Agric</span>
                                        </label>
                                    </div>

                                    <div class="sm-2 margin-right-10">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="business" {{ $school->business ? "checked" : ""}} class="custom-control-input">
                                            <span class="custom-control-label">Business</span>
                                        </label>
                                    </div>

                                    <div class="sm-2 margin-right-10">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="tech" {{ $school->tech ? "checked" : ""}} class="custom-control-input">
                                            <span class="custom-control-label">Technical</span>
                                        </label>
                                    </div>

                                    <div class="sm-2 margin-right-10">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="visual_arts" {{ $school->visual_arts ? "checked" : ""}}  class="custom-control-input">
                                            <span class="custom-control-label">Visual Arts</span>
                                        </label>
                                    </div>

                                    <div class="sm-2 margin-right-10">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="gen_arts" {{ $school->gen_arts ? "checked" : ""}}  class="custom-control-input">
                                            <span class="custom-control-label">General Arts</span>
                                        </label>
                                    </div>

                                    <div class="sm-2 margin-right-10">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" name="gen_science" {{ $school->gen_science ? "checked" : ""}} class="custom-control-input">
                                            <span class="custom-control-label">General Science</span>
                                        </label>
                                    </div>

                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right"></label>
                                <div class="col-sm-10">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="status" value="1" class="custom-control-input" @if($school->status) checked @endif>
                                        <span class="custom-control-label">Active</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right"></label>
                                <div class="col-sm-10">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="featured" value="1" class="custom-control-input" @if($school->featured) checked @endif>
                                        <span class="custom-control-label">Featured</span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right"></label>
                                <div class="col-sm-10">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="restrict_to_student" value="1" class="custom-control-input" id="restrict_to_student" @if($school->restrict_to_student) checked @endif>
                                        <span class="custom-control-label">Restrict to limited students</span>
                                    </label>
                                </div>
                            </div>


                            <div class="form-group row student_limit" @if(!$school->restrict_to_student) style="display:none;" @endif>
                                <label class="col-form-label col-sm-2 text-sm-right">Student limit</label>
                                <div class="col-sm-10">
                                    <input type="number" name="student_limit" value="{{ $school->student_limit }}" class="form-control">
                                </div>
                            </div>

                            @endrole

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right"></label>
                                <div class="col-sm-10">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="is_locked" value="1" class="custom-control-input" @if($school->is_locked) checked @endif>
                                        <span class="custom-control-label">Locked</span>
                                    </label>
                                </div>
                            </div>

{{--                            <div class="form-group row">--}}
{{--                                <div class="col-sm-10 ml-sm-auto">--}}
{{--                                    <a href = "{{route('backend.schools')}}" class="btn btn-danger mr-2">Cancel</a>--}}
{{--                                    <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                    </div>

                </div>
                <div class="tab-pane" id="tab_default_2">
                    <div class="card-body">
                        @includeif('backend.message')
{{--                        <form action="{{route('backend.school.update', $school->id)}}" method = "post" enctype="multipart/form-data">--}}
                            {{--        <form action="" method = "post" enctype="multipart/form-data">--}}
{{--                            @csrf--}}

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Contact Person</label>
                                <div class="col-sm-10">
                                    <input type="text" placeholder="Contact person" class="form-control" name="contact_person"
                                           value="{{$school->contact_person}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Contact Person's Number</label>
                                <div class="col-sm-10">
                                    <input type="text" name="contact_person_number" placeholder="Contact Person" class="form-control" value="{{$school->contact_person_number}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Form 1 Population</label>
                                <div class="col-sm-10">
                                    <input type="text" name="form_one" placeholder="Form one" value="{{$school->form_one}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Form 2 Population</label>
                                <div class="col-sm-10">
                                    <input type="text" name="form_two" placeholder="Form two" value="{{$school->form_two}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Form 3 Population</label>
                                <div class="col-sm-10">
                                    <input type="text" name="form_three" placeholder="Form three" value="{{$school->form_three}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Track</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="track" id="track">
                                        <option value="Single track">Single track</option>
                                        <option value="Double track" >Double track</option>
                                    </select>
{{--                                    <input type="text" name="track" placeholder="Track" value="{{$school->track}}" class="form-control">--}}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label col-sm-2 text-sm-right">Sid Status</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="sid_status" value="{{$school->sid_status}}" disabled/>

{{--                                    <input type="text" name="sid_status" placeholder="Sid Status" value="{{$school->sid_status}}" class="form-control">--}}
                                </div>
                            </div>

                            {{--            <div class="form-group row">--}}
                            {{--                <label class="col-form-label col-sm-2 text-sm-right"></label>--}}
                            {{--                <div class="col-sm-10">--}}
                            {{--                    <label class="custom-control custom-checkbox">--}}
                            {{--                        <input type="checkbox" name="physically_challenged"--}}
                            {{--                               class="custom-control-input" @if($school->physically_challenged) checked @endif>--}}
                            {{--                        <span class="custom-control-label">Physically Challenged</span>--}}
                            {{--                    </label>--}}
                            {{--                </div>--}}
                            {{--            </div>--}}

{{--                            <div class="form-group row">--}}
{{--                                <div class="col-sm-10 ml-sm-auto">--}}
{{--                                    <a href = "{{route('backend.schools')}}" class="btn btn-danger mr-2">Cancel</a>--}}
{{--                                    <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
                    </div>

                </div>

                <div class="tab-pane" id="tab_default_3">
                    <div class="card-body">
                        @includeif('backend.message')

                        @role('admin|subadmin')
                        <div class="form-group row">
                            <label class="col-form-label col-sm-2 text-sm-right">Theme</label>
                            <div class="col-sm-10">
                                <select name="theme" class="custom-select" required>
                                    <option value="">Select Theme</option>
                                    @foreach(config('constants.themes') as $theme)
                                        <option value="{{$theme}}" @if($school->theme == $theme) selected @endif>{{ucwords($theme)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @endrole

                    </div>

                </div>

            </div>

        </div>

            <div class="form-group row">
                <div class="col-sm-10 ml-sm-auto">
                    <a href = "{{route('backend.schools')}}" class="btn btn-danger mr-2">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </form>
    </div>

</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {

		$("#restrict_to_student").on("click", function () {
			if($(this). prop("checked") == true){
				$(".student_limit").show();
			} else {
				$(".student_limit").hide();
			}
		 });

        setTimeout(function (){
            console.log("School Gender {{$school->gender}}" );
            console.log("School Status {{$school->boarding_status}}" );
            {{--$("#school_gender").val("{{$school->gender}}")--}}
            selectElement('school_gender', {{$school->gender}})
            $("#boarding_status").val("{{$school->boarding_status}}")
        },2000);

});


    function selectElement(id, valueToSelect) {
        let element = document.getElementById(id);
        element.value = valueToSelect;
    }

    // function editLocation(){
    //     alert("hello")
    // }

</script>
@stop
