@extends('backend.layouts.layout-2')

@section("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
            integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg==" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

{{--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXI-1JdxHZTy3nb700Mpp1X21pzqAmU_c&libraries=places&callback=initAutocomplete" async defer></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyAXI-1JdxHZTy3nb700Mpp1X21pzqAmU_c&callback=initAutocomplete&region=Gh" async defer></script>

    @include("backend.school.partials.location_scripts")

        <style>
        .margin-right-10{
            margin-right: 10px;
        }
    </style>
@endsection

@section('content')
<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Schools /</span> Create School
</h4>

<div class="card mb-4">
    <h6 class="card-header">
        Create School
    </h6>
    <div class="card-body">
        @includeif('backend.message')
        <form action="{{route('backend.school.store')}}" methSod = "post" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Institute Type</label>
                <div class="col-sm-10">
                    <select name="school_category" class="custom-select" required id="school_category">
                        <option value="">Select Institute Type</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}" @if(old('school_category') == $category->id) selected @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">School Code</label>
                <div class="col-sm-10">
                    <input type="text" placeholder="Assigned school code" class="form-control" name="school_code">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="school_name" placeholder="School Name" value="{{old('school_name')}}" class="form-control" required>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Short Name</label>
                <div class="col-sm-10">
                    <input type="text" name="short_name" value="{{old('short_name')}}" placeholder="School Short Name" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Description</label>
                <div class="col-sm-10">
                    <textarea name="description" class="form-control" placeholder="School Description">{{old('description')}}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Crest</label>
                <div class="col-sm-10">
                    <input type="file" id="school_logo" name="school_logo">
                    <small class="form-text mb-4">
                        .jpg .png .bmp  |  Size max >= 2mb |  @ 212px by 150px<br>
                    </small>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Location Type</label>
                <div class="col-sm-8">
                    <select class="form-control" name="location_type" id="location-type" required>
                        <option>Choose Location Type</option>
                        <option value="ghana-post-code">Ghana Post Code</option>
                        <option value="current-location">Pick Current Location</option>
{{--                        <option value="google-places">Google places</option>--}}
                        <option value="manual">Manually Type</option>
                    </select>
                </div>
            </div>

            <div class="form-group row" style="display: none;" id="manual-location">
                <label class="col-form-label col-sm-2 text-sm-right">Location</label>
                <div class="col-sm-10">
                    <select class="form-control" name="location_id" id="location">
                        <option>select location</option>
                    </select>
                </div>
            </div>

            <div class="form-group row" id="address_box" style="display: none;">
                <label class="col-form-label col-sm-2 text-sm-right">Address</label>
                <div class="col-sm-10">
                    <input type="text" name="address" onFocus="geolocate()" id="autocomplete" class="form-control" autocomplete="false" />
                </div>
            </div>

            <div class="form-group row ghana-post-row" style="display: none;">
                <label class="col-form-label col-sm-2 text-sm-right">Ghana Post Code</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="ghana_post_code"
                           id="ghana_post_code" placeholder="Your Ghana Post Code" onfocusout="translateCode()"/>
                </div>
            </div>

            <div class="row">
                <label class="col-form-label col-sm-2 text-sm-right">Location Details</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="street_name"
                           id="street_name" placeholder="Street name" readonly/>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="district"
                           id="district" placeholder="District" readonly/>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="region"
                           id="region" placeholder="Region" readonly/>
                </div>

{{--                <div class="col-md-2">--}}
{{--                    <input type="text" class="form-control" name="coordinates"--}}
{{--                           id="coordinates" placeholder="Coordinates" readonly/>--}}
{{--                </div>--}}

                {{--            <input type="hidden" name="coordinates"  id="coordinates"/>--}}

            </div>

            <div class="row">
                <p id="demo" style="margin-left: 20px;"></p>
            </div>

            <input type="hidden" name="address_line_1" id="address_line_1" />

            <input type="hidden" name="google_place_id" id="google_place_id" />

            <input type="hidden" name="place_identifier" id="place_details" />

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Ownership</label>
                <div class="col-sm-10">
                    <select class="form-control" name="ownership" id="ownership">
                        <option value="private">Private</option>
                        <option value="public">Public</option>
                    </select>
                </div>
            </div>

            <div id="shs-type">

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Gender</label>
                <div class="col-sm-10">
                    <select class="form-control" name="gender">
                        <option value="boys">Boys</option>
                        <option value="girls">Girls</option>
                        <option value="mixed">Mixed</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">School Status</label>
                <div class="col-sm-10">
                    <select class="form-control" name="boarding_status" required>
                        <option value="day">Day</option>
                        <option value="boarding">Boarding</option>
                        <option value="day-boarding">Day/Boarding</option>
                        <option value="day-hostel">Day/Hostel</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Programmes</label>

                <div class="programmes-column d-inline-flex" style="margin-left: 12px; margin-top: 10px;">
                    <div class="sm-2 margin-right-10 " >
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" name="agric"  class="custom-control-input">
                            <span class="custom-control-label">Agric</span>
                        </label>
                    </div>

                    <div class="sm-2 margin-right-10">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" name="business" class="custom-control-input">
                            <span class="custom-control-label">Business</span>
                        </label>
                    </div>

                    <div class="sm-2 margin-right-10">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" name="tech" class="custom-control-input">
                            <span class="custom-control-label">Technical</span>
                        </label>
                    </div>

                    <div class="sm-2 margin-right-10">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" name="visual_arts"  class="custom-control-input">
                            <span class="custom-control-label">Visual Arts</span>
                        </label>
                    </div>

                    <div class="sm-2 margin-right-10">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" name="gen_arts"  class="custom-control-input">
                            <span class="custom-control-label">General Arts</span>
                        </label>
                    </div>

                    <div class="sm-2 margin-right-10">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" name="gen_science" class="custom-control-input">
                            <span class="custom-control-label">General Science</span>
                        </label>
                    </div>
                </div>

                    {{--                <div class="sm-2">--}}
                    {{--                    <label class="custom-control custom-checkbox">--}}
                    {{--                        <input type="checkbox" name="status" value="1" @if(old('status')) checked @endif class="custom-control-input">--}}
                    {{--                        <span class="custom-control-label">Business</span>--}}
                    {{--                    </label>--}}
                    {{--                </div>--}}

                    {{--                <div class="col-sm-10">--}}
                    {{--                    <label class="custom-control custom-checkbox">--}}
                    {{--                        <input type="checkbox" name="status" value="1" @if(old('status')) checked @endif class="custom-control-input">--}}
                    {{--                        <span class="custom-control-label">Active</span>--}}
                    {{--                    </label>--}}
                    {{--                </div>--}}

                </div>

            </div>

                @role('admin|subadmin')


            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Theme</label>
                <div class="col-sm-10">
                    <select name="theme" class="custom-select" required>
                        <option value="">Select Theme</option>
                        @foreach(config('constants.themes') as $theme)
                            <option value="{{$theme}}" @if(old('theme') == $theme) selected @endif>{{ucwords($theme)}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="status" value="1" @if(old('status')) checked @endif class="custom-control-input">
                               <span class="custom-control-label">Active</span>
                    </label>
                </div>
            </div>

			<div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="featured" value="1" @if(old('featured')) checked @endif class="custom-control-input">
                               <span class="custom-control-label">Featured</span>
                    </label>
                </div>
            </div>

			<div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="restrict_to_student" value="1" class="custom-control-input" id="restrict_to_student">
                               <span class="custom-control-label">Restrict to limited students</span>
                    </label>
                </div>
            </div>

		<div class="form-group row student_limit" style="display:none;">
			<label class="col-form-label col-sm-2 text-sm-right">Student limit</label>
			<div class="col-sm-10">
			   <input type="number" name="student_limit" value="" class="form-control">
			</div>
		</div>

		@endrole

		<div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="is_locked" value="1" @if(old('is_locked')) checked @endif class="custom-control-input">
                               <span class="custom-control-label">Locked</span>
                    </label>
                </div>
        </div>

        <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="physically_challenged"
                               class="custom-control-input">
                        <span class="custom-control-label">Physically challenged</span>
                    </label>
                </div>
            </div>

		<div class="form-group row">

			<div class="col-sm-10 ml-sm-auto">
				<a href = "{{route('backend.schools')}}" class="btn btn-danger mr-2">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function () {

		$("#restrict_to_student").on("click", function () {
			if($(this). prop("checked") == true){
				$(".student_limit").show();
			} else {
				$(".student_limit").hide();
			}
		 });

});
</script>
@stop
