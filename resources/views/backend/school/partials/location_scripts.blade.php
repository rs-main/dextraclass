<script>

    var placeSearch, autocomplete;

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'));
        autocomplete.setFields(['address_component']);
        autocomplete.addListener('place_changed', populateCoordinates);
    }

    function fillInAddress() {
        let place = autocomplete.getPlace();
        console.log("Place" + JSON.stringify(place));

        for (let component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        for (let i = 0; i < place.address_components.length; i++) {
            let addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                let val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    function populateCoordinates() {

        let addressVal = $("#autocomplete").val();
        let key = "AIzaSyAXI-1JdxHZTy3nb700Mpp1X21pzqAmU_c";
        let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${addressVal}&key=${key}`;
        $.get( url, function( data ) {
            $("#lat").val(JSON.stringify(data.results[0]["geometry"]["location"]["lat"]));
            $("#lng").val(JSON.stringify(data.results[0]["geometry"]["location"]["lng"]));
            $("#coordinates").val(data.results[0]["geometry"]["location"]["lat"]+ ","+data.results[0]["geometry"]["location"]["lng"])
            // $("#place_id").val(JSON.stringify(data.results[0]["place_id"]));
            let place_details_one = (data.results[0]["address_components"][0]["long_name"]);
            let place_details_two = (data.results[0]["address_components"][1]["long_name"]);
            let place_details_three = typeof data.results[0]["address_components"][2] != "undefined" ? (data.results[0]["address_components"][2]["long_name"]) : "";
            let place_details_four = typeof data.results[0]["address_components"][3] != "undefined" ? (data.results[0]["address_components"][3]["long_name"]): "";
            let place_details_five = typeof data.results[0]["address_components"][4] != "undefined" ? (data.results[0]["address_components"][4]["long_name"]): "";

            $("#address_line_1").val(addressVal);

            $("#street_name").val(place_details_one)
            $("#district").val(place_details_two)

            if (place_details_three.endsWith("Region")){
                $("#region").val(place_details_three);
            }else if (place_details_four.endsWith("Region")){
                $("#region").val(place_details_four);
            }else if (place_details_five.endsWith("Region")){
                $("#region").val(place_details_five);
            }
        });
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: 7.9527706,
                    lng:  -1.0307118
                };
                var circle = new google.maps.Circle(
                    {center: geolocation, radius: position.coords.accuracy});
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    var x = document.getElementById("demo");

    function getLocation() {
        options = {
            enableHighAccuracy: false,
            timeout:            30000,  // milliseconds (30 seconds)
            maximumAge:         600000 // milliseconds (10 minutes)
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError, options);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        $("#coordinates").val( `${position.coords.latitude},${position.coords.longitude}`);
        $("#street_name").val("")
        $("#district").val("")
        $("#region").val("");
    }

    function showError(error) {
        let location_type = $("#location-type");
        switch(error.code) {
            case error.PERMISSION_DENIED:
                // x.innerHTML = "User denied the request for Geolocation."
                alert("You have denied the request to pick your location.");
                location_type.find('[value="current-location"]').remove();
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable!");
                location_type.find('[value="current-location"]').remove();
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out!");
                location_type.find('[value="current-location"]').remove();
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred!");
                location_type.find('[value="current-location"]').remove();
                break;
        }
    }


    $("#your_location_checkbox").click(function(){
        if(!($(this).is(":checked"))){
            console.log("Checkbox is unchecked." );
            $("#address_box").show();
        }else{
            $("#address_box").hide();
        }
    });


    let shs_type = $("#shs-type");

    shs_type.hide();

    $("#location").select2({
        ajax: ({
            url: "/locations",
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        })
    })

    $("#school_category").change(function (){
        var selectedCategory = $(this).children("option:selected").val();
        var selectedCategoryText =  $(this).children("option:selected").text();
        if (selectedCategoryText === "SHS"){
            shs_type.removeAttr("style");
            shs_type.show();
        }else{
            shs_type.hide();
        }
    });

    $("#location-type").change(function(){
        let selectedLocationType = $(this).children("option:selected").val();
        const ghana_post_row = $(".ghana-post-row");
        const manual_location = $("#manual-location");
        const address_box = $("#address_box");

        switch (selectedLocationType){
            case "ghana-post-code":
                ghana_post_row.show();
                manual_location.hide();
                address_box.hide();
                break;

            case "google-places":
                ghana_post_row.hide();
                manual_location.hide();
                address_box.hide();
                break;

            case "current-location":
                ghana_post_row.hide();
                manual_location.hide();
                address_box.hide();
                getLocation();
                break;

            case "manual":
                ghana_post_row.hide();
                address_box.show();
                break;
        }

    });

    function blockUi() {
        $.blockUI({
            message: '<div class="sk-folding-cube sk-primary"><div class="sk-cube1 sk-cube"></div>' +
                '<div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div>' +
                '<div class="sk-cube3 sk-cube"></div></div><h5 style="color: #444">Getting Location ...</h5>',
            css: {
                backgroundColor: 'transparent',
                border: '0',
                zIndex: 9999999
            },
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                zIndex: 9999990
            }
        });
    }

    function unblockUI() {
        $.unblockUI();
    }

    function translateCode(){
        const base_url = "http://locations.realstudiosonline.com/";
        let ghPostCode = $("#ghana_post_code").val();
        if (ghPostCode.length > 10){
            blockUi();
            fetch(`${base_url}?address=${ghPostCode}`)
                .then(response => response.json())
                .then(data => {
                    $("#street_name").val(data.streetName)
                    $("#district").val(data.district)
                    $("#region").val(data.region);
                    $("#coordinates").val(data.lngLat);
                    unblockUI();

                }).catch(function(error) {
                console.log(error);
            });
        }
    }

</script>
