<form action="{{route('backend.periods.update', $period->id)}}" method = "POST">
            @csrf
            @method('PUT')
			<input type="hidden" name="class" value="{{$period->class_id}}">
			<input type="hidden" name="ajax_request" value="1">
			<div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right">Period Title</label>
                <div class="col-sm-10">
                    <input type="text" name="title" placeholder="Period Title" class="form-control" value="{{$period->title}}" required>
                </div>
            </div>
           
            <div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="status" value="1" class="custom-control-input" @if($period->status) checked @endif>
                        <span class="custom-control-label">Active</span>
                    </label>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-10 ml-sm-auto">
                    <button data-dismiss="modal" class="btn btn-danger mr-2">Cancel</button> <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>