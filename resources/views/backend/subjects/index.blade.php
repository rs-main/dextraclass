@extends('backend.layouts.layout-2')

@section('scripts')

    <script type="text/javascript">
        $(function () {

            $("#school").on("change", function () {
                //var school_id = $(this).val();
                var school_id = $('#school option:selected').attr('data-id');

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.stdfiltercourses") }}',
                    data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#school_course").html(data);
                    }
                });

            });

            $("#school_course").on("change", function () {
                // var school_course = $('#school_course').val();
                var school_course = $('#school_course option:selected').attr('data-id');
                if(school_course) {
                    $.ajax({
                        type: "POST",
                        url: '{{ route("ajax.school.stdfiltercourseclasses") }}',
                        data: {'course_id' : school_course, '_token': '{{ csrf_token() }}'},
                        success: function (data) {
                            $("#class").html(data);
                        }
                    });
                }
            });

            var table = $('#subject-list').DataTable({
                "columns": [
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    null,
                    { "orderable": false }
                ],
                dom: 'lrtip'
            });

            $('#subject_name').on('keyup', function(){
                //alert('gdfgfd');
                regExSearch = this.value;
                table.column(0).search(regExSearch, true, false).draw();
                // table.search(this.value, true, false).draw();
            });

            $('#school').on('change', function(){
                //alert('gdfgfd');
                regExSearch = this.value +'\\s*$';
                table.column(1).search(regExSearch, true, false).draw();
                //table.search(this.value, true, false).draw();
            });

            $('#school_course').on('change', function(){
                //alert('gdfgfd');
                regExSearch = this.value +'\\s*$';
                table.column(2).search(regExSearch, true, false).draw();
                //table.search(this.value, true, false).draw();
            });

            $('#class').on('change', function(){
                //alert('gdfgfd');
                regExSearch = this.value +'\\s*$';
                table.column(3).search(regExSearch, true, false).draw();
                table.search(this.value, true, false).draw();
            });


        });
    </script>
@endsection

@section('content')
    @includeif('backend.message')
    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Subjects</div>
        <a href="{{route('backend.subjects.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Subject</a>
    </h4>

    <div class="card">

        <div class="card-datatable table-responsive">
            <table id="subject-list" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th style="min-width: 14rem" class="align-top">
                        Subject Name
                        <input type="text" name="subject_name" id="subject_name" class="form-control">
                    </th>
                    <th style="min-width: 14rem" class="align-top">
                        School Name
                        <select name="school" id="school" class="custom-select" required>
                            <option value="" selected="">All</option>
                            @foreach($schools as $id => $type)
                                <option value="{{$type}}" data-id="{{$id}}">{{$type}}</option>
                            @endforeach
                        </select>
                    </th>
                    <th style="min-width: 7rem" class="align-top">
                        Course
                        <select name="course" id="school_course" class="custom-select" required>
                            <option value="" selected="">All</option>
                        </select>
                    </th>
                    <th style="min-width: 10rem" class="align-top">
                        Class
                        <select name="class" id="class" class="custom-select" required>
                            <option value="" selected="">All</option>
                        </select>
                    </th>
                    <th class="align-top">Status</th>
                    <th style="min-width: 9rem" class="align-top">Action</th>
                </tr>
                </thead>
                <tbody>
                @php $i=0; @endphp
                @foreach($subjects as $subject)
                    <tr>
                        <td>{{$subject->subject_name}}</td>
                        <td>{{$subject->school_details($subject->course_details($subject->subject_class->course_id)->school_id)}}</td>
                        <td>{{$subject->course_details($subject->subject_class->course_id)->name}}</td>
                        <td>@if(isset($subject->subject_class->class_name) && !empty(($subject->subject_class->class_name))){{$subject->subject_class->class_name}}@endif</td>
                        <td class='text-align align-middle '><button class="btn {{$subject->status ? 'btn-success':'btn-danger'}}">{{$subject->status ? 'Active':'Disabled'}}</button></td>
                        <td class='form-inline'>
                            <a href ="{{route('backend.subjects.edit', $subject->id)}}" class="btn-primary  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5 px;padding-top: 4px; padding-bottom: 4px;" title="Edit"><i class="ion ion-md-create"></i></a>
                            @role('admin')
                            <form method="POST" action="{{route('backend.subjects.destroy', $subject->id)}}" style="display: inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5 px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

                            </form>
                            @endrole
                            <a href ="{{route('backend.subjects.show', $subject->id)}}" class="btn-warning btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5 px;padding-top: 4px; padding-bottom: 4px;" title="View subject details"><i class="ion ion-md-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
