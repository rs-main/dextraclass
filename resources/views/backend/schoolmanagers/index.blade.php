@extends('backend.layouts.layout-2')

@section('scripts')

<script type="text/javascript">
    $(function () {
        $('#schoolmanager-list').dataTable(
			{
            "columns": [
              null,
              { "orderable": false },
			  { "orderable": false },
			  { "orderable": false },
			  null,

              { "orderable": false }
            ],
			 initComplete: function () {
            this.api().columns([3]).every( function () {
                var column = this;
                var select = $('<select class="custom-select"><option value="">All</option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
          }
		);



    });
</script>
@endsection

@section('content')

@includeif('backend.message')

<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>School Managers</div>
    <a href="{{route('backend.managers.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create School Manager</a>
</h4>

<div class="card">
    <div class="card-datatable table-responsive">
	<table id="schoolmanager-list" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th class="align-top">Name</th>
                    <th class="align-top">Username</th>
					<th class="align-top">Mobile</th>
					<th style="min-width: 7rem" class="align-top">School</th>
					<th class="align-top">Status</th>
                    <th class="align-top">Action</th>
                </tr>
            </thead>
            <tbody>
                @php $i=0; @endphp
                @foreach($schoolmanagers as $schoolmanager)
                <tr>
                    <td>{{$schoolmanager->getFullNameAttribute()}}</td>
                    <td>{{$schoolmanager->user_details->username}}</td>
					<td>{{$schoolmanager->mobile}}</td>
					<td>{{$schoolmanager->school->school_name}}</td>
                    <td class='text-align align-middle '><button class="btn {{$schoolmanager->status ? 'btn-success':'btn-danger'}}">{{$schoolmanager->status ? 'Active':'Disabled'}}</button></td>

                    <td>
                       <a href ="{{route('backend.managers.edit', $schoolmanager->id)}}" class="btn btn-primary btn-xs icon-btn md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>
						@role('admin')
                        <form method="POST" action="{{route('backend.managers.destroy', $schoolmanager->id)}}" style="display: inline-block;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn btn-danger btn-xs icon-btn md-btn-flat article-tooltip" title="Remove"><i class="ion ion-md-close"></i></button>

                        </form>
						@endrole
						 <a href ="{{route('backend.managers.show', $schoolmanager->id)}}" class="btn btn-warning btn-xs icon-btn md-btn-flat article-tooltip" title="View manager details"><i class="ion ion-md-eye"></i></a>

                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>
@endsection
