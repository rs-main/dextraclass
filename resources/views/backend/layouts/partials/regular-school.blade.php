<li class="sidenav-item">
    <a href="javascript:void(0)" class="sidenav-link sidenav-toggle">
        <i class="sidenav-icon ion ion-ios-school"></i>
        <div>Regular School</div>
    </a>

    <ul class="sidenav-menu">

        <li class="sidenav-item{{ (strpos($routeName, 'backend.regular.videos') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.regular.videos') }}" class="sidenav-link"><i class="sidenav-icon fas fa-video"></i><div>Videos</div></a>
        </li>

        <li class="sidenav-item{{ ((strpos($routeName, 'backend.courses') !== false) || (strpos($routeName, 'backend.course') !== false)) ? ' active' : '' }}">
            <a href="{{ route('backend.courses') }}" class="sidenav-link"><div><i class="sidenav-icon fas fa-book-reader"></i>Courses</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.classes') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.classes.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-chalkboard"></i><div>Classes</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.periods') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.periods.index') }}" class="sidenav-link"><i class="sidenav-icon fas Search Results
                                                                                   Web results fas fa-clock"></i><div>Periods</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.subjects') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.subjects.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-book"></i><div>Subjects</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.topics') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.topics.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-tags"></i><div>Topics</div></a>

        </li>

    </ul>
</li>
