<li class="sidenav-item">
    <a href="javascript:void(0)" class="sidenav-link sidenav-toggle">
{{--        <i class="sidenav-icon ion ion-ios-course"></i>--}}
        <i class="sidenav-icon fas fa-book-open"></i>
        <div>Professional</div>
    </a>

    <ul class="sidenav-menu">

        <li class="sidenav-item{{ (strpos($routeName, 'backend.industries.index') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.industries.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-industry"></i><div>Industry</div></a>
        </li>

        <li class="sidenav-item {{ (strpos($routeName, 'backend.professional.categories.index') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.professional.categories.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-layer-group"></i><div>Categories</div></a>
        </li>

        <li class="sidenav-item {{strpos($routeName,'backend.professional.courses.index') !== false ? ' active' : ''}}">
            <a href="{{ route('backend.professional.courses.index') }}" class="sidenav-link"><div><i class="sidenav-icon fas fa-book-reader"></i>Courses</div></a>
        </li>

        <li class="sidenav-item  {{strpos($routeName,'backend.professional.videos.index') !== false ? ' active' : ''}}">
            <a href="{{ route('backend.professional.videos.index') }}"
               class="sidenav-link"><i class="sidenav-icon fas fa-video"></i><div>Videos</div></a>
        </li>

    </ul>
</li>
