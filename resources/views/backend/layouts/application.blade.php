<!DOCTYPE html>

<html lang="en" class="default-style">
    <head>
        <title>XtraClass Admin Panel</title>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}">

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

        <!-- Icon fonts -->
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/fontawesome.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/ionicons.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/linearicons.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/open-iconic.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/pe-icon-7-stroke.css') }}">

        <!-- Core stylesheets -->
        <link rel="stylesheet" href="{{ mix('/assets/vendor/css/rtl/bootstrap.css') }}" class="theme-settings-bootstrap-css">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/css/rtl/appwork.css') }}" class="theme-settings-appwork-css">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/css/rtl/theme-corporate.css') }}" class="theme-settings-theme-css">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/css/rtl/colors.css') }}" class="theme-settings-colors-css">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/css/rtl/uikit.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/css/demo.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/libs/quill/typography.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/libs/quill/editor.css') }}">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.7/jquery.jgrowl.min.css">
        <!-- Libs -->
        <link rel="stylesheet" href="{{ mix('/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
        <link href="{{ asset('css/backend.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <link rel="stylesheet" href="{{asset("assets/vendor/libs/bootstrap-sortable/bootstrap-sortable.css")}}">

        <link rel="stylesheet" href="{{asset("assets/vendor/libs/bootstrap-select/bootstrap-select.css")}}">
        <link rel="stylesheet" href="{{asset("assets/vendor/libs/select2/select2.css")}}">

        <!-- Load polyfills -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="{{ mix('/assets/vendor/js/polyfills.js') }}"></script>
        <script>document['documentMode'] === 10 && document.write('<script src="https://polyfill.io/v3/polyfill.min.js?features=Intl.~locale.en"><\/script>')</script>

        <link rel="stylesheet" href="{{asset("assets/vendor/libs/bootstrap-markdown/bootstrap-markdown.css")}}">
        <script src="{{ mix('/assets/vendor/js/material-ripple.js') }}"></script>
        <script src="{{ mix('/assets/vendor/js/layout-helpers.js') }}"></script>
        <script src="{{ mix('/assets/vendor/libs/quill/quill.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <script src="{{ asset('js/common_new.js') }}"></script>

        <!--EXTERNAL CSS-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/monokai-sublime.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.7.1/katex.min.css">
        <!--<link rel="stylesheet" href="https://cdn.quilljs.com/1.3.6/quill.bubble.css">-->
        <!--<link rel="stylesheet" href="https://cdn.quilljs.com/1.3.6/quill.snow.css">-->



        <!--EXTERNAL JAVASCRIPT
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
        -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.7.1/katex.min.js"></script>

        <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/0.11.1/typeahead.bundle.min.js" type="text/javascript"></script>

        <!-- Theme settings -->
        <!-- This file MUST be included after core stylesheets and layout-helpers.js in the <head> section -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-markdown/2.10.0/css/bootstrap-markdown.min.css" integrity="sha512-CUHPfvm73R8lgOyl0oyhnGfM5RlGV7yRyj98ZeX3/cBsLLuSVRaqKHhahj6B04H3Q63jZcQHHZZXIonj03eUzQ==" crossorigin="anonymous" />
        <script src="{{ mix('/assets/vendor/js/theme-settings.js') }}"></script>
        <script src="{{mix("assets/vendor/libs/markdown/markdown.js")}}"></script>
        <script src="{{mix("assets/vendor/libs/bootstrap-markdown/bootstrap-markdown.js")}}"></script>

        <script>
            import {ThemeSettings} from "../../../assets/vendor/js/theme-settings";

            {{--window.themeSettings = new ThemeSettings({--}}
            {{--        cssPath: '',--}}
            {{--        themesPath: '',--}}
            {{--        pathResolver: function(path) {--}}
            {{--            var resolvedPaths = {--}}
            {{--            // Core stylesheets--}}
            {{--            //--}}
            {{--                @foreach (['bootstrap', 'appwork', 'colors'] as $name)--}}
            {{--                '{{ $name }}.css': '{{ mix("/assets/vendor/css/rtl/{$name}.css") }}',--}}
            {{--                '{{ $name }}-material.css': '{{ mix("/assets/vendor/css/rtl/{$name}-material.css") }}',--}}
            {{--                @endforeach--}}

            {{--                // UI Kit--}}
            {{--                'uikit.css': '{{ mix("/assets/vendor/css/rtl/uikit.css") }}',--}}
            {{--                // Themes--}}
            {{--                //--}}
            {{--                @foreach (config('constants.themes') as $name)--}}
            {{--                'theme-{{ $name }}.css': '{{ mix("/assets/vendor/css/rtl/theme-{$name}.css") }}',--}}
            {{--                'theme-{{ $name }}-material.css': '{{ mix("/assets/vendor/css/rtl/theme-{$name}-material.css") }}',--}}
            {{--                @endforeach--}}
            {{--            }--}}

            {{--            return resolvedPaths[path] || path;--}}
            {{--        }--}}
            {{--});--}}

            window.themeSettings.setTheme("{{Auth::user()->getSchoolTheme()}}",true);

        </script>

        <!-- Core scripts -->
        <script src="{{ mix('/assets/vendor/js/pace.js') }}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.2.3/flatpickr.css">

        <style>
            .theme-settings-open-btn{display: none !important;}
        </style>

        @yield('styles')
    </head>
    <body>
        <div class="page-loader"><div class="bg-primary"></div></div>

        @yield('layout-content')

        <!-- Core scripts -->
        <script src="{{ mix('/assets/vendor/libs/popper/popper.js') }}"></script>
        <script src="{{ mix('/assets/vendor/js/bootstrap.js') }}"></script>
        <script src="{{ mix('/assets/vendor/js/sidenav.js') }}"></script>

        <!-- Scripts -->
        <script src="{{ mix('/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
        <script src="{{ mix('/assets/vendor/libs/growl/growl.js') }}"></script>
        <script src="{{ mix('/assets/js/demo.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script src="{{asset("assets/vendor/libs/bootstrap-sortable/bootstrap-sortable.js")}}"></script>

        <script src="{{asset("assets/vendor/libs/bootstrap-select/bootstrap-select.js")}}"></script>
        <script src="{{asset("assets/vendor/libs/select2/select2.js")}}"></script>
        <script src="{{asset("assets/vendor/libs/bootstrap-tagsinput/bootstrap-tagsinput.js")}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/markdown.js/0.5.0/markdown.min.js" integrity="sha512-kaDP6dcDG3+X87m9SnhyJzwBMKrYnd2tLJjGdBrZ9yEL8Zcl2iJRsPwylLkbd2g3QC5S8efV3sgwI5r73U0HnA==" crossorigin="anonymous"></script>
        <script src="//js.pusher.com/3.1/pusher.min.js"></script>
        <script>
            const pusher = new Pusher('a7add5dfd9b9d8d52636', {
                encrypted: true, cluster: 'eu'
            });

            const channel = pusher.subscribe('streaming-channel');

        </script>

        <script>
            // Disable search and ordering by default
            $.extend( $.fn.dataTable.defaults, {
                lengthMenu: [ [10, 25, 50, 100], [10, 25, 50, 100] ],
                pageLength: {{\App\Helpers\SiteHelpers::pageLimit()}}
            } );

            // if(_this[0].baseURI === window.location.href && _this.hasClass("open")){
            //     _this.addClass("active");
            // }

            console.log("/////////////////////////////////////////////////")
            let side_nav_item = $(".sidenav-item");

            $.each(side_nav_item, function( index, value ) {
                // console.log(value.parentElement);
                console.log("nav")
                console.log(value)
            });

             side_nav_item.on("click", function(){
                let _this = $(this);
                if ($(this).hasClass("open")){
                    _this.removeClass("open")
                }else{
                    _this.addClass("open");
                }

                if (_this[0].baseURI === window.location.href && _this.hasClass("open")){
                    _this.addClass("active");
                }else {
                    _this.removeClass("active");
                }

                console.log(_this);
                console.log(_this[0].baseURI);
            })


        </script>

        <script>

        </script>


        @yield('scripts')
    </body>
</html>
