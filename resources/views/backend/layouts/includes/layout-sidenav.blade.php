@php
$routeName = Route::currentRouteName();
@endphp

<!-- Layout sidenav -->
<div id="layout-sidenav" class="{{ isset($layout_sidenav_horizontal) ? 'layout-sidenav-horizontal sidenav-horizontal container-p-x flex-grow-0' : 'layout-sidenav sidenav-vertical' }} sidenav bg-sidenav-theme">
    @empty($layout_sidenav_horizontal)
    <!-- Brand demo (see assets/css/demo/demo.css) -->
    <div class="app-brand demo">
        <span class="app-brand-logo demo bg-primary">
            <img src="{{asset('images/xt_white.png')}}" width="24" />
        </span>
        <a href="/admin" class="app-brand-text demo sidenav-text font-weight-normal ml-2">{{ config('app.name', 'XtraClass')}}</a>
        <a href="javascript:void(0)" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
            <i class="ion ion-md-menu align-middle"></i>
        </a>
    </div>

    <div class="sidenav-divider mt-0"></div>
    @endempty

    <!-- Links -->
{{--    <ul class="sidenav-inner{{ empty($layout_sidenav_horizontal) ? ' py-1' : '' }}">--}}
    <ul class="sidenav-inner py-1">
        <!-- Dashboards -->
        <li class="sidenav-item{{ strpos($routeName, 'backend.dashboard') !== false ? ' active' : '' }}">
            <a href="{{ route('backend.dashboard') }}" class="sidenav-link"><i class="sidenav-icon ion ion-md-speedometer"></i><div>Dashboard</div></a>
        </li>

        @role('admin|subadmin')
        <li class="sidenav-item{{ (strpos($routeName, 'backend.categories') !== false) ? ' active' : '' }}">
            <a href="{{ route('backend.categories.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-list"></i><div>Institutions</div></a>
        </li>

        <li class="sidenav-item{{ ((strpos($routeName, 'backend.schools') !== false) || (strpos($routeName, 'backend.school') !== false)) ? ' active' : '' }}">
            <a href="{{ route('backend.schools') }}" class="sidenav-link"><i class="sidenav-icon fas fa-school"></i><div>Schools</div></a>
        </li>


        <li class="sidenav-item{{ (strpos($routeName, 'backend.departments') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.departments.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-university"></i><div>Departments</div></a>
        </li>

        @endrole

        <li class="sidenav-item{{ (strpos($routeName, 'backend.students') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.students.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-user-graduate"></i><div>Students</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.tutors') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.tutors.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-chalkboard-teacher"></i><div>Tutors</div></a>
        </li>
        @role('admin|subadmin')
        <li class="sidenav-item{{ (strpos($routeName, 'backend.managers') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.managers.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-user-tie"></i><div>School Managers</div></a>
        </li>
        @endrole



        @role('admin|subadmin')

        @include("backend.layouts.partials.regular-school")

        @include("backend.layouts.partials.open-school")

        @include("backend.layouts.partials.professional")
        @endrole

{{--        <li class="sidenav-item{{ (strpos($routeName, 'backend.settings') !== false)  ? ' active' : '' }}">--}}
{{--            <a href="{{ route('backend.time-tables.index') }}" class="sidenav-link">--}}
{{--                <i class="sidenav-icon fas fa-calendar-times"></i>--}}
{{--                <div>Time Tables</div></a>--}}
{{--        </li>--}}

	@role('admin')
		<li class="sidenav-item{{ (strpos($routeName, 'backend.settings') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.settings.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-cog"></i><div>Settings</div></a>
        </li>

    @endrole

        <li class="sidenav-item{{ (strpos($routeName, 'backend.questions') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.questions.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-clipboard-list"></i><div>Questions</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.past-questions.index') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.past-questions.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-clipboard-check"></i><div>Past Questions</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.quiz') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.quiz.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-clipboard"></i><div>Quizzes</div></a>
        </li>

        <li class="sidenav-item{{ (strpos($routeName, 'backend.streams.index') !== false)  ? ' active' : '' }}">
            <a href="{{ route('backend.streams.index') }}" class="sidenav-link"><i class="sidenav-icon fas fa-stream"></i><div>Streams</div></a>
        </li>

    </ul>
</div>
<!-- / Layout sidenav -->
