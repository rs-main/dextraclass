@extends('backend.layouts.layout-2')

@section("styles")
    <link rel="stylesheet" href="{{asset("assets/vendor/libs/datatables/datatables.css")}}">
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="{{asset("assets/vendor/libs/datatables/datatables.js")}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


    <script type="text/javascript">
        $(function () {

            const table = $('#streams-data').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('backend.streams.data') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'tutor', name: 'tutor'},
                    {data: 'class', name: 'class'},
                    {data: 'subject', name: 'subject'},
                    {data: 'channel_id', name: 'channel_id'},
                    {data: 'total_students', name: 'total_students'},
                    {data: 'active', name: 'active'},
                    {data: 'recorded', name: 'recorded'},
                    {data: 'started_at', name: 'started_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            const channel = pusher.subscribe('streaming-channel');

            channel.bind('streaming-event', function(data) {
                table.draw();
            });
        });

    </script>
@endsection

@section('content')
    @includeif('backend.message')

    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>All Streams</div>
{{--        <a href="{{route('backend.quiz.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Quiz</a>--}}
    </h4>

    <div id="errorDiv">
        <!-- error will be shown here -->
    </div>

    <div class="card">
        <h6 class="card-header">
            All Streams
        </h6>
        <div class="card-datatable table-responsive">
            <table id="streams-data" class="datatables-demo table table-striped table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Tutor</th>
                    <th>Class</th>
                    <th>Subject</th>
                    <th>Channel</th>
                    <th>Students online</th>
                    <th>On-Going</th>
                    <th>Recorded</th>
                    <th>Started at</th>
                    <th width="100px">Action</th>
                </tr>
                </thead>
              <tbody></tbody>
            </table>
        </div>
    </div>


{{--    <div class="card">--}}
{{--        <div class="card-datatable table-responsive">--}}
{{--            <table id="streams-data" class="table table-bordered data-table">--}}
{{--                <thead>--}}
{{--                <tr>--}}
{{--                    <th>No.</th>--}}
{{--                    <th>Tutor</th>--}}
{{--                    <th>Class</th>--}}
{{--                    <th>Subject</th>--}}
{{--                    <th>Channel</th>--}}
{{--                    <th>Students online</th>--}}
{{--                    <th>Active</th>--}}
{{--                    <th>Recorded</th>--}}
{{--                    <th>Started at</th>--}}
{{--                    <th width="100px">Action</th>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody>--}}
{{--                </tbody>--}}
{{--            </table>--}}
{{--        </div>--}}
{{--    </div>--}}

@endsection
