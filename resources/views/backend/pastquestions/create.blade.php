@extends('backend.layouts.layout-2')

@section("styles")

{{--    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.css"
          integrity="sha512-pDpLmYKym2pnF0DNYDKxRnOk1wkM9fISpSOjt8kWFKQeDmBTjSnBZhTd41tXwh8+bRMoSaFsRnznZUiH9i3pxA=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

@endsection

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Past Questions Bank /</span> Create Past Question
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Create Question
        </h6>
        <div class="card-body">
            @includeif('backend.message')
            <form method = "post" id="createQuestion">
                @csrf
                <div id="errorDiv">
                    <!-- error will be shown here -->
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Class</label>
                    <div class="col-sm-10">
                        <select name="subject_name" id="subject_name" class="custom-select q_input" required>
                            @php
                            $classes = \App\Models\SchoolCategory::whereIn("id",["20","21"])->selectRaw("name,id")->get();
                            @endphp
                            @foreach($classes as $class)
{{--                            <option value="" selected="" disabled="" class="d-none">Select Class</option>--}}
                            <option value="{{$class->id}}">{{$class->name}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="subject_error">
                            Select class
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Subject</label>
                    <div class="col-sm-10">
                    <select class="subject-list" name="subject">
                        @foreach($subjects as $subject)
                          <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                        @endforeach
                    </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Year</label>
                    <div class="col-sm-10">

                        <select name="subject_name" id="subject_name" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Select Year</option>
                        @foreach($past_question_groups as $group)
                            <option value="{{$group->id}}">{{$group->group_name}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="subject_error">
                            Select year
                        </div>
                    </div>
                </div>`

                <div class="form-group row">
                    <label for="question" class="col-form-label col-sm-2 text-sm-right">Question</label>
                    <div class="col-sm-10">
                        <textarea class="form-control q_input summernote" placeholder="Enter question" required></textarea>
                        <div class="error" id="question_error">
                            Enter question
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Answer Type</label>
                    <div class="col-sm-10">
                        <select name="type" id="type" class="custom-select q_input" required>
                            <option value="" selected disabled>Select Answer Type</option>
                            <option value="multi-choice" >Multiple Choice</option>
                            <option value="true-false" >True or False</option>
                        </select>
                        <div class="error" id="type_error">
                            Select answer type
                        </div>
                    </div>
                </div>

                <div id="multi-choice" style="display: none">
                    <div class="row">
                        <label class="col-form-label col-sm-2 text-sm-right"></label>
                        <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
                    </div>

                    <div class="option" id="option">
                        <div class="form-group row ">
                            <label for="answer" class="col-form-label col-sm-2 text-sm-right">Option 1</label>
                            <div class="col-sm-10">
                                <input type="text" placeholder="Option 1" class="form-control"
                                       name="answer1" id="answer1">
                                <div class="error" id="error">
                                    Enter first option
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="answer1" class="col-form-label col-sm-2 text-sm-right"></label>
                            <div class="col-sm-10">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" name="checkAnswer" value="1" id="checkAnswer1" onClick="ckChange(this)" class="custom-control-input">
                                    <span class="custom-control-label">Check if this is the valid answer</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div style="margin: auto; width: 150px">
                        <button type="button" id="add-new-option" class="btn btn-success">Add new option</button>
                    </div>

                </div>

                <div id="true-false" style="display: none">
                    <div class="row">
                        <label class="col-form-label col-sm-2 text-sm-right"></label>
                        <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Options</label>

                        <div class="custom-controls-stacked col-sm-10">
                            <label class="custom-control custom-radio">
                                <input name="true-false-answer" type="radio" value="1" class="custom-control-input" checked>
                                <span class="custom-control-label">Select if answer is <strong>TRUE</strong></span>
                            </label>
                            <label class="custom-control custom-radio">
                                <input name="true-false-answer" type="radio" value="2" class="custom-control-input">
                                <span class="custom-control-label">Select if answer is <strong>FALSE</strong></span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Status</label>
                    <div class="col-sm-10">
                        <label class="switcher switcher-lg switcher-success">
                            <input type="checkbox" name="status" value="1" class="switcher-input" checked="">
                            <span class="switcher-indicator">
                            <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                            <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                        </span>
                        </label>
                    </div>
                </div>

                <div class="form-group row" id="year-row" style="display: none">
                    <label for="year" class="col-form-label col-sm-2 text-sm-right">Year</label>
                    <div class="col-sm-10">
                        <input type="number" name="year" id="year" min="1900" max="2099" step="1" class="form-control" placeholder="2016"  />
                        <div class="error" id="year_error">
                            Year
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <a href = "{{route('backend.questions.index')}}" class="btn btn-danger mr-2">Cancel</a>
                        <button type="button" class="btn btn-primary" id="submitQuestion">Submit</button>
                    </div>
                </div>

            </form>

        </div>

    </div>
@endsection
@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote.min.js" integrity="sha512-kZv5Zq4Cj/9aTpjyYFrt7CmyTUlvBday8NGjD9MxJyOY/f2UfRYluKsFzek26XWQaiAp7SZ0ekE7ooL9IYMM2A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <style>
        .error{
            color: #ff0000;
            display: none;
        }
    </style>
    <script>
        var option = '';
        var category_id = "{{old('institute_type')}}";
        var school_id = "{{old('school_name')}}";
        var course_id = "{{old('course_name')}}";
        var class_id = "{{old('class_name')}}";
        var subject_id = "{{old('subject_name')}}";

        $(document).ready(function() {
            $('.summernote').summernote({
                height: 150
            });

            // $('.summernote').summernote("code","<font color=\"#000000\"><span style=\"background-color: rgb(255, 255, 0);\">njshcdcjnj njnvjnvnnvjnfjn jfn vkfnvkfkmvkkfmvkk kfkfkvnkfvnjfvjfnvjfjnjvjnvjfnvnjf</span></font>");

        });
    </script>
    <script>

        let image_file = "";

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploaded_image')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }

            console.log(input.files);

            image_file = input.files[0];
        }

        $(document).ready(function () {

            $("#institute_type").on("change", function () {
                var category_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#institution_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.category.schools") }}',
                    data: {'category': category_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#school_name").html(data.schools);
                        if(school_id){
                            alert(1)
                            $("#school_name").val(school_id).trigger('change');
                        }
                    }
                });
            });

            $("#school_name").on("change", function () {
                var school_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#school_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courses") }}',
                    data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#course_name").html(data);
                        if(course_id){
                            $("#course_name").val(course_id).trigger('change');
                        }
                    }
                });
            });

            $("#course_name").on("change", function () {
                var course_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#course_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courseclasses") }}',
                    data: {'course_id': course_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#class_name").html(data);
                        if(class_id){
                            $("#class_name").val(class_id).trigger('change');
                        }
                    }
                });
            });

            $("#class_name").on("change", function () {
                var class_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#class_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.classsubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#subject_name").html(data);
                        if(subject_id){
                            $("#subject_name").val(subject_id).trigger('change');
                        }
                    }
                });
            });

            $('#subject_name').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();
            })

            if(category_id){
                $("#institute_type").val(category_id).trigger('change');
            }

            $('#type').on('change',function () {

                var type = $(this).val();
                $(this).removeClass('is-invalid');
                $('#type_error').fadeOut();
                if(type == 'multi-choice'){
                    $('#true-false').fadeOut(500, function () {
                        $('#multi-choice').fadeIn(500);
                    })
                }
                else{
                    $('#multi-choice').fadeOut(500, function () {
                        $('#true-false').fadeIn(500);
                    })
                }
            })

            $('#question').on('keyup', function () {
                $('#question_error').fadeOut();
                $(this).removeClass('is-invalid');
            })

            $('#answer1').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#1_error').fadeOut();
            })

            $('#answer2').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#2_error').fadeOut();
            })

            $('#answer3').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#3_error').fadeOut();
            })

            $('#answer4').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#4_error').fadeOut();
            })

            $('input[type=checkbox]').on('change', function () {
                if($(this).prop('checked') == true){
                    option = $(this).val();
                }
                else{
                    option = '';
                }
                $('input[type=checkbox]').removeClass('is-invalid');
            });


        });
    </script>
    <script>
        function ckChange(ckType){
            var ckName = document.getElementsByName(ckType.name);
            var checked = document.getElementById(ckType.id);

            if (checked.checked) {

                for(var i=0; i < ckName.length; i++){

                    if(!ckName[i].checked){
                        ckName[i].disabled = true;
                    }else{
                        ckName[i].disabled = false;
                    }
                }
            }
            else {
                for(var i=0; i < ckName.length; i++){
                    ckName[i].disabled = false;
                }
            }
        }

        $("#question_type").on("change",function(){
            if($(this).val() === "image-as-question"){
                $("#question_image_container").show();
                $('#question_image_container').attr('required',true);
                // $('#past-question-type').val("past_question")
            }else {
                $("#question_image_container").hide();
                $("question_image").removeAttr('required');
            }
        });

        let i = 1;
        let options_count = 0;
        let num_of_options = 0;
        let all_options = [];

        $("#add-new-option").on("click",function (){
            ++i;
            let last_option = $(".option:last")
            last_option.clone().appendTo( last_option );
            let last_option_label = $(".option:last > div > label:first");
            last_option_label.text(`Option ${i}`);
            console.log("///////////////");
            let input = $("#option > div:nth-child(1) > div > input")[i-1]
            let check_input = $("#option > div:nth-child(2) > div > label > input")[i-1]
            input['placeholder']=`Option ${i}` ;
            input['name']=`answer${i}`;
            console.log(input['value']);
            input['id']=`answer${i}`;
            check_input['id'] = `checkAnswer${i}`;
            num_of_options = i;
        });


        $('#submitQuestion').on('click', function () {

            for (i = 1; i < num_of_options; i++){
                all_options[i-1]= $("#option > div:nth-child(1) > div > input")[i-1]['value']
            }

            console.log("////// options////")
            console.log(all_options)

            var institution = $('#institute_type').val();
            var school = $('#school_name').val();
            var course = $('#course_name').val();
            var class_name = $('#class_name').val();
            var subject = $('#subject_name').val();
            var question = $('#question').val();
            var question_type = $('#type').val();

            var past_question_type = $('#past-question-type').val();
            var year          = $('#year').val();

            $('.q_input').removeClass('is-invalid');
            $('.error').fadeOut();

            var error = false;
            if(!institution){
                $('#institution_error').fadeIn(500);
                $('#institute_type').addClass('is-invalid');
                error = true;
            }
            if(!school){
                $('#school_error').fadeIn(500);
                $('#school_name').addClass('is-invalid');
                error = true;
            }
            if(!course){
                $('#course_error').fadeIn(500);
                $('#course_name').addClass('is-invalid');
                error = true;
            }
            if(!class_name){
                $('#class_error').fadeIn(500);
                $('#class_name').addClass('is-invalid');
                error = true;
            }
            if(!subject){
                $('#subject_error').fadeIn(500);
                $('#subject_name').addClass('is-invalid');
                error = true;
            }
            if(!question || question.trim() == ''){
                $('#question_error').fadeIn(500);
                $('#question').addClass('is-invalid');
                error = true;
            }

            if(!question_type){
                $('#type_error').fadeIn(500);
                $('#type').addClass('is-invalid');
                error = true;
            }

            if(question_type == 'multi-choice'){
                var option1 = $('#answer1').val(),
                    option2 = $('#answer2').val();
                var option3 = $('#answer3').val(),
                    option4 = $('#answer4').val();
                // num_of_options = 4;

                // if(!option1 || option1.trim() == ''){
                //     $('#1_error').fadeIn(500);
                //     $('#answer1').addClass('is-invalid');
                //     error = true;
                // }
                // if(!option2 || option2.trim() == ''){
                //     $('#2_error').fadeIn(500);
                //     $('#answer2').addClass('is-invalid');
                //     error = true;
                // }
                // if(!option3 || option3.trim() == ''){
                //     $('#3_error').fadeIn(500);
                //     $('#answer3').addClass('is-invalid');
                //     error = true;
                // }
                // if(!option4 || option4.trim() == ''){
                //     $('#4_error').fadeIn(500);
                //     $('#answer4').addClass('is-invalid');
                //     error = true;
                // }

                var err = false;
                $('input[name=checkAnswer]').each(function () {
                    var checked = $(this).prop('checked');
                    if(!checked){
                        err = true
                    }
                    else {
                        err = false
                        return false;
                    }
                })
                if(err == true){
                    $('input[name=checkAnswer]').addClass('is-invalid');
                    error = true;
                }
            }
            else{
                option = $('input[name=true-false-answer]:checked').val();
                num_of_options = 2;
            }

            if(!error){
                var options = [];
                if(question_type == 'multi-choice'){
                    // options = [option1, option2, option3, option4]
                    options = all_options;
                }
                else {
                    options = ['True', 'False']
                }
                var answer = option;

                var formData = new FormData()
                formData.append('type', question_type)
                formData.append('question', question)
                formData.append('num_of_options', num_of_options)
                formData.append('active_status', 1)
                formData.append('school_id', school)
                formData.append('course_id', course)
                formData.append('class_id', class_name)
                formData.append('subject_id', subject)
                formData.append('options', options)
                formData.append('past_question_type', past_question_type)
                formData.append('answer', answer)
                formData.append('year', year)
                formData.append('image_question', image_file)

                console.log(formData);
                var errorDiv = $('#errorDiv'), btn = $('#submitQuestion'), btnHtml = 'Submit';
                $.ajax({
                    url: '{{URL('api/add-question')}}',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSend: function () {

                        errorDiv.fadeOut().html('');
                        btn.html('<span class="spinner-border" role="status" aria-hidden="true"></span> &nbsp; submitting...')

                    },
                    success: function (data) {
                        console.log(data);
                        if(data.statusCode == 200){
                            var d = data.data;

                            errorDiv.fadeIn(1000, function () {
                                errorDiv.html('<div class="alert alert-dark-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Question added successfully</div>');
                                $('#createQuestion').trigger('reset');
                                window.location = d.url;
                                console.log(formData);
                            })
                        }
                        else{
                            errorDiv.fadeIn(1000, function () {
                                errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Try again later</div>')
                            })
                        }

                    },
                    complete: function () {
                        btn.html(btnHtml);
                    },
                    error: function (error) {
                        errorDiv.fadeIn(1000, function () {
                            errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Kindly try again later</div>')
                        })
                    }
                })
            }

        })

    </script>

    <script>
        // $("#subject_name").select2();
        $('.subject-list').select2();
    </script>

@stop
