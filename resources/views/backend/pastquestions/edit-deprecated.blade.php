@extends('backend.layouts.layout-2')

@section("styles")

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.css"
      integrity="sha512-pDpLmYKym2pnF0DNYDKxRnOk1wkM9fISpSOjt8kWFKQeDmBTjSnBZhTd41tXwh8+bRMoSaFsRnznZUiH9i3pxA=="
      crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote.min.js" integrity="sha512-kZv5Zq4Cj/9aTpjyYFrt7CmyTUlvBday8NGjD9MxJyOY/f2UfRYluKsFzek26XWQaiAp7SZ0ekE7ooL9IYMM2A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<style>
    .modal {
        overflow-y: auto !important;
    }
</style>
@endsection

@section('content')

<h4 class="font-weight-bold py-3 mb-4">
    <span class="text-muted font-weight-light">Past Question Group /</span> Add Past Question
</h4>

<div class="card mb-4">
    <h6 class="card-header">
        Add Past Question
    </h6>
    <div class="card-body">

        @includeif('backend.message')
        @include("backend.questions.partials.edit_form")
    </div>

</div>

@include("backend.questions.partials.modal")

@endsection
@section('scripts')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
        integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg=="
        crossorigin="anonymous">
</script>

<style>
    .error{
        color: red;
        display: none;
    }

</style>

<script>
    let section = "";
    let sections_info = @json($past_question_group->section_info);

    const add_selected_question = $(".add-selected-question")

    $(".add-question-bank-modal").off('click').on("click",function (){
        section = $(this).data("section");
        add_selected_question.attr("data-section",section)
    })

    addQuestionToList();

</script>
<script>

    $(function() {
        $('.subjects-select-2').each(function() {
            $(this)
                .wrap('<div class="position-relative"></div>')
                .select2({
                    placeholder: 'Select value',
                    dropdownParent: $(this).parent()
                });
        })
    });

    function blockUi() {
        $.blockUI({
            message: '<div class="sk-folding-cube sk-primary"><div class="sk-cube1 sk-cube"></div>' +
                '<div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div>' +
                '<div class="sk-cube3 sk-cube"></div></div><h5 style="color: #fff">Loading Questions ...</h5>',
            css: {
                backgroundColor: 'transparent',
                border: '0',
                zIndex: 9999999
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.8,
                zIndex: 9999990
            }
        });
    }

    function unblockUI() {
        $.unblockUI();
    }

    var option = '';
    var category_id = "{{old('institute_type')}}";
    var school_id = "{{old('school_name')}}";
    var question_id = "{{old('question')}}";
    var class_id = "";
    var course_id = "";
    var questions = [];
    var questions_object = [];
    var num = 1;
    let questions_data = [];
    let question_group_id = "{{Request::query("group-id")}}";
    let question_bank_ids = [];
    var questions_array = ("{{$questions}}");
    let subject_id = "{{$past_question_group->subject_id}}"


    $(document).ready(function () {

        $('#add-question-modal').on('shown.bs.modal', function() {
            $('.summernote').summernote({
                height: 100
            });
        })

        $('#type').on('change',function () {

            var type = $(this).val();
            $(this).removeClass('is-invalid');
            $('#type_error').fadeOut();
            if(type == 'multi-choice'){
                $('#true-false').fadeOut(500, function () {
                    $('#multi-choice').fadeIn(500);
                })
            }
            else{
                $('#multi-choice').fadeOut(500, function () {
                    $('#true-false').fadeIn(500);
                })
            }
        })

        $('#question').on('keyup', function () {
            $('#question_error').fadeOut();
            $(this).removeClass('is-invalid');
        })

        $('#answer1').on('keyup', function () {
            $(this).removeClass('is-invalid');
            $('#1_error').fadeOut();
        })

        $('#answer2').on('keyup', function () {
            $(this).removeClass('is-invalid');
            $('#2_error').fadeOut();
        })

        $('#answer3').on('keyup', function () {
            $(this).removeClass('is-invalid');
            $('#3_error').fadeOut();
        })

        $('#answer4').on('keyup', function () {
            $(this).removeClass('is-invalid');
            $('#4_error').fadeOut();
        })

        $('input[type=checkbox]').on('change', function () {
            if($(this).prop('checked') == true){
                option = $(this).val();
            }
            else{
                option = '';
            }
            $('input[type=checkbox]').removeClass('is-invalid');
        });

        let i = 1;
        let options_count = 0;
        let num_of_options = 0;
        let all_options = [];

        $("#add-new-option").on("click",function (){
            ++i;
            let last_option = $(".option:last")
            last_option.clone().appendTo( last_option );
            let last_option_label = $(".option:last > div > label:first");
            last_option_label.text(`Option ${i}`);
            console.log("///////////////");
            let input = $("#option > div:nth-child(1) > div > input")[i-1]
            let check_input = $("#option > div:nth-child(2) > div > label > input")[i-1]
            input['placeholder']=`Option ${i}` ;
            input['name']=`answer${i}`;
            console.log(input['value'],"input value");
            input['id']=`answer${i}`;
            check_input['id'] = `checkAnswer${i}`;
            $(`#checkAnswer${i}`).val(i)
            num_of_options = i;
        });

        $('#submitQuestion').on('click', function () {

            for (i = 1; i < num_of_options; i++){
                all_options[i-1]= $("#option > div:nth-child(1) > div > input")[i-1]['value']
            }

            console.log("////// options////")
            console.log(all_options)

            var institution = $('#institute_type').val();
            var school = $('#school_name').val();
            var course = $('#course_name').val();
            var class_name = $('#class_name').val();
            var subject = $('#subject_name').val();
            var question = $('#question').val();
            var question_type = $('#type').val();

            var past_question_type = $('#past-question-type').val();
            var year          = $('#year').val();

            $('.q_input').removeClass('is-invalid');
            $('.error').fadeOut();

            var error = false;

            if(!question || question.trim() == ''){
                $('#question_error').fadeIn(500);
                $('#question').addClass('is-invalid');
                error = true;
            }

            if(!question_type){
                $('#type_error').fadeIn(500);
                $('#type').addClass('is-invalid');
                error = true;
            }

            if(question_type == 'multi-choice'){
                var option1 = $('#answer1').val(),
                    option2 = $('#answer2').val();
                var option3 = $('#answer3').val(),
                    option4 = $('#answer4').val();
                // num_of_options = 4;


                var err = false;
                $('input[name=checkAnswer]').each(function () {
                    var checked = $(this).prop('checked');
                    if(!checked){
                        err = true
                    }
                    else {
                        err = false
                        return false;
                    }
                })
                if(err == true){
                    $('input[name=checkAnswer]').addClass('is-invalid');
                    error = true;
                }
            }

            else{
                option = $('input[name=true-false-answer]:checked').val();
                num_of_options = 2;
            }

            if(!error){
                var options = [];
                if(question_type == 'multi-choice'){
                    options = all_options;
                }
                else {
                    options = ['True', 'False']
                }
                var answer = option;

                var formData = new FormData()
                formData.append('type', question_type)
                formData.append('question', question)
                formData.append('num_of_options', num_of_options)
                formData.append('active_status', 1)
                formData.append('school_id', school)
                formData.append('course_id', course)
                formData.append('class_id', class_name)
                formData.append('subject_id', subject)
                formData.append('options', options)
                formData.append('past_question_type', past_question_type)
                formData.append('answer', answer)
                formData.append('year', year)
                formData.append('image_question', image_file)

                console.log(formData);
                var errorDiv = $('#errorDiv'), btn = $('#submitQuestion'), btnHtml = 'Submit';
                $.ajax({
                    url: '{{URL('api/add-question')}}',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSend: function () {

                    errorDiv.fadeOut().html('');
                    btn.html('<span class="spinner-border" role="status" aria-hidden="true"></span> &nbsp; submitting...')

                },
                success: function (data) {
                    console.log(data);
                    if(data.statusCode == 200){
                        var d = data.data;

                        errorDiv.fadeIn(1000, function () {
                            errorDiv.html('<div class="alert alert-dark-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Question added successfully</div>');
                            $('#createQuestion').trigger('reset');
                            window.location = d.url;
                            console.log(formData);
                        })
                    }
                    else{
                        errorDiv.fadeIn(1000, function () {
                            errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Try again later</div>')
                        })
                    }

                },
                complete: function () {
                    btn.html(btnHtml);
                },
                error: function (error) {
                    errorDiv.fadeIn(1000, function () {
                        errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Kindly try again later</div>')
                    })
                }
            })
            }

        })

        if(category_id){
            $("#institute_type").val(category_id).trigger('change');
        }


        $('#question').on('change', function () {
            $(this).removeClass('is-invalid');
            $('#question_error').fadeOut();
        })

        const data = {group_id: question_group_id }
        fetch("{{route("backend.past-questions-group.questions",$group_id)}}", { method: 'GET'})
    .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
                for (let ques of data.questions) {
                    console.log(ques.questions)
                    // if (!question_bank_ids.includes(ques.questions.id)) {
                    //     question_bank_ids.push(ques.questions.id)
                    $('#questionDiv').append(appendQuestion(ques.question));
                    num = num + 1;
                    // } else {
                    //     commonObj.messages('error', 'Question already added')
                    // }
                }
                removeQuestion()
            }).catch((error) => {
                console.error('Error:', error);
            });

        $('#addQuestion').on('click', function () {
            // var subject_id = $(this).val();
            var subject_id = "{{$past_question_group->subject_id}}";
            $(this).removeClass('is-invalid');
            $('#subject_error').fadeOut();

            $.ajax({
                type: "POST",
                url: '{{ route("ajax.school.questions") }}',
                data: {'subject_id': subject_id, '_token': '{{ csrf_token() }}'},
                beforeSend: blockUi(),
                success: function (data) {
                    $("#question").html(data.questions);
                    questions_data = data.questions;
                    if (question_id) {
                        $("#question").val(question_id).trigger('change');
                        for (question_var of parsed_questions_array) {
                            let ques = question_var.questions.question;
                            questions.push(question_var.question_bank_id);
                            $('#questionDiv')
                                .append(appendQuestion(ques));
                            num = num + 1;
                        }
                        removeQuestion();
                    }
                    unblockUI();
                }
            });
        })

        $('#question').on('change', function () {
            $(this).removeClass('is-invalid');
            $('#question_error').fadeOut();
        })

        $('#add-selected-question').on('click', function () {
            // if (!$('#question').val()) {
            if (!$('#question_text').val()) {
                $('#question').addClass('is-invalid');
                $('#question_error').fadeIn(500)
            } else {
                // var value = $('#question').val();
                var value = $('#question_text').val();
                var answer_type = $("#type").val();// true-false / multi-choice
                var true_false_question_answer = $('input[type="radio"][name="true-false-answer"]:checked').val();
                var multi_choice_question_answer = $('input[type="checkbox"][name="checkAnswer"]:checked').val();
                var answer_elements = $(".answer");
                var true_false_answer_elements = $(".true-false-answer");

                var answers = [];
                if (!question_bank_ids.includes(value)) {
                    // if(!questions.includes(value) || !question_bank_ids.includes(value)){
                    $(document).trigger('questionAdded');
                    questions.push(value);

                    if (answer_type === "true-false"){
                        for(let x =0; x < true_false_answer_elements.length; x++){
                            answers.push((true_false_answer_elements[x]).value);
                        }
                    }else{
                        for(let i =0; i < answer_elements.length; i++){
                            answers.push((answer_elements[i]).value);
                        }
                    }


                    console.log(answer_elements, "Answer Elements")
                    console.log(answers, "Answers")
                    questions_object.push(
                        {
                            question       : value,
                            type           : answer_type,
                            answer         : answer_type === "true-false" ? true_false_question_answer : multi_choice_question_answer,
                            num_of_options : answer_type === "true-false" ? 2 : num_of_options,
                            answers        : answers
                        })
                    question_bank_ids.push(value)
                    // var question = $('#question').find(":selected").text();
                    var question = $('#question_text').val();
                    $('#questionDiv').append('<div class="alert alert-dark alert-dismissible fade show"><button type="button" data-id="' + num + '" class="close rm" data-dismiss="alert">×</button>' + question + '</div>');
                    num = num + 1;
                    console.log(question_bank_ids)

                    commonObj.messages('success', `Successfully added question to section ${section}!`)
                } else {
                    commonObj.messages('error', 'Question already added')
                }
            }
        })

        $(document).on('questionAdded', function (e) {

            $('.rm').on('click', function () {
                let id = $(this).data('id');
                if (question_bank_ids.includes(id)) {
                    question_bank_ids.splice(question_bank_ids.indexOf(id));
                }
                id = parseInt(id);
                id = id - 1;
                questions.splice(id, 1);
                num = num - 1;
                console.log("questions " + questions);
                console.log("hello num " + num)
                console.log(question_bank_ids);
            })
        });

        $('#submitQuestions').on('click', function () {
            const data = {
                question_bank_ids      : question_bank_ids,
                past_question_group_id : question_group_id,
                has_section            : $("#group_has_sections").val(),
                section_naming_type    : $("#section_naming_type").val(),
                section_counts         : $("#section_counts").val(),
                sections_info          : sections_info,
                questions              : questions,
                questions_object       : questions_object,
                class_id               : $("#class_name").val(),
                subject_id             : $("#subject_name").val(),
            };

            console.log(questions_object)

            fetch("{{route("backend.past-questions-group.update")}}", {
                method: 'POST', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            }).then(response => response.json())
                .then(data => {
                    console.log('Success:', data);
                    commonObj.messages('success',
                        'Successfully updated question group!')
                }).catch((error) => {
                    console.error('Error:', error);
                });
        });
    })


    setTimeout(function (){
        $("#subject_name").val("{{$past_question_group->subject_id}}");
        $("#class_name").val("{{$past_question_group->class_id}}");
    },4000);

    {{--setTimeout(function (){--}}
        {{--    let has_sections = $("#group_has_sections").is(":checked");--}}

        {{--    if (has_sections){--}}
            {{--        $("#section_row").show();--}}

            {{--        $("#section_naming_type").val("{{$past_question_group->section_naming_type}}");--}}
            {{--    }else {--}}
            {{--        $("#section_row").hide();--}}
            {{--    }--}}

        {{--    $("#class_name").val("{{$past_question_group->class_id}}");--}}

        {{--    $("#class_name").trigger("change");--}}

        {{--},2000);--}}

    function addQuestionToList() {
    }

    function appendQuestion(text) {
        return '<div class="alert alert-dark alert-dismissible fade show">' +
            '<button type="button" data-id="' + num + '" class="close rm" data-dismiss="alert">×' +
            '</button>' + text + '</div>';
    }

    function appendSuccessMessage(){
        return '<div class="alert alert-dark-primary alert-dismissible fade show">' +
            '<button type="button" data-id="' + num + '" class="close rm" data-dismiss="alert">×' +
            '</button>' + text + '</div>';
    }

    // function removeQuestion() {
    //     $('.rm').on('click', function () {
    //         var id = $(this).data('id');
    //         if (question_bank_ids.includes(id)) {
    //             question_bank_ids.splice(question_bank_ids.indexOf(id));
    //         }
    //         id = parseInt(id);
    //         id = id - 1;
    //         questions.splice(id, 1);
    //         num = num - 1;
    //         console.log(question_bank_ids);
    //     })
    // }

    function removeQuestion() {
        $('.rm').on('click', function () {
            var id = $(this).data('id');
            if (question_bank_ids.includes(id)) {
                question_bank_ids.splice(question_bank_ids.indexOf(id));
            }
            id = parseInt(id);
            id = id - 1;
            // questions.splice(id, 1);
            questions_object.splice(id, 1);
            num = num - 1;
            console.log(question_bank_ids);
        })
    }

</script>

@stop
