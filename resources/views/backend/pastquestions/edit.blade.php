@extends('backend.layouts.layout-2')

@section("styles")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.css"
          integrity="sha512-pDpLmYKym2pnF0DNYDKxRnOk1wkM9fISpSOjt8kWFKQeDmBTjSnBZhTd41tXwh8+bRMoSaFsRnznZUiH9i3pxA=="
          crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote.min.js" integrity="sha512-kZv5Zq4Cj/9aTpjyYFrt7CmyTUlvBday8NGjD9MxJyOY/f2UfRYluKsFzek26XWQaiAp7SZ0ekE7ooL9IYMM2A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <style>
        .modal {
            overflow-y: auto !important;
        }
    </style>
@endsection

@section('content')

    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Past Question Group /</span> Add Past Question
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Add Past Question
        </h6>
        <div class="card-body">

            @includeif('backend.message')
            @include("backend.questions.partials.edit_form")
        </div>

    </div>

    @include("backend.questions.partials.modal")

@endsection
@section('scripts')

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
            integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg=="
            crossorigin="anonymous">
    </script>

    <style>
        .error{
            color: red;
            display: none;
        }

    </style>

    <script>

        let option = '';
        let question_id = "{{old('question')}}";
        let questions = [];
        let questions_object = [];
        let num = 1;
        let questions_data = [];
        let question_group_id = "{{Request::query("group-id")}}";
        let question_bank_ids = [];
        let questions_array = ("{{$questions}}");
        let subject_id = "{{$past_question_group->subject_id}}"
        const add_selected_question = $(".add-selected-question")
        const questionText = $("#question_text");
        let class_name = $('#class_name').val();
        let subject = $('#subject_name').val();
        let question = $('#question').val();
        let question_type = $('#type').val();
        let year          = $('#year').val();
        let error = false;


        $(".add-question-bank-modal").off('click').on("click",function (){
            section = $(this).data("section");
            add_selected_question.attr("data-section",section)
        })

        function blockUi() {
            $.blockUI({
                message: '<div class="sk-folding-cube sk-primary"><div class="sk-cube1 sk-cube"></div>' +
                    '<div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div>' +
                    '<div class="sk-cube3 sk-cube"></div></div><h5 style="color: #fff">Loading Questions ...</h5>',
                css: {
                    backgroundColor: 'transparent',
                    border: '0',
                    zIndex: 9999999
                },
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.8,
                    zIndex: 9999990
                }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        $('#add-question-modal').on('shown.bs.modal', function() {
            $('.summernote').summernote({
                height: 100
            });
        })

        $("#type").on('change',function () {

            var type = $(this).val();
            $(this).removeClass('is-invalid');
            $('#type_error').fadeOut();
            if(type == 'multi-choice'){
                $('#true-false').fadeOut(500, function () {
                    $('#multi-choice').fadeIn(500);
                })
            }
            else{
                $('#multi-choice').fadeOut(500, function () {
                    $('#true-false').fadeIn(500);
                })
            }
        })

        $('#question_text').on('keyup', function () {
            $('#question_error').fadeOut();
            $(this).removeClass('is-invalid');
        })

        $('#answer1').on('keyup', function () {
            $(this).removeClass('is-invalid');
            $('#1_error').fadeOut();
        })

        $('input[type=checkbox]').on('change', function () {
            if($(this).prop('checked') == true){
                option = $(this).val();
            }
            else{
                option = '';
            }
            $('input[type=checkbox]').removeClass('is-invalid');
        });

        let i = 1;
        let num_of_options = 0;
        let all_options = [];

        $("#add-new-option").on("click",function (){
            ++i;
            let last_option = $(".option:last")
            last_option.clone().appendTo( last_option );
            let last_option_label = $(".option:last > div > label:first");
            last_option_label.text(`Option ${i}`);
            let input = $("#option > div:nth-child(1) > div > input")[i-1]
            let check_input = $("#option > div:nth-child(2) > div > label > input")[i-1]
            input['placeholder']=`Option ${i}` ;
            input['name']=`answer${i}`;
            input['id']=`answer${i}`;
            check_input['id'] = `checkAnswer${i}`;
            $(`#checkAnswer${i}`).val(i)
            num_of_options = i;
        });

        $('#submitQuestion').on('click', function () {

            for (i = 1; i < num_of_options; i++){
                all_options[i-1]= $("#option > div:nth-child(1) > div > input")[i-1]['value']
            }

            $('.q_input').removeClass('is-invalid');
            $('.error').fadeOut();


            if(!question || question.trim() == ''){
                $('#question_error').fadeIn(500);
                $('#question').addClass('is-invalid');
                error = true;
            }

            if(!question_type){
                $('#type_error').fadeIn(500);
                $('#type').addClass('is-invalid');
                error = true;
            }

            if(question_type == 'multi-choice'){
                var option1 = $('#answer1').val(),
                    option2 = $('#answer2').val();
                var option3 = $('#answer3').val(),
                    option4 = $('#answer4').val();
                // num_of_options = 4;


                var err = false;
                $('input[name=checkAnswer]').each(function () {
                    var checked = $(this).prop('checked');
                    if(!checked){
                        err = true
                    }
                    else {
                        err = false
                        return false;
                    }
                })
                if(err == true){
                    $('input[name=checkAnswer]').addClass('is-invalid');
                    error = true;
                }
            }

            else{
                option = $('input[name=true-false-answer]:checked').val();
                num_of_options = 2;
            }

        })

        listAllQuestions();

        $('#addQuestion').on('click', function () {
                // var subject_id = $(this).val();
                var subject_id = "{{$past_question_group->subject_id}}";
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.questions") }}',
                    data: {'subject_id': subject_id, '_token': '{{ csrf_token() }}'},
                    beforeSend: blockUi(),
                    success: function (data) {
                        $("#question").html(data.questions);
                        questions_data = data.questions;
                        if (question_id) {
                            $("#question").val(question_id).trigger('change');
                            for (question_var of parsed_questions_array) {
                                let ques = question_var.questions.question;
                                questions.push(question_var.question_bank_id);
                                // $('#questionDiv')
                                //     .append(appendQuestion(ques));
                                // num = num + 1;
                            }
                            removeQuestion();
                        }
                        unblockUI();
                    }
                });
            })
            $('#question').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#question_error').fadeOut();
            });

            $('#add-selected-question').on('click', function () {
                if (!questionText.val()) {
                    $('#question').addClass('is-invalid');
                    $('#question_error').fadeIn(500)
                } else {
                    let value = $('#question_text').val();
                    let answer_type = $("#type").val();// true-false / multi-choice
                    let true_false_question_answer = $('input[type="radio"][name="true-false-answer"]:checked').val();
                    let multi_choice_question_answer = $('input[type="checkbox"][name="checkAnswer"]:checked').val();
                    let answer_elements = $(".answer");
                    let true_false_answer_elements = $(".true-false-answer");

                    let answers = [];
                    if (!question_bank_ids.includes(value)) {
                        $(document).trigger('questionAdded');
                        questions.push(value);

                        if (answer_type === "true-false"){
                            for(let x =0; x < true_false_answer_elements.length; x++){
                                answers.push((true_false_answer_elements[x]).value);
                            }
                        }else{
                            for(let i =0; i < answer_elements.length; i++){
                                answers.push((answer_elements[i]).value);
                            }
                        }

                        questions_object.push(
                            {
                                question       : value,
                                type           : answer_type,
                                answer         : answer_type === "true-false" ? true_false_question_answer : multi_choice_question_answer,
                                num_of_options : answer_type === "true-false" ? 2 : num_of_options,
                                answers        : answers
                            });

                        question_bank_ids.push(value)
                        let question = questionText.val();

                        const data = {
                            question_bank_ids      : question_bank_ids,
                            past_question_group_id : question_group_id,
                            has_section            : $("#group_has_sections").val(),
                            section_naming_type    : $("#section_naming_type").val(),
                            section_counts         : $("#section_counts").val(),
                            questions              : questions,
                            questions_object       : questions_object,
                            class_id               : $("#class_name").val(),
                            subject_id             : $("#subject_name").val(),
                        };

                        addNewQuestion(data)

                        commonObj.messages('success', `Successfully added question to section!`)
                    } else {
                        commonObj.messages('error', 'Question already added')
                    }
                }
            })

        $(document).on('questionAdded', function (e) {

                $('.rm').on('click', function () {
                    let id = $(this).data('id');
                    if (question_bank_ids.includes(id)) {
                        question_bank_ids.splice(question_bank_ids.indexOf(id));
                    }
                    id = parseInt(id);
                    id = id - 1;
                    questions.splice(id, 1);
                    num = num - 1;
                })
            });

        $('#submitQuestions').on('click', function () {
                const data = {
                    question_bank_ids      : question_bank_ids,
                    past_question_group_id : question_group_id,
                    has_section            : $("#group_has_sections").val(),
                    section_naming_type    : $("#section_naming_type").val(),
                    section_counts         : $("#section_counts").val(),
                    sections_info          : sections_info,
                    questions              : questions,
                    questions_object       : questions_object,
                    class_id               : $("#class_name").val(),
                    subject_id             : $("#subject_name").val(),
                };
            addNewQuestion(data);
        })

        function appendQuestion(ques) {
            return '<div class="alert alert-dark alert-dismissible fade show">' +
                '<button type="button" data-id="' + ques.id + '" class="close rm" data-dismiss="alert">×' +
                '</button>' + ques.question + '</div>';
        }

        function appendSuccessMessage(){
            return '<div class="alert alert-dark-primary alert-dismissible fade show">' +
                '<button type="button" data-id="' + num + '" class="close rm" data-dismiss="alert">×' +
                '</button>' + text + '</div>';
        }

        function removeQuestion() {
            $('.rm').on('click', function () {
                var id = $(this).data('id');
                if (question_bank_ids.includes(id)) {
                    question_bank_ids.splice(question_bank_ids.indexOf(id));
                }
                id = parseInt(id);
                id = id - 1;
                // questions.splice(id, 1);
                questions_object.splice(id, 1);
                num = num - 1;
            })
        }

        function addNewQuestion(data) {
            fetch("{{route("backend.past-questions-group.update")}}", {
                    method: 'POST', // or 'PUT'
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                }
            ).then(response => response.json())
                .then(data => {
                    // $("#question-modal-form").trigger("reset");
                    $('#questionDiv').html("");

                    listAllQuestions();
                })
        }

        function listAllQuestions() {
            fetch("{{route("backend.past-questions-group.questions",$group_id)}}", {method: 'GET'}
            )
                .then(response => response.json())
                .then(data => {
                    for (let ques of data.questions) {
                        $('#questionDiv').append(appendQuestion(ques));
                        num = num + 1;
                    }
                }).catch((error) => {
            });
        }

    </script>

@stop
