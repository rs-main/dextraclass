@extends('backend.layouts.layout-2')

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Departments /</span> Edit Department
    </h4>
	<div class="card mb-4">
        <h6 class="card-header">
            Edit Department
        </h6>
        <div class="card-body">
			@includeif('backend.message')
            <form action="{{route('backend.departments.update', $department->id)}}" method = "post">
			@csrf
			 @method('PUT')
			<div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Institute Type</label>
                    <div class="col-sm-10">
                        <select name="category" class="custom-select" disabled>
						<option value="">Select Institute Type</option>
						@foreach($categories as $category)
							<option value="{{$category->id}}" @if($school_details->school_category == $category->id) selected @endif>{{$category->name}}</option>
						@endforeach
						</select>
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">School</label>
                    <div class="col-sm-10">
						
                        <select name="school" id="school" class="custom-select" disabled required>
						<option value="">Select School</option>
						@foreach($schools as $school)
							<option value="{{$school->id}}" @if($department->school_id == $school->id) selected="selected" @endif>{{$school->school_name}}</option>
						@endforeach
						</select>
						<input type="hidden" name="school" value="{{$department->school_id}}">
                    </div>
                </div>
				
                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Department Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" placeholder="Department Name" value="{{$department->name}}" class="form-control" required>
                    </div>
                </div>
				
				<div class="form-group row">
                <label class="col-form-label col-sm-2 text-sm-right"></label>
                <div class="col-sm-10">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="status" value="1" class="custom-control-input" @if($department->status) checked @endif>
                        <span class="custom-control-label">Active</span>
                    </label>
                </div>
            </div>
                
                
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <a href = "{{route('backend.departments.index')}}" class="btn btn-danger mr-2">Cancel</a> <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')

<script>
    $(document).ready(function () {
        $("#institute_type").on("change", function () {
            var category_id = $(this).val();
          
            $.ajax({
                type: "POST",
                url: '{{ route("ajax.category.schools") }}',
                data: {'category': category_id, '_token': '{{ csrf_token() }}'},
                success: function (data) {
                    $("#school").html(data.schools);
                }
            });
        });

  });
</script>
@stop