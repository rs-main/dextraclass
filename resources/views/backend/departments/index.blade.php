@extends('backend.layouts.layout-2')

@section('scripts')

    <script type="text/javascript">
        $(function () {
            $('#department-list').dataTable(
                {
                    "columns": [
                        null,
                        { "orderable": false },
                        null,
                        { "orderable": false }
                    ],
                    initComplete: function () {
                        this.api().columns([1]).every( function () {
                            var column = this;
                            var select = $('<select class="custom-select"><option value="">All</option></select>')
                                .appendTo( $(column.header()) )
                                .on( 'change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                } );

                            column.data().unique().sort().each( function ( d, j ) {
                                select.append( '<option value="'+d+'">'+d+'</option>' )
                            } );
                        } );
                    }
                }
            );
        });
    </script>
@endsection

@section('content')

    @includeif('backend.message')

    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Departments</div>
        <a href="{{route('backend.departments.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create department</a>
    </h4>

    <div class="card">
        <div class="card-datatable table-responsive">
            <table id="department-list" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="min-width: 18rem" class="align-top">Department Name</th>
                        <th style="min-width: 18rem" class="align-top">School Name</th>
                        <th class="align-top">Status</th>
                        <th style="min-width: 9rem" class="align-top">Action</th>
                    </tr>
                </thead>
                <tbody>
                @php $i=0; @endphp
                @if(!$departments->isEmpty())
                    @foreach($departments as $department)
                        <tr>
                            <td>{{$department->name}}</td>
                            <td>{{$department->school->school_name}}</td>
                            <td class='text-align align-middle'><button class="btn {{$department->status ? 'btn-success':'btn-danger'}}">{{$department->status ? 'Active':'Disabled'}}</button></td>
                            <td class='form-inline'>
                                <a href ="{{route('backend.departments.edit', $department->id)}}" class="btn-primary  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5 px;padding-top: 3px; padding-bottom: 3px;" title="Edit"><i class="ion ion-md-create"></i></a>
                                @role('admin')
                                <form method="POST" action="{{route('backend.departments.destroy', $department->id)}}" style="display: inline-block;">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip"  style="margin-right: 5px; margin-left: 5 px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

                                </form>
                                @endrole
                                <a href ="{{route('backend.departments.show', $department->id)}}" class="btn-primary  btn-xs  md-btn-flat article-tooltip" style="margin-right: 4px; margin-left: 5 px;padding-top: 3px; padding-bottom: 3px;" title="View department details"><i class="ion ion-md-eye"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5"><p class="text-center">No Record Found!</p></td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
