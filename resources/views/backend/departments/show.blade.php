@extends('backend.layouts.layout-3')

@section('content')
	
    <!-- Content -->
          <div class="container-fluid flex-grow-1 container-p-y">

            <!-- Header -->
            <div class="container-m-nx container-m-ny bg-white mb-4">
			<div class="row">
			<div class="col-md-10">
              <div class="media col-md-10 col-lg-8 col-xl-7 py-5 ml-3">
			 <!--  @if(isset($course->school->logo) && $course->school->logo != 'noimage.jpg')
						<img class="school_logo mb-2 d-block rounded-circle" src='{{url("uploads/schools")}}/{{$course->school->logo}}'  /><br />
			   @endif -->
			  
                
                <div class="media-body ml-1">
                  <h4 class="font-weight-bold mb-4">{{$department->name}}</h4>
				  <div class="row mb-2">
                      <div class="col-md-3 text-muted">School Name:</div>
                      <div class="col-md-9">
                       <a href="{{route('backend.school.show',$department->school_id)}}" class="text-body">{{$department->school->school_name}}</a>
                      </div>
                    </div>

                   
				</div>
             </div>
			 </div>
			 <div class="col-md-2 ml-10 mt-5">
			<a href="javascript:void(0)" onclick="window.history.go(-1); return false;" class="btn btn-primary rounded-pill d-block detail-back-btn">Back</a>
			 </div>
			 </div>
              <hr class="m-0">
            </div>
            <!-- Header -->
			
			
		@includeif('backend.message') 	
			
		<div class="row">
            <div class="col">
			 <div class="card mb-8">
			 <div class="card-header row ml-0 mr-0">
		
		<div class="col-md-9"><strong>Courses</strong></div>
		 <div class="col-md-3"><a href="javascript:void(0)" class="btn btn-primary rounded-pill d-block" data-toggle="modal" data-target="#createCourse"><span class="ion ion-md-add"></span>&nbsp;Create Course</a></div>
		
		
       </div>
		
                  <div class="card-body">
                   <div class="card-datatable table-responsive">
        <table id="courses-list" class="table table-striped table-bordered">
            <thead>
                 <tr>
                    <th>S.No</th>
                    <th style="min-width: 18rem">Course Name</th>
                    <th style="min-width: 18rem">School Name</th>
					<th>Status</th>
                    <th>Action</th>
                </tr>
           <tbody>
                @php $i=0; @endphp
				@if(!$courses->isEmpty())
                @foreach($courses as $course)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{$course->name}}</td>
                    <td>{{$course->school->school_name}}</td>
					<td>{{$course->status ? 'Active':'Disabled'}}</td>
                    <td>
                       <a href ="{{route('backend.course.show', $course->id)}}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" title="View course details"><i class="ion ion-md-eye"></i></a>
                       
					   <a href ="#" data-attr-id="{{$course->id}}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip edit-course-link" title="Edit"><i class="ion ion-md-create"></i></a>
					   
					   @role('admin')
					   <form method="POST" action="{{route('backend.course.destroy', $course->id)}}" style="display: inline-block;">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
							<input type="hidden" name="course" value="{{$course->id}}">
							<input type="hidden" name="department_ajax_request" value="1">
                            <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" title="Remove"><i class="ion ion-md-close"></i></button>

                        </form>
						@endrole

                    </td>
                </tr>
                @endforeach
				@else
                  <tr>
                      <td colspan="5"><p class="text-center">No Record Found!</p></td>
				  </tr>
                  @endif
            </tbody>
            </thead>
        </table>
    </div>
                  </div>
                  
                </div>
			</div>
			

          </div>
			
		 </div>
		 
<!-- create period modal -->
<div class="modal" id="createCourse">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Create Course</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <form action="{{route('backend.course.store')}}" method = "post">
            @csrf
			
			<input type="hidden" name="institute_type" value="{{$school->school_category}}">
			<input type="hidden" name="school_name" value="{{$school->id}}">
			<input type="hidden" name="department_ajax_request" value="1">
			<input type="hidden" name="department" value="{{$department->id}}">
			
			
			 <div class="form-group row">
                    <label class="col-form-label col-sm-3 text-sm-right">Course Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" placeholder="Course Name" value="{{ old('name') }}" class="form-control" required>
                    </div>
                </div>
				
				<div class="form-group row">
                    <label class="col-form-label col-sm-3 text-sm-right">Course Description</label>
                    <div class="col-sm-9">
                        <textarea name="description" class="form-control" placeholder="Course Description">{{ old('description') }}</textarea>
                    </div>
                </div>
				
				<div class="form-group row">
                <label class="col-form-label col-sm-3 text-sm-right"></label>
                <div class="col-sm-9">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" name="status" value="1" @if(old('status')) checked @endif class="custom-control-input">
                        <span class="custom-control-label">Active</span>
                    </label>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-10 ml-sm-auto">
                    <button data-dismiss="modal" class="btn btn-danger mr-2">Cancel</button> <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
      </div>

    </div>
  </div>
</div>

<!-- edit period modal -->
<div class="modal" id="editCourse">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Course</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body edit_course">
        
      </div>

    </div>
  </div>
</div>

	 
          <!-- / Content -->
@endsection
@section('scripts')

<script>
    $(document).ready(function () {
	
    
        
     $(".edit-course-link").on("click", function () {
            var course_id = $(this).attr("data-attr-id");
			$(".edit_course").html("Loading...");
			$("#editCourse").modal();
            if(course_id) {                
                $.ajax({
                    type: "POST",
                    url: '{{ route("backend.course.edit_ajax") }}',
                    data: {'course_id' : course_id, 'department_ajax_request' : 1, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $(".edit_course").html(data);
						return true;
                    }
                });
            }
        });
		
		$(".edit-department-link").on("click", function () {
            var department_id = $(this).attr("data-attr-id");
			$(".edit_department").html("Loading...");
			$("#editDepartment").modal();
            if(department_id) {                
                $.ajax({
                    type: "POST",
                    url: '{{ route("backend.departments.edit_ajax") }}',
                    data: {'department_id' : department_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $(".edit_department").html(data);
						return true;
                    }
                });
            }
        });

 
 });
 
  $(function () {
        $('#courses-list').dataTable(
			{
           // "order": [[ 1, "asc" ]],
            "columns": [
              null,
              null,
              { "orderable": false },
			  null,
              { "orderable": false }
            ]
          }
		);
		
		 $('#department-list').dataTable(
			{
           // "order": [[ 1, "asc" ]],
            "columns": [
              null,
              null,
              null,
			  null,
              { "orderable": false }
            ]
          }
		);
		
    });
</script>
@stop