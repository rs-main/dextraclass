@extends('backend.layouts.layout-2')

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Questions Bank /</span> Edit Question
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Edit Question
        </h6>
        <div class="card-body">
            @includeif('backend.message')
            <form method = "POST"  action="{{route('backend.questions.update', $question->id)}}" id="createQuestion">
                @csrf
                @method('PUT')
                <div id="errorDiv">
                    <!-- error will be shown here -->
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Institution Type</label>
                    <div class="col-sm-10">
                        <select name="institute_type" id="institute_type" class="custom-select q_input" disabled required>
                            <option value="" selected="" disabled="" class="d-none">Select Institution Type</option>
                            @foreach($institutes as $id => $type)
                                <option value="{{$id}}"  @if($id == $question->category_id ) selected @endif>{{$type}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="institution_error">
                            Select institution
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">School</label>
                    <div class="col-sm-10">
                        <select name="school_name" id="school_name" class="custom-select q_input" disabled required>
                            <option value="" selected="" disabled="" class="d-none">Select School</option>
                            @foreach($schools as $id => $val)
                                <option value="{{$id}}" @if($id == $question->school_id ) selected @endif>{{$val}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="school_error">
                            Select school
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Course</label>
                    <div class="col-sm-10">
                        <select name="course_name" id="course_name" class="custom-select q_input" disabled required>
                            <option value="" selected="" disabled="" class="d-none">Select Course</option>
                            @foreach($courses as $id => $val)
                                <option value="{{$id}}" @if($id == $question->course_id ) selected @endif>{{$val}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="course_error">
                            Select course
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Class</label>
                    <div class="col-sm-10">
                        <select name="class_name" id="class_name" class="custom-select q_input" disabled required>
                            <option value="" selected="" disabled="" class="d-none">Choose Class</option>
                            @foreach($classes as $id => $val)
                                <option value="{{$id}}" @if($id == $question->class_id ) selected @endif>{{$val}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="class_error">
                            Select class
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Subject</label>
                    <div class="col-sm-10">
                        <select name="subject_name" id="subject_name" class="custom-select q_input" disabled required>
                            <option value="" selected="" disabled="" class="d-none">Select Subject
                            @foreach($subjects as $id => $val)
                                <option value="{{$id}}" @if($id == $question->subject_id ) selected @endif>{{$val}}</option>
                            @endforeach
                        </select>
                        <div class="error" id="subject_error">
                            Select subject
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="question" class="col-form-label col-sm-2 text-sm-right">Question</label>
                    <div class="col-sm-10">
                        <textarea class="form-control q_input" name="question" id="question" required>{{$question->question}}</textarea>
                        <div class="error" id="question_error">
                            Enter question
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Answer Type</label>
                    <div class="col-sm-10">
                        <select name="type" id="type" class="custom-select q_input" disabled required>
                            <option value="" selected disabled>Select Question Type</option>
                            <option value="multi-choice" @if($question->type == 'multi-choice' ) selected @endif>Multiple Choice</option>
                            <option value="true-false" @if($question->type == 'true-false' ) selected @endif>True or False</option>
                        </select>
                        <div class="error" id="type_error">
                            Select question type
                        </div>
                    </div>
                </div>

                @if($question->type == 'multi-choice' )
                <div id="multi-choice" >
                    <div class="row">
                        <label class="col-form-label col-sm-2 text-sm-right"></label>
                        <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
                    </div>

                    @foreach($options as $id => $option)
                    <div class="form-group row">
                        <label for="answer{{$id + 1}}" class="col-form-label col-sm-2 text-sm-right">Option {{$id + 1}}</label>
                        <div class="col-sm-10">
                            <input type="text" name="answer{{$id + 1}}" id="answer{{$id + 1}}" value="{{$option->title}}" class="form-control">
                            <div class="error" id="{{$id + 1}}_error">
                                Enter option {{$id + 1}}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="answer{{$id + 1}}" class="col-form-label col-sm-2 text-sm-right"></label>
                        <div class="col-sm-10">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" name="checkAnswer" value="{{$id + 1}}" id="checkAnswer{{$id + 1}}" onClick="ckChange(this)" @if($question->choice_id == ($id + 1) ) checked @endif class="custom-control-input">
                                <span class="custom-control-label">Check if this is the valid answer</span>
                            </label>
                        </div>
                    </div>
                    @endforeach


                </div>

                @else
                <div id="true-false" >
                    <div class="row">
                        <label class="col-form-label col-sm-2 text-sm-right"></label>
                        <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Options</label>

                        <div class="custom-controls-stacked col-sm-10">
                            <label class="custom-control custom-radio">
                                <input name="true-false-answer" type="radio" value="1" class="custom-control-input" @if($question->choice_id == 1 ) checked @endif>
                                <span class="custom-control-label">Select if answer is <strong>TRUE</strong></span>
                            </label>
                            <label class="custom-control custom-radio">
                                <input name="true-false-answer" type="radio" value="2" class="custom-control-input" @if($question->choice_id == 2 ) checked @endif>
                                <span class="custom-control-label">Select if answer is <strong>FALSE</strong></span>
                            </label>
                        </div>
                    </div>
                </div>

                @endif
                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Status</label>
                    <div class="col-sm-10">
                        <label class="switcher switcher-lg switcher-success">
                            <input type="checkbox" name="status" value="1" class="switcher-input" checked="">
                            <span class="switcher-indicator">
                            <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                            <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                        </span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <a href = "{{route('backend.questions.index')}}" class="btn btn-danger mr-2">Cancel</a>
                        <button type="submit" class="btn btn-primary" id="submitQuestion">Submit</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
@endsection
@section('scripts')
    <style>
        .error{
            color: red;
            display: none;
        }
    </style>
    <script>
        var option = '';
        var category_id = "{{old('institute_type')}}";
        var school_id = "{{old('school_name')}}";
        var course_id = "{{old('course_name')}}";
        var class_id = "{{old('class_name')}}";
        var subject_id = "{{old('subject_name')}}";
    </script>
    <script>
        $(document).ready(function () {
            $("#institute_type").on("change", function () {
                var category_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#institution_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.category.schools") }}',
                    data: {'category': category_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#school_name").html(data.schools);
                        if(school_id){
                            $("#school_name").val(school_id).trigger('change');
                        }
                    }
                });
            });

            $("#school_name").on("change", function () {
                var school_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#school_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courses") }}',
                    data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#course_name").html(data);
                        if(course_id){
                            $("#course_name").val(course_id).trigger('change');
                        }
                    }
                });
            });

            $("#course_name").on("change", function () {
                var course_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#course_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courseclasses") }}',
                    data: {'course_id': course_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#class_name").html(data);
                        if(class_id){
                            $("#class_name").val(class_id).trigger('change');
                        }
                    }
                });
            });

            $("#class_name").on("change", function () {
                var class_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#class_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.classsubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#subject_name").html(data);
                        if(subject_id){
                            $("#subject_name").val(subject_id).trigger('change');
                        }
                    }
                });
            });

            $('#subject_name').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();
            })

            if(category_id){
                $("#institute_type").val(category_id).trigger('change');
            }

            $('#type').on('change',function () {
                var type = $(this).val();
                $(this).removeClass('is-invalid');
                $('#type_error').fadeOut();
                if(type == 'multi-choice'){
                    $('#true-false').fadeOut(500, function () {
                        $('#multi-choice').fadeIn(500);
                    })
                }
                else{
                    $('#multi-choice').fadeOut(500, function () {
                        $('#true-false').fadeIn(500);
                    })
                }
            })

            $('#question').on('keyup', function () {
                $('#question_error').fadeOut();
                $(this).removeClass('is-invalid');
            })

            $('#answer1').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#1_error').fadeOut();
            })
            $('#answer2').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#2_error').fadeOut();
            })
            $('#answer3').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#3_error').fadeOut();
            })
            $('#answer4').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#4_error').fadeOut();
            })

            $('input[type=checkbox]').on('change', function () {
                if($(this).prop('checked') == true){
                    option = $(this).val();
                }
                else{
                    option = '';
                }
                $('input[type=checkbox]').removeClass('is-invalid');
            });

            {{--$('#submitQuestion').on('click', function () {--}}
            {{--    var institution = $('#institute_type').val();--}}
            {{--    var school = $('#school_name').val();--}}
            {{--    var course = $('#course_name').val();--}}
            {{--    var class_name = $('#class_name').val();--}}
            {{--    var subject = $('#subject_name').val();--}}
            {{--    var question = $('#question').val();--}}
            {{--    var question_type = $('#type').val();--}}
            {{--    var num_of_options = 0;--}}
            {{--    $('.q_input').removeClass('is-invalid');--}}
            {{--    $('.error').fadeOut();--}}

            {{--    var error = false;--}}
            {{--    if(!institution){--}}
            {{--        $('#institution_error').fadeIn(500);--}}
            {{--        $('#institute_type').addClass('is-invalid');--}}
            {{--        error = true;--}}
            {{--    }--}}
            {{--    if(!school){--}}
            {{--        $('#school_error').fadeIn(500);--}}
            {{--        $('#school_name').addClass('is-invalid');--}}
            {{--        error = true;--}}
            {{--    }--}}
            {{--    if(!course){--}}
            {{--        $('#course_error').fadeIn(500);--}}
            {{--        $('#course_name').addClass('is-invalid');--}}
            {{--        error = true;--}}
            {{--    }--}}
            {{--    if(!class_name){--}}
            {{--        $('#class_error').fadeIn(500);--}}
            {{--        $('#class_name').addClass('is-invalid');--}}
            {{--        error = true;--}}
            {{--    }--}}
            {{--    if(!subject){--}}
            {{--        $('#subject_error').fadeIn(500);--}}
            {{--        $('#subject_name').addClass('is-invalid');--}}
            {{--        error = true;--}}
            {{--    }--}}
            {{--    if(!question || question.trim() == ''){--}}
            {{--        $('#question_error').fadeIn(500);--}}
            {{--        $('#question').addClass('is-invalid');--}}
            {{--        error = true;--}}
            {{--    }--}}

            {{--    if(!question_type){--}}
            {{--        $('#type_error').fadeIn(500);--}}
            {{--        $('#type').addClass('is-invalid');--}}
            {{--        error = true;--}}
            {{--    }--}}


            {{--    if(question_type == 'multi-choice'){--}}
            {{--        var option1 = $('#answer1').val(), option2 = $('#answer2').val();--}}
            {{--        var option3 = $('#answer3').val(), option4 = $('#answer4').val();--}}
            {{--        num_of_options = 4;--}}

            {{--        if(!option1 || option1.trim() == ''){--}}
            {{--            $('#1_error').fadeIn(500);--}}
            {{--            $('#answer1').addClass('is-invalid');--}}
            {{--            error = true;--}}
            {{--        }--}}
            {{--        if(!option2 || option2.trim() == ''){--}}
            {{--            $('#2_error').fadeIn(500);--}}
            {{--            $('#answer2').addClass('is-invalid');--}}
            {{--            error = true;--}}
            {{--        }--}}
            {{--        if(!option3 || option3.trim() == ''){--}}
            {{--            $('#3_error').fadeIn(500);--}}
            {{--            $('#answer3').addClass('is-invalid');--}}
            {{--            error = true;--}}
            {{--        }--}}
            {{--        if(!option4 || option4.trim() == ''){--}}
            {{--            $('#4_error').fadeIn(500);--}}
            {{--            $('#answer4').addClass('is-invalid');--}}
            {{--            error = true;--}}
            {{--        }--}}

            {{--        var err = false;--}}
            {{--        $('input[name=checkAnswer]').each(function () {--}}
            {{--            var checked = $(this).prop('checked');--}}
            {{--            if(!checked){--}}
            {{--                err = true--}}
            {{--            }--}}
            {{--            else {--}}
            {{--                err = false--}}
            {{--                return false;--}}
            {{--            }--}}
            {{--        })--}}
            {{--        if(err == true){--}}
            {{--            $('input[name=checkAnswer]').addClass('is-invalid');--}}
            {{--            error = true;--}}
            {{--        }--}}

            {{--    }--}}
            {{--    else{--}}
            {{--        option = $('input[name=true-false-answer]:checked').val();--}}
            {{--        num_of_options = 2;--}}
            {{--    }--}}

            {{--    if(!error){--}}
            {{--        var options = [];--}}
            {{--        if(question_type == 'multi-choice'){--}}
            {{--            options = [option1, option2, option3, option4]--}}
            {{--        }--}}
            {{--        else {--}}
            {{--            options = ['True', 'False']--}}
            {{--        }--}}
            {{--        var answer = option;--}}
            {{--        var data = {type: question_type, question: question, num_of_options: num_of_options, active_status: 1, school_id: school, course_id: course, class_id: class_name, subject_id: subject, answer: answer, options: options}--}}
            {{--        console.log(data);--}}
            {{--        var errorDiv = $('#errorDiv'), btn = $('#submitQuestion'), btnHtml = 'Submit';--}}
            {{--        $.ajax({--}}
            {{--            url: '{{URL('api/add-question')}}',--}}
            {{--            type: 'POST',--}}
            {{--            data: data,--}}
            {{--            beforeSend: function () {--}}

            {{--                errorDiv.fadeOut().html('');--}}
            {{--                btn.html('<span class="spinner-border" role="status" aria-hidden="true"></span> &nbsp; submitting...')--}}

            {{--            },--}}
            {{--            success: function (data) {--}}
            {{--                console.log(data);--}}
            {{--                if(data.statusCode == 200){--}}
            {{--                    var d = data.data;--}}

            {{--                    errorDiv.fadeIn(1000, function () {--}}
            {{--                        errorDiv.html('<div class="alert alert-dark-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Question added successfully</div>');--}}
            {{--                        $('#createQuestion').trigger('reset');--}}
            {{--                        window.location = d.url;--}}
            {{--                    })--}}
            {{--                }--}}
            {{--                else{--}}
            {{--                    errorDiv.fadeIn(1000, function () {--}}
            {{--                        errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Try again later</div>')--}}
            {{--                    })--}}
            {{--                }--}}

            {{--            },--}}
            {{--            complete: function () {--}}
            {{--                btn.html(btnHtml);--}}
            {{--            },--}}
            {{--            error: function (error) {--}}
            {{--                errorDiv.fadeIn(1000, function () {--}}
            {{--                    errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Kindly try again later</div>')--}}
            {{--                })--}}
            {{--            }--}}
            {{--        })--}}
            {{--    }--}}

            {{--})--}}
        });
    </script>
    <script>
        function ckChange(ckType){
            var ckName = document.getElementsByName(ckType.name);
            var checked = document.getElementById(ckType.id);

            if (checked.checked) {

                for(var i=0; i < ckName.length; i++){

                    if(!ckName[i].checked){
                        ckName[i].disabled = true;
                    }else{
                        ckName[i].disabled = false;
                    }
                }
            }
            else {
                for(var i=0; i < ckName.length; i++){
                    ckName[i].disabled = false;
                }
            }
        }
    </script>
@stop
