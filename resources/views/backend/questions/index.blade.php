@extends('backend.layouts.layout-2')

@section('scripts')
<script type="text/javascript">
    $(function () {
        $('#question-list').dataTable(
            {
                "columns": [
                    null,
                    null,
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false },
                    { "orderable": false }
                ],
                initComplete: function () {
                    this.api().columns([3]).every( function () {
                        var column = this;
                        var select = $('<select class="custom-select"><option value="">All</option></select>')
                            .appendTo( $(column.header()) )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            }
        );



    });
</script>
@endsection
@section('content')
    @includeif('backend.message')

    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Questions Bank</div>
        <a href="{{route('backend.questions.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Question</a>
    </h4>

    <div class="card">
        <div class="card-datatable table-responsive">
            <table id="question-list" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="min-width: 8rem" class="align-top">Question</th>
                        <th style="min-width: 8rem" class="align-top">
                            School Name
                            <select name="school" id="school" class="custom-select" required>
                                <option value="" selected="">All</option>
                                @foreach($schools as $id => $type)
                                    <option value="{{$type}}" data-id="{{$id}}">{{$type}}</option>
                                @endforeach
                            </select>
                        </th>
                        <th class="align-top">Subject</th>
                        <th class="align-top">Type</th>

                        <th class="align-top">Created By</th>

                        <th class="align-top">Status</th>
                        <th class="align-top">Action</th>
                    </tr>
                </thead>

                <tbody>
                @if($questionsbank)

                    @foreach($questionsbank as $question)
                    <tr>
                        <td>{{$question->question}}</td>
                        <td>{{$question->school_details($question->school_id)}}</td>
                        <td>{{$question->subject_details($question->subject_id)}}</td>
                        <td>{{$question->type}}</td>
                        <td>Admin</td>
                        <td class='text-align align-middle '>
                            <button class="btn {{$question->active_status ? 'btn-success':'btn-danger'}}">{{$question->active_status ? 'Active':'Disabled'}}
                            </button>
                        </td>
                        <td class='form-inline align-middle'>
                            <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.questions.edit', $question->id)}}" class="btn btn-primary btn-xs md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>
{{--                            @role('admin')--}}
                            <form method="POST" action="{{route('backend.questions.destroy', $question->id)}}" style="display: inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

                            </form>
{{--                            @endrole--}}
                            <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.questions.show', $question->id)}}" class="btn btn-warning btn-xs md-btn-flat article-tooltip" title="View question details"><i class="ion ion-md-eye"></i></a>

                        </td>
                    </tr>
                @endforeach
                @else
                    <tr>
                        <td colspan="6">
                            <div class="alert alert-info">
                                <p class="text-center">No Records Found!</p>
                            </div>
                        </td>
                    </tr>
                @endif
                </tbody>

            </table>
        </div>
    </div>

@endsection
