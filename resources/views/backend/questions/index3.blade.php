@extends('backend.layouts.layout-2')

@section('scripts')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<script>

    let table = $('#questions_bank_table').DataTable({
        pageLength: 10,
        responsive: true,
        processing: true,
        serverSide: true,
        ordering  : false,
        language: {
            'loadingRecords': '&nbsp;',
            'processing': 'Loading...'
        },
        ajax: "{{route("backend.questions.all-questions")}}",
        columns: [
            { data: "question",name:'question',searchable:true,orderable: true},
            // { data: "school_name",name:'school_name' },
            { data: "subject",name:'subject', searchable: true },
            { data : 'type', name: 'type',searchable: false},
            { data : 'created_by', name: 'created_by', searchable: true},
            { data : 'status', name: 'status',searchable: false},
            { data : 'action', name: 'action', searchable: false},
        ],
        "initComplete": function( settings, json ) {

        },
    });

    table.on( 'draw', function () {
        $("td").css("cursor","pointer")
    });

    const question_modal_update = $("#questionModalUpdate");
    const user_add_modal = $("#userModal");

    $(document).on("click","#questions_bank_table td:not(:last-child)",function(){
        let data = table.row(this).data();
        console.log(data)
        form_id = data.id;
    });

    $(document).on("click","#ques td:last-child",function(){
        let status_data = table.row( $(this).parents('tr') ).data();
        let id= status_data.id;
        const base_url = "{{url('')}}"
    });

    $("#update-switcher").on("click",function (){
        console.log($(this).prop("checked"))
    })

    function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
    }

</script>

@endsection
@section('content')
    @includeif('backend.message')

    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Questions Bank</div>
        <a href="{{route('backend.questions.create')}}" class="btn btn-primary rounded-pill d-block"><span class="ion ion-md-add"></span>&nbsp;Create Question</a>
    </h4>

    <div class="card">
        <div class="table-responsive">
            <table id="questions_bank_table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="min-width: 8rem" class="align-top">Question</th>
{{--                        <th style="min-width: 8rem" class="align-top">School Name</th>--}}
                        <th class="align-top">Subject</th>
                        <th class="align-top">Type</th>
                        <th class="align-top">Created By</th>
                        <th class="align-top">Status</th>
                        <th class="align-top">Action</th>
                    </tr>
                </thead>

                <tbody>

                </tbody>

            </table>
        </div>
    </div>

    @include("backend.questions.partials.modals.question_update_modal")
@endsection
