<div class="modal fade" id="add-question-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">Add Question </h3>
            </div>
            <form action="{{route("backend.past-questions-group.edit")}}" id="question-modal-form">

                <div class="modal-body">

                    <div class="form-group row">
                        <label for="question" class="col-form-label col-sm-2 text-sm-right">Question</label>
                        <div class="col-sm-10">
                            <textarea class="form-control q_input summernote" placeholder="Enter question" id="question_text" required name="question"></textarea>
                            <div class="error" id="question_error">
                                Enter question
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Answer Type</label>
                        <div class="col-sm-10">
                            <select name="type" id="type" class="custom-select q_input" required>
                                <option value="" selected disabled>Select Answer Type</option>
                                <option value="multi-choice" >Multiple Choice</option>
                                <option value="true-false" >True or False</option>
                            </select>
                            <div class="error" id="type_error">
                                Select answer type
                            </div>
                        </div>
                    </div>

                    <div id="multi-choice" style="display: none">
                        <div class="row">
                            <label class="col-form-label col-sm-2 text-sm-right"></label>
                            <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
                        </div>

                        <div class="option" id="option">
                            <div class="form-group row ">
                                <label for="answer" class="col-form-label col-sm-2 text-sm-right">Option 1</label>
                                <div class="col-sm-10">
                                    <input type="text" placeholder="Option 1" class="form-control answer"
                                           name="answer1" id="answer1">
                                    <div class="error" id="error">
                                        Enter first option
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="answer1" class="col-form-label col-sm-2 text-sm-right"></label>
                                <div class="col-sm-10">
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" name="checkAnswer" value="1" id="checkAnswer1" class="custom-control-input">
                                        <span class="custom-control-label">Check if this is the valid answer</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div style="margin: auto; width: 150px">
                            <button type="button" id="add-new-option" class="btn btn-success">Add new option</button>
                        </div>

                    </div>

                    <div id="true-false" style="display: none">
                        <div class="row">
                            <label class="col-form-label col-sm-2 text-sm-right"></label>
                            <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-sm-2 text-sm-right">Options</label>

                            <div class="custom-controls-stacked col-sm-10">
                                <label class="custom-control custom-radio">
                                    <input name="true-false-answer" type="radio" value="1" class="custom-control-input true-false-answer" checked>
                                    <span class="custom-control-label">Select if answer is <strong>TRUE</strong></span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input name="true-false-answer" type="radio" value="2" class="custom-control-input true-false-answer">
                                    <span class="custom-control-label">Select if answer is <strong>FALSE</strong></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Status</label>
                        <div class="col-sm-10">
                            <label class="switcher switcher-lg switcher-success">
                                <input type="checkbox" name="status" value="1" class="switcher-input" checked="">
                                <span class="switcher-indicator">
                            <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                            <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                        </span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group row" id="year-row" style="display: none">
                        <label for="year" class="col-form-label col-sm-2 text-sm-right">Year</label>
                        <div class="col-sm-10">
                            <input type="number" name="year" id="year" min="1900" max="2099" step="1" class="form-control" placeholder="2016"  />
                            <div class="error" id="year_error">
                                Year
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                        </div>

                        <div class="btn-group" role="group">
                            <button type="button" id="add-selected-question" class="btn btn-success btn-hover-green add-selected-question" data-action="add" role="button">Add question</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
