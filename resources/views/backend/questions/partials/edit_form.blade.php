@php
    $add_questions = Request::query("add-address") == "yes";
@endphp
<form method = "post">
    @csrf
    <div id="errorDiv">
        <!-- error will be shown here -->
    </div>

    <div class="form-group row">
        <label for="answer1" class="col-form-label col-sm-2 text-sm-right">Group Name</label>
        <div class="col-sm-10">
            <input type="text" name="group_name" value="{{$past_question_group->group_name}}" id="group_name"
                   placeholder="Group name" class="form-control"  required {{$add_questions ? "disabled" : ""}}>
            <div class="error" id="q_name_error">
                {{--                            Enter group name--}}
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label for="answer1" class="col-form-label col-sm-2 text-sm-right">Month</label>
        <div class="col-sm-10">
            <select class="custom-select q_input" name="month" required {{$add_questions ? "disabled" : ""}}>

                <option>{{$past_question_group->month_name}}</option>
                @foreach(DB::table("months")->get() as $month)
                    <option value="{{$month->id}}">{{$month->month_name}}</option>
                @endforeach
            </select>

            <div class="error" id="month_error">
                Enter month
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Class</label>
        <div class="col-sm-10">
            <select name="class_id" id="class_name" class="custom-select q_input" required {{$add_questions ? "disabled" : ""}}>

                @php
                    $classes = \App\Models\SchoolCategory::whereIn("id",["20","21"])->selectRaw("name,id")->get();
                @endphp
                @foreach($classes as $class)
                    {{--                            <option value="" selected="" disabled="" class="d-none">Select Class</option>--}}
                    <option value="{{$class->id}}">{{$class->name}}</option>
                @endforeach

{{--                @foreach(\App\Models\Classes::all() as $class)--}}
{{--                    @php--}}
{{--                        $course = \App\Models\Course::find($class->course_id)--}}
{{--                    @endphp--}}
{{--                    <option value="{{$class->id}}">{{$class->class_name}} ({{ $course ? $course->name : "" }}) </option>--}}
{{--                @endforeach--}}

                <option value="" selected="" disabled="" class="d-none">Choose Class</option>
            </select>
            <div class="error" id="class_error">
                Select class
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Subject</label>
        <div class="col-sm-10">
            <select name="subject_id" id="subject_name" class="custom-select q_input" required {{$add_questions ? "disabled" : ""}}>

                @foreach($subjects as $subject)
                    <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                @endforeach

                <option value="" selected="" disabled="" class="d-none">Select Subject</option>
            </select>
            <div class="error" id="subject_error">
                Select subject
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right" >Year</label>
        <div class="col-sm-10">

            <input type="number" name="year" class="custom-select q_input" min="1900" max="2099"
                   step="1" value="{{$past_question_group->year}}" {{$add_questions ? "disabled" : ""}} />
            <div class="error" id="year_error">
                Select year
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Exam Duration(minutes)</label>
        <div class="col-sm-10">
            <input type="number" name="alloted_time_in_minutes" class="custom-select q_input" step="1"
                   required value="{{$past_question_group->alloted_time_in_minutes}}" {{$add_questions ? "disabled" : ""}} />
            <div class="error" id="duration_error">
                Type Duration
            </div>
        </div>
    </div>

    {{--    <div class="form-group row">--}}
{{--        <label class="col-form-label col-sm-2 text-sm-right">Is this group in sections?</label>--}}
{{--        <div class="col-sm-10">--}}
{{--            <input type="checkbox" name="has_section" id="group_has_sections"--}}
{{--                   class="checkbox-input" {{$past_question_group->has_section == 1 ? "checked" : ""}} style="height: 20px; width: 20px; margin-top: 8px;"--}}
{{--                {{$add_questions ? "disabled" : ""}}/>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div id="section_row" style="display: none;">--}}
{{--        <div class="form-group row">--}}
{{--            <label class="col-form-label col-sm-2 text-sm-right">Section naming type: </label>--}}
{{--            <div class="col-sm-10">--}}
{{--                <select name="section_naming_type" id="section_naming_type" class="custom-select q_input" {{$add_questions ? "disabled" : ""}}--}}
{{--                required>--}}
{{--                    <option value="alphabet">Alphabet</option>--}}
{{--                    <option value="number">Number</option>--}}
{{--                    <option value="roman_numerals">Roman Numerals</option>--}}
{{--                </select>--}}
{{--                <div class="error" id="class_error">--}}
{{--                    Select type--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group row">--}}
{{--            <label class="col-form-label col-sm-2 text-sm-right">How many sections?</label>--}}
{{--            <div class="col-sm-10">--}}
{{--                <input type="number" name="section_counts"--}}
{{--                       id="section_counts" class="custom-select q_input" step="1"--}}
{{--                       value="{{$past_question_group->section_counts}}"  required {{$add_questions ? "disabled" : ""}} />--}}
{{--                <div class="error" id="section_count_error">--}}
{{--                    Type section number--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </div>--}}


    {{--    <div class="form-group row">--}}
{{--        <label for="question" class="col-form-label col-sm-2 text-sm-right">Add Question</label>--}}
{{--        <div class="col-sm-8">--}}
{{--            <select name="question" id="question" class="custom-select q_input" required>--}}
{{--                <option value="" selected="" disabled="" class="d-none">Select Question</option>--}}
{{--            </select>--}}
{{--            <div class="error" id="question_error">--}}
{{--                Add questions--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-sm-2">--}}
{{--            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-question-modal" id="addQuestion"><span class="fa fa-plus-circle"></span> &nbsp; Add</button>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">

            <div id="accordion">

                @php

                    function numberToRomanRepresentation($number) {
                        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50,
                        'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
                        $returnValue = '';
                        while ($number > 0) {
                            foreach ($map as $roman => $int) {
                                if($number >= $int) {
                                    $number -= $int;
                                    $returnValue .= $roman;
                                    break;
                                }
                            }
                        }
                        return $returnValue;
                    }

                    $alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
                @endphp

                @if($past_question_group->section_counts > 1)
                @foreach(range(1, $past_question_group->section_counts) as $y)
                    <div class="card mb-2">
                        <div class="card-header">
                            <a class="text-body" data-toggle="collapse" href="#accordion-{{$y}}">
                                @if($past_question_group->section_naming_type == "alphabet")
                                    Section #{{$alpha[$y-1]}}
                                @elseif($past_question_group->section_naming_type == "roman_numerals")
                                    Section #{{numberToRomanRepresentation($y)}}
                                @else
                                    Section #{{$y}}
                                @endif
                            </a>
                        </div>

                        <div id="accordion-{{$y}}" class="collapse" data-parent="#accordion" >
                            <div class="card-body">
                                <button  type="button" class="btn btn-primary add-question-bank-modal"
                                         data-toggle="modal" data-section="{{$y}}"
                                         data-target="#add-question-modal" id="addQuestion">
                                    <span class="fa fa-plus-circle"></span> &nbsp; Add Question from bank
                                </button>

                                <button type="button" class="btn btn-success add-new-question-modal"
                                        data-toggle="modal" data-section="{{$y}}"
                                        data-target="#add-question-modal" id="addQuestion" >
                                    <span class="fa fa-plus"></span> &nbsp; Add new Question</button>

                                <br>
                                <br>

                                @foreach($past_question_group->section_info as $info)
                                    @if($info["section"] == $y)
                                        <div class="alert alert-dark-primary alert-dismissible fade show">
                                            <button type="button" data-id="{{$info["id"]}}" class="close rm"
                                                     data-dismiss="alert">×
                                                </button> {{$info["text"]}} </div>
                                    @endif
                                @endforeach

{{--                                {!! json_encode($past_question_group->section_info) !!}--}}
{{--                                <div id="questionDiv">--}}

{{--                                </div>--}}


                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            @else

                <div id="questionDiv">

                </div>

                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                data-target="#add-question-modal" id="addQuestion">
                            <span class="fa fa-plus"></span> &nbsp; Add new Question
                        </button>
                    </div>
                </div>
            @endif

            <div class="form-group row ml-2">
                    <a href = "{{route('backend.past-questions.index')}}" class="btn btn-danger mr-2">Cancel</a>
                    <button type="button" class="btn btn-primary" id="submitQuestions">Update</button>
            </div>

        </div>


    </div>


</form>
