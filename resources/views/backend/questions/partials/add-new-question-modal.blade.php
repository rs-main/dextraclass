<div class="modal fade" id="add-question-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">Add Question(s) </h3>
            </div>
            <form action="{{route("backend.past-questions-group.edit")}}">

                <div class="modal-body">

                    <div class="form-group">
                        <label for="subject_name">Subject</label>

                        <select name="subject_name" id="subject_name" class="select2-demo form-control" style="width: 100%"
                                data-allow-clear="true" required>
                            @foreach(\App\Models\Subject::all() as $subject)
                            <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Question</label>

                        <select name="question" id="question" class="custom-select q_input" required>
                                        <option value="" selected="" disabled="" class="d-none">Select Question</option>
                        </select>
                        <div class="error" id="question_error">Add questions</div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                        </div>

                        <div class="btn-group" role="group">
                            <button type="button" id="add-selected-question" class="btn btn-success btn-hover-green" data-action="add" role="button">Add question</button>
                        </div>

                        <div class="btn-group" role="group">
                            <button type="button" id="add-all-questions" class="btn btn-secondary btn-hover-green"
                                    data-action="add-all" role="button">Add all questions</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
