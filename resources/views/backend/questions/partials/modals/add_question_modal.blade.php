<div id="userModal" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger print-error-msg" style="display:none">
                    <ul></ul>
                </div>
                <form id="add-user-form" method="post" action="" autocomplete="off">
                    @csrf

                    <input type="text"  name="id" class="form-control id-up" style="display: none;" />

                    <div class="row row-no-gutters">
                        <div class="form-group col-sm-6">
                            <label>First name<span class="text-danger">*</span></label>
                            <input type="text" name="user_fname"
                                   placeholder="first name" class="form-control user_fname" required="required" />
                            <span id='message1'> </span>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Last name<span class="text-danger">*</span></label>
                            <input type="text"  name="user_lname" class="form-control user_lname" placeholder="Last name" required="required" />
                            <span id='message2'> </span>
                        </div>
                    </div>

                    <div class="row row-no-gutters">
                        <div class="form-group col-sm-6">
                            <label>Username<span class="text-danger">*</span></label>
                            <input type="text"  name="username" class="form-control user_name"
                                   placeholder="Username" required="required" />
                            <span id='check'> </span>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="role">Role <span class="text-danger">*</span></label>
{{--                            <div class="col-lg-20 col-md-20 col-sm-20">--}}

{{--                                <select style="width:100%" class="form-control roles" name="role_id[]" id="role_id" required multiple>--}}
{{--                                    <option value="" selected="" disabled>Select role</option>--}}
{{--                                    @foreach($roles as $role)--}}
{{--                                        <option value="{{$role->id}}">{{$role->name}}</option>--}}
{{--                                    @endforeach--}}
{{--                                </select>--}}
{{--                            </div>--}}
                        </div>
                    </div>

                    <div class="row row-no-gutters">
                        <div class="form-group col-sm-6">
                            <label for="password">Password <span class="text-danger">*</span></label>
                            <input type="password" autocomplete="off" name="password" id="add-password" class="form-control" placeholder="Password" required="required" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="confirm_password">Confirm Password <span class="text-danger">*</span></label>
                            <input type="password" id="confirmation_password" class="form-control"
                                   placeholder="Confirm Password" required="required" name="password_confirmation" />
                            <span id='message'> </span>
                        </div>
                    </div>

                    <div class="row row-no-gutters">
                        <div class="form-group  col-sm-6">
                            <label for="manager">team leader</label>
                            <select style="width:100%" class="form-control kt-select2 manager-up" id="team-leader"
                                    name="team_leader_id" required>
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6" >
                            <label for="business-up">Business unit</label>
                            <select style="width:100%" class="form-control business-up" required name="business_unit_id" id="business_unit_id" disabled>
                            </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="status-up" style="position: absolute; top:30px ">Status</label>
                            <span class='kt-switch kt-switch--icon ' style="position: relative; top: 25px; left: 60px">
                            <label>
                            <input type='checkbox' name='status' class="switcher" id='add-switcher' />
                              <span></span>
                            </label>
                            </span>

                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Add User</button>
                        <button type="reset" class="btn btn-default" onclick="this.form.reset();" data-dismiss="modal">Close</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
