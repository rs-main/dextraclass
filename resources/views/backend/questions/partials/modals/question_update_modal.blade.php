<div id="questionModalUpdate" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
{{--                <div class="alert alert-danger print-error-msg" style="display:none">--}}
{{--                    <ul></ul>--}}
{{--                </div>--}}
                <form id="user-update-form" method="post" autocomplete="off">
                    @csrf
                    <input type="text" id="id-up" name="id" class="form-control" style="display: none;" />

                    <div class="row row-no-gutters">
                        <div class="form-group col-sm-6">
                            <label>First name<span class="text-danger">*</span></label>
                            <input type="text" id="user_fname" name="user_fname"
                                   placeholder="first name" class="form-control" required="required" />
{{--                            <span id='message1'> </span>--}}
                            <small class="user_fname" style="color: red;"></small>

                        </div>

                        <div class="form-group col-sm-6">
                            <label>Last name<span class="text-danger">*</span></label>
                            <input type="text" id="user_lname" name="user_lname" class="form-control" placeholder="Last name" required="required" />
{{--                            <span id='message2'> </span>--}}
                            <small class="user_lname" style="color: red;"></small>

                        </div>
                    </div>
                    <div class="row row-no-gutters">
                        <div class="form-group col-sm-6">
                            <label>Username<span class="text-danger">*</span></label>
                            <input type="text" id="user_name" name="username" class="form-control"
                                   placeholder="Username" required="required" />
                            <span id='check'>
                                <small class="username" style="color: red;"></small>
                            </span>

                        </div>
                        <div class="form-group col-sm-6">
                            <label for="role">Role <span class="text-danger">*</span></label>
                            <div class="col-lg-20 col-md-20 col-sm-20">

                                <select style="width:100%"  class="form-control kt-select2 roles" id="roles-up" name="role_id[]"
                                        required multiple="multiple">
{{--                                    @foreach($roles as $role)--}}
{{--                                        <option value="{{$role->id}}">{{$role->name}}</option>--}}
{{--                                    @endforeach--}}
                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="row row-no-gutters">
                        <div class="form-group col-sm-6">
                            <label for="password">Password</label>
                            <input type="password" autocomplete="off" name="password" id="password"
                                   class="form-control" placeholder="Password" />
                            <small class="password" style="color: red;"></small>

                        </div>

                        <div class="form-group col-sm-6">
                            <label for="confirm_password">Confirm Password</label>
                            <input type="password" id="confirm_password" class="form-control"
                                   placeholder="Confirm Password"  name="password_confirmation" />
{{--                            <span id='message'> </span>--}}
                            <small class="password_confirmation" style="color: red;"></small>

                        </div>
                    </div>

                    <div class="row row-no-gutters">
                        <div class="form-group  col-sm-6">
                            <label for="manager">Team leader  </label>
                            <select style="width:100%" class="form-control kt-select2 manager-up" id="manager-up" name="team_leader_id" required></select>
                            <small class="team_leader_id" style="color: red;"></small>

                        </div>

                        <div class="form-group col-sm-6">
                            <label for="departments">Department</label>
                            <select style="width:100%" class="form-control departments-up" name="department_id" id="departments-up" >
                                <option value="" selected="" disabled>Select Department</option>
{{--                                @foreach($departments as $key => $department)--}}
{{--                                    <option value="{{$key}}">{{$department}}</option>--}}
{{--                                @endforeach--}}
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6" >
                            <label for="business-up">Business unit</label>
                            <select style="width:100%; height: 50px;" class="form-control business-up" name="business_unit_id" id="business-up" required>
                            </select>

                            <small class="business_unit_id" style="color: red;"></small>

                        </div>

                        <div class="form-group col-sm-6">
                            <label for="status-up" style="position: absolute; top:30px ">Status</label>
                            <span class='kt-switch kt-switch--icon ' style="position: relative; top: 25px; left: 60px">
                            <label>
                            <input type='checkbox' name='status' class="switcher" id='update-switcher' />
                              <span></span>
                            </label>
                            </span>

                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                        <button type="reset" class="btn btn-default" onclick="this.form.reset();" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
