<section class="search-sec">
    <div class="container">
        <form action="" method="get" novalidate="novalidate">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                            <input type="text" style="width: 132%"
                                   class="form-control search-slt"
                                   name="question"
                                   placeholder="Search question" id="search" value="{{Request::query("question")}}">
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                            <select name="subject" class="form-control search-slt" id="search_subject">
                                <option value="">Choose subject</option>
                            </select>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                            <button type="submit" class="btn btn-danger wrn-btn">Search</button>
                        </div>


                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
