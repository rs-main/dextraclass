<div class="d-inline-flex">

    <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.questions.show', $question_bank->id)}}" class="btn btn-warning btn-xs md-btn-flat article-tooltip" title="View question details"><i class="ion ion-md-eye"></i></a>

    <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.questions.edit', $question_bank->id)}}" class="btn btn-primary btn-xs md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>

{{--    @role('admin')--}}
    <form method="POST" action="{{route('backend.questions.destroy', $question_bank->id)}}" style="display: inline-block;">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}

        <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

    </form>
    {{--                            @endrole--}}

</div>
