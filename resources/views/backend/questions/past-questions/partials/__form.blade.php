<form method = "post" id="createQuestion" enctype="multipart/form-data">
    @csrf
    <div id="errorDiv">
        <!-- error will be shown here -->
    </div>
    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Institution Type</label>
        <div class="col-sm-9">
            <select name="institute_type" id="institute_type" class="custom-select q_input" required>
                <option value="" selected="" disabled="" class="d-none">Select Institution Type</option>
                @foreach($institutes as $id => $type)
                    <option value="{{$id}}" >{{$type}}</option>
                @endforeach
            </select>
            <div class="error" id="institution_error">
                Select institution
            </div>
        </div>

        <div class="col-sm-1">
            <i class="fas fa-plus-circle fa-2x" style="color: green; margin-top: 2px;"></i>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">School</label>
        <div class="col-sm-9">
            <select name="school_name" id="school_name" class="custom-select q_input" required>
                <option value="" selected="" disabled="" class="d-none">Select School</option>
            </select>
            <div class="error" id="school_error">
                Select school
            </div>
        </div>

        <div class="col-sm-1">
            <i class="fas fa-plus-circle fa-2x" style="color: green; margin-top: 2px;"></i>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Course</label>

        <div class="col-sm-9">
            <select name="course_name" id="course_name" class="custom-select q_input" required>
                <option value="" selected="" disabled="" class="d-none">Select Course</option>
            </select>
            <div class="error" id="course_error">
                Select course
            </div>
        </div>

        <div class="col-sm-1">
            <i class="fas fa-plus-circle fa-2x" style="color: green; margin-top: 2px;"></i>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Class</label>
        <div class="col-sm-9">
            <select name="class_name" id="class_name" class="custom-select q_input" required>
                <option value="" selected="" disabled="" class="d-none">Choose Class</option>
            </select>
            <div class="error" id="class_error">
                Select class
            </div>
        </div>

        <div class="col-sm-1">
            <i class="fas fa-plus-circle fa-2x" style="color: green; margin-top: 2px;"></i>
        </div>

    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Subject</label>
        <div class="col-sm-9">
            <select name="subject_name" id="subject_name" class="custom-select q_input" required>
                <option value="" selected="" disabled="" class="d-none">Select Subject</option>
            </select>
            <div class="error" id="subject_error">
                Select subject
            </div>
        </div>

        <div class="col-sm-1">
            <i class="fas fa-plus-circle fa-2x" style="color: green; margin-top: 2px;"></i>
        </div>

    </div>

    <div class="form-group row">
        <label for="question" class="col-form-label col-sm-2 text-sm-right">Question</label>
        <div class="col-sm-10">
            <textarea class="form-control q_input" name="question" id="question" placeholder="Enter question" required></textarea>
            <div class="error" id="question_error">
                Enter question
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Question Type</label>
        <div class="col-sm-10">
            <select name="type" id="question-type" class="custom-select q_input" required>
                <option value="" selected disabled>Select Question Type</option>
                <option value="text" >Text</option>
                <option value="image" >Image</option>
            </select>
            <div class="error" id="type_error">
                Select question type
            </div>
        </div>
    </div>

    <div class="form-group row" id="image-question-row">
        <label class="col-form-label col-sm-2 text-sm-right">Upload Image as Question</label>
        <div class="col-sm-10">
            <input type="file" id="image_question" name="image_question" title="Add Image">
            <small class="form-text mb-4">
                .jpg .png .bmp  |  Size max &gt;= 2mb |  @ 212px by 150px<br>
            </small>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Answer Type</label>
        <div class="col-sm-10">
            <select name="type" id="type" class="custom-select q_input" required>
                <option value="" selected disabled>Select Answer Type</option>
                <option value="multi-choice" >Multiple Choice</option>
                <option value="true-false" >True or False</option>
            </select>
            <div class="error" id="type_error">
                Select question type
            </div>
        </div>
    </div>

    <div id="multi-choice" style="display: none">
        <div class="row">
            <label class="col-form-label col-sm-2 text-sm-right"></label>
            <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
        </div>

        <div class="form-group row">
            <label for="answer1" class="col-form-label col-sm-2 text-sm-right">Option 1</label>
            <div class="col-sm-10">
                <input type="text" name="answer1" id="answer1" placeholder="First Option" class="form-control">
                <div class="error" id="1_error">
                    Enter first option
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="answer1" class="col-form-label col-sm-2 text-sm-right"></label>
            <div class="col-sm-10">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" name="checkAnswer" value="1" id="checkAnswer1" onClick="ckChange(this)" class="custom-control-input">
                    <span class="custom-control-label">Check if this is the valid answer</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="answer2" class="col-form-label col-sm-2 text-sm-right">Option 2</label>
            <div class="col-sm-10">
                <input type="text" name="answer2" id="answer2" placeholder="Second Option" class="form-control">
                <div class="error" id="2_error">
                    Enter second option
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="answer2" class="col-form-label col-sm-2 text-sm-right"></label>
            <div class="col-sm-10">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" name="checkAnswer" value="2" id="checkAnswer2" onClick="ckChange(this)" class="custom-control-input">
                    <span class="custom-control-label">Check if this is the valid answer</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="answer3" class="col-form-label col-sm-2 text-sm-right">Option 3</label>
            <div class="col-sm-10">
                <input type="text" name="answer3" id="answer3" placeholder="Third Option" class="form-control">
                <div class="error" id="3_error">
                    Enter third option
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="answer3" class="col-form-label col-sm-2 text-sm-right"></label>
            <div class="col-sm-10">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" name="checkAnswer" id="checkAnswer3" value="3" onClick="ckChange(this)" class="custom-control-input">
                    <span class="custom-control-label">Check if this is the valid answer</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="answer4" class="col-form-label col-sm-2 text-sm-right">Option 4</label>
            <div class="col-sm-10">
                <input type="text" name="answer4" id="answer4" placeholder="Fourth Option" class="form-control">
                <div class="error" id="4_error">
                    Enter fourth option
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="answer4" class="col-form-label col-sm-2 text-sm-right"></label>
            <div class="col-sm-10">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" name="checkAnswer" id="checkAnswer4" value="4" onClick="ckChange(this)" class="custom-control-input">
                    <span class="custom-control-label">Check if this is the valid answer</span>
                </label>
            </div>
        </div>

    </div>

    <div id="true-false" style="display: none">
        <div class="row">
            <label class="col-form-label col-sm-2 text-sm-right"></label>
            <label class="col-form-label col-sm-10 mt-2 mb-2"><strong>Answers</strong></label>
        </div>

        <div class="form-group row">
            <label class="col-form-label col-sm-2 text-sm-right">Options</label>

            <div class="custom-controls-stacked col-sm-10">
                <label class="custom-control custom-radio">
                    <input name="true-false-answer" type="radio" value="1" class="custom-control-input" checked>
                    <span class="custom-control-label">Select if answer is <strong>TRUE</strong></span>
                </label>
                <label class="custom-control custom-radio">
                    <input name="true-false-answer" type="radio" value="2" class="custom-control-input">
                    <span class="custom-control-label">Select if answer is <strong>FALSE</strong></span>
                </label>
            </div>
        </div>
    </div>

    <div class="form-group row" id="year-row">
        <label for="year" class="col-form-label col-sm-2 text-sm-right">Date</label>
        <div class="col-sm-10">
            {{--            <input type="number" name="year" id="year" min="1900" max="2099" step="1" class="form-control" placeholder="2016"  />--}}
            <input type="date" name="year" id="question_date" class="form-control" placeholder="2016"  />
            <div class="error" id="year_error">
                Year
            </div>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-form-label col-sm-2 text-sm-right">Status</label>
        <div class="col-sm-10">
            <label class="switcher switcher-lg switcher-success">
                <input type="checkbox" name="status" value="1" class="switcher-input" checked="">
                <span class="switcher-indicator">
                            <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                            <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                        </span>
            </label>
        </div>
    </div>

    <div class="form-group row" style="display: none">
        <label class="col-form-label col-sm-2 text-sm-right">Past Question?</label>
        <div class="col-sm-10">
            <label class="switcher switcher-lg switcher-success">
                <input type="checkbox" name="past_question" id="past_question" value="1" class="switcher-input">
                <span class="switcher-indicator">
                            <span class="switcher-yes"><span class="ion ion-md-checkmark"></span></span>
                            <span class="switcher-no"><span class="ion ion-md-close"></span></span>
                        </span>
            </label>
        </div>
        <input type="hidden" name="past-question-type" id="past-question-type" value="past_question"/>
    </div>


    <div class="form-group row">
        <div class="col-sm-10 ml-sm-auto">
            <a href = "{{route('backend.questions.index')}}" class="btn btn-danger mr-2">Cancel</a>
            <button type="submit" class="btn btn-primary" id="submitQuestion">Submit</button>
        </div>
    </div>
</form>
