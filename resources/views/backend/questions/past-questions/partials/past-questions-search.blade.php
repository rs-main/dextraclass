<section class="search-sec">
    <div class="container">
        <form action="" method="get" novalidate="novalidate">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12 p-0">
                            <select class="form-control search-slt" id="exam-year" name="exam-year">
                                <option value="" >Choose Year</option>
                                @foreach($yearCollections as $year)
                                    <option>{{$year}}</option>
                                @endforeach
                            </select>
                            {{--                            <input type="text" class="form-control search-slt" placeholder="Enter School Name" id="school_name">--}}
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 p-0">
                            @php

                                $subject_object = Request::has("subject") && !empty(Request::query("subject")) ? \App\Models\Subject::find(Request::query("subject")) : null;
                            @endphp
                            <select name="subject" class="form-control search-slt" id="exampleFormControlSelect1">
{{--                                <option value=""></option>--}}
                                <option value="{{Request::query("subject")}}">{{Request::has("subject") && !empty(Request::query("subject"))? $subject_object->subject_name: "Choose Subject"}}</option>
{{--                                <option>Select Subject</option>--}}
                            </select>
                        </div>

{{--                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">--}}
{{--                            <select class="form-control search-slt" id="exam-type" name="exam-type">--}}
{{--                                <option value="">Choose Class</option>--}}
{{--                            </select>--}}
{{--                            --}}{{--<input type="text" class="form-control search-slt" placeholder="Enter School Name" id="school_name">--}}
{{--                        </div>--}}

                        <div class="col-lg-4 col-md-4 col-sm-12 p-0">
                            <button type="submit" class="btn btn-danger wrn-btn">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
