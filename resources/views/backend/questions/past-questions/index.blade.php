@extends('backend.layouts.layout-2')

@section('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <style>
    /*search box css start here*/

    .search-sec{
        padding: 2rem;
    }
    .search-slt{
        display: block;
        /*width: 100%;*/
        font-size: 0.875rem;
        line-height: 1.5;
        color: #55595c;
        background-color: #fff;
        background-image: none;
        /*border: 1px solid #ccc;*/
        height: calc(3rem + 2px) !important;
        border-radius:0;
    }
    .wrn-btn{
        width: 100%;
        font-size: 16px;
        font-weight: 400;
        text-transform: capitalize;
        height: calc(3rem + 2px) !important;
        border-radius:0;
    }
    @media (min-width: 992px){
        .search-sec{
            /*position: relative;*/
            /*top: -114px;*/
            background: rgba(26, 70, 104, 0.51);
        }
    }

    @media (max-width: 992px){
        .search-sec{
            background: #1A4668;
        }
    }

    .typeahead,
    .tt-query,
    .tt-hint {
        width: 250px;
        height: 40px;
        /*padding: 8px 12px;*/
        font-size: 17px;
        line-height: 20px;
        border: 2px solid #ccc;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        outline: none;
    }

    .typeahead {
        background-color: #fff;
    }

    .typeahead:focus {
        border: 2px solid #0097cf;
    }

    .tt-query {
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    }

    .tt-hint {
        color: #999
    }

    .tt-menu {
        width: 422px;
        margin: 12px 0;
        padding: 8px 0;
        background-color: #fff;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.2);
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
    }

    .tt-suggestion {
        padding: 3px 20px;
        font-size: 18px;
        line-height: 24px;
        font-family: "Poppins";
        text-transform: lowercase;
    }

    .tt-suggestion:hover {
        cursor: pointer;
        color: #fff;
        background-color: #0097cf;
    }

    .tt-suggestion.tt-cursor {
        color: #fff;
        background-color: #0097cf;
    }

    .tt-suggestion p {
        margin: 0;
    }

    .gist {
        font-size: 14px;
    }

    .empty-message {
        padding: 5px 10px;
        text-align: center;
    }

    .tt-input-group {
        width: 100%;
    }

    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 50px;
        user-select: none;
        -webkit-user-select: none;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #444;
        line-height: 50px;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 46px;
        position: absolute;
        top: 1px;
        right: 1px;
        width: 20px;
    }

</style>

<script type="text/javascript">

    let substringMatcher = function(strings) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            matches = [];
            substrRegex = new RegExp(q, 'i');

            $.each(strings, function(i, string) {
                if (substrRegex.test(string)) {
                    matches.push(string);
                }
            });
            cb(matches);
        };
    };

    // Questions Suggestion engine
    const questions = new Bloodhound({
        datumTokenizer: datum => Bloodhound.tokenizers.whitespace(datum.value),
        queryTokenizer: Bloodhound.tokenizers.whitespace,

        prefetch: "{{url('/admin/type-ahead-questions?q=%QUERY')}}",
        remote: {
            url: "{{url('/admin/type-ahead-questions?q=%QUERY')}}",
            wildcard: '%QUERY',
            filter: questions => $.map(questions, question => ({
                value: question.question,
                id: question.id,
                class: question.class.class_name,
                class_id: question.class.id,
                subject: question.subject.subject_name,
                school: question.school.school_name
            }))
        }
    });

    questions.initialize();

    $('#search').typeahead(null, {
        displayKey: 'value',
        display: 'value',
        source: questions.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                'unable to find any question that matches the current query',
                '</div>'
            ].join('\n'),
            suggestion: Handlebars.compile(
                '<div>' +
                ' <div style="border-bottom: 1px solid #cccccc;">' +
                '<strong>@{{value}}</strong> – @{{class}} – @{{school}}</div> '
            )
        }
    });

    $('#search').on('typeahead:selected', function (e, datum) {
        console.log(datum);
    });

    let subject = $("#exampleFormControlSelect1");

    $("#exampleFormControlSelect1").select2({
        ajax: ({
            url: "/admin/type-ahead-subjects",
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        })
    })


    $("#school-select").select2({
        ajax: {
            url: "/admin/type-ahead-schools",
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        }
    });

</script>
@endsection
@section('content')
    @includeif('backend.message')

    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
        <div>Past Questions Bank</div>
    </h4>

    <div class="align-items-end d-flex justify-content-between mb-4">
        <a href="{{route('backend.past-questions.create')}}" class="btn btn-primary rounded-pill d-block">
            <span class="ion ion-md-add"></span>&nbsp;Create Past Question</a>

        <a href="{{route('backend.past-questions-group.create')}}" class="btn btn-success rounded-pill d-block">
            <span class="ion ion-md-add"></span>&nbsp;Create Past Questions Group</a>
    </div>

{{--    <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">--}}
{{--        <div></div>--}}
{{--    <a href="{{route('backend.past-questions.create')}}" class="btn btn-primary rounded-pill d-block">--}}
{{--        <span class="ion ion-md-add"></span>&nbsp;Create Group</a>--}}
{{--    </h4>--}}

{{--    @include("backend.questions.past-questions.partials.past-questions-search")--}}

    <div class="card">
        <div class="card-datatable table-responsive">
            <table id="question-list" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="align-top">No.</th>
                        <th style="min-width: 8rem" class="align-top">Question</th>
                        <th style="min-width: 8rem" class="align-top">School Name</th>
                        <th class="align-top"><a href="">Subject </a></th>
                        <th class="align-top">Type</th>

                        <th class="align-top">Created By</th>

                        <th class="align-top">Status</th>
                        <th class="align-top">Action</th>
                    </tr>
                </thead>

                <tbody>

                @if($questionsData)

                    @foreach($questionsData as $key => $question)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$question->question}}</td>
                        <td>{{$question->school_details($question->school_id)}}</td>
                        <td>{{$question->subject_details($question->subject_id)}}</td>
                        <td>{{$question->type}}</td>
                        <td>Admin</td>
                        <td class='text-align align-middle '><button class="btn {{$question->active_status ? 'btn-success':'btn-danger'}}">{{$question->active_status ? 'Active':'Disabled'}}</button></td>
                        <td class='form-inline align-middle'>
                            <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.past-questions.edit', $question->id)}}" class="btn btn-primary btn-xs md-btn-flat article-tooltip" title="Edit"><i class="ion ion-md-create"></i></a>
{{--                            @role('admin')--}}
                            <form method="POST" action="{{route('backend.questions.destroy', $question->id)}}" style="display: inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" onclick="return confirm('You are about to delete this record?')" class="btn-danger  btn-xs  md-btn-flat article-tooltip" style="margin-right: 5px; margin-left: 5px;padding-top: 2px; padding-bottom: 2px;" title="Remove"><i class="ion ion-md-close"></i></button>

                            </form>
{{--                            @endrole--}}
                            <a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;" href ="{{route('backend.questions.show', $question->id)}}" class="btn btn-warning btn-xs md-btn-flat article-tooltip" title="View question details"><i class="ion ion-md-eye"></i></a>

                        </td>
                    </tr>
                @endforeach
                @else
                    <tr>
                        <td colspan="6">
                            <div class="alert alert-info">
                                <p class="text-center">No Records Found!</p>
                            </div>
                        </td>
                    </tr>
                @endif
                </tbody>

            </table>

            <div class="text-center">
                <ul class="pagination-custom list-unstyled list-inline">

                    <?php
                    if(isset($paginator)){ ?>
                    {{--{!! $paginator->render() !!}--}}
                    {!! $paginator->appends(
                    ['question'=>Request::get('question'),
                     'subject' => Request::get("subject"),
                     'school' => Request::get("school")
                     ])->render()
                    !!}

                    <?php   }else {?>
                    {!! $questionsData->render() !!}
                    <?php } ?>

                </ul>
            </div>

        </div>
    </div>

@endsection
