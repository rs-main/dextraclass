@extends('backend.layouts.layout-2')

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Questions Bank /</span> Create Past Question (Objectives)
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Create Past Question
        </h6>
        <div class="card-body">
            @includeif('backend.message')

            @include("backend.questions.past-questions.partials.__form")

        </div>

    </div>
@endsection
@section('scripts')
    <style>
        .error{
            color: red;
            display: none;
        }
    </style>
    <script>
        var option = '';
        var category_id = "{{old('institute_type')}}";
        var school_id = "{{old('school_name')}}";
        var course_id = "{{old('course_name')}}";
        var class_id = "{{old('class_name')}}";
        var subject_id = "{{old('subject_name')}}";
    </script>

    <script>
        $(document).ready(function () {
            $("#institute_type").on("change", function () {
                var category_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#institution_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.category.schools") }}',
                    data: {'category': category_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#school_name").html(data.schools);
                        if(school_id){
                            $("#school_name").val(school_id).trigger('change');
                        }
                    }
                });
            });

            $("#school_name").on("change", function () {
                var school_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#school_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courses") }}',
                    data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#course_name").html(data);
                        if(course_id){
                            $("#course_name").val(course_id).trigger('change');
                        }
                    }
                });
            });

            $("#course_name").on("change", function () {
                var course_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#course_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courseclasses") }}',
                    data: {'course_id': course_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#class_name").html(data);
                        if(class_id){
                            $("#class_name").val(class_id).trigger('change');
                        }
                    }
                });
            });

            $("#class_name").on("change", function () {
                var class_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#class_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.classsubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#subject_name").html(data);
                        if(subject_id){
                            $("#subject_name").val(subject_id).trigger('change');
                        }
                    }
                });
            });

            $('#subject_name').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();
            })

            if(category_id){
                $("#institute_type").val(category_id).trigger('change');
            }

            $('#type').on('change',function () {
                var type = $(this).val();
                $(this).removeClass('is-invalid');
                $('#type_error').fadeOut();
                if(type == 'multi-choice'){
                    $('#true-false').fadeOut(500, function () {
                        $('#multi-choice').fadeIn(500);
                    })
                }
                else{
                    $('#multi-choice').fadeOut(500, function () {
                        $('#true-false').fadeIn(500);
                    })
                }
            })

            $('#question').on('keyup', function () {
                $('#question_error').fadeOut();
                $(this).removeClass('is-invalid');
            })

            $('#answer1').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#1_error').fadeOut();
            })
            $('#answer2').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#2_error').fadeOut();
            })
            $('#answer3').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#3_error').fadeOut();
            })
            $('#answer4').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#4_error').fadeOut();
            })

            $('input[type=checkbox]').on('change', function () {
                if($(this).prop('checked') == true){
                    option = $(this).val();
                }
                else{
                    option = '';
                }
                $('input[type=checkbox]').removeClass('is-invalid');
            });

            // $('#submitQuestion').on('submit', function () {
            $('#createQuestion').on('submit', function (e) {

                e.preventDefault();
                var institution = $('#institute_type').val();
                var school = $('#school_name').val();
                var course = $('#course_name').val();
                var class_name = $('#class_name').val();
                var subject = $('#subject_name').val();
                var question = $('#question').val();
                var question_type = $('#type').val();
                // var image = $("input[name=image_question]");

                var past_question_type = $('#past-question-type').val();
                var year          = $('#question_date').val();

                var num_of_options = 0;
                $('.q_input').removeClass('is-invalid');
                $('.error').fadeOut();

                var error = false;
                if(!institution){
                    $('#institution_error').fadeIn(500);
                    $('#institute_type').addClass('is-invalid');
                    error = true;
                }
                if(!school){
                    $('#school_error').fadeIn(500);
                    $('#school_name').addClass('is-invalid');
                    error = true;
                }
                if(!course){
                    $('#course_error').fadeIn(500);
                    $('#course_name').addClass('is-invalid');
                    error = true;
                }
                if(!class_name){
                    $('#class_error').fadeIn(500);
                    $('#class_name').addClass('is-invalid');
                    error = true;
                }
                if(!subject){
                    $('#subject_error').fadeIn(500);
                    $('#subject_name').addClass('is-invalid');
                    error = true;
                }
                if(!question || question.trim() == ''){
                    $('#question_error').fadeIn(500);
                    $('#question').addClass('is-invalid');
                    error = true;
                }

                if(!question_type){
                    $('#type_error').fadeIn(500);
                    $('#type').addClass('is-invalid');
                    error = true;
                }


                if(question_type == 'multi-choice'){
                    var option1 = $('#answer1').val(), option2 = $('#answer2').val();
                    var option3 = $('#answer3').val(), option4 = $('#answer4').val();
                    num_of_options = 4;

                    if(!option1 || option1.trim() == ''){
                        $('#1_error').fadeIn(500);
                        $('#answer1').addClass('is-invalid');
                        error = true;
                    }
                    if(!option2 || option2.trim() == ''){
                        $('#2_error').fadeIn(500);
                        $('#answer2').addClass('is-invalid');
                        error = true;
                    }
                    if(!option3 || option3.trim() == ''){
                        $('#3_error').fadeIn(500);
                        $('#answer3').addClass('is-invalid');
                        error = true;
                    }
                    if(!option4 || option4.trim() == ''){
                        $('#4_error').fadeIn(500);
                        $('#answer4').addClass('is-invalid');
                        error = true;
                    }

                    var err = false;
                    $('input[name=checkAnswer]').each(function () {
                        var checked = $(this).prop('checked');
                        if(!checked){
                            err = true
                        }
                        else {
                            err = false
                            return false;
                        }
                    })
                    if(err == true){
                        $('input[name=checkAnswer]').addClass('is-invalid');
                        error = true;
                    }

                }
                else{
                    option = $('input[name=true-false-answer]:checked').val();
                    num_of_options = 2;
                }

                if(!error){
                    var options = [];
                    if(question_type == 'multi-choice'){
                        options = [option1, option2, option3, option4]
                    }
                    else {
                        options = ['True', 'False']
                    }
                    var answer = option;

                    var sec_data = {
                        type: question_type,
                        question: question,
                        num_of_options: num_of_options,
                        active_status: 1, school_id: school,
                        course_id: course, class_id: class_name,
                        subject_id: subject, answer: answer,
                        options: options,
                        past_question_type:past_question_type,
                        year: year
                    }

                    let options_string = "";

                    for (let i = 0; i < options; i++){
                        options_string+=options[i];
                        console.log(" options " + options_string);
                    }

                    console.log(options_string);

                    let data = new FormData(this);
                    // data.append('options', options);
                    // data.append('options', options_string);
                    data.append('type', question_type);
                    data.append('num_of_options', num_of_options);
                    data.append('active_status', 1);
                    data.append('course_id', course);
                    data.append('class_id', class_name);
                    data.append('subject_id', subject);
                    data.append('answer', answer);
                    data.append('past_question_type', past_question_type);
                    data.append('year', year);
                    data.append('school_id', school);

                    console.log(data);
                    var errorDiv = $('#errorDiv'), btn = $('#submitQuestion'), btnHtml = 'Submit';
                    $.ajax({
{{--                        url: '{{URL('api/add-question')}}',--}}
                        url: '{{route('backend.past-questions.store')}}',
                        type: 'POST',
                        data: data,
                        contentType: false,                         // The content type used when sending data to the server.
                        cache: false,                              // To unable request pages to be cached
                        processData: false,
                        beforeSend: function () {

                            errorDiv.fadeOut().html('');
                            btn.html('<span class="spinner-border" role="status" aria-hidden="true"></span> &nbsp; submitting...')

                        },
                        success: function (data) {
                            console.log(data);
                            if(data.statusCode == 200){
                                var d = data.data;

                                errorDiv.fadeIn(1000, function () {
                                    errorDiv.html('<div class="alert alert-dark-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Question added successfully</div>');
                                    $('#createQuestion').trigger('reset');
                                    window.location = d.url;
                                })
                            }
                            else{
                                errorDiv.fadeIn(1000, function () {
                                    errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Try again later</div>')
                                })
                            }

                        },
                        complete: function () {
                            btn.html(btnHtml);
                        },
                        error: function (error) {
                            errorDiv.fadeIn(1000, function () {
                                errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Kindly try again later</div>')
                            })
                        }
                    })
                }

            })
        });
    </script>
    <script>
        function ckChange(ckType){
            var ckName = document.getElementsByName(ckType.name);
            var checked = document.getElementById(ckType.id);

            if (checked.checked) {

                for(var i=0; i < ckName.length; i++){

                    if(!ckName[i].checked){
                        ckName[i].disabled = true;
                    }else{
                        ckName[i].disabled = false;
                    }
                }
            }
            else {
                for(var i=0; i < ckName.length; i++){
                    ckName[i].disabled = false;
                }
            }
        }

        $("#image-question-row").hide();

        $("#question-type").click(function(){
            if(document.getElementById("question-type").value ==="image"){
                $("#image-question-row").show();
                $('#image-question-row').attr('required',true);
            }else {
                $("#image-question-row").hide();
                $("image-question-row").removeAttr('required');
            }
        });

    </script>

@stop
