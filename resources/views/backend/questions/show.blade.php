@extends('backend.layouts.layout-3')

@section('content')

    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Questions /</span> Question Details
    </h4>
    <div class="card mb-4">

        <h3 class="card-header">
            {{$question->question}}

            <a href="javascript:void(0)" onclick="window.history.go(-1); return false;" class="btn btn-primary rounded-pill d-block detail-back-btn">Back</a>

        </h3>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-xl-7">
                    <div class="row">
                        <div class="col-sm-6 col-xl-3 mb-2"><strong>School</strong></div>
                        <div class="col-sm-6 col-xl-9">
                            {{$question->getSchoolAttribute()}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xl-3 mb-2"><strong>Class</strong></div>
                        <div class="col-sm-6 col-xl-9">{{$question->getClassAttribute()}}</div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-xl-3 mb-2"><strong>Subject</strong></div>
                        <div class="col-sm-6 col-xl-9">{{$question->subject_details($question->subject_id)}}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-xl-3 mb-2"><strong>Type</strong></div>
                        <div class="col-sm-6 col-xl-9">{{$question->type}}</div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-xl-3 mb-2"><strong>Created by</strong></div>
                        <div class="col-sm-6 col-xl-9">Admin</div>
                    </div>


                </div>

                <div class="col-sm-6 col-xl-5">
                    <h6>Options</h6>
                    <ul class="list-group">
                        @foreach($options as $key => $option)
                         <li class="list-group-item">
                             {{$option->title}}
                             @if($question->choice_id == ($key + 1))
                                 <span class="badge badge-success">Answer</span>
                             @endif
                         </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
