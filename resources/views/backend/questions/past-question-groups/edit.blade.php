@extends('backend.layouts.layout-2')

@section('content')

    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Past Question /</span> Edit Past Question Group
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Edit Past Question Group
        </h6>
        <div class="card-body">

            @includeif('backend.message')
            @include("backend.questions.partials.edit_form")
        </div>

    </div>

    @include("backend.questions.partials.modal")

@endsection
@section('scripts')

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
            integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg=="
            crossorigin="anonymous"></script>

    <style>
        .error{
            color: red;
            display: none;
        }
    </style>

    <script>
        let section = "";
        let sections_info = @json($past_question_group->section_info);

        const add_selected_question = $(".add-selected-question")

        $(".add-question-bank-modal").off('click').on("click",function (){
            section = $(this).data("section");
            add_selected_question.attr("data-section",section)
        })

        add_selected_question.off("click").on("click",function (){
            const question_select = $(".question_selected option:selected")
            $(`#accordion-${section} > div`).append(appendQuestion(question_select.text()));

            let section_info ={
                id: question_select.val(),
                text: question_select.text(),
                section: section
            };

            sections_info.push(section_info)

            console.log("//////////////////")
            console.log(sections_info);

            commonObj.messages('success', `Successfully added question to section ${section}!`)

        });


        console.log("second one //////////////////")
        console.log(sections_info);

    </script>
    <script>

        $(function() {
            $('.subjects-select-2').each(function() {
                $(this)
                    .wrap('<div class="position-relative"></div>')
                    .select2({
                        placeholder: 'Select value',
                        dropdownParent: $(this).parent()
                    });
            })
        });

        function blockUi() {
            $.blockUI({
                message: '<div class="sk-folding-cube sk-primary"><div class="sk-cube1 sk-cube"></div>' +
                    '<div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div>' +
                    '<div class="sk-cube3 sk-cube"></div></div><h5 style="color: #fff">Loading Questions ...</h5>',
                css: {
                    backgroundColor: 'transparent',
                    border: '0',
                    zIndex: 9999999
                },
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.8,
                    zIndex: 9999990
                }
            });
        }

        function unblockUI() {
            $.unblockUI();
        }

        var option = '';
        var category_id = "{{old('institute_type')}}";
        var school_id = "{{old('school_name')}}";
        var question_id = "{{old('question')}}";
        var class_id = "";
        var course_id = "";
        var questions = [];
        var num = 1;
        let questions_data = [];
        let question_group_id = "{{Request::query("group-id")}}";
        let question_bank_ids = [];
        var questions_array = ("{{$questions}}");
        let subject_id = "{{$past_question_group->subject_id}}"

        function appendQuestion(text) {
            return '<div class="alert alert-dark-primary alert-dismissible fade show">' +
                '<button type="button" data-id="' + num + '" class="close rm" data-dismiss="alert">×' +
                '</button>' + text + '</div>';
        }

        function appendSuccessMessage(){
            return '<div class="alert alert-dark-primary alert-dismissible fade show">' +
                '<button type="button" data-id="' + num + '" class="close rm" data-dismiss="alert">×' +
                '</button>' + text + '</div>';
        }

        function removeQuestion() {
            $('.rm').on('click', function () {
                var id = $(this).data('id');
                if (question_bank_ids.includes(id)) {
                    question_bank_ids.splice(question_bank_ids.indexOf(id));
                }
                id = parseInt(id);
                id = id - 1;
                questions.splice(id, 1);
                num = num - 1;
                console.log(question_bank_ids);
            })
        }

        $(document).ready(function () {

            $(".institute_type").on("change", function () {
                var category_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#institution_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.category.schools") }}',
                    data: {'category': category_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#school_name").html(data.schools);
                        if(school_id){
                            $("#school_name").val(school_id).trigger('change');
                        }

                    }
                });
            });
            ``
            $(".school_name").on("change", function () {
                var school_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#school_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courses") }}',
                    data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#course_name").html(data);
                        if(course_id){
                            $("#course_name").val(course_id).trigger('change');
                        }
                    }
                });
            });

            $(".course_name").on("change", function () {
                var course_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#course_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courseclasses") }}',
                    data: {'course_id': course_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $(".class_name").html(data);
                        if(class_id){
                            $(".class_name").val(class_id).trigger('change');
                        }
                    }
                });
            });

            $(".class_name").on("change", function () {
                var class_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#class_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.classsubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $(".subject_name").html(data);
                        if(subject_id){
                            $(".subject_name").val(subject_id).trigger('change');
                        }
                    }
                });
            });

            if(category_id){
                $("#institute_type").val(category_id).trigger('change');
            }

            $('.subject_name').on('change',function () {
                var subject_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.questions") }}',
                    data: {'subject_id': subject_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#question").html(data.questions);
                        if(question_id){
                            $("#question").val(question_id).trigger('change');
                            // alert("worked " + subject_id + " " + question_id);
                            var question_var;

                            for (question_var of parsed_questions_array){
                                console.log("hello " + question_var.questions.question);
                                var ques = question_var.questions.question;
                                // $(document).trigger('questionAdded');
                                questions.push(question_var.question_bank_id);
                                // var question = $('#question').find(":selected").text();
                                $('#questionDiv').append('<div class="alert alert-dark-primary alert-dismissible fade show"><button type="button" data-id="'+num+'" class="close rm" data-dismiss="alert">×</button>'+ques+'</div>');
                                num = num + 1;
                            }

                            $('.rm').on('click', function () {
                                var id = $(this).data('id');
                                id = parseInt(id);
                                id = id - 1;
                                questions.splice(id, 1);
                                num = num - 1;
                                console.log("questions " + questions);
                                console.log("hello num " + num)
                            })
                        }
                    }
                });
            })

            $('#question').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#question_error').fadeOut();
            })

            const data = {group_id: question_group_id }
            fetch("{{route("backend.past-questions-group.questions",$group_id)}}", { method: 'GET'})
                .then(response => response.json())
                .then(data => {
                    console.log('Success:', data);
                    for (let ques of data.questions) {
                            console.log(ques.questions)
                            if (!question_bank_ids.includes(ques.questions.id)) {
                                question_bank_ids.push(ques.questions.id)
                                $('#questionDiv').append(appendQuestion(ques.questions.question));
                                num = num + 1;
                            } else {
                                commonObj.messages('error', 'Question already added')
                            }
                    }
                    removeQuestion()
                }).catch((error) => {
                    console.error('Error:', error);
                });

            $('#addQuestion').on('click', function () {
                // var subject_id = $(this).val();
                var subject_id = "{{$past_question_group->subject_id}}";
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.questions") }}',
                    data: {'subject_id': subject_id, '_token': '{{ csrf_token() }}'},
                    beforeSend: blockUi(),
                    success: function (data) {
                        $("#question").html(data.questions);
                        questions_data = data.questions;
                        if (question_id) {
                            $("#question").val(question_id).trigger('change');
                            for (question_var of parsed_questions_array) {
                                let ques = question_var.questions.question;
                                questions.push(question_var.question_bank_id);
                                $('#questionDiv')
                                    .append(appendQuestion(ques));
                                num = num + 1;
                            }
                            removeQuestion();
                        }
                        unblockUI();
                    }
                });
            })

            $("#add-all-questions").click(function () {
                let questions = $('#question > option');
                for (let ques of questions) {
                    if (ques.text !== "Select Question") {
                        console.log(ques.value)
                        if (!question_bank_ids.includes(ques.value)) {
                            question_bank_ids.push(ques.value)
                            $('#questionDiv').append(appendQuestion(ques.text));
                            num = num + 1;
                        } else {
                            commonObj.messages('error', 'Question already added')
                        }
                    }
                }
                console.log(question_bank_ids);
            })

            $('#question').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#question_error').fadeOut();
            })

            $('#add-selected-question').on('click', function () {
                if (!$('#question').val()) {
                    $('#question').addClass('is-invalid');
                    $('#question_error').fadeIn(500)
                } else {
                    var value = $('#question').val();
                    if (!question_bank_ids.includes(value)) {
                        // if(!questions.includes(value) || !question_bank_ids.includes(value)){
                        $(document).trigger('questionAdded');
                        questions.push(value);
                        question_bank_ids.push(value)
                        var question = $('#question').find(":selected").text();
                        $('#questionDiv').append('<div class="alert alert-dark-primary alert-dismissible fade show"><button type="button" data-id="' + num + '" class="close rm" data-dismiss="alert">×</button>' + question + '</div>');
                        num = num + 1;
                        console.log(question_bank_ids)
                    } else {
                        commonObj.messages('error', 'Question already added')
                    }
                }
            })

            $(document).on('questionAdded', function (e) {

                $('.rm').on('click', function () {
                    let id = $(this).data('id');
                    if (question_bank_ids.includes(id)) {
                        question_bank_ids.splice(question_bank_ids.indexOf(id));
                    }
                    id = parseInt(id);
                    id = id - 1;
                    questions.splice(id, 1);
                    num = num - 1;
                    console.log("questions " + questions);
                    console.log("hello num " + num)
                    console.log(question_bank_ids);
                })
            });

            $('#submitQuestions').on('click', function () {
                const data = {question_bank_ids: question_bank_ids,
                    past_question_group_id: question_group_id,
                    has_section: $("#group_has_sections").val(),
                    section_naming_type: $("#section_naming_type").val(),
                    section_counts: $("#section_counts").val(),
                    sections_info  : sections_info
                };

                fetch("{{route("backend.past-questions-group.update")}}", {
                    method: 'POST', // or 'PUT'
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                })
                    .then(response => response.json())
                    .then(data => {
                        console.log('Success:', data);
                        commonObj.messages('success', 'Successfully updated question group!')

                    })
                    .catch((error) => {
                        console.error('Error:', error);
                    });
            });
        })

        $("#group_has_sections").on("click",function (){
            let has_sections = $(this).is(":checked");

            if (has_sections){
                $("#section_row").show();
            }else{
                $("#section_row").hide();
            }
        })

        setTimeout(function (){
            let has_sections = $("#group_has_sections").is(":checked");

            if (has_sections){
                $("#section_row").show();

                $("#section_naming_type").val("{{$past_question_group->section_naming_type}}");
            }else {
                $("#section_row").hide();
            }

            $("#class_name").val("{{$past_question_group->class_id}}");

            $("#class_name").trigger("change");

        },2000)

        setTimeout(function (){
            $("#subject_name").val("{{$past_question_group->subject_id}}");
        },4000);

        {{--$('#subject_name').on('change',function () {--}}
        {{--    var subject_id = $(this).val();--}}
        {{--    $(this).removeClass('is-invalid');--}}
        {{--    $('#subject_error').fadeOut();--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: '{{ route("ajax.school.questions") }}',--}}
        {{--        data: {'subject_id': subject_id, '_token': '{{ csrf_token() }}'},--}}
        {{--        success: function (data) {--}}
        {{--            $("#question").html(data.questions);--}}
        {{--            if(question_id){--}}
        {{--                $("#question").val(question_id).trigger('change');--}}
        {{--                // alert("worked " + subject_id + " " + question_id);--}}
        {{--                var question_var;--}}

        {{--                for (question_var of parsed_questions_array){--}}
        {{--                    console.log("hello " + question_var.questions.question);--}}
        {{--                    var ques = question_var.questions.question;--}}
        {{--                    // $(document).trigger('questionAdded');--}}
        {{--                    questions.push(question_var.question_bank_id);--}}
        {{--                    // var question = $('#question').find(":selected").text();--}}
        {{--                    $('#questionDiv').append('<div class="alert alert-dark-primary alert-dismissible fade show"><button type="button" data-id="'+num+'" class="close rm" data-dismiss="alert">×</button>'+ques+'</div>');--}}
        {{--                    num = num + 1;--}}
        {{--                }--}}

        {{--                $('.rm').on('click', function () {--}}
        {{--                    var id = $(this).data('id');--}}
        {{--                    id = parseInt(id);--}}
        {{--                    id = id - 1;--}}
        {{--                    questions.splice(id, 1);--}}
        {{--                    num = num - 1;--}}
        {{--                    console.log("questions " + questions);--}}
        {{--                    console.log("hello num " + num)--}}
        {{--                })--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--})--}}


        {{--$("#school_name").on("change", function () {--}}
        {{--    var school_id = $(this).val();--}}
        {{--    $(this).removeClass('is-invalid');--}}
        {{--    $('#school_error').fadeOut();--}}

        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: '{{ route("ajax.school.courses") }}',--}}
        {{--        data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},--}}
        {{--        success: function (data) {--}}
        {{--            $("#course_name").html(data);--}}
        {{--            if(course_id){--}}
        {{--                $("#course_name").val(course_id).trigger('change');--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}

        {{--$("#course_name").on("change", function () {--}}
        {{--    var course_id = $(this).val();--}}
        {{--    $(this).removeClass('is-invalid');--}}
        {{--    $('#course_error').fadeOut();--}}

        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: '{{ route("ajax.school.courseclasses") }}',--}}
        {{--        data: {'course_id': course_id, '_token': '{{ csrf_token() }}'},--}}
        {{--        success: function (data) {--}}
        {{--            $("#class_name").html(data);--}}
        {{--            if(class_id){--}}
        {{--                $("#class_name").val(class_id).trigger('change');--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}


        {{--$("#class_name").on("change", function () {--}}
        {{--    var class_id = $(this).val();--}}
        {{--    $(this).removeClass('is-invalid');--}}
        {{--    $('#class_error').fadeOut();--}}

        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: '{{ route("ajax.school.classsubjects") }}',--}}
        {{--        data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},--}}
        {{--        success: function (data) {--}}
        {{--            $("#subject_name").html(data);--}}
        {{--            if(subject_id){--}}
        {{--                $("#subject_name").val(subject_id).trigger('change');--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}

        {{--});--}}

        </script>


@stop
