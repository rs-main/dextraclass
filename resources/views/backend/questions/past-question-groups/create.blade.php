@extends('backend.layouts.layout-2')

@section('content')
    <h4 class="font-weight-bold py-3 mb-4">
        <span class="text-muted font-weight-light">Past Question /</span> Create Past Question Group
    </h4>

    <div class="card mb-4">
        <h6 class="card-header">
            Create Past Question Group
        </h6>
        <div class="card-body">
            @includeif('backend.message')
            <form method = "post" action="{{route("backend.past-questions-group.store")}}" >
                @csrf
                <div id="errorDiv">
                    <!-- error will be shown here -->
                </div>

                <div class="form-group row">
                    <label for="answer1" class="col-form-label col-sm-2 text-sm-right">Group Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="group_name" id="group_name" placeholder="Group name" class="form-control" required>
                        <div class="error" id="q_name_error">
{{--                            Enter group name--}}
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="answer1" class="col-form-label col-sm-2 text-sm-right">Month</label>
                    <div class="col-sm-10">
                        <select class="custom-select q_input" name="month" required>

                            @foreach(DB::table("months")->get() as $month)
                                <option value="{{$month->id}}">{{$month->month_name}}</option>
                            @endforeach
                        </select>

                        <div class="error" id="month_error">
                            Enter month
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Class</label>
                    <div class="col-sm-10">
                        <select name="class_id" id="class_name" class="custom-select q_input" required>

                            @foreach(\App\Models\Classes::all() as $class)
                                @php
                                    $course = \App\Models\Course::find($class->course_id)
                                @endphp
                                <option value="{{$class->id}}">{{$class->class_name}} ({{ $course ? $course->name : "" }}) </option>
                            @endforeach
                            <option value="" selected="" disabled="" class="d-none">Choose Class</option>
                        </select>
                        <div class="error" id="class_error">
                            Select class
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Subject</label>
                    <div class="col-sm-10">
                        <select name="subject_id" id="subject_name" class="custom-select q_input" required>
                            <option value="" selected="" disabled="" class="d-none">Select Subject</option>
                        </select>
                        <div class="error" id="subject_error">
                            Select subject
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Year</label>
                    <div class="col-sm-10">

                        <input type="number" name="year" class="custom-select q_input" min="1900" max="2099" step="1" value="2016" />
                        <div class="error" id="year_error">
                            Select year
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Exam Duration(minutes)</label>
                    <div class="col-sm-10">
                        <input type="number" name="alloted_time_in_minutes" class="custom-select q_input" step="1"  required />
                        <div class="error" id="duration_error">
                            Type Duration
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-form-label col-sm-2 text-sm-right">Is this group in sections?</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="has_section" id="group_has_sections" class="checkbox-input"  style="height: 20px; width: 20px; margin-top: 8px;"/>
                    </div>
                </div>

                <div id="section_row" style="display: none;">
                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">Section naming type: </label>
                        <div class="col-sm-10">
                            <select name="section_naming_type" id="section_naming_type" class="custom-select q_input" required>
                                    <option value="alphabet">Alphabet</option>
                                    <option value="number">Number</option>
                                    <option value="roman_numerals">Roman Numerals</option>
                            </select>
                            <div class="error" id="class_error">
                                Select type
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-sm-2 text-sm-right">How many sections?</label>
                        <div class="col-sm-10">
                            <input type="number" name="section_counts" class="custom-select q_input" step="1"  required />
                            <div class="error" id="section_count_error">
                                Type section number
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <a href = "{{route('backend.past-questions.index')}}" class="btn btn-danger mr-2">Cancel</a>
                        <button type="submit" class="btn btn-primary" id="submitQuestion">Submit</button>
                    </div>
                </div>
            </form>

        </div>

    </div>
@endsection
@section('scripts')
    <style>
        .error{
            color: red;
            display: none;
        }
    </style>
    <script>
        var option = '';
        var category_id = "{{old('institute_type')}}";
        var school_id = "{{old('school_name')}}";
        var question_id = "{{old('question')}}";
        var questions = [];
        var num = 1;
    </script>
    <script>
        $(document).ready(function () {
            $("#institute_type").on("change", function () {
                var category_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#institution_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.category.schools") }}',
                    data: {'category': category_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#school_name").html(data.schools);
                        if(school_id){
                            $("#school_name").val(school_id).trigger('change');
                        }
                    }
                });
            });

            $("#school_name").on("change", function () {
                var school_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#school_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courses") }}',
                    data: {'school_id': school_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#course_name").html(data);
                        if(course_id){
                            $("#course_name").val(course_id).trigger('change');
                        }
                    }
                });
            });

            $("#course_name").on("change", function () {
                var course_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#course_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.courseclasses") }}',
                    data: {'course_id': course_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#class_name").html(data);
                        if(class_id){
                            $("#class_name").val(class_id).trigger('change');
                        }
                    }
                });
            });

            $("#class_name").on("change", function () {
                var class_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#class_error').fadeOut();

                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.classsubjects") }}',
                    data: {'class_id': class_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#subject_name").html(data);
                        if(subject_id){
                            $("#subject_name").val(subject_id).trigger('change');
                        }
                    }
                });
            });



            if(category_id){
                $("#institute_type").val(category_id).trigger('change');
            }

            $('#subject_name').on('change',function () {
                var subject_id = $(this).val();
                $(this).removeClass('is-invalid');
                $('#subject_error').fadeOut();
                $.ajax({
                    type: "POST",
                    url: '{{ route("ajax.school.questions") }}',
                    data: {'subject_id': subject_id, '_token': '{{ csrf_token() }}'},
                    success: function (data) {
                        $("#question").html(data.questions);
                        if(question_id){
                            $("#question").val(question_id).trigger('change');
                        }
                    }
                });
            })

            $('#question').on('change', function () {
                $(this).removeClass('is-invalid');
                $('#question_error').fadeOut();
            })

            $('#addQuestion').on('click', function () {
                if(!$('#question').val()){
                    $('#question').addClass('is-invalid');
                    $('#question_error').fadeIn(500)
                }
                else{
                    var value = $('#question').val();
                    if(!questions.includes(value)){
                        $(document).trigger('questionAdded');
                        questions.push(value);
                        var question = $('#question').find(":selected").text();
                        $('#questionDiv').append('<div class="alert alert-dark-primary alert-dismissible fade show"><button type="button" data-id="'+num+'" class="close rm" data-dismiss="alert">×</button>'+question+'</div>');
                        num = num + 1;
                    }
                    else{
                        commonObj.messages('error', 'Question already added')
                    }

                }
            })

            $(document).on('questionAdded', function(e) {

                $('.rm').on('click', function () {
                    var id = $(this).data('id');
                    id = parseInt(id);
                    id = id - 1;
                    questions.splice(id, 1);
                    num = num - 1;
                })
            })

            $('#q_name').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#q_name_error').fadeOut();
            })

            $('#pass_mark').on('keyup', function () {
                $(this).removeClass('is-invalid');
                $('#pass_mark_error').fadeOut();
            })


            $('#submitQuestion').on('click', function () {
                var institution = $('#institute_type').val();
                var school = $('#school_name').val();
                var course = $('#course_name').val();
                var class_name = $('#class_name').val();
                var subject = $('#subject_name').val();
                var pass_mark = $('#pass_mark').val();
                var quiz_name = $('#q_name').val();
                $('.error').fadeOut();

                var error = false;
                if(!institution){
                    $('#institution_error').fadeIn(500);
                    $('#institute_type').addClass('is-invalid');
                    error = true;
                }
                if(!school){
                    $('#school_error').fadeIn(500);
                    $('#school_name').addClass('is-invalid');
                    error = true;
                }
                if(!course){
                    $('#course_error').fadeIn(500);
                    $('#course_name').addClass('is-invalid');
                    error = true;
                }
                if(!class_name){
                    $('#class_error').fadeIn(500);
                    $('#class_name').addClass('is-invalid');
                    error = true;
                }
                if(!subject){
                    $('#subject_error').fadeIn(500);
                    $('#subject_name').addClass('is-invalid');
                    error = true;
                }
                if(questions === undefined || questions.length == 0){
                    $('#question_error').fadeIn(500);
                    $('#question').addClass('is-invalid');
                    error = true;
                }
                if(!pass_mark){
                    $('#pass_mark_error').fadeIn(500);
                    $('#pass_mark').addClass('is-invalid');
                    error = true;

                }
                if(!quiz_name || quiz_name.trim() == ''){
                    $('#q_name_error').fadeIn(500);
                    $('#q_name').addClass('is-invalid');
                    error = true;
                }

                if(parseInt(pass_mark) == 0 || parseInt(pass_mark) > 100){
                    $('#pass_mark_error').fadeIn(500);
                    $('#pass_mark').addClass('is-invalid');
                    error = true;
                }

                if(!error){

                    var data = {name: quiz_name, marks: pass_mark, active_status: 1, school_id: school, course_id: course, class_id: class_name, subject_id: subject, questions: questions}

                    var errorDiv = $('#errorDiv'), btn = $('#submitQuestion'), btnHtml = 'Submit';
                    $.ajax({
                        url: '{{URL('api/add-quiz')}}',
                        type: 'POST',
                        data: data,
                        beforeSend: function () {

                            errorDiv.fadeOut().html('');
                            btn.html('<span class="spinner-border" role="status" aria-hidden="true"></span> &nbsp; submitting...')

                        },
                        success: function (data) {
                            console.log(data);
                            if(data.statusCode == 200){
                                var d = data.data;

                                errorDiv.fadeIn(1000, function () {
                                    errorDiv.html('<div class="alert alert-dark-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Question added successfully</div>');
                                    $('#createQuestion').trigger('reset');
                                    window.location = d.url;
                                })
                            }
                            else{
                                errorDiv.fadeIn(1000, function () {
                                    errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Try again later</div>')
                                })
                            }

                        },
                        complete: function () {
                            btn.html(btnHtml);
                        },
                        error: function (error) {
                            errorDiv.fadeIn(1000, function () {
                                errorDiv.html('<div class="alert alert-dark-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">×</button>Sorry an error occurred. Kindly try again later</div>')
                            })
                        }
                    })
                }

            })

            $("#group_has_sections").on("click",function (){
                let has_sections = $(this).is(":checked");

                if (has_sections){
                    $("#section_row").show();
                }else{
                    $("#section_row").hide();
                }
            })

            function convertToRoman(num) {
                var roman = {
                    M: 1000,
                    CM: 900,
                    D: 500,
                    CD: 400,
                    C: 100,
                    XC: 90,
                    L: 50,
                    XL: 40,
                    X: 10,
                    IX: 9,
                    V: 5,
                    IV: 4,
                    I: 1
                };
                var str = '';

                for (var i of Object.keys(roman)) {
                    var q = Math.floor(num / roman[i]);
                    num -= q * roman[i];
                    str += i.repeat(q);
                }

                return str;
            }
        });
    </script>
@stop
