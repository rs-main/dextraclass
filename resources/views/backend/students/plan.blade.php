<option value="" selected disabled>{{$student->subscribed ? 'Subscribed' : 'Not Subscribed'}}</option>

@if(!$student->subscribed)
    @foreach($plans as $plan)
        <option value="{{$plan->id}}">{{$plan->name}} -  GH&cent;{{$plan->amount}}</option>
    @endforeach
@endif




