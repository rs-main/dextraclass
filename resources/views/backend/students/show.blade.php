@extends('backend.layouts.layout-3')

@section('content')
<style>
    .unsubscribed{
        background-color: #d9534f;
        color: #fff;
    }

    .unsubscribed option{
        color: #000c19;
        background-color: #fff;
    }

    .subscribed{
        background-color: #02BC77;
        color: #fff;
    }

    .subscribed option{
        color: #000c19;
        background-color: #fff;
    }

</style>

@section('scripts')

<script>
    $(document).ready(function (){
        $('#subscribe').on('change', function (){
            $('#submit-activation').removeAttr('disabled');
        })

        $('#submit-activation').on('click', function (e) {
            e.preventDefault();
            var id = $('#user').val();
            var palnId = $('#subscribe').val();
            $.ajax({
                url: '{{URL("/api/directactive")}}',
                type: 'POST',
                data: {student_id: id, plan: palnId, activate_mode: 'admin'},
                beforeSend: function(){
                    $('#submit-activation').html('<span class="spinner-border" role="status" aria-hidden="true"></span> activating..')
                        .attr('disabled', 'disabled');
                },
                success: function (data) {
                    $('#subscribe')
                        .removeClass('unsubscribed')
                        .addClass('subscribed');
                    $('#submit-activation').hide();
                    commonObj.messages('success',"User Activation successful")

                },
                error: function (error) {
                    commonObj.messages('error',"Sorry an error occurred. Please try again later")
                    console.log(error);
                    $('#submit-activation').html('Activate').removeAttr('disabled')

                }
            });


        })
    })
</script>

@endsection
<!-- Content -->
<div class="container-fluid flex-grow-1 container-p-y">

    <!-- Header -->
    <div class="container-m-nx container-m-ny bg-white mb-4 pb-4">
        <div class="row">
            <div class="col-md-2 mr-5 pt-5"><a href="javascript:void(0)" onclick="window.history.go(-1);
                                 return false;" class="btn btn-primary rounded-pill d-block detail-back-btn"><span class="ion ion-ios-arrow-back"></span> &nbsp; Back</a></div>
            <div class="col-md-10 col-lg-10 col-xl-7">
                <div class="media mx-auto pt-5">
                    @if(!empty($student->getProfileOrAvatarImageAttribute()))
                    <img class="school_logo mb-2 d-block" style="max-width:150px;min-width: 135px;" src="{{url($student->getProfileOrAvatarImageAttribute())}}"  />
                    @else
                    <img class="school_logo mb-2 d-block" style="max-width:150px;min-width: 135px;" src="{{url('images/default_tutor_manager.png')}}"  />
                    @endif
                    <div class="media-body ml-5">
                        <h4 class="font-weight-bold mb-2">{{ucwords($student->getFullNameAttribute())}}</h4>
                        <div class="text-muted mb-2">
                            <strong>School Name:</strong> <a href="{{route('backend.school.show',$student->school_id)}}" class="text-body">{{$student->school->school_name}}</a>
                        </div>
{{--                        <div class="row">--}}
{{--                            <div class="col-md-3">--}}
{{--                                <div class="card">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <h1 class="card-title text-center text-primary">{{$classesWatched}}</h1>--}}
{{--                                        <p class="text-center">Classes Watched</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-3">--}}
{{--                                <div class="card">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <h1 class="card-title text-center text-primary">{{$noteDownloads}}</h1>--}}
{{--                                        <p class="text-center">Notes downloaded</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-3">--}}
{{--                                <div class="card">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <h1 class="card-title text-center text-primary">{{$questionsCount}}</h1>--}}
{{--                                        <p class="text-center">Questions Asked</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-3">--}}
{{--                                <div class="card">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <h1 class="card-title text-center text-primary">{{$replyCount}}</h1>--}}
{{--                                        <p class="text-center">Answers contributed</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="text-muted mb-2">
                            <ul class="profile-achievement list-unstyled">
                                <li>
                                    <span class="icon"><img src="{{asset('images/icon-eye.png')}}" alt=""></span>
                                    <span class="text">{{$classesWatched}}	Classes watched</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="{{asset('images/icon-notes.png')}}" alt=""></span>
                                    <span class="text">{{$noteDownloads}} Lecture notes downloaded</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="{{asset('images/icon-question.png')}}" alt=""></span>
                                    <span class="text">{{$questionsCount}} Questions asked</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="{{asset('images/icon-tick.png')}}" alt=""></span>
                                    <span class="text">{{$replyCount}} Answers contributed</span>
                                </li>
                            </ul>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div id="s-error"></div>
                            </div>
                        </div>

                        <div class="row mb-5">
                            <div class="form-inline">
                                <input type="hidden" name="user" id="user" value="{{$student->id}}">
                                <div class="form-group">
                                    <select class="form-control {{$student->subscribed ? 'subscribed' : 'unsubscribed'}}" name="subscribe" id="subscribe">
                                        <option value="" selected disabled>{{$student->subscribed ? 'Subscribed' : 'Not Subscribed'}}</option>
                                        @if(!$student->subscribed)
                                            <option value="1" class="unsubscribed-option">Monthly - GH&cent; 20</option>
                                            <option value="2"  class="unsubscribed-option" >Per - Term - GH&cent; 50</option>
                                            <option value="3"  class="unsubscribed-option" >Academic Year - GH&cent; 150</option>
                                        @endif
                                    </select>
                                </div>
                                @if(!$student->subscribed)
                                    <button type="button" id="submit-activation" class="btn btn-primary ml-2" disabled>{{ __('Activate') }}</button>
                                @endif
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Header -->

    <div class="row">
        <div class="col">

            <!-- Info -->
            <div class="card mb-4">
                <div class="card-body">

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Email:</div>
                        <div class="col-md-9">
                            {{$student->email}}
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Mobile:</div>
                        <div class="col-md-9">
                            {{$student->mobile}}
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Category:</div>
                        <div class="col-md-9">
                            {{$student->category->name}}
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">School:</div>
                        <div class="col-md-9">
                            {{$student->school->school_name}}
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Course:</div>
                        <div class="col-md-9">
                            {{$student->course->name}}
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Class:</div>
                        <div class="col-md-9">
                            {{$student->student_class->class_name}}
                        </div>
                    </div>

                    <div class="row mb-2">
                        <div class="col-md-3 text-muted">Country:</div>
                        <div class="col-md-9">
                            @if(isset($student->country_name->name) && !empty($student->country_name->name))
                            {{$student->country_name->name}}
                            @else
                            N/A
                            @endif
                        </div>
                    </div>


                </div></div>
            <!-- / Info -->

            <!-- Posts -->



            <!-- / Posts -->

        </div>

    </div>

    <div class="row">
        <div class="col">
            <h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
                <div>History</div>
            </h4>
            <!-- Info -->
            <div class="card mb-4">
                <div class="card-body">
                @if(!empty($studentHistories[0]))
                  <ul class="list-group list-group-flush">
                    @foreach($studentHistories as $studentHistory)
                    <li class="list-group-item">
                        <div class="item-row history-row-count" id="history-row-{{$studentHistory->id}}">
                            <div class="row">
                                <div class="col flex-grow-0">
                                    <figure class="item-row-image mb-0"><img src="{{asset($studentHistory->video->school->school_logo)}}" alt="" width="48"></figure>
                                </div>

                                <div class="col flex-grow-1">
                                    <h4 class="heading mb-0">{!!$studentHistory->video->title!!}</h4>
                                    <p class="heading-sub-text mb-0">{{$studentHistory->video->sub_title}}</p>
                                </div>
                                <div class="col  flex-grow-0 mt-2 mt-md-0 ">
                                    <div class="action-button d-flex align-items-center">
                                        <a href="{{route('backend.videos.show', $studentHistory->video->uuid)}}" title="View Video" class="btn-play"><i class="ion ion-md-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                  </ul>
                @else
                <div class="list-view-sec" style="text-align:center">
                    <h5 class="text-info">No Record Available</h5>
                </div>
                @endif

                    {{ $studentHistories->links() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- / Content -->
@endsection
