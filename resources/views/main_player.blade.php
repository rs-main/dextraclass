<html>
<head>
    <title></title>
    <link rel="stylesheet" href="https://cdn.plyr.io/3.6.8/plyr.css" />
    <script src="https://cdn.plyr.io/3.6.8/plyr.js"></script>

    <script>
        // Change the second argument to your options:
        // https://github.com/sampotts/plyr/#options
        const player = new Plyr('video', {captions: {active: true}});

        // Expose player so it can be used from the console
        window.player = player;
    </script>

    <style>
        .container {
            margin: 50px auto;
            max-width: 500px;
        }
    </style>
</head>

<body>

<div class="container">
    <video controls crossorigin playsinline
           poster="https://video.dextraclass.com/media/original/thumbnails/user/admin/2bb4704b6dcb4d88824d13b736b93469.8._IONS.mp4_z4We62n.jpg">
           poster="">
{{--        <source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4" type="video/mp4" size="576">--}}
{{--        <source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4" type="video/mp4" size="720">--}}
{{--        <source src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4" type="video/mp4" size="1080">--}}
        <source src="https://video.dextraclass.com/media/encoded/15/admin/2bb4704b6dcb4d88824d13b736b93469.2bb4704b6dcb4d88824d13b736b93469.8._IONS.mp4.mp4" type="video/mp4" size="576">
        <source src="https://video.dextraclass.com/media/encoded/10/admin/2bb4704b6dcb4d88824d13b736b93469.2bb4704b6dcb4d88824d13b736b93469.8._IONS.mp4.mp4" type="video/mp4" size="720">
        <source src="https://video.dextraclass.com/media/encoded/7/admin/2bb4704b6dcb4d88824d13b736b93469.2bb4704b6dcb4d88824d13b736b93469.8._IONS.mp4.mp4" type="video/mp4" size="1080">

        <!-- Caption files -->
        <track kind="captions" label="English" srclang="en" src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.en.vtt"
               default>
        <track kind="captions" label="Français" srclang="fr" src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.fr.vtt">
        <!-- Fallback for browsers that don't support the <video> element -->
        <a href="https://video.dextraclass.com/media/encoded/7/admin/2bb4704b6dcb4d88824d13b736b93469.2bb4704b6dcb4d88824d13b736b93469.8._IONS.mp4.mp4" download>Download</a>
{{--        <a href="https://video.desafrica.com/media/encoded/15/admin/2bb4704b6dcb4d88824d13b736b93469.2bb4704b6dcb4d88824d13b736b93469.8._IONS.mp4.mp4" download>Download</a>--}}
    </video>
</div>
<!-- Plyr resources and browser polyfills are specified in the pen settings -->

</body>
</html>
