@extends('frontend.layouts.app')

@section('page-hero')
    @include('frontend.includes.landing_masterclass_page_hero')
@endsection

@section('course-carousel')
    @include('frontend.includes.masterclass_carousel')
@endSection

@section('login-form')
<section class="login-wrapper py-lg-5" id="landing-organization-login-wrapper">
    <div class="container">
        <div class="row d-flex justify-content-end">
            <div class="col-md-6">
                <div class="login-card">
                    <div class="sub-heading pb-4"><strong>Login </strong>to enable you ask questions, & connect with classmates</div>
                    <form action="{{ route('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input name="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" value="{{ old('username') }}" placeholder="{{ __('Username') }}">
                            @if ($errors->has('username'))
                            <div class="invalid-feedback">
                                {{ $errors->first('username') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('Password') }}">
                            @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary w-100">LOGIN</button>
                        </div>
                        <div class="form-group info-text">or register if you dont have an account</div>
                        <div class="form-group mb-0">
                            <a href="{{ route('register') }}" class="btn btn-secondary w-100" title="Register">REGISTER</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endSection

@section('join-any-device')
@section('join-any-device')
    @include('frontend.includes.join_any_device_banner')
@endsection
@section('contact-banner')
    @include('frontend.includes.contact_banner')
@endSection