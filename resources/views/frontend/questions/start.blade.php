@extends("frontend.layouts.past_questions_layout")

@section("scripts")

    <style>

        .quiz-option-selector li label .exp-number {
            margin-right: 25px;
        }
        .quiz-option-selector li label {
            z-index: 2;
            width: 100%;
            height: 100px;
            padding: 10px;
        }
        .quiz-option-selector li label .exp-label {
            font-size: 14px;
            font-weight: 600;
            line-height: 70px;
            width: 10%;
        }

    </style>

    <script>
        $(".actions").hide();

        console.log("{{Auth::user()->uuid}}")

        $(".exp-option-box").on("click",function (){
            $(".actions").show();

            $("html, body").animate({ scrollTop: document.body.scrollHeight }, "slow");

            let subject_id = $(this).data("subject-id");
            let class_id = $(this).data("class-id");

            $(".js-btn-start").on("click",function (){

                // localStorage.clear();
                let user_uuid = "{{Auth::user()->uuid}}";
                localStorage.setItem("start_test_session_token", user_uuid);
                localStorage.setItem("subject_id",subject_id);

                var oldDateObj = new Date();
                var diff = 15;
                var newDateObj = new Date(oldDateObj.getTime() + diff*60000);

                localStorage.setItem("time_started", "" +newDateObj);

                console.log(localStorage.getItem("time_started" ));

                setTimeout(function (){
                    location.href = `/past-questions/${subject_id}`;
                },1000);
            });
         });

    </script>
@endsection

@section("content")
<div class="quiz-top-area text-center">
    <h1>PAST QUESTIONS</h1>
{{--    <div class="quiz-countdown text-center ul-li">--}}
{{--        <ul>--}}
{{--            <li class="days">--}}
{{--                <span class="count-down-number"></span>--}}
{{--                <span class="count-unit">Days</span>--}}
{{--            </li>--}}

{{--            <li class="hours">--}}
{{--                <span class="count-down-number"></span>--}}
{{--                <span class="count-unit">Hours</span>--}}
{{--            </li>--}}

{{--            <li class="minutes">--}}
{{--                <span class="count-down-number"></span>--}}
{{--                <span class="count-unit">Min</span>--}}
{{--            </li>--}}

{{--            <li class="seconds">--}}
{{--                <span class="count-down-number"></span>--}}
{{--                <span class="count-unit">Sec</span>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}
</div>

<div class="wrapper position-relative">

    <br>
    <div class="quiz-title text-center">

        @if(count($past_questions_group))
        <h2>{{ count($past_questions_group) > 0 ?strtoupper($past_questions_group[0]->subject_class->class_name) ." PAST QUESTIONS" : ""}} </h2>
        <h3>Choose from the options below. </h3>

        @else
            <h2>NO PAST QUESTIONS FOR THIS SUBJECT YET! </h2>
{{--            <h3>Choose from the options below. </h3>--}}
        @endif
    </div>

    <div class="quiz-option-selector clearfix">
        <ul>
            @foreach($past_questions_group as $key => $past_question)
                    <li>
                        <label class="start-quiz-item">
                            <input type="radio" name="quiz"
                                   data-class-id="{{$past_question->class_id}}"
                                   data-subject-id="{{$past_question->id}}"
                                   data-year="" value="" class="exp-option-box">
                            <span class="exp-number text-uppercase">{{$key+1}}</span>
                            <span class="exp-label">{{ \Illuminate\Support\Str::limit($past_question->subject_name, 60, $end='...') }}</span>
                            <span class="checkmark-border"></span>
                        </label>
                    </li>
            @endforeach
        </ul>
    </div>

    <div class="actions clearfix">
        <ul>
                <li style="margin-left: 35%"><span class="js-btn-start">Start Test</span></li>
        </ul>
    </div>

</div>
@endsection
