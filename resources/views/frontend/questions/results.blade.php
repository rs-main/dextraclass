@extends("frontend.layouts.past_questions_layout")

@section("scripts")

    <style>

    </style>

    <script>

        if (localStorage.length > 0) {
            let results = localStorage.getItem("results-{{Auth::user()->id}}");
            let total = localStorage.getItem("total-{{Auth::user()->id}}");
            let class_id = localStorage.getItem("test-class-id-{{Auth::user()->id}}")

            $(".results").text(`You Scored ${results} out of ${total}`);

            $(".actions").on("click",function (){
                location.href = `/past-questions-start/${class_id}`;
            });

        }else {
            location.href = "/";
        }

    </script>
@endsection

@section("content")

    <div class="quiz-top-area text-center">
        <h1> TEST RESULTS </h1>
    </div>
    <div class="wrapper position-relative">

        <div class="wizard-content-1 clearfix">
            <div class="step-inner-content clearfix position-relative">
                <div class="form-area position-relative">
                    <div class="wizard-forms">
                        <div class="quiz-option-selector">
                            <div class="thankyou-msg text-center">
                                <img src="{{asset("assets/img/th.png")}}" alt="">
                                <h4>You have successfully completed the test</h4>
                                <h2 class="results" ></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="actions clearfix">
            <ul>
                <li style="margin-left: 35%"><span class="js-btn-start">Back to Home</span></li>
            </ul>
        </div>
    </div>

@endsection
