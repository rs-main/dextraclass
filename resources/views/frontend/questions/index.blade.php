@extends("frontend.layouts.past_questions_layout")

@section("header_scripts")
    <script>
{{--        window.onbeforeunload = function (){--}}
{{--            return "Are you sure you want to leave this page?"--}}
{{--        }--}}

$(window).bind('beforeunload', function(){
    return 'Are you sure you want to leave?';
});
    </script>
@endsection

@section("scripts")

    <style>
        .quiz-option-selector li label .exp-label {
            z-index: 2;
            color: #446d76;
            font-size: 15px;
            font-weight: 600;
            line-height: 70px;
            position: relative;
            padding-right: 10px;
        }

        .quiz-top-area .quiz-countdown li:after {
            top: 0;
            right: -12px;
            content: "";
        }
    </style>

<script>



        const form = $(".multisteps-form__form");
        const btn_next = $(".js-btn-next");
        const btn_prev = $(".js-btn-prev");
        const btn_stop = $(".js-btn-stop");
        const btn_end_test = $(".stop-test");
        const btn_continue = $(".continue");
        const true_false_type = 'true-false';
        const multi_choice_type = 'multi-choice';
        let correct_marks = 0;
        let question_answer_collections = [];
        let stop_test_confirm = true;

        btn_next.on("click",function (){
            let id = $(this).data("id");
            let multi_choice_answer =  $('input[name=quiz]:checked').data("answer");
            let answer = $(this).data("answer");
            let answer_input = null;
            if ($( "input[name=quiz]:checked" )) {
                answer_input = $('input[name=quiz]:checked', '.multisteps-form__form').val();
            }
            let type = $(this).data('type');
            let user_answer = "";

            if (type === true_false_type){
                switch (answer_input){
                    case 'true':
                        user_answer = 1;
                        break;
                    case 'false':
                        user_answer = 2;
                        break;
                }

                if (answer === user_answer){
                    let object = {id : id, answer : answer, answer_input : answer_input}
                    question_answer_collections.push(object);
                    ++correct_marks;
                }

            }else if (type === multi_choice_type){
                console.log("answer multi choice " + multi_choice_answer);
                console.log("answer result " + answer);

                if (multi_choice_answer === true){
                    let object = {id : id, answer : answer, answer_input : answer_input}
                    question_answer_collections.push(object);
                    ++correct_marks;
                }
            }

            console.log("Marks" + correct_marks + " total " + "{{count($questions)}}");
            console.log( question_answer_collections);
        });

        btn_prev.on('click',function (){
            let id = $(this).data('id');
            let type = $(this).data('type');
            if(question_answer_collections.findIndex(x => x.id === id) && question_answer_collections.length > 0){
                correct_marks--;
                question_answer_collections.pop();
            }

            console.log(correct_marks);
            console.log(question_answer_collections);
        });

        form.submit(function(e){
            e.preventDefault();

            // if(confirm("Are you sure?")){
            //     alert("Hello fred");
            // }

            btn_next.trigger('click');
            btn_stop.trigger("click");

        })

        btn_end_test.on("click",function (){
            btn_stop.trigger("click");
        })

        btn_continue.on("click",function (){
            stop_test_confirm = false;
            btn_stop.trigger("click");
        })

        btn_stop.on("click",function (){

            let subject_id = $(this).data("subject-id");
            let class_id = $(this).data("class-id");

            localStorage.clear();
            localStorage.setItem("results-{{Auth::user()->id}}", correct_marks);
            localStorage.setItem("total-{{Auth::user()->id}}", "{{count($questions)}}");
            localStorage.setItem("test-class-id-{{Auth::user()->id}}",class_id)

            if (!stop_test_confirm){
                location.href=`/past-questions-results`;
                return;
            }

            if (confirm("Are you sure you want to end the test?")){
                location.href=`/past-questions-results`;
            }
        });



    </script>

@endsection

@section("content")
<div class="quiz-top-area text-center">
    <h1>{{count($questions)}} Questions</h1>
    <div class="quiz-countdown text-center ul-li">
        <ul>
{{--            <li class="days">--}}
{{--                <span class="count-down-number"></span>--}}
{{--                <span class="count-unit">Days</span>--}}
{{--            </li>--}}

            <li class="minutes">
                <span class="count-down-number"></span>
                <span class="count-unit">Min</span>
            </li>


            <li class="seconds">
                <span class="count-down-number"></span>
                <span class="count-unit">Sec</span>
            </li>

            <li class="days stop-test">
                <span class="count-down-number"></span>
                <span class="count-unit">STOP TEST</span>
            </li>
        </ul>
    </div>
</div>
<div class="wrapper position-relative">
    <div class="wizard-content-1 clearfix">
        <div class="steps d-inline-block position-absolute clearfix">
            <ul class="tablist multisteps-form__progress">
                <li class="multisteps-form__progress-btn js-active current"></li>
                <li class="multisteps-form__progress-btn "></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
            </ul>
        </div>
        <div class="step-inner-content clearfix position-relative">
            <form class="multisteps-form__form" action="" id="wizard" method="POST">
                <div class="form-area position-relative">

                    @foreach($questions as $key => $question)

                    <!-- step 1 -->
                    <div class="multisteps-form__panel {{ $key == 0 ? 'js-active' : ''  }}" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                @php
                                    $var = $key+1;
                                    $percentage = intval($var/count($questions) * 100);

                                @endphp

                                <span>Question {{$var}}</span>
                                <h3>{{ $question->question }}</h3>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>

                                    @if($question->type == 'true-false')
                                        <li>
                                            <label class="start-quiz-item">
                                                <input type="radio" name="quiz" value="true" class="exp-option-box required" required>
                                                <span class="exp-number text-uppercase"><img src="assets/img/qi1.png" alt=""></span>
                                                <span class="exp-label">True</span>
                                                <span class="checkmark-border"></span>
                                            </label>
                                        </li>

                                        <li>
                                            <label class="start-quiz-item">
                                                <input type="radio" name="quiz" value="false" class="exp-option-box">
                                                <span class="exp-number text-uppercase"><img src="assets/img/qi1.png" alt=""></span>
                                                <span class="exp-label">False</span>
                                                <span class="checkmark-border"></span>
                                            </label>
                                        </li>

                                    @else

                                    @foreach($question->options as $key => $option)
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="quiz" data-answer="{{ $question->choice_id == $key+1 ? 'true': 'false' }}"
                                                   data-key="{{$key}}" value="{{$option->title}}" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi1.png" alt=""></span>
                                            <span class="exp-label">{{ $option->title }}</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>

                                    @endforeach

                                    @endif


                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: {{$percentage}}%">
                                        <span>{{$percentage}}% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>

                            <div class="actions clearfix">
                                <ul>

                                    @if($var == 1)
                                    <li>
                                        <span class="js-btn-stop" data-class-id="{{$question->class_id}}" title="PREV" data-subject-id="{{$question->subject_id}}">Stop Test</span></li>
                                    @else
                                    <li>
                                        <span  data-type="{{$question->type}}" data-answer="{{$question->choice_id}}"
                                               data-id="{{$question->id}}" class="js-btn-prev" title="PREV" >Previous Question
                                        </span>
                                    </li>
                                    @endif

                                    @if($var < count($questions))
                                    <li><span class="js-btn-next" data-type="{{$question->type}}" data-answer="{{$question->choice_id}}" data-id="{{$question->id}}" title="NEXT">Next Question  {{$var+1}}</span></li>

                                    @elseif($var == count($questions))

                                    <li><button class="js-btn-submit" type="submit"><span>SUBMIT</span></button></li>

                                    @endif

                                </ul>
                            </div>
                        </div>
                    </div>

                    @endforeach

                </div>
            </form>
        </div>
    </div>
</div>

<div id="end-test-modal" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                <h4 class="modal-title">Time is Up!</h4>
            </div>
            <div class="modal-body">
                <h4>Your time is up {{Auth::user()->name}}.</h4>
                <p>Click continue to results page.</p>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
{{--                    <button class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>--}}
                    <button class="btn btn-secondary continue"><span class="glyphicon glyphicon-check"></span> Continue to results</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dalog -->
</div>

@endsection
