<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
		@yield('metaData')
        <title>
            @if(View::hasSection('title'))@yield('title') - @endif{{ config('app.name', 'XtraClass')}}
        </title>
        @yield('before-styles')
        <!-- Scripts -->
{{--        <script src="{{ asset('js/app.js') }}" defer></script>--}}

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/owl.theme.default.min.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/ionicons.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/linearicons.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/open-iconic.css') }}">
        <link rel="stylesheet" href="{{ mix('/assets/vendor/fonts/pe-icon-7-stroke.css') }}">
        <link rel="stylesheet" href="{{asset("assets/vendor/libs/ladda/ladda.css")}}">

        <!-- Developer Styles Css -->
        <link href="{{ asset('css/developer.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
              integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/shepherd.js@8.1.0/dist/css/shepherd.css">
        <script src="{{ asset('js/axios.min.js') }}"></script>


        @yield('after-styles')
        @yield('header-styles')
        <link href="{{ asset('css/pace.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
		<script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
		<script>tinymce.init({ selector:'.textarea-editor' });</script>
        @if(Session::has('jsError'))

        <script>
        $(function () {
            toastr['error']('{{ Session::get("jsError") }}')
            toastr.options = {
                "closeButton": true,
                "debug": true,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "showDuration": "200",
                "hideDuration": "2000",
                "timeOut": "6000",
                "extendedTimeOut": "2000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        });
        </script>
        @endif

        @if(Session::has('jsSuccess'))

        <script>
            $(function () {
                toastr['success']('{{ Session::get("jsSuccess") }}')
                toastr.options = {
                    "closeButton": true,
                    "debug": true,
                    "newestOnTop": true,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": true,
                    "showDuration": "200",
                    "hideDuration": "2000",
                    "timeOut": "6000",
                    "extendedTimeOut": "2000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            });
        </script>
        @endif
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.2.4/pdfobject.min.js" integrity="sha512-mW7siBAOOJTkMl77cTke1Krn+Wz8DJrjMzlKaorrGeGecq0DPUq28KgMrX060xQQOGjcl7MSSep+/1FOprNltw==" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
                integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="app">
            <header class="header sticky-top">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-6 col-lg-3">
                            <div class="logo">
                                <a href="{{ URL::to('/') }}">
                                    <img src="{{asset('images/logo.png')}}" alt="{{ config('app.name', 'XtraClass') }}">
                                </a>
                            </div>
                        </div>

                        <div class="col-6 col-lg-9">
                            <div class="mobile-toggle-btn"> <span></span> </div>
                            <nav class="header-nav">
                                <ul class="main-menu">
									@if(Request::segment(1) != 'classroom' && !empty(GLB::continueVideo()))
                                     <li>
										<a href="{{GLB::continueVideo()}}" class="btn-custom nav-link" title="Continue">Continue </a>
									</li>
									@endif
                                    @if(Request::segment(1) == 'classroom' && !empty($defaultVideo->school->school_category) && !empty($defaultVideo->school_id))
                                    <li>
                                        <a href="javascript:void(0)" title="" onclick="JoinAClass2.openModel({{$defaultVideo->school->school_category}},{{$defaultVideo->school_id}})" class="btn-custom nav-link" >Change Class </a>
                                    </li>
									@else
									 @if(!Request::is('/') && !Request::is('home'))
                                     <!--- <li>
                                        <a href="javascript:void(0)" title="" class="btn-custom nav-link" data-toggle="modal" data-target="#myModal">Change Class</a>
                                     </li> --->
                                     @endif
                                    @endif
                                    <li class="header-search pl-0 pl-lg-3 position-relative">
                                        <form action="{{route('frontend.search')}}" method="get" class="search-form">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" name="search_input" class="form-control" value="{{Request::input('search_input')}}" required="">
                                                <button type="submit" class="search-btn"><i class="fas fa-search"></i></button>
                                            </div>
                                        </form>
                                    </li>

                                    @if(Auth::check())
                                    @if(Auth::user()->hasRole(['tutor']))
                                    <li class="tutor-actions-icon position-relative">
                                        <button type="" class="shortcut-tutor-actions-btn position-absolute" onclick="tutorActionsObj.openTutorActionsPopup()">
                                            <i class="fas fa-plus-circle"></i>
                                        </button>
                                        @include('frontend.includes.tutor_actions_popup')
                                    </li>
                                    @endif
                                    @endif


                                    <!-- Authentication Links -->
                                    @guest
                                    <li>
                                        <a href="javascript:void(0)" title="" class="btn-custom "  onclick="loginObj.openPopup()" >
                                            <ion-icon name="person" ></ion-icon> &nbsp;
                                            {{ __('Login / Signup') }}
                                        </a>
                                        @include('frontend.includes.login_popup')
                                    </li>
                                    @else
                                    <li class="nav-item  dropdown ">

                                        <a id="navbarDropdown" class="btn-custom   dropdown-toggle" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                            <img src='{{asset(Auth::user()->userData->profile_or_avatar_image)}}' class="rounded-circle" id="profileImageRun" width="24" height="24">
                                            <span class="mx-2">{{ ucwords(Auth::user()->username) }}</span> <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu after-login-dropdown dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            @if(Auth::user()->hasRole(['student','tutor']))
                                            <a class="dropdown-item" href="{{ route('frontend.profile') }}">
                                                {{ __('Profile') }}
                                            </a>
                                            @endif
                                            @if(Auth::user()->hasRole(['admin','subadmin','school']))
                                            <a class="dropdown-item" href="{{ route('backend.dashboard') }}">
                                                {{ __('Dashboard') }}
                                            </a>
                                            @endif

											@if(SiteHelpers::emailVerified() == 1)
											  <a href="javascript:void(0)" class="dropdown-item firstSendOtp" data-toggle="modal" data-target="#activeModel">Activate Account </a>
											@endif
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                    @endguest

                                </ul>
                            </nav>
                        </div>
                    </div>


                </div>
            </header>

            @yield('content')

            @hasSection('page-hero')
                @yield('page-hero')
            @endif

            @hasSection('school-carousel')
                @yield('school-carousel')
            @endif

            @hasSection('course-carousel')
                @yield('course-carousel')
            @endif

            @hasSection('login-form')
                @yield('login-form')
            @endif

            @hasSection('join-any-device')
                @yield('join-any-device')
            @endif

            @hasSection('contact-banner')
                @yield('contact-banner')
            @endif

            <footer class="footer">
                <div class="container">
                    <ul class="footer-menu">
                        @guest
                        <li><a href="{{ route('register') }}" title="Register">Register</a></li>
                        @endguest
                        <li class="d-none"><a href="{{route('frontend.knowledge.articles.index')}}" title="Articles">Articles</a></li>
                        <li class="d-none"><a href="{{route('front')}}" title="About Xc">About Xc</a></li>
                        <li ><a href="{{route('frontend.pages.how_to_access')}}" title="Connect">Connect</a></li>
                        <li ><a href="{{route('frontend.pages.help')}}" title="Help">Help</a></li>
                        <li ><a href="{{route('frontend.pages.contact')}}" title="Contact">Contact</a></li>
                    </ul>
                    <hr>
                    <h6 class="footer-copyright"><strong>Xtra</strong>Class (c) 2021 All rights reserved     <a href="{{route('frontend.pages.terms_condition')}}" class="d-none">Terms & conditions</a></h6>
                </div>
            </footer>

        </div>

        <!-- Loader div -->
        <div id="cover-spin">
            <div>
                <div class="spinner-grow spinner-grow-lg"></div>
                <b>Please wait.....</b>
            </div>
        </div>

        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <!-- Jquery validations-->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
        <script src="{{ asset('js/pace.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/shepherd.js@8.1.0/dist/js/shepherd.min.js"></script>

        <script src="{{ asset('js/common_new.js') }}"></script>
        <script src="{{ asset('js/join_open_class.js') }}"></script>
        <script src="{{ asset('js/join_a_school.js') }}"></script>
        <script src="{{ asset('js/join_a_class.js') }}"></script>
        <script src="{{ asset('js/take_a_course.js') }}"></script>
        <script src="{{ asset("assets/vendor/libs/spin/spin.js")}}"></script>
        <script src="//js.pusher.com/3.1/pusher.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


        @if(Auth::check())
			@include('frontend.includes.active_account')
		@endif

        <script>
            const pusher = new Pusher('a7add5dfd9b9d8d52636', {
                encrypted: true, cluster: 'eu'
            });
        </script>

        @yield('scripts')
        @yield('footer-scripts')

    </body>
</html>
