@extends('frontend.layouts.app')

<style>
    .joinanchor {
        color: white;
        text-decoration: underline;
    }

</style>

@section('content')
{{--<section class="content-top-wrapper bg-white">--}}
{{--    <div class="container">--}}
{{--        <div class="join-class-wrapper text-white">--}}
{{--            <div class="row pt-0 pb-0 pt-md-4 pb-md-5">--}}
{{--                <div class="col-md-8 col-xl-6 pb-0 pb-md-3">--}}
{{--                    <h1 class="heading text-white">Learn at your own pace</h1>--}}
{{--                    <p class="heading-sub-text">Stream, pause & repeat any lesson until you understand <br> Select a class below to start learning now...</p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <!--            <div class="graphic-card">Insert illustration here...</div>-->--}}

{{--            @php--}}
{{--            $joinButton  = "submit";--}}
{{--            $is_home = true;--}}
{{--            @endphp--}}


{{--            @include('frontend.includes.open_home_search_form')--}}

{{--            <div class="mt-5">--}}
{{--                <a href="{{route('school')}}" _target="self"><span class="joinanchor" >Or Join A Class in your School if you have access</span> </a>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

<section  class="header-open">

    <div class="container">
        <div class="row pt-0 pb-0 pt-md-4 ">
            <div class="col-md-8 col-xl-6  pt-2 pt-md-5 pb-2 pb-md-3">
                <h1 class="heading">Learn at your <br> own pace</h1>
                <p class="heading-sub-text">Stream, pause & repeat any lesson <br>until you understand </p>

                @guest
                <div class="row pt-3">
                    <div class="col-6">
                        <a href="{{ route('login') }}">
                            <button class="btn btn-outline-primary btn-block btn-lg">LOGIN</button>
                        </a>
                    </div>
                    <div class="col-6">
                        <a href="{{ route('register') }}">
                            <button class="btn btn-primary btn-block btn-lg">JOIN NOW</button>
                        </a>
                    </div>
                </div>
                @endguest
            </div>
            <div class="col-md-4 col-xl-6 pb-0 text-center">
                <img src="{{asset('/images/header-right.png')}}" class="img-fluid">
            </div>
        </div>
    </div>

</section>
@auth
    <section>
        <div class="container">
            <div class="row pt-5 pb-5">
                <div class="col-12">
                    <h2 class="sub-heading">Select a class below to start learning now</h2>
                    @include('frontend.includes.open_home_search_form')
                </div>

            </div>
        </div>
    </section>
@endauth

                @include('frontend.includes.change_open_class')

                <section class="school-carousel-wrapper bg-white">
                    <div class="container">
                        <h2 class="sub-heading">Classes streaming online now</h2>
                        <div class="school-carousel owl-carousel owl-theme">

                            @if(!empty($schools[0]))
                            @foreach($schools as $school)
                            @if (in_array($school->school_category, [19, 20, 21, 22]))
                            <div class="item">
                                <div class="school-card btn-group" >
                                    @php
                                    $coursesHasVideo = $school->coursesHasVideo;
                                    @endphp
                                    <a href="javascript:void(0)" title="" class="school-card-link @if($coursesHasVideo->count() > 0) openChangeClassModal @endif" onclick="JoinOpenClass2.openModel({{$school->school_category}},{{$school->id}})" data-cat="{{$school->school_category}}" data-school="{{$school->id}}">
                                        @if($school->is_locked)
                                        <div class="school-lock">
                                            <img src="/images/lock-icon.png" alt="lock">
                                        </div>
                                        @endif
                                        <figure class="school-logo">
                                            <img src="{{asset('/uploads/schools/'.$school->logo)}}" class="img-fluid" height="73" alt="{{ $school->school_name }}">
                                        </figure>

                                        <h3 class="school-name">{{ $school->school_name }}</h3>
                                        @if($school->school_category == config('constants.BASIC_SCHOOL'))
                                        @php
                                        $course = $coursesHasVideo[0];
                                        @endphp
                                        <p class="school-course-qty">
                                            {{--  {{$school->coursesLabel($course->classesHasVideosWithKey->count())}}   --}}
                            {{$school->videos->count()}} Lessons
                        </p>
                       @else
                        <p class="school-course-qty">
                            {{--  {{$school->coursesLabel($coursesHasVideo->count())}}   --}}
                            {{$school->videos->count()}} Lessons
                        </p>
                       @endif

                    </a>
                </div>
            </div>
            @endif
            @endforeach
            @endif
        </div>
    </div>
</section>



@guest

    <section class="login-wrapper">
        <video id="video1" preload="" autoplay="" muted="" playsinline="" loop="">
            <source src="{{asset('video/XTRACLASS_AD.mp4')}}" type="video/mp4">
        </video>
        <div id="videoMessage">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">

                        <div class="login-card">
                            <div class="sub-heading pb-4"><strong>Login </strong>to enable you ask questions, & connect with classmates</div>
                            <form action="{{ route('login') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input name="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" value="{{ old('username') }}" placeholder="{{ __('Username') }}">
                                    @if ($errors->has('username'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('username') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('Password') }}">
                                    @if ($errors->has('password'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary w-100">LOGIN</button>
                                </div>
                                <div class="form-group info-text">or register if you dont have an account</div>
                                <div class="form-group mb-0">
                                    <a href="{{ route('register') }}" class="btn btn-secondary w-100" title="Register">REGISTER</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

{{--    <section class="featured-carousel-wrapper bg-white">--}}
{{--        @include('frontend.includes.feature_tutors')--}}
{{--    </section>--}}
@endguest

<section class="any-device">
    <div class="container">
        <div class="row">
            <div class="col-md-7 offset-md-5 text-center text-md-right">
                <div class="device-info">
                    <div class="pb-0 pb-md-5" >
                        <h2 class="heading text-center text-md-right">Join from any device</h2>
                        <p class="heading-sub-text text-center text-md-right">PC, smartphone, tablet, iPhone etc.</p>
                    </div>
                    <div class="pt-2 pt-md-4">
                        <a href="{{route('frontend.pages.how_to_access')}}" class="btn-gray-outline">LEARN HOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<section class="pricing">--}}
{{--    <div class="container">--}}
{{--        <div class="row mb-5">--}}
{{--            <div class="col-sm-3">--}}
{{--            </div>--}}
{{--            <div class="col-sm-6">--}}
{{--                <h2 class="heading text-center">Our Pricing</h2>--}}
{{--            </div>--}}
{{--            <div class="col-sm-3">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-4">--}}
{{--                <div class="card shadow  mb-5">--}}
{{--                    <span class="pricing-header shadow-lg" id="per-month">--}}
{{--                        <h1 class="text-center">Per Month</h1>--}}
{{--                    </span>--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="text-body">--}}
{{--                            <h1 class="text-center"><sup class="text-muted">GH&cent;</sup><span class="price-amount">25</span></h1>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-footer">--}}
{{--                        <div class="text-center">--}}
{{--                            @guest--}}
{{--                            <button class="btn btn-primary subscribe" data-id="1" onclick="loginObj.openPopup()">Subscribe</button>--}}
{{--                            @endguest--}}
{{--                            @auth--}}

{{--                                    <button class="btn btn-primary subscribe" data-id="1" data-toggle="modal" data-target="#subscribeModal">Subscribe</button>--}}

{{--                            @endauth--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-4">--}}
{{--                <div class="card shadow mb-5">--}}
{{--                    <span class="pricing-header shadow-lg" id="per-term">--}}
{{--                        <h1 class="text-center">Per Term</h1>--}}
{{--                    </span>--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="text-body">--}}
{{--                            <h1 class="text-center"><sup class="text-muted">GH&cent;</sup><span class="price-amount">70</span></h1>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-footer">--}}
{{--                        <div class="text-center">--}}
{{--                            @guest--}}
{{--                            <button class="btn btn-success subscribe" data-id="2" onclick="loginObj.openPopup()">Subscribe</button>--}}
{{--                            @endguest--}}
{{--                            @auth--}}
{{--                                    <button class="btn btn-success subscribe" data-id="2" data-toggle="modal" data-target="#subscribeModal">Subscribe</button>--}}
{{--                                @endauth--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-4">--}}
{{--                <div class="card shadow">--}}
{{--                    <span class="pricing-header shadow-lg" id="per-year">--}}
{{--                        <h1 class="text-center">Per Year</h1>--}}
{{--                    </span>--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="text-body">--}}
{{--                            <h1 class="text-center"><sup class="text-muted">GH&cent;</sup><span class="price-amount">225</span></h1>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-footer">--}}
{{--                        <div class="text-center">--}}
{{--                            @guest--}}
{{--                            <button class="btn btn-warning btn-lg subscribe" data-id="3" onclick="loginObj.openPopup()">Subscribe</button>--}}
{{--                            @endguest--}}
{{--                            @auth--}}
{{--                                    <button class="btn btn-warning btn-lg subscribe" data-id="3" data-toggle="modal" data-target="#subscribeModal">Subscribe</button>--}}
{{--                                @endauth--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

<section class="contact-banner bg-white">
    <div class="container">
        <div class="contact-banner-box">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <aside>
                        <h3>Get your school online</h3>
                        <p>Contact us to get your school online at no cost</p>
                    </aside>
                </div>
                <div class="col-lg-5 offset-lg-2 mt-3 mt-lg-0">
                    <a href="{{route('frontend.pages.contact')}}" class="btn-white-outline w-100">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade custom-modal" id="subscribeModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="register-card">
                    <div class="register-card-header">
                        <h1 class="heading">Subscribe</h1>
                        <p>Kindly make payments to the XtraClass Mobile Money Number below to Subscribe</p>
                        <p>Account Name: <strong>Anita Mensah</strong></p>
                        <p>Account Number: <strong>054 300 8164</strong></p>
                        <p>Kindly use your XtraClass Username as reference and call 0509962708 to confirm</p>
                    </div>

{{--                    <form id="subscribeForm" action="{{route("frontend.action.pay")}}" method="post">--}}
{{--                        @csrf--}}
{{--                        <div class="register-card-body">--}}
{{--                            <div id="subError"></div>--}}
{{--                            <input type="hidden" name="activate_mode" value="self">--}}

{{--                            <div class="form-group" style="margin-top: -10px">--}}
{{--                                <label for="network">Wallet</label>--}}
{{--                                <select id="network" name="network" class="form-control {{ $errors->has('sub_wallet') ? 'is-invalid' : '' }}" data-validation="required">--}}
{{--                                    <option value="MTN" selected>MTN Mobile Money</option>--}}
{{--                                    <option value="VODAFONE" >Vodafone Cash</option>--}}
{{--                                    <option value="AIRTEL">Airtel Money</option>--}}
{{--                                    <option value="TIGO">Tigo Cash</option>--}}
{{--                                </select>--}}
{{--                                @if ($errors->has('sub_wallet'))--}}
{{--                                    <div class="invalid-feedback">{!! $errors->first('sub_wallet') !!}</div>--}}
{{--                                @else--}}
{{--                                    <small class="form-text">{{ "Select your payment network" }}</small>--}}
{{--                                @endif--}}
{{--                            </div>--}}

{{--                            <div class="form-group" style="margin-top: -15px">--}}
{{--                                <label for="momo_number">Mobile money number</label>--}}
{{--                                <div class="input-group">--}}
{{--                                    <div class="input-group-prepend">--}}
{{--                                        <span class="input-group-text"> +233 </span>--}}
{{--                                    </div>--}}
{{--                                    <input id="momo_number" name="momo_number" type="tel" class="form-control {{ $errors->has('sub_num') ? 'is-invalid' : '' }}" data-validation="required length number" data-validation-length="10-15" placeholder="0244123456">--}}
{{--                                </div>--}}
{{--                                @if ($errors->has('sub_num'))--}}
{{--                                    <div class="invalid-feedback">{!! $errors->first('sub_num') !!}</div>--}}
{{--                                @else--}}
{{--                                    <small class="form-text">{{ "A prompt will be sent to this number for approval" }}</small>--}}
{{--                                @endif--}}
{{--                            </div>--}}

{{--                            <div class="form-group" style="margin-top: -15px">--}}
{{--                                <label for="plan">Plans</label>--}}
{{--                                <select name="plan" id="plan" class="form-control {{ $errors->has('sub_plan') ? 'is-invalid' : '' }}" data-validation="required">--}}
{{--                                    <option value="" selected disabled>Select Plan</option>--}}
{{--                                </select>--}}

{{--                                @if ($errors->has('sub_plan'))--}}
{{--                                    <div class="invalid-feedback">{!! $errors->first('sub_plan') !!}</div>--}}
{{--                                @else--}}
{{--                                    <small class="form-text">{{ "Pick from our various plans what suites you" }}</small>--}}
{{--                                @endif--}}
{{--                            </div>--}}


{{--                        </div>--}}
{{--                        <hr class="mt-4">--}}
{{--                        <div class="register-card-footer">--}}

{{--                            <button type="submit"  id="submitSubscription" class="btn btn-primary w-100">Subscribe</button>--}}
{{--                        </div>--}}

{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>
</div>


<script>
//    $(".btn-group, .dropdown").hover(
//        function () {
//            $('>.dropdown-menu', this).stop(true, true).fadeIn("fast");
//            $(this).addClass('open');
//        },
//        function () {
//            $('>.dropdown-menu', this).stop(true, true).fadeOut("fast");
//            $(this).removeClass('open');
//    });


</script>

@endsection

@section('footer-scripts')
<script>
    $(document).ready(function(){
        $("#joinClass").on('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            let _token   = $('meta[name="csrf-token"]').attr('content');
            alert("Join A Class");
            window.location.reload();
        })

        $.ajax({
            url: '{{route("frontend.plan")}}',
            type: 'GET',
            data: {id: 1},
            success: function (data) {

                $('#plan').html(data);
            },
            error: function (error) {
                console.log(error);
            }
        });

        $('#subscribeForm').on('submit', function () {
            var errorDiv = $('#subError');
            var btn = $('#submitSubscription');
            btn.html('<span class="fa fa-spinner"></span> subscribing...').attr('disabled','disabled');

        })
    })
    /* $(document).ready(function(){
        $('.openChangeClassModal').on('click', function(){
            var cat_id = $(this).data('cat');
            var school_id = $(this).data('school');
            $('#institution_id2').val(cat_id);
            JoinAClass2.schoolList(true, 'institution', school_id);

            $("#myModal").modal();
        });
    }); */
</script>
@endsection
