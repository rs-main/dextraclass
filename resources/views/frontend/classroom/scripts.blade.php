@php
    $stream = \App\Stream::whereActive(true)->where("subject_id",$defaultVideo->topic->subject->id)
        ->whereClassId($defaultVideo->topic->subject->class_id)->latest()->first();
   $channel_id = $stream ? $stream->channel_id : "";
@endphp

@section('scripts')
<link rel="stylesheet" href="https://cdn.plyr.io/3.6.1/plyr.css" />
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdn.plyr.io/3.6.1/plyr.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{{ asset('js/join_a_class.js') }}"></script>
<script src="{{ asset('js/join_open_class.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
const player = new Plyr('#video_player_box',{
  settings: ['captions', 'quality', 'speed', 'loop'],
});

player.on('ended', event => {
	var nextVideo = $("#nextVideo").val();
	if(nextVideo != '' && nextVideo > 0 ){
		classroomObj.playVideo(nextVideo);
	}
  //player.restart();
});
</script>
<script>

var dataObj = {

    separator:'/',
    strDouble:'"',
    strSingle:"'",
    parentUrl:'{{URL::to("/")}}',
   jsId : {
        defaultClassroomDate        : "#defaultClassroomDate",
        nextVideo                   : "#nextVideo",
        forNextPlay                 : "#forNextPlay-",
        myModalFlagVideo            : "#myModalFlagVideo",
        flegStatus                  : "#flegStatus",
        setFlegBtnId                : "#setFlegBtnId",
        spamBtnHtm                  : "#spamBtnHtm",
        spamBtnId                   : "#spamBtnId",
        flegVideoInputId            : "#flegVideoInputId",
        videoPlayer    	            : "#videoPlayer",
        totalView    	            : "#totalView",
        totalViewsInput    	        : "#total-views-",
        notesBtnId    	            : "#notesBtnId",
       libraryNotesBtnId            : "#download-",
        archibeSearch    	        : "#archibeSearch",
        semesterBtnSearchBox    	: "#semesterBtnSearchBox",
        semesterYear        		: "#semesterYear",
        semesterSelect        		: "#semesterSelect",
        semesterDate        		: "#semesterDate",
        videoAction        			: "#videoAction",
        favBtnHtm        			: "#favBtnHtm",
        videoTopicInput    			: "#video-topic-",
        videoTopic       			: "#videoTopic",
        favBtnId        			: "#favBtnId",
        donloadNoteInput        	: "#donloadNoteInput",
        downloadLibraryNoteInput    : "#note-",
        defaultPlay        			: "#defaultPlay",
        myTabJust        			: "#myTabJust",
        videoTeacher       			: "#videoTeacher",
        currentVideo       			: "#currentVideo",
       currentSubject       		: "#currentSubject",
        videoPlayerBox        	    : "#video_player_box",
        videoUrl        			: "#video_url-",
        schoolsTabJust        		: "#schools-tab-just",
        coursesTabJust        		: "#courses-tab-just",
        activeTabInput        		: "#activeTabInput",
        playinglList        		: "#playinglList",
        questionsList           	: "#questionsList",
        archiveList           		: "#archiveList",
        libraryList           		: "#libraryList",
        angleSpanId               	: "#angle-span-",
        playBtnId               	: "#play-btn-",
        playingMore               	: "#playingMore",
        questionsMore              	: "#questionsMore",
        archiveMore               	: "#archiveMore",
        libraryMore               	: "#libraryMore",
        favouritesMore             	: "#favouritesMore",
        playingPage               	: "#playingPage",
        questionsPage              	: "#questionsPage",
        archivePage               	: "#archivePage",
        favouritesPage            	: "#favouritesPage",
        libraryPage            	    : "#libraryPage",
        videoShowing            	: "#videoShowing",
        videoSubject            	: "#videoSubject",
        videoTitleInput          	: "#video-title-",
        videoSubjectInput          	: "#video-subject-",
        videoTeacherInput          	: "#video-teacher-",
        favouritesList           	: "#favouritesList",
        postQuestionsBtn           	: "#postQuestionsBtn-",
        activetedTab            	: "#activetedTab",
        cartPlaying             	: "#cart-playing-",
        replyBoxId              	: "#reply-box-id-",
        collapseId              	: "#collapse-",
        askQuestion             	: "#ask_question-",
        addUserLibraryId            : "#lib-",
       topic            : "#topic-",
       class            : "#class-",
       tutor            : "#tutor-",
       desc            : "#desc-",
       added            : "#added-",
    },
	status : {
        success     : 200
    },
    jsClass : {
        forNextPlay 	        : ".forNextPlay",
        navItem 				: ".nav-item",
        navLink 				: ".nav-link",
        accordionToggle 		: ".accordion-toggle",
        acHeader 				: ".ac-header",
        collapseJs 				: ".collapse-js",
        angleSpan 				: ".angle-span",
        playBtn 				: ".play-btn",
        playing 				: ".playing",
        contentOpen 			: ".content-open",
        replyBoxClass 			: ".replyBoxClass",
        active 					: ".active",
        dnone 					: ".d-none"
    },
	jsData : {
        playing 				: "playing",
        questions 				: "questions",
        archive 				: "archive",
        favourites 				: "favourites",
        library 				: "library"
    },
	jsNames : {
        noname 			: "",
    },
	jsValue : {
        categoryName		: "{{$defaultVideo->school->category_name}}",
        classroom_id		: "{{$classroom_id}}",
        school_id		    : "{{$defaultVideo->school_id}}",
        course_id		    : "{{$defaultVideo->course_id}}",
        class_id		    : "{{$defaultVideo->class_id}}",
        video    			: "{{$defaultVideo->id}}",
        playOn    			: "{{$defaultVideo->play_on}}"
    },
    extra : {
        jsSeparator:'-',
		url : {
				playing     : '{{route("frontend.playingData")}}',
				questions   : '{{route("frontend.questionsData")}}',
				archive     : '{{route("frontend.archiveData")}}',
				favourites  : '{{route("frontend.favouritesData")}}',
				library     : '{{route("frontend.libraryData")}}',
				libraryFilter        : '{{route("frontend.libraryFilterData")}}',
				libraryFilterRecent  : '{{route("frontend.libraryFilterRecent")}}',
				playVideo            : '{{route("frontend.playVideo")}}',
				postQuestions        : '{{route("frontend.postQuestions")}}',
				setFavourites        : '{{route("frontend.setFavourites")}}',
				flegVideo            : '{{route("frontend.flegVideo")}}',
				archiveSearch        : '{{route("frontend.archiveSearch")}}',
				getSemesterOptions   : '{{route("frontend.getSemesterOptions")}}',
				getSemesterDaterange : '{{route("frontend.getSemesterDaterange")}}',
				studentDownloads     : '{{route("frontend.studentDownloads")}}',
                studentAddLibrary    : '{{route("frontend.studentAddLibrary")}}',
                removeFromUserLibrary    : '{{route("frontend.removeFromUserLibrary")}}',
                             quiz    : '{{route("frontend.takeQuiz")}}'
		}
    },
	createUrl : function(set){
        return ( this.parentUrl+this.separator+set);
    },
	createId : function(arr){
        return ( arr.join(''));
    }
}

/* import {Classroom} from "/js/classroom.js";
(function() {  window.classroomObj = new Classroom(dataObj);  })();   */
</script>
<script src="{{ asset('js/classroom.js') }}"></script>

<script>
    //function to embed PDF on page
    function viewBook(book){
        $('#libraryList').css('height', '500px')
        PDFObject.embed(book, "#libraryList");
    }
</script>

    <script>
        // script to show or hide calendar on classroom page
        $(document).ready(function () {
            $('#showCalender').on('click', function () {
               $('#videoInfoSection').fadeOut(500, function () {
                   $('#calendarDiv').fadeIn(500);
               });
            });

            $('#cancel').on('click', function () {
                $('#calendarDiv').fadeOut(500, function () {
                    $('#videoInfoSection').fadeIn(500);
                });
            });
        })


        const class_id                 =  "{{$defaultVideo->topic->subject->class_id}}";
        const subject_id               =  "{{$defaultVideo->topic->subject->id}}";
        const joinLiveButton           =  $("#join-live-button");
        const exitLiveButton           =  $("#exit-live-button");
        const streamVideoPlayer        =  $("#stream_video_player_box");
        const videoPlayer              =  $("#video_player_box");
        const streamVideoPlayerIframe  =  $(".stream-video-player");
        const audience_stream_url      =  "/stream-audience?channel_id=";
        const start_event              = "started";
        const stop_event               = "stopped";
        const channel_id               = "{{$channel_id}}"
        const channel                  = pusher.subscribe('streaming-channel');

        function startStream() {
            streamVideoPlayer.hide();
            videoPlayer.show();
            joinLiveButton.show();
            exitLiveButton.hide();
        }

        function stopStream() {
            exitLiveButton.hide();
            joinLiveButton.hide();
        }

        function classAndSubjectStreaming(data){
            return data.class_id == class_id && data.subject_id == subject_id;
        }

        function startedEvent(data){
            return data.status === start_event;
        }

        function stoppedEvent(data){
            return data.status === stop_event;
        }

        channel.bind('streaming-event', function(data) {
            console.log("event " + data.status )
            console.log( data )

            if (classAndSubjectStreaming(data)) {

                if (startedEvent(data)) {
                    startStream();
                }else if (stoppedEvent(data)) {
                    stopStream();
                }

                streamVideoPlayerIframe.attr("src", audience_stream_url + data.streaming_channel);
                console.log((data));
            }
        });

        joinLiveButton.on("click",function (){
            streamVideoPlayer.show();
            videoPlayer.hide();
            exitLiveButton.show();
            joinLiveButton.hide();
            player.stop();
        });

        exitLiveButton.on("click",function (){
            streamVideoPlayer.hide();
            videoPlayer.show();
            exitLiveButton.hide();
            joinLiveButton.show();
            var iframe = document.querySelector(".stream-video-player");
            iframe.src = iframe.src;
        });

        if (channel_id){
            streamVideoPlayerIframe.attr("src", audience_stream_url + "{{$channel_id}}");
            startStream()
        }

    </script>

@stop

