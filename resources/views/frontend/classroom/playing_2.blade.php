@php

    ob_start();
    $next_unlocked_video_period_weight = null;
    $current_video_weight = 0;

@endphp

<div class="col-12">


    <!--Accordion wrapper-->
    <div class="accordion accordion-playing" id="accordion2">
        @if(!empty($videos[0]))
        @foreach($videos as $key => $video)
            @if($loadMore != 1 && $key == 0)
                <input  type="hidden" id="defaultPlay" value="{{$video->id}}">
            @endif
            <input  type="hidden" id="forNextPlay-{{$video->id}}" class="forNextPlay" value="{{$video->id}}">
            <input  type="hidden" id="video_url-{{$video->id}}" value="{{$video->videoURL()}}">
            <input  type="hidden" id="video-title-{{$video->id}}" value="{{$video->title}}">
            <input  type="hidden" id="video-teacher-{{$video->id}}" value="{{$video->tutor->fullname}}">
            <input  type="hidden" id="total-views-{{$video->id}}" value="{{$video->total_views != '' ? $video->total_views : 0 }} Views">
            <input  type="hidden" id="video-subject-{{$video->id}}" value="{!!$video->topic->subject->subject_name!!}">
            <input  type="hidden" id="video-topic-{{$video->id}}" value="{!!$video->topic->topic_name!!}">

            <!-- Accordion card -->
            <div id="cart-playing-{{$video->id}}" class="card mb-2 ">

                <!-- Card header -->
                <div class="card-header d-flex align-items-center " id="heading-{{$video->id}}">
                    @if(empty($video->checkQuiz))
                       @include("frontend.classroom.partials.unlocked_video")
                    @else

                        @if(!empty($video->Quiz))

                          {{-- Video has a quiz--}}

                            @if($video->check_quiz_lesson($video->subject_id) > 0)

                                @if($quizTakenStatus)

                                    {{-- Video has quiz and checks your quiz taken status --}}

                                    @foreach($quizTaken as $taken)
                                        @if($taken->quiz_id == $video->Quiz->id)


                                            @if( $video->get_student_mark($taken->quiz_id, $video->subject_id) < $video->Quiz->get_pass_mark($taken->quiz_id))

                                                @include("frontend.classroom.partials.locked_video")

                                            @else

                                                @php
                                                      //$next_unlocked_video_period_weight = $video->getNextVideo($video->id, $video->class_id);
                                                      $next_unlocked_video_period_weight = $video->topic->weight+1;
                                                @endphp

{{--                                                {{$next_unlocked_video_period_weight }}--}}

                                                @include("frontend.classroom.partials.unlocked_video")

                                            @endif
                                        @else

                                            @include("frontend.classroom.partials.locked_video")

                                        @endif

                                    @endforeach

                                @else

                                    @if($video->get_quiz_lesson($video->subject_id) >= $video->topic_id)

                                        @include("frontend.classroom.partials.unlocked_video")

                                    @else

                                        @include("frontend.classroom.partials.locked_video")

                                    @endif

                                @endif

                            @else

                                @include("frontend.classroom.partials.unlocked_video")

                            @endif
                        @else
                            @if($video->check_quiz_lesson($video->subject_id) > 0)
                                @if($quizTakenStatus)
                                    @foreach($quizTaken as $taken)

                                        @if( $video->get_student_mark($taken->quiz_id, $video->subject_id)< $video->get_pass_mark($taken->quiz_id))

{{--                                            @include("frontend.classroom.partials.locked_video")--}}
                                            @include("frontend.classroom.partials.unlocked_video")

                                        @else
                                       {{-- Unlocked Videos after taking tests--}}


                                            @if($video->id == $next_unlocked_video_period_weight)

                                                @include("frontend.classroom.partials.unlocked_video")
                                            @else
                                                @if($video->quiz)
                                                @include("frontend.classroom.partials.locked_video")

                                                @else

                                                    @include("frontend.classroom.partials.unlocked_video")

                                                    @endif
                                            @endif

                                        @endif
                                    @endforeach

                                @else
                                    @if($video->get_quiz_lesson($video->subject_id) >= $video->topic_id)

                                        @include("frontend.classroom.partials.unlocked_video")

                                    @else

                                        @include("frontend.classroom.partials.locked_video")

                                    @endif

                                @endif

                            @else



                                @include("frontend.classroom.partials.unlocked_video")

                            @endif
                        @endif

                    @endif
                </div>

                <!-- Card body -->
                <div id="accordion2-{{$video->id}}" class="collapse {{ ($key==0) ? 'show' : 'hide' }}" data-parent="#accordion2" id="collapse-{{$video->id}}" aria-labelledby="heading-{{$video->id}}">
                    <div class="card-body">
                        <p>{!!$video->description!!} </p>
                    </div>
                </div>
            </div>
            <!-- Accordion card -->

            {{-- Take Quiz View --}}
            @include("frontend.classroom.partials.quiz")

        @endforeach

        @else
            <div class="list-view-sec" style="text-align:center">
                <img src="{{asset('images/norecord.png')}}">
                <h3 class="text-info">No records found</h3>
            </div>
        @endif

        </div>
    <!--/.Accordion wrapper-->

</div>


 @php
 $content = ob_get_contents();
 $result['resultHtml']  = $content;
 $result['tab']         = $tab;
 $result['loadMore']    = $loadMore;
 $result['videos']    = $videos;
 $result['page']        = $page+1;
 $result['totalRecord'] = $videos->total();
 $result['lastPage']    = $videos->lastPage();
 $result['to']          = $videos->lastItem();
 if($page >= $result['lastPage']){
     $result['show_morerecords'] = 0;
 }
 else {
    $result['show_morerecords'] = 1;
 }

 ob_end_clean();
 echo json_encode($result);
 @endphp
