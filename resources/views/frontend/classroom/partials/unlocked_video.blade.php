<div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
    @if($video->video_watch_count > 0)
    <i class="ion ion-md-eye" aria-hidden="true"></i>
    @else
    <i class="fas" aria-hidden="true"></i>
    @endif
</div>
<div class="col flex-grow-1" >
    <h4 class="m-0">
        <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

            <span class="time-slot">
                {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
            </span>
            {!!$video->title!!}
        </a>
    </h4>
</div>

<div class="col flex-grow-0 ">
    <div class="arrow">
        <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
            <span class="ion ion-md-arrow-dropdown"></span>
        </a>
    </div>

</div>
