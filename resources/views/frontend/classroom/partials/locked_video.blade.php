<div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
    <i class="ion ion-md-lock" aria-hidden="true"></i>
</div>
<div class="col flex-grow-1" >
    <h4 class="m-0">
        <a href="javascript:void(0)">

            <span class="time-slot">
                {{$video->Period->title}} <span class="ion ion-md-arrow-dropright"></span>
            </span>

            {!!$video->title!!}
        </a>
    </h4>
</div>

<div class="col flex-grow-0 ">
    <div class="arrow">
        <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
            <span class="ion ion-md-arrow-dropdown"></span>
        </a>
    </div>

</div>
