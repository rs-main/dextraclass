<html>
<head>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@clappr/player@latest/dist/clappr.min.js"></script>
    <script type="text/javascript"
            src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>

    <title></title>
</head>

<body>
<div id="player"></div>
<script>

    let params = location.href.split('?')[1].split('&');
    data = {};
    for (x in params)
    {
        data[params[x].split('=')[0]] = params[x].split('=')[1];
    }

    const base_url = "https://video.desafrica.com/"
    let video_id = (data.video_id)

    const playerElement = document.getElementById("player");

    let requestOptions = {
        method: 'GET',
        Accept: 'application/json'
    };

    fetch(`${base_url}api/v1/media/${video_id}`, requestOptions)
    .then((res=>res.json()))
    .then(function (data){
        console.log(data.encodings_info);
        let encodings = data.encodings_info;
        let vid_480 = base_url+encodings["480"]["h264"]["url"];
        console.log(vid_480, "240");
        let player = new Clappr.Player({
            source: vid_480,
            poster: `${data.thumbnail_url}`,
            mute: true,
            height: 360,
            width: 640,
            plugins: [LevelSelector],
            levelSelectorConfig: {
                title: 'Quality',
                labels: {
                    2: 'High', // 500kbps
                    1: 'Med', // 240kbps
                    0: 'Low', // 120kbps
                },
                labelCallback: function(playbackLevel, customLabel) {
                    return customLabel + playbackLevel.level.height+'p'; // High 720p
                }
            },

        });

        player.attachTo(playerElement);
    })

</script>

<script>

</script>
</body>

</html>
