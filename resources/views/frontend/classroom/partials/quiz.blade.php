@if(!empty($video->checkQuiz) && !empty($video->Quiz))

    @if($video->Quiz->lesson_id == $video->topic_id)
        @if(!empty($quizTaken[0]))
            @foreach($quizTaken as $taken)
                @if($taken->quiz_id == $video->Quiz->id)
                    @if( $video->get_student_mark($taken->quiz_id, $video->subject_id) < $video->Quiz->get_pass_mark($taken->quiz_id))
                        <div class="card-danger mb-2">
                            <!-- Card header -->
                            <div class="card-header d-flex align-items-center ">
                                <div class="play-btn col px-0 flex-grow-0" >

                                </div>
                                <div class="col flex-grow-1" >
                                    <h4 class="m-0">
                                        <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                            {!!$video->Quiz->title!!}
                                        </a>
                                    </h4>
                                </div>

                                <div class="col flex-grow-0 ">
                                    <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}}, 'retake')">Retake</button>

                                </div>

                            </div>

                        </div>
                    @else
                        <div class="card-success mb-2">
                            <!-- Card header -->
                            <div class="card-header d-flex align-items-center ">
                                <div class="play-btn col px-0 flex-grow-0" >

                                </div>
                                <div class="col flex-grow-1" >
                                    <h4 class="m-0">
                                        <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                            {!!$video->Quiz->title!!}
                                        </a>
                                    </h4>
                                </div>

                                <div class="col flex-grow-0 ">
                                    <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}}, 'retake')">Passed</button>

                                </div>

                            </div>

                        </div>
                    @endif
                @else
                    <div class="card-quiz mb-2">
                        <!-- Card header -->
                        <div class="card-header d-flex align-items-center ">
                            <div class="play-btn col px-0 flex-grow-0" >

                            </div>
                            <div class="col flex-grow-1" >
                                <h4 class="m-0">
                                    <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                        {!!$video->Quiz->title!!}
                                    </a>
                                </h4>
                            </div>

                            <div class="col flex-grow-0 ">
                                <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}})">Take</button>

                            </div>

                        </div>

                    </div>
                @endif
            @endforeach
        @else
            <div class="card-quiz mb-2">
                <!-- Card header -->
                <div class="card-header d-flex align-items-center ">
                    <div class="play-btn col px-0 flex-grow-0" >

                    </div>
                    <div class="col flex-grow-1" >
                        <h4 class="m-0">
                            <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                {!!$video->Quiz->title!!}
                            </a>
                        </h4>
                    </div>

                    <div class="col flex-grow-0 ">
                        <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}})">Take</button>

                    </div>

                </div>

            </div>
        @endif
    @endif

@endif
