@php
    ob_start();
    $next_module_unlocked = "";
 @endphp

<div class="col-12">


    <!--Accordion wrapper-->
    <div class="accordion accordion-playing" id="accordion2">
        @if(!empty($videos[0]))
        @foreach($videos as $key => $video)
            @if($loadMore != 1 && $key == 0)
                <input  type="hidden" id="defaultPlay" value="{{$video->id}}">
            @endif
            <input  type="hidden" id="forNextPlay-{{$video->id}}" class="forNextPlay" value="{{$video->id}}">
            <input  type="hidden" id="video_url-{{$video->id}}" value="{{$video->videoURL()}}">
            <input  type="hidden" id="video-title-{{$video->id}}" value="{{$video->title}}">
            <input  type="hidden" id="video-teacher-{{$video->id}}" value="{{$video->tutor->fullname}}">
            <input  type="hidden" id="total-views-{{$video->id}}" value="{{$video->total_views != '' ? $video->total_views : 0 }} Views">
            <input  type="hidden" id="video-subject-{{$video->id}}" value="{!!$video->topic->subject->subject_name!!}">
            <input  type="hidden" id="video-topic-{{$video->id}}" value="{!!$video->topic->topic_name!!}">
            <!-- Accordion card -->
            <div id="cart-playing-{{$video->id}}" class="card mb-2 ">

                <!-- Card header -->
                <div class="card-header d-flex align-items-center " id="heading-{{$video->id}}">
                    @if(empty($video->checkQuiz))
                        <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                            @if($video->video_watch_count > 0)
                                <i class="ion ion-md-eye" aria-hidden="true"></i>
                            @else
                                <i class="fas" aria-hidden="true"></i>
                            @endif
                        </div>
                        <div class="col flex-grow-1" >
                            <h4 class="m-0">
                                <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

                                            <span class="time-slot">
                                                {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                            </span>
                                    {!!$video->title!!}
                                </a>
                            </h4>
                        </div>

                        <div class="col flex-grow-0 ">
                            <div class="arrow">
                                <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                    <span class="ion ion-md-arrow-dropdown"></span>
                                </a>
                            </div>

                        </div>
                    @else

                        @if(!empty($video->Quiz))

                            @if($video->check_quiz_lesson($video->subject_id) > 0)
                                @if($quizTakenStatus)
                                    @foreach($quizTaken as $taken)
                                        @if($taken->quiz_id == $video->Quiz->id)
                                            @if( $video->get_student_mark($taken->quiz_id, $video->subject_id) < $video->Quiz->get_pass_mark($taken->quiz_id))

                                                <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">

                                                    <i class="ion ion-md-lock" aria-hidden="true"></i>
                                                </div>
                                                <div class="col flex-grow-1" >
                                                    <h4 class="m-0">
                                                        <a href="javascript:void(0)">

                                                    <span class="time-slot">
                                                        {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                    </span>
                                                            {!!$video->title!!}
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div class="col flex-grow-0 ">
                                                    <div class="arrow">
                                                        <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                            <span class="ion ion-md-arrow-dropdown"></span>
                                                        </a>
                                                    </div>

                                                </div>
                                            @else
                                                @php
                                                $next_module_unlocked = $video->period->weight+1;
                                                @endphp

                                                <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                                    @if($video->video_watch_count > 0)
                                                        <i class="ion ion-md-eye" aria-hidden="true"></i>
                                                    @else
                                                        <i class="fas" aria-hidden="true"></i>
                                                    @endif
                                                </div>
                                                <div class="col flex-grow-1" >
                                                    <h4 class="m-0">
                                                        <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

                                                        <span class="time-slot">
                                                            {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                        </span>
                                                            {!!$video->title!!}
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div class="col flex-grow-0 ">
                                                    <div class="arrow">
                                                        <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                            <span class="ion ion-md-arrow-dropdown"></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                                <i class="ion ion-md-lock" aria-hidden="true"></i>
                                            </div>
                                            <div class="col flex-grow-1" >
                                                <h4 class="m-0">
                                                    <a href="javascript:void(0)">

                                                    <span class="time-slot">
                                                        {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                    </span>
                                                        {!!$video->title!!}
                                                    </a>
                                                </h4>
                                            </div>

                                            <div class="col flex-grow-0 ">
                                                <div class="arrow">
                                                    <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                        <span class="ion ion-md-arrow-dropdown"></span>
                                                    </a>
                                                </div>

                                            </div>

                                        @endif
                                    @endforeach

                                @else
                                    @if($video->get_quiz_lesson($video->subject_id) >= $video->topic_id)
                                        <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                            @if($video->video_watch_count > 0)
                                                <i class="ion ion-md-eye" aria-hidden="true"></i>
                                            @else
                                                <i class="fas" aria-hidden="true"></i>
                                            @endif
                                        </div>
                                        <div class="col flex-grow-1" >
                                            <h4 class="m-0">
                                                <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

                                            <span class="time-slot">
                                                {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                            </span>
                                                    {!!$video->title!!}
                                                </a>
                                            </h4>
                                        </div>

                                        <div class="col flex-grow-0 ">
                                            <div class="arrow">
                                                <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                    <span class="ion ion-md-arrow-dropdown"></span>
                                                </a>
                                            </div>

                                        </div>

                                    @else
                                        <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                            <i class="ion ion-md-lock" aria-hidden="true"></i>
                                        </div>
                                        <div class="col flex-grow-1" >
                                            <h4 class="m-0">
                                                <a href="javascript:void(0)">

                                                    <span class="time-slot">
                                                        {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                    </span>
                                                    {!!$video->title!!}
                                                </a>
                                            </h4>
                                        </div>

                                        <div class="col flex-grow-0 ">
                                            <div class="arrow">
                                                <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                    <span class="ion ion-md-arrow-dropdown"></span>
                                                </a>
                                            </div>

                                        </div>

                                    @endif

                                @endif

                            @else
                                <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                    @if($video->video_watch_count > 0)
                                        <i class="ion ion-md-eye" aria-hidden="true"></i>
                                    @else
                                        <i class="fas" aria-hidden="true"></i>
                                    @endif
                                </div>
                                <div class="col flex-grow-1" >
                                    <h4 class="m-0">
                                        <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

                                            <span class="time-slot">
                                                {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                            </span>
                                            {!!$video->title!!}
                                        </a>
                                    </h4>
                                </div>

                                <div class="col flex-grow-0 ">
                                    <div class="arrow">
                                        <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                            <span class="ion ion-md-arrow-dropdown"></span>
                                        </a>
                                    </div>

                                </div>
                            @endif
                        @else
                            @if($video->check_quiz_lesson($video->subject_id) > 0)
                                @if($quizTakenStatus)
                                    @foreach($quizTaken as $taken)

                                        @if( $video->get_student_mark($taken->quiz_id, $video->subject_id)< $video->get_pass_mark($taken->quiz_id))
                                            <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                                <i class="ion ion-md-lock" aria-hidden="true"></i>
                                            </div>
                                            <div class="col flex-grow-1" >
                                                <h4 class="m-0">
                                                    <a href="javascript:void(0)">

                                                    <span class="time-slot">
                                                        {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                    </span>
                                                        {!!$video->title!!}
                                                    </a>
                                                </h4>
                                            </div>

                                            <div class="col flex-grow-0 ">
                                                <div class="arrow">
                                                    <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                        <span class="ion ion-md-arrow-dropdown"></span>
                                                    </a>
                                                </div>

                                            </div>
                                        @else
                                            <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                                @if($video->video_watch_count > 0)
                                                    <i class="ion ion-md-eye" aria-hidden="true"></i>
                                                @else
                                                    <i class="fas" aria-hidden="true"></i>
                                                @endif
                                            </div>
                                            <div class="col flex-grow-1" >
                                                <h4 class="m-0">
                                                    <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

                                                        <span class="time-slot">
                                                            {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                        </span>
                                                        {!!$video->title!!}
                                                    </a>
                                                </h4>
                                            </div>

                                            <div class="col flex-grow-0 ">
                                                <div class="arrow">
                                                    <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                        <span class="ion ion-md-arrow-dropdown"></span>
                                                    </a>
                                                </div>

                                            </div>


                                        @endif
                                    @endforeach

                                @else
                                    @if($video->get_quiz_lesson($video->subject_id) >= $video->topic_id)
                                        <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                            @if($video->video_watch_count > 0)
                                                <i class="ion ion-md-eye" aria-hidden="true"></i>
                                            @else
                                                <i class="fas" aria-hidden="true"></i>
                                            @endif
                                        </div>
                                        <div class="col flex-grow-1" >
                                            <h4 class="m-0">
                                                <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

                                            <span class="time-slot">
                                                {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                            </span>
                                                    {!!$video->title!!}
                                                </a>
                                            </h4>
                                        </div>

                                        <div class="col flex-grow-0 ">
                                            <div class="arrow">
                                                <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                    <span class="ion ion-md-arrow-dropdown"></span>
                                                </a>
                                            </div>

                                        </div>

                                    @else
                                        <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                            <i class="ion ion-md-lock" aria-hidden="true"></i>
                                        </div>
                                        <div class="col flex-grow-1" >
                                            <h4 class="m-0">
                                                <a href="javascript:void(0)">

                                                    <span class="time-slot">
                                                        {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                    </span>
                                                    {!!$video->title!!}
                                                </a>
                                            </h4>
                                        </div>

                                        <div class="col flex-grow-0 ">
                                            <div class="arrow">
                                                <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                                    <span class="ion ion-md-arrow-dropdown"></span>
                                                </a>
                                            </div>

                                        </div>

                                    @endif

                                @endif

                            @else
                                <div class="play-btn col px-0 flex-grow-0" id="play-btn-{{$video->id}}">
                                    @if($video->video_watch_count > 0)
                                        <i class="ion ion-md-eye" aria-hidden="true"></i>
                                    @else
                                        <i class="fas" aria-hidden="true"></i>
                                    @endif
                                </div>
                                <div class="col flex-grow-1" >
                                    <h4 class="m-0">
                                        <a href="javascript:void(0)" onclick="classroomObj.playVideo({{$video->id}})">

                                            <span class="time-slot">
                                                {{$video->Period->title}} &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                            </span>
                                            {!!$video->title!!}
                                        </a>
                                    </h4>
                                </div>

                                <div class="col flex-grow-0 ">
                                    <div class="arrow">
                                        <a data-toggle="collapse" aria-expanded="true" href="#accordion2-{{$video->id}}" data-lession_id="{{$video->id}}"  data-class_id="{{$video->class_id}}">
                                            <span class="ion ion-md-arrow-dropdown"></span>
                                        </a>
                                    </div>

                                </div>
                            @endif
                        @endif

                    @endif
                </div>

                <!-- Card body -->
                <div id="accordion2-{{$video->id}}" class="collapse {{ ($key==0) ? 'show' : 'hide' }}" data-parent="#accordion2" id="collapse-{{$video->id}}" aria-labelledby="heading-{{$video->id}}">
                    <div class="card-body">
                        <p>{!!$video->description!!} </p>
                    </div>
                </div>
            </div>
            <!-- Accordion card -->
                @if(!empty($video->checkQuiz) && !empty($video->Quiz))

                    @if($video->Quiz->lesson_id == $video->topic_id)
                        @if(!empty($quizTaken[0]))
                            @foreach($quizTaken as $taken)
                                @if($taken->quiz_id == $video->Quiz->id)
                                    @if( $video->get_student_mark($taken->quiz_id, $video->subject_id) < $video->Quiz->get_pass_mark($taken->quiz_id))
                                    <div class="card-danger mb-2">
                                        <!-- Card header -->
                                        <div class="card-header d-flex align-items-center ">
                                            <div class="play-btn col px-0 flex-grow-0" >

                                            </div>
                                            <div class="col flex-grow-1" >
                                                <h4 class="m-0">
                                                    <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                                        {!!$video->Quiz->title!!}
                                                    </a>
                                                </h4>
                                            </div>

                                            <div class="col flex-grow-0 ">
                                                <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}}, 'retake')">Retake</button>

                                            </div>

                                        </div>

                                    </div>
                                    @else
                                        <div class="card-success mb-2">
                                            <!-- Card header -->
                                            <div class="card-header d-flex align-items-center ">
                                                <div class="play-btn col px-0 flex-grow-0" >

                                                </div>
                                                <div class="col flex-grow-1" >
                                                    <h4 class="m-0">
                                                        <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                                            {!!$video->Quiz->title!!}
                                                        </a>
                                                    </h4>
                                                </div>

                                                <div class="col flex-grow-0 ">
                                                    <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}}, 'retake')">Retake</button>

                                                </div>

                                            </div>

                                        </div>
                                    @endif
                                @else
                                    <div class="card-quiz mb-2">
                                        <!-- Card header -->
                                        <div class="card-header d-flex align-items-center ">
                                            <div class="play-btn col px-0 flex-grow-0" >

                                            </div>
                                            <div class="col flex-grow-1" >
                                                <h4 class="m-0">
                                                    <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                                        {!!$video->Quiz->title!!}
                                                    </a>
                                                </h4>
                                            </div>

                                            <div class="col flex-grow-0 ">
                                                <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}})">Take</button>

                                            </div>

                                        </div>

                                    </div>
                                @endif
                            @endforeach
                        @else
                            <div class="card-quiz mb-2">
                                <!-- Card header -->
                                <div class="card-header d-flex align-items-center ">
                                    <div class="play-btn col px-0 flex-grow-0" >

                                    </div>
                                    <div class="col flex-grow-1" >
                                        <h4 class="m-0">
                                            <a href="javascript:void(0)" >

                                                <span class="time-slot">
                                                    Quiz &nbsp; <span class="ion ion-md-arrow-dropright"></span>
                                                </span>
                                                {!!$video->Quiz->title!!}
                                            </a>
                                        </h4>
                                    </div>

                                    <div class="col flex-grow-0 ">
                                        <button class="btn btn-custom" onclick="classroomObj.takeQuiz({{$video->Quiz->id}})">Take</button>

                                    </div>

                                </div>

                            </div>
                        @endif
                    @endif

                @endif

        @endforeach

        @else
            <div class="list-view-sec" style="text-align:center">
                <img src="{{asset('images/norecord.png')}}">
                <h3 class="text-info">No records found</h3>
            </div>
        @endif

        </div>
    <!--/.Accordion wrapper-->

</div>


 @php
 $content = ob_get_contents();
 $result['resultHtml']  = $content;
 $result['tab']         = $tab;
 $result['loadMore']    = $loadMore;
 $result['videos']    = $videos;
 $result['page']        = $page+1;
 $result['totalRecord'] = $videos->total();
 $result['lastPage']    = $videos->lastPage();
 $result['to']          = $videos->lastItem();
 if($page >= $result['lastPage']){
     $result['show_morerecords'] = 0;
 }
 else {
    $result['show_morerecords'] = 1;
 }

 ob_end_clean();
 echo json_encode($result);
 @endphp
