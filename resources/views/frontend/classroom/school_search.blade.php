@php ob_start() @endphp

@if(!empty($schools[0]))
	@foreach($schools as $school)
									
  <div class="col-md-3">
    <div class="thumbnail">
      
        <img src="https://www.w3schools.com/w3images/lights.jpg" alt="Lights" style="width:100%" class="img-rounded">
        <div class="caption">
          <h5>{!! $school->title !!}</h5>
        </div>
      
	  
    </div>
  </div>
  @endforeach
@else
	 <div class="list-view-sec" style="padding-left:35%; text-align:center">
         <h1>No Record Available</h1> 
     </div>
@endif	
  
  
 @php 
 $content = ob_get_contents();
 $result['resultHtml'] = $content;
 $result['tab'] = $tab;
 $result['loadMore'] = $loadMore;
 
 ob_end_clean();
 echo json_encode($result);
 @endphp 