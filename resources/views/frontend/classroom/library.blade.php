@php ob_start() @endphp

@if(!empty($libraryItems[0]))
    <div class="row mt-3">
        @foreach($libraryItems as $libraryItem)
            <div class="col-md-6 col-lg-4">
                <div class="card shadow library-item">
                    <img src="{{asset('images/book.png')}}" class="card-img" alt="{{$libraryItem->topic_name}}">
                    <div class="card-body">
                        <h4 style="color: #777777">{{$libraryItem->topic_name}}</h4>
                        <p style="color: #000">{{$libraryItem->description}}</p>
                        <a href="#"> <span style="color: #585858;">by&nbsp;</span> <span style="text-decoration: underline">{{$libraryItem->teacher_name}}</span></a>
                        <p>Added {{$libraryItem->added}}</p>
                        <p><span class="fa fa-eye"></span> {{$libraryItem->total_read}} Read</p>
                    </div>
                    @if(!isset($libraryItem->ext))
                        <div class="card-footer" style="color: #fff; background-color: #d8b425; border-radius: 0 0 10px 10px; padding-bottom: 0px; margin-top: -20px">
                            <h3 style="color: #fff">File</h3>
                        </div>
                    @elseif($libraryItem->ext == 'pdf')
                        <div class="card-footer" style="color: #fff; background-color: #268afc; border-radius: 0 0 10px 10px; padding-bottom: 0px; margin-top: -20px">
                            <h3 style="color: #fff">Book</h3>
                        </div>
                    @else
                        <div class="card-footer" style="color: #fff; background-color: #c959e5; border-radius: 0 0 10px 10px; padding-bottom: 0px; margin-top: -20px">
                            <h3 style="color: #fff">Slide</h3>
                        </div>
                    @endif

                    <div class="library-item-hover">
                        @if(!isset($libraryItem->ext))

                        @elseif($libraryItem->ext == 'pdf' || $libraryItem->ext == 'ppt' || $libraryItem->ext == 'pptx' || $libraryItem->ext == 'doc' || $libraryItem->ext == 'docx')
                            <a href="javascript:void(0)" onclick="viewBook('{{$libraryItem->notes_url}}')">
                                <button class="btn btn-outline-light btn-lg">{{__('READ')}}</button>
                            </a>
                            <hr>
                        @else
                            <a href="{{$libraryItem->notes_url}}" target="_blank">
                                <button class="btn btn-outline-light btn-lg">{{__('VIEW')}}</button>
                            </a>
                            <hr>
                        @endif
{{--                        <div class="form-check">--}}
{{--                            <input type="checkbox" class="form-check-input" id="pib" >--}}
{{--                            <label style="color: #fff; font-size: 1.1rem" class="form-check-label" for="pib">Put into bag</label>--}}
{{--                        </div>--}}
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="lib-{{$libraryItem->noteId}}" onchange="classroomObj.addToUserLibrary({{$libraryItem->noteId}})" {{$libraryItem->exist != 0 ? 'checked': ''}}>

                                <span class="custom-control-label" style="color: #fff; font-size: 1.1rem">Put into bag</span>
                            </label>
                        @if(isset($libraryItem->ext))

                            <input type="hidden" id="note-{{$libraryItem->noteId}}" value="{{$libraryItem->notes_url}}">
                            <a href="javascript:void(0)">
                                <button class="btn btn-outline-light btn-lg mt-3 download-file" onclick="classroomObj.downloadLibraryNotes({{$libraryItem->noteId}})" id="download-{{$libraryItem->noteId}}">{{__('DOWNLOAD')}}</button>
                            </a>
                        @endif
                    </div>
                </div>
            </div>

            <input type="hidden" id="topic-{{$libraryItem->noteId}}" value="{{$libraryItem->topic_name}}">
            <input type="hidden" id="tutor-{{$libraryItem->noteId}}" value="{{$libraryItem->teacher_id}}">
            <input type="hidden" id="desc-{{$libraryItem->noteId}}" value="{{$libraryItem->description}}">
            <input type="hidden" id="added-{{$libraryItem->noteId}}" value="{{$libraryItem->added}}">
        @endforeach

            <div class="col-md-6 col-lg-4">
                <div class="card shadow library-item">
                    <img src="{{asset('images/book.png')}}" class="card-img" alt="">
                    <div class="card-body">
                        <h4 style="color: #777777">{{$libraryItem->class_name}}</h4>
                        <p style="color: #000">{{$libraryItem->subject}}</p>
                        <a href="#"> <span style="color: #585858;">by&nbsp;</span> <span style="text-decoration: underline">{{$libraryItem->teacher_name}}</span></a>
                        <p>Added {{$libraryItem->added}}</p>
                        <p><span class="fa fa-eye"></span> {{$libraryItem->total_read}} Read</p>
                    </div>

                        <div class="card-footer" style="color: #fff; background-color: darkolivegreen; border-radius: 0 0 10px 10px; padding-bottom: 0px; margin-top: -20px">
                            <h3 style="color: #fff">Past Questions</h3>
                        </div>

                    <div class="library-item-hover">
{{--                        @if(!isset($libraryItem->ext))--}}

{{--                        @elseif($libraryItem->ext == 'pdf' || $libraryItem->ext == 'ppt' || $libraryItem->ext == 'pptx' || $libraryItem->ext == 'doc' || $libraryItem->ext == 'docx')--}}
                            <a href="{{route('frontend.classroom.pastQuestionsStart',[$libraryItem->classId])}}" target="_blank">
                                <button class="btn btn-outline-light btn-lg">{{__('TAKE TEST')}}</button>
                            </a>
{{--                            <hr>--}}
{{--                        @else--}}
{{--                            <a href="{{$libraryItem->notes_url}}" target="_blank">--}}
{{--                                <button class="btn btn-outline-light btn-lg">{{__('VIEW')}}</button>--}}
{{--                            </a>--}}
{{--                            <hr>--}}
{{--                        @endif--}}
                        {{--                        <div class="form-check">--}}
                        {{--                            <input type="checkbox" class="form-check-input" id="pib" >--}}
                        {{--                            <label style="color: #fff; font-size: 1.1rem" class="form-check-label" for="pib">Put into bag</label>--}}
                        {{--                        </div>--}}
{{--                        <label class="custom-control custom-checkbox">--}}
{{--                            <input type="checkbox" class="custom-control-input" id="lib" >--}}

{{--                            <span class="custom-control-label" style="color: #fff; font-size: 1.1rem">Put into bag</span>--}}
{{--                        </label>--}}
{{--                            <input type="hidden" id="note">--}}
{{--                            <a href="javascript:void(0)">--}}
{{--                                <button class="btn btn-outline-light btn-lg mt-3 download-file">{{__('DOWNLOAD')}}</button>--}}
{{--                            </a>--}}

                    </div>
                </div>
            </div>


    </div>
@else
    <div class="list-view-sec" style="text-align:center">
        <img src="{{asset('images/norecord.png')}}">
        <h3 class="text-info">No records found</h3>
    </div>
@endif


@php
    $content = ob_get_contents();
    $result['resultHtml']  = $content;
    $result['tab']         = $tab;
    $result['loadMore']    = $loadMore;
    $result['page']        = $page+1;
    $result['totalRecord'] = $libraryItems->total();
    $result['lastPage']    = $libraryItems->lastPage();
    $result['to']          = $libraryItems->lastItem();
    if($page >= $result['lastPage']){
        $result['show_morerecords'] = 0;
    }
    else {
       $result['show_morerecords'] = 1;
    }

    ob_end_clean();
    echo json_encode($result);
@endphp

