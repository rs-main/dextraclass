<script>
    let classroomStreamClassObj = {
        jsId: {
            fullscreen: "#fullscreen",
            videoDiv: "#video_div"
        },
    }

</script>

<style>
    #full-screen-video-iframe {
        position: absolute;
        width: 100vw;
        height: 100vh;
        background-image: url('../images/AllThingsRTC_live-bg.jpg');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
    }
</style>
<section class="content-top-wrapper bg-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">


                @if(Request::has("live") && Request::query("live") == true)

                    <div  class=" container lesson-video" id="video_player_box">
                        <iframe class="responsive-iframe bg-dark"
                                src="{{url("/stream-audience")}}" autoplay=1" id="videoPlayer" width="700" height="400"
                                allow="autoplay; fullscreen"  allowfullscreen></iframe>
                    </div>
                @else
                <div class="lesson-video" id="video_player_box">
{{--                    <iframe class="bg-dark" src="{{$defaultVideo->videoURL()}}?&autoplay=1" id="videoPlayer" width="" height="" frameborder="0" allow="autoplay; fullscreen"  allowfullscreen></iframe>--}}
                    <iframe class="bg-dark" src="{{url("/player")}}?&autoplay=1" id="videoPlayer" width="" height="" frameborder="0" allow="autoplay; fullscreen"  allowfullscreen></iframe>
                </div>
                @endif
            </div>
            <div class="col-lg-4">
                <div class="lesson-card" id="videoInfoSection">
                    <div class="d-flex align-items-center justify-content-evenly pb-3">
                        <div class="lesson-logo">
                            <figure>
                                {!!GLB::classlLogo($classroom)!!}
                            </figure>
                        </div>
                        <div>
                            <div class="join-live mx-4">
                                <a href="?live=true&subject={{$defaultVideo->topic->subject->subject_name}}&subject_id={{$defaultVideo->topic->subject->id}}" class="btn-success px-3">
                                    <span class="icon"><img src="{{asset("images/circle.png")}}"></span>
                                    <span class="text">Join Live</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <h2 class="lesson-title">{{$classroom->class_name}} @if(config('constants.BASIC_SCHOOL') != $classroom->course->school->school_category) ({{$classroom->course->name}}) @endif</h2>
                    <ul class="lesson-info">
                        <li>
                            <span class="icon"> <i class="fas fa-map-marker-alt"></i></span>
                            <span class="text">{{$classroom->course->school->school_name}}</span>
                        </li>
                        <li>
                            @if ($defaultVideo->total_views != '' && $defaultVideo->total_views >= 100)
                                <span class="icon"><i class="fas fa-user"></i></span>
                                <span class="text" id="totalView">{{($defaultVideo->total_views != '' ? $defaultVideo->total_views : 0)}} Views</span>
                            @endif
                        </li>
                    </ul>
                    <div class="lesson-topic d-none">
                        <span>Showing ></span>  <text id="videoShowing"> </text>
                    </div>
                    <ul class="lesson-detail">
                        <li>Subject: <text id="videoSubject">{!!$defaultVideo->topic->subject->subject_name!!}</text></li>
                        <li>Topic: <text id="videoTopic">{!!$defaultVideo->topic->topic_name!!}</text></li>
                        <li>Tutor: <text id="videoTeacher">{{$defaultVideo->tutor->fullname}}</text> </li>
                        <li>
                            <span class="text-info" id="showCalender" style="cursor: pointer"><i class="fas fa-calendar-alt text-info"></i> {{$defaultVideo->playOn('d M, Y')}}</span>
                        </li>

                    </ul>

                    <div class="action-button video-action-button" id="videoAction" >


                        @if($lern_more)
                         <div class="pb-2 learn-more"> <a target="_blank" href="{{$lern_more}}" >Learn more</a>  </div>
                        @endif
                        @if($notes_id)
                         <button class="btn-custom" id="notesBtnId" title="Download note" onclick="classroomObj.downloadNotes({{$notes_id}})"><span class="icon"><i class="fas fa-book"></i></span>Get notes</button> <input type="hidden" value="{{$note_url}}" id="donloadNoteInput"/>
                         @endif

                        <span id="favBtnHtm"><button class="btn-custom fav-btn" title="Add favourite" id="favBtnId" onclick="classroomObj.setFavourites({{$defaultVideo->id}},{{$fav_status}})"><span class="icon {{ ( $fav_status ? 'fav-active' : '')}}"><i class="fas fa-star"></i></span></button></span>

                        <span id="spamBtnHtm"><button class="report-btn-custom fav-btn" title="Report this video" id="spamBtnId" onclick="classroomObj.openFlegPopup({{$defaultVideo->id}},{{$fleg_status}})"><span class="icon {{ ( $fleg_status ? 'fav-active' : '')}}"><i class="fas fa-flag"></i></span></button></span>

                    </div>

                </div>

                <div class="lesson-card hide" id="calendarDiv">
                    <button class="btn btn-custom btn-sm mb-2" id="cancel">Back</button>

                    <div class="calender">
                        <input class="date" value="" onchange="classroomObj.archiveSearch('daterange', true)" type="text" name="date" id="semesterDate" style="display:none" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include("frontend.classroom.partials.agora_scripts")

<script>
    class classroomStreamClass {

        constructor(external) {
            this.ext = external;
        }

        fullscreen() {
            let ids = this.ext.jsId;
            let videoDiv = document.getElementById('video_div');
            videoDiv.requestFullscreen()
        }

        play() {
            alert(1)
        }

        toggleVideoResolutions() {

            $("#videoResolutionsDiv").toggle(300);
        }

    }

    var classroomStreamObj = new classroomStreamClass(classroomStreamClassObj);

</script>
