<div class="col-12" id="quizHtml">
    <input type="hidden" value="{{ count($questions) }}" id="totalQxn">
    <input type="hidden" value="{{ $questions[0]->q_quiz_id }}" id="quiz_id">

    @foreach($questions as $key => $question)
        <div class="card mb-5 mt-5 {{($key > 0) ? 'hide': 'show'}}" id="question-card-{{$key + 1}}">
            <div class="card-header">
                <h4 class="card-title">
                    {{$key + 1}}. {{$question->questions->question}}
                </h4>
            </div>
            <div class="card-body">
                <div class="custom-controls-stacked">
                    @if($question->questions->type == 'true-false')
                        <label class="custom-control custom-radio">
                            <input name="answer-{{$key + 1}}" type="radio" value="1" class="custom-control-input">
                            <span class="custom-control-label">True</span>
                        </label>

                        <label class="custom-control custom-radio">
                            <input name="answer-{{$key + 1}}" type="radio" value="2" class="custom-control-input">
                            <span class="custom-control-label">False</span>
                        </label>

                    @else
                        @foreach($question->questions->options as $k => $option)
                            <label class="custom-control custom-radio">
                                <input name="answer-{{$key + 1}}" value="{{$k + 1}}" type="radio" class="custom-control-input">
                                <span class="custom-control-label">{{$option->title}}</span>
                            </label>
                        @endforeach
                    @endif

                </div>


            </div>
            <div class="card-footer clearfix">
                <div class="float-left">
                    <button class="btn btn-custom" onclick="classroomObj.listClassroom('playing',0,0,'quiz')">Cancel</button>
                </div>
                <div class="float-right">
                    @if($key > 0)
                        <button class="btn btn-lg btn-danger qxnBack" data-id="{{$key + 1}}">Back</button> &nbsp; &nbsp;
                    @endif
                    @if($key == ((int)count($questions) - 1))
                        <button class="btn btn-lg btn-primary submitAnswer" data-id="{{$key + 1}}">Submit</button>
                    @else
                        <button class="btn btn-lg btn-primary answerQuestion" data-id="{{$key + 1}}">Next</button>
                    @endif
                </div>
            </div>

        </div>
    @endforeach

</div>
<script>
    $(document).ready(function () {
        var total = $('#totalQxn').val();

        $('input[type=radio]').on('change', function () {
            // if($(this).prop('checked') == true){
            //     option = $(this).val();
            // }
            // else{
            //     option = '';
            // }
            $('input[type=radio]').removeClass('is-invalid');
        });

       $('.answerQuestion').on('click', function () {
           var key = $(this).data('id');

           if(key < total){
               var next = parseInt(key) + 1;
               var val = $('input[name=answer-'+key+']:checked').val();
               if(!val){
                   $('input[name=answer-'+key+']').addClass('is-invalid')
               }
               else{
                   $('#question-card-'+key)
                       .fadeOut()
                       .removeClass('show')
                       .addClass('hide');
                   $('#question-card-'+next)
                       .removeClass('hide')
                   .addClass('show')
                   .fadeIn(500)
               }
           }
       })

        $('.qxnBack').on('click', function () {
            var key = $(this).data('id');

            if(key > 1){
                var prev = parseInt(key) - 1;

                $('#question-card-'+key)
                    .fadeOut()
                    .removeClass('show')
                    .addClass('hide');
                $('#question-card-'+prev)
                    .removeClass('hide')
                    .addClass('show')
                    .fadeIn(500)
            }
        })

        $('.submitAnswer').on('click', function () {
            var key = $(this).data('id');

            var val = $('input[name=answer-'+key+']:checked').val();
            if(!val){
                $('input[name=answer-'+key+']').addClass('is-invalid')
            }
            else{
                var quiz_id = $('#quiz_id').val();
                var answers = [];
                for(var i = 1; i <= total; i++){
                    var answer = $('input[name=answer-'+i+']:checked').val();
                    answers.push(answer);
                }

                $.ajax({
                    url: '{{route('frontend.classroom.submitQuiz')}}',
                    type: 'POST',
                    data: {quiz: quiz_id, answers: answers},
                    beforeSend: function () {
                        $('.submitAnswer').html('<span class="spinner-grow" role="status" aria-hidden="true"></span> &nbsp; submitting...').attr('disabled','disabled');
                    },
                    complete: function () {
                        $('.submitAnswer').html('Submit').removeAttr('disabled');
                    },
                    error: function (xhr, e, h) {
                        // TODO fix commonObj error
                        // commonObj.messages('Error', 'Sorry an error occurred')
                        console.error(e)
                    },
                    success: function (data) {
                        console.log(data);
                        var response = data;
                       // var status = data.status;

                        var passed = response.passed;
                        if(passed){
                            $('#quizHtml').html('' +
                                '<div class="card">' +
                                '<div class="card-body">' +
                                '<div class="text-center">' +
                                '<img src="{{asset('images/passed.gif')}}" width="200">' +
                                '<h1 class="text-center text-success">Excellent.</h1>' +
                                '<p class="text-center mb-3">Excellent, you have successfully passed. You got <strong>'+response.student_marks+'%</strong>.</p>' +
                                '<button class="btn btn-success btn-sm" onclick="classroomObj.listClassroom(\'playing\',0,0,\'quiz\')"> Proceed</button>' +
                                '</div> ' +
                                '</div>' +
                                '</div>')
                        }
                        else{
                            $('#quizHtml').html('' +
                                '<div class="card">' +
                                '<div class="card-body">' +
                                '<div class="text-center">' +
                                '<img src="{{asset('images/failed.gif')}}" width="200">' +
                                '<h1 class="text-center text-danger">Give it another go.</h1>' +
                                '<p class="text-center mb-3">Oops! you need to get a pass mark of <strong>'+response.pass_mark+'%</strong>. <br> You got <strong>'+response.student_marks+'%</strong> but no worries you can take it again.</p>' +
                                '<button class="btn btn-danger" onclick="classroomObj.takeQuiz('+response.quiz_id+', \'retake\')"> Retake</button>' +
                                '</div> ' +
                                '</div>' +
                                '</div>')
                        }
                    }
                })
            }
        })
    });
</script>
