<html lang="en">
<head>
    <title>Agora Live Stream</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{asset("js/agora/AgoraRTCSDK-3.1.1.js")}}"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

    <script>
        window.addEventListener('load', function() {
            // alert('linked!');
            video = document.getElementById('video');

            pbarContainer = document.getElementById('pbar-container');
            pbar = document.getElementById('pbar');

            playButton = document.getElementById('play-button');

            soundButton = document.getElementById('sound-button');
            sbarContainer = document.getElementById('sbar-container');
            sbar = document.getElementById('sbar');

            fullscreenButton = document.getElementById('fullscreen-button');

            screenDiv = document.getElementById('screen');
            screenButton = document.getElementById('screen-button');

            timeField = document.getElementById('time-field');

            video.load();
            video.addEventListener('canplay', function(){

                playButton.addEventListener('click', playOrPause, false);
                pbarContainer.addEventListener('click', skip, false);
                updatePlayer();
                soundButton.addEventListener('click', muteOrUnmute, false);
                sbarContainer.addEventListener('click', changeVolume, false);
                fullscreenButton.addEventListener('click', fullScreen, false);
                screenButton.addEventListener('click', playOrPause, false);
            }, false);


        }, false);

        function playOrPause() {
            if(video.paused) {
                screenDiv.style.display = 'none';
                video.play();
                playButton.src = 'images/pause.png';
                // Updating the progress bar every 30 seconds
                update = setInterval(updatePlayer, 30);
            }
            else {
                screenDiv.style.display = 'block';
                video.pause();
                playButton.src = 'images/play.png';
                // stopping the progress to be updated when the video is paused
                window.clearInterval(update);
            }
        }

        function updatePlayer() {
            var precentage = (video.currentTime/video.duration)*100;
            pbar.style.width = precentage + '%';
            timeField.innerHTML = getFormattedTime();

            // stoping the update when video has ended
            if(video.ended){
                window.clearInterval(update);
                playButton.src = 'images/replay.png'
                screenDiv.style.display = 'block';
                screenButton.src = 'images/replay.png';
            }
            else if(video.paused){
                playButton.src = 'images/play.png';
                screenButton.src = 'images/play.png';
            }
        }

        // ev is for passing the event
        function skip(ev) {
            var mouseX = ev.pageX - pbarContainer.offsetLeft;
            // getting the width of the progress bar
            var width = window.getComputedStyle(pbarContainer).getPropertyValue('width');
            width = parseFloat(width.substr(0, width.length - 2));

            video.currentTime = (mouseX/width)*video.duration;

            // update the player when video is paused
            updatePlayer();
            // alert(width);
        }

        function getFormattedTime(){
            // rounding off seconds to nearest integer
            var seconds = Math.round(video.currentTime);
            // calculating minutes by taking floor
            var minutes = Math.floor(seconds/60);

            if(minutes > 0){
                seconds = seconds - minutes*60;
            }

            if(seconds.toString().length === 1){
                seconds = '0' + seconds;
            }

            var totalSeconds = Math.round(video.duration);
            var totalMinutes = Math.floor(totalSeconds/60);

            if(totalMinutes > 0){
                totalSeconds = totalSeconds - totalMinutes*60;
            }

            if(totalSeconds.toString().length === 1){
                totalSeconds = '0' + totalSeconds;
            }

            return minutes + ':' + seconds + ' / ' + totalMinutes + ':' + totalSeconds;
        }

        function muteOrUnmute(){
            if(video.muted){
                video.muted = false;
                soundButton.src = 'images/sound.png';
                sbar.style.display = 'block';
            }
            else{
                video.muted = true;
                soundButton.src = 'images/mute.png';
                sbar.style.display = 'none';
            }
        }

        function changeVolume(ev){
            var mouseX = ev.pageX - sbarContainer.offsetLeft;
            var width = window.getComputedStyle(sbarContainer).getPropertyValue('width');
            width = parseFloat(width.substr(0, width.length - 2));

            video.volume = (mouseX/width);
            sbar.style.width = (mouseX/width)*100 + '%';

            // we should be able to change the volume when the video is muted
            video.muted = false;
            soundButton.src = 'images/sound.png';
            sbar.style.display = 'block';
        }

        function fullScreen(){
            if(video.requestFullScreen){
                video.requestFullScreen();
            }
            else if(video.webkitRequestFullScreen){
                video.webkitRequestFullScreen();
            }
            else if(video.msRequestFullScreen){
                video.msRequestFullScreen();
            }
            else if(video.mozRequestFullScreen){
                video.mozRequestFullScreen();
            }
        }
    </script>

    {{--    <link rel="stylesheet" type="text/css" href="{{asset("css/agora/style.css")}}"/>--}}

    <style>
        #loading-stream-text,#stream-failed-button{
            position: absolute;
            top: 60%;
            left: 46%;
            width: 200px;
        }
        .loader {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 80px;
            height: 80px;
            transform: translate(-50%, -50%) rotate(0deg) translate3d(0, 0, 0);
            animation: loader 1.4s infinite ease-in-out;
        }
        .loader span {
            position: absolute;
            display: block;
            width: 40px;
            height: 40px;
            background-color: #12CB56;
            animation: loaderBlock 1.4s infinite ease-in-out both;
        }
        .loader span:nth-child(1) {
            top: 0;
            left: 0;
        }
        .loader span:nth-child(2) {
            top: 0;
            right: 0;
            animation: loaderBlockInverse 1.4s infinite ease-in-out both;
        }
        .loader span:nth-child(3) {
            bottom: 0;
            left: 0;
            animation: loaderBlockInverse 1.4s infinite ease-in-out both;
        }
        .loader span:nth-child(4) {
            bottom: 0;
            right: 0;
        }
        body {
            background-color: #2A3239;
            /*background-color: #fff;*/
        }
        @keyframes loader {
            0%, 10%, 100% {
                width: 80px;
                height: 80px;
            }
            65% {
                width: 130px;
                height: 130px;
            }
        }
        @keyframes loaderBlock {
            0%, 30% {
                transform: rotate(0);
            }
            55% {
                background-color: #f37272;
            }
            100% {
                transform: rotate(360deg);
            }
        }
        @keyframes loaderBlockInverse {
            0%, 20% {
                transform: rotate(0);
            }
            55% {
                background-color: #f37272;
            }
            100% {
                transform: rotate(-360deg);
            }
        }

        /*
	darkgrey: rgb(30, 30, 30);
	lightgrey: rgb(70, 70, 70);
	lightblue: rgb(50, 170, 220);
*/

        #player {
            background-color: rgb(30, 30, 30);
            display: inline-block;
            /*Ensuring that user can't select the player*/
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }

        /* for all images */
        img {
            background-color: rgb(70, 70, 70);
        }

        img:hover {
            background-color: rgb(50, 170, 220);
        }

        img, #pbar-container, #sbar-container {
            cursor: pointer;
        }

        #video-container {
            /*width: 640px;*/
            width: 100%;
            height: 100%;
            /*width: 640px;*/
            /*height: 360px;*/
            position: absolute;
        }

        video {
            height: 100%;
            width: 100%;
        }

        #screen {
            height: 100%;
            width: 100%;
            line-height: 360px;
            text-align: center;
            position: absolute;
            z-index: 1 !important;

            background: -moz-linear-gradient(top,  rgba(0,0,0,0.65) 0%, rgba(0,0,0,0) 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a6000000', endColorstr='#00000000',GradientType=0 ); /* IE6-9 */
        }

        #screen img{
            vertical-align: middle;
        }

        #pbar-container {
            margin-top: 2px;
            padding-bottom: 2px;
            background-color: rgb(70, 70, 70);
        }
        #pbar{
            height: 5px;
            width: 0%;
            background-color: rgb(50, 170, 220);
        }

        #buttons-container {
            height: 50px;
            position: absolute;
            left: 40%;
            bottom: 10%;
        }
        /* Accessing all the direct children of the buttons container */
        #buttons-container > * {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            /*outline: 1px solid red;*/
            margin: 0 10px;
        }

        #time-field {
            color: rgb(50, 170, 220);
            cursor: default;
            /*We can now ensure that the text appears in the middle*/
            line-height: 50px;
        }
        #sound-button {
            margin-right: 0;
        }
        #sbar-container{
            width: 100px;
            height: 5px;
            background-color: rgb(70, 70, 70);
            margin-left: 0;
        }
        #sbar {
            height: 100%;
            width: 100%;
            background-color: rgb(50, 170, 220);
        }
        #fullscreen-button {
            float: right;
        }
    </style>
</head>
<body>
<div class="container-fluid p-0">
    <div id="main-container">

        <div id="player">

            <div id="video-container">
                <div id="screen">
                    <img id="screen-button" src="images/play.png">
                </div>
{{--                <video id="video" poster="https://ichef.bbci.co.uk/images/ic/640x360/p046dthw.jpg">--}}
{{--                    <source src="http://techslides.com/demos/sample-videos/small.mp4">--}}
{{--                </video>--}}
            </div>

            <div id="pbar-container">
                <div id="pbar">

                </div>
            </div>

            <div id="buttons-container">
                <img id="play-button" src="{{asset("images/play.png")}}">
                <div id="time-field">
                    0:00 / 0:00
                </div>
                <img id="sound-button" src="{{asset("images/sound.png")}}">
                <div id="sbar-container">
                    <div id="sbar">

                    </div>
                </div>
                <img id="fullscreen-button" src="{{asset("images/player_fullscreen.png")}}">
            </div>

        </div>

{{--        <div id="full-screen-video"></div>--}}



        {{--        <div class="m-5 p-5" id="loader" style="display: none">--}}
{{--            <div class="loader">--}}
{{--                <span></span>--}}
{{--                <span></span>--}}
{{--                <span></span>--}}
{{--                <span></span>--}}
{{--            </div>--}}
{{--        </div>--}}

    </div>
</div>

{{--<div id="watch-live-overlay">--}}
{{--    <div id="overlay-container">--}}
{{--        <div class="col-md text-center">--}}
{{--            <button id="watch-live-btn" type="button" class="btn btn-block btn-primary btn-xlg">--}}
{{--                <i id="watch-live-icon" class="fas fa-broadcast-tower"></i><span>Watch the Live Stream</span>--}}
{{--            </button>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}




</body>
<script>
    let channel_id = "{{Request::query("channel_id")}}";
    // let channel_id = "288-384-1267";
    // let channel_id = "aYjskb";

    /*
	Checking by adding an event listener for loading of the window
*/
</script>
<script src="{{asset("js/agora/agora-audience-client.js")}}"></script>
</html>
