
@foreach($courses as $school)
    {{--                    @if (in_array($school['school_category'], [19, 20, 21, 22]))--}}
    <div class="item">
        <div class="school-card btn-group" >
            <a href="javascript:void(0)" title="" class="school-card-link p-0" onclick="JoinOpenClass2.openModel()" data-cat="" data-school="">
                {{--                                    @if($school['is_locked'])--}}
                {{--                                        <div class="school-lock">--}}
                {{--                                            <img src="/images/lock-icon.png" alt="lock">--}}
                {{--                                        </div>--}}
                {{--                                    @endif--}}
                <figure class="w-100">
                    <img src="{{asset('uploads/courses/'.$school->display_image)}}" class="img-fluid" height="73" alt="{{ $school->name }}">
                </figure>

                <div class="pt-0 pb-4 px-4">
                    <h3 class="school-name">{{ $school->name }}</h3>

                    <p class="school-course-qty">
                        {{\App\Models\Course::getCourseVideosCount($school->id)}} Lessons
                        {{--                                            {{$school['lesson_num']}} Lessons--}}
                    </p>
                </div>


            </a>
        </div>
    </div>
    {{--                    @endif--}}
@endforeach
