<section class="pt-3">
    <div class="row pb-3" id="AddQuizBtnDiv">
        <div class="col">
            <button type="button" id="AddQuizBtn" onclick="tutorPost.showHideQuizPost('show')" class="btn btn-primary px-5 ">
                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                CREATE QUIZ
            </button>
        </div>
    </div>

    <div class="contact-card hide" id="postQuizTab">
        <form action="" method="POST" id="quizForm" class="has-validation-callback">
            @csrf

            <div>
                <!--<label for="exampleInputEmail166">Video info</label>-->

                <h3 class="sub-heading">Add a new quiz</h3>

            </div>
            <div class="row">
                <div class="col-md-12 {{($schoolCategoryName == 'UNIVERSITY' ? '' : 'd-none' )}}">
                    <div class="form-group">
                        <div class="custom-select-outer">
                            <select class="custom-select" onchange="tutorPost.fullProgramList()" id="video_departmentId">
                                <option class="d-none" value=""> Department </option>
                                @foreach($defaultArray['deparments'] as $deparmentId => $deparment)
                                    <option value="{{$deparmentId}}">{{$deparment}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 {{($schoolCategoryName == 'BASIC_SCHOOL' ? 'd-none' : '' )}}">
                    <div class="form-group">
                        <div class="custom-select-outer">
                            <select class="custom-select" onchange="tutorPost.fullClassesList()" id="quiz_courseId">
                                @if($schoolCategoryName == 'UNIVERSITY')
                                    <option class="d-none" value=""> Program </option>
                                @else
                                    <option class="d-none" value=""> Course </option>
                                @endif

                                    @foreach(\App\Models\Course::whereSchoolId($tutor->userData->school->id)->get() as $course)
                                        <option value="{{$course->id}}">{{$course->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-{{($schoolCategoryName == 'BASIC_SCHOOL' ? '12' : '6' )}}">
                    <div class="form-group">
                        <div class="custom-select-outer" >
                            <select class="custom-select" onchange="tutorPost.fullSubjectList()" id="video_classId">
                                <option class="d-none" value="">Class </option>
                                @foreach($defaultArray['classes'] as $classeId => $classe)
                                    <option value="{{$classeId}}">{{$classe}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="custom-select-outer">
                            <select class="custom-select" onchange="tutorPost.fullSubjectList()" data-validation="" name="subjectId" id="subjectId">
                                <option value="" class="d-none"> Subject </option>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" class="form-control " placeholder="Caption" name="caption" value="" data-validation="" data-validation-length="2-255" >
                        <small class="form-text">In minutes</small>
                    </div>
                </div>

            </div>
            <div class="row d-flex flex-row">
                <div class="flex-grow-1 col-md-auto">
                    <div class="form-group">
                        <div class="custom-select-outer">
                            <select class="custom-select" onchange="tutorPost.questionList()" data-validation="" name="subjectId" id="subjectId">
                                <option value="" class="d-none"> Question </option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="flex-grow-0 col-md-auto">
                    <button type="button" class="btn btn-primary px-5" onclick="tutorPost.addQuestion()" id="addQuizQuestionBtn">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        Add
                    </button>
                </div>
            </div>

            <div class="row d-none">
                <div class="col">
                    <div class="form-group">

                        <input type="text" class="form-control " placeholder="Caption" name="caption" value="" data-validation="" data-validation-length="2-255" >

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="form-inline mb-4">
                        <label class="custom-control custom-switch justify-content-start mr-2">
                            <input name="video_type" type="checkbox" value="url" onchange="tutorPost.showHideVideoSource()" class="custom-control-input video_type" checked="" required="">
                            <span class="custom-control-label">Status</span>
                        </label>
                        {{--
                                                <label class="custom-control custom-radio justify-content-start mr-2">
                            <input name="video_type" type="radio" value="file" onchange="tutorPost.showHideVideoSource()" class="custom-control-input video_type"  >
                            <span class="custom-control-label">Video by File</span>
                        </label>--}}
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col">
                    <button type="button" class="btn btn-success px-5" onclick="tutorPost.createQuiz()" id="videoCancelBtn">SUBMIT</button>
                    <button type="button" class="btn btn-secondary px-5" onclick="tutorPost.showHideQuizPost('hide')" id="videoPostBtn">CANCEL</button>
                </div>
            </div>


        </form>
    </div>
</section>
