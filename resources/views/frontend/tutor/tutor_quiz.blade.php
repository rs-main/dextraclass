
@if(!empty($tutorQuizes[0]))
    @foreach($tutorQuizes as $tutorQuiz)

        {{-- {{$tutorQuiz}} --}}

        <div class="item-row item-row-lectures history-row-count" id="history-row-{{$tutorQuiz->id}}">
            <div class="row align-items-center align-items-xl-start">
                <div class="col flex-grow-0">
                    <figure class="item-row-image"><img src="{{asset('/uploads/schools/'.$tutorQuiz->school->logo)}}" alt=""></figure>
                </div>

                <div class="col flex-grow-1">
                    <a href="{{route('frontend.classroom',$tutorQuiz->classDetail->uuid)}}?video={{$tutorQuiz->uuid}}" class="lecture-title">
                        <h3 class="heading">
                            {!!$tutorQuiz->title!!}
                        </h3>
                        <p class="heading-sub-text">{{$tutorQuiz->sub_title}}</p>
                        @if ($tutorQuiz->status)
                            <small class="text-success"> <strong>Active</strong> </small>
                        @else
                            <small class="text-danger"> <strong>Disabled</strong> </small>
                        @endif
                    </a>
                </div>
                <div class="col-12 col-md flex-grow-1 flex-md-grow-0 mt-2 mt-md-0 d-flex align-items-center justify-content-center">
                    <div class="action-button mr-3 mr-xl-0">
                        <a href="javascript:void(0)" title="" class="btn-knowledge-link d-none">Knowledge link</a>
                        @if($tutorQuiz->tutor->upload_access > 0)
                            <a href="javascript:void(0)" id="btn-lecture-id-{{$tutorQuiz->id}}" onclick="tutorPost.addNotes({{$tutorQuiz->id}})" title=""  class="btn-notes ml-3">Add notes</a>	@endif
                    </div>

                    <div class="text-right upload-time">
                        <div class="d-flex align-items-center justify-content-end w-100">
                            @if($tutorQuiz->article_id)
                                <span class="mr-2"><img src="{{asset('images/link-icon.png')}}" alt=""></span>
                            @endif
                            @if($tutorQuiz->note_id)
                                <span><img src="{{asset('images/book-icon.png')}}" alt=""></span>
                            @endif
                        </div>
                        <small class="d-flex align-items-center justify-content-end w-100">{!!GLB::dayAgo($tutorQuiz->created_at)!!}

                        </small>
                    </div>

                </div>
            </div>
        </div>
    @endforeach

@else
    <div class="list-view-sec" style="text-align:center">
        <h5 class="text-info">No Record Available</h5>
    </div>
@endif

{{--
@php
    $content = ob_get_contents();
    $result['resultHtml']  = $content;
    $result['tab']         = $tab;
    $result['loadMore']    = $loadMore;
    $result['page']        = $page+1;
    $result['totalRecord'] = $tutorQuizes->total();
    $result['lastPage']    = $tutorQuizes->lastPage();
    $result['to']          = $tutorQuizes->lastItem();
    if($page >= $result['lastPage']){
    $result['show_morerecords'] = 0;
    }
    else {
    $result['show_morerecords'] = 1;
    }

    ob_end_clean();
    echo json_encode($result);
@endphp

--}}
