{{--@if($tutor->userData->upload_access > 0)--}}
@if($tutor)

    <section class="pt-3 ">
        <div class="contact-card" id="postVideoTab">
            <form action="" method="POST" id="videoPostForm" class="has-validation-callback">
{{--                @csrf--}}
                <div>
                    <h3 class="sub-heading">Stream to</h3>
                </div>
                <input type="hidden" name="tutor_id" value="{{$tutor->id}}">
                <div class="row">
                    <div class="col-md-12 {{($schoolCategoryName == 'UNIVERSITY' ? '' : 'd-none' )}}">
                        <div class="form-group">
                            <div class="custom-select-outer">
                                <select class="custom-select" onchange="tutorPost.fullProgramList()" id="video_departmentId">
                                    <option class="d-none" value=""> Department </option>
                                    @foreach($defaultArray['deparments'] as $deparmentId => $deparment)
                                        <option value="{{$deparmentId}}">{{$deparment}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 {{($schoolCategoryName == 'BASIC_SCHOOL' ? 'd-none' : '' )}}">
                        <div class="form-group">
                            <div class="custom-select-outer">
                                <select name="course_id" class="custom-select" onchange="tutorPost.fullClassesList()" id="video_courseId">
                                    @if($schoolCategoryName == 'UNIVERSITY')
                                        <option class="d-none" value=""> Program </option>
                                    @else
                                        <option class="d-none" value=""> Course </option>
                                    @endif

                                        @foreach(\App\Models\Course::where("school_id",$tutor->userData->school->id)->select("id","name")->get() as $course)
                                            <option value="{{$course->id}}">{{$course->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-{{($schoolCategoryName == 'BASIC_SCHOOL' ? '12' : '6' )}}">
                        <div class="form-group">
                            <div class="custom-select-outer" >
                                <select name="class_id" class="custom-select" onchange="tutorPost.fullSubjectList()" id="video_classId">
                                    <option class="d-none" value="">Class </option>
                                    @foreach($defaultArray['classes'] as $classeId => $classe)
                                        <option value="{{$classeId}}">{{$classe}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="custom-select-outer">
                                <select required class="custom-select" onchange="tutorPost.topicList()" data-validation="" name="subject_id" id="subjectId">
                                    <option value="" class="d-none"> Subject </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <input required class="flatpickr flatpickr-input date form-control active"
                                   name="play_on" id="play_on"  readonly="readonly" placeholder="Start Date">
                        </div>

                    </div>

                </div>
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="custom-select-outer">
                                <select class="custom-select" onchange="tutorPost.periodList()" data-validation=""  name="topic_id" id="topicId">
                                    <option value="" class="d-none"> Topic </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="custom-select-outer">
                                <select required class="custom-select" data-validation="" name="period_id" id="periodId">
                                    <option value="" class="d-none"> Period No.</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <input required type="datetime-local" class="form-control" name="play_on" id="play_on">
                        </div>

                    </div>
                </div>


                <h3 class="sub-heading">Stream with</h3>

                <div class="row">
                    <div class="col">
                        <div class="form-inline mb-4">
                            <label class="custom-control custom-radio justify-content-start mr-2">
                                <input name="webcam" type="radio" value="url" class="custom-control-input video_type" checked="" required="">
                                <span class="custom-control-label">Webcam</span>
                            </label>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col">

                    <div class="custom-control custom-switch">
                        <input name="save_stream" type="checkbox" class="custom-control-input" id="customSwitch1" checked>
                        <label class="custom-control-label" for="customSwitch1">Save stream session</label>
                    </div>
                    </div>

                </div>
                <br>

                <h3 class="sub-heading">Go Live</h3>

                <div class="row mt-2">
                    <div class="col">
                        <button type="button" class="btn btn-primary px-5" id="sendNoticeBtn">SEND NOTICE</button>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col">
                        <button type="submit" class="btn btn-success px-5" id="videoPostBtn">GO LIVE NOW</button>
                        <button class="btn btn-success px-5 hide" type="button" disabled="" id="stream-loader-btn">
                            <span class="spinner-border" role="status" aria-hidden="true"></span>
                            Loading stream...
                        </button>
                    </div>
                </div>
            </form>
        </div>


    </section>
@endif
