
@foreach(\App\Models\KnowledgeArticle::whereAuthorId($tutor->userData->user_id)->get() as  $article)
    <div class="col-sm-6 col-lg-4">
        <div class="Knowledge-card">
            <div class="Knowledge-card-image">
                <a href="#" title="">
                    <img src="{{url("/".$article->image)}}" alt="" class="img-fluid"></a>
            </div>
            <div class="Knowledge-card-body">
                <h3><a href="#" title="History: Slave trade in Ghana">
                        {{$article->title}}
                    </a></h3>
                <p>
{{--                    {!! substr($article->content, 0, strpos(wordwrap($article->content, 10,true), "\n")); !!}--}}
                    {!! substr($article->content, 0,10); !!}
{{--                    wordwrap ($string, $width = 75, $break = "\n", $cut_long_words = false)--}}
                </p>
            </div>
            <div class="Knowledge-card-footer">
                <p>by <a href="#">{{ $tutor->userData->first_name." ".$tutor->userData->last_name}}</a></p>
                <p>{{$article->created_at}}</p>
            </div>
        </div>
    </div>
@endforeach
