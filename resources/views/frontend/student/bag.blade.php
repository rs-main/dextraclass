@php ob_start() @endphp

@if(!empty($bagItems[0]))
    <div class="row mt-3">
        @foreach($bagItems as $bagItem)
            <div class="col-md-6 col-lg-4">
                <div class="card shadow library-item">
                    <img src="{{asset('images/book.png')}}" class="card-img" alt="{{$bagItem->topic_name}}">
                    <div class="card-body">
                        <h4 style="color: #777777">{{$bagItem->topic_name}}</h4>
                        <p style="color: #000">{{$bagItem->description}}</p>
                        <a href="#"> <span style="color: #585858;">by&nbsp;</span> <span style="text-decoration: underline">{{$bagItem->teacher_name}}</span></a>
                        <p>Added {{$bagItem->added}}</p>
                    </div>
                    @if(!isset($bagItem->ext))
                        <div class="card-footer" style="color: #fff; background-color: #d8b425; border-radius: 0 0 10px 10px; padding-bottom: 0px; margin-top: -20px">
                            <h3 style="color: #fff">File</h3>
                        </div>
                    @elseif($bagItem->ext == 'pdf')
                        <div class="card-footer" style="color: #fff; background-color: #268afc; border-radius: 0 0 10px 10px; padding-bottom: 0px; margin-top: -20px">
                            <h3 style="color: #fff">Book</h3>
                        </div>
                    @else
                        <div class="card-footer" style="color: #fff; background-color: #c959e5; border-radius: 0 0 10px 10px; padding-bottom: 0px; margin-top: -20px">
                            <h3 style="color: #fff">Slide</h3>
                        </div>
                    @endif

                    @if($bagItem->open == 0)
                        <img src="{{asset('images/new.png')}}" class="new-ribbon">
                    @endif

                    <div class="library-item-hover">
                        @if(!isset($bagItem->ext))

                        @elseif($bagItem->ext == 'pdf' || $bagItem->ext == 'ppt' || $bagItem->ext == 'pptx' || $bagItem->ext == 'doc' || $bagItem->ext == 'docx')
                            <a href="javascript:void(0)" onclick="viewBook('{{$bagItem->notes_url}}')">
                                <button class="btn btn-outline-light btn-lg">{{__('READ')}}</button>
                            </a>
                            <hr>
                        @else
                            <a href="{{$bagItem->notes_url}}" target="_blank">
                                <button class="btn btn-outline-light btn-lg">{{__('VIEW')}}</button>
                            </a>
                            <hr>
                        @endif
                        {{--                        <div class="form-check">--}}
                        {{--                            <input type="checkbox" class="form-check-input" id="pib" >--}}
                        {{--                            <label style="color: #fff; font-size: 1.1rem" class="form-check-label" for="pib">Put into bag</label>--}}
                        {{--                        </div>--}}
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" onchange="studentObj.removeFromUserLibrary({{$bagItem->noteId}})" id="lib-{{$bagItem->noteId}}"  checked>

                            <span class="custom-control-label" style="color: #fff; font-size: 1.1rem">Remove from bag</span>
                        </label>
                        @if(isset($bagItem->ext))

                            <input type="hidden" id="note-{{$bagItem->noteId}}" value="{{$bagItem->notes_url}}">
                            <a href="javascript:void(0)">
                                <button class="btn btn-outline-light btn-lg mt-3 download-file" onclick="studentObj.downloadLibraryNotes({{$bagItem->noteId}})" id="download-{{$bagItem->noteId}}">{{__('DOWNLOAD')}}</button>
                            </a>
                        @endif
                    </div>
                </div>
            </div>

            <input type="hidden" id="topic-{{$bagItem->noteId}}" value="{{$bagItem->topic_name}}">
            <input type="hidden" id="tutor-{{$bagItem->noteId}}" value="{{$bagItem->teacher_id}}">
            <input type="hidden" id="desc-{{$bagItem->noteId}}" value="{{$bagItem->description}}">
            <input type="hidden" id="added-{{$bagItem->noteId}}" value="{{$bagItem->added}}">
        @endforeach

    </div>
@else
    <div class="list-view-sec" style="text-align:center">
        <img src="{{asset('images/norecord.png')}}">
        <h3 class="text-info">No records found</h3>
    </div>
@endif


@php
    $content = ob_get_contents();
    $result['resultHtml']  = $content;
    $result['tab']         = $tab;
    $result['loadMore']    = $loadMore;
    $result['page']        = $page+1;
    $result['totalRecord'] = $bagItems->total();
    $result['lastPage']    = $bagItems->lastPage();
    $result['to']          = $bagItems->lastItem();
    if($page >= $result['lastPage']){
        $result['show_morerecords'] = 0;
    }
    else {
       $result['show_morerecords'] = 1;
    }

    ob_end_clean();
    echo json_encode($result);
@endphp
