
@section('after-styles')

@endsection

@section('scripts')
<script src="{{ asset('js/jquery.serializejson.js') }}"></script>
<script type="module">
let studentCon = {

    separator:'/',
    strDouble:'"',
    strSingle:"'",
    parentUrl:'{{URL::to("/")}}',
   jsId : {
        historyRow          		: "#history-row-",
        favRow          	    	: "#fav-row-",
        btnRmHistory            	: "#btn-rm-history-",
        btnRmFav                 	: "#btn-rm-fav-",
        activeTabInput        		: "#activeTabInput",
        historyHtml        		    : "#historyHtml",
        favouritesHtml        		: "#favouritesHtml",
        bagHtml        		        : "#bagHtml",
        historyMore        		    : "#historyMore",
        favouritesMore        		: "#favouritesMore",
        bagMore        		        : "#bagMore",
        historyPage          		: "#historyPage",
        favouritesPage        		: "#favouritesPage",
        bagPage        		        : "#bagPage",
        school_id        		    : "#school_id",
        course_id        		    : "#course_id",
        activetedTab           		: "#activetedTab",
       libraryNotesBtnId            : "#download-",
       downloadLibraryNoteInput    : "#note-",

   },
	status : {
        success     : 200
    },
    jsClass : {
        navItem 				: ".nav-item",
        favRowCount 		    : ".fav-row-count",
        historyRowCount 		: ".history-row-count",
        bagRowCount 		    : ".bag-row-count"

    },
	jsData : {
        histories 				: "histories",
        favourites 				: "favourites",
        bag 				    : "bag"
    },
	jsNames : {
        noname 			: "",
    },
	jsValue : {
        user_id 			: "",
    },
    extra : {
        jsSeparator:'-',
		url : {
			   histories     : '{{route("frontend.studentHistory")}}',
			   rmHistories   : '{{route("frontend.removeHtudentHistory")}}',
			   favourites    : '{{route("frontend.studentFavourites")}}',
			   bag           : '{{route("frontend.studentBag")}}',
               studentDownloads     : '{{route("frontend.studentDownloads")}}',
            removeFromUserLibrary    : '{{route("frontend.removeFromUserLibrary")}}',
            filterUserLibrary    : '{{route("frontend.filterUserLibrary")}}',
            filterUserRecent    : '{{route("frontend.filterUserRecent")}}'

        }
    },
	createUrl : function(set){
        return ( this.parentUrl+this.separator+set);
    },
	createId : function(arr){
        return ( arr.join(''));
    }
}

import {Student} from "/js/student_history.js";
(function() {  window.studentObj = new Student(studentCon);  })();
</script>

    <script>
        $(document).ready(function () {
            $.ajax({
                url: '{{route("frontend.plan")}}',
                type: 'GET',
                data: {id: 1},
                success: function (data) {

                    $('#plan').html(data);
                },
                error: function (error) {
                    console.log(error);
                }
            });

            $('#subscribeForm').on('submit', function () {
                var errorDiv = $('#subError');
                var btn = $('#submitSubscription');
                btn.html('<span class="fa fa-spinner"></span> subscribing...').attr('disabled','disabled');

            })
        })
    </script>


    <script>
        function viewBook(book){
            $('#bagHtml').css('height', '500px')
            PDFObject.embed(book, "#bagHtml");
        }
    </script>



@stop

