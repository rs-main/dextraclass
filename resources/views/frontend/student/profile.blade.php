@extends('frontend.layouts.app')
@section('header-styles')

@endsection
@section('content')


<section class="content-top-wrapper bg-white">
    <div class="container">
        <div class="row">

            @include('frontend.student.profile_upload')
			@include('frontend.student.edit_profile')
            @include('frontend.tutor.change_password')

            <div class="col-md-7 col-lg-8 col-xl-9">
                <div class="profile-detail">
                    <div class="d-flex align-items-center flex-wrap profile-info-section">
                        <h1 class="sub-heading">{{ $student->userData->fullname }}</h1>
						@if(SiteHelpers::emailVerified() == 1)
							<a href="javascript:void(0)" class="btn-activate m-2 firstSendOtp" data-toggle="modal" data-target="#activeModel">Activate Account </a>
                        @endif

                        @if($student->userData->subscribed)
                            <button class="btn-subscribe m-2">Subscribed</button>
                        @else
{{--                            <button class="btn-activate m-2 activate-profile" data-toggle="modal" data-target="#subscribeModal">Activate account</button>--}}
                        @endif
                        {{-- <a href="#" class="btn-activate m-2">Activate account</a> --}}
                        {{-- <a href="#" class="btn-subscribe m-2">Subscribe</a> --}}

						<div class="dropdown profile-menu flex-grow-1 text-right">
							<a href="javascript:void(0)" class="dropdown-toggle edit-profile" data-toggle="dropdown"><i class="fas fa-ellipsis-v"></i></a>
							<ul class="dropdown-menu dropdown-menu-right">
                              <li><a href="javascript:void(0)" class="dropdown-item" data-toggle="modal" data-target="#editProfile">Edit profile</a></li>
                              <li><a href="javascript:void(0)" class="dropdown-item" data-toggle="modal" data-target="#changePassword">Change password</a></li>
                            </ul>

						</div>
                    </div>


                    <div class="row mt-2">
                        <div class="col flex-grow-0 pr-0">
                            <figure class="profile-item-logo"><img src="{{asset('/uploads/schools/'.$student->userData->school->logo)}}" alt=""></figure>
                        </div>
                        <div class="col flex-grow-1">
                            <h3 class="profile-item-name">{{ $student->userData->school->school_name }}</h3>
                            <h4 class="profile-item-location">{{ $student->userData->course->name }}</h4>
                        </div>
                    </div>
                    <hr class="mt-2 mb-4">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="profile-achievement">
                                <li>
                                    <span class="icon"><img src="images/icon-eye.png" alt=""></span>
                                    <span class="text">{{$classesWatched}}	Classes watched</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="images/icon-notes.png" alt=""></span>
                                    <span class="text">{{$noteDownloads}} Lecture notes downloaded</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="images/icon-question.png" alt=""></span>
                                    <span class="text">{{$questionsCount}} Questions asked</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="images/icon-tick.png" alt=""></span>
                                    <span class="text">{{$replyCount}} Answers contributed</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="images/icon-group.png" alt=""></span>
                                    <span class="text">0 groups joined</span>
                                </li>
                                <li>
                                    <span class="icon"><img src="images/icon-added.png" alt=""></span>
                                    <span class="text">0 friends added</span>
                                </li>

                            </ul>
                        </div>
                        <div class="col-lg-6 mt-3 mt-lg-0">
{{--                            <div class="profile-progress">--}}
{{--                                <small class="d-block">Profile complete <strong>{{ round($student->userData->profilePercentage($student->userData->user_id) ,2)}}%</strong></small>--}}
{{--                                <div class="progress my-1">--}}
{{--                                    <div class="progress-bar progress-bar" style="width:{{ $student->userData->profilePercentage($student->userData->user_id) }}%"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="pt-1">
                                <small class="d-block">Achievement</small>
                                <ul class="profile-achievement-type">
								@if(!empty($starShow->silverCount))
									<li class="silver" data-toggle="tooltip" data-placement="top" title="{{$starShow->silverDec}}">
										<div class="icon" ><i class="fas fa-star"></i></div>
										<div class="text">{{$starShow->silverCount}}</div>
									</li>
								@endif
								@if(!empty($starShow->bronzeCount))
									<li class="bronze" data-toggle="tooltip" data-placement="top" title="{{$starShow->bronzeDec}}">
										<div class="icon"><i class="fas fa-star"></i></div>
										<div class="text">{{$starShow->bronzeCount}}</div>
									</li>
								@endif
								@if(!empty($starShow->blueCount))
									<li class="blue" data-toggle="tooltip" data-placement="top" title="{{$starShow->blueDec}}">
										<div class="icon"><i class="fas fa-star"></i></div>
										<div class="text">{{$starShow->blueCount}}</div>
									</li>
								@endif
								@if(!empty($starShow->yellowCount))
									<li class="yellow" data-toggle="tooltip" data-placement="top" title="{{$starShow->yellowDec}}">
										<div class="icon"><i class="fas fa-star"></i></div>
										<div class="text">{{$starShow->yellowCount}}</div>
									</li>
								@endif
								</ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<section class="tab-card">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" title="" onclick="studentObj.studentHistory('histories')" data-toggle="tab" href="#History">History</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" title="" onclick="studentObj.studentHistory('favourites')" data-toggle="tab" href="#Favourites">Favourites</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" title="" onclick="studentObj.studentHistory('bag')" data-toggle="tab" href="#Bag">School Bag</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-5 d-flex py-2 py-md-0 profile-filter">
                <div class="row align-items-center justify-content-end">
                    <div class="col flex-grow-0  text-right">Filter&nbsp;By:</div>
                    <div class="col-8 flex-grow-1">
                        <div class="row filter-row">
                            <div class="col">
                                <div class="custom-select-outer">
                                <select class="custom-select filterby-form-control" id="school_id" onchange="studentObj.studentHistory('', 0, 1)" >
                                    <option value="">School </option>
                                    @if(!empty($schools))
                                    @foreach($schools as $skey => $school)
                                    <option value="{{$skey}}">{{$school}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            </div>
                            <div class="col">
                                <div class="custom-select-outer">
                                <select class="custom-select filterby-form-control" id="course_id" onchange="studentObj.studentHistory('', 0, 1)" >
                                    <option value="">Course</option>
                                    @if(!empty($courses))
                                    @foreach($courses as $ckey => $course)
                                    <option value="{{$ckey}}">{{$course}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            </div>
                        </div>



                    </div>
                </div>

            </div>

        </div>


    </div>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane container active pad-inherit history" id="History">
            <div id="historyHtml">


            </div>

            <button type="button" style="display:none;" id="historyMore" onclick="studentObj.studentHistory('histories', 1)" class="btn btn-secondary w-100">SHOW MORE</button>
        </div>

        <div class="tab-pane container pad-inherit" id="Favourites">
            <div id="favouritesHtml">
                <div class="item-row">
                    <div class="row">
                        <div class="col flex-grow-0">
                            <figure class="item-row-image"><img src="images/class-logo.png" alt=""></figure>
                        </div>
                        <div class="col flex-grow-1">
                            <h3 class="heading">Accra High School (Ahisco)</h3>
                            <p class="heading-sub-text">General Arts 1 (Economics - Scarcity)</p>
                        </div>
                        <div class="col  flex-grow-0 mt-2 mt-md-0 ">
                            <div class="action-button d-flex align-items-center">
                                <a href="#" title="" class="btn-play">Watch</a>
                                <a href="#" title="" class="btn-remove ml-3">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <button type="button" style="display:none;" id="favouritesMore" onclick="studentObj.studentHistory('favourites', 1)" class="btn btn-secondary w-100">SHOW MORE</button>


        </div>

        <div class="tab-pane pad-inherit container mb-5" id="Bag">
            <div class="d-flex">
                <div class="mt-3 pr-2">Filter by</div>
                <div class="p-2">
                    <select class="custom-select-sm" onchange="studentObj.filterFiletype('bag',0,0, this)">
                        <option value="" disabled selected>File type</option>
                        <option value="all">All</option>
                        <option value="mp3">Audio</option>
                        <option value="mp4">Video</option>
                        <option value="pdf">Book</option>
                        <option value="ppt" >Slide</option>
                        <option value="jpg">Photo</option>
                    </select>
                </div>
                <div class="p-2">
                    <select class="custom-select-sm" onchange="studentObj.filterFileRecent('bag',0,0,this)">
                        <option value="all" selected>Recent</option>
                        <option value="week">Last Week</option>
                        <option value="month" >Last Month</option>
                        <option value="year" >Last Year</option>
                    </select>
                </div>
                {{--                <div class="ml-auto p-2">--}}
                {{--                    <div class="d-flex">--}}
                {{--                        <span class="p-2">Show/page</span>--}}
                {{--                        <select class="form-control ml-2">--}}
                {{--                            <option value="" selected>9 cards</option>--}}
                {{--                        </select>--}}
                {{--                    </div>--}}

                {{--                </div>--}}
            </div>

            <div class="mt-4" id="bagHtml">

            </div>
            <button type="button" style="display:none;" id="bagMore" onclick="studentObj.studentHistory('bag', 1)" class="btn btn-secondary w-100">SHOW MORE</button>

        </div>

    </div>
</section>
<div class="modal fade custom-modal" id="subscribeModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="register-card">
                    <div class="register-card-header">
                        <h1 class="heading">Subscribe</h1>
{{--                        <p>Select your subscription plan and payment method</p>--}}
                        <p>Kindly make payments to the XtraClass Mobile Money Number below to Subscribe</p>
                        <p>Account Name: <strong>Real Studios Limited</strong></p>
                        <p>Account Number: <strong>054 300 8164</strong></p>
                        <p>Kindly use your XtraClass Username as reference and call 0509962708 to confirm</p>
                    </div>
{{--
{{--                    <form id="subscribeForm" action="{{route("frontend.action.pay")}}" method="post">--}}
{{--                            @csrf--}}
{{--                        <div class="register-card-body">--}}
{{--                            <div id="subError"></div>--}}
{{--                            <input type="hidden" name="activate_mode" value="self">--}}

{{--                            <div class="form-group" style="margin-top: -10px">--}}
{{--                                <label for="network">Wallet</label>--}}
{{--                                <select id="network" name="network" class="form-control {{ $errors->has('sub_wallet') ? 'is-invalid' : '' }}" data-validation="required">--}}
{{--                                    <option value="MTN" selected>MTN Mobile Money</option>--}}
{{--                                    <option value="VODAFONE" >Vodafone Cash</option>--}}
{{--                                    <option value="AIRTEL">Airtel Money</option>--}}
{{--                                    <option value="TIGO">Tigo Cash</option>--}}
{{--                                </select>--}}
{{--                                @if ($errors->has('sub_wallet'))--}}
{{--                                    <div class="invalid-feedback">{!! $errors->first('sub_wallet') !!}</div>--}}
{{--                                @else--}}
{{--                                    <small class="form-text">{{ "Select your payment network" }}</small>--}}
{{--                                @endif--}}
{{--                            </div>--}}

{{--                            <div class="form-group" style="margin-top: -15px">--}}
{{--                                <label for="momo_number">Mobile money number</label>--}}
{{--                                <div class="input-group">--}}
{{--                                    <div class="input-group-prepend">--}}
{{--                                        <span class="input-group-text"> +233 </span>--}}
{{--                                    </div>--}}
{{--                                    <input id="momo_number" name="momo_number" type="tel" class="form-control {{ $errors->has('sub_num') ? 'is-invalid' : '' }}" data-validation="required length number" data-validation-length="10-15" placeholder="0244123456">--}}
{{--                                </div>--}}
{{--                                @if ($errors->has('sub_num'))--}}
{{--                                    <div class="invalid-feedback">{!! $errors->first('sub_num') !!}</div>--}}
{{--                                @else--}}
{{--                                    <small class="form-text">{{ "A prompt will be sent to this number for approval" }}</small>--}}
{{--                                @endif--}}
{{--                            </div>--}}

{{--                            <div class="form-group" style="margin-top: -15px">--}}
{{--                                <label for="plan">Plans</label>--}}
{{--                                <select name="plan" id="plan" class="form-control {{ $errors->has('sub_plan') ? 'is-invalid' : '' }}" data-validation="required">--}}
{{--                                    <option value="" selected disabled>Select Plan</option>--}}
{{--                                </select>--}}

{{--                                @if ($errors->has('sub_plan'))--}}
{{--                                    <div class="invalid-feedback">{!! $errors->first('sub_plan') !!}</div>--}}
{{--                                @else--}}
{{--                                    <small class="form-text">{{ "Pick from our various plans what suites you" }}</small>--}}
{{--                                @endif--}}
{{--                            </div>--}}


{{--                        </div>--}}
{{--                        <hr class="mt-4">--}}
{{--                        <div class="register-card-footer">--}}

{{--                            <button type="submit"  id="submitSubscription" class="btn btn-primary w-100">Subscribe</button>--}}
{{--                        </div>--}}

{{--                    </form>--}}
                </div>
            </div>
        </div>
    </div>
</div>



@include('frontend.includes.contact_banner')
<input type="hidden" id="activetedTab" data-histories="0" data-favourites="0" data-bag="0">
<input type="hidden" id="activeTabInput" value="histories">
<input type="hidden" id="historyPage" value="1">
<input type="hidden" id="favouritesPage" value="1">
<input type="hidden" id="bagPage" value="1">
@include('frontend.student.scripts')

@if (Session::get('newUser'))
    <script>
        $(document).ready(function () {
            const tour = new Shepherd.Tour({
                defaultStepOptions: {
                    cancelIcon: {
                        enabled: true
                    },
                    classes: 'shadow-md bg-purple-dark',
                    scrollTo: { behavior: 'smooth', block: 'center' },
                },
                useModalOverlay: true
            });

            tour.addStep({
                title: 'Welcome to XtraClass',
                text: 'Let us guide you on how to use\
                   the platform in a few steps.',

                buttons: [
                    {
                        action() {
                            $.ajax({
                                url: '{{route('frontend.closeSession')}}',
                                type: 'POST',
                                data: {id: 1},
                                success: function (data) {
                                    console.log(data);
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                            return this.cancel();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Cancel'
                    },
                    {
                        action() {
                            return this.next();
                        },
                        text: "Let's Go"
                    }
                ],
                id: 'creating'
            });

            tour.addStep({
                title: 'Change profile image',
                text: 'Hover on the image to either choose an image\
                   or an avatar.',
                attachTo: {
                    element: '.profile-image',
                    on: 'bottom'
                },

                buttons: [
                    {
                        action() {
                            return this.back();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Back'
                    },
                    {
                        action() {
                            return this.next();
                        },
                        text: "Next"
                    }
                ],
                id: 'photo'
            });

            tour.addStep({
                title: 'Activate account',
                text: 'Pick from either our monthly, per term or school year plans.',
                attachTo: {
                    element: '.activate-profile',
                    on: 'bottom'
                },

                buttons: [
                    {
                        action() {
                            return this.back();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Back'
                    },
                    {
                        action() {
                            return this.next();
                        },
                        text: "Next"
                    }
                ],
                id: 'profile-info'
            });

            tour.addStep({
                title: 'Edit profile',
                text: 'Click here to update your profile info or change your password.',
                attachTo: {
                    element: '.edit-profile',
                    on: 'bottom'
                },

                buttons: [
                    {
                        action() {
                            return this.back();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Back'
                    },
                    {
                        action() {
                            return this.next();
                        },
                        text: "Next"
                    }
                ],
                id: 'edit-profile-menu'
            });

            tour.addStep({
                title: 'Progress',
                text: 'Here you can find your progress on the platform.',
                attachTo: {
                    element: '.profile-achievement',
                    on: 'right'
                },

                buttons: [
                    {
                        action() {
                            return this.back();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Back'
                    },
                    {
                        action() {
                            return this.next();
                        },
                        text: "Next"
                    }
                ],
                id: 'progress'
            });

            tour.addStep({
                title: 'Achievement',
                text: 'Earn stars as you continue using the platform.',
                attachTo: {
                    element: '.profile-achievement-type',
                    on: 'left'
                },

                buttons: [
                    {
                        action() {
                            return this.back();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Back'
                    },
                    {
                        action() {
                            return this.next();
                        },
                        text: "Next"
                    }
                ],
                id: 'achievement'
            });

            tour.addStep({
                title: 'History',
                text: 'This shows all the past lessons you have taken.',
                attachTo: {
                    element: '.history',
                    on: 'top'
                },

                buttons: [
                    {
                        action() {
                            return this.back();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Back'
                    },
                    {
                        action() {
                            return this.next();
                        },
                        text: "Next"
                    }
                ],
                id: 'history'
            });

            tour.addStep({
                title: 'Filter',
                text: 'Filter your history and favourites by institution or class.',
                attachTo: {
                    element: '.profile-filter',
                    on: 'top'
                },

                buttons: [
                    {
                        action() {
                            return this.back();
                        },
                        classes: 'shepherd-button-secondary',
                        text: 'Back'
                    },
                    {
                        action() {
                            $.ajax({
                                url: '{{route('frontend.closeSession')}}',
                                type: 'POST',
                                data: {id: 1},
                                success: function (data) {
                                    console.log(data);
                                },
                                error: function (error) {
                                    console.log(error);
                                }
                            });
                            return this.cancel();
                        },
                        text: "Finish"
                    }
                ],
                id: 'filter'
            });

            tour.start();
        })

    </script>
@endif

@endsection








