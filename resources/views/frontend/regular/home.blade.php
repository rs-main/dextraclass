@extends('frontend.layouts.app')

<style>
    .joinanchor {
        color: white;
        text-decoration: underline;
    }

</style>

@section('content')
@include('frontend.includes.landing_regular_school_page_hero')

@include( 'frontend.includes.open_school_search_form')
@include( 'frontend.includes.my_school_search_form')
@include(' frontend.includes.professional_search_form')

    @include('frontend.includes.open_school_carousel')
    @include('frontend.includes.professional_carousel')
    @include('frontend.includes.my_school_carousel')

<section class="any-device">
    <div class="container">
        <div class="row">
            <div class="col-md-7 offset-md-5 text-center text-md-right">
                <div class="device-info">
                    <div class="pb-0 pb-md-5" >
                        <h2 class="heading text-center text-md-right">Join from any device</h2>
                        <p class="heading-sub-text text-center text-md-right">PC, smartphone, tablet, iPhone etc.</p>
                    </div>
                    <div class="pt-2 pt-md-4">
                        <a href="{{route('frontend.pages.how_to_access')}}" class="btn-gray-outline">LEARN HOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@guest
<section class="login-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6" style="padding: 50px;">
                <div class="login-card">
                    <div class="sub-heading pb-4"><strong>Login </strong>to enable you ask questions, & connect with classmates</div>
                    <form action="{{ route('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input name="username" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" value="{{ old('username') }}" placeholder="{{ __('Username') }}">
                            @if ($errors->has('username'))
                            <div class="invalid-feedback">
                                {{ $errors->first('username') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="{{ __('Password') }}">
                            @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary w-100">LOGIN</button>
                        </div>
                        <div class="form-group info-text">or register if you don't have an account</div>
                        <div class="form-group mb-0">
                            <a href="{{ route('register') }}" class="btn btn-secondary w-100" title="Register">REGISTER</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endguest
<section class="contact-banner bg-white">
    <div class="container">
        <div class="contact-banner-box">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <aside>
                        <h3>Get your school online</h3>
                        <p>Contact us to get your school online at no cost</p>
                    </aside>
                </div>
                <div class="col-lg-5 offset-lg-2 mt-3 mt-lg-0">
                    <a href="{{route('frontend.pages.contact')}}" class="btn-white-outline w-100">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
//    $(".btn-group, .dropdown").hover(
//        function () {
//            $('>.dropdown-menu', this).stop(true, true).fadeIn("fast");
//            $(this).addClass('open');
//        },
//        function () {
//            $('>.dropdown-menu', this).stop(true, true).fadeOut("fast");
//            $(this).removeClass('open');
//    });


</script>

@endsection

@section('footer-scripts')
<script>
    $(document).ready(function(){
        $("#selfLearn").on('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            let _token   = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('home')}}",
                type:"POST",
                data:{
                    mode: 'open',
                  _token: _token
                },
            })
        })
    })

</script>

<script>
    let joinClassObj = {

        separator:'/',
        strDouble:'"',
        strSingle:"'",
        parentUrl:'{{URL::to("/")}}',
        jsId : {
            formId               		: "#joinSchool",
            institutionDiv       		: "#institution_id",
            schoolDiv       		    : "#school_div",
            departmentDiv       		: "#department_div",
            courseDiv       		    : "#course_div",
            classDiv       		        : "#class_div",
            institutionId       		: "#institution_id",
            schoolId        			: "#school_id",
            departmentId        		: "#department_id",
            courseId        			: "#course_id",
            classId        				: "#class_id",
            searchInput        			: "#search_input"
        },
        status : {
            success     : 200
        },
        jsClass : {
            navItem 				: ".nav-item"

        },
        jsNames : {
            noname 			: "",
        },
        jsValue : {
            institutionVal        		: "{{Request::input('institution_id')}}",
            schoolVal        			: "{{Request::input('school_id')}}",
            departmentVal        		: "{{Request::input('department_id')}}",
            courseVal        			: "{{Request::input('course_id')}}",
            classVal        			: "{{Request::input('class_id')}}",
            searchVal        			: "{{Request::input('search_input')}}"
        },
        extra : {
            jsSeparator:'-',
            url : {
                getInstitutionOptinns : '{{route("frontend.api.getInstitutionOptinns")}}',
                getSchoolOptinns      : '{{route("frontend.api.getSchoolOptinns")}}',
                getCourseOptions      : '{{route("frontend.api.getDepartmentOrCourseOptions")}}',
                getClassOptions       : '{{route("frontend.api.getClassOptions")}}'
            }
        },
        createUrl : function(set){
            return ( this.parentUrl+this.separator+set);
        }
    }

    let joinMySchoolObj = {

        separator:'/',
        strDouble:'"',
        strSingle:"'",
        parentUrl:'{{URL::to("/")}}',
        jsId : {
            formId               		: "#joinSchool",
            institutionDiv       		: "#regular_institution_id",
            schoolDiv       		    : "#regular_school_div",
            departmentDiv       		: "#regular_department_div",
            courseDiv       		    : "#regular_course_div",
            classDiv       		        : "#regular_class_div",
            institutionId       		: "#regular_institution_id",
            schoolId        			: "#regular_school_id",
            departmentId        		: "#regular_department_id",
            courseId        			: "#regular_course_id",
            classId        				: "#regular_class_id",
            searchInput        			: "#regular_search_input"
        },
        status : {
            success     : 200
        },
        jsClass : {
            navItem 				: ".nav-item"

        },
        jsNames : {
            noname 			: "",
        },
        jsValue : {
            institutionVal        		: "{{Request::input('regular_institution_id')}}",
            schoolVal        			: "{{Request::input('regular_school_id')}}",
            departmentVal        		: "{{Request::input('regular_department_id')}}",
            courseVal        			: "{{Request::input('regular_course_id')}}",
            classVal        			: "{{Request::input('regular_class_id')}}",
            searchVal        			: "{{Request::input('regular_search_input')}}"
        },
        extra : {
            jsSeparator:'-',
            url : {
                getInstitutionOptinns : '{{route("frontend.api.getInstitutionOptinns")}}',
                getSchoolOptinns      : '{{route("frontend.api.getSchoolOptinns")}}',
                getCourseOptions      : '{{route("frontend.api.getDepartmentOrCourseOptions")}}',
                getClassOptions       : '{{route("frontend.api.getClassOptions")}}'
            }
        },
        createUrl : function(set){
            return ( this.parentUrl+this.separator+set);
        }
    }

    let joinProfessionalObj = {

        separator:'/',
        strDouble:'"',
        strSingle:"'",
        parentUrl:'{{URL::to("/")}}',
        jsId : {
            formId               		: "#joinSchool",
            institutionDiv       		: "#professional_institution_id",
            schoolDiv       		    : "#professional_school_div",
            departmentDiv       		: "#professional_department_div",
            courseDiv       		    : "#professional_course_div",
            classDiv       		        : "#professional_class_div",
            institutionId       		: "#professional_institution_id",
            schoolId        			: "#professional_school_id",
            departmentId        		: "#professional_department_id",
            courseId        			: "#professional_course_id",
            classId        				: "#professional_class_id",
            searchInput        			: "#professional_search_input"
        },
        status : {
            success     : 200
        },
        jsClass : {
            navItem 				: ".nav-item"

        },
        jsNames : {
            noname 			: "",
        },
        jsValue : {
            institutionVal        		: "{{Request::input('professional_institution_id')}}",
            schoolVal        			: "{{Request::input('professional_school_id')}}",
            departmentVal        		: "{{Request::input('professional_department_id')}}",
            courseVal        			: "{{Request::input('professional_course_id')}}",
            classVal        			: "{{Request::input('professional_class_id')}}",
            searchVal        			: "{{Request::input('professional_search_input')}}"
        },
        extra : {
            jsSeparator:'-',
            url : {
                getInstitutionOptinns : '{{route("frontend.api.getInstitutionOptinns")}}',
                getIndustryOptions    : '{{route("frontend.api.getIndustryOptions")}}',
                getCategoryOptions    : '{{route("frontend.api.getCategoryOptions")}}',
                getSchoolOptinns      : '{{route("frontend.api.getSchoolOptinns")}}',
                getCourseOptions      : '{{route("frontend.api.getDepartmentOrCourseOptions")}}',
                getClassOptions       : '{{route("frontend.api.getClassOptions")}}'
            }
        },
        createUrl : function(set){
            return ( this.parentUrl+this.separator+set);
        }
    }

    let joinAclassObject = new JoinAClassBox(joinClassObj);

    let joinMyclassObject = new JoinAClassBox(joinMySchoolObj);

    let joinProfessionalClassObject = new TakeACourse(joinProfessionalObj);

    $('#class_id').on('change', function(){
        if($(this).val() != '') {
            $('.join_button').removeAttr('disabled');
        }
    });

    $('#institution_id, #school_id, #department_id, #course_id').on('change', function(){
        if($(this).val() != '') {
            $('.join_button').attr('disabled',true);
        }
    });

    $('#regular_class_id').on('change', function(){
        if($(this).val() != '') {
            $('.regular_join_button').removeAttr('disabled');
        }
    });

    $('#regular_institution_id, #regular_school_id, #regular_department_id, #regular_course_id').on('change', function(){
        if($(this).val() != '') {
            $('.regular_join_button').attr('disabled',true);
        }
    });


</script>

@endsection
