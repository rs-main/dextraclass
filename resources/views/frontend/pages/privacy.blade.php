@extends('frontend.layouts.app')

@section('title', 'Terms & Conditions')

@section('content')
<section class="howtoaccess-banner">	
    <div class="container">
        <div class="row h-100">
            <div class="col-md-7">
                <div class="caption">
                    <h1 class="heading">Terms & Conditions</h1>
                </div>
            </div>
            <div class="col-md-5 d-flex align-items-end justify-content-center justify-content-md-end">
                <img src="{{asset('images/details-18.png')}}" alt="" class="img-fluid">
            </div>
        </div>	
    </div>
</section>

<section class="bg-gray py-5">
    <div class="container py-0 py-md-3">
        <div class="row">
            <div class="col-md-8 d-flex flex-column justify-content-center">
                Coming Soon...
            </div>
        </div>	
    </div>
</section>

@endsection
