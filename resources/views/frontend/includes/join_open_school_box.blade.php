<script>
    let joinSchoolObj = {

        separator:'/',
        strDouble:'"',
        strSingle:"'",
        parentUrl:'{{URL::to("/")}}',
        jsId : {
            formId               		: "#joinSchool",
            institutionDiv       		: "#institution_id",
            schoolDiv       		    : "#school_div",
            departmentDiv       		: "#department_div",
            courseDiv       		    : "#course_div",
            classDiv       		        : "#class_div",
            institutionId       		: "#institution_id",
            schoolId        			: "#school_id",
            departmentId        		: "#department_id",
            courseId        			: "#course_id",
            classId        				: "#class_id",
            searchInput        			: "#search_input"
        },
        status : {
            success     : 200
        },
        jsClass : {
            navItem 				: ".nav-item"

        },
        jsNames : {
            noname 			: "",
        },
        jsValue : {
            institutionVal        		: "{{Request::input('institution_id')}}",
            schoolVal        			: "{{Request::input('school_id')}}",
            departmentVal        		: "{{Request::input('department_id')}}",
            courseVal        			: "{{Request::input('course_id')}}",
            classVal        			: "{{Request::input('class_id')}}",
            searchVal        			: "{{Request::input('search_input')}}"
        },
        extra : {
            jsSeparator:'-',
            url : {
                getInstitutionOptinns : '{{route("frontend.api.getInstitutionOptinns")}}',
                getSchoolOptinns      : '{{route("frontend.api.getSchoolOptinns")}}',
                getCourseOptions      : '{{route("frontend.api.getDepartmentOrCourseOptions")}}',
                getClassOptions       : '{{route("frontend.api.getClassOptions")}}'
            }
        },
        createUrl : function(set){
            return ( this.parentUrl+this.separator+set);
        }
    }

    // let joinAclassObject = new JoinAClassBox(joinSchoolObj);


    // /*import {JoinASchool} from "/js/join_a_school.js"
    // (function() { window.JoinAClass = new JoinASchoolBox(joinClassObj); })();
</script>

<form action="{{route('frontend.schools')}}" method="post" class="" id="joinSchool">
@csrf
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg mb-2 mb-lg-0" id="institution_div">
                    <label>Institution</label>
                    <div class="custom-select-outer">
                        <select class="custom-select"  onchange="joinAclassObject.schoolList(true, 'institution')"
                                name="institution_id" id="institution_id">
                            <option class="d-none">Institution</option>
							@if(!empty($openSchoolCategories))
								@foreach($openSchoolCategories as $openSchoolCategory)
                                        <option value="{{$openSchoolCategory->id}}">{{$openSchoolCategory->name}}</option>
								@endforeach
							@endif
                        </select>
                    </div>
                </div>


                <div class="col-lg mb-2 mb-lg-0 hide" id="school_div">
{{--                <label>School</label>--}}
                <label>Class</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled onchange="joinAclassObject.courseList(true, 'school')" name="school_id" id="school_id">
                            <option class="d-none">Institute Name</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg mb-2 mb-lg-0 hide"  id="department_div">
                    <label>Department</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled onchange="joinAclassObject.courseList(true, 'department')" name="department_id" id="department_id">
                            <option class="d-none">Department</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg mb-2 mb-lg-0 hide" style="display:none;" id="course_div">
                    <label>Course</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled onchange="joinAclassObject.classList(true, 'course')" name="course_id" id="course_id">
                            <option class="d-none">Course</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg mb-2 mb-lg-0 hide" id="class_div">
{{--                    <label>Class</label>--}}
                    <label>Subject</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled name="class_id" id="class_id">
{{--                            <option class="d-none">Class</option>--}}
                            <option class="d-none">Subject</option>
                        </select>
                    </div>
                </div>
				<div class="col-lg mb-2 mb-lg-0">
                    <label>&nbsp;</label>
            		<button type="button" onclick="joinAclassObject.onSubmitBtn(true)" class="btn btn-success w-100 join_button" disabled>JOIN</button>
       		 </div>
            </div>
        </div>

    </div>
</form>
