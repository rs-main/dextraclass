<section class="content-top-wrapper bg-white">
    <div class="container">
        <div class="join-class-wrapper text-white" id="landing-school-hero-wrapper">
        <div class="row pt-0 pb-0 pt-md-4 pb-md-5">
                <div class="col-md-8 col-xl-6 pb-0 pb-md-3">
                    <h1 id="hero-header" class="">Join a class at ATTC</h1>
                    <p id="hero-paragraph" class="col-lg-12">
                        remotely from anywhere on the planet, Stream, pause & repeat any teaching session till you understand select a school below to start learning now
                    </p>
                </div>
            </div>
            <div class="pb-5">
                @include('frontend.includes.join_a_school_box')
            </div>
            
            
        </div>  
    </div>
</section>
