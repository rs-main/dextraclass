<script>

    let tutorActionsClassObj = {
        parentUrl:'{{URL::current()}}',
        jsId : {
            activeTabInput: "#activeTabInput",
            AddVideoBtn   : "#AddVideoBtn",
            LiveStreamBtn : "#LiveStreamBtn",
            AddQuizBtn    : "#AddQuizBtn",
            AddArticleBtn : "#AddArticleBtn",
            VideosNavLink : "#VideosNavLink",
            QuizzesNavLink : "#QuizzesNavLink",
            ArticlesNavLink : "#ArticlesNavLink",
            LoginBtn      : "#LoginBtnTop",
            tutorActionsPopUpDiv : "#tutorActionsPopUpDiv",
            returnUrl     : "#returnUrl",
            userName      : "#userName",
            userPassword  : "#userPassword"
        },
        jsData : {

        },
        status : {
            success     : 200
        },
        jsClass : {
            navItem  : ".nav-item"
        },
        extra : {
            url : {
                tutorProfileUrl : '{{route("frontend.profile")}}',
                loginUrl  : '{{route("frontend.ajaxLogin")}}'
            }
        }
    }

    /* import {LoginClass} from "/js/login.js";
    window.loginObj = new LoginClass(loginClassObj);  */
</script>
<script src="{{ asset('js/login.js') }}"></script>
<script>
    $(document).ready(function(){
        $('#userName').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            var userPassword = $("#userPassword").val();
            var userName     = $("#userName").val();

            if(keycode == '13'){
                if(userName){
                    if(userPassword){
                        loginObj.loginUser();
                    }else{
                        $("#userPassword").focus();
                    }
                }
            }
        });
        $('#userPassword').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            var userPassword = $("#userPassword").val();
            var userName     = $("#userName").val();

            if(keycode == '13'){
                if(userPassword && userName){
                    loginObj.loginUser();
                }
            }
        });
        $(".google-signup").on("click",function (){
            location.href = "{{url('/login/google')}}";
        })
        $(".email-signup").on("click",function (){
            location.href = "{{ route('register') }}";
        })

        // $(".mobile-signin").on("click",function (){
        //     $("#username_mobile").show();
        //     $("#username_login").hide();
        // })

    });
</script>

<div id="tutorActionsPopUpDiv" class="dropdown-menu tutor-actions-dropdown justify-content-center align-items-center flex-column" style="display: none">
    <a href="javascript:void(0)" onclick="tutorActionsObj.goToLiveStream()">Live Stream</a>
    <div class="separator"></div>
    <a href="javascript:void(0)" onclick="tutorActionsObj.goToAddVideo()">Add Video</a>
    <div class="separator"></div>
    <a href="javascript:void(0)" onclick="tutorActionsObj.goToCreateTest()">Create Test</a>
    <div class="separator"></div>
    <a href="javascript:void(0)" onclick="tutorActionsObj.goToPostArticle()">Post Article</a>
</div>

<script>
    class tutorActionsClass {
        constructor(external) {
            this.ext = external;
        }

        goToLiveStream() {
            var ids = this.ext.jsId;
            var url = this.ext.extra.url;
            var redirectUrl = this.createUrl(url.tutorProfileUrl, "#go-to-livestream") ;

            if (this.ext.parentUrl != url.tutorProfileUrl) {
                commonObj.redirect(redirectUrl)
            }

        var _delay = setTimeout(function() {
            $(ids.VideosNavLink).click();
            $(ids.LiveStreamBtn).click();
        }, 1000);

        }

        goToAddVideo() {
            var ids = this.ext.jsId;
            var url = this.ext.extra.url;
            var redirectUrl = this.createUrl(url.tutorProfileUrl, "#go-to-add-video") ;

            if (this.ext.parentUrl != url.tutorProfileUrl) {
                commonObj.redirect(redirectUrl)
            }

        var _delay = setTimeout(function() {
            $(ids.VideosNavLink).click();
            $(ids.AddVideoBtn).click();
        }, 1000);

        }

        goToCreateTest() {

            var ids = this.ext.jsId;
            var url = this.ext.extra.url;
            var redirectUrl = this.createUrl(url.tutorProfileUrl, "#go-to-create-test") ;

            if (this.ext.parentUrl != url.tutorProfileUrl) {
                commonObj.redirect(redirectUrl)
            }

        var _delay = setTimeout(function() {
            $(ids.QuizzesNavLink).click();
            $(ids.AddQuizBtn).click();
        }, 1000);
        }

        goToPostArticle() {
            var ids = this.ext.jsId;
            var url = this.ext.extra.url;
            var redirectUrl = this.createUrl(url.tutorProfileUrl, "#go-to-post-article");

            if (this.ext.parentUrl != url.tutorProfileUrl) {
                commonObj.redirect(redirectUrl)
            }

            var _delay = setTimeout(function() {
                $(ids.ArticlesNavLink).click();
                $(ids.AddArticleBtn).click();
            }, 1000);

        }

        openTutorActionsPopup(returnUrl = "", reloadIt = "") {

            if (returnUrl == "" && $(this.ext.jsId.activeTabInput).val()) {
                var v = $(this.ext.jsId.activeTabInput).val();
                //returnUrl = `?active=${v}`;
            }
            /* console.log(1,returnUrl); */
            $(this.ext.jsId.tutorActionsPopUpDiv).toggle(300);
            $(this.ext.jsId.returnUrl).val(returnUrl);
            $(this.ext.jsId.reloadIt).val(reloadIt);
        }

        createUrl(url = "", hash="") {
            return url + hash;
        }
    }
    var tutorActionsObj = new tutorActionsClass(tutorActionsClassObj);

</script>



<script>
    $(function () {
        var urlHash = window.location.hash;

        if (urlHash === "#go-to-livestream") {
            tutorActionsObj.goToLiveStream();
        }
        else if (urlHash === "#go-to-add-video") {
            tutorActionsObj.goToAddVideo();
        }
        else if (urlHash === "#go-to-create-test") {
            tutorActionsObj.goToCreateTest();
        }
        else if (urlHash === "#go-to-post-article") {
            tutorActionsObj.goToPostArticle();
        }
    });
</script>
