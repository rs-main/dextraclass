<section class="contact-banner">
	<div class="container">
		<div class="contact-banner-box">
			<div class="row align-items-center">
				<div class="col-lg-5">
					<aside>
						<h3>Get your school online</h3>
						<p>Contact us to get your school online at no cost</p>
					</aside>
				</div>
				<div class="col-lg-5 offset-lg-2 mt-3 mt-lg-0">
                                    <a href="{{route('frontend.pages.contact')}}" class="btn-white-outline w-100">CONTACT US</a>
				</div>
			</div>
		</div>
	</div>	
</section>