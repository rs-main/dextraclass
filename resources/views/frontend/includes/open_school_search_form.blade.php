<section class="content-top-wrapper bg-white" id="open-school-search-form">
    <div class="container">
        <div class="text-white organization-hero-wrapper">
        <div class="row pt-0 pb-0 pt-md-4 pb-md-0">
                <div class="col-md-8 pb-0 pb-md-3">

                    <p id="hero-paragraph">
                        Learn at your own pace, Join a class from K-12, nursery, kindergarten, Basic 1-6 and Junior High
                    </p>
                </div>
            </div>
            @include('frontend.includes.join_open_school_box')
        </div>
    </div>
</section>
