<section class="content-top-wrapper bg-white" id="my-school-search-form" style="display: none;">
    <div class="container">
        <div class="text-white organization-hero-wrapper">
        <div class="row pt-0 pb-0 pt-md-4 pb-md-0">
                <div class="col-md-8 pb-0 pb-md-3">

                    <p id="hero-paragraph">
                        Select your school below if you have access
                    </p>
                </div>
            </div>
{{--            @include('frontend.includes.join_open_school_box')--}}
            @include('frontend.includes.join_my_school_box')
        </div>
    </div>
</section>
