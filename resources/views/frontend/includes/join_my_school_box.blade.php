<script>

</script>

<form action="{{route('frontend.schools')}}" method="post" class="" id="joinSchool">
@csrf
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg mb-2 mb-lg-0" id="regular_institution_div">
                    <label>Institution</label>
                    <div class="custom-select-outer">
                        <select class="custom-select"  onchange="joinMyclassObject.schoolList(true, 'institution')" name="regular_institution_id"
                                id="regular_institution_id">
                            <option class="d-none">Institution</option>
							@if(!empty($regularSchoolCategories))
								@foreach($regularSchoolCategories as $regularSchoolCategory)
                                        <option value="{{$regularSchoolCategory->id}}">{{$regularSchoolCategory->name}}</option>
								@endforeach
							@endif
                        </select>
                    </div>
                </div>


                <div class="col-lg mb-2 mb-lg-0 hide" id="regular_school_div">
                <label>School</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled onchange="joinMyclassObject.courseList(true, 'school')" name="regular_school_id" id="regular_school_id">
                            <option class="d-none">Institute Name</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg mb-2 mb-lg-0 hide"  id="regular_department_div">
                    <label>Department</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled onchange="joinMyclassObject.courseList(true, 'department')" name="regular_department_id" id="regular_department_id">
                            <option class="d-none">Department</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg mb-2 mb-lg-0 hide" style="display:none;" id="regular_course_div">
                    <label>Course</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled onchange="joinMyclassObject.classList(true, 'course')" name="regular_course_id" id="regular_course_id">
                            <option class="d-none">Course</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg mb-2 mb-lg-0 hide" id="regular_class_div">
                    <label>Class</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled name="regular_class_id" id="regular_class_id">
                            <option class="d-none">Class</option>
                        </select>
                    </div>
                </div>
				<div class="col-lg mb-2 mb-lg-0">
                    <label>&nbsp;</label>
            		<button type="button" onclick="joinMyclassObject.onSubmitBtn(true)" class="btn btn-success w-100 regular_join_button" disabled>JOIN</button>
       		 </div>
            </div>
        </div>

    </div>
</form>
