<section class="content-top-wrapper bg-white">
    <div class="container">
        <div class="masterclass-hero-wrapper pb-0" id="regular-school-hero-wrapper">


                <div class="pb-0 pb-md-3 text-center">
                    <h1 id="hero-header" class="masterclass-hero-header text-black">Join your Class remotely</h1>
                    <p id="hero-paragraph" class="masterclass-hero-paragraph text-center text-black">
                        From anywhere on the planet, Stream, pause & repeat any classroom session on your
                        desktop and devices till you understand. Start by joining a class below.
                    </p>
                </div>


            <div class="row py-3">
                <div class="col-md-12 m-auto">
                    <div class="row">
                        <div class="col-lg col-sm-12 justify-content-end d-flex mb-lg-0 mb-3 mb-sm-3"><button class="btn btn-primary w-100"  id="regular-school-open-school-btn">Open School</button></div>
                        <div class="col-lg col-sm-12 mb-sm-3 mb-lg-0 mb-3"><button class="btn btn-default w-100 regular-school-nav-link" id="regular-school-my-school-btn">My School</button></div>
                        <div class="col-lg col-sm-12 mb-sm-3 mb-lg-0 mb-3 justify-content-start d-flex" ><button class="btn btn-default w-100 regular-school-nav-link " id="regular-school-professional-btn">Professional</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<noscript>
    Enable javascript for some functionality to work.
</noscript>

<script>
    $(document).ready(function() {
        var openSchoolBtn = $('#regular-school-open-school-btn');
        var mySchoolBtn = $('#regular-school-my-school-btn');
        var professionalBtn = $('#regular-school-professional-btn');

        openSchoolBtn.on('click', function() {
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
            mySchoolBtn.removeClass('btn-primary');
            professionalBtn.removeClass('btn-primary');
            mySchoolBtn.addClass('btn-default');
            professionalBtn.addClass('btn-default');

            $('#open-school-search-form').fadeIn(500);
            $('#open-school-carousel').fadeIn(500);

            $('#professional-school-search-form').fadeOut(500);
            $('#my-school-search-form').fadeOut(500);


            $('#professional-carousel').fadeOut(500);
            $('#my-school-carousel').fadeOut(500);

        });

        mySchoolBtn.on('click', function() {
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
            openSchoolBtn.removeClass('btn-primary');
            professionalBtn.removeClass('btn-primary');
            openSchoolBtn.addClass('btn-default');
            openSchoolBtn.addClass('regular-school-nav-link');
            professionalBtn.addClass('btn-default');

            $('#open-school-search-form').fadeOut(500);
            $('#professional-school-search-form').fadeOut(500);

            $('#my-school-carousel').fadeIn(500);
            $('#my-school-search-form').fadeIn(500);

            $('#open-school-carousel').fadeOut(500);
            $('#professional-carousel').fadeOut(500);

        });

        professionalBtn.on('click', function() {
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
            mySchoolBtn.removeClass('btn-primary');
            openSchoolBtn.removeClass('btn-primary');
            openSchoolBtn.addClass('regular-school-nav-link');
            mySchoolBtn.addClass('btn-default');
            openSchoolBtn.addClass('btn-default');

            $('#open-school-search-form').fadeOut(500);
            $('#professional-school-search-form').fadeIn(500);
            $('#professional-carousel').fadeIn(500);

            $('#my-school-search-form').fadeOut(500);

            $('#open-school-carousel').fadeOut(500);

            $('#my-school-carousel').fadeOut(500);
        });
    });

</script>
