<section class="school-carousel-wrapper bg-white">
    <div class="container">
        <h2 class="sub-heading">Classrooms streaming online now</h2>
        <div class="school-carousel owl-carousel owl-theme">

                @php
                    $schools = [
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "schoolcarousel1.png",
                            "school_name" => "General Arts 1 Form 1",
                            "lesson_num" => 20
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "schoolcarousel2.png",
                            "school_name" => "Auto Mechanic",
                            "lesson_num" => 16
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "schoolcarousel3.png",
                            "school_name" => "Home Economics",
                            "lesson_num" => 30
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "schoolcarousel4.png",
                            "school_name" => "Building Construction",
                            "lesson_num" => 15
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "schoolcarousel4.png",
                            "school_name" => "Becoming a great public speaker",
                            "lesson_num" => 15
                        ]
                    ]
                        
                    
                @endphp

                @foreach($schools as $school)
                    @if (in_array($school['school_category'], [19, 20, 21, 22]))
                        <div class="item">
                            <div class="school-card btn-group" >
                                <a href="javascript:void(0)" title="" class="school-card-link" onclick="JoinOpenClass2.openModel()" data-cat="" data-school="">
                                    @if($school['is_locked'])
                                        <div class="school-lock">
                                            <img src="/images/lock-icon.png" alt="lock">
                                        </div>
                                    @endif
                                    <figure>
                                        <img src="{{asset('images/'.$school['logo'])}}" class="img-fluid" height="73" alt="{{ $school['school_name'] }}">
                                    </figure>

                                    
                                            <h3 class="school-name">{{ $school['school_name'] }}</h3>
                                        
                                        <p class="school-course-qty">
                                            {{$school['lesson_num']}} Lessons
                                        </p>
                                    
                                    
                                   

                                </a>
                            </div>
                        </div>
                    @endif
                @endforeach
            
        </div>
    </div>
</section>
