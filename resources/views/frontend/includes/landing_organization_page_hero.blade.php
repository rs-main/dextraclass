<section class="content-top-wrapper bg-white">
    <div class="container">
        <div class="text-white organization-hero-wrapper">
        <div class="row pt-0 pb-0 pt-md-4 pb-md-5">
                <div class="col-md-8 col-xl-7 pb-0 pb-md-3">
                    <h1 id="hero-header" class="">Take a Professional Course</h1>
                    <p id="hero-paragraph">
                        from our experienced teachers, lessons endorsed by the Ghana Tourism Authority and offered in alignment with the COTVET standards to further your career.
                    </p>
                </div>
            </div>
            @include('frontend.includes.join_a_school_box')
            <div class="mt-5">
                <a href="http://127.0.0.1:8000/open" _target="self"><span class="joinanchor">Or Join a virtual Class</span> </a>
            </div>
        </div>  
    </div>
</section>
