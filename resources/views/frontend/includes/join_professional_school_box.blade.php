<script>

</script>

<form action="{{route('frontend.schools')}}" method="post" class="" id="joinSchool">
@csrf
    <div class="row">
        <div class="col-12">
            <div class="row">

{{--                <div class="col-lg mb-2 mb-lg-0" id="professional_institution_div">--}}
{{--                    <label>Institution</label>--}}
{{--                    <div class="custom-select-outer">--}}
{{--                        <select class="custom-select"  onchange="joinProfessionalClassObject.industryList(true, 'institution')" name="professional_institution_id"--}}
{{--                                id="professional_institution_id">--}}
{{--                            <option class="d-none">Institution</option>--}}
{{--							@if(!empty($professionalSchoolCategories))--}}
{{--								@foreach($professionalSchoolCategories as $professionalSchoolCategory)--}}
{{--                                        <option value="{{$professionalSchoolCategory->id}}">{{$professionalSchoolCategory->name}}</option>--}}
{{--								@endforeach--}}
{{--							@endif--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}


                <div class="col-lg mb-2 mb-lg-0 " id="professional_school_div">
                <label>Industry</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" onchange="getIndustryCategories()" name="professional_school_id" id="industries">
                            <option class="d-none">Industry name</option>
                        @foreach(\App\Industry::all() as $industry)
                                <option value="{{$industry->id}}">{{$industry->name}}</option>
                        @endforeach

                        </select>
                    </div>
                </div>

                <div class="col-lg mb-2 mb-lg-0"  id="professional_department_div">
                    <label>Categories</label>
                    <div class="custom-select-outer">
                        <select class="custom-select" disabled onchange="getCategoryCourses()" id="industry_categories">
                            <option class="d-none">Categories</option>
                        </select>
                    </div>
                </div>

{{--                <div class="col-lg mb-2 mb-lg-0" id="professional_course_div">--}}
{{--                    <label>Course</label>--}}
{{--                    <div class="custom-select-outer">--}}
{{--                        <select class="custom-select" disabled  name="professional_course_id"--}}
{{--                                id="professional_courses">--}}
{{--                            <option class="d-none">Course</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg mb-2 mb-lg-0 hide" id="professional_class_div">--}}
{{--                    <label>Class</label>--}}
{{--                    <div class="custom-select-outer">--}}
{{--                        <select class="custom-select" disabled name="professional_class_id" id="professional_class_id">--}}
{{--                            <option class="d-none">Class</option>--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}

				<div class="col-lg mb-2 mb-lg-0">
                    <label>&nbsp;</label>
{{--            		<button type="button" onclick="joinProfessionalClassObject.onSubmitBtn(true)"--}}
{{--                            class="btn btn-success w-100 professional_join_button" disabled>JOIN</button>--}}

                    <button type="button" onclick="viewCourses()"
                            class="btn btn-success w-100 professional_join_button" disabled>VIEW COURSES</button>
       		 </div>
            </div>
        </div>

    </div>
</form>
