<section class="content-top-wrapper bg-white">
    <div class="container">
        <div class="join-class-wrapper text-white" id="landing-cotvet-hero-wrapper">
        <div class="row pt-0 pb-0 pt-md-4 pb-md-5">
                <div class="col-md-8 col-xl-6 pb-0 pb-md-3">
                    <h1 id="hero-header" class="">Take a TVET Course</h1>
                    <p id="hero-paragraph" class="col-lg-12">
                        from our professional and experienced teachers giving you lessons to further your career
                    </p>
                </div>
            </div>
                @include('frontend.includes.join_a_school_box')
            
            
            <div class="mt-5">
                <a href="http://127.0.0.1:8000/open" _target="self"><span class="joinanchor">Or Learn your own pace</span> </a>
            </div>
        </div>  
    </div>
</section>
