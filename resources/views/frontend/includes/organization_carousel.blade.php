<section class="school-carousel-wrapper bg-white">
    <div class="container">
        <h2 class="sub-heading">Courses available now</h2>
        <div class="school-carousel owl-carousel owl-theme">

                @php
                    $schools = [
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "organization_carousel1.png",
                            "school_name" => "Hotel Management fundamentals",
                            "lesson_num" => 20
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "organization_carousel2.png",
                            "school_name" => "French basics for Communication",
                            "lesson_num" => 16
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "organization_carousel3.png",
                            "school_name" => "Ethiquettes of the Kitchen",
                            "lesson_num" => 30
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "organization_carousel4.png",
                            "school_name" => "Moving from 2 star to 4 star",
                            "lesson_num" => 15
                        ],
                        [
                            "school_category" => 19,
                            "is_locked" => false,
                            "logo" => "organization_carousel4.png",
                            "school_name" => "Becoming a great public speaker",
                            "lesson_num" => 15
                        ]
                    ]
                        
                    
                @endphp

                @foreach($schools as $school)
                    @if (in_array($school['school_category'], [19, 20, 21, 22]))
                        <div class="item">
                            <div class="school-card btn-group" >
                                <a href="javascript:void(0)" title="" class="school-card-link p-0" onclick="JoinOpenClass2.openModel()" data-cat="" data-school="">
                                    @if($school['is_locked'])
                                        <div class="school-lock">
                                            <img src="/images/lock-icon.png" alt="lock">
                                        </div>
                                    @endif
                                    <figure class="w-100">
                                        <img src="{{asset('images/'.$school['logo'])}}" class="img-fluid" height="73" alt="{{ $school['school_name'] }}">
                                    </figure>

                                    <div class="pt-0 pb-4 px-4">
                                            <h3 class="school-name">{{ $school['school_name'] }}</h3>
                                        
                                        <p class="school-course-qty">
                                            {{$school['lesson_num']}} Lessons
                                        </p>
                                    </div>
                                    
                                   

                                </a>
                            </div>
                        </div>
                    @endif
                @endforeach
            
        </div>
    </div>
</section>
