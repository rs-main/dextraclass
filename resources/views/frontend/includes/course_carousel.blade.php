<section class="school-carousel-wrapper bg-white">
    <div class="container">
        <h2 class="sub-heading">Courses available now</h2>
        <div class="school-carousel owl-carousel owl-theme">
            

            @if(!empty($schools))
                @foreach($schools as $school)
                    
                        <div class="item">
                            <div class="school-card btn-group" >
                                
                                <a href="javascript:void(0)" title="" class="school-card-link" onclick="JoinOpenClass2.openModel({{$school->school_category}},{{$school->id}})" data-cat="{{$school->school_category}}" data-school="{{$school->id}}">
                                    @if($school->is_locked)
                                        <div class="school-lock">
                                            <img src="/images/lock-icon.png" alt="lock">
                                        </div>
                                    @endif
                                    <figure class="school-logo">
                                        <img src="{{asset('/uploads/schools/'.$school->logo)}}" class="img-fluid" height="73" alt="{{ $school->school_name }}">
                                    </figure>

                                    <h3 class="school-name">{{ $school->school_name }}</h3>
                                    @if($school->school_category == config('constants.BASIC_SCHOOL'))
                                       
                                        <p class="school-course-qty">
                                      7 Lessons
                                        </p>
                                    @endif

                                </a>
                            </div>
                        </div>
                   
                @endforeach
            @endif
        </div>
    </div>
</section>
