<section class="school-carousel-wrapper bg-white" style="display: none;" id="my-school-carousel">
    <div class="container">
        <h2 class="sub-heading">Schools online now</h2>
        <div class="school-carousel owl-carousel owl-theme">

            @if(!empty($schools[0]))
            @foreach($schools as $school)
            @if (!in_array($school->school_category, [19, 20, 21, 22]))
            <div class="item">
                <div class="school-card btn-group" >
                    @php
                    $coursesHasVideo = $school->coursesHasVideo;
                    @endphp
                    <a href="javascript:void(0)" title="" class="school-card-link @if($coursesHasVideo->count() > 0) openChangeClassModal @endif" onclick="JoinAClass2.openModel({{$school->school_category}},{{$school->id}})" data-cat="{{$school->school_category}}" data-school="{{$school->id}}">
                        @if($school->is_locked)
                        <div class="school-lock">
                            <img src="/images/lock-icon.png" alt="lock">
                        </div>
                        @endif
                        <figure class="school-logo">
                            <img src="{{asset('/uploads/schools/'.$school->logo)}}" class="img-fluid" height="73" alt="{{ $school->school_name }}">
                        </figure>

                        <h3 class="school-name">{{ $school->school_name }}</h3>
                        @if($school->school_category == config('constants.BASIC_SCHOOL'))
                        @php
                        $course = $coursesHasVideo[0];
                        @endphp
                        <p class="school-course-qty">{{$school->coursesLabel($course->classesHasVideosWithKey->count())}} </p>
                       @else
                        <p class="school-course-qty">{{$school->coursesLabel($coursesHasVideo->count())}}</p>
                       @endif

                    </a>
                </div>
            </div>
            @endif
            @endforeach
            @endif
        </div>
    </div>
</section>
