<script>

let loginClassObj = {
    parentUrl:'{{URL::current()}}',
    jsId : {
        activeTabInput: "#activeTabInput",
        LoginBtn      : "#LoginBtnTop",
        signUpBtnTop  : "#signUpBtnTop",
        loginPopUpDiv : "#loginPopUpDiv",
        returnUrl     : "#returnUrl",
        userName      : "#userName",
        userPassword  : "#userPassword",
        signUpEmail   : "#signUpEmail",
        signUpPassword : "#signUpPassword",
        signUpName     : "#signUpName",
        signUpMobile   : "#signUpMobile",
        signUpUserName  :"#signUpUserName",

    },
	status : {
        success     : 200
    },
    jsClass : {
        navItem  : ".nav-item"
    },
    extra : {
		url : {
				loginUrl  : '{{route("frontend.ajaxLogin")}}',
				signUpUrl : '{{route("frontend.ajaxSignUp")}}'
		}
    }
}

let mobileSignUpClassObj = {
    jsId : {
        userName       : "#userName",
        userPassword   : "#userPassword",
        mobileSignUpBtn: "#mobile-signup",
        emailSignUpBtn : "#email-signup",
        googleSignUpBtn: "#google-signup",
        formText       : '#form-text-signup',
        forgotPassBtn  : '#forgot-password',
        extraElement    : '#extra-element',
        loginDropDownForm: '#loginDropDownForm',
        loginBtn         : "#LoginBtnTop",
        signUpBtn        : '#MobileSignUpBtnTop',
        verifyBtn        : '#MobileVerifyBtnTop',
        mobileInput      : '#mobile',
        otpInput         : '#verify',
        otpCode          : '#otpCode',
        verifyInput      : '#verify',
        mobileSignUpBackBtn: '#MobileSignUpBackBtn',
        loginPopUpDiv    : '#loginPopUpDiv'
    },
    extra : {
        url : {
            mobileSignUpUrl : '{{route("front.mobile.signup")}}',
            mobileVerifyUrl : '{{route("front.mobile.verifyUrl")}}'
        }
    },
}

let emailSignUpClassObj = {
    jsId : {
        loginForm : "#loginForm",
        emailSignUpForm : "#emailSignUpForm",
        userEmail : "#userSignupEmail",
        userPass : "#userSignpPass",
        userFullname : "#userSignupFullname",
        userMobile : "#userSignupMobile",
        signUpBtn : "#signupEmailBtn"
    },
    extra : {
        url : {
            emailSignUpUrl : "{{route("front")}}"
        }
    }
}

/* import {LoginClass} from "/js/login.js";
window.loginObj = new LoginClass(loginClassObj);  */
</script>
<script src="{{ asset('js/login.js') }}"></script>
<script>
$(document).ready(function(){
	$('#userName').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var userPassword = $("#userPassword").val();
		var userName     = $("#userName").val();

		if(keycode == '13'){
			if(userName){
				if(userPassword){
				 loginObj.loginUser();
				}else{
					$("#userPassword").focus();
				}
			}
		}
	});

	$('#userPassword').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var userPassword = $("#userPassword").val();
		var userName     = $("#userName").val();

		if(keycode == '13'){
		  if(userPassword && userName){
				loginObj.loginUser();
			}
		}
	});
	$(".google-signup").on("click",function (){
	    location.href = "{{url('/login/google')}}";
    })
    $(".email-signup").on("click",function (){
	    location.href = "{{ route('register') }}";
    })

    // $(".mobile-signin").on("click",function (){
    //     $("#username_mobile").show();
    //     $("#username_login").hide();
    // })

});
</script>

<div id="loginPopUpDiv" class="dropdown-menu login-dropdown" style="">
    <form id="loginForm" style="">
        <input type="hidden" id="returnUrl" value="">
        <input type="hidden" id="reloadIt" value="">

        <div id="username_login">
            <div class="form-group">
                <input type="text" id="userName" class="form-control" placeholder="Username">
            </div>

            <div class="form-group">
                <input type="password" id="userPassword" class="form-control " placeholder="Password">
            </div>

            <div class="form-group ">
                <button type="button" id="LoginBtnTop" onclick="loginObj.loginUser()" class="btn btn-primary w-100"> Login </button>
            </div>


            <div class="form-group">
                <input type="text" id="mobile" class="form-control" placeholder="Enter mobile number" style="display: none">
            </div>

            <div class="form-group">
                <input type="text" id="verify" class="form-control" placeholder="xxxxxx" style="display: none">
            </div>

            <div class="form-group ">
                <button type="button" id="MobileSignUpBtnTop" onclick="mobileSignUpObj.signupUser()" class="btn btn-primary w-100" style="display: none"> Sign up </button>
            </div>

            <div class="form-group ">
                <button type="button" id="MobileVerifyBtnTop" onclick="mobileSignUpObj.verifyUser()" class="btn btn-primary w-100" style="display: none"> Verify </button>
            </div>

            <div class="d-flex justify-content-center">
                <a href="javascript:void(0)" id="MobileSignUpBackBtn" onclick="mobileSignUpObj.signUpHtmlLayout('destructure')" style="display: none;" class="underline">
                    <small>{{__("<< Go back")}}</small></a>
            </div>

            <hr id="extra-element">
            <div class="form-text " id="forgot-password">Forgot password? <a href="{{ route('password.update') }}">Request</a></div>

        </div>

        <div id="username_mobile" style="display: none;">
            <div class="form-group">
                <input type="text" id="mobile" class="form-control" placeholder="Mobile number">
            </div>

            <div class="form-group ">
                <button type="button" id="LoginBtnTop" onclick="" class="btn btn-primary w-100"> Send Code </button>
            </div>

{{--            <div class="form-text "> <a href=""><small>login with email</small></a></div>--}}

            <hr>

        </div>

        <div class="form-text" id="form-text-signup">New here? <br></div>
        <br>

{{--        <button type="button" class="btn btn-outline-primary w-100 mobile-signup" id="mobile-signup" onclick="mobileSignUpObj.signUpHtmlLayout()">--}}
{{--            <span class="fa fa-phone"></span> &nbsp; &nbsp;Sign Up With Mobile</button>--}}
{{--        <br>--}}
{{--        <br>--}}
        <button type="button" class="btn btn-outline-primary w-100" id="email-signup" onclick="emailSignUpObj.signUpHtmlLayout('restructure')">
            <span class="fa fa-envelope"></span> &nbsp; &nbsp;Sign Up With Email
        </button>
        <br>
        <br>
        <button type="button" class="btn btn-outline-primary w-100 google-signup" id="google-signup">
            <span class="fab fa-google"></span> &nbsp; &nbsp; Sign up with Google</button>

    </form>


    <form id="emailSignUpForm" style="display: none;">
        <input type="hidden" id="returnUrl" value="">
        <input type="hidden" id="reloadIt" value="">

        <div class="usersignup">

            <div class="form-group">
                <input type="text" id="signUpUserName" class="form-control" placeholder="Username">
            </div>

            <div class="form-group">
                <input type="email" id="signUpEmail" class="form-control" placeholder="Email">
            </div>

            <div class="form-group">
                <input type="password" id="signUpPassword" class="form-control " placeholder="Password">
            </div>

            <div class="form-group">
                <input type="text" id="signUpName" class="form-control " placeholder="Full Name">
            </div>

            <div class="form-group">
                <input type="text" id="signUpMobile" class="form-control " placeholder="Mobile No.">
            </div>

            <div class="custom-control custom-checkbox">
                <input class="custom-control-input" type="checkbox" id="tutorCheckbox">
                <label class="custom-control-label" for="tutorCheckbox">
                    <span class="text">Tutor?<span class="text-underline"></span></span>
                </label>
            </div>

            <br>

            <div class="custom-control custom-checkbox">
                <input class="custom-control-input" type="checkbox" value="" id="termsCheckBox">
                <label class="custom-control-label" for="termsCheckBox">
                    <span class="text" id="forgot-password">I agree to <a href="#"><span class="text-underline">terms of use</span></a></span>
                </label>
            </div>

            <div class="form-group ">
                <br>
                <button type="button" id="signUpBtnTop" onclick="loginObj.signUpUser()" class="btn btn-primary w-100"> Signup </button>
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <small class="d-block text-center">Make sure your phone is active a passcode will be sent to it</small>
            </div>
            <br>

            <div class="d-flex justify-content-center">
                <a href="javascript:void(0)" class="underline" onclick="emailSignUpObj.signUpHtmlLayout('destructure')"><small>{{__("<< Go back")}}</small></a>
            </div>
        </div>
    </form>


  </div>

<script>
    class mobileSignUpClass {
        constructor(external) {
            this.ext = external;
        }

        signUpHtmlLayout(type = 'restructure') {

            var ids = this.ext.jsId;

            if(type == 'restructure') {
                $(ids.formText).toggle(300);
                $(ids.userName).toggle(300);
                $(ids.forgotPassBtn).toggle(300);
                $(ids.loginBtn).toggle(300);
                $(ids.userPassword).toggle(300);
                $(ids.mobileSignUpBtn).toggle(300);
                $(ids.signUpBtn).toggle(300);
                $(ids.emailSignUpBtn).toggle(300);
                $(ids.googleSignUpBtn).toggle(300);
                $(ids.extraElement).toggle(300);
                $(ids.mobileInput).toggle(300);
                commonObj.inputDesEnb(ids.mobileInput, 'enb');
                commonObj.btnDesEnb(ids.signUpBtn, 'Sign up', 'enb');
                $(ids.mobileSignUpBackBtn).show(300);
                $('#loginPopUpDiv br').hide(300);
            }

            if(type == 'destructure') {
                $(ids.formText).toggle(300);
                $(ids.userName).toggle(300);
                $(ids.forgotPassBtn).toggle(300);
                $(ids.loginBtn).toggle(300);
                $(ids.userPassword).toggle(300);
                $(ids.mobileSignUpBtn).toggle(300);
                $(ids.signUpBtn).toggle(300);
                $(ids.emailSignUpBtn).toggle(300);
                $(ids.googleSignUpBtn).toggle(300);
                $(ids.extraElement).toggle(300);
                $(ids.mobileInput).toggle(300);
                $(ids.mobileSignUpBackBtn).hide(300);
                $(ids.verifyInput).hide(300);
                $('#loginPopUpDiv br').show(300);
                //$(ids.loginPopUpDiv).removeClass('setHeightLoginPopUpDiv')
            }


        }

        signupUser() {
            var ids = this.ext.jsId;

            var nameVal = $(ids.signUpName).val();
            var mobileVal = $("#mobile").val();
            var url = this.ext.extra.url;
            var postUrl = url.sign;
            var postData = {
                'country_code' : '233',
                'phone' : mobileVal,
            }

            commonObj.inputDesEnb(ids.mobileInput, 'des');
            commonObj.inputShow(ids.verifyInput, 'show');
            commonObj.inputHide(ids.signUpBtn);
            commonObj.inputShow(ids.verifyBtn);

            commonObj.btnDesEnb(ids.signUpBtn, 'Verify', 'des')
            commonObj.btnDesEnb(ids.signUpBtn, 'Verify', 'hide')
            commonObj.btnDesEnb(ids.signUpBtn, 'Verify', 'des')

            var responseData = axios.post(postUrl, postData).
                then(function (response) {

                    //console.log(response);
                    //var getData = response.data;

                    if(getData) {
                        commonObj.inputToggle(ids.verifyInput);
                    }else {
                        commonObj.inputToggle(ids.verifyInput);
                    }
            }).catch(function(error) {
                commonObj.catchErr(error);
            })
        }

        verifyOtp() {
            var ids = this.ext.jsId;

            var mobileVal = $(ids.mobileInput).val();
            var url = this.ext.extra.url;
            var postUrl = url.mobileSignUpUrl;
            var verifyUrl = url.mobileVerifyUrl;
            var postData = {
                'country_code' : '233',
                'phone' : mobileVal,
            }

            commonObj.inputDesEnb(ids.mobileInput, 'des');
            commonObj.inputShow(ids.verifyInput, 'show');
            commonObj.btnDesEnb(ids.signUpBtn, 'Sign up', 'hide');
            // commonObj.btnDesEnb(ids.signUpBtn, 'Verify', 'show')
            // commonObj.btnDesEnb(ids.signUpBtn, 'Verify', 'des')

            var responseData = axios.post(postUrl, postData).
            then(function (response) {

                //console.log(response);
                //var getData = response.data;

                if(getData) {
                    commonObj.inputToggle(ids.verifyInput);
                }else {
                    commonObj.inputToggle(ids.verifyInput);
                }
            }).catch(function(error) {
                commonObj.catchErr(error);
            })
        }

        verifyUser() {
            var ids = this.ext.jsId;
            var mobileVal = $(ids.mobileInput).val();
            var otpVal = $(ids.otpInput).val();
            var url = this.ext.extra.url;
            var postUrl = url.mobileSignUpUrl;
            var verifyUrl = url.mobileVerifyUrl;
            var postData = {
                'country_code' : '233',
                'mobile' : mobileVal,
                'otp'    : otpVal
            }

            var responseData = axios.post(verifyUrl, postData).
            then(function (response) {

                console.log("verify response " + response);
                //var getData = response.data;

                if(getData) {
                    commonObj.inputToggle(ids.verifyInput);
                }else {
                    commonObj.inputToggle(ids.verifyInput);
                }
            }).catch(function(error) {
                commonObj.catchErr(error);
            })
        }
    }


    class emailSignUpClass {
        constructor(external) {
            this.ext = external;
        }

        signupUser() {
            var ids = this.ext.jsId;
            var url	= this.ext.extra.url;
        }

        signUpHtmlLayout(type) {
            var ids = this.ext.jsId;

            if(type == 'destructure') {
                $(ids.emailSignUpForm).hide(300);
                $(ids.loginForm).show(300);
            }
            if(type == 'restructure') {
                $(ids.emailSignUpForm).show(300);
                $(ids.loginForm).hide(300);
            }
        }
    }

    var emailSignUpObj = new emailSignUpClass(emailSignUpClassObj);
    var mobileSignUpObj = new mobileSignUpClass(mobileSignUpClassObj);
</script>
