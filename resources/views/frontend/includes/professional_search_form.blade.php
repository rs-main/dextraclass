<section class="content-top-wrapper bg-white" id="professional-school-search-form" style="display: none;">
    <div class="container">
        <div class="text-white organization-hero-wrapper">
        <div class="row pt-0 pb-0 pt-md-4 pb-md-0">
                <div class="col-md-8 pb-0 pb-md-3">

                    <p id="hero-paragraph">
                        Take a course from our professional and experienced teachers giving you lessons to further your career
                    </p>
                </div>
            </div>
            @include('frontend.includes.join_professional_school_box')
        </div>
    </div>

    <script>
        let industryCategory = $("#industry_categories");
        let industries = $("#industries");
        let professionalCourses = $("#professional_courses");

        function getIndustryCategories(){
            industryCategory.removeAttr("disabled");
            fetchIndustryCategories(industries.val());
        }

        function getCategoryCourses(){
            professionalCourses.removeAttr("disabled");
            fetchCategoryCourses(industryCategory.val());
        }

        function fetchIndustryCategories(industry_id){
            fetch(`/get-industry-categories/${industry_id}`)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    industryCategory.html(data.data)
                });
        }

        function fetchCategoryCourses(category_id){
            fetch(`/get-category-courses/${category_id}`)
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    professionalCourses.html(data.data)
                });

            $(".professional_join_button").removeAttr("disabled")
        }

        function viewCourses(){
            let categoryValue = industryCategory.val();
            fetch(`?category_id=${categoryValue}`)
                // .then(response => response.json())
                .then(data => {
                    console.log(data);
                    $(".professional-carousel").html(data);
                });
        }


    </script>
</section>
