<section class="any-device">
    <div class="container">
        <div class="row">
            <div class="col-md-7 offset-md-5 text-center text-md-right">
                <div class="device-info">
                    <div class="pb-0 pb-md-5" >
                        <h2 class="heading text-center text-md-right">Join from any device</h2>
                        <p class="heading-sub-text text-center text-md-right">PC, smartphone, tablet, iPhone etc.</p>
                    </div>
                    <div class="pt-2 pt-md-4">
                        <a href="{{route('frontend.pages.how_to_access')}}" class="btn-gray-outline">LEARN HOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
