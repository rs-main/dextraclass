<section class="content-top-wrapper bg-white">
    <div class="container">
        <div class="text-white masterclass-hero-wrapper" id="masterclass-hero-wrapper">
        <div class="row pt-0 pb-0 pt-md-4 pb-md-5">
                <div class="col-md-8 col-xl-7 pb-0 pb-md-3">
                    <h1 id="hero-header" class="masterclass-hero-header">Take a Professional Course</h1>
                    <p id="hero-paragraph" class="masterclass-hero-paragraph">
                        from our experienced teachers, lessons endorsed by the Ghana Tourism Authority and offered in alignment with the COTVET standards to further your career.
                    </p>
                </div>
        </div>

            <div class="row">
                <div class="col-md-8 col-xl-7 pb-0 pb-md-3" id="masterclass-hero-join-button"></div>
            </div>
        </div>  
    </div>
</section>

<script>
var slideIndex = 0;
showSlides();

function showSlides() {
    
    var slideImages = ["url(../images/kafuideyCard1.png)", "url(../images/kafuideyCard2.png)"];
    var slideHeaderText = [`<text class="text-black">Kafui Dey's masterclass</text>`, "Kafui Dey's xtra class..."];
    var slideParagraph = [`<text class="text-black">join my masterclass to learn how i get things done <br>and how you can also apply it to how you get <br>things done. you can subscribe to my Lessons or<br> request a 1-on-1 coaching session</text>`, `<text style="text-white">join my masterclass to learn how i get things done<br> and how you can also apply it to how you get<br> things done. you can subscribe to my Lessons or<br> request a 1-on-1 coaching session</text>`]
    var join = [`<button class="btn btn-black-outline waves-effect waves-themed w-50 mr-3 mb-sm-1 mb-lg-0" type="button">Join now on</button>
                    <img src="../images/xtraclassLogoBlack.png">`, `<button class="btn btn-white-outline waves-effect waves-themed w-50 mr-3 mb-sm-1 mb-lg-0" type="button">Join now on</button>
                    <img src="../images/xtraclassLogoWhite.png">`]
    
    slideIndex++;
    

    if (slideIndex > slideImages.length) {
        slideIndex = 0;
    }

        
    $('#masterclass-hero-wrapper').css('background-image' , slideImages[slideIndex -1]);
    
    $('.masterclass-hero-header').html(slideHeaderText[slideIndex - 1]);
    $('.masterclass-hero-paragraph').html(slideParagraph[slideIndex - 1]);
    $('#masterclass-hero-join-button').html(join[slideIndex -1]);

    setTimeout(showSlides, 20000);
}

</script>
