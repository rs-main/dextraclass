<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXtraclassAddManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xtraclass_add_manager', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name')->nullable();
            $table->string('add_location')->nullable();
            $table->string('add_url')->nullable();
            $table->dateTime('expired_on')->nullable();
            $table->string('media_type')->nullable();
            $table->string('add_text')->nullable();
            $table->timestamps();
            $table->char('uuid', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xtraclass_add_manager');
    }
}
