<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_transactions', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id')->nullable();
            $table->integer('user_plan_id')->nullable();
            $table->string('customer_phone', 15)->nullable();
            $table->integer('txn_id')->nullable();
            $table->string('tx_ref')->nullable();
            $table->string('flwRef')->nullable();
            $table->string('amount')->nullable();
            $table->string('charged_amount')->nullable();
            $table->string('appfee')->nullable();
            $table->string('chargeResponseMessage')->nullable();
            $table->string('orderRef')->nullable();
            $table->string('authModelUsed')->nullable();
            $table->string('currency')->nullable();
            $table->string('clientIP')->nullable();
            $table->string('narration')->nullable();
            $table->string('status')->nullable();
            $table->string('modalauditid')->nullable();
            $table->string('acctvalrespmsg')->nullable();
            $table->string('acctvalrespcode')->nullable();
            $table->string('paymentType')->nullable();
            $table->integer('customerId')->nullable();
            $table->integer('accountId')->nullable();
            $table->dateTime('createdAt')->nullable();
            $table->dateTime('updatedAt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_transactions');
    }
}
