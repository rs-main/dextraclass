<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToStudentDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_downloads', function (Blueprint $table) {
            $table->foreign('notes_id', 'Fb_download_note')->references('id')->on('notes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('student_id', 'Fb_download_student')->references('id')->on('students')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_downloads', function (Blueprint $table) {
            $table->dropForeign('Fb_download_note');
            $table->dropForeign('Fb_download_student');
        });
    }
}
