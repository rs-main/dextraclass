<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('video_id')->default(0)->index('Fb_question_video');
            $table->bigInteger('class_id')->default(0);
            $table->bigInteger('subject_id')->nullable()->default(0);
            $table->longText('content');
            $table->string('type', 20)->default('');
            $table->unsignedBigInteger('sender_id')->default(0)->index('Fb_question_sender');
            $table->unsignedInteger('parent_id')->default(0);
            $table->enum('status', ['approved', 'unapproved', 'flagged'])->nullable()->default('approved');
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
