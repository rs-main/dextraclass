<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSectionInfoToPastQuestionsGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('past_questions_group', function (Blueprint $table) {
            $table->boolean("has_section")->default(false);
            $table->integer("section_counts")->default(0);
            $table->string("section_naming_type")->nullable();
            $table->json("section_info")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('past_questions_group', function (Blueprint $table) {
            $table->dropColumn([
                "section_counts",
                "section_naming_type",
                "section_info"
            ]);
        });
    }
}
