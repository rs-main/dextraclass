<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolProgrammesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_programmes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('school_id')->index('school_programmes_school_id_foreign');
            $table->boolean('agriculture')->default(0);
            $table->boolean('business')->default(0);
            $table->boolean('tech')->default(0);
            $table->boolean('visual_arts')->default(0);
            $table->boolean('gen_arts')->default(0);
            $table->boolean('gen_science')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_programmes');
    }
}
