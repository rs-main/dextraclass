<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMarksGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marks_grades', function (Blueprint $table) {
            $table->foreign('academic_id', 'sm_marks_grades_academic_id_foreign')->references('id')->on('sm_academic_years')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('school_id', 'sm_marks_grades_school_id_foreign')->references('id')->on('sm_schools')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marks_grades', function (Blueprint $table) {
            $table->dropForeign('sm_marks_grades_academic_id_foreign');
            $table->dropForeign('sm_marks_grades_school_id_foreign');
        });
    }
}
