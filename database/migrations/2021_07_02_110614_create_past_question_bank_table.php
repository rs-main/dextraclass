<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePastQuestionBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('past_question_bank', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text("question")->nullable();
            $table->integer("num_of_options")->default(0);
            $table->string("type")->nullable();
            $table->boolean("has_image")->default(false);
            $table->bigInteger("past_question_answer_id")->unsigned()->index();
            $table->bigInteger("subject_id")->nullable()->unsigned()->index();
            $table->bigInteger("class_id")->nullable()->unsigned()->index();
            $table->bigInteger("section_id")->nullable()->unsigned()->index();
            $table->bigInteger("past_question_group_id")->nullable()->unsigned()->index();
            $table->boolean("active_status")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('past_question_bank');
    }
}
