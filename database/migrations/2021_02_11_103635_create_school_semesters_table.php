<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolSemestersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_semesters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('school_id')->default(0);
            $table->unsignedInteger('category_id')->nullable();
            $table->string('semester', 100)->nullable();
            $table->date('date_begin')->nullable();
            $table->date('date_end')->nullable();
            $table->unsignedInteger('days_count')->default(0);
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_semesters');
    }
}
