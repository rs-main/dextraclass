<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentFavouritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_favourites', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->unsignedBigInteger('student_id')->default(0)->index('Fb_favourite_student');
            $table->unsignedBigInteger('video_id')->default(0)->index('Fb_favourite_video');
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_favourites');
    }
}
