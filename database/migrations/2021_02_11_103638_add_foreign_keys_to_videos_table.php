<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->foreign('period_id', 'FK_video_period')->references('id')->on('periods')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('subject_id', 'FK_video_subject')->references('id')->on('subjects')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('topic_id', 'FK_video_topic')->references('id')->on('topics')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id', 'FK_video_user')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->dropForeign('FK_video_period');
            $table->dropForeign('FK_video_subject');
            $table->dropForeign('FK_video_topic');
            $table->dropForeign('FK_video_user');
        });
    }
}
