<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_quiz', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('student_id');
            $table->integer('quiz_id')->nullable();
            $table->integer('subject_id')->nullable();
            $table->integer('user_id');
            $table->dateTime('date_taken')->nullable();
//            $table->string('created_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_quiz');
    }
}
