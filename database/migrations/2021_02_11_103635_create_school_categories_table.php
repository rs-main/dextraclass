<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->enum('mode', ['regular', 'open', 'professional', 'regular-open'])->nullable()->default('regular-open');
            $table->integer('status')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_categories');
    }
}
