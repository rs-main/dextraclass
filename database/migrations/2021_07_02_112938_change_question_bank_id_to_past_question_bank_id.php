<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeQuestionBankIdToPastQuestionBankId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('past_questions', function (Blueprint $table) {
            $table->dropForeign("past_questions_past_question_group_id_foreign");
            $table->dropColumn("question_bank_id");
            $table->bigInteger("past_question_bank_id")->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('past_questions', function (Blueprint $table) {
            $table->bigInteger("question_bank_id")->unsigned()->index();
            $table->dropColumn("past_question_bank_id");
        });
    }
}
