<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('school_id')->nullable();
            $table->unsignedBigInteger('course_id')->nullable();
            $table->unsignedBigInteger('class_id')->nullable();
            $table->unsignedBigInteger('classroom_id')->nullable()->index('FK_video_classroom');
            $table->date('play_on')->nullable();
            $table->unsignedBigInteger('period_id')->nullable()->index('FK_video_period');
            $table->string('video_id', 100)->nullable();
            $table->string('video_url')->nullable();
            $table->string('mobile_video_url', 300)->nullable();
            $table->string('video_file', 191)->nullable();
            $table->string('video_type', 20)->nullable()->default('url');
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('subject_id')->nullable()->default(0)->index('FK_video_subject');
            $table->unsignedBigInteger('topic_id')->nullable()->default(0)->index('FK_video_topic');
            $table->unsignedBigInteger('tutor_id')->nullable()->default(0);
            $table->unsignedBigInteger('user_id')->nullable()->index('FK_video_user');
            $table->unsignedBigInteger('article_id')->nullable()->default(0);
            $table->unsignedBigInteger('note_id')->nullable()->default(0);
            $table->text('keywords')->nullable();
            $table->unsignedBigInteger('total_views')->nullable();
            $table->string('vimeo_status', 20)->nullable();
            $table->tinyInteger('status')->nullable()->default(1);
            $table->enum('school_video_type', ['regular_school', 'open_school', 'professional'])->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
