<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid("uuid");
            $table->string("subject_id")->nullable();
            $table->string("school_id")->nullable();
            $table->string("class_id")->nullable();
            $table->unsignedBigInteger("period_id")->nullable();
            $table->string("start_date_string");
            $table->json("data")->nullable();
            $table->dateTimeTz("start_date_time")->nullable();
            $table->dateTimeTz("end_date_time")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_tables');
    }
}
