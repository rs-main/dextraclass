<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubjectIdAndClassIdToPastQuestionGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('past_questions_group', function (Blueprint $table) {
            $table->bigInteger("subject_id")->nullable()->after("year");
            $table->bigInteger("class_id")->nullable()->after("subject_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('past_questions_group', function (Blueprint $table) {
            //
        });
    }
}
