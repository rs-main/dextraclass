<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToActivationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activation_codes', function (Blueprint $table) {
            $table->foreign('school_id', 'Fb_activation_school')->references('id')->on('schools')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('student_id', 'Fb_activation_student')->references('id')->on('students')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activation_codes', function (Blueprint $table) {
            $table->dropForeign('Fb_activation_school');
            $table->dropForeign('Fb_activation_student');
        });
    }
}
