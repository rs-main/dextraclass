<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrackGenderForm1Form2Form3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->string("track")->nullable();
            $table->integer("form_one")->nullable();
            $table->integer("form_two")->nullable();
            $table->integer("form_three")->nullable();
            $table->string("contact_person")->nullable();
            $table->string("contact_person_number")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn(["track","form_one","form_two","form_three","contact_person","contact_person_number"]);
        });
    }
}
