<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("agora_stream_id")->nullable();
            $table->string("channel_id")->nullable();
            $table->bigInteger("tutor_id")->nullable();
            $table->string("subject")->nullable();
            $table->bigInteger("subject_id")->nullable();
            $table->bigInteger("school_id")->nullable();
            $table->boolean("active")->default(false);
            $table->integer("joined_students_total")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streams');
    }
}
