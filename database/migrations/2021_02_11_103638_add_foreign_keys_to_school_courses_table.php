<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSchoolCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('school_courses', function (Blueprint $table) {
            $table->foreign('course_id', 'FK_school_courses_course_id')->references('id')->on('courses')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('school_id', 'FK_school_courses_school_id')->references('id')->on('schools')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('school_courses', function (Blueprint $table) {
            $table->dropForeign('FK_school_courses_course_id');
            $table->dropForeign('FK_school_courses_school_id');
        });
    }
}
