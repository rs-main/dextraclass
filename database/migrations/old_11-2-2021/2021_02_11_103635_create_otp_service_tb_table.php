<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtpServiceTbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otp_service_tb', function (Blueprint $table) {
            $table->string('generated_uid')->index('generated_uid');
            $table->longText('code')->nullable();
            $table->longText('verificationSid')->nullable();
            $table->longText('status')->nullable();
            $table->date('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otp_service_tb');
    }
}
