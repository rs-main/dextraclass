<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classrooms', function (Blueprint $table) {
            $table->foreign('class_id', 'FK_classroom_class')->references('id')->on('classes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('course_id', 'FK_classroom_course')->references('id')->on('courses')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('school_id', 'FK_classroom_school')->references('id')->on('schools')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classrooms', function (Blueprint $table) {
            $table->dropForeign('FK_classroom_class');
            $table->dropForeign('FK_classroom_course');
            $table->dropForeign('FK_classroom_school');
        });
    }
}
