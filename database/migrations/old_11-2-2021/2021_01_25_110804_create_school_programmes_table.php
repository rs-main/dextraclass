<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolProgrammesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_programmes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("school_id")->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

            $table->boolean("agriculture")->default(false);
            $table->boolean("business")->default(false);
            $table->boolean("tech")->default(false);
            $table->boolean("visual_arts")->default(false);
            $table->boolean("gen_arts")->default(false);
            $table->boolean("gen_science")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_programmes');
    }
}
