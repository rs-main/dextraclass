<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_managers', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedBigInteger('user_id')->index('user_id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->string('mobile', 20)->nullable();
            $table->unsignedInteger('country')->nullable()->default(0);
            $table->unsignedBigInteger('school_id')->default(0);
            $table->string('profile_image')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('restrict_to_student')->default(0);
            $table->integer('student_limit')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_managers');
    }
}
