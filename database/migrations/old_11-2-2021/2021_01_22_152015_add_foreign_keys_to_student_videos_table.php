<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToStudentVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_videos', function (Blueprint $table) {
            $table->foreign('video_id', 'Fb_student_videos_video')->references('id')->on('videos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_videos', function (Blueprint $table) {
            $table->dropForeign('Fb_student_videos_video');
        });
    }
}
