<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivationCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activation_codes', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->integer('country_code')->nullable();
            $table->string('mobile', 20)->nullable();
            $table->unsignedBigInteger('student_id')->default(0)->index('Fb_activation_student');
            $table->unsignedBigInteger('school_id')->default(0)->index('Fb_activation_school');
            $table->integer('activation_code')->nullable();
            $table->tinyInteger('status')->nullable()->default(1);
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->timestamp('updated_at')->nullable()->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activation_codes');
    }
}
