<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username', 191);
            $table->string('name', 191)->nullable();
            $table->string('email', 191)->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 191);
            $table->tinyInteger('status')->nullable()->default(1);
            $table->rememberToken();
            $table->string('token', 100)->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('device_token', 100)->nullable();
            $table->string('device_type', 20)->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
