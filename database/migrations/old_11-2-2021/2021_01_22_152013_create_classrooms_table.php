<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('classroom_name')->default('');
            $table->unsignedBigInteger('school_id')->nullable()->index('FK_classroom_school');
            $table->unsignedBigInteger('course_id')->index('FK_classroom_course');
            $table->unsignedBigInteger('class_id')->default(0)->index('FK_classroom_class');
            $table->date('date')->nullable();
            $table->tinyInteger('status')->nullable()->default(1);
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classrooms');
    }
}
