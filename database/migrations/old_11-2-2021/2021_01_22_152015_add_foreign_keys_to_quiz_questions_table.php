<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToQuizQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_questions', function (Blueprint $table) {
            $table->foreign('question_bank_id', 'fk_question_BkId')->references('id')->on('questions_bank')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('q_quiz_id', 'fk_question_qId')->references('id')->on('questions_quiz')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_questions', function (Blueprint $table) {
            $table->dropForeign('fk_question_BkId');
            $table->dropForeign('fk_question_qId');
        });
    }
}
