<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index('Fb_tutor_user');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->string('mobile', 20)->nullable();
            $table->unsignedInteger('country')->nullable()->default(0);
            $table->string('tutor_subject', 150)->nullable();
            $table->string('type', 50)->nullable();
            $table->unsignedBigInteger('school_id')->default(0);
            $table->string('profile_image')->nullable();
            $table->unsignedBigInteger('avatar_id')->nullable()->default(0)->index('Fb_tutor_avatar');
            $table->tinyInteger('upload_access')->nullable()->default(0);
            $table->tinyInteger('status')->nullable()->default(1);
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutors');
    }
}
