<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarksGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grade_name', 191)->nullable();
            $table->string('gpa', 191)->nullable();
            $table->double('from', 8, 2)->nullable();
            $table->double('up', 8, 2)->nullable();
            $table->integer('percent_from')->nullable();
            $table->integer('percent_upto')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();
            $table->unsignedInteger('created_by')->nullable()->default(1);
            $table->unsignedInteger('updated_by')->nullable()->default(1);
            $table->unsignedInteger('school_id')->nullable()->default(1)->index('sm_marks_grades_school_id_foreign');
            $table->unsignedInteger('academic_id')->nullable()->default(1)->index('sm_marks_grades_academic_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks_grades');
    }
}
