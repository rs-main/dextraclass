<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnowledgeArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('content');
            $table->string('subject', 200)->nullable()->index('Fb_article_subject');
            $table->unsignedBigInteger('topic_id')->default(0)->index('Fb_article_topic');
            $table->string('target')->nullable();
            $table->string('image')->nullable();
            $table->unsignedBigInteger('author_id')->default(0)->index('Fb_article_author');
            $table->text('keywords')->nullable();
            $table->boolean('status')->nullable()->default(1);
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledge_articles');
    }
}
