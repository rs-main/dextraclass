<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToVideoTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_tags', function (Blueprint $table) {
            $table->foreign('topic_id', 'Fb_video_tags_topic')->references('id')->on('topics')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('video_id', 'Fb_video_tags_video')->references('id')->on('videos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_tags', function (Blueprint $table) {
            $table->dropForeign('Fb_video_tags_topic');
            $table->dropForeign('Fb_video_tags_video');
        });
    }
}
