<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name', 30)->nullable();
            $table->string('duration', 30)->nullable();
            $table->string('amount')->nullable();
            $table->string('description', 30)->nullable();
            $table->string('status', 10)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->char('uuid', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
