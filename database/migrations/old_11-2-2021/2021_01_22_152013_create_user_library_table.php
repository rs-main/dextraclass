<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_library', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id')->nullable();
            $table->string('item_name')->nullable();
            $table->integer('class_id')->nullable();
            $table->integer('tutor_id')->nullable();
            $table->integer('library_id')->nullable();
            $table->string('description')->nullable();
            $table->integer('note_id')->nullable();
            $table->integer('is_open')->nullable()->default(0);
            $table->dateTime('library_item_created')->nullable();
            $table->timestamps();
            $table->char('uuid', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_library');
    }
}
