<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id')->nullable();
            $table->integer('class_id')->nullable();
            $table->integer('subject_id')->nullable();
            $table->longText('library_item')->nullable();
            $table->string('description')->nullable();
            $table->string('file_type')->nullable();
            $table->string('status')->nullable();
            $table->bigInteger('total_download')->nullable();
            $table->bigInteger('view_count')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->char('uuid', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library');
    }
}
