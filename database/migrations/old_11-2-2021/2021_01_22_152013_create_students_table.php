<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index('Fb_student_user');
            $table->string('username');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('mobile_payment', 20)->nullable();
            $table->unsignedInteger('country')->default(0)->index('Fb_student_country');
            $table->unsignedInteger('school_category')->default(0)->index('Fb_student_school_category');
            $table->string('school_type', 15)->nullable();
            $table->unsignedBigInteger('school_id')->default(0)->index('Fb_student_school');
            $table->unsignedBigInteger('course_id')->default(0)->index('Fb_student_course');
            $table->unsignedBigInteger('class_id')->default(0)->index('Fb_student_class');
            $table->string('profile_image')->nullable();
            $table->unsignedBigInteger('avatar_id')->nullable()->index('Fb_student_avator');
            $table->tinyInteger('activated')->default(0);
            $table->tinyInteger('is_firstTime')->nullable()->default(0);
            $table->tinyInteger('request_access')->nullable()->default(0);
            $table->tinyInteger('subscribed')->default(0);
            $table->tinyInteger('status')->nullable()->default(1);
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
