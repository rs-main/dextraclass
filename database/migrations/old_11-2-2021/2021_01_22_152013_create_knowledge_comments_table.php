<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnowledgeCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('content');
            $table->unsignedBigInteger('article_id')->nullable()->index('Fb_comment_article');
            $table->unsignedBigInteger('sender_id')->nullable()->index('Fb_comment');
            $table->string('type', 30)->nullable();
            $table->enum('status', ['approved', 'unapproved', 'flagged'])->nullable()->default('approved');
            $table->timestamps();
            $table->char('uuid', 36);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledge_comments');
    }
}
