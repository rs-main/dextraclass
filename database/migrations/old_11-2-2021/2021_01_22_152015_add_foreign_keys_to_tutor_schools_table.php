<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTutorSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_schools', function (Blueprint $table) {
            $table->foreign('school_id', 'Fb_tutor_schools_school')->references('id')->on('schools')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('tutor_id', 'Fb_tutor_schools_tutor')->references('id')->on('tutors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_schools', function (Blueprint $table) {
            $table->dropForeign('Fb_tutor_schools_school');
            $table->dropForeign('Fb_tutor_schools_tutor');
        });
    }
}
