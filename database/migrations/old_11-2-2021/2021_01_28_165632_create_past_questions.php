<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePastQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('past_questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("question_bank_id")->unsigned();
//            $table->foreign('question_bank_id')->references('id')->on('questions_bank')->onDelete("cascade");
            $table->bigInteger("past_question_group_id")->unsigned();
            $table->foreign('past_question_group_id')->references('id')->on('past_questions_group')->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('past_questions');
    }
}
