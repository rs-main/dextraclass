<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToQuestionsQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questions_quiz', function (Blueprint $table) {
            $table->foreign('q_group_id', 'fk_q_group_bkId')->references('id')->on('question_groups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions_quiz', function (Blueprint $table) {
            $table->dropForeign('fk_q_group_bkId');
        });
    }
}
