<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGhanapostAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ghanapost_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('CenterLatitude', 18, 15)->nullable();
            $table->decimal('CenterLongitude', 18, 15)->nullable();
            $table->string('GPSName', 100)->nullable();
            $table->string('PostCode', 100)->nullable();
            $table->string('District', 100)->nullable();
            $table->string('Country', 100)->nullable();
            $table->string('Area', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ghanapost_addresses');
    }
}
