<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->default(0)->index('Fb_user_subs_user');
            $table->string('subs_type', 100)->nullable();
            $table->dateTime('starts_on')->nullable();
            $table->unsignedInteger('plan_id')->nullable();
            $table->integer('transaction_id')->nullable()->default(0);
            $table->integer('transaction_cust_id')->nullable()->default(0);
            $table->integer('transaction_acc_id')->nullable()->default(0);
            $table->string('activate_mode_type', 20)->nullable();
            $table->string('payment_status')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->dateTime('expired_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_plans');
    }
}
