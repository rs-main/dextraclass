<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProfessionalCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_school_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("industry_id")->unsigned();
            $table->foreign('industry_id')->references('id')->on('industries')->onDelete('cascade');
            $table->string("name")->nullable();
            $table->text("description")->nullable();
            $table->boolean("status")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_school_categories');
    }

}
