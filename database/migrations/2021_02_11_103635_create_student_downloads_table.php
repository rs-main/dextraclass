<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_downloads', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->unsignedBigInteger('student_id')->default(0)->index('Fb_download_student');
            $table->unsignedBigInteger('notes_id')->default(0)->index('Fb_download_note');
            $table->bigInteger('count')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_downloads');
    }
}
