<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProfessionalVideosTopic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_videos_topic', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("video_id")->index();
            $table->foreign("video_id")->references("id")->on("videos")->onDelete("cascade");
            $table->unsignedBigInteger("topic_id")->index();
            $table->foreign("topic_id")->references("id")->on("topics")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_videos_topic');
    }
}
