<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToKnowledgeCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('knowledge_comments', function (Blueprint $table) {
            $table->foreign('sender_id', 'Fb_comment')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('article_id', 'Fb_comment_article')->references('id')->on('knowledge_articles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('knowledge_comments', function (Blueprint $table) {
            $table->dropForeign('Fb_comment');
            $table->dropForeign('Fb_comment_article');
        });
    }
}
