<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_quiz', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title')->nullable();
            $table->string('pass_mark')->nullable();
            $table->string('type')->nullable();
            $table->integer('q_group_id')->nullable()->index('fk_q_group_bkId');
            $table->integer('subject_id')->nullable();
            $table->integer('lesson_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('active')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions_quiz');
    }
}
