<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('school_name', 191);
            $table->string('short_name', 100)->nullable();
            $table->unsignedInteger('school_category')->default(0)->index('FK_school_category');
            $table->longText('description')->nullable();
            $table->string('logo')->nullable()->default('noimage.jpg');
            $table->tinyInteger('is_locked')->nullable()->default(0);
            $table->tinyInteger('featured')->nullable()->default(0);
            $table->string('theme', 50)->nullable()->default('corporate');
            $table->integer('semester_count')->default(3);
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('restrict_to_student')->default(0);
            $table->integer('student_limit')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->char('uuid', 36);
            $table->unsignedBigInteger('location_id')->nullable()->index('schools_location_id_foreign');
            $table->enum('gender', ['girls', 'boys', 'mixed'])->nullable();
            $table->enum('boarding_status', ['day', 'boarding', 'day-boarding', 'day-hostel'])->nullable();
            $table->string('school_code')->nullable();
            $table->enum('ownership', ['public', 'private'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
