<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToKnowledgeArticleTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('knowledge_article_tags', function (Blueprint $table) {
            $table->foreign('article_id', 'FK_article_tags_article')->references('id')->on('knowledge_articles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('topic_id', 'FK_article_tags_topic')->references('id')->on('topics')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('knowledge_article_tags', function (Blueprint $table) {
            $table->dropForeign('FK_article_tags_article');
            $table->dropForeign('FK_article_tags_topic');
        });
    }
}
