<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToStudentFavouritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_favourites', function (Blueprint $table) {
            $table->foreign('video_id', 'Fb_favourite_video')->references('id')->on('videos')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_favourites', function (Blueprint $table) {
            $table->dropForeign('Fb_favourite_video');
        });
    }
}
