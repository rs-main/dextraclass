<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_bank', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('question')->nullable();
            $table->string('marks')->nullable();
            $table->string('suitable_word')->nullable();
            $table->string('num_of_options')->nullable();
            $table->integer('choice_id')->nullable();
            $table->integer('active_status')->nullable();
            $table->timestamps();
            $table->integer('subject_id')->nullable();
            $table->string('school_type', 15)->nullable();
            $table->integer('q_group_id')->nullable()->index('fk_questionGpId');
            $table->integer('class_id')->nullable();
            $table->integer('section_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('school_id')->nullable();
            $table->integer('academic_id')->nullable();
            $table->string('type')->nullable();
            $table->string('question_type')->nullable();
            $table->boolean('has_image')->nullable()->default(0);
            $table->longText('image')->nullable();
            $table->string('year')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions_bank');
    }
}
