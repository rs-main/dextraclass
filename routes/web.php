<?php




/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

 Route::get('sitemap', function () {
     return view('sitemap');
 });

 Route::get("/socket",function (){
     return view("index");
 });

 Route::get("/initialise-calendar/{id}", function(\Illuminate\Http\Request $request, $id) {
     $class_id = $id;

     $periods = \App\Models\Period::all();
     $new_periods =[];
     $pm_time_array = ["12:00" => "12:00","01:00" => "13:00","02:00" => "14:00",
                        "03:00" => "15:00","04:00" => "16:00","05:00"=> "17:00"];
     foreach ($periods as  $key => $period){
         $new_periods[$key] = explode(" - ",$period->title);
         if (isset($new_periods[$key][1])) {
             $start_time = ($new_periods[$key][0]);
             $end_time =   ($new_periods[$key][1]);
             \App\Models\Period::find($period->id)->update([
                 "start_time" =>  in_array(explode(" ",$start_time)[0],array_keys($pm_time_array)) ?
                     $pm_time_array[explode(" ",$start_time)[0]] : $start_time,
                 "end_time"  =>  in_array(explode(" ",$end_time)[0],array_keys($pm_time_array)) ?
                     $pm_time_array[explode(" ",$end_time)[0]] : $end_time
             ]);
         }

//             $nnn[$key] =  in_array(explode(" ",$start_time)[0],array_keys($pm_time_array)) ?
//             $pm_time_array[explode(" ",$start_time)[0]] : $start_time;
     }

     return response()->json(["data" => ""]);

//     foreach ($periods as $period){
//
//         $start_time = $period->start_time;
//         $end_time   = $period->end_time;
//
//         foreach (\App\Models\Video::wherePeriodId($period->id)->get() as $video) {
//
//             foreach (\App\Models\SchoolSemester::whereSchoolId($video->school_id)->get() as $semester) {
//
//                 $data = [];
//                 if (!empty($id)) {
//                     $subject = Subject::find($video->subject_id);
//                     $data["title"] = $subject ? $subject->subject_name : "";
//                     $data["start"] = $start_time;
//                     $data["end"] = $end_time;
//                     $data["color"] = "15ca92";
//                 }
//                 $timeTable = new TimeTable();
//                 $timeTable->class_id = $class_id;
//                 $timeTable->subject_id = $video->subject_id;
//                 $timeTable->school_id = $video->school_id;
//                 $timeTable->start_date_string = $start_time;
//                 $timeTable->data = json_encode($data);
//                 $timeTable->start_date_time = $start_time;
//                 $timeTable->end_date_time = $end_time;
//                 $timeTable->save();
//             }
//         }
//     }
 });

Route::get('/videotest', 'Frontend\ClassroomController@videotest')->name('videotest');
Route::get('/emailtest', function() {
    return view('emails.sendContactInquiry');
});
Route::get('/tab_page', function() {
    return view('frontend.pages.tab_page');
});

 Route::get("/variable",function(){
     $string = file_get_contents("https://ghanapostgps.com/map/");
     $position = strpos($string,"AsaaseOwner",20);
     $subString = substr($string,$position);
     $asaase_owner = substr($subString, 15,16);

     return response()->json(["AsaaseOwner" => $asaase_owner]);
 });

 Route::get("ghana-post-code",function (){
    return \App\GhanaPostGps::getSearchableLocationById("GM0960360");
 });

Route::get('/videotest', 'Frontend\ClassroomController@videotest')->name('videotest');
Route::get('/emailtest', function() {
    return view('emails.sendContactInquiry');
});
Route::get('/tab_page', function() {
    return view('frontend.pages.tab_page');
});

Route::get("locations","API\LocationController@getLocation");


Auth::routes();

include 'Auth/auth.php';

//Route::get('/',     'OpenHomeController@index')->name('front.home');
Route::get('/',     'HomeController@index')->name('front.home');
Route::get('/open', 'OpenHomeController@index')->name('front');
//Route::get('/open', 'OpenHomeController@index')->name('open.school.home');

Route::get('login/{provider}', 'SocialController@redirect');

Route::get('login/{provider}/callback','SocialController@Callback');

// Route::get('/', 'HomeController@index')->name('front');
Route::get('/school', 'HomeController@index')->name('school');
Route::post('/home', 'HomeController@index')->name('home');

Route::post("/api/mobile-login","API\UserController@postMobileLogin")->name('front.mobile.signup');

Route::post("/api/verify-login-otp","API\UserController@postVerifyOtp");

include 'Frontend/frontend.php';

include 'Backend/admin_backend.php';

//------------------ Axios Routes Starts--------------------------
Route::group(['prefix' => 'axios', 'namespace' => 'axios'], function () {
    Route::any('index/{page}', 'AxiosController@index')->where('page', ".*");
});
//------------------ Axios Routes Ends----------------------------

Route::get("/player",function(){
    return view("frontend.classroom.partials.player");
});

Route::get("/mini-player",function(){
    return view("mini_player");
});

Route::get("/main-player",function(){
    return view("main_player");
});

/* Function for print query log */
if (!function_exists('qLog')) {
    \Illuminate\Support\Facades\DB::enableQueryLog();
    function qLog()
    {
        pr(\Illuminate\Support\Facades\DB::getQueryLog());
    }

}

Route::any('/tus/{any?}', function () {
    $response = app('tus-server')->serve();

    return $response->send();
})->where('any', '.*');



Route::get("/websocket-test",function() {

$client = stream_socket_client("tcp://127.0.0.1:6379", $errno, $errmsg, 1);
$data = array("uid"=>"uid1", "percent"=>"88%");
fwrite($client, json_encode($data)."\n");
echo fread($client, 8192);
});

Route::get("/welcome",function (){
    return view("welcome");
});

Route::get("/test-socket",function(){
    return view("index");
});

Route::get("/stream/on-publish",function (){
    return response()->json(["message" => "success"],200);
});

Route::get("/test-socketio",function (){
    return view("socketio");
});

//Route::get("redis",function (){
//    $client = new Predis\Client('tcp://18.184.172.239:6380?database=0&password=password123');
//    $client->set("full_names","Frederick Ankamah");
//    return $client->get("full_names");
//});

Route::get('publish-redis', function () {
    $client = new Predis\Client('tcp://18.184.172.239:6380?database=0&password=password123');
    $client->publish("app:notifications", json_encode(['foo' => 'bar']));
//    $client->sub
});

