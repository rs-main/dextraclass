<?php

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {

    /* Static pages route start  */
    Route::get('/about', 'PageController@getAbout')->name('pages.about');

    Route::get('/plan', 'PlanController@allPlans')->name('plan');

    Route::post('/payment', 'PaymentActionController@actionPay')->name('action.pay');
    Route::get('/payment', 'PaymentActionController@paymentProcess')->name('payment.process');

    Route::get('/contact', 'PageController@getContact')->name('pages.contact');
    Route::post('/contact', 'PageController@sendContact')->name('pages.sendContact');
    Route::get('/terms-and-condition', 'PageController@getPrivacy')->name('pages.terms_condition');
    Route::get('/how-to-access', 'PageController@getHowToAccess')->name('pages.how_to_access');
    Route::get('/help', 'PageController@getHelp')->name('pages.help');

    /* Knowledge Article route start  */
    Route::get('/articles', 'KnowledgeArticleController@index')->name('knowledge.articles.index');

    /* Ajax Login Route */
    Route::post('/ajax-login', 'AjaxLoginController@login')->name('ajaxLogin');

    Route::post('/ajax-sign-up', 'AjaxController@signUpNewUser')->name('ajaxSignUp');

    Route::get('/knowledge-articles', 'StudentController@getArticles')->name('articles');

    /* Student route start  */

    /* Student Profile Route */
    Route::get('/profile', 'StudentController@profile')->name('profile');
    /* Student history Route */
    Route::post('/student-history', 'StudentController@studentHistory')->name('studentHistory');
    /* Student remove history Route */
    Route::post('/remove-student-history', 'StudentController@removeHtudentHistory')->name('removeHtudentHistory');

    Route::post('/closeSession', 'StudentController@closeSession')->name('closeSession');

    /* Student favourites Route */
    Route::post('/student-favourites', 'StudentController@studentFavourites')->name('studentFavourites');

    /*Student bag route */
    Route::post('/student-bag', 'StudentController@studentBag')->name('studentBag');

    /* filter user bag */
    Route::get('/student-bag-filter', 'StudentController@filterUserLibrary')->name('filterUserLibrary');
    Route::get('/student-bag-recent', 'StudentController@filterUserRecent')->name('filterUserRecent');


    /* Student profile picture update route */
    Route::post('/upload-urofile', 'StudentController@uploadProfile')->name('uploadProfile');
    Route::post('/change-avatar', 'StudentController@changeAvatar')->name('changeAvatar');

    /* Student route end  */

    /* Tutor route start  */
    Route::get('/tutor-lecture', 'StudentController@tutorLecture')->name('tutorLecture');
    Route::get('/tutor-posts', 'StudentController@tutorPosts')->name('tutorPosts');
    Route::post('/upload-notes', 'StudentController@uploadNotes')->name('uploadNotes');
    Route::get('/video/upload-file/{uuid}', 'StudentController@uploadVideoFile')->name('uploadVideoFile');
    /* Tutor route end  */

    /* Schools route start  */
    Route::get('/search', 'SchoolController@searchResult')->name('search');

    Route::post('/search', 'SchoolController@search')->name('schools');
    Route::post('/school-data.json', 'SchoolController@schoolData')->name('schoolData');
    Route::post('/course-data.json', 'SchoolController@courseData')->name('courseData');
    Route::post('/classes-data.json', 'SchoolController@classesData')->name('classesData');
    /* Schools route end  */

    /* Classroom route start  */
    Route::get('/classroom/{id}', 'ClassroomController@index')->name('classroom');
    Route::get('/playing-data.json', 'ClassroomController@playingData')->name('playingData');
    Route::get('/questions-data.json', 'ClassroomController@questionsData')->name('questionsData');
    Route::get('/archive-data.json', 'ClassroomController@archiveData')->name('archiveData');
    Route::get('/favourites-data.json', 'ClassroomController@favouritesData')->name('favouritesData');
    Route::get('/library-data.json', 'ClassroomController@libraryData')->name('libraryData');
    Route::get('/insert-play-video-status', 'ClassroomController@playVideo')->name('playVideo');
    Route::get('/post-questions', 'ClassroomController@postQuestions')->name('postQuestions');
    Route::get('/set-favourites', 'ClassroomController@setFavourites')->name('setFavourites');
    Route::get('/fleg-video', 'ClassroomController@flegVideo')->name('flegVideo');
    Route::get('/student-downloads', 'ClassroomController@studentDownloads')->name('studentDownloads');
    Route::get('/archive-search', 'ClassroomController@archiveSearch')->name('archiveSearch');
    Route::get('/get-semester-options.json', 'ClassroomController@getSemesterOptions')->name('getSemesterOptions');
    Route::get('/get-semester-daterange.json', 'ClassroomController@getSemesterDaterange')->name('getSemesterDaterange');
    Route::get('/student-add-library', 'ClassroomController@studentAddLibrary')->name('studentAddLibrary');
    Route::get('/student-remove-library', 'ClassroomController@removeFromUserLibrary')->name('removeFromUserLibrary');
    Route::get('/student-library-type', 'ClassroomController@libraryFilterData')->name('libraryFilterData');
    Route::get('/student-library-recent', 'ClassroomController@libraryFilterRecent')->name('libraryFilterRecent');
    Route::get('/student-take-quiz', 'ClassroomController@takeQuiz')->name('takeQuiz');
    Route::post('/student-submit-quiz', 'ClassroomController@submitQuiz')->name('classroom.submitQuiz');

    Route::get('/past-questions-start/{subject_id}', 'ClassroomController@pastQuestionsStart')->name('classroom.pastQuestionsStart');

    Route::get('/past-questions/{subject_id}', 'ClassroomController@pastQuestions')->name('classroom.pastQuestions');
    Route::get('/past-questions-results/', 'ClassroomController@pastQuestionsTestResults')->name('classroom.pastQuestionsTestResults');

    /* Classroom route end  */

    /*
     * Frontend Routes
     * Namespaces indicate folder structure
     */
    Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {

        /* Api route start  */
        Route::post('/change-password-api', 'ApiController@changePasswordApi')->name('changePasswordApi');
        Route::post('/update-tutor', 'ApiController@updateTutor')->name('updateTutor');
        Route::post('/update-student', 'ApiController@updateStudent')->name('updateStudent');
        Route::post('/get-institution-optinns.json', 'ApiController@getInstitutionOptinns')->name('getInstitutionOptinns');
        Route::post('/get-industry-options.json', 'ApiController@getIndustryOptions')->name('getIndustryOptions');
        Route::post('/get-category-options.json', 'ApiController@getCategoryOptions')->name('getCategoryOptions');

        // get Industry Categories
        Route::get('/get-industry-categories/{industry_id}', 'ApiController@getIndustryCategories')->name('getIndustryCategories');

        // get Category Courses
        Route::get('/get-category-courses/{category_id}', 'ApiController@getCategoryCourses')->name('getCategoryCourses');

        Route::post('/get-school-optinns.json', 'ApiController@getSchoolOptinns')->name('getSchoolOptinns');

        Route::post('/get-course-or-department-optinns.json', 'ApiController@getDepartmentOrCourseOptions')->name('getDepartmentOrCourseOptions');
        Route::post('/get-class-optinns.json', 'ApiController@getClassOptions')->name('getClassOptions');
        Route::get('/get-topic-optinns.json', 'ApiController@getTopicOptions')->name('getTopicOptions');
        Route::get('/get-period-optinns.json', 'ApiController@getPeriodOptions')->name('getPeriodOptions');
        Route::get('/get-program-full-optinns.json', 'ApiController@getProgramFullOptions')->name('getProgramFullOptions');
        Route::get('/get-classes-full-optinns.json', 'ApiController@getClassesFullOptions')->name('getClassesFullOptions');
        Route::get('/get-subjects-full-optinns.json', 'ApiController@getSubjectsFullOptions')->name('getSubjectsFullOptions');
        Route::post('/create-video', 'ApiController@createVideo')->name('createVideo');
        Route::post('/send-notice-students', 'ApiController@SendNoticeToStudents')->name('sendNoticeToStudents');

        // Stream routes
        Route::post('/stream-video', 'StreamController@startStream')->name('createStream');

        Route::post('/create-article', 'ApiController@createArticle')->name('createArticle');
        /* Api route end  */

        Route::get('/update-uuid/{table}', 'ApiController@updateUUID')->name('updateUUID');
    });

    /*
     * Frontend Ajax Routes
     * prefix indicate the url common profix
     */
    Route::group(['prefix' => 'admin', 'as' => 'ajax.'], function () {
        Route::post('/send-otp', 'AjaxController@sendOtp')->name('sendOtp');
        Route::post('/verify-otp', 'AjaxController@verifyOtp')->name('verifyOtp');
        Route::post('/searchschool', 'AjaxController@searchschool')->name('searchschool');
        Route::post('/schoolcourses', 'AjaxController@schoolcourses')->name('schoolcourses');
        Route::post('/schoolclasses', 'AjaxController@schoolclasses')->name('schoolclasses');
    });

    //landing school routes
    Route::get('/school/{id}', 'SchoolController@landingSchoolData')->name('landingschool');

    //landing organization routes
    Route::get('/organization', 'OrganizationController@index')->name('landingorganization');

    //landing masterclass routes
    Route::get('/masterclass/{id}', 'MasterclassController@index')->name('landingmasterclass');


    //landing cotvet routes
    Route::get('/cotvet', 'CotvetController@index')->name('landingcotvetclass');
});

Route::post("mobile-login","Auth\LoginController@postMobileLogin");

Route::get("host-stream","StreamController@hostStream")->name("host-stream");

Route::get("stream-audience","StreamController@streamAudience")->name("stream-audience");

Route::post("start-stream","StreamController@startStream");

Route::post("join-stream","StreamController@joinStream");

Route::post("stop-stream","StreamController@stopStream");
