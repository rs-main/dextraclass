<?php

Route::group(["middleware"=>'auth:api', "prefix" => "past-question"],function () {

    Route::get("/years/{subject_id}","API\PastQuestionController@getYears");

    Route::get("/questions/{group_id}","API\PastQuestionController@getPastQuestions");

});
