<?php
/*
 * Backend Routes
 * Namespaces indicate folder structure
 */

Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'ajax.'], function () {

    Route::post('/category/schools/{std_filter?}', 'AjaxController@getSchools')->name('category.schools');
    Route::post('/school/questions/{qxn_filter?}', 'AjaxController@getQeustions')->name('school.questions');
    Route::post('/school/subject/{qxn_filter?}', 'AjaxController@getSchoolCourses')->name('school.subject');
    Route::post('/school-departments', 'AjaxController@getSchoolDepartments')->name('school.departments');
    Route::post('/school-courses', 'AjaxController@getSchoolCourses')->name('school.courses');
    Route::post('/stdfilter-courses', 'AjaxController@getStudentfilterCourses')->name('school.stdfiltercourses');
    Route::post('/department-courses', 'AjaxController@getDepartmentCourses')->name('department.courses');
    Route::post('/school-courseclasses', 'AjaxController@getSchoolCourseclasses')->name('school.courseclasses');
    Route::post('/stdfilter-courseclasses', 'AjaxController@getStudentfilterCourseclasses')->name('school.stdfiltercourseclasses');
    Route::post('/class-subject', 'AjaxController@getClassSubjects')->name('class.subject');
    Route::post('/class-periods', 'AjaxController@getClassPeriods')->name('class.period');
    Route::get('/class-period-options/{class_id}', 'AjaxController@getClassPeriodOptions')->name('class.period.options');
    Route::post('/subject-topics', 'AjaxController@getSubjectTopics')->name('subject.topics');
    Route::post('/school-classsubjects', 'AjaxController@getSchoolClassSubjects')->name('school.classsubjects');
    Route::post('/school-filterclasssubjects', 'AjaxController@getSchoolfilterClassSubjects')->name('school.filterclasssubjects');
    Route::post('/school-tutors', 'AjaxController@getSchoolTutors')->name('school.tutors');
    Route::post('/upload-video', 'AjaxController@dropzoneStore')->name('dropzone.upload.video');
    Route::post('/upload-note', 'AjaxController@dropzoneNoteStore')->name('dropzone.upload.note');
    Route::get('/student-plan', 'AjaxController@studentPlan')->name('student.plan');
});

Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'backend.', 'middleware' => ['admin', 'preventBackHistory']], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    //Route::resource('schools', 'SchoolController')->name('schools');
    //schools routes
    Route::get('/schools', 'SchoolController@index')->name('schools');
    Route::get('/schools-data', 'SchoolController@getSchoolsData')->name('schools.data');
    Route::get('/school/create', 'SchoolController@create')->name('school.create');
    Route::post('/school/store', 'SchoolController@store')->name('school.store');
    Route::get('school/edit/{id}', 'SchoolController@edit')->name('school.edit');

    Route::post('school/update/{id?}', 'SchoolController@update')->name('school.update');

    Route::delete('school/delete/{id}', 'SchoolController@destroy')->name('school.destroy');
    Route::get('/school/details/{id}', 'SchoolController@show')->name('school.show');
    Route::post('/savesemester', 'SchoolController@savesemester')->name('school.savesemester');
    Route::post('/add-topic-quiz', 'QuestionQuizController@addTopicQuiz')->name('topic.quiz');
    Route::post('/remove-topic-quiz', 'QuestionQuizController@removeTopicQuiz')->name('topic.removeQuestion');
    //category routes
    Route::resource('categories', 'CategoryController');

    //course routes
    Route::get('/courses', 'CourseController@index')->name('courses');
    Route::get('/course/create/{id?}', 'CourseController@create')->name('course.create');
    Route::post('/course/store', 'CourseController@store')->name('course.store');
    Route::get('course/edit/{id}/{school_id?}', 'CourseController@edit')->name('course.edit');
    Route::post('course/update/{id}', 'CourseController@update')->name('course.update');
    Route::delete('course/delete/{id}/{school_id?}', 'CourseController@destroy')->name('course.destroy');
    Route::get('/course/{id}', 'CourseController@show')->name('course.show');
    Route::post('course/edit', 'CourseController@edit_ajax')->name('course.edit_ajax');


    //Comments routes
    Route::get('/comments', 'CommentController@index')->name('comments');
    Route::get('/comments/create/{id?}', 'CommentController@create')->name('comments.create');
    Route::post('/comments/store', 'CommentController@store')->name('comments.store');
    Route::get('comments/edit/{id}', 'CommentController@edit')->name('comments.edit');
    Route::post('comments/update/{id}', 'CommentController@update')->name('comments.update');
    Route::get('comments/delete/{id}', 'CommentController@destroy')->name('comments.destroy');
    Route::get('/comments/{id}', 'CommentController@show')->name('comments.show');
    Route::post('comments/edit', 'CommentController@edit_ajax')->name('comments.edit_ajax');

    //classroom routes
    Route::resource('classrooms', 'ClassroomController');

    //Subject routes
    Route::resource('subjects', 'SubjectController');
    Route::post('subject/edit', 'SubjectController@edit_ajax')->name('subjects.edit_ajax');

    //class routes
    Route::resource('classes', 'ClassesController');
    Route::post('class/edit', 'ClassesController@edit_ajax')->name('classes.edit_ajax');

    //Topic routes
    Route::resource('topics', 'TopicController');
    Route::post('topics/ordering/save', 'TopicController@saveOrdering')->name('topics.ordering.save');
    Route::post('topic/edit', 'TopicController@edit_ajax')->name('topics.edit_ajax');

    //Videos routes
    Route::resource('videos', 'VideoController');

    Route::get('video/upload-files/{uuid}', 'VideoController@uploadFiles')->name('video.upAjaxCload.files');

    //Students routes
    Route::resource('students', 'StudentController');
    Route::get('/students/assignedclasses/{uuid}', 'StudentController@assignedclasses')->name('students.assignedclasses');
    Route::post('students/assignedclasses/store', 'StudentController@save_assignedclasses')->name('students.saveassignedclasses');

    //Tutors routes
    Route::resource('tutors', 'TutorController');

    //profile routes
    Route::resource('profile', 'ProfileController');

    //setting routes
    Route::resource('settings', 'SettingController');

    //School Managers routes
    Route::resource('managers', 'SchoolManagerController');

    //questions routes
    Route::resource('questions', 'QuestionsBankController');

//    Route::get('past-questions', 'QuestionsBankController@getPastQuestions')
//        ->name("past-questions.index");

    Route::get('past-questions', 'QuestionsBankController@getPastQuestions')
        ->name("past-questions.index");

    Route::get('questions-data', 'QuestionsBankController@getQuestionsData')
        ->name("questions.all-questions");

    Route::get('past-questions/create', 'QuestionsBankController@getPastQuestionsCreate')
        ->name("past-questions.create");

    Route::get('past-questions/create-group', 'QuestionsBankController@getPastQuestionsGroupCreate')
        ->name("past-questions-group.create");

    Route::post('past-questions/store-group', 'QuestionsBankController@postPastQuestionsGroupStore')
        ->name("past-questions-group.store");

    Route::get('past-questions/edit-group/{id?}', 'QuestionsBankController@getEditPastQuestions')
        ->name("past-questions-group.edit");

    Route::post('past-questions/update-group/{id?}', 'QuestionsBankController@postUpdateGroupQuestions')
        ->name("past-questions-group.update");

    Route::get('past-questions/group-questions/{id}', 'AjaxController@getPastQuestionsByGroupId')
        ->name('past-questions-group.questions');

    Route::get("main-questions/edit/{id}","QuestionsBankController@getEdit");
    Route::get('past-questions/edit/{id}', 'QuestionsBankController@getPastQuestionsEdit')
        ->name("past-questions.edit");
    Route::get('past-questions/update', 'QuestionsBankController@getPastQuestionsUpdate')
        ->name("past-questions.update ");
    Route::post('past-questions/store', 'QuestionsBankController@store')
        ->name("past-questions.store");
    Route::get("type-ahead-questions","QuestionsBankController@getTypeAheadQuestions");
    Route::get("type-ahead-schools","QuestionsBankController@getTypeAheadSchools");
    Route::get("type-ahead-subjects","QuestionsBankController@getSelectSubjects");
    Route::get("quiz-questions/{question_bank_id}","QuestionQuizController@questionsByQuestionBankId")->name("quiz.questions");

    //question quiz routes
    Route::resource('quiz', 'QuestionQuizController');
    Route::get('/quiz/{id}', 'QuestionQuizController@show')->name('quiz.show');
    Route::get('/edit/{id}', 'QuestionQuizController@show')->name('quiz.update');
    Route::post('/remove-quiz/{id}', 'QuestionQuizController@destroy')->name('quiz.remove');
    //Department routes
    Route::resource('departments', 'DepartmentController');
    Route::post('department/edit', 'DepartmentController@edit_ajax')->name('departments.edit_ajax');

    //periods routes
    Route::resource('periods', 'PeriodController');
    Route::post('period/ordering/save', 'PeriodController@saveOrdering')->name('periods.ordering.save');
    Route::post('period/edit', 'PeriodController@edit_ajax')->name('periods.edit_ajax');

    Route::resource("time-tables","TimeTableController");
    Route::get("/class-calendar/{id}","TimeTableController@classCalendar");

    //Regular School
    Route::group([ 'namespace' => 'RegularSchool','prefix' => 'regular'], function () {
        Route::get('/videos', 'VideoController@listVideos')->name('regular.videos');
        Route::get('/video/{id}', 'VideoController@showVideo')->name('regular.videos.show');
        Route::get('/videos/create', 'VideoController@createVideo')->name('regular.video.create');
        Route::get('/videos/edit/{id}', 'VideoController@editVideo')->name('regular.video.edit');
        Route::put('/videos/update/{id}', 'VideoController@updateVideo')->name('regular.video.update');
        Route::post('/videos/store', 'VideoController@storeVideo')->name('regular.video.store');
    });

    //Open School
    Route::group(['namespace' => 'OpenSchool', 'prefix' => 'open'], function () {
        Route::get('/videos', 'VideoController@listVideos')->name('open.videos');
        Route::get('/video/{id}', 'VideoController@showVideo')->name('open.videos.show');
        Route::get('/videos/create', 'VideoController@createVideo')->name('open.video.create');
        Route::get('/videos/edit/{id}', 'VideoController@editVideo')->name('open.video.edit');
        Route::put('/videos/update/{id}', 'VideoController@updateVideo')->name('open.video.update');
        Route::post('/videos/store', 'VideoController@storeVideo')->name('open.video.store');
    });

    //Professional School
    Route::group(['namespace' => 'Professional', 'prefix' => 'professional'], function () {
        Route::get('/videos', 'VideoController@listVideos')->name('professional.videos.index');
        Route::get('/video/{id}', 'VideoController@showVideo')->name('professional.videos.show');
        Route::get('/videos/create', 'VideoController@createVideo')->name('professional.videos.create');
        Route::get('/videos/edit/{id}', 'VideoController@editVideo')->name('professional.videos.edit');
        Route::post('/videos/update/{id}', 'VideoController@updateVideo')->name('professional.videos.update');
        Route::post('/videos/topic/store', 'VideoController@storeTopic')->name('professional.videos.topic.store');

        Route::post('/videos/store', 'VideoController@store')->name('professional.videos.store');

        Route::resource('/industries', 'IndustryController');
        Route::post('/industries/update/{id}','IndustryController@update')->name("professional.industries.update");
        Route::post('/industries/store','IndustryController@store')->name("professional.industries.store");
        Route::delete('/industries/delete/{id}','IndustryController@destroy')->name("professional.industries.destroy");

        Route::get('/professional-categories', 'ProfessionalCategoryController@index')->name("professional.categories.index");
        Route::post('/professional-categories/update/{id}','ProfessionalCategoryController@update')->name("professional.categories.update");
        Route::post('/professional-categories/store','ProfessionalCategoryController@store')->name("professional.categories.store");
        Route::delete('/professional-categories/delete/{id}','ProfessionalCategoryController@destroy')->name("professional.categories.destroy");

        Route::get('/courses/all', 'CourseController@index')->name('professional.courses.index');
        Route::get('/courses/create', 'CourseController@create')->name('professional.courses.create');
        Route::post('/courses/store', 'CourseController@store')->name('professional.courses.store');
        Route::get('/courses/edit/{id}', 'CourseController@edit')->name('professional.courses.edit');
        Route::post('/courses/update/{id}', 'CourseController@update')->name('professional.courses.update');
        Route::delete('/courses/delete/{id}', 'CourseController@destroy')->name('professional.courses.destroy');

    });

//    Route::group([ 'prefix' => 'streams'], function () {

//    Route::resource('streams', 'StreamsController');
        Route::get('/streams', 'StreamsController@index')->name("streams.index");
        Route::get('/streams-data', 'StreamsController@getStreamsData')->name("streams.data");
        Route::post('/update-stream', 'StreamsController@update')->name("streams.update");
//    });
});
