<?php

Route::post('auth/user/login', 'API\UserController@login');
Route::post('auth/user/role', 'API\GetUserRoleController@getRole');
Route::post('auth/user/register', 'API\UserController@register');
Route::post('auth/phone/verification', 'API\UserController@confirm_phone');
Route::post('auth/validate', 'API\UserController@validate_phone_and_username');
Route::post('auth/change-password', 'API\UserController@update_password');
Route::post('auth/user/save_academic_info', 'API\UserController@save_academic_info');
Route::post('auth/user/save_open_academic_info', 'API\UserAcademicController@save_academic_info');
Route::post('auth/user/save_avatar', 'API\UserController@save_avatar');
