<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::post('/greeting', function (Request $request, \App\Services\NestJsService $nestService) {
    $name = $request->get('name');
    $nestResponse = $nestService->send(['cmd' => 'greeting'], $name);
    return $nestResponse->first();
});

Route::get('/observable', function (\App\Services\NestJsService $nestService) {
    $nestResponse = $nestService->send(['cmd' => 'observable']);
    return $nestResponse->sum();
});

Route::post('removestudent', 'Dex\RemoveUserController@removeRegisteredUser');

Route::group(['namespace' => 'API'], function () {


    Route::post('auth/user/login', 'UserController@login');

    Route::post("mobile-login","UserController@postMobileLogin");

    Route::get("locations","LocationController@getLocation");
    //login with  api
    Route::apiResource('plan', 'PlanController');
    Route::get('check_app_version', 'AppVersionCheckController@getMobileAppVersion');
    Route::post('check_app', 'AppVersionCheckController@getMobileAppVersion2');

    //Request Access
    Route::post('requestaccess', 'RequestAccessController@requestAccess');

      //Request Subscription
    Route::post('request_subscription_pay', 'AppVersionCheckController@make_subscription');
    Route::post('subscription_request', 'PaymentActionController@make_subscription');

    Route::get('subscription_callback', 'PaymentActionController@subscription_callback');

    //Student Quiz
    Route::apiResource('studentquiz', 'StudentQuizController');

    Route::apiResource('questiongroups', 'QuestionGroupController')->only(['index', 'store']);
    Route::apiResource('questionchoices', 'QuestionChoiceController');
    Route::apiResource('quizoption', 'QuestionChoiceController');
    Route::apiResource('questionbank', 'QuestionBankController');

    Route::get("questionbank/questions/{video_id}/{class_id?}/{subject_id?}/{user_id?}",
        "QuestionBankController@videoQuizQuestions")->middleware("auth:api");

    Route::post('add-question', 'QuestionBankController@store');

    Route::post('add-quiz', 'QuestionQuizController@store');

    Route::post('update-quiz/{id}', 'QuestionQuizController@update');
    //
    Route::post('remove-quiz', 'QuestionQuizController@remove');

    Route::apiResource('questionquiz', 'QuestionQuizController');

    Route::post('userplan', 'UserPlanController@addUserPlan');

    Route::post('assignschool', 'SchoolAssignController@autoMobileSchoolAssign');

    Route::apiResource('user_library', 'UserLibraryController')->only(['index', 'store']);

    Route::get('userplan', 'UserPlanController@getUserPlan');

    // Plans
    Route::get('plans','PlanController@index');

    Route::post('directactive', 'UserPlanController@dirrecSubscribe');

    Route::get('daysremaining', 'UserPlanController@getRemaningDays');

    Route::post('sendotp', 'UserVerficationController@sendOtp');

    Route::post('updateotp', 'UserVerficationController@reSendOtp');

    Route::group(['prefix' => 'v2', 'namespace' => 'v2' ], function () {
        Route::post('sendotp', 'UserVerificationController@sendOtp');
        Route::post('updateotp', 'UserVerificationController@reSendOtp');
        Route::post('verifyotp', 'UserVerificationController@verifyOtp');
    });

    Route::post('verifyotp', 'UserVerficationController@verifyOtp')->name("front.mobile.verifyUrl");

//    @include "questions.php";

    Route::get('questiondata/class/{class?}/lesson/{lesson?}', 'QuestionController@questionsData');

    Route::get('all-chats/class/{class?}/lesson/{lesson?}', 'QuestionController@getAllChats');

    Route::post('send-chat', 'QuestionController@sendChat');

//    Route::get('all-chats/class/{class?}/lesson/{lesson?}', 'QuestionController@getAllChats');

    Route::post('questiondata', 'QuestionController@postQuestions');

    Route::get('questions/chat-participants/{class_id}/{lesson_id}/{subject_id}',"QuestionController@getChatParticipants");

//    Route::get('questions/chat-participants/{class_id}/{lesson_id}/{subject_id}',"QuestionController@getChatParticipants");

//    Route::get('questiondata/class/', 'QuestionController@questionsData');
//        ->middleware('auth:api');

    Route::post('questionreply', 'QuestionReplyController@postReply');
    Route::put('questionreply', 'QuestionReplyController@updateReply');

    Route::get('class/library/{class?}/lesson/{id?}', 'LibraryController@getSubjectLibrary');

    Route::get('library', 'LibraryController@getSubjectLibrary');

    Route::post('subscription/check ', 'UserSubscriptionController@checkSubscription')->middleware('auth:api');
    Route::post('subscription/activate', 'UserSubscriptionController@subScribeUser')->middleware('auth:api');
});

//    @include "users_api.php";

Route::get('countries', 'API\UserController@list_countries');
Route::get('avatars', 'API\UserController@list_avatars');

Route::get('institution', 'API\SchoolController@list_institutions');//
Route::get('institution_schools', 'API\InstitutionSchoolsController@loadInstitutionSchools');//
Route::get('institution/{id}/school', 'API\SchoolController@list_schools');//
//Route::get('institution/{id}', 'API\SchoolController@institution_details');


Route::get('school', 'API\SchoolController@list_schools');//
Route::get('load_school', 'API\LoadSchoolsController@index');//
Route::get('get_school_categories', 'API\SchoolCategoryController@loadSchoolInsitutions');//

Route::get('school/{id}', 'API\SchoolController@school_details');//

Route::get('school/{schoolId}/department', 'API\SchoolController@list_departments');//
Route::get('school/{schoolId}/course', 'API\SchoolController@list_school_courses');//
Route::get('school/{schoolId}/class', 'API\SchoolController@list_school_classes');//

Route::get('school/{schoolId}/{departmentId}/course', 'API\SchoolController@list_courses');
Route::get('school/{schoolId}/{courseId}/class', 'API\SchoolController@list_classes');

Route::get('department/{id}', 'API\SchoolController@department_details');//
Route::get('department/{id}/course', 'API\SchoolController@list_department_course');//

//Route::get('courses/{id?}', 'API\SchoolController@list_courses');
Route::get('course/{id}', 'API\SchoolController@course_details');//
Route::get('course/{id}/class', 'API\SchoolController@list_course_classes');//

Route::get('classes/{id?}', 'API\SchoolController@list_classes');
Route::get('class/{id}', 'API\SchoolController@class_details');
//Route::get('class/{id}/lesson', 'API\SchoolController@list_lessons');

Route::get('class/{id}/lesson', 'API\SchoolController@list_lessons_old');

Route::get('class/{id}/lesson/{student_id}', 'API\SchoolController@list_lessons');

Route::get('class/{id}/period', 'API\SchoolController@list_periods');
Route::get('class/{id}/years', 'API\SchoolController@list_years');
Route::get('class/{id}/semesters', 'API\SchoolController@list_semesters');
Route::get('class/{id}/semester-video-dates', 'API\SchoolController@get_semester_video_dates');
Route::get('class/{id}/archive-search', 'API\SchoolController@archive_search');
Route::get('class/{id}/subjects', 'API\SchoolController@list_subjects');
Route::get('subject/{id}/topics', 'API\SchoolController@list_topics');

Route::get('questions/{id}', 'API\SchoolController@list_questions');

Route::post('list_classrooms', 'API\SchoolController@list_classrooms');
Route::get('classroom_detail/{id}', 'API\SchoolController@classroom_detail');

Route::get('teacher/{teacherId}', 'API\UserController@teacher_details');
Route::get('teacher/{teacherId}/video', 'API\UserController@list_teacher_videos');

Route::middleware(['auth:api'])->group(function () {

//    Route::get('library', 'LibraryController@getSubjectLibrary');


    $reqLang = request('lang');
	if(is_null($reqLang)) {
		$lang_id = 1;
		App::setLocale('en');
	} else {
		$lang_id = 1;
		App::setLocale('en');
	}

	Route::post('auth/token/refresh', 'API\UserController@refresh_token');
	Route::post('auth/device', 'API\UserController@save_device');

	Route::get('profile', 'API\UserController@get_user_profile');//

	Route::post('avatar', 'API\UserController@update_avatar');//
	Route::patch('profile', 'API\UserController@update_profile');//
	Route::post('password', 'API\UserController@change_password');//

	//Tutor APIs
	Route::post('video/{id}/send_mail', 'API\UserController@send_mail');//
	Route::post('note', 'API\UserController@upload_note');//
	Route::post('video', 'API\UserController@upload_video');//
	Route::post('attachment', 'API\UserController@upload_video_attachment');//

	//Route::post('verifysignupotp', 'API\UserController@verifysignupotp');
	//Route::post('save_academic_info', 'API\UserController@save_academic_info');
	//Route::post('save_avatar', 'API\UserController@save_avatar');
	//Route::post('profile/make_favorite', 'API\UserController@make_fav_video');
    Route::get('get_profile/history', 'API\StudentController@class_history');

	Route::get('profile/history', 'API\UserController@class_history');
	Route::post('profile/history', 'API\UserController@add_to_history');
	Route::get('profiles', 'API\UserController@list_profiles');

	Route::get('profile/favorite', 'API\UserController@fav_video_list');
	Route::put('profile/favorite', 'API\UserController@make_fav_video');
	Route::delete('profile/favorite', 'API\UserController@make_fav_video');

	Route::put('profile/download', 'API\UserController@download');

    Route::post('question/save', 'API\SchoolController@save_question');

    Route::get('secured-api','API\AuthUserController@getAuthenticatedAPI');

    Route::post("submit-quiz-mobile","Frontend\ClassroomController@SubmitQuizMobile");

    Route::get("subject-library/{class_id}/{video_id}","API\LibraryController@getSubjectLibrary");

    Route::get("library-items/{subject_id}/{class_id}","API\LibraryController@getLibraryItems");

    Route::get("all-library-items","API\LibraryController@getAllLibraryItems");

    Route::get("past-question-years/{subject_id}/{class_id}","API\QuestionBankController@getPastQuestionsGroup");

    Route::get("past-questions/{year}/{subject_id}/{class_id}","API\QuestionBankController@getPastQuestions");

    Route::get("all-past-questions","API\QuestionBankController@getAllPastQuestions");


});

Route::post("subscribe-plan","API\\v2\\PaymentsController@subscribe");

Route::post("payments/submit-otp","API\\v2\\PaymentsController@submitOtp");

Route::get("payments/verify-payment/{reference}","API\\v2\\PaymentsController@verifyPayment");



//Route::fallback(function(){
//    return response()->json(['data'=>new \stdClass(), 'message' => 'Page Not Found. If error persists, contact web@realstudiosonline.com', 'statusCode' => 404], 404);
//});


Route::group(['namespace' => 'Dex'], function () {
    Route::apiResource('scategories','SCategoryController');
    Route::apiResource('ssubjects','SSubjectController');
    Route::apiResource('sclass','SClassController');
    Route::apiResource('open_period','OpenPeriodController');
    Route::apiResource('open_topic','OpenTopicController');
    Route::apiResource('open_tutors','OpenTutorController');
    Route::get('subject_topics/{subject}','OpenSubjectTopicController@getSubjectTopics');

    Route::post('class_subject_topics/{class?}','OpenSubjectTopicController@getClassSubjectTopics');

    Route::post('lecture_notes','OpenSchoolAjaxController@openSchooldropzoneNoteStore');
    Route::post('lesson_video','OpenVideoUploadController@store');
    Route::get('lesson_video','OpenVideoUploadController@index');

    Route::get('subject_lesson_video/{subject?}','OpenClassLessonSubjectsController@loadSubjClassLessons');
    Route::post('class_lesson_video/{class?}','OpenClassLessonSubjectsController@loadClassLessons');

    Route::post('scategoryclass','SCategoryClassController@saveCategoryClass');

    Route::post('assign_categoryclass','SCategoryClassController@saveCategoryClass');
    Route::get('scategoryclass/{id?}','SCategoryClassController@getClasses');
    Route::get('sclasses','SCategoryClassController@getClasses');
    // Route::get('ssubjects','ClassSubjectsController@loadSubjects');
    Route::post('sclass_subjects','ClassSubjectsController@saveClassSubjects');

    Route::post('class_subjects','ClassSubjectsController@store');

    Route::post('assign_class_subjects','ClassSubjectsController@assignClawessSubjects');

     Route::post('payment','ClassSubjectsController@make_subscription');


    Route::get('get_class_subjects','ClassSubjectsController@classSubjects');

    Route::post('assign_course_subjects','CourseSubjectsController@assignClassSubjects');
    Route::get('get_course_subjects/{id?}','CourseSubjectsController@courseSubjects');

    Route::get('class_subjects','ClassSubjectsController@loadSubjects');
    Route::get('class_subjects/{id?}','ClassSubjectsController@getClassSubjects');

    Route::get('open_course','OpenCourseController@index');
    Route::post('open_course','OpenCourseController@store');
    Route::apiResource('class_course','ClassCourseController');
    Route::post('assign_class_course','ClassCourseController@assignClassCourse');
    Route::get('open_class_course/{id?}','ClassCourseController@store');
    Route::get('open_course/{id?}','ClassSubjectsController@getClassSubjects');

//    Route::get('','')
    // Route::apiResource('scategories','SCategoryController');
});

Route::group(["prefix" => "sid"],function (){
    Route::get("schools/all-schools","API\SchoolController@listAllSchools");
    Route::get("schools/school-details-by-code/{code}","API\SchoolController@schoolDetailsByCode");
    Route::post("schools/update-school-details-by-code","API\SchoolController@updateSchoolDetailsByCode");

    Route::get("schools/students/{school_code}","API\SchoolController@getSidStudents");

    Route::get("schools/students/{student_id}","API\SchoolController@getStudentByStudentId");

    Route::post("schools/students/update-student/{student_id}", "API\SchoolController@postUpdateStudentByStudentId");

    Route::post("schools/students/add-new-student", "API\SchoolController@postAddNewStudent");

//    Route::post("schools/students/update-student/{student_id}", "API\SchoolController@schoolBySchoolCode");

    Route::get("/students",function (){
//        $student_builder = \App\SidStudent::where('info->shs_number','1800304030137')->first();
//        return response()->json(["data" => $student_builder]);
        return response()->json(["data" => \App\SidStudent::take(100)->get()]);
    });
});

Route::group(["prefix" => "videos"],function (){
    Route::get("lessons/{class}/{user}","API\SchoolController@list_lessons");
});

Route::get("/schools","API\SchoolController@getSelectSchools");

@include_once 'past_question_routes.php';

