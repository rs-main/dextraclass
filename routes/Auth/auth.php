<?php

Route::namespace('Auth')->group(function () {
    /* Below route for all register get routes land on that route */
    Route::get('/register/step{num}', 'RegisterController@step')->name('registerStep');

    /* Below routes create user and save user extra inforamtion */
    Route::post('/register/step2', 'RegisterController@step2')->name('registerStep2');
    Route::post('/register/step3', 'RegisterController@step3')->name('registerStep3');
    Route::post('/register/step4', 'RegisterController@step4')->name('registerStep4');
    Route::get('/register/step', 'RegisterController@step5')->name('register.step5');
    /* End routes create user and save user extra inforamtion */

    Route::get('/register-success/{id}', 'RegisterController@success')->name('registerSuccess');

    Route::get('/user/verify/{token}', 'RegisterController@verifyUser');

    Route::get("/google","LoginController@redirectToGoogle");
    Route::get("/google/callback","LoginController@handleGoogleCallback");
});
