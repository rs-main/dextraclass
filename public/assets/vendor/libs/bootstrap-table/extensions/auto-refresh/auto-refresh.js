/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 35);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/auto-refresh/bootstrap-table-auto-refresh.js":
/*!**************************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/auto-refresh/bootstrap-table-auto-refresh.js ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author: Alec Fenichel
 * @webSite: https://fenichelar.com
 * @update: zhixin wen <wenzhixin2010@gmail.com>
 */

const Utils = $.fn.bootstrapTable.utils

$.extend($.fn.bootstrapTable.defaults, {
  autoRefresh: false,
  autoRefreshInterval: 60,
  autoRefreshSilent: true,
  autoRefreshStatus: true,
  autoRefreshFunction: null
})

$.extend($.fn.bootstrapTable.defaults.icons, {
  autoRefresh: {
    bootstrap3: 'glyphicon-time icon-time',
    materialize: 'access_time'
  }[$.fn.bootstrapTable.theme] || 'fa-clock'
})

$.extend($.fn.bootstrapTable.locales, {
  formatAutoRefresh () {
    return 'Auto Refresh'
  }
})

$.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales)

$.BootstrapTable = class extends $.BootstrapTable {
  init (...args) {
    super.init(...args)

    if (this.options.autoRefresh && this.options.autoRefreshStatus) {
      this.options.autoRefreshFunction = setInterval(() => {
        this.refresh({silent: this.options.autoRefreshSilent})
      }, this.options.autoRefreshInterval * 1000)
    }
  }

  initToolbar (...args) {
    super.initToolbar(...args)

    if (this.options.autoRefresh) {
      const $btnGroup = this.$toolbar.find('>.columns')
      let $btnAutoRefresh = $btnGroup.find('.auto-refresh')

      if (!$btnAutoRefresh.length) {
        $btnAutoRefresh = $(`
          <button class="auto-refresh ${this.constants.buttonsClass}
          ${this.options.autoRefreshStatus ? ` ${this.constants.classes.buttonActive}` : ''}"
          type="button" title="${this.options.formatAutoRefresh()}">
          ${ this.options.showButtonIcons ? Utils.sprintf(this.constants.html.icon, this.options.iconsPrefix, this.options.icons.autoRefresh) : ''}
          ${ this.options.showButtonText ? this.options.formatAutoRefresh() : ''}
          </button>
        `).appendTo($btnGroup)

        $btnAutoRefresh.on('click', $.proxy(this.toggleAutoRefresh, this))
      }
    }
  }

  toggleAutoRefresh () {
    if (this.options.autoRefresh) {
      if (this.options.autoRefreshStatus) {
        clearInterval(this.options.autoRefreshFunction)
        this.$toolbar.find('>.columns').find('.auto-refresh')
          .removeClass(this.constants.classes.buttonActive)
      } else {
        this.options.autoRefreshFunction = setInterval(() => {
          this.refresh({silent: this.options.autoRefreshSilent})
        }, this.options.autoRefreshInterval * 1000)
        this.$toolbar.find('>.columns').find('.auto-refresh')
          .addClass(this.constants.classes.buttonActive)
      }
      this.options.autoRefreshStatus = !this.options.autoRefreshStatus
    }
  }
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/auto-refresh/auto-refresh.js":
/*!**********************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/auto-refresh/auto-refresh.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/auto-refresh/bootstrap-table-auto-refresh.js */ "./node_modules/bootstrap-table/src/extensions/auto-refresh/bootstrap-table-auto-refresh.js");

/***/ }),

/***/ 35:
/*!****************************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/auto-refresh/auto-refresh.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\auto-refresh\auto-refresh.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/auto-refresh/auto-refresh.js");


/***/ })

/******/ });