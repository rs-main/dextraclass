/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 54);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.js":
/*!**************************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.js ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author: Dennis Hernández
 * @webSite: http://djhvscf.github.io/Blog
 * @version: v1.0.1
 */

const isSearch = false

const rowAttr = (row, index) => ({
  id: `customId_${index}`
})

$.extend($.fn.bootstrapTable.defaults, {
  reorderableRows: false,
  onDragStyle: null,
  onDropStyle: null,
  onDragClass: 'reorder_rows_onDragClass',
  dragHandle: null,
  useRowAttrFunc: false,
  onReorderRowsDrag (table, row) {
    return false
  },
  onReorderRowsDrop (table, row) {
    return false
  },
  onReorderRow (newData) {
    return false
  }
})

$.extend($.fn.bootstrapTable.Constructor.EVENTS, {
  'reorder-row.bs.table': 'onReorderRow'
})

const BootstrapTable = $.fn.bootstrapTable.Constructor
const _init = BootstrapTable.prototype.init
const _initSearch = BootstrapTable.prototype.initSearch

BootstrapTable.prototype.init = function (...args) {

  if (!this.options.reorderableRows) {
    _init.apply(this, Array.prototype.slice.apply(args))
    return
  }

  const that = this
  if (this.options.useRowAttrFunc) {
    this.options.rowAttributes = rowAttr
  }

  const onPostBody = this.options.onPostBody
  this.options.onPostBody = () => {
    setTimeout(() => {
      that.makeRowsReorderable()
      onPostBody.apply()
    }, 1)
  }

  _init.apply(this, Array.prototype.slice.apply(args))
}

BootstrapTable.prototype.initSearch = function (...args) {
  _initSearch.apply(this, Array.prototype.slice.apply(args))

  if (!this.options.reorderableRows) {
    return
  }

  // Known issue after search if you reorder the rows the data is not display properly
  // isSearch = true;
}

BootstrapTable.prototype.makeRowsReorderable = function () {
  if (this.options.cardView) {
    return
  }

  const that = this
  this.$el.tableDnD({
    onDragStyle: that.options.onDragStyle,
    onDropStyle: that.options.onDropStyle,
    onDragClass: that.options.onDragClass,
    onDrop: that.onDrop,
    onDragStart: that.options.onReorderRowsDrag,
    dragHandle: that.options.dragHandle
  })
}

BootstrapTable.prototype.onDrop = (table, droppedRow) => {
  const tableBs = $(table)
  const tableBsData = tableBs.data('bootstrap.table')
  const tableBsOptions = tableBs.data('bootstrap.table').options
  let row = null
  const newData = []

  for (let i = 0; i < table.tBodies[0].rows.length; i++) {
    row = $(table.tBodies[0].rows[i])
    newData.push(tableBsOptions.data[row.data('index')])
    row.data('index', i).attr('data-index', i)
  }

  tableBsOptions.data = tableBsOptions.data.slice(0, tableBsData.pageFrom - 1)
    .concat(newData)
    .concat(tableBsOptions.data.slice(tableBsData.pageTo))

  // Call the user defined function
  tableBsOptions.onReorderRowsDrop.apply(table, [table, droppedRow])

  // Call the event reorder-row
  tableBsData.trigger('reorder-row', newData)
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/reorder-rows/reorder-rows.js":
/*!**********************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/reorder-rows/reorder-rows.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.js */ "./node_modules/bootstrap-table/src/extensions/reorder-rows/bootstrap-table-reorder-rows.js");

/***/ }),

/***/ 54:
/*!****************************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/reorder-rows/reorder-rows.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\reorder-rows\reorder-rows.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/reorder-rows/reorder-rows.js");


/***/ })

/******/ });