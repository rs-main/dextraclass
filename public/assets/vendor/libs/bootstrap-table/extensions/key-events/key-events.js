/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 46);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/key-events/bootstrap-table-key-events.js":
/*!**********************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/key-events/bootstrap-table-key-events.js ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author: Dennis Hernández
 * @webSite: http://djhvscf.github.io/Blog
 * @update zhixin wen <wenzhixin2010@gmail.com>
 */

$.extend($.fn.bootstrapTable.defaults, {
  keyEvents: false
})

$.BootstrapTable = class extends $.BootstrapTable {

  init (...args) {
    super.init(...args)

    if (this.options.keyEvents) {
      this.initKeyEvents()
    }
  }

  initKeyEvents () {
    $(document).off('keydown').on('keydown', e => {
      const $search = this.$toolbar.find('.search input')
      const $refresh = this.$toolbar.find('button[name="refresh"]')
      const $toggle = this.$toolbar.find('button[name="toggle"]')
      const $paginationSwitch = this.$toolbar.find('button[name="paginationSwitch"]')

      if (document.activeElement === $search.get(0) || !$.contains(document.activeElement ,this.$toolbar.get(0))) {
        return true
      }

      switch (e.keyCode) {
        case 83: // s
          if (!this.options.search) {
            return
          }
          $search.focus()
          return false
        case 82: // r
          if (!this.options.showRefresh) {
            return
          }
          $refresh.click()
          return false
        case 84: // t
          if (!this.options.showToggle) {
            return
          }
          $toggle.click()
          return false
        case 80: // p
          if (!this.options.showPaginationSwitch) {
            return
          }
          $paginationSwitch.click()
          return false
        case 37: // left
          if (!this.options.pagination) {
            return
          }
          this.prevPage()
          return false
        case 39: // right
          if (!this.options.pagination) {
            return
          }
          this.nextPage()
          return
        default:
          break
      }
    })
  }
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/key-events/key-events.js":
/*!******************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/key-events/key-events.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/key-events/bootstrap-table-key-events.js */ "./node_modules/bootstrap-table/src/extensions/key-events/bootstrap-table-key-events.js");

/***/ }),

/***/ 46:
/*!************************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/key-events/key-events.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\key-events\key-events.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/key-events/key-events.js");


/***/ })

/******/ });