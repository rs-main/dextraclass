/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 49);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/natural-sorting/bootstrap-table-natural-sorting.js":
/*!********************************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/natural-sorting/bootstrap-table-natural-sorting.js ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * @author: Brian Huisman
 * @webSite: http://www.greywyvern.com
 * JS functions to allow natural sorting on bootstrap-table columns
 * add data-sorter="alphanum" or data-sorter="numericOnly" to any th
 *
 * @update Dennis Hernández <http://djhvscf.github.io/Blog>
 * @update Duane May
 */

function alphanum (a, b) {
  function chunkify (t) {
    const tz = []
    let y = -1
    let n = 0

    for (let i = 0; i <= t.length; i++) {
      const char = t.charAt(i)
      const charCode = char.charCodeAt(0)
      const m = (charCode === 46 || (charCode >= 48 && charCode <= 57))
      if (m !== n) {
        tz[++y] = ''
        n = m
      }
      tz[y] += char
    }

    return tz
  }

  function stringfy (v) {
    if (typeof(v) === 'number') {
      v = `${v}`
    }
    if (!v) {
      v = ''
    }
    return v
  }

  const aa = chunkify(stringfy(a))
  const bb = chunkify(stringfy(b))

  for (let x = 0; aa[x] && bb[x]; x++) {
    if (aa[x] !== bb[x]) {
      const c = Number(aa[x])
      const d = Number(bb[x])

      if (c === aa[x] && d === bb[x]) {
        return c - d
      }
      return (aa[x] > bb[x]) ? 1 : -1

    }
  }
  return aa.length - bb.length
}

function numericOnly (a, b) {
  function stripNonNumber (s) {
    s = s.replace(new RegExp(/[^0-9]/g), '')
    return parseInt(s, 10)
  }

  return stripNonNumber(a) - stripNonNumber(b)
}

/* harmony default export */ __webpack_exports__["default"] = ({
  alphanum,
  numericOnly
});


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/natural-sorting/natural-sorting.js":
/*!****************************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/natural-sorting/natural-sorting.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/natural-sorting/bootstrap-table-natural-sorting.js */ "./node_modules/bootstrap-table/src/extensions/natural-sorting/bootstrap-table-natural-sorting.js");

/***/ }),

/***/ 49:
/*!**********************************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/natural-sorting/natural-sorting.js ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\natural-sorting\natural-sorting.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/natural-sorting/natural-sorting.js");


/***/ })

/******/ });