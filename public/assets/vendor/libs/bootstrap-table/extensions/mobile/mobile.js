/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 47);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/mobile/bootstrap-table-mobile.js":
/*!**************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/mobile/bootstrap-table-mobile.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author: Dennis Hernández
 * @webSite: http://djhvscf.github.io/Blog
 * @update zhixin wen <wenzhixin2010@gmail.com>
 */

const debounce = (func, wait) => {
  let timeout = 0
  return (...args) => {
    const later = () => {
      timeout = 0
      func(...args)
    }
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
  }
}

$.extend($.fn.bootstrapTable.defaults, {
  mobileResponsive: false,
  minWidth: 562,
  minHeight: undefined,
  heightThreshold: 100, // just slightly larger than mobile chrome's auto-hiding toolbar
  checkOnInit: true,
  columnsHidden: []
})

$.BootstrapTable = class extends $.BootstrapTable {
  init (...args) {
    super.init(...args)

    if (!this.options.mobileResponsive || !this.options.minWidth) {
      return
    }

    if (this.options.minWidth < 100 && this.options.resizable) {
      console.info('The minWidth when the resizable extension is active should be greater or equal than 100')
      this.options.minWidth = 100
    }

    let old = {
      width: $(window).width(),
      height: $(window).height()
    }

    $(window).on('resize orientationchange', debounce(() => {
      // reset view if height has only changed by at least the threshold.
      const width = $(window).width()
      const height = $(window).height()

      if (
        Math.abs(old.height - height) > this.options.heightThreshold ||
        old.width !== width
      ) {
        this.changeView(width, height)
        old = {
          width,
          height
        }
      }
    },200))

    if (this.options.checkOnInit) {
      const width = $(window).width()
      const height = $(window).height()
      this.changeView(width, height)
      old = {
        width,
        height
      }
    }
  }

  conditionCardView () {
    this.changeTableView(false)
    this.showHideColumns(false)
  }

  conditionFullView () {
    this.changeTableView(true)
    this.showHideColumns(true)
  }

  changeTableView (cardViewState) {
    this.options.cardView = cardViewState
    this.toggleView()
  }

  showHideColumns (checked) {
    if (this.options.columnsHidden.length > 0 ) {
      this.columns.forEach(column => {
        if (this.options.columnsHidden.includes(column.field)) {
          if (column.visible !== checked) {
            this._toggleColumn(this.fieldsColumnsIndex[column.field], checked, true)
          }
        }
      })
    }
  }

  changeView (width, height) {
    if (this.options.minHeight) {
      if ((width <= this.options.minWidth) && (height <= this.options.minHeight)) {
        this.conditionCardView()
      } else if ((width > this.options.minWidth) && (height > this.options.minHeight)) {
        this.conditionFullView()
      }
    } else {
      if (width <= this.options.minWidth) {
        this.conditionCardView()
      } else if (width > this.options.minWidth) {
        this.conditionFullView()
      }
    }

    this.resetView()
  }
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/mobile/mobile.js":
/*!**********************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/mobile/mobile.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/mobile/bootstrap-table-mobile.js */ "./node_modules/bootstrap-table/src/extensions/mobile/bootstrap-table-mobile.js");

/***/ }),

/***/ 47:
/*!****************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/mobile/mobile.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\mobile\mobile.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/mobile/mobile.js");


/***/ })

/******/ });