/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 40);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js":
/*!******************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author zhixin wen <wenzhixin2010@gmail.com>
 * extensions: https://github.com/vitalets/x-editable
 */

const Utils = $.fn.bootstrapTable.utils

$.extend($.fn.bootstrapTable.defaults, {
  editable: true,
  onEditableInit () {
    return false
  },
  onEditableSave (field, row, rowIndex, oldValue, $el) {
    return false
  },
  onEditableShown (field, row, $el, editable) {
    return false
  },
  onEditableHidden (field, row, $el, reason) {
    return false
  }
})

$.extend($.fn.bootstrapTable.Constructor.EVENTS, {
  'editable-init.bs.table': 'onEditableInit',
  'editable-save.bs.table': 'onEditableSave',
  'editable-shown.bs.table': 'onEditableShown',
  'editable-hidden.bs.table': 'onEditableHidden'
})

$.BootstrapTable = class extends $.BootstrapTable {
  initTable () {
    super.initTable()

    if (!this.options.editable) {
      return
    }

    $.each(this.columns, (i, column) => {
      if (!column.editable) {
        return
      }

      const editableOptions = {}
      const editableDataMarkup = []
      const editableDataPrefix = 'editable-'
      const processDataOptions = (key, value) => {
        // Replace camel case with dashes.
        const dashKey = key.replace(/([A-Z])/g, $1 => `-${$1.toLowerCase()}`)
        if (dashKey.indexOf(editableDataPrefix) === 0) {
          editableOptions[dashKey.replace(editableDataPrefix, 'data-')] = value
        }
      }

      $.each(this.options, processDataOptions)

      column.formatter = column.formatter || (value => value)
      column._formatter = column._formatter ? column._formatter : column.formatter
      column.formatter = (value, row, index) => {
        let result = Utils.calculateObjectValue(column, column._formatter, [value, row, index], value)
        result = typeof result === 'undefined' || result === null ? this.options.undefinedText : result

        $.each(column, processDataOptions)

        $.each(editableOptions, (key, value) => {
          editableDataMarkup.push(` ${key}="${value}"`)
        })

        let _dont_edit_formatter = false
        if (column.editable.hasOwnProperty('noeditFormatter')) {
          _dont_edit_formatter = column.editable.noeditFormatter(value, row, index)
        }

        if (_dont_edit_formatter === false) {
          return `<a href="javascript:void(0)"
            data-name="${column.field}"
            data-pk="${row[this.options.idField]}"
            data-value="${result}"
            ${editableDataMarkup.join('')}></a>`
        }
        return _dont_edit_formatter
      }
    })
  }

  initBody (fixedScroll) {
    super.initBody(fixedScroll)

    if (!this.options.editable) {
      return
    }

    $.each(this.columns, (i, column) => {
      if (!column.editable) {
        return
      }

      const data = this.getData()
      const $field = this.$body.find(`a[data-name="${column.field}"]`)

      $field.each((i, element) => {
        const $element = $(element)
        const $tr = $element.closest('tr')
        const index = $tr.data('index')
        const row = data[index]

        const editableOpts = Utils.calculateObjectValue(column,
          column.editable, [index, row, $element], {})

        $element.editable(editableOpts)
      })

      $field.off('save').on('save', ({currentTarget}, {submitValue}) => {
        const $this = $(currentTarget)
        const data = this.getData()
        const rowIndex = $this.parents('tr[data-index]').data('index')
        const row = data[rowIndex]
        const oldValue = row[column.field]

        $this.data('value', submitValue)
        row[column.field] = submitValue
        this.trigger('editable-save', column.field, row, rowIndex, oldValue, $this)
        this.initBody()
      })

      $field.off('shown').on('shown', ({currentTarget}, editable) => {
        const $this = $(currentTarget)
        const data = this.getData()
        const rowIndex = $this.parents('tr[data-index]').data('index')
        const row = data[rowIndex]

        this.trigger('editable-shown', column.field, row, $this, editable)
      })

      $field.off('hidden').on('hidden', ({currentTarget}, reason) => {
        const $this = $(currentTarget)
        const data = this.getData()
        const rowIndex = $this.parents('tr[data-index]').data('index')
        const row = data[rowIndex]

        this.trigger('editable-hidden', column.field, row, $this, reason)
      })
    })
    this.trigger('editable-init')
  }
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/editable/editable.js":
/*!**************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/editable/editable.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/editable/bootstrap-table-editable.js */ "./node_modules/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js");

/***/ }),

/***/ 40:
/*!********************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/editable/editable.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\editable\editable.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/editable/editable.js");


/***/ })

/******/ });