/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 34);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/addrbar/bootstrap-table-addrbar.js":
/*!****************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/addrbar/bootstrap-table-addrbar.js ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author: general
 * @website: note.generals.space
 * @email: generals.space@gmail.com
 * @github: https://github.com/generals-space/bootstrap-table-addrbar
 * @update: zhixin wen <wenzhixin2010@gmail.com>
 */

/*
 * function: 获取浏览器地址栏中的指定参数.
 * key: 参数名
 * url: 默认为当前地址栏
 */
function _GET (key, url = window.location.search) {
  /*
   * 注意这里正则表达式的书写方法
   * (^|&)key匹配: 直接以key开始或以&key开始的字符串
   * 同理(&|$)表示以&结束或是直接结束的字符串
   * ...当然, 我并不知道这种用法.
   */
  const reg = new RegExp(`(^|&)${key}=([^&]*)(&|$)`)
  const result = url.substr(1).match(reg)

  if (result) {
    return decodeURIComponent(result[2])
  }
  return null
}

/*
 * function: 根据给定参数生成url地址
 * var dic = {name: 'genreal', age: 24}
 * var url = 'https://www.baidu.com?age=22';
 * _buildUrl(dic, url);
 * 将得到"https://www.baidu.com?age=24&name=genreal"
 * 哦, 忽略先后顺序吧...
 *
 * 补充: 可以参考浏览器URLSearchParams对象, 更加方便和强大.
 * 考虑到兼容性, 暂时不使用这个工具.
 */

function _buildUrl (dict, url = window.location.search) {
  for (const [key, val] of Object.entries(dict)) {
    // 搜索name=general这种形式的字符串(&是分隔符)
    const pattern = `${key}=([^&]*)`
    const targetStr = `${key}=${val}`

    /*
     * 如果目标url中包含了key键, 我们需要将它替换成我们自己的val
     * 不然就直接添加好了.
     */
    if (url.match(pattern)) {
      const tmp = new RegExp(`(${key}=)([^&]*)`, 'gi')
      url = url.replace(tmp, targetStr)
    } else {
      const seperator = url.match('[?]') ? '&' : '?'
      url = url + seperator + targetStr
    }
  }
  if (location.hash) {
    url += location.hash
  }
  return url
}

$.BootstrapTable = class extends $.BootstrapTable {
  init (...args) {
    if (
      this.options.pagination &&
      this.options.sidePagination === 'server' &&
      this.options.addrbar
    ) {
      // 标志位, 初始加载后关闭
      this.addrbarInit = true
      const _prefix = this.options.addrPrefix || ''

      // 优先级排序: 用户指定值最优先, 未指定时从地址栏获取, 未获取到时采用默认值
      this.options.pageNumber = +_GET(`${_prefix}page`) || $.BootstrapTable.DEFAULTS.pageNumber
      this.options.pageSize = +_GET(`${_prefix}size`) || $.BootstrapTable.DEFAULTS.pageSize
      this.options.sortOrder = _GET(`${_prefix}order`) || $.BootstrapTable.DEFAULTS.sortOrder
      this.options.sortName = _GET(`${_prefix}sort`) || $.BootstrapTable.DEFAULTS.sortName
      this.options.searchText = _GET(`${_prefix}search`) || $.BootstrapTable.DEFAULTS.searchText

      const _onLoadSuccess = this.options.onLoadSuccess

      this.options.onLoadSuccess = data => {
        if (this.addrbarInit) {
          this.addrbarInit = false
        } else {
          const params = {}
          params[`${_prefix}page`] = this.options.pageNumber,
          params[`${_prefix}size`] = this.options.pageSize,
          params[`${_prefix}order`] = this.options.sortOrder,
          params[`${_prefix}sort`] = this.options.sortName,
          params[`${_prefix}search`] = this.options.searchText
          // h5提供的修改浏览器地址栏的方法
          window.history.pushState({}, '', _buildUrl(params))
        }

        if (_onLoadSuccess) {
          _onLoadSuccess.call(this, data)
        }
      }
    }
    super.init(...args)
  }
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/addrbar/addrbar.js":
/*!************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/addrbar/addrbar.js ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/addrbar/bootstrap-table-addrbar.js */ "./node_modules/bootstrap-table/src/extensions/addrbar/bootstrap-table-addrbar.js");

/***/ }),

/***/ 34:
/*!******************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/addrbar/addrbar.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\addrbar\addrbar.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/addrbar/addrbar.js");


/***/ })

/******/ });