/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 43);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/fixed-columns/bootstrap-table-fixed-columns.js":
/*!****************************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/fixed-columns/bootstrap-table-fixed-columns.js ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author zhixin wen <wenzhixin2010@gmail.com>
 */

$.extend($.fn.bootstrapTable.defaults, {
  fixedColumns: false,
  fixedNumber: 1
})

$.BootstrapTable = class extends $.BootstrapTable {

  fitHeader (...args) {
    super.fitHeader(...args)

    if (!this.options.fixedColumns) {
      return
    }

    if (this.$el.is(':hidden')) {
      return
    }

    this.$container.find('.fixed-table-header-columns').remove()
    this.$fixedHeader = $('<div class="fixed-table-header-columns"></div>')
    this.$fixedHeader.append(this.$tableHeader.find('>table').clone(true))
    this.$tableHeader.after(this.$fixedHeader)

    const width = this.getFixedColumnsWidth()

    this.$fixedHeader.css({
      top: 0,
      width,
      height: this.$tableHeader.outerHeight(true)
    })

    this.initFixedColumnsBody()

    this.$fixedBody.css({
      top: this.$tableHeader.outerHeight(true),
      width,
      height: this.$tableBody.outerHeight(true) - 1
    })

    this.initFixedColumnsEvents()
  }

  initBody (...args) {
    super.initBody(...args)

    if (!this.options.fixedColumns) {
      return
    }

    if (this.options.showHeader && this.options.height) {
      return
    }

    this.initFixedColumnsBody()

    this.$fixedBody.css({
      top: 0,
      width: this.getFixedColumnsWidth(),
      height: this.$tableHeader.outerHeight(true) + this.$tableBody.outerHeight(true)
    })

    this.initFixedColumnsEvents()
  }

  initFixedColumnsBody () {
    this.$container.find('.fixed-table-body-columns').remove()
    this.$fixedBody = $('<div class="fixed-table-body-columns"></div>')
    this.$fixedBody.append(this.$tableBody.find('>table').clone(true))
    this.$tableBody.after(this.$fixedBody)
  }

  getFixedColumnsWidth () {
    const visibleFields = this.getVisibleFields()
    let width = 0

    for (let i = 0; i < this.options.fixedNumber; i++) {
      width += this.$header.find(`th[data-field="${visibleFields[i]}"]`).outerWidth(true)
    }

    return width + 1
  }

  initFixedColumnsEvents () {
    // events
    this.$tableBody.off('scroll.fixed-columns').on('scroll.fixed-columns', e => {
      this.$fixedBody.find('table').css('top', -$(e.currentTarget).scrollTop())
    })

    this.$body.find('> tr[data-index]').off('hover').hover(e => {
      const index = $(e.currentTarget).data('index')
      this.$fixedBody.find(`tr[data-index="${index}"]`)
        .css('background-color', $(e.currentTarget).css('background-color'))
    }, e => {
      const index = $(e.currentTarget).data('index')
      const $tr = this.$fixedBody.find(`tr[data-index="${index}"]`)
      $tr.attr('style', $tr.attr('style').replace(/background-color:.*;/, ''))
    })

    this.$fixedBody.find('tr[data-index]').off('hover').hover(e => {
      const index = $(e.currentTarget).data('index')
      this.$body.find(`tr[data-index="${index}"]`)
        .css('background-color', $(e.currentTarget).css('background-color'))
    }, e => {
      const index = $(e.currentTarget).data('index')
      const $tr = this.$body.find(`> tr[data-index="${index}"]`)
      $tr.attr('style', $tr.attr('style').replace(/background-color:.*;/, ''))
    })
  }
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/fixed-columns/fixed-columns.js":
/*!************************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/fixed-columns/fixed-columns.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/fixed-columns/bootstrap-table-fixed-columns.js */ "./node_modules/bootstrap-table/src/extensions/fixed-columns/bootstrap-table-fixed-columns.js");

/***/ }),

/***/ 43:
/*!******************************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/fixed-columns/fixed-columns.js ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\fixed-columns\fixed-columns.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/fixed-columns/fixed-columns.js");


/***/ })

/******/ });