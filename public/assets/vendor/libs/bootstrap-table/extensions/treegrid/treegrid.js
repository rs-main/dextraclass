/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 58);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/bootstrap-table/src/extensions/treegrid/bootstrap-table-treegrid.js":
/*!******************************************************************************************!*\
  !*** ./node_modules/bootstrap-table/src/extensions/treegrid/bootstrap-table-treegrid.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * @author: YL
 * @update: zhixin wen <wenzhixin2010@gmail.com>
 */

$.extend($.fn.bootstrapTable.defaults, {
  treeEnable: false,
  treeShowField: null,
  idField: 'id',
  parentIdField: 'pid',
  rootParentId: null
})

$.BootstrapTable = class extends $.BootstrapTable {

  init (...args) {
    this._rowStyle = this.options.rowStyle
    super.init(...args)
  }

  initHeader (...args) {
    super.initHeader(...args)
    const treeShowField = this.options.treeShowField
    if (treeShowField) {
      for (const field of this.header.fields) {
        if (treeShowField === field) {
          this.treeEnable = true
          break
        }
      }
    }
  }

  initBody (...args) {
    this.options.virtualScroll = !this.treeEnable
    super.initBody(...args)
  }

  initTr (item, idx, data, parentDom) {
    const nodes = data.filter(it => item[this.options.idField] === it[this.options.parentIdField])

    parentDom.append(super.initRow(item, idx, data, parentDom))

    // init sub node
    const len = nodes.length - 1
    for (let i = 0; i <= len; i++) {
      const node = nodes[i]
      const defaultItem = $.extend(true, {}, item)
      node._level = defaultItem._level + 1
      node._parent = defaultItem
      if (i === len) {
        node._last = 1
      }
      // jquery.treegrid.js
      this.options.rowStyle = (item, idx) => {
        const res = this._rowStyle(item, idx)
        const id = item[this.options.idField] ? item[this.options.idField] : 0
        const pid = item[this.options.parentIdField] ? item[this.options.parentIdField] : 0
        res.classes = [
          res.classes || '',
          `treegrid-${id}`,
          `treegrid-parent-${pid}`
        ].join(' ')
        return res
      }
      this.initTr(node, $.inArray(node, data), data, parentDom)
    }
  }

  initRow (item, idx, data, parentDom) {
    if (this.treeEnable) {
      if (
        this.options.rootParentId === item[this.options.parentIdField] ||
        !item[this.options.parentIdField]
      ) {
        if (item._level === undefined) {
          item._level = 0
        }
        // jquery.treegrid.js
        this.options.rowStyle = (item, idx) => {
          const res = this._rowStyle(item, idx)
          const x = item[this.options.idField] ? item[this.options.idField] : 0
          res.classes = [
            res.classes || '',
            `treegrid-${x}`
          ].join(' ')
          return res
        }
        this.initTr(item, idx, data, parentDom)
        return true
      }
      return false
    }
    return super.initRow(item, idx, data, parentDom)
  }
}


/***/ }),

/***/ "./resources/assets/vendor/libs/bootstrap-table/extensions/treegrid/treegrid.js":
/*!**************************************************************************************!*\
  !*** ./resources/assets/vendor/libs/bootstrap-table/extensions/treegrid/treegrid.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! bootstrap-table/src/extensions/treegrid/bootstrap-table-treegrid.js */ "./node_modules/bootstrap-table/src/extensions/treegrid/bootstrap-table-treegrid.js");

/***/ }),

/***/ 58:
/*!********************************************************************************************!*\
  !*** multi ./resources/assets/vendor/libs/bootstrap-table/extensions/treegrid/treegrid.js ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\dextraclassdemo\resources\assets\vendor\libs\bootstrap-table\extensions\treegrid\treegrid.js */"./resources/assets/vendor/libs/bootstrap-table/extensions/treegrid/treegrid.js");


/***/ })

/******/ });