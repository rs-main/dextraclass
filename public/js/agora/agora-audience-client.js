/**
 * Agora Broadcast Client
 */

const appId = "3a66d4551f5641b3825424ef8c310e8c";
let localChannelStorage = window.localStorage;
let channelId = channel_id;
// let channelId = localStorage.getItem("channel_name");
let streamId  = "";

const mainContainer = $("#main-container");
console.log("container " + mainContainer)

var agoraAppId = appId; // set app id
var channelName = channelId; // set channel name

// create client
var client = AgoraRTC.createClient({mode: 'live', codec: 'vp8'}); // vp8 to work across mobile devices

AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.DEBUG);
$( document ).ready( function() {
  // Due to browser restrictions on auto-playing video,
  // user must click to init and join channel
  // $("#watch-live-btn").click(function(){
  // $("#play-button").click(function(){
  $("#fullscreen-button").click(function(){
    console.log("user clicked to watch broadcast")
    // init Agora SDK
    client.init(agoraAppId, function () {
      $("#watch-live-overlay").remove();
        $("#loader").show();
        $("#loading-stream-text").show();
      console.log('AgoraRTC client initialized');
      joinChannel(); // join channel upon successful init
    }, function (err) {
      console.log('[ERROR] : AgoraRTC client init failed', err);
    });
  });
});

client.on('stream-published', function (evt) {
  console.log('Publish local stream successfully');
});

// connect remote streams
client.on('stream-added', function (evt) {
  var stream = evt.stream;
  var streamId = stream.getId();
  console.log('New stream added: ' + streamId);
  console.log('Subscribing to remote stream:' + streamId);
    getStreamId(streamId);
  // Subscribe to the stream.
  client.subscribe(stream, function (err) {
    console.log('[ERROR] : subscribe stream failed', err);
  });
});

client.on('stream-removed', function (evt) {
  var stream = evt.stream;
  stream.stop(); // stop the stream
  stream.close(); // clean up and close the camera stream
  console.log("Remote stream is removed " + stream.getId());
    getStreamId(stream.getId())
});

client.on('stream-subscribed', function (evt) {
  var remoteStream = evt.stream;
  var remoteId =  remoteStream.getId();
  streamId = remoteStream.getId();
    // remoteStream.play('full-screen-video');
    getStreamId(streamId);

    mainContainer.trigger("container:start",[evt.stream.getId(),true]);

  console.log('Successfully subscribed to remote stream: ' + remoteId);
    // joinStream
  // if( $('#full-screen-video').is(':empty') ) {
  if( $('#video-container').is(':empty') ) {
    mainStreamId = remoteId;
    remoteStream.play('video-container');
    // remoteStream.play('full-screen-video');
  } else {
    addRemoteStreamMiniView(remoteStream);
  }
});

// remove the remote-container when a user leaves the channel
client.on('peer-leave', function(evt) {
  console.log('Remote stream has left the channel: ' + evt.uid);
  evt.stream.stop(); // stop the stream
});

// show mute icon whenever a remote has muted their mic
client.on('mute-audio', function (evt) {
  var remoteId = evt.uid;
});

client.on('unmute-audio', function (evt) {
  var remoteId = evt.uid;
});

// show user icon whenever a remote has disabled their video
client.on('mute-video', function (evt) {
  var remoteId = evt.uid;
});

client.on('unmute-video', function (evt) {
  var remoteId = evt.uid;
});

// ingested live stream
client.on('streamInjectedStatus', function (evt) {
  console.log("Injected Stream Status Updated");
  // evt.stream.play('full-screen-video');
  console.log(JSON.stringify(evt));
});

// join a channel
function joinChannel() {
  var token = generateToken();

  // set the role
  client.setClientRole('audience', function() {
    console.log('Client role set to audience');
  }, function(e) {
    console.log('setClientRole failed', e);
  });

  client.join(token, channelName, 0, function(uid) {
      console.log('User ' + uid + ' join channel successfully');
      $("#loader").hide();
      $("#loading-stream-text").hide();
      $("#stream-failed-button").show();
  }, function(err) {
      console.log('[ERROR] : join channel failed', err);
  });


}

function leaveChannel() {
  client.leave(function() {
    console.log('client leaves channel');
      joinStream(false);
      $("#watch-live-overlay").show();
  }, function(err) {
    console.log('client leave failed ', err); //error handling
  });
}

// use tokens for added security
function generateToken() {
  return null; // TODO: add a token generation
}


// REMOTE STREAMS UI
function addRemoteStreamMiniView(remoteStream){
  var streamId = remoteStream.getId();
  // append the remote stream template to #remote-streams
  $('#external-broadcasts-container').append(
    $('<div/>', {'id': streamId + '_container',  'class': 'remote-stream-container col'}).append(
      $('<div/>', {'id': streamId + '_mute', 'class': 'mute-overlay'}).append(
          $('<i/>', {'class': 'fas fa-microphone-slash'})
      ),
      $('<div/>', {'id': streamId + '_no-video', 'class': 'no-video-overlay text-center'}).append(
        $('<i/>', {'class': 'fas fa-user'})
      ),
      $('<div/>', {'id': 'agora_remote_' + streamId, 'class': 'remote-video'})
    )
  );
  remoteStream.play('agora_remote_' + streamId);

  var containerId = '#' + streamId + '_container';
  $(containerId).dblclick(function() {
    client.removeInjectStreamUrl(injectedStreamURL);
    $(containerId).remove();
  });
}

function getStreamId(agoraStreamId){
    streamId = agoraStreamId;
    console.log("stream_id " + streamId);
    return streamId;
}

mainContainer.on("container:start", function( event,streamId,arg2 ) {
    console.log(event);
    console.log(`Container started... argument1 ${streamId} argument2 ${arg2}`);

    setTimeout(function(){
        joinStream(streamId,arg2)
    },5000)
});

mainContainer.on("container:stop", function( event,streamId,arg2 ) {
    console.log(event);
    console.log(`Stream stopped... argument1 ${streamId} argument2 ${arg2}`);

    setTimeout(function(){
        // stopStream(streamId)
    },3000)
});

function joinStream(stream_id,joined = true){
    const url = "/join-stream";
    const data = {
        channel_id: channelId,
        agora_stream_id: stream_id,
        joined : joined
    };

    fetch(url, {
        method: 'POST', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}
