class JoinASchoolBox {

    constructor(external){
        //super();
        this.ext            = external;
        this.getPostData    = "";
        this.institution_id = "";
        this.school_id      = "";
        this.department_id  = "";
        this.course_id      = "";
        this.class_id       = "";

    }

    courseList(onchange = false, fromCall = '', selected = '') {

    }

    classList(onchange = false, fromCall = '',selected = ''){
        commonObj.paceRestart('classList');

        var ids				= this.ext.jsId;
        $(ids.classDiv).show();
        var jsClass			= this.ext.jsClass;
        var jsValue			= this.ext.jsValue;
        var extra			= this.ext.extra;
        var url			    = this.ext.extra.url;
        var selectedVal    	= jsValue.classVal;
        var searchVal       = this.getSearchVal(onchange,fromCall);
        var self = this;

        $(ids.classDiv).show();
        if(selected){
            selectedVal = selected;
        }

        var postData    = {
            'selectedId':selectedVal,
            'searchVal' : searchVal,
            'fromCall'  : fromCall,
            'onchange'  : onchange
        };

        $(ids.classId).html(commonObj.createOptions({},selectedVal,'Subject'));
        $(ids.classId).show();
        /* this.getPostData = this.post(url.getClassOptions,postData); */

        axios.post(url.getClassOptions,postData)
            .then(function (response) {

                var getData = response.data;
                //self.disableUndisable(getData.optionData);
                $.when($(ids.classId).html(commonObj.createOptions(getData.classes,selectedVal,"Subject"))).done(function(){
                    if(getData.optionData.selectedId != '' && int(getData.optionData.selectedId)){
                        $(ids.classId).val(getData.optionData.selectedId);
                    }

                });

            })
            .catch(function (error) {
                //console.log(error);
                //var re = {"status" : error.response.status, "message" : `Error Code ${error.response.status} : ${error.response.statusText}`};
                //commonObj.catchErr(re);
            });

    }
}

if (typeof joinSchoolObj != "undefined") {
    var JoinASchool = new JoinASchoolBox(joinSchoolObj);
}
if (typeof joinSchoolObj2 != "undefined") {
    var JoinASchool2 = new JoinASchoolBox(joinSchoolObj2);
}
