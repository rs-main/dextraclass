/* import {Common} from "/js/common.js"; */
class LoginClass{

		constructor(external){
            /* super(); */
			this.ext            = external;

		}

		loginUser(att){
			var ids				= this.ext.jsId;
			var jsClass			= this.ext.jsClass;
			var extra			= this.ext.extra;
			var url			    = this.ext.extra.url;
			var returnUrl       = $(ids.returnUrl).val();
			var reloadIt        = $(ids.reloadIt).val();
			var userNameVal     = $(ids.userName).val();
			var userPasswordVal = $(ids.userPassword).val();
			var self = this;
			var postUrl  = url.loginUrl;

			commonObj.btnDesEnb(ids.LoginBtn,"Login",'des');

			var postData    = {
								'returnUrl':returnUrl,
								'reloadIt':reloadIt,
								'username':userNameVal,
								'password':userPasswordVal
								};

			axios.post(postUrl,postData)
			.then(function (response) {
				//commonObj.btnDesEnb(ids.LoginBtn,"Login",'enb');
				var getData = response.data;

				if(!getData.errStatus)
				{
				    commonObj.messages(getData.messageType,getData.message);
				}else {
					commonObj.messages(getData.messageType,getData.message);
					if(getData.isAdmin == 1){
                        window.location.href = getData.adminUrl;
                    }else{
                        var urlRe = self.ext.parentUrl+returnUrl
                        window.location.href = urlRe;
                    }
				}

			})
			.catch(function (error) {
				commonObj.btnDesEnb(ids.LoginBtn,"Login",'enb');
			});


		}

		signUpUser(att){
        var ids				= this.ext.jsId;
        var jsClass			= this.ext.jsClass;
        var extra			= this.ext.extra;
        var url			    = this.ext.extra.url;
        var returnUrl       = $(ids.returnUrl).val();
        var signUpEmail     = $(ids.signUpEmail).val();
        var signUpPassword  = $(ids.signUpPassword).val();
        var signUpName      = $(ids.signUpName).val();
        var signUpUserName      = $(ids.signUpUserName).val();
        var signUpMobile      = $(ids.signUpMobile).val();
        var self = this;
        var postUrl  = url.signUpUrl;

        commonObj.btnDesEnb(ids.signUpBtnTop,"Login",'des');

        var postData    = {
            'returnUrl':returnUrl,
            'email'  :signUpEmail,
            'password':signUpPassword,
            'username': signUpUserName,
            'name' :signUpName,
            'mobile':signUpMobile,
            "tutor" : $("#tutorCheckbox").is(":checked"),
            "termsCheckBox": $("#termsCheckBox").is(":checked") ? true: "",
        };

        axios.post(postUrl,postData)
            .then(function (response) {
                var getData = response.data;
                console.log( response)

                commonObj.messages("success","Successfully created an account!");

                setTimeout(function (){
                    location.reload();
                },2000);

            })
            .catch(function (error) {
                if (error.response) {
                    let errors = error.response.data.errors;
                    for (const property in errors) {
                        console.log(`${property} ${errors[property]}` );
                        commonObj.messages("error",errors[property]);
                    }
                }
                commonObj.btnDesEnb(ids.signUpBtnTop,"Sign Up",'enb');
            });


    }

        post(url,postData){

            var responseData = axios.post(url,postData)
			.then(function (response) {
				return response;
			})
			.catch(function (error) {
                var re = {"status" : error.response.status, "message" : `Error Code ${error.response.status} : ${error.response.statusText}`};
				return re;
			});

            this.getPostData = responseData;
        }

		openPopup(returnUrl = "",reloadIt = ""){

			if(returnUrl == "" && $(this.ext.jsId.activeTabInput).val())
			{
			 var v = $(this.ext.jsId.activeTabInput).val();
			 //returnUrl = `?active=${v}`;
			}
			/* console.log(1,returnUrl); */
			$(this.ext.jsId.loginPopUpDiv).toggle(300);
			$(this.ext.jsId.returnUrl).val(returnUrl);
			$(this.ext.jsId.reloadIt).val(reloadIt);
		}


}

var loginObj = new LoginClass(loginClassObj);
/* export {LoginClass} */
