import {Common} from "/js/common.js";
class Student extends Common{

		constructor(external){
            super();
			this.ext            = external;
			this.studentHistory();

		}
		async studentHistory(tab = '',loadMore = 0,ex = 0){

			var ids				= this.ext.jsId;
			var jsClass			= this.ext.jsClass;
			var jsData			= this.ext.jsData;
			var jsValue			= this.ext.jsValue;
			var extra			= this.ext.extra;
			var url			    = this.ext.extra.url;

			if(tab == ''){
				tab = $(ids.activeTabInput).val();
			} else {
				$(ids.activeTabInput).val(tab);
			}

			var setLoder = "";
			var MoreId = "";
			var postUrl  = url.histories;
			var pageNo   = 1;

			if(tab == 'histories') {
				setLoder      = ids.historyHtml;
				postUrl       = url.histories;
				MoreId        = ids.historyMore;
				pageNo        = $(ids.historyPage).val();
				if($(ids.activetedTab).data(jsData.histories) && loadMore == 0 && ex == 0){
					return false;
				}else{
					$(ids.activetedTab).data(jsData.histories,1)
				}
			}
			else if(tab == 'favourites') {
				setLoder      = ids.favouritesHtml;
				postUrl       = url.favourites;
				MoreId        = ids.favouritesMore;
				pageNo        = $(ids.favouritesPage).val();
				if($(ids.activetedTab).data(jsData.favourites) && loadMore == 0 && ex == 0){
					return false;
				}else{
					$(ids.activetedTab).data(jsData.favourites,1)
				}
			}
            else if(tab == 'bag') {
                setLoder      = ids.bagHtml;
                postUrl       = url.bag;
                MoreId        = ids.bagMore;
                pageNo        = $(ids.bagPage).val();
                if($(ids.activetedTab).data(jsData.bag) && loadMore == 0 && ex == 0){
                    return false;
                }else{
                    $(ids.activetedTab).data(jsData.favourites,1)
                }
            }

			$(MoreId).hide();

			if(loadMore == 1){
				$(setLoder).append(this.bootLoder());
				 pageNo   = pageNo;
			} else {
				$(setLoder).html(this.bootLoder());
				pageNo   = 1;
			}

			var school_id = $(ids.school_id).val();
			var course_id = $(ids.course_id).val();

			var postData    = {
								'tab':tab,
								'page':pageNo,
								'school_id':school_id,
								'course_id':course_id,
								'loadMore':loadMore
								};


            await this.post(postUrl,postData);
			try {
				if(this.getPostData.status != this.ext.status.success) throw this.getPostData.message;
				var getData = this.getPostData.data;

				setTimeout(function(){
					$("#loderId").remove();
					var htmlId = "";
					if(getData.tab == 'histories'){
						htmlId = ids.historyHtml;
						$(ids.historyPage).val(getData.page);
						if(getData.show_morerecords == 1){
							$(ids.historyMore).show('slow');
						} else {
							$(ids.historyMore).hide('slow');
						}
					} else if(getData.tab == 'favourites'){
						htmlId = ids.favouritesHtml;
						$(ids.favouritesPage).val(getData.page);
						if(getData.show_morerecords == 1){
							$(ids.favouritesMore).show('slow');
						} else {
							$(ids.favouritesMore).hide('slow');
						}

					}
                    else if(getData.tab == 'bag') {
                        htmlId = ids.bagHtml;
                        $(ids.bagPage).val(getData.page);
                        if (getData.show_morerecords == 1) {
                            $(ids.bagMore).show('slow');
                        } else {
                            $(ids.bagMore).hide('slow');
                        }
                    }

                    if (getData.loadMore == 1) {
                        $.when($(htmlId).append(getData.resultHtml)).done();
                    } else {
                        $.when($(htmlId).html(getData.resultHtml)).done();
                    }



				}, 500);

			}
			catch(err) {
				console.log(err);
			}

		}
		async removeStudentHistory(id,removeFrom = 'history'){
			var ids				= this.ext.jsId;
			var jsClass			= this.ext.jsClass;
			var jsValue			= this.ext.jsValue;
			var extra			= this.ext.extra;
			var url			    = this.ext.extra.url;

			if(!confirm("Are you sure want to remove this!"))
			{
				return false;
			}

			var postUrl  = url.rmHistories;
			var rowId = '';
			if(removeFrom == 'favourite'){
			 $(this.ext.createId([ids.btnRmFav,id])).html(this.iconLoder());
			  rowId = this.ext.createId([ids.favRow,id]);
			}else{
			 $(this.ext.createId([ids.btnRmHistory,id])).html(this.iconLoder());
			  rowId = this.ext.createId([ids.historyRow,id]);
			}


			var postData    = {
								'id':id,
								'rmFrom':removeFrom
								};


            await this.post(postUrl,postData);
			try {
				if(this.getPostData.status != this.ext.status.success) throw this.getPostData.message;
				var getData = this.getPostData.data;

				setTimeout(function(){
				 $.when($(rowId).hide('slow')).done($(rowId).remove());

				}, 500);

			}
			catch(err) {
				console.log(err);
			}
			if(removeFrom == 'favourite'){
				if(this.rowCount(jsClass.favRowCount) <= 1)
				{
				  this.studentHistory('',0,1);
				}
			}else{
				if(this.rowCount(jsClass.historyRowCount) <= 1)
				{
				  this.studentHistory('',0,1);
				}
			}
		}
        async post(url,postData){

            var responseData = await axios.post(url,postData)
			.then(function (response) {
				return response;
			})
			.catch(function (error) {
                var re = {"status" : error.response.status, "message" : `Error Code ${error.response.status} : ${error.response.statusText}`};
				return re;
			});

            this.getPostData = responseData;
        }

        async downloadLibraryNotes(notes_id){
        var self = this;
        var ids				= this.ext.jsId;
        var jsClass			= this.ext.jsClass;
        var extra			= this.ext.extra;
        var url			    = this.ext.extra.url;
        var btnName         = 'DOWNLOAD';
        commonObj.paceRestart('downloadLibraryNotes');
        if(!notes_id){
            return false;
        }
        commonObj.btnDesEnb(ids.libraryNotesBtnId+''+notes_id,btnName,'des');
        var postUrl  = url.studentDownloads;

        var postData    = {
            'notes_id'	:notes_id
        };


        var responseData = axios.get(postUrl,{ params: postData })
            .then(function (response) {

                commonObj.btnDesEnb(ids.libraryNotesBtnId+''+notes_id, btnName,'enb');

                var getData = response.data;

                if(getData.isLogin == 0){

                    commonObj.openLoginModel();
                    commonObj.messages(getData.messageType,getData.message);
                }
                if(!getData.errStatus)
                {
                    commonObj.message(getData.messageType,getData.message);
                }else {
                    commonObj.messages(getData.messageType,getData.message);
                    var downloadUrl = $(ids.downloadLibraryNoteInput+''+notes_id).val();
                    window.open(downloadUrl,'_blank');
                }

            })
            .catch(function (error) {
                commonObj.btnDesEnb(ids.libraryNotesBtnId+''+notes_id,btnName,'enb');
                commonObj.catchErr(error);
            });



    }
        async removeFromUserLibrary(notes_id){
            var self = this;
            var ids				= this.ext.jsId;
            var jsClass			= this.ext.jsClass;
            var extra			= this.ext.extra;
            var url			    = this.ext.extra.url;
            commonObj.paceRestart('removeFromUserLibrary');

            if(!notes_id){
                return false;
            }
            var postUrl  = url.removeFromUserLibrary;

            var postData    = {
                'notes_id'	:notes_id
            };

            var responseData = axios.get(postUrl,{ params: postData })
                .then(function (response) {

                    var getData = response.data;

                    if(getData.isLogin == 0){
                        commonObj.openLoginModel();
                        commonObj.messages(getData.messageType,getData.message);
                    }
                    else{
                        if(!getData.errStatus)
                        {
                            commonObj.message(getData.messageType,getData.message);
                        }else {
                            commonObj.messages(getData.messageType,getData.message);
                        }


                        var setLoder      = ids.bagHtml;
                        var postUrl       = url.bag;
                        var MoreId        = ids.bagMore;
                        var pageNo        = $(ids.bagPage).val();
                        var loadMore = 0;

                        $(MoreId).hide();

                        if(loadMore == 1){
                            $(setLoder).append(self.bootLoder());
                            pageNo   = pageNo;
                        } else {
                            $(setLoder).html(self.bootLoder());
                            pageNo   = 1;
                        }


                        var postData    = {
                            'tab': 'bag',
                            'page':pageNo,
                            'loadMore':loadMore
                        };


                        var responseData = axios.post(postUrl,{params: postData})
                            .then(function (response) {
                                var getPostData =  response;
                                if(getPostData.status != self.ext.status.success) throw getPostData.message;
                                var getData = getPostData.data;

                                setTimeout(function(){
                                    $("#loderId").remove();
                                    var htmlId = ids.bagHtml;
                                    $(ids.bagPage).val(getData.page);
                                    if (getData.show_morerecords == 1) {
                                        $(ids.bagMore).show('slow');
                                    } else {
                                        $(ids.bagMore).hide('slow');
                                    }

                                    if (getData.loadMore == 1) {
                                        $.when($(htmlId).append(getData.resultHtml)).done();
                                    } else {
                                        $.when($(htmlId).html(getData.resultHtml)).done();
                                    }

                                }, 500);

                            })
                            .catch(function (error) {
                                console.error(error);
                                commonObj.catchErr(error);
                            });


                        // this.post(postUrl,postData);
                        // try {
                        //     alert('again');
                        //     if(this.getPostData.status != this.ext.status.success) throw this.getPostData.message;
                        //     var getData = this.getPostData.data;
                        //
                        //     setTimeout(function(){
                        //         alert('done');
                        //         $("#loderId").remove();
                        //         var htmlId = ids.bagHtml;
                        //         $(ids.bagPage).val(getData.page);
                        //         if (getData.show_morerecords == 1) {
                        //             $(ids.bagMore).show('slow');
                        //         } else {
                        //             $(ids.bagMore).hide('slow');
                        //         }
                        //
                        //         if (getData.loadMore == 1) {
                        //             $.when($(htmlId).append(getData.resultHtml)).done();
                        //         } else {
                        //             $.when($(htmlId).html(getData.resultHtml)).done();
                        //         }
                        //
                        //     }, 500);
                        //
                        // }
                        // catch(err) {
                        //     console.log(err);
                        // }
                    }


                })
                .catch(function (error) {
                    commonObj.catchErr(error);
                });


        }
        filterFiletype(tab = 'bag', loadMore = 0, ex = 0, selectedValue){
            commonObj.paceRestart('filterFiletype');

            var self = this;
            var ids				= self.ext.jsId;
            var jsClass			= self.ext.jsClass;
            var jsValue			= self.ext.jsValue;
            var extra			= self.ext.extra;
            var url			    = self.ext.extra.url;
            var jsData		    = self.ext.jsData;

            var setLoder      = ids.bagHtml;
            var postUrl       = url.filterUserLibrary;
            var htmlId        = ids.bagHtml;
            var MoreId        = ids.bagMore;
            var pageNo        = $(ids.bagPage).val();


            if(loadMore == 1){
                $(setLoder).append(commonObj.bootLoder());
            } else {
                $(setLoder).html(commonObj.bootLoder());
                pageNo   = 1;
            }
            $(MoreId).hide();
            var school_id = $(ids.school_id).val();
            var course_id = $(ids.course_id).val();

            var postData    = {
                'tab':tab,
                'page':pageNo,
                'school_id':school_id,
                'course_id':course_id,
                'loadMore':loadMore,
                'type': selectedValue.value
            };


        //this.get(postUrl,postData);

        var responseData = axios.get(postUrl,{ params: postData })
            .then(function (response) {

                var getData = response.data;
                var pass = self;
                setTimeout(function(){
                    $("#loderId").remove();
                    var htmlId = "";

                    htmlId = ids.bagHtml;
                    $(ids.bagPage).val(getData.page);
                    if (getData.show_morerecords == 1) {
                        $(ids.bagMore).show('slow');
                    } else {
                        $(ids.bagMore).hide('slow');
                    }

                    if (getData.loadMore == 1) {
                        $.when($(htmlId).append(getData.resultHtml)).done();
                    } else {
                        $.when($(htmlId).html(getData.resultHtml)).done();
                    }

                }, 500,self);


            })
            .catch(function (error) {
                commonObj.catchErr(error);
            });
    }

    filterFileRecent(tab = 'bag', loadMore = 0, ex = 0, selectedValue){
        commonObj.paceRestart('filterFileRecent');

        var self = this;
        var ids				= self.ext.jsId;
        var jsClass			= self.ext.jsClass;
        var jsValue			= self.ext.jsValue;
        var extra			= self.ext.extra;
        var url			    = self.ext.extra.url;
        var jsData		    = self.ext.jsData;

        var setLoder      = ids.bagHtml;
        var postUrl       = url.filterUserRecent;
        var htmlId        = ids.bagHtml;
        var MoreId        = ids.bagMore;
        var pageNo        = $(ids.bagPage).val();


        if(loadMore == 1){
            $(setLoder).append(commonObj.bootLoder());
        } else {
            $(setLoder).html(commonObj.bootLoder());
            pageNo   = 1;
        }
        $(MoreId).hide();
        var school_id = $(ids.school_id).val();
        var course_id = $(ids.course_id).val();

        var postData    = {
            'tab':tab,
            'page':pageNo,
            'school_id':school_id,
            'course_id':course_id,
            'loadMore':loadMore,
            'type': selectedValue.value
        };

        var responseData = axios.get(postUrl,{ params: postData })
            .then(function (response) {

                var getData = response.data;
                var pass = self;
                setTimeout(function(){
                    $("#loderId").remove();
                    var htmlId = "";

                    htmlId = ids.bagHtml;
                    $(ids.bagPage).val(getData.page);
                    if (getData.show_morerecords == 1) {
                        $(ids.bagMore).show('slow');
                    } else {
                        $(ids.bagMore).hide('slow');
                    }

                    if (getData.loadMore == 1) {
                        $.when($(htmlId).append(getData.resultHtml)).done();
                    } else {
                        $.when($(htmlId).html(getData.resultHtml)).done();
                    }

                }, 500,self);


            })
            .catch(function (error) {
                commonObj.catchErr(error);
            });
    }


}

export {Student}
