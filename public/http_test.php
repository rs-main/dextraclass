<?php

use Workerman\Worker;
require_once __DIR__ . 'Workerman/Autoloader.php';


$http_worker = new \Workerman\Worker("http://0.0.0.0:2345");

// start the process of external services 4
$http_worker->count = 4;

//    Reply hello world to the browser // when receiving the data sent by the browser
$http_worker->onMessage = function($connection, $data)
{
    // hello world send to the browser
    $connection->send('hello world');
};

\Workerman\Worker::runAll();
