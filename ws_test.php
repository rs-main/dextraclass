
<?php
use Workerman\Worker;
//require_once __DIR__ . '/Workerman/Autoloader.php';
require_once __DIR__ . '/vendor/autoload.php';


// Note: This is different from the previous example, using a protocol websocket
$ws_worker = new Worker("websocket://0.0.0.0:2000");

// start the process of external services 4
$ws_worker->count = 4;

// When receiving the data sent by the client hello $ data returned to the client
$ws_worker->onMessage = function($connection, $data)
{
    // hello $ data is sent to the client
    $connection->send($data);
};



// run worker
if (!defined('GLOBAL_START')) {
    Worker::runAll();
}
