<?php

namespace App;

use App\Models\Video;
use Illuminate\Database\Eloquent\Model;

class ProfessionalCategory extends Model
{
    //

    protected $table = "professional_school_categories";

    protected $guarded = [];

    public function courseCategory(){
        return $this->hasMany(CourseCategory::class,"category_id");
    }

    public static function getCategoryVideosCount($id){
        return Video::leftJoin("course_category","course_category.course_id","=","videos.course_id")
            ->leftJoin("professional_school_categories","professional_school_categories.id","=","course_category.category_id")
            ->where("professional_school_categories.id",$id)
            ->count();
    }
}
