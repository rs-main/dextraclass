<?php

namespace App;

use App\Models\QuestionBank;
use Illuminate\Database\Eloquent\Model;

class PastQuestionBank extends Model
{
    protected $table = "past_question_bank";

    protected $guarded = [];

    /**
     *
     */

}
