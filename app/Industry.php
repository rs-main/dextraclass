<?php

namespace App;

use App\Models\Video;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    //
    protected $table= "industries";


    protected $guarded = [];

    public function categories(){
        return $this->hasMany(ProfessionalCategory::class);
    }

    public static function getIndustryCourses($id){
        return MainCourse::leftJoin("course_category","course_category.course_id","=","courses.id")
            ->leftJoin("professional_school_categories","professional_school_categories.id","=","course_category.category_id")
            ->leftJoin("industries","industries.id","=","professional_school_categories.industry_id")
            ->where("industries.id",$id)
            ->count();
    }

    public static function getIndustryVideosCount($id){
        return Video::leftJoin("course_category","course_category.course_id","=","videos.course_id")
            ->leftJoin("professional_school_categories","professional_school_categories.id","=","course_category.category_id")
            ->leftJoin("industries","industries.id","=","professional_school_categories.industry_id")
            ->where("industries.id",$id)
            ->count();
    }

}
