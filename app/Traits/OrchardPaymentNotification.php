<?php

namespace App\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

trait OrchardPaymentNotification {
/**
 * @param mixed $array
 *
 * @return void
 */

 private static $SECRET_KEY  = env('SECRET_KEY', '');
 private static $CLIENT_KEY = env('CLIENT_KEY', '');
 private static $SERVICE_ID  = env('SERVICE_ID', '');


 function makePaymentRequest($customer_number, $amount, $reference, $nw, $voucher_code)
 {
     
     
        $service_url = 'https://orchard-api.anmgw.com/sendRequest';
  
        $exttrid = getGUIDnoHash();

        $callback_url = '';
        
        $timestamp = date('YYYY-MM-DD H:M:S', time());
        
        $data = array(
        'service_id' => $SERVICE_ID, 
        'trans_type' => 'CTM',
        'customer_number'=>$customer_number,
        'amount' => $amount,
        'exttrid' => $exttrid,
        'reference' => $reference,
        'nw' => $nw,
        'callback_url' => $callback_url,
        'ts' => $timestamp,
        'voucher_code' => $voucher_code,
        );
        
        $data_string = json_encode($data);
        $signature =  hash_hmac( 'sha256' , $data_string , $SECRET_KEY );
        $auth = $CLIENT_KEY.':'.$signature;
        
        $ch = curl_init($service_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: '.$auth,
        'Content-Type: application/json',
        'timeout: 180',
        'open_timeout: 180'
        )
        );
        
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        $response = preg_replace('/\\\\\"/',"\"", $result);
        
        if(stristr($response['message'], "Success") != FALSE){
            return true;
        }
        else{
            return false;
        }
     
     
    /*$username = 'dev@XtraClass';
    $password = 'Mywayisgr8!';
    $from = "XtraClass";
    $baseurl = "https://isms.wigalsolutions.com/ismsweb/sendmsg/";
    $to = $number;
    $params = "username=".$username."&password=".$password."&from=".$from."&to=".$to."&message=".$message;
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$baseurl);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
    $return = curl_exec($ch);
    curl_close($ch);
    $send = explode(" :: ",$return);
    if(stristr($send[0], "SUCCESS") != FALSE){
        return true;
    }
    else{
        return false;
    }*/
}

function getGUIDnoHash(){
            mt_srand((double)microtime()*10000);
            $charid = md5(uniqid(rand(), true));
            $c = unpack("C*",$charid);
            $c = implode("",$c);

            return substr($c,0,20);
    }


}

?>
