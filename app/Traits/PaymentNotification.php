<?php

namespace App\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

trait PaymentNotification {
    /**
     * @param mixed $array
     *
     * @return void
     */

     private static $API_KEY = '';
     private static $N_USERNAME = '';
     private static $N_PASS = '';
     private static $SENDER_ID = '';

    public function performRequest($method, $to, $text, $formParams=[], $headers=[])
    {
        $requestUrl = "https://api";

        $client = new Client();
        // $headers = ['Content-Type' => 'application/json;charset=UTF-8'];
        $headers = ['Content-Type' => 'application/json'];
        $response = $client->request($method, $requestUrl, ['form_param' => $formParams, 'headers' => $headers]);

        return $response->getBody()->getContents();

    }

    public function performPaymentRequest($method, $formParams=[], $headers=[])
    {
        $requestUrl = "https://api.flutterwave.com/v3/charges?type=mobile_money_ghana";

        $client = new Client();
        // $headers = ['Content-Type' => 'application/json;charset=UTF-8'];
        $headers = ['Content-Type' => 'application/json'];
        // $headers = ['Authorization' => 'Bearer FLWSECK_TEST-82c161910086df6f7bdc45857b24f6c1-X'];
        $headers = ['Authorization' => 'Bearer FLWSECK-ef0007f5fe490339e93366a1e498b1b0-X'];
        $response = $client->post($requestUrl, ['json' => $formParams, 'headers' => $headers]);

        return $response->getBody()->getContents();

    }


    public function verifyPaymentRequest($method, $formParams=[], $txnId = '')
    {
        $requestUrl = "https://api.flutterwave.com/v3/transactions/$txnId/verify";

        $client = new Client();
        // $headers = ['Content-Type' => 'application/json;charset=UTF-8'];
        $headers = ['Content-Type' => 'application/json'];
        // $headers = ['Authorization' => 'Bearer FLWSECK_TEST-82c161910086df6f7bdc45857b24f6c1-X'];
        $headers = ['Authorization' => 'Bearer FLWSECK-ef0007f5fe490339e93366a1e498b1b0-X'];
        $response = $client->get($requestUrl, ['headers' => $headers]);

        return $response->getBody()->getContents();

    }
}

?>
