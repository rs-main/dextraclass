<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolProgrammes extends Model
{
    protected $table = "school_programmes";
    protected $guarded = [];

}
