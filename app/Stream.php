<?php

namespace App;

use App\Models\Classes;
use App\Models\Tutor;
use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{
    //
    protected $guarded = [];

    protected $casts = [
        "created_at" => "string"
    ];

//    protected $with = ['tutor',"class"];

    public function tutor(){
        return $this->belongsTo(Tutor::class);
    }

    public function class(){
        return $this->belongsTo(Classes::class);
    }

}
