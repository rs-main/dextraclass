<?php

namespace App;

use App\Models\Course;
use Illuminate\Database\Eloquent\Model;

class ProfessionalSchoolCategory extends Model
{
    protected $table = "professional_school_categories";

    protected $guarded = [];

    public function courseCategory(){
        return $this->belongsTo(CourseCategory::class,"category_id");
    }

    public static function CategoryCoursesCount($id){
        return Course::leftJoin("course_category","course_category.course_id","=","courses.id")
            ->where("course_category.category_id",$id)
            ->selectRaw("course_category.id")
            ->count();
    }
}
