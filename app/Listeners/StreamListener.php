<?php

namespace App\Listeners;

use App\Events\StreamingEvent;
use App\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StreamListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StreamingEvent  $event
     * @return void
     */
    public function handle(StreamingEvent $event)
    {
        if($event->type == "started"){
            $title = "Streaming session has just started!";
            $message = $event->clazz. " has started streaming ".$event->subject;
            $this->createNotification($title,$message);
        }else {
            $title = "Streaming session has just ended";
            $message = $event->clazz. " has ended streaming ".$event->subject;
            $this->createNotification($title,$message);
        }
    }


    public function createNotification( $title, $message): void
    {
        Notification::create([
            "title" => $title,
            "message" => $message,
            "read" => 0,
            "owner" => "\App\Stream"
        ]);
    }
}
