<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SidStudent extends Model
{
    //

    protected $table = "sid_students";

    protected $guarded = [];

    protected $casts = [
        "info" => "array"
    ];
}
