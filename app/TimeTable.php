<?php

namespace App;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class TimeTable extends Model
{
    use Uuids;
    protected $table = "time_tables";

    protected $guarded = [];

}
