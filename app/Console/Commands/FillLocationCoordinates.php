<?php

namespace App\Console\Commands;

use App\Location;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FillLocationCoordinates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:coordinates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill Coordinates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $locationCollection = new \Illuminate\Support\Collection();

        DB::table('locations')->where("filled",false)->orderBy('id')->chunk(10, function ($locations) {
            foreach ($locations as $location) {
                $queryString = http_build_query([
                    'access_key' => '838a40e225516f0b8ddff4821b98e00f',
                    'query' => $location->name.",Ghana",
                    'output' => 'json',
                    'limit' => 1,
                ]);

                $ch = curl_init(sprintf('%s?%s', 'http://api.positionstack.com/v1/forward', $queryString));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $json = curl_exec($ch);

                curl_close($ch);

                $apiResult = json_decode($json, true);
                $latitude = isset($apiResult["data"][0]["latitude"]) ? $apiResult["data"][0]["latitude"] : "";
                $longitude = isset($apiResult["data"][0]["longitude"]) ? $apiResult["data"][0]["longitude"] : "";
                $coordinates = "$latitude,$longitude";
                if (!empty($coordinates)) {
                    Location::find($location->id)->update([
                        "coordinates" => $coordinates,
                        "filled" => true
                    ]);
                }
                $this->comment("Location: " . $location->name  ." => " . PHP_EOL);
                $this->comment("Coordinates: " . $coordinates ." => " . PHP_EOL);

            }
        });
    }
}
