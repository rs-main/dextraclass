<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolMode extends Model
{
    //

    protected $table = "delivery_modes";

    protected $guarded = [];
}
