<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendMailToStudent extends Mailable
{
    //use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$data = $this->data;
        $message = $this->from($data->from_email, $data->from_name)->subject($data->subject)->view('emails.student.sendEmailApiTpl', compact('data'));
        if ($data->attachment) {
            $message->attachFromStorageDisk($data->attachment['disk'], $data->attachment['path']);
        }
        return $message;
    }
}
