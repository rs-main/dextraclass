<?php

namespace App;

use App\Models\QuestionBank;
use Illuminate\Database\Eloquent\Model;

class PastQuestion extends Model
{

    protected $table = "past_questions";

    protected $guarded = [];

    /**
     *
     */
    public function questions()
    {
        return $this->belongsTo(QuestionBank::class, 'question_bank_id');
    }

    public function group(){
        return $this->belongsTo(PastQuestionGroup::class,"past_question_group_id");
    }
}
