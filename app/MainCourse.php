<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCourse extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = "courses";

    protected $guarded = [];
}
