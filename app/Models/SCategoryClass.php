<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SCategoryClass extends Model
{
    protected $table = 'scategory_class';
    protected $fillable = ['scategory_id','scategory_uuid','class_id','class_uuid'];


    public function class()
    {
        return $this->belongsTo(SClass::class);
    }
}
