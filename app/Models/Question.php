<?php

namespace App\Models;

use App\Models\Student;
use Emadadly\LaravelUuid\Uuids;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    use Uuids;

    protected $appends = ['student_name'];

    public function tutor()
    {
        return $this->hasOne(Tutor::Class, 'user_id', 'sender_id')->withDefault();
    }

    public function student()
    {
        return $this->hasOne(Student::Class, 'user_id', 'sender_id')->withDefault();
    }

    public function getStudentNameAttribute()
    {
        $student = DB::table('students')->select(['username', 'first_name', 'last_name'])->where('id', $this->sender_id)->first();
        // $student = Student::where('id', $this->sender_id)->get();
        return $student;
    }

    public function replies()
    {
        return $this->hasMany(QuestionReply::class);
    }


	public function user()
    {
        return $this->hasOne('App\User', 'id', 'sender_id')->withDefault();
    }

	public function childrenAccounts()
	{
		return $this->hasMany(Question::class, 'parent_id', 'id');
	}

	public function allChildrenAccounts()
	{
		return $this->childrenAccounts()->with('allChildrenAccounts');
	}

}
