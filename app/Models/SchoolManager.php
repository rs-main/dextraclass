<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Emadadly\LaravelUuid\Uuids;
use App\User;

class SchoolManager extends Model
{

    use Uuids;

	use SoftDeletes;

    protected $dates = ['deleted_at'];

    public $table = 'school_managers';

    public function school()
    {
        return $this->hasOne(School::Class, 'id', 'school_id')->withDefault();
    }

    public function user_details()
    {
        return $this->hasOne(User::Class, 'id', 'user_id')->withDefault();
    }

    public function country_name()
    {
        return $this->hasOne(Countries::Class, 'phonecode', 'country')->withDefault();
    }

    public function avatar()
    {
        return $this->belongsTo(Avatar::class, 'avatar_id', 'id')->where('status', 1)->withDefault();
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getProfileOrAvatarImageAttribute()
    {
        /* userData->profile_or_avatar_image */
        if (!empty($this->profile_image) && file_exists(public_path('uploads/schools/managers/' . $this->profile_image))) {
            return 'uploads/schools/managers/' . $this->profile_image;
        }
        else{
            return User::DEFAULT_USER_PROFILE;
        }
    }
	
}