<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentQuiz extends Model
{
    protected $table = 'students_quiz';

    protected $fillable = ['user_id','student_id', 'quiz_id', 'subject_id', 'date_taken'];



    public function quiz_history()
    {
        return $this->hasMany(StudentQuizHistory::class, 'student_quiz_id');
    }

}
