<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentQuizHistory extends Model
{
    protected $table = 'student_quiz_history';

    protected $fillable = ['student_quiz_id','quiz_mark'];
}
