<?php

namespace App\Models;

use App\VideoDetail;
use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use SiteHelpers;

class Video extends Model
{

    use Uuids;

    use SoftDeletes;

    protected $guarded = [];

    /*
     * Get referenced record of class.
     */
    public function classDetail()
    {
        return $this->belongsTo(Classes::Class, 'class_id')->withDefault();
    }

    public function videoDetail()
    {
        return $this->belongsTo(VideoDetail::Class);
    }

    /*
     * Get referenced record of school.
     */
    public function school()
    {
        return $this->belongsTo(School::Class, 'school_id')->withDefault();
    }

    /*
     * Get referenced record of course.
     */
    public function course()
    {
        return $this->belongsTo(Course::Class, 'course_id')->withDefault();
    }

    public function classroom()
    {
        return $this->belongsTo(Classroom::Class, 'classroom_id')->withDefault();
    }

    /*
     * Get referenced record of subject.
     */
    public function subject()
    {
        return $this->belongsTo(Subject::Class, 'subject_id')->withDefault();
    }

    /*
     * Get referenced record of topic.
     */
    public function topic()
    {
        return $this->belongsTo(Topic::Class, 'topic_id')->withDefault();
    }

    /*
     * Get referenced record of period.
     */
    public function Period()
    {
        return $this->belongsTo(Period::Class, 'period_id')->withDefault();
    }

    /*
     * Get referenced record of tutor.
     */
    public function tutor()
    {
        return $this->belongsTo(Tutor::Class, 'tutor_id','user_id')->withDefault();
    }

    /*
     * Get referenced record from student favourite.
     */
    public function studentFavourite()
    {
        return $this->belongsTo(StudentFavourites::Class, 'video_id', 'id');
    }

    /*
     * Get referenced record of note.
     */
    public function note()
    {
        return $this->belongsTo(Note::Class, 'note_id');
    }

    /*
     * Get Title for a video from it's subject and topic name.
     */
    public function getTitleAttribute()
    {

        return "{$this->subject->subject_name} - {$this->topic->topic_name}";
    }

    /*
     * Get Sub-Title for a video from it's subject and topic name.
     */
    public function getSubTitleAttribute()
    {

        return "{$this->school->school_name} - {$this->course->name}";
    }

    /*
     * Get formated date from Play-On field.
     */
    public function playOn($format = 'd-m-Y')
    {
        return date($format, strtotime($this->play_on));
    }

    /*
     * Get formated video URL.
     */
    public function videoURL()
    {
        return 'https://player.vimeo.com/video/' . $this->video_id;
    }


    /*
     * Get formated note URL.
     */
    public function noteURL()
    {
        if(isset($this->note->file_url) && !empty($this->note->file_url)){
            $file_url = $this->note->file_url;

            if($this->note->storage == 's3') {
                $storage = 's3';
            } else {
                $storage = 'public';
            }

            if(Storage::disk($storage)->exists($file_url)){
                $url = Storage::disk($storage)->url($file_url);

                return $url;
            }
        }

        return '';
    }

    /*
     * Force Video Download.
     */
    public function mobileVideoURL($video_url)
    {
        $file_url = $video_url;
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url);
    }

    /*
     * Get size of note file.
     */
    public function noteFileSize()
    {
        if(isset($this->note->file_url) && !empty($this->note->file_url)){
            $file_url = $this->note->file_url;

            if($this->note->storage == 's3') {
                $storage = 's3';
            } else {
                $storage = 'public';
            }

            if(Storage::disk($storage)->exists($file_url)){
                $size = Storage::disk($storage)->size($file_url);

                return $size;
            }
        }

        return 0;
    }

    /*
     * Get size of note file.
     */
    public function noteFileName()
    {
        if(isset($this->note->file_url) && !empty($this->note->file_url)){

            return basename($this->note->file_url);
        }

        return '';
    }

    /*
     * Get size of note file.
     */
    public function videoFileName()
    {
        if(isset($this->mob_url) && !empty($this->mob_url)){

            return basename($this->mob_url);
        }

        return '';
    }

    /**
     * Gets a vimeo thumbnail url
     * @param mixed $id A vimeo id (ie. 1185346)
     * @return thumbnail's url
     */
    function getVimeoThumb() {
        $id = $this->video_id;
        if($id){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://vimeo.com/api/v2/video/$id.php");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            $response = curl_exec($ch);

            if(curl_errno($ch)){
                curl_close($ch);
                return '';
            } else {
                $output = @unserialize($response);
                $data = $output[0];
                if(isset($data['thumbnail_medium'])){
                    return $data['thumbnail_medium'];
                }
                curl_close($ch);
            }
        }

        return '';
    }

    public function Quiz(){
        return $this->belongsTo(QuestionQuiz::Class, 'topic_id','lesson_id');
    }

    public function checkQuiz (){
        return $this->belongsTo(QuestionQuiz::Class, 'subject_id','subject_id');

    }

    public function check_quiz_lesson ($subject_id){
        $details = QuestionQuiz::where('subject_id', '=', $subject_id )
            ->where('lesson_id', '!=', null)->count();
        return $details;
    }

    public function get_quiz_lesson ($subject_id){
        $details = QuestionQuiz::where('subject_id', '=', $subject_id )
            ->where('lesson_id', '!=', null)->first();
        return $details->lesson_id;
    }

    public function get_pass_mark($quiz_id){
        $quiz_details = QuestionQuiz::find($quiz_id);
        if(isset($quiz_details->pass_mark) && !empty($quiz_details->pass_mark)){
            return $quiz_details->pass_mark;
        }
        else{
            return '';
        }
    }

    public function get_student_mark($quiz_id, $subject_id){
        $answer_details = StudentQuiz::where(['quiz_id'=>$quiz_id, 'subject_id'=>$subject_id])
            ->orderBy('id', 'desc')->first();
        if ($answer_details) {
            $history_details = StudentQuizHistory::where('student_quiz_id', $answer_details->id)->pluck('quiz_mark');
            return $history_details[0];
        }

        return null;

    }

    public function getNextVideo($id,$class_id){
        return $this->where('id', '>', $id)->whereClassId($class_id)->min('id');
    }

}
