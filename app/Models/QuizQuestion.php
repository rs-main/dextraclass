<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\QuestionBank;

class QuizQuestion extends Model
{
    public $table = 'quiz_questions';
    protected $fillable  = [
        'question_bank_id',
        'q_quiz_id',
        'question_type'
    ];


    public function questions(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(QuestionBank::class, 'question_bank_id');
    }


}
