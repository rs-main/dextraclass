<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolCategory extends Model
{

    use SoftDeletes;

    use Uuids;

    protected $dates = ['deleted_at'];

    protected $casts = [ "uuid" => "string"];


    public function schools()
    {
        return $this->hasMany(School::class, 'school_category');
    }

    public function list_institutions()
    {
        $scholCates= [];
        $categorySchool= [];
        $schools = [];

		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$categories = DB::table('school_categories')
            ->where(['status' => 1])->whereNull('deleted_at')->select('uuid as id', 'name')->orderBy('name', 'ASC')->get()->toArray();

        foreach($categories as $cat) {
               $scholCates['category'] = $cat->name;
               $scholCates['schools'] = $this->list_schools($cat->id, $cat->name);
               $schools[] = $this->list_schools($cat->id, $cat->name);
               $categorySchool[] = $scholCates;

        }
        $mixedschools =  collect($schools)->flatten();;

        $data['categories'] = $categories;
        $data['category_school'] = $categorySchool;
        $data['schools'] = $mixedschools;

        return response()->json($data);
    }

    public function list_schools($uuid = '', $name = '')
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$school_category_id = $uuid;

		$schoolLogoPath = url('/') . '/uploads/schools/';

		$db = DB::table('schools')->where('schools.status', 1)->whereNull('schools.deleted_at');
		if(!is_null($school_category_id) && $school_category_id != '') {
			$db->where('school_categories.uuid', $uuid);
		}
        $schools = $db->leftJoin('school_categories', 'school_categories.id', '=', 'schools.school_category')
					->select('schools.uuid as id', 'schools.is_locked', 'school_name as name', DB::raw('CONCAT("'.$schoolLogoPath.'", "", logo) as logo_url'), DB::raw('CONCAT("'.$name.'", "") as institute_type'),'school_categories.uuid as institution_id')->orderBy('school_name', 'ASC')->get()->toArray();

		return $schools;
    }


}
