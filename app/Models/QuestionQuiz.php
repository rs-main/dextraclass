<?php

namespace App\Models;

use App\Models\QuestionBank;
use Illuminate\Database\Eloquent\Model;

class QuestionQuiz extends Model
{
    public $table = 'questions_quiz';
    protected $fillable = [
    'question_bank_id',
    'title',
    'subject_id',
    'type',
    'pass_mark',
    'lesson_id',
    'q_group_id',
    'active',
    'status'];
    //

    public function question_quiz()
    {
        return $this->hasMany(QuizQuestion::class, 'q_quiz_id');
    }

    public function subject(){
        return $this->belongsTo(Subject::class);
    }

    public function questions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(QuestionBank::class, 'quiz_questions', 'question_bank_id', 'q_quiz_id');
    }

    public function school_details($subject_id)
    {
        $subject_details = Subject::find($subject_id);

        $class_details = Classes::find($subject_details->class_id);
        $course_details = Course::find($class_details->course_id);
        $school_details = School::find($course_details->school_id);

        if(isset($school_details->school_name) && !empty($school_details->school_name)) {
            return $school_details->school_name;
        } else {
            return '';
        }
    }

    public function class_details($subject_id)
    {
        $subject_details = Subject::find($subject_id);

        $class_details = Classes::find($subject_details->class_id);


        if(isset($class_details->class_name) && !empty($class_details->class_name)) {
            return $class_details->class_name;
        } else {
            return '';
        }
    }

    public function subject_details($subject_id)
    {
        $subject_details = Subject::find($subject_id);
        if(isset($subject_details->subject_name) && !empty($subject_details->subject_name)) {
            return $subject_details->subject_name;
        } else {
            return '';
        }
    }

    public function topic_details($topic_id)
    {
        $topic_details = Topic::find($topic_id);
        if(isset($topic_details->topic_name) && !empty($topic_details->topic_name)) {
            return $topic_details->topic_name;
        } else {
            return '';
        }
    }

    public function get_pass_mark($quiz_id){
        $quiz_details = $this->find($quiz_id);
        if(isset($quiz_details->pass_mark) && !empty($quiz_details->pass_mark)){
            return $quiz_details->pass_mark;
        }
        else{
            return '';
        }
    }
}
