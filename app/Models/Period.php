<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Emadadly\LaravelUuid\Uuids;

class Period extends Model
{
	use Uuids;
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $guarded = [];

    /*
     * Get all related videos.
     */
    public function videos()
    {
        $query = $this->hasMany(Video::class, 'period_id');

        return $query;
    }
    public function startTime()
    {
        return date('h:i A', strtotime($this->start_time));
    }

    public function endTime()
    {
        return date('h:i A', strtotime($this->end_time));
    }

    public function period_class()
    {
        return $this->belongsTo('App\Models\Classes', 'class_id', 'id');
    }

	 /*
     * Get referenced record.
     */
    public function school_details($school_id)
    {
       $school_details = School::find($school_id);
	   if(isset($school_details->school_name) && !empty($school_details->school_name)) {
			return $school_details->school_name;
	   } else {
			return '';
	   }
    }

	 /*
     * Get referenced record.
     */
    public function course_details($course_id)
    {
       $course_details = Course::find($course_id);
	   if(isset($course_details->name) && !empty($course_details->name)) {
			return $course_details;
	   } else {
			return '';
	   }
    }

}
