<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionOptions extends Model
{
    protected $table = 'question_options';

    protected $fillable = ['title', 'status',
            'active_status',
            'created_at',
            'updated_at',
            'updated_by',
            'question_bank_id',
            'school_id',
            'academic_id'];
    //
}
