<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
// use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SClass extends Model
{
    // use Compoships;

    const PMR = 'PMR';
    const JHS = 'JHS';
    const SHS = 'SHS';
    const PROF = 'PROF';
    const TVET = 'TVET';
    const UNI = 'UNI';

    use Uuids;
    use SoftDeletes;

    protected $table = 'sclasses';

    protected $fillable = ['name','status','category_code'];


    public function scategories()
    {
        return $this->belongsTo(SCategory::class);
    }

    public function class_topics()
    {
        return $this->hasMany(OpenTopic::class, 'class_id');
    }

    public function class_subject_topics($subject)
    {
        return $this->hasMany(OpenTopic::class, 'class_id')
           ->where('subject_id', $subject);
    }


    public function subjects()
    {
        return $this->belongsToMany(SSubject::class,'sclass_subjects','class_id', 'subject_id')->select(['ssubjects.id','ssubjects.subject_name','ssubjects.uuid']);
    }


    public function courses()
    {
        return $this->belongsToMany(OpenCourse::class,'sclass_courses','class_id', 'course_id')->select(['scourses.id','scourses.name','scourses.uuid']);
    }

    public function open_subjects()
    {
        return $this->morphToMany(SSubject::class, 'subjectable','subjectables','','subject_id')->select(['ssubjects.id','ssubjects.subject_name as name','ssubjects.uuid']);
    }

    public function class_videos()
    {
        return $this->hasMany(OpenVideo::class, 'class_id');
    }

    // public function cvideos()
    // {
    //     return $this->hasMany(OpenVideo::class, ['class_id', 'subject_id']);
    // }

    public function open_topics()
    {
        return $this->morphToMany(OpenTopic::class, 'topictable','topictables','','topic_id');
    }

    public function class_subject_videos($subject)
    {
        return $this->hasMany(OpenVideo::class, 'class_id')
           ->where('subject_id', $subject);
    }

    public function scopeIsBasic($query)
    {
        return $query->whereIn('category_code',['PMR','JSH']);
    }

    public function scopeIsTvet($query)
    {
        return $query->where('category_code',['TVET']);
    }


    public static function getClassId($uuid)
    {
        $subject = self::where('uuid', $uuid)->firstOrFail();
        return $subject;
    }

    protected $hidden = ['pivot'];
}
