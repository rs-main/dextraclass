<?php

namespace App\Models;

use App\User;
use Emadadly\LaravelUuid\Uuids;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    use Uuids;
    protected $table = 'plans';
    protected $fillable = ['name', 'duration', 'status','description', 'amount'];

    public function user()
    {
        return $this->belongsToMany(User::class,'user_plans', 'user_id', 'plan_id');
    }


    public static function planDateDiff($startOn, $endDate): int
    {
        $startDate = strtotime($startOn);
        $endOn = strtotime($endDate);
        $diff =  $endOn - $startDate;
        $daysRemain = round($diff / 86400);

        return $daysRemain;
    }


    public static function userPlanCheck()
    {
        $userPlan = '';
        $user = Auth::user();
        $ifPlans = $user->plan->count();
        if ($ifPlans > 0) {
            $userPlan = collect($user->plan)->last();
            $userPlan = self::planDateDiff($userPlan->starts_on, $userPlan->expired_on);
        }

        return $userPlan;
    }

}
