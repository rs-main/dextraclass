<?php

namespace App\Models;

use App\User;
use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class UserLibrary extends Model
{
    use Uuids;
    protected $table = 'user_library';

    protected $fillable  = ['user_id' , 'class_id', 'library_id', 'item_name' ,'tutor_id', 'description','library_item_created'];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function classroom()
    {
        return $this->belongsTo(Classroom::Class, 'class_id')->withDefault();
    }

    public function tutor()
    {
        return $this->belongsTo(Tutor::Class, 'tutor_id','user_id')->withDefault();
    }

    /*
    * Get referenced record of note.
    */
    public function note()
    {
        return $this->hasOne(Note::Class, 'id', 'note_id')->withDefault();
    }

}
