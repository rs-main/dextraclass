<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class SCategory extends Model
{
    use Uuids;

    protected $fillable = ['name','status','uuid','code'];
    protected $table = 'scategories_institution';


    public function classes()
    {
        return $this->belongsToMany(SClass::class, 'scategory_class', 'scategory_id', 'class_id')->select(['sclasses.id as class_id','name','uuid','sclasses.status as class_status','category_code']);
    }

    protected $hidden = ['pivot'];
}
