<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Emadadly\LaravelUuid\Uuids;

class Department extends Model
{

    use Uuids;
	
	use SoftDeletes;

    protected $dates = ['deleted_at'];
 
    public $table = 'departments';
	
	 /*
     * Get referenced record of category.
     */
    public function school()
    {
        return $this->belongsTo('App\Models\School', 'school_id', 'id');
    }


}
