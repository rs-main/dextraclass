<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpenPeriods extends Model
{
    use Uuids;
    use SoftDeletes;
    protected $table = 'op_periods';

    protected $fillable = ['title', 'start_time', 'end_time' ,'class_id', 'status','weight'];
}
