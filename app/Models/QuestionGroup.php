<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionGroup extends Model
{
    //
    protected $table = 'question_groups';
    protected $fillable = ['name' ,'group_code' ,'status' ,'created_by' ,'school_id' ,'academic_id'];

    public function school_details($school_id)
    {
        $school_details = School::find($school_id);
        if(isset($school_details->school_name) && !empty($school_details->school_name)) {
            return $school_details->school_name;
        } else {
            return '';
        }
    }

}
