<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SidStudent extends Model
{

    protected $guarded = [];

    protected $casts = [
        "info" => "array"
    ];
}
