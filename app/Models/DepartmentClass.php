<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class DepartmentClass extends Model
{

    use Uuids;
 
    public $table = 'department_classes';

}
