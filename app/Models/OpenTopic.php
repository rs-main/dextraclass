<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpenTopic extends Model
{
    use Uuids;
    use SoftDeletes;

    protected $table = "op_topics";

    protected $fillable = ['topic_name','subject_id', 'class_id'];



    public function courses()
    {
        return $this->morphedByMany(OpenCourse::class,'topictable');
    }


    public function classes()
    {
        return $this->morphedByMany(SClass::class,'topictable');
    }

    public function subjects()
    {
        return $this->morphedByMany(SSubject::class,'topictable');
    }
}
