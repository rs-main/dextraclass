<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionChoice extends Model
{
    protected $fillable = ['name', 'status'];

    public function questionsBank()
    {
        return $this->belongsTo(QuestionBank::class);
    }
}
