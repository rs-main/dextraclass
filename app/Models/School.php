<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Emadadly\LaravelUuid\Uuids;

class School extends Model
{
    use Uuids;

    const DEFAULT_SCHOOL_LOGO = 'images/class-logo.png';
    const BASIC_SCHOOL = 1;
    const SENIOR_HIGH = 2;
    const UNIVERSITY = 5;
    const BASIC_SCHOOL_OPEN = 19;
    const JHS_SCHOOL_OPEN = 20;
    const SENIOR_HIGH_OPEN = 21;
    const SENIOR_HIGH_OPEN_TVET = 22;
    const SID_PROJECT_TOKEN = '2y10eZiFIBKBuS5SEtcgZ0YJAezQN63xcjOOm9IJBI8ploxw10Hf832';

    use SoftDeletes;
    
    protected $dates = ['deleted_at'];

    public $appends = ['school_logo'];

    protected $guarded = [];

    /**
     * Change school name title to name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return "{$this->title}";
    }

    public function coursesList()
    {
        return $this->hasMany(Course::class, 'school_id');
    }

    /*
     * Get only those related courses which have video to play.
     */
    public function coursesHasVideo()
    {
        return $this->hasMany(Course::class, 'school_id')
                        ->whereHas('latestVideo');
    }


    /*
     * Get only those related courses which have video to play with a key.
     */
    public function coursesHasVideoWithKey()
    {
        return $this->hasMany(Course::class, 'school_id')
                        ->whereHas('latestVideoWithKey');
    }

    /*
     * Get referenced record of category.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\SchoolCategory', 'school_category', 'id');
    }

    /*
     * Get school category name.
     */
    public function getCategoryNameAttribute()
    {
        if($this->school_category == School::BASIC_SCHOOL || School::BASIC_SCHOOL_OPEN || School::JHS_SCHOOL_OPEN) {
           return 'Term';
        } else if ($this->school_category == School::SENIOR_HIGH ||  School::SENIOR_HIGH_OPEN || School::SENIOR_HIGH_OPEN_TVET) {
            return 'Semester';
        } else if ($this->school_category == School::UNIVERSITY) {
            return 'Semester';
        }


    }

    /*
     * Get semester name list based on category.
     */
    public function semesterNameArray()
    {

        if ($this->school_category == School::BASIC_SCHOOL || School::BASIC_SCHOOL_OPEN || School::JHS_SCHOOL_OPEN) {
            return array(
                1 => '1st Term',
                2 => '2nd Term',
                3 => '3rd Term'
            );
        } else if ($this->school_category == School::SENIOR_HIGH || School::SENIOR_HIGH_OPEN || School::SENIOR_HIGH_OPEN_TVET) {
            return array(
                1 => 'First Semester',
                2 => 'Second Semester',
                3 => 'Third Semester'
            );
        } else if ($this->school_category == School::UNIVERSITY) {
            return array(
                1 => 'First Semester',
                2 => 'Second Semester',
                3 => 'Third Semester'
            );
        } else  {
            return array(
                1 => 'First Semester',
                2 => 'Second Semester',
                3 => 'Third Semester'
            );
        }
    }


    /*
     * Get all related videos.
     */
    public function videos()
    {
        $query = $this->hasMany(Video::class, 'school_id');

        return $query;
    }

    /*
     * Get latest video from the list.
     */
    public function latestVideo()
    {
        $query = $this->videos()
                ->where('play_on', '<=', date('Y-m-d'))
                ->where('status', '=', 1)
                ->orderBy('play_on', 'Desc');

        return $query;
    }

    /*
     * Get program nad course label based on school category.
     */
    public function coursesLabel($count)
    {
        $label = $count . ' ';
        if ($this->school_category == School::BASIC_SCHOOL || School::BASIC_SCHOOL_OPEN || School::JHS_SCHOOL_OPEN) {
            $course = $this->coursesHasVideo[0];
            $count = $course->classesHasVideos->count();
            $label .= ($count > 1) ? 'Classes': 'Class';

        } else if ($this->school_category == School::SENIOR_HIGH || School::SENIOR_HIGH_OPEN || School::SENIOR_HIGH_OPEN_TVET) {
            $label .= ($count > 1) ? 'Courses': 'Course';
        } else if ($this->school_category == School::UNIVERSITY) {
            $label .= ($count > 1) ? 'Programs': 'Program';
        }

        return $label;
    }

    /*
     * Get latest video having a keyword from the list.
     */
    public function latestVideoWithKey()
    {
        $keyword = '';
        if (request()->get('search_input')) {
            $keyword = request()->get('search_input');
        }

        $query = $this->videos()
                ->where('play_on', '<=', date('Y-m-d'))
                ->where('status', '=', 1)
                ->orderBy('play_on', 'Desc');

        if($keyword !=''){
            $query = $query->where(function($qry) use ($keyword){
                    $qry->whereHas('subject', function ($query1) use ($keyword)
                            {
                                 $query1->where('subject_name', 'LIKE', '%'.$keyword.'%');
                            });
                    $qry->orWhereHas('topic', function ($query2) use ($keyword)
                            {
                                 $query2->where('topic_name', 'LIKE', '%'.$keyword.'%');
                            });
                    $qry->orWhereHas('school', function ($query3) use ($keyword)
                            {
                                 $query3->where('school_name', 'LIKE', '%'.$keyword.'%');
                            });
                    $qry->orWhereHas('course', function ($query4) use ($keyword)
                            {
                                 $query4->where('name', 'LIKE', '%'.$keyword.'%');
                            });
                });
        }

        $query = $query->whereHas('school', function ($query1) {
                             $query1->where('status', '=', 1);
                        })->whereHas('course', function ($query2) {
                             $query2->where('status', '=', 1);
                        })->whereHas('classDetail', function ($query3) {
                             $query3->where('status', '=', 1);
                        })->whereHas('subject', function ($query4) {
                             $query4->where('status', '=', 1);
                        })->whereHas('topic', function ($query5) {
                             $query5->where('status', '=', 1);
                        });

        return $query;
    }


	public function getSchoolLogoAttribute()
	{
		/* userData->created_profile_image */
		if(!empty($this->logo) && file_exists(public_path('uploads/schools/'.$this->logo))){
			return 'uploads/schools/'.$this->logo;
		}else{
			return School::DEFAULT_SCHOOL_LOGO;
		}
	}
	/*
     * return boolean.
     */

	public function schoolHasLimit($school_id) {

		$school_details = School::where('id',$school_id)->first();
		$total_assign_classes_std = StudentClasses::where('school_id', $school_id)->distinct('user_id')->count();

		if(isset($school_details->restrict_to_student) && $school_details->restrict_to_student != 0) {
			if($school_details->student_limit <= $total_assign_classes_std) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}

	}

	public static function getProfessionalCourseSchoolId(){
	    $school = School::where("school_name","LIKE","%Take a Course%")->first();
	    return $school? $school->id : null;
    }

    public static function getSchoolCourses($school_id){
	    $courses = Course::whereSchoolId($school_id)->get();

//	    $courses = $courses->map(function ($item, $index){
//	        return [
//
//            ]
//        });

        return $courses;

    }

}
