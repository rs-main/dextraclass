<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class OpenNotes extends Model
{
    use Uuids;

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public $table = 'op_notes';

    public function courses()
    {
        return $this->morphedByMany(OpenCourse::class,'noteable');
    }


    public function classes()
    {
        return $this->morphedByMany(SClass::class,'noteable');
    }

    public function open_topics()
    {
        return $this->morphToMany(OpenTopic::class, 'noteable','noteables','','note_id');
    }


}
