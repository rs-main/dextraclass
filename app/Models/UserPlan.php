<?php

namespace App\Models;

use App\Models\SubscriptionTransaction;
use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
//    protected $fillable = ['user_id','plan_id','starts_on','expired_on','payment_status','activate_mode_type' ];
    protected $guarded = [];

    protected $with = ['plan'];



    public function plan()
    {
        return $this->belongsTo(Plans::class)->select(['id','name','duration', 'amount']);
    }


    public function users()
    {
        return $this->belongsTo(User::class);
    }


    public function insertSubscriptionTransaction($user, $plan, $txRef, $data = '')
    {
        $obj = new SubscriptionTransaction();
        $obj->user_id = $user->id;
        $obj->user_plan_id = $plan->id;
        $obj->tx_ref = $txRef; // $data['txRef'];
        $obj->save();
    }


    public function updateSubscriptionTransaction($user, $data = '')
    {
        $subs = new SubscriptionTransaction();

        $obj = $subs::where(['user_id' => $user->id, 'tx_ref'=> $data['txRef']])->first();

        $obj->customer_phone = $data['customer.phone'];
        $obj->txn_id = $data['id'];
        $obj->flwRef = $data['flwRef'];
        $obj->amount = $data['amount'];
        $obj->charged_amount = $data['charged_amount'];
        $obj->appfee = $data['appfee'];
        $obj->chargeResponseMessage = $data['chargeResponseMessage'];
        $obj->orderRef = $data['orderRef'];
        $obj->authModelUsed = $data['authModelUsed'];
        $obj->currency = $data['currency'];
        $obj->clientIP = $data['IP'];
        $obj->narration = $data['narration'];
        $obj->status = $data['status'];
        $obj->modalauditid = $data['modalauditid'];
        $obj->acctvalrespmsg = $data['acctvalrespmsg'];
        $obj->acctvalrespcode = $data['acctvalrespcode'];
        $obj->paymentType = $data['paymentType'];
        $obj->customerId = $data['customerId'];
        $obj->accountId = $data['AccountId'];
        $obj->createdAt = $data['createdAt'];
        $obj->updatedAt = $data['updatedAt'];

        $obj->save();
    }

}
