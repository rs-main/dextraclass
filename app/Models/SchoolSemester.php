<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Emadadly\LaravelUuid\Uuids;

class SchoolSemester extends Model
{
	use Uuids;
    public $table = "school_semesters";
}
