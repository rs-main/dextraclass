<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Support\Arrayable;

class ClassLesson implements Arrayable
{
    public $viedo_url = '';
    public $vimeo_id = '';
    public $id = '';
    public $video_id = '';
    public $tutor = '';
    public $description = '';
    public $keywords = '';
    public $note_url = '';
    public $total_views = '';
    public $play_on = '';
    public $lesson = '';
    public $topic = '';



    /**
     * Converts the item to array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'tutor' => $this->tutor,
            'viedo_url'  => $this->viedo_url,
            'id'  => $this->id,
            'video_id'  => $this->video_id,
            'description'  => $this->description,
            'keywords'  => $this->keywords,
            'note_url'  => $this->note_url,
            'play_on'  => $this->play_on,
            'lesson'  => $this->lesson,
            'vimeo_id'  => $this->vimeo_id,
            'topic'  => $this->topic,
            'total_views'  => $this->total_views == null ? 0 : $this->total_views
        ];
    }
}
