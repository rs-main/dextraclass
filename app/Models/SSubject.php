<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SSubject extends Model
{
    use Uuids;
    use SoftDeletes;

    protected $table = "ssubjects";

    protected $fillable = ['subject_name'];

    protected $hidden = ['pivot'];

    // public $appends = ['name'];


    public function courses()
    {
        return $this->morphedByMany(OpenCourse::class,'subjectable');
    }


    public function classes()
    {
        return $this->morphedByMany(SClass::class,'subjectable');
    }

    public function open_topics()
    {
        return $this->morphToMany(OpenTopic::class, 'topictable','topictables','','topic_id');
    }


    public function topics()
    {
        return $this->hasMany(OpenTopic::class,'subject_id');
    }



    public function subject_videos()
    {
        return $this->hasMany(OpenVideo::class, 'subject_id');
    }

    public static function getSubjectId($uuid)
    {
        $subject = self::where('uuid', $uuid)->firstOrFail();
        return $subject;
    }


    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    // public function getNameAttribute()
    // {
    //     return $this->attributes['name'] = $this->subject_name;
    // }
    // public function subjects()
    // {
    //     return $this->belongsTo(Subjects::class);
    // }

    // public function classes()
    // {
    //     return $this->belongsTo(SClasses::class);
    // }
}
