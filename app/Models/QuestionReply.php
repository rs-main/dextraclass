<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class QuestionReply extends Model
{
    use Uuids;


    protected $fillable = ['question_id', 'type','content','sender_id'];


    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
