<?php

namespace App\Models;

use App\Models\School;
use App\Models\Classes;
use App\User;
use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{
    public $table = 'questions_bank';
//    protected $fillable = ['type','question','marks','suitable_word','num_of_options','choice_id','active_status', 'subject_id',
//              'q_group_id','class_id','section_id','created_by','school_id','academic_id',
//        'question_type','year','has_image','image'];
//    //

    protected $guarded = [];

    protected $with = ['options'];
    protected $appends = ['school','quiz_creator'];
    protected $casts = [
        'year' => 'date',
    ];


    public function question_quiz()
    {
        return $this->belongsTo(QuestionQuiz::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function class()
    {
        return $this->belongsTo(\App\Models\Classes::class);
    }


    public function options()
    {
        return $this->hasMany(QuestionOptions::class);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }


    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function getQuizCreateAttribute()
    {
        $user = User::find($this->created_by);
        $userRole = $user->userRole->role->slug;
        return  $userRole;
    }

    public function getQuizCreatorAttribute()
    {
//        $user = User::find($this->created_by);
//        $userRole = $user->userRole->role->slug;
//        return  $userRole;
    }

    public function getSchoolAttribute()
    {
        $school = School::find($this->school_id);
//        return  $school->school_name;
    }

    public function getClassAttribute()
    {
        $school = Classes::find($this->class_id);
        return  $school->class_name;
    }

    public function school_details($school_id)
    {
        $school_details = School::find($school_id);
        if(isset($school_details->school_name) && !empty($school_details->school_name)) {
            return $school_details->school_name;
        } else {
            return '';
        }
    }

    public function subject_details($subject_id)
    {
        $subject_details = Subject::find($subject_id);
        if(isset($subject_details->subject_name) && !empty($subject_details->subject_name)) {
            return $subject_details->subject_name;
        } else {
            return '';
        }
    }

}
