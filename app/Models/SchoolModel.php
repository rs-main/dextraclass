<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Emadadly\LaravelUuid\Uuids;

class SchoolModel extends Model
{
    protected $table = "schools";

    public function coursesList(): HasMany
    {
        return $this->hasMany(Course::class, 'school_id');
    }
}
