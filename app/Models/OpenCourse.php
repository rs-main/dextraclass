<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpenCourse extends Model
{
    use Uuids;
    use SoftDeletes;

    protected $table = 'scourses';

    protected $fillable = ['name','type','description','status','uuid'];

    protected $hidden = ['pivot','open_subjects'];


    public function open_subjects()
    {
        return $this->morphToMany(SSubject::class, 'subjectable','subjectables','','subject_id')->select(['ssubjects.id','ssubjects.subject_name','ssubjects.uuid']);
    }

    public function open_topics()
    {
        return $this->morphToMany(OpenTopic::class, 'topictable','topictables','','topic_id');
    }


}
