<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SClassSubjects extends Model
{
    protected $table = "sclass_subjects";

    protected $fillable = ['class_id', 'class_uuid', 'subject_id', 'subject_uuid'];


    public function subjects()
    {
        return $this->belongsTo(Subjects::class);
    }

    public function classes()
    {
        return $this->belongsTo(Classes::class);
    }
}
