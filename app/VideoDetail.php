<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoDetail extends Model
{
    protected $guarded = [];

    protected $casts = [
        "details" => "array"
    ];
}
