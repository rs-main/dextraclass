<?php

namespace App;

use App\Models\Course;
use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    protected $table = "course_category";

    protected $guarded = [];

    public function course(){
        return $this->belongsTo(MainCourse::class,"course_id");
    }
}
