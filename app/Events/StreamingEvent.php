<?php

namespace App\Events;

use App\Models\Classes;
use App\Models\Subject;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StreamingEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $classId;
    public $subjectId;
    public $streamingChannel;
    public $status;
    public $message;
    public $type;
    public $clazz;
    public $subject;
    public $classObject;
    public $subjectObject;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($class_id, $subject_id,$streaming_channel, $status,$type="streaming")
    {
        $this->classId = $class_id;
        $this->subjectId = $subject_id;
        $this->streamingChannel = $streaming_channel;
        $this->status = $status;
        $this->type = $type;
        $this->classObject   = Classes::find($this->classId);
        $this->subjectObject = Subject::find($this->subjectId);
        $this->clazz = $this->classObject->class_name;
        $this->subject = $this->subjectObject->subject_name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ["streaming-channel"];
    }

    public function broadcastWith()
    {
        $message = $this->status === "started" ? "Class started streaming" : "Class ended streaming!";
        return [
                'message'    =>  $message,
                'class'      => $this->classObject->class_name,
                "subject"    => $this->subjectObject->subject_name,
                "class_id"   => $this->classId,
                "subject_id" => $this->subjectId,
                "streaming_channel" => $this->streamingChannel,
                "status"       => $this->status,
                "type"         => $this->type
        ];
    }

    public function broadcastAs()
    {
        return 'streaming-event';
    }
}
