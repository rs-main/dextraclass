<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class PaymentActionController extends APIController
{
    
    
    
    /*    public function __construct()
    {

        $this->middleware('auth:api');
    }*/

    public function subscription_callback(Request $request){
        
         //$request_data = $request->all();
        
        //Sample Response: {"message":"Success", "trans_status":"000/200", "trans_id":"MP151224.1621.C00005", "trans_ref":"1512241630284"}
        
        return $this->respond($this->successStatus, 'Success , Call 0509962708 for further assistance');
    }
    
    
    public function make_subscription(Request $request)
    {
        
        
        
        $request_data = $request->all();
        
        //YYYY-MM-DD H:M:S
        $timestamp = date('Y-m-d H:i:s');
        
        $uuid = $request_data['uuid'];
   
        $customer_number = $request_data['customer_number'];
        
        $amount = $request_data['amount'];
        $network = $request_data['network'];
        $voucher_code = $request_data['voucher_code'];
        $reference = $request_data['reference'];
        
        $SECRET_KEY = env("SECRET_KEY", "");
        $CLIENT_KEY = env("CLIENT_KEY", "");
        $SERVICE_ID = env("SERVICE_ID", "");
        
        
        $service_url = 'https://orchard-api.anmgw.com/sendRequest';
        
        $today = date("Ymdhis");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,5));
  
        $exttrid =  $today . $rand;

        $callback_url = 'https://demo.dextraclass.com/api/subscription_callback';
    
        
        $data = array(
        'service_id' => $SERVICE_ID, 
        'trans_type' => 'CTM',
        'customer_number'=>$customer_number,
        'amount' => $amount,
        'exttrid' => $exttrid,
        'reference' => $reference,
        'nw' => $network,
        'callback_url' => $callback_url,
        'ts' => $timestamp,
        'voucher_code' => $voucher_code,
        );
        
        $data_string = json_encode($data);
        $signature =  hash_hmac( 'sha256' , $data_string , $SECRET_KEY );
        $auth = $CLIENT_KEY.':'.$signature;
        
        $ch = curl_init($service_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: '.$auth,
        'Content-Type: application/json',
        'timeout: 180',
        'open_timeout: 180'
        )
        );
        
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        
        $response = preg_replace('/\\\\\"/',"\"", $result);
        $response1 = json_decode($response);
        
        if(stristr($response1->resp_code, "015") != FALSE){
            return $this->respond($this->successStatus, "Request successfully received for processing $uuid");
        }
        else{
             return $this->respond($this->badRequestStatus, $response1->resp_desc, 'Sorry Could not Proccess, Call 0509962708 for further assistance' );
           // return $this->respond($this->badRequestStatus, 'Sorry Could not Proccess, Call 0509962708 for further assistance');
        }
     
    
       

    }

}
