<?php

namespace App\Http\Controllers\API;

use App\User;
use Exception;
use App\Models\Tutor;
use App\Models\Student;
use App\Helpers\SiteHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UserVerficationController extends Controller
{
//    public function sendOtp(Request $request)
//    {
//        $data = $request->all();
//        $data['otp'] = mt_rand(10000, 99999);
//        $user = User::where('uuid', $request->user_id)->first();
//
//        $vsid = SiteHelpers::sendOtpUsingTwilio($user->id, $data['phone_code'], $data['mobile']);
//
//        if(!empty($vsid)){
//            $res['status'] = true;
//            $res['message'] = "Verification code successfully sent on your mobile number.";
//        } else {
//            $res['status'] = false;
//            $res['message'] = "Unable to send verification code";
//        }
//
//        return json_encode($res);
//
//    }

    public function reSendOtp(Request $request)
    {

        if ($request->isMethod('post')) {


            if (empty($request->input('user_id'))) {
                $res['status'] = false;
                $res['message'] = "User not found";
            } else {

                $user_id = $request->input('user_id');
                $sessionUser = User::find($request->input('user_id'));
                $userRole = $sessionUser->userRole->role->slug;

                $res['status'] = false;
                $res['message'] = null;

                $data = $request->all();

                if ($data['type'] == 'send-otp') {
                    try {
                        $otp = mt_rand(10000, 99999);

                        //change mobile number
                        if ($data['mobile_number_type'] == "new") {

                            $student_mobile_exists = Student::where(['mobile' => $data['mobile']])->count();
                            if ($student_mobile_exists > 0) {
                                $res['status'] = false;
                                $res['message'] = "This mobile number already registered.";
                                return json_encode($res);
                            } else {
                                if ($userRole == 'student') {
                                    Student::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                } else if ($userRole == 'student') {
                                    Tutor::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                }
                            }
                        }

                        if ($userRole == 'student') {
                            $student = Student::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        } else if ($userRole == 'tutor') {
                            $student = Tutor::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        }
                        if ($student) {
                            $vsid = SiteHelpers::sendOtpUsingTwilio($student->user_id, $data['phone_code'], $data['mobile']);

                            if(!empty($vsid)) {
                                $res['status'] = true;
                                $res['message'] = "Verification code successfully sent on your mobile number.";
                            } else {
                                $res['status'] = false;
                                $res['message'] = "Unable to send verification code";
                            }

                        } else {
                            $res['status'] = false;
                            $res['message'] = "Wrong registered mobile number";
                        }
                    } catch (Exception $e) {
                        $res['status'] = false;
                        $res['message'] = $e->getMessage();
                    }

                    return json_encode($res);
                }
            }
        } else {
            return "Invalid request";
        }
    }




    /*
     * Verify user otp
     * return status true / false with error message
     */
    public function verifyOtp(Request $request)
    {

        if ($request->isMethod('post')) {
            $res['status'] = false;
            $res['message'] = null;

            $data = $request->all();

            if ($data['type'] == 'verify-otp') {

                    $verify = SiteHelpers::verifyOtpUsingSmsCore($request->mobile,$request->otp);

                    if ($verify["responseCode"] == 200){
                        $res['status'] = true;
                        $res['message'] = "Otp verified successfully.";
                    }else{
                        $res['status'] = false;
                        $res['message'] = "Verification failed";
                    }

                return json_encode($res);
            }
        } else {
            return "Invalid request";
        }
    }


    public function sendOtp(Request $request)
    {
        if ($request->isMethod('post')) {

            if (empty($request->input('user_id'))) {
                $res['status'] = false;
                $res['message'] = "User not found";
            } else {

                $user = User::whereUuid($request->get("user_id"))->first();
                $sessionUser = User::find($user->id);
                $userRole = $sessionUser->userRole->role->slug;

                $res['status'] = false;
                $res['message'] = null;

                $data = $request->all();

//                if ($data['type'] == 'send-otp') {
                    try {
                        $otp = mt_rand(10000, 99999);

                        //change mobile number
                        if ($data['mobile_number_type'] == "new") {

                            $student_mobile_exists = Student::where(['mobile' => $data['mobile']])->count();
                            if ($student_mobile_exists > 0) {
                                $res['status'] = false;
                                $res['message'] = "This mobile number already registered.";
                                return json_encode($res);
                            } else {
                                if ($userRole == 'student') {
                                    Student::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                } else if ($userRole == 'student') {
                                    Tutor::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                }
                            }
                        }

                        if ($userRole == 'student') {
                            $student = Student::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        } else if ($userRole == 'tutor') {
                            $student = Tutor::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        }
                        if ($student) {

                            $otp_mobile_prefix = substr($request->mobile,0,1);
                            $otp_mobile = $otp_mobile_prefix == "0" ? substr($data["mobile"],1) : $data["mobile"];
                            $res["sms"] = SiteHelpers::sendOtpUsingCoreSms($data['phone_code'].$otp_mobile);

//                            $vsid = SiteHelpers::sendOtpUsingTwilio($student->user_id, $data['phone_code'], $data['mobile']);

                            if($res["sms"]->responseCode == 200){
                                $res['status'] = true;
                                $res['message'] = "Verification code successfully sent on your mobile number.";
                            } else {
                                $res['status'] = false;
                                $res['message'] = "Unable to send verification code";
                            }

                        } else {
                            $res['status'] = false;
                            $res['message'] = "Wrong registered mobile number";
                        }
                    } catch (Exception $e) {
                        $res['status'] = false;
                        $res['message'] = $e->getMessage();
                    }

                    return json_encode($res);
                }
            }
//        } else {
//            return "Invalid request";
//        }
    }

}
