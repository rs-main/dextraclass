<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\SchoolCategory;
use App\Http\Controllers\Controller;

class SchoolCategoryController extends APIController
{
    private $schoolCategory;
    public function __construct(SchoolCategory $schoolCategory)
    {
        $this->schoolCategory = $schoolCategory;
    }

    public function loadSchoolCategory()
    {
        $schoolCategory = $this->schoolCategory::all();

        return $this->respond($this->successStatus, '',  $schoolCategory);
    }


    public function loadSchoolInsitutions(Request $request)
    {
        $request['lang'];
        $schoolCategory = $this->schoolCategory->list_institutions($request);

        return $schoolCategory;

    }


    public function loadInsitutionsSchool()
    {
        $schoolCategory = $this->schoolCategory->list_institutions();

    }
}
