<?php

namespace App\Http\Controllers\API;

use App\Models\UserLibrary;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\APIController;

class UserLibraryController extends APIController
{

    public $userlibrary;
    public function __construct(UserLibrary $userlibrary)
    {
        $this->userlibrary = $userlibrary;
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        $userlibrary = $user->library;

        $limit = 200;

        $userlibraryItems = $this->userlibrary->where('user_id', $user->id)
					->leftJoin('notes', 'user_library.library_id','=','notes.id')
					->select('user_library.library_id','notes.id as noteId', 'notes.file_url', 'notes.storage')
					->get()
                    ->toArray();

				$favResponse = array();
				$notesUrl = url('/') . '/';
				$notesPath = public_path('/');

				if(!empty($userlibraryItems) && count($userlibraryItems) > 0) {
					foreach($userlibraryItems as $k => $v) {
						$userlibraryItems[$k]['notes_url'] = '';
						if($v['library_id'] > 0 && $v['file_url'] != '') {
							if($v['storage'] == 'local') {
								if(file_exists($notesPath . $v['file_url']) && is_file($notesPath . $v['file_url'])) {
									$userlibraryItems[$k]['notes_url'] = $notesUrl . '' . $v['file_url'];
								}
							} else if($v['storage'] == 's3') {
								if(Storage::disk($v['storage'])->exists($v['file_url'])){
									$userlibraryItems[$k]['notes_url'] = Storage::disk($v['storage'])->url($v['file_url']);
								}
							}
						}
						unset($userlibraryItems[$k]['library_id'], $userlibraryItems[$k]['storage'], $userlibraryItems[$k]['file_url']);
					}
				}
				return $this->respond($this->successStatus, '', $userlibraryItems);

                // return $this->respond($this->successStatus,'' , $userlibrary);

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $request['library_item_created'] = \date('Y-m-d H:i:s', $request->library_item_created);
        $library = $this->userlibrary::create($request->all());

        return $this->respond($this->successStatus,'' ,$library);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
