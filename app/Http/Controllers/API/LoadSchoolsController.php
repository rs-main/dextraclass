<?php

namespace App\Http\Controllers\API;

use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoadSchoolsController extends APIController
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => array('index')]);
    }
    
    public function index()
    {
        $schools = School::where('status', '=', 1)->where('featured',1)
        ->whereHas('latestVideo');
        $schools = $schools->orderBy('school_name', 'asc');
        $schools = $schools->paginate(10);

        return $this->respond($this->successStatus, '',  $schools);
    }

    
}
