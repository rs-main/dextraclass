<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Models\Tutor;
use App\Models\Course;
use App\Models\School;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Models\SchoolCategory;
use App\Models\StudentClasses;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SchoolAssignController extends Controller
{
    /**
     * Save extra user details after validate mobile.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function autoMobileSchoolAssign(Request $request)
    {

        $sessionUser = User::uuid($request->uuid);

        // if (!empty(session('userId'))) {
        //     $sessionUser = User::uuid($request->uuid);
        // } else {
        //     return response()->json(['message' => 'Please complete first step before you can access next steps!']);
        // }

        $schoolCat = SchoolCategory::uuid($request->school_cat);

        $data = $request->all();

        if (!empty($request->school_cat) && !empty($request->school_id) && $schoolCat->id == School::BASIC_SCHOOL || $schoolCat->id == School::BASIC_SCHOOL_OPEN
            || $schoolCat->id == School::JHS_SCHOOL_OPEN) {
            $schools_details = School::where('uuid', trim($request->input('school_id')))->where('school_category', $schoolCat->id)
                ->select('id')->first();

            if (!empty($schools_details->id)) {
                $school = School::find($schools_details->id);
                $schoolcourses = Course::where('school_id', $school->id)->first();
                if (!empty($schoolcourses->id)) {
                    $data['course'] = $schoolcourses->id;
                }
            }
        }

        $userRole = "student";
        $user = $sessionUser;
        $userRole = $user->userRole->role->slug;


        if ($userRole == 'tutor') {
            $validator = Validator::make($data, [
                        'tutor_subject' => 'required|regex:/^[a-zA-Z0-9_\-]*$/|max:150',
                        'school_id' => 'required|string|max:255',
            ],[
				'tutor_subject.regex' => "Subject Name contains <li>The subject name must contain alphanumeric characters only</li>",
			]);
        } else {
            $validator = Validator::make($data, [
                        'school_cat' => 'required',
                        'school_id' => 'required',
            ]);
        }

        if (empty($schools_details)) {
            $schools_details = School::where('uuid', $request->input('school_id'))->where('school_category', $request->input('school_cat'))->select('id')->first();
        }

        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => 'Invalid School Details!']);
        }


        if (!isset($schools_details->id) && empty($schools_details->id)) {
            return response()->json(['message' => 'Invalid School Name!']);
        }
        if ($userRole == 'tutor') {
            $tutor = Tutor::where('user_id', $sessionUser->id)->first();
            $tutor->school_id = $schools_details->id;
            $tutor->tutor_subject = $request->tutor_subject;
            $tutor->save();
        } else {
            $student = Student::where('user_id', $sessionUser->id)->first();
            $student->school_id = $schools_details->id;
            $student->school_category = $data['school_cat'];
            $student->save();

            $dcs = $this->assignedclasses($student->uuid);

        }
        return response()->json(['status' => 1, 'message' => 'Schoool Assigned Successful']);
    }

    public function assignedclasses($uuid) {

        $student = Student::where('uuid',$uuid)->first();
        $myclassArr = array();

		if(isset($student->school_id) && !empty($student->school_id)) {
			$courses = Course::orderBy('name')->where('status','=',1)->where('school_id','=',$student->school_id)->get();
		} else {
			$courses = array();
		}

        $studentclasses = StudentClasses::where('user_id', $student->user_id)->select('class_id')->pluck('class_id')->toArray();

       foreach($courses as $course) {
          foreach($course->classes as $class){
              $myclassArr[] = $class->id;
          }
       }

       $toAssign = $this->save_assignedclasses($student->school_id, $student->user_id, $myclassArr);

	}

	/**
     * form to assigned classes to student.
     *
     * @param  int  $uuid
     * @return \Illuminate\Http\Response
     */
	public function save_assignedclasses($school = '', $user = '', $classes = []) {

        $count_assigned_classes = StudentClasses::where('user_id', $user)->count();

        $data['school'] = $school;
        $data['class_id'] = $classes;
        $data['user'] = $user;

		$count_assigned_classes = StudentClasses::where('user_id', $user)->count();

		$student = Student::where('user_id', $user)->first();

		StudentClasses::where('user_id', $user)->delete();

		if(!empty($data['class_id'])) {
			foreach($data['class_id'] as $key=>$val) {
				$student_class = new StudentClasses();
				$student_class->school_id = $school;
				$student_class->user_id = $user;
				$student_class->class_id = $val;
				$student_class->save();
			}
		}

	}
}
