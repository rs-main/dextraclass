<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Traits\OrchardPaymentNotification;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class SubscriptionRequestController extends APIController
{
    use OrchardPaymentNotification;

  /*  public $user, $student;
    public function __construct(User $user, Student $student)
    {
        $this->user = $user;
        $this->student = $student;
        $this->middleware('auth:api');
    }*/
    
    
    
    

    public function requestSubscription(Request $request)
    {

       // $user = $this->user::uuid($request->uuid);
       // $student = $user->student;
        //$numBer = $student->mobile;
        
        $customer_number = $request->customer_number;
        $amount = $request->amount;
        $network = $request->network;
        $voucher_code = $request->voucher_code;
        $reference = $request->reference;

        //$access = $student->save();
        // makePaymentRequest($customer_number, $amount, $reference, $nw, $voucher_code)
        

        $vsMakePayment = $this->makePaymentRequest($customer_number, $amount, $reference, $network, $voucher_code);



        if ($vsMakePayment) {
            
            return $this->respond($this->successStatus, 'Access Request in Progress, Call 0509962708 for further assistance');
        }
        return $this->respond($this->badRequestStatus, 'Sorry Could not Proccess, Call 0509962708 for further assistance'.$getNum);

    }
}
