<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response as IlluminateResponse;

/**
 * Base API Controller.
 */
class APIController extends Controller
{
    public $successStatus = 200;
    public $badRequestStatus = 400;
    public $unauthorizedStatus = 401;
    public $forbiddenStatus = 403;
    public $notFoundStatus = 404;
    public $alreadyExistsStatus = 409;
    public $unprocessableEntityStatus = 422;
    public $internalServerErrorStatus = 500;

    /**
     * Respond.
     *
     * @param array $data
     * @param array $headers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($statusCode, $message, $data = null) {
        return response()->json([
            'statusCode' => $statusCode,
            'message' => $message,
            'data' => $data ? $data : new \StdClass()
        ]);
    }

    public function throwValidation($message) {
        return response()->json([
            'statusCode' => $this->unprocessableEntityStatus,
            'message' => $message,
            'data' => new \stdClass(),
        ]);
    }

	public function pushNotificationIOS($title, $message, $deviceToken, $notifyType, $args = array(), $badgeCount = 0) {
		if($deviceToken == '') {
			return false;
		}
        $passphrase = 'admin@123';
		$pem = public_path('/pem/ck.pem');

		$production = 1;
		if ($production) {
			$gateway = 'gateway.push.apple.com:2195';
		} else {
			$gateway = 'gateway.sandbox.push.apple.com:2195';
		}

		// Create a Stream
		$ctx = stream_context_create();
		// Define the certificate to use
		stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
		// Passphrase to the certificate
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client('ssl://'.$gateway, $err, $errstr, 60,  STREAM_CLIENT_CONNECT, $ctx);

		$payload = "";
		$setFlag = 1;
		//$badgeCount = 2;

		//$payload = '{"aps": {"alert": "'.$title.'", "badge":'.$badgeCount.', "sound": "default", "message": "'.$message.'"}}';
        $body['aps'] = array(
			'alert' => array(
			    'title' => $title,
                'body' => $message,
			),
			//'category' => $notifyType,
			'sound' => 'default'
		);
		// Encode the payload as JSON
		$payload = json_encode($body);

		if($payload!="") {
			if(!$fp) {
				$setFlag = 0;
				//echo("Failed to connect (1) $err $errstr");
			}
			$msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $deviceToken)) . pack("n",strlen($payload)) . $payload;
			$result = fwrite($fp, $msg);

			if (!$result) {
				//fclose($fp);
				sleep (3);
				$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60,  STREAM_CLIENT_CONNECT, $ctx);
				if(!$fp) {
					$setFlag = 0;
					//echo("Failed to connect (2): ".$err." ".$errstr.PHP_EOL);
				}
                $setFlag = 0;
			}
		}
		fclose($fp);
		return $setFlag;
	}
}
