<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\QuestionChoice;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class QuestionChoiceController extends APIController
{
    public $questionChoice;
    public function __construct(QuestionChoice $questionChoice)
    {
        $this->questionChoice = $questionChoice;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionChoice = $this->questionChoice::all();
        return $this->respond($this->successStatus,'', $questionChoice);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questionChoice = $this->questionChoice::create($request->all());
        return $this->respond($this->successStatus,'', $questionChoice);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
