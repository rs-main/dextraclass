<?php

namespace App\Http\Controllers\API;

use App\Models\StudentQuiz;
use Illuminate\Http\Request;
use App\Models\StudentQuizHistory;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class StudentQuizController extends APIController
{
    public $studentQuiz, $studentQuizHistory;
    public function __construct(StudentQuiz $studentQuiz, StudentQuizHistory $studentQuizHistory)
    {
        $this->studentQuiz = $studentQuiz;
        $this->studentQuizHistory = $studentQuizHistory;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['date_taken'] = date('Y-m-d H:i:s');
        $studentQuiz = $this->studentQuiz::create($request->all());

        $this->studentQuizHistory::create([
            'student_quiz_id' => $studentQuiz->id,
            'quiz_mark'  => $request->mark
        ]);
        return $this->respond($this->successStatus, '','Saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
