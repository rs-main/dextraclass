<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class AuthUserController extends APICOntroller
{
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
		$success_msg = __('messages.user_loggedin_successfully');
		$error_msg = __('messages.login_error.invalid_credentials');
        $data = new \stdClass();
		$message = __('messages.login_error.login_failed');

        $status = $this->forbiddenStatus;

        $creds = ['username' => request('username'), 'password' => request('password')];


        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            $user = Auth::user();


			if($user->status == 1 && $user->deleted_at === NULL &&$user->mobile_verified_at !== NULL ){
                $user->device_type = request('platform');
				$user->device_token = request('device_token');
				$user->save();

                $client = new \GuzzleHttp\Client();

                $tokenData = $this->tokenRequest($request);

				$respData['access'] = $tokenData->access_token;
				$respData['refresh'] = $tokenData->refresh_token;

				return $respData;

				return $this->respond($this->successStatus, "", $respData);
			} else if($user->status == 0 && $user->email_verified_at !== NULL) {
                return $this->respond($this->badRequestStatus, __('messages.login_error.account_deactivated'));
			} else if($user->email_verified_at === NULL) {
                return $this->respond($this->badRequestStatus, __('messages.login_error.email_not_verified'));
			}
        }
		return $this->respond($this->unauthorizedStatus, __('messages.login_error.invalid_credentials'));
    }

    function tokenRequest(Request $request)
    {
        // return "Requesting Token";
        $user = Auth::user();

        $request->request->add([
                        'grant_type' => 'password',
						'client_id' => env('PASSPORT_PERSONAL_ACCESS_CLIENT_ID'),
						'client_secret' => env('PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET'),
						'username' => $request->username,
						'password' => $request->password,
						'scope' => '',
              ]);

              $tokenRequest = $request->create(
                      env('APP_URL').'/oauth/token',
                      'post'
              );

              $instance = Route::dispatch($tokenRequest);

              return json_decode($instance->getContent());

      }

        /**
         * refresh_token api
         *
         * @return \Illuminate\Http\Response
         */
        public function refresh_token(Request $request) {
            $client = new \GuzzleHttp\Client();

            $request->request->add([
                'grant_type' => 'refresh_token',
                'refresh_token' => request('refresh'),
                'client_id' => env('PASSPORT_PERSONAL_ACCESS_CLIENT_ID'),
                'client_secret' => env('PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET'),
                'scope' => '',
        ]);

        $tokenRequest = $request->create(
                env('APP_URL').'/oauth/token',
                'post'
        );

        $instance = Route::dispatch($tokenRequest);


        $tokenData =  json_decode($instance->getContent());

		// $tokenData = json_decode((string) $getToken->getBody(), true);

		$respData['access'] = $tokenData['access_token'];
		$respData['refresh'] = $tokenData['refresh_token'];

		return $this->respond($this->successStatus, '', $respData);
    }

      /**
     * get_device api
     *
     * @return \Illuminate\Http\Response
     */
    public function save_device(Request $request) {
		$user_id = Auth::user()->id;
		$data = $request->all();

		$user = User::where('id', $user_id)->first();
		if(!empty($user)) {
			$user->device_token = $data['device_token'];
			$user->device_type = $data['platform'];

			if($user->save()){
				return $this->respond($this->successStatus, '');
			} else {
				return $this->respond($this->internalServerErrorStatus, 'Failed to save user');
			}
		}

		return $this->respond($this->notFoundStatus, 'User not found');
    }

    public function getAuthenticatedAPI(Request $request): \Illuminate\Http\JsonResponse
    {

        return response()->json(["data" => "data"],200);
    }


}
