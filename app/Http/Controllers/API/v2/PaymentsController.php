<?php

namespace App\Http\Controllers\API\v2;

use App\Models\Plans;
use App\Models\UserPlan;
use App\User;
use Exception;
use Carbon\Carbon;
use App\Models\Tutor;
use App\Models\Student;
use App\Helpers\SiteHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PaymentsController extends Controller
{
    const BASE_URI = "http://services.desafrica.com";
    private $otp_url = "/api/payments/otp";
    private $verify_url = "/api/payments/";

    public function subscribe(Request $request){
        $request->validate([
            'amount' => "required",
            'network' => "required",
            'email' => "required",
            'phone_number' => "required",
            'fullname' => "required",
            'user_id' => "required",
            'description' => "required",
            "plan_id"     => "required"
        ]);

        return $this->buildPayment($request);
    }

    private function buildPayment($request){

        $amount = $request->amount * 100;

        $data = array(
            'amount' => $amount,
            'currency' => 'GHS',
            'network' => $request->network,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'fullname' => $request->fullname,
            'user_id' => $request->user_id,
            'service' => 'XTRACLASS',
            'amount_expected' => $amount,
            'description' => $request->description,
            'plan_id' => $request->plan_id
        );

        $query_data = http_build_query($data);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://services.desafrica.com/api/payments',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $query_data,
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json',
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $data_response = json_decode($response);
        $this->subscribeToPlan($request, $data_response->data->transaction_reference);
        return response()->json($data_response);
    }

    public function submitOtp(Request $request){

        $request->validate([
            "otp" => "required",
            "transaction_reference" => "required"
        ]);

        $data = array(
            'otp' => $request->otp,
            'transaction_reference' => $request->transaction_reference,
        );

        $query_data = http_build_query($data);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::BASE_URI.$this->otp_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $query_data,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response_data = json_decode($response);

        return response()->json(json_decode($response));

    }

    public function verifyPayment(Request $request, $reference){

        $curl = curl_init();

        $url = self::BASE_URI.$this->verify_url;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url.$reference,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $response_data = (json_decode($response));

        if (($response_data->data)){
            if ($response_data->data->state == "success"){
                $user_plan = UserPlan::whereTransactionReference($reference)->first();
                if ($user_plan){
                    DB::beginTransaction();
                    $user_plan->update([
                        "payment_status" => "paid"
                    ]);

                    DB::table('students')
                        ->where('user_id', $user_plan->user_id)
                        ->update(['subscribed' => 1]);
                }

                DB::commit();
            }
        }

        return response()->json($response_data);
    }

    public function subscribeToPlan(Request $request,$transaction_id)
    {
        $carbon = new Carbon();
        $user_id = $request->user_id;
        $student = Student::whereUserId($user_id)->first();
        $payStatus = "pending";
        $status = 1;
        $planId = $request->plan_id;
        $activeMode =  'self';
        $startOn = $carbon->now();
        $startOn  =  date('Y-m-d H:i:s', strtotime($startOn));
        $myplan = Plans::find($planId );
        $duration = $myplan->duration;
        $subsDate = strtotime("+ $duration", strtotime($startOn));
        $expDuration =  date('Y-m-d H:i:s', $subsDate);

//        $daysRemain = $this->plans::planDateDiff($startOn, $expDuration);

        $userPlan = UserPlan::create([
            'user_id' => $user_id,
            'plan_id' => $planId,
            'starts_on' => $startOn,
            'expired_on' => $expDuration,
            'activate_mode_type' => $activeMode,
            "transaction_reference" => $transaction_id,
            'payment_status' => $payStatus
        ]);

//        $student->updatedStudentTableSubscribe($user_id, $status);

        return \response()->json($userPlan);
    }


}
