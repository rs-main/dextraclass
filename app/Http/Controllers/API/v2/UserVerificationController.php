<?php

namespace App\Http\Controllers\API\v2;

use App\User;
use Exception;
use Carbon\Carbon;
use App\Models\Tutor;
use App\Models\Student;
use App\Helpers\SiteHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UserVerificationController extends Controller
{
    public function sendOtp(Request $request)
    {
        $data = $request->all();
        $data['otp'] = mt_rand(10000, 99999);

        $user = User::where('uuid', $request->user_id)->first();

        $vsid = SiteHelpers::sendOtpUsingWiga($user->id, $data['phone_code'], $data['mobile']);

        if(!empty($vsid)){
            $res['status'] = $vsid;
            $res['message'] = "Verification code successfully sent on your mobile number.";
        } else {
            $res['status'] = $vsid;
            $res['message'] = "Unable to send verification code";
        }

        // return json_encode($res);
        return response()->json($res);

    }

       /**
     * Verify user otp
     * return status true / false with error message
     */
    public function verifyOtp(Request $request)
    {
        $data = $request->all();
        $data['otp'] = mt_rand(10000, 99999);

        $user = User::where('uuid', $request->user_id)->first();

        $otpdetails = DB::table("mobile_verifications")
                        ->where('user_id', $user->id)
                        ->where('mobile', $request->all('mobile'))
                        ->orderBy('id','desc')->first();

        // return response()->json($otpdetails);
            if(!empty($otpdetails->vsid)){

                            $vsid = $otpdetails->vsid;
                            $code = $request->input('otp');

            $status = SiteHelpers::verifyOtpUsingWiga($vsid, $code);
                // $status = SiteHelpers::verifyOtpUsingTwilio($vsid, $code);
                if($status == 'approved') {
                    User::where('id', $user->id)->update(['mobile_verified_at' => Carbon::now()]);
                    $res['status'] = true;
                    $res['message'] = "Verification code verified successfully.";
                } else {
                    $res['status'] = false;
                    $res['message'] = "Wrong verification code.";
                }

                return response()->json($res);
            }else{
                $res['status'] = false;
                $res['message'] = "Wrong registered mobile number or verification code";
            }

            return response()->json($res);
    }
    public function reSendOtp(Request $request)
    {
        // return $request;

        // $vsid = SiteHelpers::sendOtpUsingTwilio($student->user_id, $data['phone_code'], $data['mobile']);


        // if(!empty($vsid)){
        //     $res['status'] = true;
        //     $res['message'] = "Verification code successfully sent on your mobile number.";
        // } else {
        //     $res['status'] = false;
        //     $res['message'] = "Unable to send verification code";
        // }

        if ($request->isMethod('post')) {


            if (empty($request->input('user_id'))) {
                $res['status'] = false;
                $res['message'] = "User not found";
            } else {

                $user_id = $request->input('user_id');
                $sessionUser = User::find($request->input('user_id'));
                $userRole = $sessionUser->userRole->role->slug;

                $res['status'] = false;
                $res['message'] = null;

                $data = $request->all();

                if ($data['type'] == 'send-otp') {
                    try {
                        $otp = mt_rand(10000, 99999);

                        //change mobile number
                        if ($data['mobile_number_type'] == "new") {

                            $student_mobile_exists = Student::where(['mobile' => $data['mobile']])->count();
                            if ($student_mobile_exists > 0) {
                                $res['status'] = false;
                                $res['message'] = "This mobile number already registered.";
                                return json_encode($res);
                            } else {
                                if ($userRole == 'student') {
                                    Student::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                } else if ($userRole == 'student') {
                                    Tutor::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                }
                            }
                        }

                        if ($userRole == 'student') {
                            $student = Student::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        } else if ($userRole == 'tutor') {
                            $student = Tutor::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        }
                        if ($student) {
                            //$student->otp = $otp;
                            //$student->save();
//                            if ($data['mobile_number_type'] == "new") {
//                                SiteHelpers::updateOtp($student->user_id, $data['phone_code'], $data['mobile'], $otp, $data['old_mobile_number']);
//                            } else {
//                                SiteHelpers::updateOtp($student->user_id, $data['phone_code'], $data['mobile'], $otp);
//                            }
                            //$otpSend = SiteHelpers::sendOtpToUser($data['country-code'], $data['mobile_number'], $otp);

                            /* Below code for send otp to student */
                            //$sendOtp = SiteHelpers::sendOtpToUser($data['phone_code'], $data['mobile'], $otp);
                            /* Send verification to student or  tutor using TWILo */
                            $vsid = SiteHelpers::sendOtpUsingTwilio($student->user_id, $data['phone_code'], $data['mobile']);

                            if(!empty($vsid)) {
                                $res['status'] = true;
                                $res['message'] = "Verification code successfully sent on your mobile number.";
                            } else {
                                $res['status'] = false;
                                $res['message'] = "Unable to send verification code";
                            }


//                            $sendOtp = json_decode(json_encode($sendOtp), true);
//                            //echo "<pre>"; print_r($sendOtp); exit;
//                            if (isset($sendOtp['messages']) && array_key_exists('error-text', $sendOtp['messages'][0]) && $sendOtp['messages'][0]['status'] != 0) {
//                                $res['status'] = false;
//                                $res['message'] = $sendOtp['messages'][0]['error-text'];
//                            } else {
//                                $res['status'] = true;
//                                $res['message'] = "Verification code successfully sent on your mobile number.";
//                            }
                        } else {
                            $res['status'] = false;
                            $res['message'] = "Wrong registered mobile number";
                        }
                    } catch (Exception $e) {
                        $res['status'] = false;
                        $res['message'] = $e->getMessage();
                    }

                    return json_encode($res);
                }
            }
        } else {
            return "Invalide request";
        }
    }

    // public function verifyOtp(Request $request)
    // {
    //     if ($request->isMethod('post')) {
    //         $res['status'] = false;
    //         $res['message'] = null;

    //         $data = $request->all();

    //         if ($data['type'] == 'verify-otp') {
    //             try {

    //                 $user_id = $request->all('user_id');

    //                 if ($user_id) {

    //                 $user = User::where('uuid', $request->user_id)->first();

    //                     $otpdetails = DB::table("mobile_verifications")
    //                                         ->where('user_id', $user->id)
    //                                         ->where('mobile', $request->all('mobile'))
    //                                         ->orderBy('id','desc')->first();

    //                     if(!empty($otpdetails->vsid)){

    //                         $vsid = $otpdetails->vsid;
    //                         $code = $request->input('otp');

    //                         $status = SiteHelpers::verifyOtpUsingTwilio($vsid, $code);

    //                         if($status == 'approved') {
    //                             User::where('id', $user_id)->update(['mobile_verified_at' => Carbon::now()]);
    //                             $res['status'] = true;
    //                             $res['message'] = "Verification code verified successfully.";
    //                         } else {
    //                             $res['status'] = false;
    //                             $res['message'] = "Wrong verification code.";
    //                         }
	// 						/* }else{

	// 							$res['status'] = false;
    //                             $res['message'] = "Your OTP expired.";
	// 						} */

    //                     } else {
    //                         $res['status'] = false;
    //                         $res['message'] = "Wrong verification code.";
    //                     }
    //                 } else {
    //                     $res['status'] = false;
    //                     $res['message'] = "Wrong registered mobile number.";
    //                     //return redirect()->route('registerStep', 2)->withErrors($validator)->withInput();
    //                 }
    //             } catch (Exception $e) {
    //                 $res['status'] = false;
    //                 $res['message'] = $e->getMessage();
    //             }

    //             return json_encode($res);
    //         }
    //     } else {
    //         return "Invalide request";
    //     }
    // }
}
