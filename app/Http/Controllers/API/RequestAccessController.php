<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Models\Student;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Traits\SendSMSNotification;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class RequestAccessController extends APIController
{
    use SendSMSNotification;

    public $user, $student;
    public function __construct(User $user, Student $student)
    {
        $this->user = $user;
        $this->student = $student;
        $this->middleware('auth:api');
    }

    public function requestAccess(Request $request)
    {
        $pfx = "233";

        $user = $this->user::uuid($request->uuid);
        $student = $user->student;
        $numBer = $student->mobile;

        $getNum = Str::after($numBer, "0");
        $mobileNum = "$pfx$getNum";

        $msg = "Thank you for Joining XtraClass.\nSelect a plan below to get access to all lessons and notes.\n-Monthly (GHc 25)\n-Term - 4 months (GHc 70)\n-Yearly - (GHc 225). All plans come with free monthly data. Pay to Merchant ID 144838 account name is XtraClass.\nCall 0509962708 for assistance.";



        $student->request_access = 1;

        $access = $student->save();

        $vsms = $this->sendMessage($msg, $mobileNum);


        if ($access && $vsms) {
            return $this->respond($this->successStatus, 'Access Request in Progress, Call 0509962708 for further assistance');
        }
        return $this->respond($this->badRequestStatus, 'Sorry Could not Proccess, Call 0509962708 for further assistance');

    }
}
