<?php

namespace App\Http\Controllers\API;


use App\Location;
use Illuminate\Http\Request;



class LocationController extends APIController
{
    //

    public function getLocation(){

        $query = \request()->query("q");
        $locations = Location::leftJoin("regions","regions.id","=","locations.region_id")
            ->Where("locations.name","LIKE", "%".$query."%")
            ->selectRaw("locations.id, CONCAT(locations.name, ' (',regions.name,' )') as text")
            ->take(30)
            ->get();

        return $locations;
    }

}
