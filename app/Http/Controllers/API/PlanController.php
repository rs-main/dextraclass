<?php

namespace App\Http\Controllers\API;

use App\Models\Plans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{
    public $plan;
    public function __construct(Plans $plan)
    {
        $this->plan = $plan;
    }

    public function index()
    {
        $plans = $this->plan::selectRaw("id,name,duration,amount")->get();
         return \response()->json(["data" => $plans]);
    }


    public function store(Request $request)
    {

        $plans = $this->plan::create($request->all());
        return \response()->json($plans);
    }


    public function update(Request $request, $plan)
    {
        $plan  = $this->plan::find($plan)->update($request->all());
        return \response()->json(['message' => 'Plan updated Successfully']);
    }

    public function allPlans()
    {
        $plans = $this->plan::all();
        return view('frontend.student.plan', compact('plan', 'plans'));
    }
}
