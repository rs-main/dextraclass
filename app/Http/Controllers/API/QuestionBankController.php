<?php

namespace App\Http\Controllers\API;

use App\Models\Classes;
use App\Models\Period;
use App\Models\QuestionBank;
use App\Models\QuestionQuiz;
use App\Models\QuizQuestion;
use App\Models\StudentQuiz;
use App\Models\StudentQuizHistory;
use App\Models\Subject;
use App\Models\Video;
use App\PastQuestion;
use App\PastQuestionGroup;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\QuestionOptions;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Integer;

class QuestionBankController extends APIController
{
    public $questionBank, $questionOptions;
    public function __construct(QuestionBank $questionBank, QuestionOptions $questionOptions)
    {
        $this->questionBank = $questionBank;
        $this->questionOptions = $questionOptions;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionBank = $this->questionBank::all();
        return $this->respond($this->successStatus,'', $questionBank);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $options[] = explode(",",$request->get('options'));
        //$request['suitable_word'] = \implode(",", $request->suitable_words);

        if ($request->has("year")) {
            $exams_date_input = $request->get($request->year);
            $exams_date = Carbon::parse($exams_date_input);
            $exams_date->format('Y-m-d');
        }

        $questionBank = $this->questionBank::create([
            'type' => $request->type,
            'question' => $request->question,
            'choice_id' => $request->answer,
            'num_of_options' => $request->num_of_options,
            'active_status' => $request->active_status,
            'school_id' => $request->school_id,
            'class_id' => $request->class_id,
            'subject_id' => $request->subject_id,
            'question_type' => $request->past_question_type,
            'year'          => $exams_date,
            'has_image' => $request->image_question != null
        ]);

        if ($request->has('image_question') && !empty($request->image_question)) {

            $validator = Validator::make($request->all(), [
                'image_question' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            ], [
                    'image_question.max' => 'The image may not be greater than 2mb.',
                ]
            );

            if ($validator->fails()) {
                return redirect()->route('backend.questions.create')->withErrors($validator)->withInput();
            }

            $destinationPath = public_path('/uploads/questions/');
            $newName = '';
            $fileName = $request->all()['image_question']->getClientOriginalName();
            $file = request()->file('image_question');
            $fileNameArr = explode('.', $fileName);
            $fileNameExt = end($fileNameArr);
            $newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;

            $file->move($destinationPath, $newName);

            $imagePath = 'uploads/questions/' . $newName;

            QuestionBank::find($questionBank->id)->update([
                "image" => $imagePath,
                "has_image" => true
            ]);
        }

        foreach ($options as $value) {
            foreach ($value as $val){
                $this->questionOptions::create([
                    'title' => $val,
                    'active_status' => 1,
                    'question_bank_id' => $questionBank->id,
                    'school_id' => $request->school_id
                ]);
            }
        }
        $data = array(
            'url' => URL::to('/admin/questions'),
            'success' => true);

        return $this->respond($this->successStatus,'Question addition successful',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions = QuestionBank::whereId($id)
            ->pluck("question");
        return response()->json(["questions" => $questions]);
    }

    public function videoQuizQuestions($video_id,$class_id=null,$subject_id=null, $student_id=null): \Illuminate\Http\JsonResponse
    {

        try {

            $video_builder = Video::leftJoin('periods','periods.id','=','videos.period_id');

            $user = User::whereUuid($student_id)->first();
            $quizzesTaken = StudentQuiz::where('user_id' , $user->id)
            ->leftJoin('student_quiz_history', 'students_quiz.id','=','students_quiz.id')
            ->groupBy('quiz_id')
            ->distinct('quiz_id')
            ->pluck("quiz_id","student_id")->toArray();

            $quizTaken_collection = collect($quizzesTaken);
            $quiz_taken_status = 0;
            $has_quiz = false;
            $quiz_passed = false;
            $video_quiz_taken_id = null;
            $next_video_module = 0;

            $videos = $video_builder->with(["Quiz"=> function($quiz){
               return $quiz->with(["question_quiz"=> function($ques_quiz){
                   return $ques_quiz->with(["questions" => function($ques){
                       return $ques->with("options")->get();
                   }])->get();
               }])->get();
            }])->where("videos.id",$video_id)
                ->where("videos.status",true)
                ->get();

            $collection = new Collection();

            foreach ($videos as $video) {
                $quiz_taken_status = $video->quiz ? $quizTaken_collection->contains($video->quiz->id) : null;
                $has_quiz = $video->quiz ? true : false;
                $video_quiz_taken_id = $quiz_taken_status ? $video->quiz->id : 0;
                $student_quiz_marks = $video->quiz ? intval($video->get_student_mark($video_quiz_taken_id, $video->subject_id)) : null;
                $quiz_pass_marks = intval($video->get_pass_mark($video_quiz_taken_id));
                $next_video_module = $video->period->weight+1;

                $quiz_passed = $student_quiz_marks >= $quiz_pass_marks;

                if ($video->quiz) {
                    foreach ($video->quiz->question_quiz as $ques_quiz) {
                        $options = new Collection();

                        if ($ques_quiz->questions->type != "true-false") {

                            if ($ques_quiz->questions->options) {
                                foreach ($ques_quiz->questions->options as $key => $ques_option) {
                                    $options->push([
                                        "value" => $key + 1,
                                        "detail" => $ques_option->title,
                                        "correct" => $ques_quiz->questions->choice_id === $key + 1]);
                                }
                            }

                            $collection->push([
                                "text" => $ques_quiz->questions->question,
                                "options" => $options,
                                "type" => $ques_quiz->questions->type,
                            ]);

                        } else {
                            $collection->push([
                                "text" => $ques_quiz->questions->question,
                                "options" => [
                                    "option " => [
                                        "value" => 1,
                                        "detail" => "true",
                                        "correct" => $ques_quiz->questions->choice_id === 1
                                    ],
                                    "option" => [
                                        "value" => 2,
                                        "detail" => "false",
                                        "correct" => $ques_quiz->questions->choice_id === 2],
                                    "type" => $ques_quiz->questions->type,
                                ],
                            ]);
                        }
                    }
                }
            }

            $next_video_module_object = $video_builder
                ->selectRaw("videos.id,videos.uuid,videos.video_url,videos.description,
                videos.subject_id,videos.class_id,videos.video_type")
//                ->where("videos.id",$video_id)
                ->where("periods.weight",$next_video_module)->first();

            return response()->json(
                     [
                        "quiz_id" =>  $video->quiz? $video->quiz->id:null,
                        "questions" => $collection,
                        "quiz_taken" => $quiz_taken_status,
                        "has_quiz" => $has_quiz,
                        "quiz_passed" => $video->quiz? $quiz_passed : null ,
                        "video_quiz_taken_id" => $video->quiz ? $video_quiz_taken_id : null,
                        "quiz_marks" =>  $video->quiz? intval($video->get_student_mark($video_quiz_taken_id, $video->subject_id)) : null,
                        "pass_mark" => intval($video->get_pass_mark($video_quiz_taken_id)),
                        "next_video_weight" => $next_video_module,
                        "next_video_module" => $next_video_module_object
                    ]);

//            return response()->json(["videos" => $videos]);
//            return response()->json(["videos" => $video->quiz->question_quiz]);
        }catch(\Exception $exception) {
                return response()->json(["exception" => $exception->getMessage(),
                    "trace" => $exception->getLine(),
                    "trace_2" => $exception->getTraceAsString()
                ]);

        }
    }

    public function submitQuiz(Request $request): \Illuminate\Http\JsonResponse
    {
        $user_id = $request->user_id;
        $quiz_id = $request->quiz;
        $answers[] = $request->answers;
        $marks_in_percentage = $request->marks;
        $main_quiz = QuestionQuiz::find($quiz_id);
        $pass_mark = $main_quiz->pass_mark;
        $subject_id = $main_quiz->subject_id;

        $student_quiz = StudentQuiz::create([
            'user_id' => $user_id,
            'quiz_id' => $quiz_id,
            'subject_id' => $subject_id,
            'date_taken' => date('Y-m-d H:i:s')
        ]);

        StudentQuizHistory::create([
            'student_quiz_id' => $student_quiz->id,
            'quiz_mark' => $marks_in_percentage
        ]);

        if($marks_in_percentage >= $pass_mark){
            $passed = 1;
        }
        else{
            $passed = 0;
        }

        $data = array(
            'passed' => $passed,
            'pass_mark' => $pass_mark,
            'student_marks' => $marks_in_percentage,
            'quiz_id' => $quiz_id
        );

        return response()->json($data,200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getTypeAheadQuestions(): \Illuminate\Http\JsonResponse
    {
        $query = \request()->query("q");
        $questions = QuestionBank::where("question","%LIKE%", $query)->pluck("name");

        return response()->json(["data" => $questions]);
    }

    public function getPastQuestionsGroup($subject_id,$class_id){
        $subject_object = Subject::whereUuid($subject_id)->first();
        $class_object = Classes::whereUuid($class_id)->first();

        $classId = $class_object? $class_object->id : "";
        $subjectId = $subject_object? $subject_object->id : "";

      return   PastQuestionGroup::leftJoin("past_questions","past_questions.past_question_group_id","=", "past_questions.id")
            ->leftJoin("questions_bank","questions_bank.id","=","past_questions.question_bank_id")
            ->leftJoin("subjects","subjects.id","=","past_questions_group.subject_id")
            ->leftJoin("classes","classes.id","=","past_questions_group.class_id")
            ->where("past_questions_group.subject_id",$subjectId)
            ->where("past_questions_group.class_id"  ,$classId)
            ->selectRaw("past_questions_group.id,
            past_questions_group.group_name,
            past_questions_group.year,
            past_questions_group.month,
            subjects.uuid as subject_id,
            classes.uuid as class_id")
//          ->groupBy("year")
//          ->distinct()
          ->get();
    }

    public function getPastQuestions($id,$subject_id, $class_id){
        $class_object = Classes::whereUuid($class_id)->first();
        $subject_object = Subject::whereUuid($subject_id)->first();

//        return response()->json(["class" => $class_object, "subject_id" => $subject_object]);

        $class_id =   $class_object ? $class_object->id : null;
        $subject_id = $subject_object ? $subject_object->id : null;
        $past_questions = PastQuestion::with(["questions","group"])
              ->whereHas("group",function ($q) use($id,$subject_id,$class_id){
                  $q->where("id",$id)
                      ->where("subject_id",$subject_id)
                      ->where("class_id",$class_id);
            })->get();

        $past_questions = $past_questions->map(function($question_item,$index) {

            $options = $question_item->questions->options->map(function ($item, $index) use($question_item) {
                return [
                    "value" => $index + 1,
                    "detail" => $item->title,
                    "correct" => $index + 1 == $question_item->questions->choice_id ? true : false
                ];
            });

            return [
                "text" => $question_item->questions->question,
                "options" => $options,
                "year"   =>  $question_item->group->year
            ];
        });

        return response()->json(["questions" => $past_questions ]);
    }

    public function getAllPastQuestions(){
        $past_questions = PastQuestion::with(["questions","group"])->get();

        $past_questions = $past_questions->map(function($question_item,$index) {

            $options = $question_item->questions->options->map(function ($item, $index) use($question_item) {
                return [
                    "value" => $index + 1,
                    "detail" => $item->title,
                    "correct" => $index + 1 == $question_item->questions->choice_id ? true : false
                ];
            });

            return [
                "text" => $question_item->questions->question,
                "options" => $options,
                "year"   =>  $question_item->group->year
            ];
        });

        return response()->json(["questions" => $past_questions ]);
    }

}
