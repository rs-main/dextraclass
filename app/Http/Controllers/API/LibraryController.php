<?php

namespace App\Http\Controllers\API;

use App\Models\Library;
use App\Models\Subject;
use App\Models\Video;
use App\Models\Classes;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\API\APIController;

class LibraryController extends APIController
{

    public function getSubjectLibrary( $cid, $vid)
    {
        $limit = 200;
        $class_id = $cid;

         $idObj = $this->getId($class_id, Classes::class);
         $v = Video::uuid($vid);

         $s = $v->subject;

         $class_videos = Video::where('videos.class_id', $idObj->id)
					->where('videos.subject_id', $s->id)
					->where('videos.status','=','1')
					->leftJoin('classes', 'videos.class_id','=','classes.id')
					->leftJoin('periods', 'videos.period_id','=','periods.id')
					->leftJoin('subjects', 'videos.subject_id','=','subjects.id')
					->leftJoin('topics', 'videos.topic_id','=','topics.id')
					->leftJoin('tutors', 'videos.tutor_id','=','tutors.user_id')
					->leftJoin('users as u', 'tutors.user_id','=','u.id')
					->leftJoin('notes', 'videos.note_id','=','notes.id')
					->select('videos.uuid as lesson_id',
                        'classes.uuid as class_id', 'classes.class_name' ,
                        'subjects.subject_name as subject', 'videos.note_id',
                        'videos.description', 'notes.id as noteId', 'notes.file_url',
                        'notes.storage', 'u.uuid as teacher_id', 'u.name as teacher_name',
                        'topics.topic_name', 'videos.total_views as total_read')
					->get()
					->toArray();

				$favResponse = array();
				$notesUrl = url('/') . '/';
				$notesPath = public_path('/');

				if(!empty($class_videos) && count($class_videos) > 0) {
					foreach($class_videos as $k => $v) {
						$class_videos[$k]['notes_url'] = '';
						if($v['note_id'] > 0 && $v['file_url'] != '') {
							if($v['storage'] == 'local') {
								if(file_exists($notesPath . $v['file_url']) && is_file($notesPath . $v['file_url'])) {
									$class_videos[$k]['notes_url'] = $notesUrl . '' . $v['file_url'];
								}
							} else if($v['storage'] == 's3') {
								if(Storage::disk($v['storage'])->exists($v['file_url'])){
									$class_videos[$k]['notes_url'] = Storage::disk($v['storage'])->url($v['file_url']);
								}
							}
						}

                        unset($class_videos[$k]['note_id'], $class_videos[$k]['storage'], $class_videos[$k]['file_url']);
					}
				}
				return $this->respond($this->successStatus, '', $class_videos);
    }

    public function getClassLibrary(Type $var = null)
    {
        # code...
    }

    public function getLibraryItems($subjectId, $classId)
    {
        $class_object = Classes::whereUuid($classId)->first();
        $class_id     = $class_object ? $class_object->id : null;

        $subject_object = Subject::whereUuid($subjectId)->first();
        $subject_id = $subject_object ? $subject_object->id : null;

        $libraryItems =  Library::leftJoin("users","users.id","=","library.user_id")
           ->whereSubjectId($subject_id)
            ->whereClassId($class_id)->selectRaw("users.name as author,
           library.library_item, library.description,library.description,library.file_type,library.view_count,
           library.total_download,library.source,library.file_extension")->get();
       return response()->json(["data" => $libraryItems,"status" => "success"],200);

    }

    public function getAllLibraryItems()
    {
        $libraryItems =  Library::leftJoin("users","users.id","=","library.user_id")
            ->selectRaw("users.name as author,
           library.library_item, library.description,library.description,library.file_type,library.view_count,
           library.total_download,library.source,library.file_extension")->get();
        return response()->json(["data" => $libraryItems,"status" => "success"],200);

    }
    /**
     * Note Upload on S3
     *
     * @return Response
     */
    public function dropzoneNoteStore(Request $request)
    {
        // Get file extension
        $extension = $request->file('notefile')->getClientOriginalExtension();

        // Valid extensions
        $validextensions = array("pdf","zip","jpeg","jpg","png","doc", "docx", "ppt", "pptx");

        // Check extension
        if (in_array(strtolower($extension), $validextensions)) {

            // Rename file
            //$fileName = str_slug(time()) . rand(11111, 99999) . '.' . $extension;

            //$path = 'notes/' . $fileName ;
            $path = Storage::disk('s3')->put('notes', $request->file('notefile'), 'public');
        }

        return response()->json(['success' => 'success', 'savefilename' => $path]);
    }

    public function getId($uuid, $model) {
		return $model::where('uuid', $uuid)->select('id')->first();
	}


}
