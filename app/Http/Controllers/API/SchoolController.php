<?php

namespace App\Http\Controllers\API;

use App\Models\SchoolSemester;
use App\Models\StudentQuiz;
use App\Models\Subject;
use App\SidStudent;
use App\VideoDetail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Classroom;
use App\Models\Course;
use App\Models\Department;
use App\Models\Period;
use App\Models\Question;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Setting;
use App\Models\Student;
use App\Models\StudentFavourites;
use App\User;
use App\Models\Video;
use App\Helpers\SiteHelpers;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;



class SchoolController extends APIController
{

	/**
     * Get all Categories
     *
     * @return [object]
     */
    public function list_institutions(Request $request)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}

		$categories = false;
        $schoolCategoryBuilder = SchoolCategory::whereStatus(true);

		if ($request->query("school") == "open") {
            $categories = DB::table('school_categories')->whereStatus(true)
                //             ->leftJoin("schools","schools.school_category","=","school_categories.id")
                ->where("name", "<>", "Basic School")
                ->where("name", "<>", "University")
                ->where("name", "<>", "Professional")
                ->where("name", "NOT LIKE", "%Take a Course%")
                ->where("name", "NOT LIKE", "%Institution%")
                ->select('uuid as id', 'name')->orderBy('name', 'ASC')
                ->orderBy("name", "asc")
                ->get()->toArray();
        }else if ($request->query("school") == "regular"){
            $categories = DB::table('school_categories')->whereStatus(true)
                ->where("name","LIKE","%Basic School%")
                ->orWhere("name","LIKE","%University%")
                ->orWhere("name","LIKE","%Senior High%")
                ->where("name","NOT LIKE","%Take a Course%")
                ->where("name","NOT LIKE","%Institution%")
                ->select('uuid as id', 'name')->orderBy('name', 'ASC')
                ->orderBy("name", "asc")
                ->get();
        }else{
		    $categories = DB::table('school_categories')
            ->where(['status' => 1])->whereNull('deleted_at')
            ->select('uuid as id', 'name')->orderBy('name', 'ASC')
            ->get()->toArray();
        }



		return $this->respond($this->successStatus, '', $categories);
    }

	/**
     * Get details of an institute
     *
     * @return [object]
     */
    public function institution_details($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		try {
		    $category = SchoolCategory::where(['status' => 1, 'uuid' => $id])->select('uuid', 'name')->firstOrFail();
		} catch (ModelNotFoundException $e) {
            return $this->respond($this->notFoundStatus, 'Category not found');

        }

		return $this->respond($this->successStatus, '', $category);
    }

	/**
     * Get all Schools
     *
     * @return [object]
     */
    public function list_schools($uuid = '')
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$school_category_id = $uuid;

		$schoolLogoPath = url('/') . '/uploads/schools/';

		$db = DB::table('schools')->where('schools.status', 1)->whereNull('schools.deleted_at');
		if(!is_null($school_category_id) && $school_category_id != '') {
			$db->where('school_categories.uuid', $uuid);
		}
		$schools = $db->leftJoin('school_categories', 'school_categories.id', '=', 'schools.school_category')
					->select('schools.uuid as id', 'schools.is_locked', 'school_name as name', DB::raw('CONCAT("'.$schoolLogoPath.'", "", logo) as logo_url'), 'school_categories.uuid as institution_id')->orderBy('school_name', 'ASC')->get()->toArray();

		return $this->respond($this->successStatus, '', $schools);
    }

	/**
     * Get details of an school
     *
     * @return [object]
     */
    public function school_details($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$schoolLogoPath = url('/') . '/uploads/schools/';
		$school = DB::table('schools')->where(['schools.status' => 1, 'schools.uuid' => $id])->whereNull('schools.deleted_at')
					->leftJoin('school_categories', 'school_categories.id', '=', 'schools.school_category')
					->select('schools.uuid as id', 'school_name as name', DB::raw('CONCAT("'.$schoolLogoPath.'", "", logo) as logo_url'), 'school_categories.uuid as institution_id' )->first();
		if(!empty($school)) {
			return $this->respond($this->successStatus, '', $school);
		}
		return $this->respond($this->notFoundStatus, 'School not found');
    }

	/**
     * Get all Departments
     *
     * @return [object]
     */
    public function list_departments($id = '')
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$school_id = $id;
		if($school_id != '') {
			$getSchool = School::where('uuid', '=', $school_id)->where('status', 1)->select('id', 'school_category')->first();
			if(!empty($getSchool)) {
				if($getSchool->school_category == School::UNIVERSITY){
					$departments = DB::table('departments')->where('departments.status', 1)->whereNull('departments.deleted_at')
						->where('school_id', $getSchool->id)
						->leftJoin('schools', 'schools.id', '=', 'departments.school_id')
						->select("departments.uuid as id", "departments.name", 'schools.uuid as school_id')
						->get()->toArray();

					return $this->respond($this->successStatus, '', $departments);
				} else {
                    return $this->respond($this->badRequestStatus, 'School is not a university', []);
				}
			}
		}
		return $this->respond($this->notFoundStatus, 'School not found', []);
    }

	/**
     * Get details of an department
     *
     * @return [object]
     */
    public function department_details($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$department = DB::table('departments')->where(['departments.status' => 1, 'departments.uuid' => $id])->whereNull('departments.deleted_at')
					->leftJoin('schools', 'schools.id', '=', 'departments.school_id')
					->select("departments.uuid as id", "departments.name", 'schools.uuid as school_id')
					->first();
		if(!empty($department)) {
			return $this->respond($this->successStatus, '', $department);
		}
		return $this->respond($this->notFoundStatus, 'Department not found');
    }

	/**
     * Get all Courses
     *
     * @return [object]
     */
    public function list_courses($schoolId, $departmentId = '')
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		if($schoolId != '') {
			$getSchool = School::where('uuid', '=', $schoolId)->where('status', 1)->first();
			if(!empty($getSchool)) {
				if($getSchool->school_category == School::UNIVERSITY){
					$getDepartment = Department::where('uuid', '=', $departmentId)->where('status', 1)->first();
					if(!empty($getDepartment)) {
						//return courses based on schoolId and departmentId
						$courses = Course::where('courses.status', 1)->where('courses.school_id', $getSchool->id)->where('department_id', $getDepartment->id)
							->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
							->leftJoin('departments', 'departments.id', '=', 'courses.department_id')
							->select("courses.name", "courses.uuid", "courses.department_id", 'schools.uuid as school_id', 'departments.uuid as department_id')
							->orderBy('courses.name', 'ASC')->get()->toArray();

						return $this->respond($this->successStatus, '', $courses);
					} else {
						//invalid id
						return $this->respond($this->notFoundStatus, 'Department not found', []);
					}
				} else if($getSchool->school_category == School::SENIOR_HIGH){
					//return coursed based on school
					$courses = Course::where('courses.status', 1)->where('school_id', $getSchool->id)
							->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
							->select("courses.name", "courses.uuid", "courses.department_id", 'schools.uuid as school_id')
							->orderBy('courses.name', 'ASC')->get()->toArray();

					return $this->respond($this->successStatus, '', $courses);
				} else {
                    return $this->respond($this->successStatus, 'School is not a university or senior high', []);
                }
			}
		}
		return $this->respond($this->notFoundStatus, 'School not found', []);
    }

	/**
     * Get all Courses
     *
     * @return [object]
     */
    public function list_school_courses($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$school_id = $id;
        $getSchool = School::where('uuid', '=', $school_id)->where('status', 1)->select('id', 'school_category')->first();
        if(!empty($getSchool)) {
            if($getSchool->school_category == School::SENIOR_HIGH || School::SENIOR_HIGH_OPEN || School::SENIOR_HIGH_OPEN_TVET ){
                $courses = DB::table('courses')->where('courses.status', 1)->whereNull('courses.deleted_at')->where('school_id', $getSchool->id)
                        ->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
                        ->select("courses.uuid as id", "courses.name", DB::Raw("if(courses.department_id = 0 ,'','' ) as department_id"), 'schools.uuid as school_id')
                        ->orderBy('courses.name', 'ASC')->get()->toArray();

                return $this->respond($this->successStatus, '', $courses);
            } else {
                return $this->respond($this->badRequestStatus, 'School is not a senior high', []);
            }
        }
		return $this->respond($this->notFoundStatus, 'School not found', []);
    }


    /**
     * Get all Courses
     *
     * @return [object]
     */
    public function list_school_classes($id)
    {
        $lang = request('lang');
        if(!$lang) {
            $lang = 1;
        }
        $school_id = $id;
        if($school_id != '') {
            $getSchool = School::where('schools.uuid', '=', $school_id)->where('schools.status', 1)->leftJoin('courses', 'schools.id', '=', 'courses.school_id')->select('school_category', 'courses.id as course_id')->first();
            $courseId = '';
            if(!empty($getSchool)) {
                if ($getSchool->school_category == School::BASIC_SCHOOL || School::BASIC_SCHOOL || School::BASIC_SCHOOL_OPEN || School::JHS_SCHOOL_OPEN) {
                    $courseId = $getSchool->course_id;
                    $getCourse = Course::where('id', '=', $courseId)->where('status', 1)->first();
                    if(!empty($getCourse)) {
                        $classes = DB::table('classes')->where('classes.status', 1)->whereNull('classes.deleted_at')->where('classes.course_id', $getCourse->id)
                                     ->leftJoin('courses', 'courses.id', '=', 'classes.course_id')
                                     ->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
                                     ->select("classes.uuid as id", "class_name as name", DB::raw('CONCAT(schools.short_name, " ", classes.class_name) as full_name'), 'courses.uuid as course_id', 'schools.uuid as school_id')
                                     ->orderBy('class_name', 'ASC')->get()->toArray();

                        return $this->respond($this->successStatus, '', $classes);
                    } else {
                        return $this->respond($this->notFoundStatus, 'Course not found', []);
                    }
                } else {
                    return $this->respond($this->badRequestStatus, 'School is not a basic school', []);
                }
            }
        }
        return $this->respond($this->notFoundStatus, 'School not found', []);
    }

	/**
     * Get all Courses
     *
     * @return [object]
     */
    public function list_department_course($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$department_id = $id;
		if($department_id != '') {
			$getDepartment = Department::where('uuid', '=', $department_id)->where('status', 1)->select('id')->first();
			if(!empty($getDepartment)) {
				$courses = DB::table('courses')->where('courses.status', 1)->whereNull('courses.deleted_at')->where('department_id', $getDepartment->id)
						->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
						->leftJoin('departments', 'departments.id', '=', 'courses.department_id')
						->select("courses.uuid as id", "courses.name",  "departments.uuid as department_id", 'schools.uuid as school_id')
						->orderBy('courses.name', 'ASC')->get()->toArray();

				return $this->respond($this->successStatus, '', $courses);
			}
		}
		return $this->respond($this->notFoundStatus, 'Department not found', []);
    }

	/**
     * Get details of an course
     *
     * @return [object]
     */
    public function course_details($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$course = DB::table('courses')->where(['courses.status' => 1, 'courses.uuid' => $id])
            ->whereNull('courses.deleted_at')
            ->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
            ->leftJoin('departments', 'departments.id', '=', 'courses.department_id')
            ->select("courses.uuid as id", "courses.name", "departments.uuid as department_id", 'schools.uuid as school_id')
					->first();
		if(!empty($course)) {
			$course->department_id = (is_null($course->department_id)) ? "" : $course->department_id;
			return $this->respond($this->successStatus, '', $course);
		}
		return $this->respond($this->notFoundStatus, 'Course not found');
    }

	/**
     * Get all Courses
     *
     * @return [object]
     */
    public function list_course_classes($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}

		$courseId = $id;
		if($courseId != '') {
			$getCourse = Course::where('uuid', '=', $courseId)->where('status', 1)->select('id')->first();
			if(!empty($getCourse)) {
				$classes = DB::table('classes')->where('classes.status', 1)->where('classes.course_id', $getCourse->id)->whereNull('classes.deleted_at')
					->leftJoin('courses', 'courses.id', '=', 'classes.course_id')
					->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
                    ->select("class_name", "classes.uuid", "courses.uuid as course_id", 'schools.uuid as school_id', 'schools.school_name as school_name')
					->orderBy('class_name', 'ASC')->get()->toArray();

				return $this->respond($this->successStatus, '', $classes);
			}
		}
		return $this->respond($this->notFoundStatus, 'Course not found', []);
    }

	/**
     * Get all Classes
     *
     * @return [object]
     */
    public function list_classes($schoolId = '', $courseId = '')
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
        $getSchool = School::where('schools.uuid', '=', $schoolId)->where('schools.status', 1)->leftJoin('courses', 'schools.id', '=', 'courses.school_id')->select('school_category', 'courses.id as course_id')->first();
        if(!empty($getSchool) && $getSchool->school_category == School::BASIC_SCHOOL || School::BASIC_SCHOOL_OPEN || $data['institution_id'] == School::BASIC_SCHOOL_OPEN || School::JHS_SCHOOL_OPEN) {
            $courseId = $getSchool->course_id;
        }
        $getCourse = Course::where('uuid', '=', $courseId)->where('status', 1)->first();
        if(!empty($getCourse)) {
            //return classes based on courseId
            $classes = Classes::where('classes.status', 1)->where('classes.course_id', $getCourse->id)
                ->leftJoin('courses', 'courses.id', '=', 'classes.course_id')
                ->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
                ->select("class_name", "classes.uuid", "courses.uuid as course_id", 'schools.uuid as school_id', 'schools.school_name as school_name')
                ->orderBy('class_name', 'ASC')->get()->toArray();

            return $this->respond($this->successStatus, '', $classes);
        } else {
            //invalid id
            return $this->respond($this->notFoundStatus, 'Course not found', []);
		}
    }

	/**
     * Get details of a class
     *
     * @return [object]
     */
    public function class_details($id, Request $request)
    {
		$data = $request->all();
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$play_on = date("Y-m-d");
		$classroom = Classes::where('classes.uuid', $id)
							->where(['classes.status' => 1])
							->leftJoin('courses', 'courses.id', '=', 'classes.course_id')
							->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
                            ->select("classes.id", "classes.uuid",
                                "class_name as name", 'courses.name as course_name',
                                'courses.uuid as course_id', 'schools.uuid as school_uuid',
                                'schools.id as school_id', 'schools.is_locked', 'schools.school_category',
                                'schools.school_name')
							->first();
		if(!empty($classroom)) {
			if(isset($data['videoId']) && !empty($data['videoId'])) {
				$defaultVideo = Video::where('videos.uuid', $data['videoId'])
										->where(['videos.status' => 1])
										->leftJoin('periods','periods.id','=','videos.period_id')
										->select('videos.*')
										->first();
				if(!empty($defaultVideo)) {
					$classroom = Classes::where('classes.id', $defaultVideo->class_id)
							->where(['classes.status' => 1])
							->leftJoin('courses', 'courses.id', '=', 'classes.course_id')
							->leftJoin('schools', 'schools.id', '=', 'courses.school_id')
                            ->select("classes.id", "classes.uuid", "class_name as name", 'courses.name as course_name', 'courses.uuid as course_id', 'schools.uuid as school_uuid', 'schools.id as school_id', 'schools.is_locked', 'schools.school_category', 'schools.school_name')
							->first();
				} else {
					return $this->respond($this->notFoundStatus, 'Video not found');
				}
			} else {
				$defaultVideo = Video::where('videos.status', 1)
                                ->leftJoin('periods','periods.id','=','videos.period_id')
								->where('videos.class_id', $classroom->id)
                                ->where('videos.play_on', '<=', $play_on)
                                ->select('videos.*')
                                ->orderBy('videos.play_on', 'desc')
                                ->orderBy('periods.weight','ASC')
                                ->orderBy('periods.id','ASC')
                                ->first();
			}
		}
		if(!empty($classroom) && !empty($defaultVideo)) {
			//check for loggedin user have permission to access a class of a locked school
			$has_access = false;
			if($classroom->is_locked){
				$has_access = false;
				if (Auth::check() && Auth::user()->id) {
					//check if user id amdin or school manager
					if(Auth::user()->hasRole('admin')){
						$has_access = false;
					}
					else if(Auth::user()->hasRole('school') && isset(Auth::user()->profile->school_id))
					{
						if($classroom->school_id == Auth::user()->profile->school_id){
							$has_access = true;
						}
					}

					//check user have this class access
					$has_class = DB::table('student_classes')
										->where('user_id','=',Auth::user()->id)
										->where('class_id','=',$classroom->id)
										->count();
					if($has_class > 0){
						$has_access = true;
					}
				}
			}
			$note_url = $lern_more = '';
			if (!empty($defaultVideo->noteURL())) {
				$note_url = $defaultVideo->noteURL();
			}
			if (!empty($video->article_id)) {
				$lern_more = URL::to('article/' . $video->article_id);
			}
			//pr($classroom);die;
			//pr($defaultVideo);die;
			$class['id'] = $classroom->uuid;
			$class['name'] = $classroom->name;
			$class['full_name'] = $classroom->name;
			if($classroom->school_category != School::BASIC_SCHOOL || School::BASIC_SCHOOL || $data['institution_id'] == School::BASIC_SCHOOL_OPEN || School::JHS_SCHOOL_OPEN) {
				$class['full_name'] .= ' ' . $classroom->course_name;
			}
			$class['course_id'] = $classroom->course_id;
			$class['school_id'] = $classroom->school_uuid;
			$class['school_name'] = $classroom->school_name;
			$class['subject_id'] = $classroom->subject_id;
			$class['subject_name'] = $classroom->subject_name;
			$class['locked'] = $has_access;
			$class['vimeo_url'] = $defaultVideo->video_url;
			$class['vimeo_id'] = $defaultVideo->video_id;
			$class['view_count'] = ($defaultVideo->total_views != '') ? $defaultVideo->total_views : 0;
			$class['video'][0]['id'] = $defaultVideo->uuid;
			$class['video'][0]['class_id'] = $classroom->uuid;
			$class['video'][0]['title'] = $defaultVideo->title;
			$class['video'][0]['topic'] = $defaultVideo->topic->topic_name;
			$class['video'][0]['play_on'] = $defaultVideo->play_on;
			$class['video'][0]['content'] = $defaultVideo->description;
			$class['video'][0]['note_url'] = $note_url;
			$class['video'][0]['learn_more_url'] = $lern_more;
			$class['video'][0]['teacher']['profile_id'] = $defaultVideo->tutor->user_details->uuid;
			$class['video'][0]['teacher']['name'] = $defaultVideo->tutor->fullname;

			$student_id = 0;
			if (Auth::check()) {
				$student_id = Auth::user()->id;
			}
			$this->studentVideo($defaultVideo->id, $student_id);
			if(empty($defaultVideo->total_views)){
				$total_views = 1;
			}else{
				$total_views = $defaultVideo->total_views+1;
			}
			$defaultVideo->total_views = $total_views;
			$defaultVideo->save();

			return $this->respond($this->successStatus, '', $class);
		}
		return $this->respond($this->notFoundStatus, 'Class not found');
    }

    function studentVideo($video_id, $student_id){
		if ($student_id > 0) {
            $StudentVideo = StudentVideo::where([
                        'video_id' => $video_id,
                        'student_id' => $student_id
                    ])
                    ->first();

            if (!empty($StudentVideo->id)) {
                $insertUpdate = $StudentVideo;
                $watchCount = $StudentVideo->video_watch_count + 1;
            } else {
                $insertUpdate = new StudentVideo;
                $watchCount = 1;
            }

            $insertUpdate->video_id = $video_id;
            $insertUpdate->student_id = $student_id;
            $insertUpdate->video_watch_count = $watchCount;
            $insertUpdate->save();

            $studentHistory = new StudentHistory;
            $studentHistory->student_id = $student_id;
            $studentHistory->video_id = $video_id;
            $studentHistory->save();

            return $this->respond($this->successStatus, '');
        }
        return $this->respond($this->notFoundStatus, 'Student not found');
    }


	/**
     * Get all Classes
     *
     * @return [object]
     */
    public function list_lessons($class_id, $user_id, Request $request) {

        $defaultDate = date("Y-m-d");

        $comp = '<=';
        if( ($request->has('play_on'))) {
            $defaultDate = date("Y-m-d", strtotime($request->query('play_on')));
            $comp = '=';
        }

        $page_no = request('page_no');

        $setting = Setting::where('key_name','records_per_page')->first();

        $limit = $setting->val;
        $limit = 200;
        $page= 1;

        if (isset($page_no) && $page_no != ''){
            $page = $page_no;
        }

        $offset = ($page-1) * $limit;
        $user = User::whereUuid($user_id)->first();

        $class_videos = Video::with([
            "classDetail:id,uuid,class_name,course_id,status",
            "Period:id,uuid,title,weight",
            "subject:id,uuid,subject_name",
            "tutor:id,uuid,first_name",
            "note",
            "topic:id,uuid,topic_name,weight","Quiz" => function ($quiz) {
            return $quiz->with(["question_quiz" => function ($ques_quiz) {
                return $ques_quiz->with(["questions" => function ($ques) {
                    return $ques->with(["options:id,title,question_bank_id,school_id"])
                        ->select("id","question","num_of_options","choice_id",
                        "active_status","subject_id","class_id","school_id","type", "has_image","image");
                }])->select("id","q_quiz_id","question_bank_id")->get();
            }])->get();
        }])->whereHas("classDetail", function($q) use($class_id){
            $q->where("uuid",$class_id); })
            ->whereHas("period", function($q){
                $q->orderBy('periods.weight','ASC'); }
                )
            ->where('videos.status', '=', '1')
            ->where('videos.play_on', $comp, $defaultDate)
            ->limit($limit)
            ->offset($offset)
            ->orderBy('videos.play_on', 'desc')
            ->get();


         $quizzesTaken =  StudentQuiz::where('user_id', $user->id)
            ->leftJoin('student_quiz_history', 'students_quiz.id', '=', 'students_quiz.id')
            ->groupBy('quiz_id')
            ->distinct('quiz_id')
            ->pluck("quiz_id")
            ->toArray();

        $quizTaken_collection = collect($quizzesTaken);
        $video_quiz_taken_id = null;

        $notesUrl = url('/') . '/';
        $notesPath = public_path('/');

        $collection = new Collection();

        $allVideos = [];

        foreach($class_videos as $k => $v) {
            $quiz_taken_status = isset($v->quiz) ? $quizTaken_collection->contains($v->quiz->id) : false;
            $has_quiz = isset($v->quiz) ? true : false;
            $video_quiz_taken_id = $quiz_taken_status ? $v->quiz->id : null;
            $student_quiz_marks = isset($v->quiz) ? intval($v->get_student_mark($video_quiz_taken_id, $v->subject_id)) : null;
            $quiz_pass_marks = isset($v->quiz) ? intval($v->get_pass_mark($video_quiz_taken_id)) : null;
            $next_video_module = isset($v->period->weight) ? $v->period->weight+1 : null;
            $quiz_passed = $student_quiz_marks >= $quiz_pass_marks;

            if ((isset($v->quiz))) {
                foreach ($v->quiz->question_quiz as $ques_quiz) {
                    $options = new Collection();
                    if ($ques_quiz->questions->type != "true-false") {

                        if ($ques_quiz->questions->options) {
                            foreach ($ques_quiz->questions->options as $key => $ques_option) {
                                $options->push (
                                    [
                                        "value" => $key + 1,
                                        "detail" => $ques_option->title,
                                        "correct" => $ques_quiz->questions->choice_id === $key + 1
                                    ]
                                );
                            }
                        }

                        $collection->push([
                            "text" => $ques_quiz->questions->question,
                            "options" => $options,
                            "type" => $ques_quiz->questions->type,
                        ]);

                    } else {

                        $options_array = array(
                             [
                                "value" => 1,
                                "detail" => "true",
                                "correct" => $ques_quiz->questions->choice_id === 1
                            ],
                             [
                                "value" => 2,
                                "detail" => "false",
                                "correct" => $ques_quiz->questions->choice_id === 2
                            ],
                        );

                        $collection->push([
                            "text" => $ques_quiz->questions->question,
                            "options" => $options_array,
                            "type" => $ques_quiz->questions->type
                        ]);
                    }
                }
            }

            $s3_url = "https://xtraclassnotes.s3.us-west-2.amazonaws.com/";
//            https://xtraclassnotes.s3.us-west-2.amazonaws.com/notes/J9cVqzllPvKq5lL9BhB8trTBmtOZhF4P5v82vhVr.pdf

            $allVideos[$k]["lesson_id"] =  $v->uuid;
            $allVideos[$k]["class_id"] = $v->classDetail->uuid;
            $allVideos[$k]["period_id"] = $v->period->uuid;
            $allVideos[$k]["period_title"] = $v->period->title;
            $allVideos[$k]["subject"] = $v->subject->subject_name;
            $allVideos[$k]["subject_id"] = $v->subject->uuid;
            $allVideos[$k]["vimeo_id"] = $v->video_id;
            $allVideos[$k]["video_id"] = $v->video_id;
            $allVideos[$k]["mobile_video_url"] = $v->video_url;
            $allVideos[$k]["teacher_id"] = $v->tutor_id;
            $allVideos[$k]["teacher_name"] = "";
            $allVideos[$k]["starts_at"] = isset($v->play_on) ? $v->play_on : "";
            $allVideos[$k]["topic_name"] = isset($v->topic) ? $v->topic->topic_name : "";
            $allVideos[$k]['note_id'] = isset($v->note) ? $v->note->id : 0;
            $allVideos[$k]['file_url'] = isset($v->note) ? $v->note->file_url : "";
            $allVideos[$k]['storage'] = isset($v->note) ? $v->note->storage : "";
            $allVideos[$k]['notes_url'] = $allVideos[$k]['file_url'] != "" ? $s3_url.$allVideos[$k]['file_url']:"";
//            $allVideos[$k]['notes_url'] = '';
            $allVideos[$k]['quiz_id'] =  isset($v->quiz) ? $v->quiz->id : null;
            $allVideos[$k]['questions']  = isset($v->quiz) ? ($collection) : null;
            $allVideos[$k]["quiz_taken"] = $quiz_taken_status;
            $allVideos[$k]["has_quiz"] = $has_quiz;
            $allVideos[$k]["quiz_passed"] = isset($v->quiz) ? $quiz_passed : null;
            $allVideos[$k]["video_quiz_taken_id"] = isset($v->quiz) ? $video_quiz_taken_id : null;
            $allVideos[$k]["quiz_marks"] =  isset($v->quiz) ? intval($v->get_student_mark($video_quiz_taken_id, $v->subject_id)) : null;
            $allVideos[$k]["pass_mark"] = isset($v->quiz) ? intval($v->get_pass_mark($video_quiz_taken_id)) : "";
            $allVideos[$k]["next_video_weight"] = $next_video_module;

            $allVideos[$k]["video_info"] = VideoDetail::whereVideoId($v->id)->first()? SiteHelpers::getVideoData($v->video_id) : SiteHelpers::getVideoDataObject();
//
//            if($v['note_id'] > 0) {
//                    if(Storage::disk($v['storage'])->exists($v['file_url'])){
//                        $allVideos[$k]['notes_url'] = Storage::disk($v['storage'])->url($v['file_url']);
//                    }
//
//            }
//            unset($allVideos[$k]['note_id'], $allVideos[$k]['storage'], $allVideos[$k]['file_url']);

        }

        return response()->json([
            'statusCode' => 200,
            'message' => "success",
            'data' => $allVideos
        ]);
    }

    public function list_lessons_2($id, $user_id, Request $request)
    {
        $data = $request->all();
        $lang = request('lang');
        if(!$lang) {
            $lang = 1;
        }

        $defaultDate = date("Y-m-d");
        $comp = '<=';
        if(isset($data['play_on']) && !empty($data['play_on'])) {
            $defaultDate = date("Y-m-d", strtotime($data['play_on']));
            $comp = '=';
        }

        $class_id = $id;
        $page_no = request('page_no');

        $setting = Setting::where('key_name','records_per_page')->first();

        $limit = $setting->val;
        $limit = 200;
        $page = 1;
        if(isset($page_no) && $page_no != '') {
            $page = $page_no;
        }
        $offset = ($page-1) * $limit;

        $idObj = $this->getId($class_id, Classes::class);

//         $student = Student::whereUuid($student_id)->first();
         $user = User::whereUuid($user_id)->first();

        $video_builder = Video::leftJoin('periods','periods.id','=','videos.period_id');

        $quizzesTaken = $this->getQuizzesTaken($user);

        $quizTaken_collection = collect($quizzesTaken);
        $video_quiz_taken_id = null;

        if (!empty($idObj)) {
			$defaultVideo = array();
			if(1 == 1 || !empty($defaultVideo)) {
				//$defaultDate = $defaultVideo->play_on;

//                return json_encode(["data" => [$idObj,$comp,$defaultDate,$limit,$offset],]);
                $class_videos = $this->getClassVideos($idObj, $comp, $defaultDate, $limit, $offset);

				$notesUrl = url('/') . '/';
				$notesPath = public_path('/');

                $collection = new Collection();

                $allVideos = [];

                if(!empty($class_videos) && count($class_videos) > 0) {
					foreach($class_videos as $k => $v) {
					    $quiz_taken_status = isset($v->quiz) ? $quizTaken_collection->contains($v->quiz->id) : false;
                        $has_quiz = isset($v->quiz) ? true : false;
                        $video_quiz_taken_id = $quiz_taken_status ? $v->quiz->id : null;
                        $student_quiz_marks = isset($v->quiz) ? intval($v->get_student_mark($video_quiz_taken_id, $v->subject_id)) : null;
                        $quiz_pass_marks = isset($v->quiz) ? intval($v->get_pass_mark($video_quiz_taken_id)) : null;
                        $next_video_module = isset($v->period->weight) ? $v->period->weight+1 : null;
                         $quiz_passed = $student_quiz_marks >= $quiz_pass_marks;

                        if (($v->quiz)) {
                            foreach ($v->quiz->question_quiz as $ques_quiz) {
                                $options = new Collection();
//                                $options = [];
                                if ($ques_quiz->questions->type != "true-false") {

                                    if ($ques_quiz->questions->options) {
                                        foreach ($ques_quiz->questions->options as $key => $ques_option) {
                                            $options->push (
                                                  [
                                                    "value" => $key + 1,
                                                    "detail" => $ques_option->title,
                                                    "correct" => $ques_quiz->questions->choice_id === $key + 1
                                                ]
                                            );
                                        }
                                    }

                                    $collection->push([
                                        "text" => $ques_quiz->questions->question,
                                        "options" => $options,
                                        "type" => $ques_quiz->questions->type,
                                    ]);

                                } else {

                                    $collection->push([
                                        "text" => $ques_quiz->questions->question,
                                        [
                                            0 => [
                                            "option " =>  [
                                                "value" => 1,
                                                "detail" => "true",
                                                "correct" => $ques_quiz->questions->choice_id === 1
                                            ],
                                                ],
                                            1=> [
                                            "option" => [
                                                "value" => 2,
                                                "detail" => "false",
                                                "correct" => $ques_quiz->questions->choice_id === 2
                                            ],
                                            "type" => $ques_quiz->questions->type,
                                        ]
                                ],
                                    ]);
                                }
                            }
                        }

                        $allVideos[$k]["lesson_id"] = $v->uuid;
                        $allVideos[$k]["class_id"] = Classes::find($v->class_id)->uuid;
                        $allVideos[$k]["period_id"] = Period::find($v->period_id)->uuid;
                        $allVideos[$k]["period_title"] = isset($v->period)? $v->period->title : null;
                        $allVideos[$k]["subject"] = Subject::find($v->subject_id)->subject_name;
                        $allVideos[$k]["subject_id"] = $v->subject_id;
                        $allVideos[$k]["vimeo_id"] = $v->video_id;
                        $allVideos[$k]["teacher_id"] = $v->tutor_id;
                        $allVideos[$k]["teacher_name"] = $v->teacher_name;
                        $allVideos[$k]["starts_at"] = $v->play_on;
                        $allVideos[$k]["topic_name"] = $v->topic_name;
                        $allVideos[$k]['notes_url'] = '';
                        $allVideos[$k]['quiz_id'] =  isset($v->quiz)? $v->quiz->id:null;
                        $allVideos[$k]['questions']  = isset($v->quiz) ? ($collection) : null;
                        $allVideos[$k]["quiz_taken"] = $quiz_taken_status;
                        $allVideos[$k]["has_quiz"] = $has_quiz;
                        $allVideos[$k]["quiz_passed"] = $v->quiz? $quiz_passed : null;
                        $allVideos[$k]["video_quiz_taken_id"] = $v->quiz ? $video_quiz_taken_id : null;
                        $allVideos[$k]["quiz_marks"] =  $v->quiz? intval($v->get_student_mark($video_quiz_taken_id, $v->subject_id)) : null;
                        $allVideos[$k]["pass_mark"] = intval($v->get_pass_mark($video_quiz_taken_id));
                        $allVideos[$k]["next_video_weight"] = $next_video_module;
//                        $class_videos[$k]["next_video_module"] = $next_video_module_object


						if($v['note_id'] > 0 && $v['file_url'] != '') {
							if($v['storage'] == 'local') {
								if(file_exists($notesPath . $v['file_url']) && is_file($notesPath . $v['file_url'])) {
                                    $allVideos[$k]['notes_url'] = $notesUrl . '' . $v['file_url'];
								}
							} else if($v['storage'] == 's3') {
								if(Storage::disk($v['storage'])->exists($v['file_url'])){
                                    $allVideos[$k]['notes_url'] = Storage::disk($v['storage'])->url($v['file_url']);
								}
							}
						}
						unset($allVideos[$k]['note_id'], $allVideos[$k]['storage'], $allVideos[$k]['file_url']);

					}
				}

				return $this->respond($this->successStatus, '', $allVideos);
			}
			return $this->respond($this->badRequestStatus, '', []);
		} else {
			return $this->respond($this->notFoundStatus, 'Class not found', []);
		}
    }

    /**
     * Get all Classes
     *
     * @return [object]
     */
    public function list_lessons_old($id, Request $request)
    {
        $data = $request->all();
        $lang = request('lang');
        if(!$lang) {
            $lang = 1;
        }

//        /*$play_on = date("Y-m-d");
        $comp = '<=';
        if(isset($data['play_on']) && !empty($data['play_on'])) {
            $play_on = date("Y-m-d", strtotime($data['play_on']));
            $comp = '=';

        $defaultDate = date("Y-m-d");
        $comp = '<=';
        if(isset($data['play_on']) && !empty($data['play_on'])) {
            $defaultDate = date("Y-m-d", strtotime($data['play_on']));
            $comp = '=';
        }

        $class_id = $id;
        $page_no = request('page_no');

        $setting = Setting::where('key_name','records_per_page')->first();

        $limit = $setting->val;
        $limit = 200;
        $page = 1;
        if(isset($page_no) && $page_no != '') {
            $page = $page_no;
        }
        $offset = ($page-1) * $limit;

        $idObj = $this->getId($class_id, Classes::class);
        if (!empty($idObj)) {
            $defaultVideo = array();
//            /*$defaultVideo = Video::where('videos.status', 1)
//                                ->leftJoin('periods','periods.id','=','videos.period_id')
//                                ->where('videos.class_id', $idObj->id)
//                                ->where('videos.play_on', $comp, $play_on)
//                                ->select('videos.play_on')
//                                ->orderBy('videos.play_on', 'desc')
//                                ->orderBy('periods.weight','ASC')
//                                ->orderBy('periods.id','ASC')
//                                ->first(); */
//            if(1 == 1 || !empty($defaultVideo)) {
//                //$defaultDate = $defaultVideo->play_on;
//
            $class_videos = Video::where('videos.class_id', $idObj->id)
                ->where('videos.status','=','1')
                ->where('videos.play_on', $comp, $defaultDate)
                ->leftJoin('classes', 'videos.class_id','=','classes.id')
                ->leftJoin('periods', 'videos.period_id','=','periods.id')
                ->leftJoin('subjects', 'videos.subject_id','=','subjects.id')
                ->leftJoin('topics', 'videos.topic_id','=','topics.id')
                ->leftJoin('tutors', 'videos.tutor_id','=','tutors.user_id')
                ->leftJoin('users as u', 'tutors.user_id','=','u.id')
                ->leftJoin('notes', 'videos.note_id','=','notes.id')
                ->selectRaw("videos.uuid as lesson_id,
            classes.uuid as class_id,periods.uuid as period_id,
            periods.title as period_title,
            subjects.subject_name as subject,
                        videos.video_id as vimeo_id,videos.note_id,notes.file_url,
            notes.storage,u.uuid as teacher_id,
            u.name as teacher_name,play_on as starts_at, topics.topic_name")
                ->limit($limit)
                ->offset($offset)
                ->orderBy('videos.play_on', 'desc')
                ->orderBy('periods.weight','ASC')
                ->orderBy('periods.id','ASC')
                //->orderBy('student_favourites.id', 'DESC')
                ->get()
                ->toArray();

                $favResponse = array();
                $notesUrl = url('/') . '/';
                $notesPath = public_path('/');

                if(!empty($class_videos) && count($class_videos) > 0) {
                    foreach($class_videos as $k => $v) {
                        //$class_videos[$k]['id'] = $v['lesson_id'];
                        //$class_videos[$k]['topic'] = $v['topic_name'];
                        //$class_videos[$k]['title'] = $v['subject'];
                        $class_videos[$k]['notes_url'] = '';
                        if($v['note_id'] > 0 && $v['file_url'] != '') {
                            if($v['storage'] == 'local') {
                                if(file_exists($notesPath . $v['file_url']) && is_file($notesPath . $v['file_url'])) {
                                    $class_videos[$k]['notes_url'] = $notesUrl . '' . $v['file_url'];
                                }
                            } else if($v['storage'] == 's3') {
                                if(Storage::disk($v['storage'])->exists($v['file_url'])){
                                    $class_videos[$k]['notes_url'] = Storage::disk($v['storage'])->url($v['file_url']);
                                }
                            }
                        }
                        unset($class_videos[$k]['note_id'], $class_videos[$k]['storage'], $class_videos[$k]['file_url']);
                    }
                }
                return $this->respond($this->successStatus, '', $class_videos);
            }
            return $this->respond($this->badRequestStatus, '', []);
        } else {
            return $this->respond($this->notFoundStatus, 'Class not found', []);
        }
    }

	/**
     * Get all Classes
     *
     * @return [object]
     */
    public function list_periods($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$class_id = $id;
		$idObj = $this->getId($class_id, Classes::class);
		if(!empty($idObj)) {
			$db = DB::table('periods')->where('status', 1)->where('class_id', $idObj->id)->whereNull('deleted_at');
		} else {
			return $this->respond($this->notFoundStatus, 'Class not found', []);
		}
		$periods = $db->select('uuid as id', 'weight as number', 'title')->orderBy('title', 'ASC')->get()->toArray();

		return $this->respond($this->successStatus, '', $periods);
    }

	/**
     * Get all Classes
     *
     * @return [object]
     */
    public function list_years($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$class_id = $id;
		$idObj = $this->getId($class_id, Classes::class);
		if(!empty($idObj)) {
            $years = Video::where('status', 1)->where('class_id', $idObj->id)->selectRaw('YEAR(play_on) AS year')->groupBy('year')->get();
		} else {
			return $this->respond($this->notFoundStatus, 'Class not found', []);
		}

		return $this->respond($this->successStatus, '', $years);
    }

	/**
     * Get all Classes
     *
     * @return [object]
     */
    public function list_semesters($id, Request $request)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$class_id = $id;
		$year = $request->year;
		$idObj = $this->getId($class_id, Classes::class);
		if(!empty($idObj)) {
		    $class = Classes::where('id', $idObj->id)->with('course.school')->first();
            $semesterNameArray = $class->course->school->semesterNameArray();
            $semesters = SchoolSemester::where('school_id', $class->course->school_id)->where(function ($query) use ($year) {
                $query->where('date_begin', '<=', $year . '-12-31')
                      ->orWhere('date_end', '>=', $year . '-01-01');
            })->get()->map(function ($semester) use ($semesterNameArray) {
                return [
                    'id' => $semester->id,
                    'name' => $semesterNameArray[(int)$semester->semester],
                    'date_begin' => $semester->date_begin,
                    'date_end' => $semester->date_end
                ];
            });
		} else {
			return $this->respond($this->notFoundStatus, 'Class not found', []);
		}

		return $this->respond($this->successStatus, '', $semesters);
    }

	/**
     * Get all Classes
     *
     * @return [object]
     */
    public function get_semester_video_dates($id, Request $request)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$class_id = $id;
		$idObj = $this->getId($class_id, Classes::class);
		if(!empty($idObj)) {
		    $semester = SchoolSemester::where('id', $request->semester)->first();
            $date_range = Video::where('class_id', $idObj->id)
                               ->whereBetween('play_on', [$semester->date_begin, $semester->date_end])
                               ->where('play_on','<=',date('Y-m-d'))
                               ->where('status',1)
                               ->orderBy('play_on','asc')
                               ->pluck('play_on');
		} else {
			return $this->respond($this->notFoundStatus, 'Class not found', []);
		}

		return $this->respond($this->successStatus, '', $date_range);
    }

    public function archive_search($id, Request $request) {
        $lang = request('lang');
        if(!$lang) {
            $lang = 1;
        }
        $class_id = $id;
        $idObj = $this->getId($class_id, Classes::class);
        if(!empty($idObj)) {
            $data = Video::where('videos.class_id', $idObj->id)
                         ->where('videos.play_on', $request->play_on)
                         ->leftJoin('classes', 'videos.class_id','=','classes.id')
                         ->leftJoin('periods', 'videos.period_id','=','periods.id')
                         ->leftJoin('subjects', 'videos.subject_id','=','subjects.id')
                         ->leftJoin('topics', 'videos.topic_id','=','topics.id')
                         ->leftJoin('tutors', 'videos.tutor_id','=','tutors.user_id')
                         ->leftJoin('users as u', 'tutors.user_id','=','u.id')
                         ->leftJoin('notes', 'videos.note_id','=','notes.id')
                         ->select('videos.uuid as lesson_id', 'classes.uuid as class_id', 'periods.uuid as period_id', 'periods.title as period_title', 'subjects.subject_name as subject', 'videos.video_id as vimeo_id', 'videos.note_id', 'notes.file_url', 'notes.storage', 'u.uuid as teacher_id', 'play_on as starts_at', 'topics.topic_name')
                         ->first()
                         ->toArray();
        } else {
            return $this->respond($this->notFoundStatus, 'Class not found');
        }

        return $this->respond($this->successStatus, '', $data);
    }

	/**
     * Get all Courses
     *
     * @return [object]
     */
    public function list_classrooms(Request $request)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$school_id = request('school_id');
		$course_id = request('course_id');
		$class_id = request('class_id');

		$db = Classroom::where('status', 1)->with('school', 'course', 'singleClass');
		if(!is_null($school_id) && $school_id != '') {
			$db->where('school_id', $school_id);
		}
		if(!is_null($course_id) && $course_id != '') {
			$db->where('course_id', $course_id);
		}
		if(!is_null($class_id) && $class_id != '') {
			$db->where('class_id', $class_id);
		}
		$classrooms = $db->orderBy('classroom_name', 'ASC')->get()->toArray();

		return $this->respond($this->successStatus, '', $classrooms);
    }

	/**
     * Get Classroom details
     *
     * @return [object]
     */
	 public function classroom_detail($id) {
		$classroom_id = $id;

		$classroom = Classroom::where('classrooms.uuid', $classroom_id)->with('school', 'course', 'singleClass')
		->first();
		if(!empty($classroom)) {
			$classroom = $classroom->toArray();
			$classroom['videos'] = Classroom::find($classroom['id'])->videos;
		} else {
            return $this->respond($this->badRequestStatus, '');
        }


		return $this->respond($this->successStatus, '', $classroom);
	 }

	/**
     * Get all Questions of a video
     *
     * @return [object]
     */
    public function list_questions($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}

		$video = Video::where('uuid', $id)->first();
		if(!empty($video)) {
			$video = $video->toArray();
			$video['questions'] = Question::where('video_id', $video['id'])->with('childrenAccounts')->get()->toArray();
		} else {
            return $this->respond($this->notFoundStatus, 'Video not found', $video);
        }

		return $this->respond($this->successStatus, '', $video);
    }

	/**
     * step 2 of registration
     * @param Request $request
     * @return Validator object
     */
    public function save_question(Request $request) {
        $data = $request->all();
		$user_id = Auth::user()->id;

		$validator = Validator::make($data, [
            'content' => 'required|string|sanitizeScripts'
        ],[
			"content.required" => "The question field is required.",
			'content.sanitize_scripts' => 'Invalid value entered for Content field.',
		]);

        if ($validator->fails()) {
            return $this->throwValidation($validator->messages()->first());
        }

        if (User::where('id', $user_id)->exists()) {
			$video = Video::where('id', $data['video_id'])->select('id', 'class_id')->first();
			if(!empty($video)) {
				$parent_id = ($data['parent_id'] == "") ? 0 : $data['parent_id'];
				$type = ($data['type'] == "") ? 'question' : $data['type'];
				$insertQuestion = new Question();
				$insertQuestion->content = $data['content'];
				$insertQuestion->type = $type;
				$insertQuestion->parent_id = $parent_id;
				$insertQuestion->sender_id = $user_id;
				$insertQuestion->class_id = $video->class_id;
				$insertQuestion->video_id = $data['video_id'];
				$insertQuestion->save();

				return $this->respond($this->successStatus, '');
			} else {
				return $this->respond($this->notFoundStatus, 'Video not found');
			}
        } else {
            return $this->respond($this->unauthorizedStatus, 'You are not authenticated');
        }
    }

	public function getId($uuid, $model) {
		return $model::where('uuid', $uuid)->select('id')->first();
	}

    public static function getIdStatic($uuid, $model) {
        return $model::where('uuid', $uuid)->select('id')->first();
    }

    /**
     * Get all Subjects of a class
     *
     * @return [object]
     */
    public function list_subjects($id)
    {
        $lang = request('lang');
        if(!$lang) {
            $lang = 1;
        }

        $class = Classes::where('uuid', $id)->first();
        if(!empty($class)) {
            $subjects = Subject::where('class_id', $class->id)->get()->toArray();
        } else {
            return $this->respond($this->notFoundStatus, 'Class not found', []);
        }

        return $this->respond($this->successStatus, '', $subjects);
    }

    /**
     * Get all Topics of a subject
     *
     * @return [object]
     */
    public function list_topics($id)
    {
        $lang = request('lang');
        if(!$lang) {
            $lang = 1;
        }

        $subject = Subject::where('id', $id)->first();
        if(!empty($subject)) {
            $topics = $subject->topics->toArray();
        } else {
            return $this->respond($this->notFoundStatus, 'Subject not found', []);
        }

        return $this->respond($this->successStatus, '', $topics);
    }


    /**
     * @param $idObj
     * @param string $comp
     * @param bool $defaultDate
     * @param int $limit
     * @param float $offset
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|Collection
     */
    public function getClassVideos($idObj, string $comp, bool $defaultDate, int $limit, float $offset)
    {

    }

    /**
     * @param $user
     * @return mixed
     */
    public function getQuizzesTaken($user)
    {
        $quizzesTaken = StudentQuiz::where('user_id', $user->id)
            ->leftJoin('student_quiz_history', 'students_quiz.id', '=', 'students_quiz.id')
            ->groupBy('quiz_id')
            ->distinct('quiz_id')
            ->pluck("quiz_id", "student_id")
            ->toArray();
        return $quizzesTaken;
    }

    public function listAllSchools()
    {

//        if (!$this->checkAuthorized()){
//            return response()->json(["message" => "unauthorized","data" => ""],401);
//        }
        $is_per_page = false;

        $SENIOR_HIGH_SCHOOL = "";
         $schools_builder = School::leftJoin("school_categories","school_categories.id","=","schools.school_category")
            ->leftJoin("locations","locations.id","=","schools.location_id")
            ->leftJoin("districts","districts.id","=","locations.district_id")
            ->leftJoin("regions","regions.id","=","locations.region_id")
//            ->where("school_code","<>",'')
//            ->orWhereNotNull("school_code")
            ->orwhere("school_category","2")
            ->orWhere("school_category","3")
//            ->orWhere("school_category","21")
//            ->orWhere("school_category","22")
            ->selectRaw("schools.id,school_name, school_code, gender,form_one,form_two,form_three,
             contact_person,contact_person_number,locations.id as location_id,locations.name as location, districts.name as district,
             regions.id as region_id, regions.name as region,locations.coordinates,sid_status,schools.logo");

         if (\request()->has("list") && \request()->query("list") == "all"){
             $schools = $schools_builder->get();

         }else{
             $schools = $schools_builder->paginate();
             $is_per_page = true;
         }

        $schools = $schools->map(function ($item, $index){

            return [
                "school_id" =>   $item->id,
                "school_name" => !empty($item->school_name) ? $item->school_name : "",
                "school_code" => !empty($item->school_code) ? $item->school_code : "",
                "gender"      => !empty($item->gender) ? $item->gender : "",
                "location" =>    !empty($item->location) ? $item->location : "",
                "district" =>    !empty($item->district) ? $item->district: "",
                "region"   =>    !empty($item->region) ?   $item->region : "",
                "coordinates" =>  !empty($item->coordinates) || !is_null($item->coordinates) ? $item->coordinates : "",
                "lat"         =>  !empty($item->coordinates) ? explode(",",$item->coordinates)[0]:"",
                "lng"         =>  !empty($item->coordinates) ? explode(",",$item->coordinates)[1]:"",
                "form_one_count" => $item->form_one != null ? $item->form_one : 0,
                "form_two_count" => $item->form_two != null ? $item->form_two : 0,
                "form_three_count" => $item->form_three != null ? $item->form_three : 0,
                "population"       => $item->form_one + $item->form_two + $item->form_three,
                "contact_person"    => !empty($item->contact_person) ? $item->contact_person : "",
                "contact_person_number" => !empty($item->contact_person_number) ? $item->contact_person_number : "",
                "sid_status"            => $item->sid_status,
//                "school_logo"           => !empty($item->logo) ? "/uploads/schools/".$item->logo : ""
                "school_logo"           => file_exists(public_path()."/uploads/schools/$item->school_code.png") ? "/uploads/schools/".$item->school_code.".png" : "",
                "project_officers"           => []
            ];
        });



         if ($is_per_page){
             $paginated = $schools_builder->paginate();
             return response()->json(["data" => $schools,
                 "current_page" =>$paginated->currentPage(),
                 "per_page" => $paginated->perPage(),
                 "previous_page_url" =>$paginated->previousPageUrl(),
                 "next_page_url" => $paginated->nextPageUrl(),
                 "total" => $paginated->total()
             ]);
         }else{
             $target    = School::orWhere("school_category","2")->orWhere("school_category","3")
                 ->selectRaw("sid_status")->where("sid_status","target")->count();
             $delivered = School::orWhere("school_category","2")->orWhere("school_category","3")
                 ->selectRaw("sid_status")->where("sid_status","delivered")->count();
             $printed   = School::orWhere("school_category","2")->orWhere("school_category","3")
                 ->selectRaw("sid_status")->where("sid_status","printed")->count();
             $submitted = School::orWhere("school_category","2")->orWhere("school_category","3")
                 ->selectRaw("sid_status")->where("sid_status","submitted")->count();
             $totalCompletion = 0;
             $started   =  School::orWhere("school_category","2")->orWhere("school_category","3")
                 ->selectRaw("sid_status")->where("sid_status","started")->count();
             $totalSchools =  $schools_builder->count();
             $keys =  [
                 "target" => $target,
                 "delivered" => $delivered,
                 "printed" => $printed,
                 "submitted" => $submitted,
                 "totalCompletion" => $totalCompletion,
                 "started" => $started,
                 "totalSchools" => $totalSchools
             ];

             return response()->json(["data" => $schools, "keys" => ($keys)]);
         }
    }

    /**
     * @OA\Get(
     *     path="/api/sid/schools/school-details-by-code/{code}",
     *     tags={"School Details by School Code"},
     *     summary="Returns school details by code",
     *     description="Returns school details by code",
     *     operationId="getSchoolDetailsByCode",
     *      @OA\Parameter(
     *         name="code",
     *         in="path",
     *         required=true,
     *         description="code of the school that needs to be fetched",
     *         @OA\Schema(
     *             type="string",
     *             minimum=1
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid ID supplied"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="School not found"
     *     )
     * ),
     *     @OA\Response(
     *         response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\AdditionalProperties(
     *                  type="string"
     *              )
     *          )
     *     ),
     *     security={
     *         {"bearerAuth": {}}
     *     }
     * )
     */
    public function schoolDetailsByCode($code): JsonResponse
    {
        if (!$this->checkAuthorized()){
            return response()->json(["message" => "unauthorized","data" => ""],401);
        }

        $school = School::leftJoin("school_categories","school_categories.id","=","schools.school_category")
            ->leftJoin("locations","locations.id","=","schools.location_id")
            ->leftJoin("districts","districts.id","=","locations.district_id")
            ->leftJoin("regions","regions.id","=","locations.region_id")
            ->where("school_code",$code)
            ->selectRaw("schools.id,school_name, school_code, gender,form_one,form_two,form_three,
              contact_person,contact_person_number,
             locations.id as location_id,locations.name as location, locations.coordinates, districts.name as district,
             regions.id as region_id, regions.name as region,sid_status")
            ->first();
        if ($school){

            return response()->json(["message" => "success","data" => $school]);
        }

        return response()->json(["message" => "No details found","data"=> ""],400);
    }


    public function updateSchoolDetailsByCode(Request $request): JsonResponse
    {
        if (!$this->checkAuthorized()){
            return response()->json(["message" => "unauthorized","data" => ""],401);
        }
        $code   = $request->school_code;
        $school =  School::whereSchoolCode($code)->first();
        try {
            if ($school) {
                if ($request->has("sid_status")){
                    $school->update($request->only("sid_status"));
                }

                if($request->has("contact_person") || $request->has("contact_person_number")) {
                    $school->update($request->only("contact_person", "contact_person_number"));
                }


                return response()->json(["message" => "success","data" =>$school],200);
            }

        }catch (\Exception $exception){
            return response()->json(["message" => "school details not found!"], 400);
        }

    }

    public function schoolBySchoolCode(Request $request): JsonResponse
    {
        if (!$this->checkAuthorized()){
            return response()->json(["message" => "unauthorized","data" => ""],401);
        }
        $code   = $request->school_code;
        $school =  School::whereSchoolCode($code)->first();

        return response()->json(["message" => "success","data" =>$school],200);

    }

    public function getSidStudents($school_code){
        if (!$this->checkAuthorized()){
            return response()->json(["message" => "unauthorized","data" => ""],401);
        }

        $students = DB::table("sid_students")
            ->whereJsonContains("info",["school_code" => $school_code])->get();

        $students = $students->map(function ($item,$index){
            return [
                "id" => $item->id,
                "info" => json_decode($item->info)
            ];
        });

        return response()->json(["data" => json_decode($students,true,JSON_UNESCAPED_SLASHES)]);
    }

    public function getStudentByStudentId($student_id): JsonResponse
    {
        if (!$this->checkAuthorized()){
            return response()->json(["message" => "unauthorized","data" => ""],401);
        }

        $students = DB::table("sid_students")
            ->whereJsonContains("info",["shs_number" => $student_id])->get();

        $students = $students->map(function ($item,$index){
            return [
                "id" => $item->id,
                "info" => json_decode($item->info)
            ];
        });

        return response()->json(["data" => json_decode($students,true,JSON_UNESCAPED_SLASHES)]);
    }


    public function postUpdateStudentByStudentId(Request $request, $student_id){
        if (!$this->checkAuthorized()){
            return response()->json(["message" => "unauthorized","data" => ""],401);
        }

            $dateOfBirth = $request->date_of_birth;
            $guardian_number = $request->guardian_number;

            $student = \App\SidStudent::where('info->shs_number',$student_id)->first();

            if ($student != null && $request->has("date_of_birth")  ) {
                    DB::table('sid_students')
                        ->where('info->shs_number',$student_id)
                        ->update([
                            'info->date_of_birth' => $dateOfBirth
                ]);
            }

            if ($student != null  && $request->has("guardian_number")) {

                DB::table('sid_students')
                    ->where('info->shs_number',$student_id)
                    ->update([
                        'info->guardian_number' => $guardian_number
                    ]);
            }

            if (!$student){
                return response()->json(["message" => "student does not exist!", "data"=> $student],404);
            }else{
                return response()->json(["message" => "success", "data" => $student],200);
            }
    }

    public function postAddNewStudent(Request $request){

        $array = [];
        $array["school_code"] = $request->school_code;
        $array["school"] = $request->school;
        $array["region"] = $request->region;
        $array["index_number_jhs"] = $request->index_number_jhs;
        $array["student_name"] = $request->student_name;
        $array["year_group"] = $request->year_group;
        $array["date_of_birth"] = $request->date_of_birth;
        $array["residential_status"] = $request->residential_status;
        $array["student_mobile_number"] = $request->student_mobile_number;
        $array["shs_number"] = $request->shs_number;
        $array["other_id"] = $request->other_id;

        SidStudent::create([
            "info" => json_encode($array)
        ]);

        return $request->all();
    }

    private function checkAuthorized(): bool
    {
        $header_token =    base64_decode(\request()->bearerToken());
        if ($header_token === School::SID_PROJECT_TOKEN){
            return $authorized = true;
        }else{
            return $authorized = false;
        }
    }

    public function allLessons($class_id, $user_id, Request $request){
        $defaultDate = date("Y-m-d");

        $comp = '<=';
        if(isset($data['play_on']) && !empty($data['play_on'])) {
            $defaultDate = date("Y-m-d", strtotime($data['play_on']));
            $comp = '=';
        }

        $page_no = request('page_no');

        $setting = Setting::where('key_name','records_per_page')->first();

        $limit = $setting->val;
        $limit = 200;
        $page= 1;

        if (isset($page_no) && $page_no != ''){
            $page = $page_no;
        }

        $offset = ($page-1) * $limit;
        $idObj = $this->getId($class_id, Classes::class);
        $user = User::whereUuid($user_id)->first();

        $class_videos = Video::where('videos.class_id', $idObj->id)
            ->where('videos.status','=','1')
            ->where('videos.play_on', $comp, $defaultDate)
            ->leftJoin('classes', 'videos.class_id','=','classes.id')
            ->leftJoin('periods', 'videos.period_id','=','periods.id')
            ->leftJoin('subjects', 'videos.subject_id','=','subjects.id')
            ->leftJoin('topics', 'videos.topic_id','=','topics.id')
            ->leftJoin('tutors', 'videos.tutor_id','=','tutors.user_id')
            ->leftJoin('users as u', 'tutors.user_id','=','u.id')
            ->leftJoin('notes', 'videos.note_id','=','notes.id')
            ->select('videos.uuid as lesson_id',
                'classes.uuid as class_id', 'periods.uuid as period_id',
                'periods.title as period_title',
                'subjects.subject_name as subject', '
                        videos.video_id as vimeo_id', 'videos.note_id', 'notes.file_url',
                'notes.storage', 'u.uuid as teacher_id',
                'u.name as teacher_name', 'play_on as starts_at', 'topics.topic_name')
            ->limit($limit)
            ->offset($offset)
            ->orderBy('videos.play_on', 'desc')
            ->orderBy('periods.weight','ASC')
            ->orderBy('periods.id','ASC')
            //->orderBy('student_favourites.id', 'DESC')
            ->get()
            ->toArray();

        return $class_videos;
    }

    public function getSelectSchools(Request $request){
        return $schools = School::where('school_name', 'LIKE',  $request->query('q').'%')
            ->orwhere("school_category","2")
            ->orWhere("school_category","3")
            ->orderBy('school_name')->take(25)->get(['id',DB::raw('school_name as text')]);

    }
}
