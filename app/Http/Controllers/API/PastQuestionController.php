<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\PastQuestionGroupResource;
use App\Http\Resources\PastQuestionResource;
use App\Models\Subject;
use App\PastQuestionGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PastQuestionController extends Controller
{
    public function getYears($subject_uuid){
        $subjectObject = Subject::whereUuid($subject_uuid)->first();
        $subject_id = $subjectObject ? $subjectObject->id : null;
        $groups = PastQuestionGroup::whereSubjectId($subject_id)->get();
        return PastQuestionGroupResource::collection($groups);
    }

    public function getPastQuestions($group_id){
        $past_questions = DB::table("past_question_bank")
            ->where("past_question_group_id",$group_id)->get();
        return PastQuestionResource::collection($past_questions);
    }

}
