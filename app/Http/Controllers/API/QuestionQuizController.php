<?php

namespace App\Http\Controllers\API;

use App\Models\QuestionBank;
use App\Models\QuestionQuiz;
use App\Models\QuizQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;


class QuestionQuizController extends APIController
{
    public $questionQuiz, $questionBank, $quizQuestion;
    public function __construct(QuestionQuiz $questionQuiz, QuestionBank $questionBank, QuizQuestion $quizQuestion)
    {
        $this->questionQuiz = $questionQuiz;
        $this->questionBank = $questionBank;
        $this->quizQuestion = $quizQuestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionQuiz = $this->questionQuiz::all();
        return $this->respond($this->successStatus,'', $questionQuiz);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $options[] = $request->questions;

        //return $options;


        $questionQuiz = $this->questionQuiz::create([
            'title' => $request->name,
            'pass_mark' => $request->marks,
            'status' => $request->active_status,
            'subject_id' => $request->subject_id,
        ]);


        foreach ($options as $value) {
            foreach ($value as $val){
                $this->quizQuestion::create([
                    'q_quiz_id' => $questionQuiz->id,
                    'question_bank_id' => $val,
                ]);
            }
        }
        //return $valueArr;
        $data = array(
            'url' => URL::to('/admin/quiz'),
            'success' => true);

        return $this->respond($this->successStatus,'Quiz addition successful',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $options[] = $request->questions;

        //return $options;

        $this->questionQuiz::find($id)
            ->update([
            'title' => $request->name,
            'pass_mark' => $request->marks,
            'status' => $request->active_status,
            'subject_id' => $request->subject_id,
        ]);

        if($this->quizQuestion::where('q_quiz_id',$id)->delete()){
            foreach ($options as $value) {
                foreach ($value as $val){
                    $this->quizQuestion::create([
                        'q_quiz_id' => $id,
                        'question_bank_id' => $val,
                    ]);
                }
            }
        }

        //return $valueArr;
        $data = array(
            'url' => URL::to('/admin/quiz'),
            'success' => true);

        return $this->respond($this->successStatus,'Quiz updated successful',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
