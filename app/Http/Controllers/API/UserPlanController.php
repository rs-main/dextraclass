<?php

namespace App\Http\Controllers\API;

use App\User;
use Carbon\Carbon;
use App\Models\Plans;
use App\Models\UserPlan;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserPlanController extends Controller
{
    public $user, $plans, $student;
    public function __construct(User $user, Plans $plans, Student $student )
    {
        $this->user = $user;
        $this->plans = $plans;
        $this->student = $student;
        // $this->middleware('auth:api');
    }


    public function addUserPlan(Request $request)
    {
        $carbon = new Carbon();
        $user = Auth::user();

        $startOn = $carbon->now();
        $startOn  =  date('Y-m-d H:i:s', strtotime($startOn));

        $myplan = $this->plans::find($request->plan_id);
        $duration = $myplan->duration;

        $subsDate = strtotime("+ $duration", strtotime($startOn));
        $expDuration =  date('Y-m-d H:i:s', $subsDate);

        $daysRemain = $this->plans::planDateDiff($startOn, $expDuration);

        $userPlan = DB::table('user_plans')->insert([
                ['user_id' => $user->id, 'plan_id' => $request->plan_id, 'starts_on' => $startOn, 'expired_on' => $expDuration],
            ]);

        return \response()->json($userPlan);
    }


    public function dirrecSubscribe(Request $request)
    {

        $carbon = new Carbon();
        $admin = Auth::user();
        $payStatus = "pending";

        Validator::make($request->all(), [
            'student_id' => 'required|exists:students,id',
            'plan' => 'required|exists:plans,id'
        ]);


        $status = 1;
        $planId = $request->plan;
        $activeMode = ($request->has('activate_mode')) ? $request->activate_mode : '';
        $student = $this->student::where('id', $request->student_id)->first();


        $startOn = $carbon->now();
        $startOn  =  date('Y-m-d H:i:s', strtotime($startOn));

        $myplan = $this->plans::find($planId );
        $duration = $myplan->duration;

        $subsDate = strtotime("+ $duration", strtotime($startOn));
        $expDuration =  date('Y-m-d H:i:s', $subsDate);

        $daysRemain = $this->plans::planDateDiff($startOn, $expDuration);

        $userPlan = UserPlan::create([
            'user_id' => $student->user_id,
            'plan_id' => $myplan->id,
            'starts_on' => $startOn,
            'expired_on' => $expDuration,
            'activate_mode_type' => $activeMode,
            'payment_status' => $payStatus
        ]);

        $this->student->updatedStudentTableSubscribe($student->user_id, $status);

        return \response()->json($userPlan);
    }




    public function getUserPlan(Request $request)
    {
        $user = false;
        $userPlanObject = [[
            "id" => 0,
            "user_id" => 0,
            "subs_type" => "",
            "starts_on" => "",
            "expired_on" => "",
            "plan_id" => 0,
            "transaction_id" => "",
            "transaction_cust_id"=> "",
            "activate_mode_type" => "",
            "payment_status" => "",
            "status" => "",
            "plan" =>  [
                "id" => 0,
                "name" => "",
                "duration" => "",
                "amount" => 0
            ]
        ]];

        if ($request->has("user_id")){
            $user = User::find($request->query("user_id"));
//            $userPlan = $user->plan;
            $userPlan = sizeof($user->plan) > 0? $user->plan : $userPlanObject ;

        }elseif(Auth::check()){
            $user = Auth::user();
            $userPlan = sizeof($user->plan) > 0 ? $user->plan : $userPlanObject ;

        }else{
            $userPlan = $userPlanObject;
        }

        return \response()->json(["data" => $userPlan,"current_date_time"=> Carbon::now()->format("Y-m-d H:i:s")]);
    }


    public function getRemaningDays(Request $request)
    {

        if ($request->has("user_id")){
            $user = User::find($request->query("user_id"));
            $plan = $this->getPlan($user);
        }elseif (Auth::check()){
            $user = Auth::user();
            $plan = $this->getPlan($user);
        }else{
            $plan = [];
        }


        return \response()->json($plan);
    }

    /**
     * @param $user
     * @return int
     */
    public function getPlan($user): int
    {
        $userPlan = collect($user->plan)->last();
        $plan = $this->plans::planDateDiff($userPlan->starts_on, $userPlan->expired_on);
        return $plan;
    }
}
