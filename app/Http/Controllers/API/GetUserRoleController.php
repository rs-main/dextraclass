<?php

namespace App\Http\Controllers\API;

use GLB;
use App\User;
use App\Models\Note;
use App\Models\Video;
use App\Models\Avatar;
use App\Models\Course;
use App\Models\School;
use App\Models\Classes;
use App\Models\Question;
use App\Models\Department;
use App\Models\StudentVideo;
use Illuminate\Http\Request;
use App\Models\SchoolCategory;
use App\Models\StudentHistory;
use App\Models\KnowledgeTarget;
use App\Models\StudentDownload;
use App\Models\StudentFavourites;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\APIController;

class GetUserRoleController extends APIController
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function getRole(Request $request)
    {
        // $userRoles = Auth::user()->userRole->role;
        // $request['page'] = 1;

        // return Auth::user();
        // return $userRoles;
        $user = $this->getUserProfile();
        unset($user->user_role);

        if(Auth::user()->userRole->role->slug == 'student' ||  Auth::user()->userRole->role->slug == 'tutor') {
            $school = $this->school_details($user->school_id);
            $profile = $this->profile();

            return $this->respond($this->successStatus, '', ['user' => $user ,'profile' => $profile, 'school' => $school]);

        }

        return $this->respond($this->successStatus, '', $user);

        // return response()->json(['user' => $user ,'profile' => $profile]);
    }

    public function getUserProfile()
    {
        $user = Auth::user();
		$user_id = Auth::user()->id;

		if (Auth::user()->userRole->role->slug == 'student') {

            $student_id = Auth::user()->student->id;
			$userData = DB::table('users')->where('users.id', $user_id)
				->join('students', 'users.id', '=', 'students.user_id')
				->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
				->leftJoin('avatars', 'students.avatar_id', '=', 'avatars.id')
				->leftJoin('countries', 'students.country', '=', 'countries.phonecode')
				/*->leftJoin('schools', function($join) {
					$join->on('schools.id', '=', 'students.school_id');
					$join->where('students.school_id', '>', 0);
				})*/
				->leftJoin('school_categories', 'school_categories.id', '=', 'students.school_category')
				->leftJoin('schools', 'schools.id', '=', 'students.school_id')
				->leftJoin('courses', 'courses.id', '=', 'students.course_id')
				->leftJoin('classes', 'classes.id', '=', 'students.class_id')
				->select(['users.id', 'users.uuid', 'users.email', 'users.username', 'role_user.role_id as usertype', 'students.first_name', 'students.last_name', 'mobile as phone', 'country', 'profile_image', 'avatars.file_url', 'countries.name as country_name', 'school_categories.uuid as institution_id', 'schools.school_category as cat_id', 'schools.uuid as school_id', 'courses.id as co_id', 'courses.uuid as course_id', 'classes.uuid as class_id'])
				->first();

			if($userData->profile_image != "" && file_exists(public_path('/'.$userData->profile_image)) && is_file(public_path('/'.$userData->profile_image))) {
				$userData->profile_image = url('/') . '/' . $userData->profile_image;
			} else {
				$userData->profile_image = url('/') . '/' . $userData->file_url;
			}
			$userData->department_id = '';
			if($userData->cat_id == School::UNIVERSITY && $userData->course_id != ''){
				$getDepartment = Course::where('courses.uuid', $userData->course_id)
						->leftJoin('departments', 'departments.id', '=', 'courses.department_id')
						->select('departments.uuid as department_id')->first();
				if(!empty($getDepartment)) {
					$userData->department_id = $getDepartment->department_id;
				}
			}
			$userData->role = 'student';
			$userData->stat_classes_watched = (int)StudentVideo::where('student_id', $student_id)->sum('video_watch_count');
			$userData->stat_questions = Question::where('sender_id', $userData->id)->where('type', 'question')->count();
			$userData->stat_answers = Question::where('sender_id', $userData->id)->where('type', 'reply')->count();
			$userData->stat_downloads = (int)StudentDownload::where([
						'student_id' => $student_id,
						'status' => 1
					])->sum('count');
            $userData->classes_hosted = 0;
            $userData->notes_added = 0;

			//$userData->lessons_followed = Classes::where('status', 1)->where('course_id', $userData->co_id)->orderBy('class_name', 'asc')->pluck("class_name", "id");
            $userData->role = Auth::user()->userRole->role->slug;
            unset($userData->id, $userData->file_url, $userData->cat_id, $userData->co_id );
			$userData->id = $userData->uuid;
			unset($userData->uuid);
        } else if (Auth::user()->userRole->role->slug == 'tutor') {
			$userData = DB::table('users')->where('users.id', $user_id)
				->join('tutors', 'users.id', '=', 'tutors.user_id')
				->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
				->leftJoin('avatars', 'tutors.avatar_id', '=', 'avatars.id')
				->leftJoin('countries', 'tutors.country', '=', 'countries.phonecode')
				->leftJoin('schools', 'schools.id', '=', 'tutors.school_id')
				->select(['users.id', 'users.uuid', 'users.email', 'users.username', 'role_user.role_id as usertype', 'tutors.first_name', 'tutors.last_name', 'mobile as phone', 'country', 'profile_image', 'tutors.school_id as sch_id','avatars.file_url', 'countries.name as country_name', 'schools.school_category as cat_id', 'schools.uuid as school_id'])
				->first();

			if($userData->profile_image != "" && file_exists(public_path('/'.$userData->profile_image)) && is_file(public_path('/'.$userData->profile_image))) {
				$userData->profile_image = url('/') . '/' . $userData->profile_image;
			} else {
				$userData->profile_image = url('/') . '/' . $userData->file_url;
			}
			$student_videos = StudentVideo::where('student_id', $userData->id)->pluck('video_id', 'video_id');
            $userData->stat_classes_watched = 0;
            $userData->stat_questions = Question::where('sender_id', $userData->id)->where('type', 'question')->count();
            $userData->stat_answers = Question::where('sender_id', $userData->id)->where('type', 'reply')->count();
            $userData->stat_downloads = 0;
			$userData->classes_hosted = Video::whereIn('id', $student_videos)->groupBy('class_id')->count();
			$userData->notes_added = Video::where([
						'tutor_id' => $userData->id,
						'status' => 1
					])->groupBy('topic_id')->count();

			$tutorSchoolCategory = $userData->cat_id;
			$tutorSchoolId = $userData->sch_id;
			$schoolCategory = SchoolCategory::find($tutorSchoolCategory);
			$userData->institution_id = $schoolCategory->uuid;

			$tutorVideos = Video::where('status',1)
								->where('vimeo_status','=','complete')
								->where('tutor_id', $userData->id)
								->select('id', 'tutor_id', 'uuid')
								->get();

			$videos = [];
			if(!empty($tutorVideos)) {
				foreach($tutorVideos as $k => $v) {
					$videos[] = $v['uuid'];
					$videosIds[] = $v['id'];
				}
			}
			$userData->video_ids = $videos;
			$userData->role = 'teacher';

			unset($userData->id, $userData->file_url, $userData->cat_id, $userData->sch_id );
			$userData->id = $userData->uuid;
			unset($userData->uuid);
        }else if (Auth::user()->userRole->role->slug == 'subadmin'){
            $userData['user'] = collect(Auth::user())->forget('user_role');
            $userData['user']['role'] = Auth::user()->userRole->role->slug;
        }

        return $userData;
		// return $this->respond($this->successStatus, '', $userData);
    }


    /**
     * Show Student profile.
     *
     */
    public function profile()
    {


        // session()->forget('newCustomer');
        // session()->forget('userId');
        $student = User::find(Auth::user()->id);

        // if (Auth::user()->userRole->role->slug == 'admin') {
        //     return redirect()->route('backend.dashboard');
        // } else if (Auth::user()->userRole->role->slug == 'subadmin') {
        //     return redirect()->route('backend.dashboard');
        // } else if (Auth::user()->userRole->role->slug == 'school') {
        //     return redirect()->route('backend.dashboard');
         if (Auth::user()->userRole->role->slug == 'student') {
            return $this->student();
        }  if (Auth::user()->userRole->role->slug == 'tutor') {
            return $this->tutor();
        }
    }

    public function tutor()
    {

        $tutor = User::find(Auth::user()->id);

        $student_videos = StudentVideo::where('student_id', $tutor->id)->pluck('video_id', 'video_id');
        $classesHosted = Video::whereIn('id', $student_videos)->groupBy('class_id')->count();
        $questionsAsked = Question::where('sender_id', $tutor->id)->where('type', 'question')->count();
        $replyCount = Question::where('sender_id', $tutor->id)->where('type', 'reply')->count();
        $noteAdded = Video::where([
                    'tutor_id' => $tutor->id,
                    'status' => 1
                ])->groupBy('topic_id')->count();




        //print_r($questionsAsked); exit;

        $schools = School::where('status', 1)->orderBy('school_name', 'asc')
                ->pluck("school_name", "id");
        $courses = Course::where('status', 1)->orderBy('name', 'asc')
                ->pluck("name", "id");
        $avatars = Avatar::where('status', 1)
                ->get();
		$knowledgeTargets = KnowledgeTarget::get();
        //pr($student->toArray());die;

        $tutorSchoolCategory = $tutor->userData->school->school_category;
        $tutorSchoolId = $tutor->userData->school->id;
        $schoolCategoryName = '';
        $defaultArray = array('deparments' => array(), 'courses' => array(), 'classes' => array());

        if ($tutorSchoolCategory == School::BASIC_SCHOOL) {
            $school = School::find($tutorSchoolId);
            if (!empty($school->coursesList[0])) {
                $course = $school->coursesList[0];
                $course_id = $course->id;

                $defaultArray['classes'] = Classes::where('status', 1)->where('course_id', $course_id)->orderBy('class_name', 'asc')->pluck("class_name", "id");
            }
            $schoolCategoryName = 'BASIC_SCHOOL';
        } else if ($tutorSchoolCategory == School::SENIOR_HIGH) {
            $defaultArray['courses'] = Course::where('status', 1)->where('school_id', $tutorSchoolId)->orderBy('name', 'asc')->pluck("name", "id");
            $schoolCategoryName = 'SENIOR_HIGH';
        } else if ($tutorSchoolCategory == School::UNIVERSITY) {

            $defaultArray['deparments'] = Department::where('status', 1)
                    ->where('school_id', $tutorSchoolId)
                    ->pluck("name", "id");

            $schoolCategoryName = 'UNIVERSITY';
        }

        $starShow = $this->tutorStar($tutor->id);

        $data ['tutor'] = $tutor;
        $data ['schools'] = $schools;
        $data ['courses'] = $courses;
        $data ['avatars'] = $avatars;
        $data ['classesHosted'] = $classesHosted;
        $data ['replyCount'] = $replyCount;
        $data ['questionsAsked'] = $questionsAsked;
        $data ['noteAdded'] = $noteAdded;
        $data ['defaultArray'] = $defaultArray;
        $data ['schoolCategoryName'] = $schoolCategoryName;
        $data ['tutorSchoolId'] = $tutorSchoolId;
        $data ['knowledgeTargets'] = $knowledgeTargets;
        $data ['starShow'] = $starShow;

        return $data;

        // return view('frontend.tutor.profile', compact('tutor', 'schools', 'courses', 'avatars', 'classesHosted', 'replyCount', 'questionsAsked', 'noteAdded', 'defaultArray', 'schoolCategoryName', 'tutorSchoolId','knowledgeTargets','starShow'));
    }

	public function tutorStar($tutor_id)
	{
		$silverDec = 'silver';
		$bronzeDec = 'bronze';
		$blueDec   = 'blue';
		$yellowDec = 'yellow';


		$count = Video::where([
                    'tutor_id' => $tutor_id,
                    'status' => 1
                ])->count();


		return (object) array(
				'silverCount'   => 1+$this->generateStarCount($count,500),
				'silverDec'     => $silverDec,
				'bronzeCount'   => $this->generateStarCount($count,100),
				'bronzeDec'     => $bronzeDec,
				'blueCount'     => $this->generateStarCount($count,1000),
				'blueDec'       => $blueDec,
				'yellowCount'   => $this->generateStarCount($count,5000),
				'yellowDec'     => $yellowDec
		);
	}

	public function generateStarCount($count,$mnt){

		//echo round(100/100,0); exit;
		if(!empty($count) && !empty($mnt) && ($count >= $mnt)){
		    return round($count/$mnt,0);
		}else{
			return 0;
		}
	}

	public function studentStar($student_id)
	{


		$silverDec = 'silver';
		$bronzeDec = 'bronze';
		$blueDec   = 'blue';
		$yellowDec = 'yellow';
		$count = 0;

		$pluck = StudentHistory::where([
                    'student_id' => $student_id
                ])->pluck('video_id','video_id');

		if(!empty($pluck)){
			 $count = collect($pluck)->count($pluck);
		}

		return (object) array(
				'silverCount'   => 1+$this->generateStarCount($count,500),
				'silverDec'     => $silverDec,
				'bronzeCount'   => $this->generateStarCount($count,100),
				'bronzeDec'     => $bronzeDec,
				'blueCount'     => $this->generateStarCount($count,1000),
				'blueDec'       => $blueDec,
				'yellowCount'   => $this->generateStarCount($count,5000),
				'yellowDec'     => $yellowDec
		);

	}

    public function student()
    {

        $student = User::find(Auth::user()->id);

        $student_videos = StudentVideo::where('student_id', $student->id)->pluck('video_id', 'video_id');
        $classesWatched = Video::whereIn('id', $student_videos)->count();
        $questionsCount = Question::where('sender_id', $student->id)->where('type', 'question')->count();
        $replyCount = Question::where('sender_id', $student->id)->where('type', 'reply')->count();
        $noteDownloads = StudentDownload::where([
                    'student_id' => $student->id,
                    'status' => 1
                ])->count();

		$classes = Classes::where('status', 1)->where('course_id', $student->userData->course_id)->orderBy('class_name', 'asc')->pluck("class_name", "id");

        //print_r($questionsCount); exit;

        $schools = School::where('status', 1)->orderBy('school_name', 'asc')
                ->pluck("school_name", "id");
        $courses = Course::where('status', 1)->orderBy('name', 'asc')
                ->pluck("name", "id");
        $avatars = Avatar::where('status', 1)
                ->get();
        //pr($student->toArray());die;
        $starShow = $this->studentStar($student->id);
        // $data ['student'] = $student;
        $data ['schools'] = $schools;
        $data ['courses'] = $courses;
        $data ['avatars'] = $avatars;
        $data ['classesWatched'] = $classesWatched;
        $data ['replyCount'] = $replyCount;
        $data ['questionsCount'] = $questionsCount;
        $data ['noteDownloads'] = $noteDownloads;
        $data ['starShow'] = $starShow;
        $data ['classes'] = $classes;

        return $data;
        // return view('frontend.student.profile', compact('student', 'schools', 'courses', 'avatars', 'classesWatched', 'replyCount', 'questionsCount', 'noteDownloads','starShow','classes'));
    }

    /**
     * Show Student profile.
     *
     */
    public function studentHistory(Request $request)
    {

        $student_id = 0;
        $page = $request->input('page', 1);
        if (\Auth::check()) {
            $student_id = \Auth::user()->id;
        }

        $tab = $request->tab;
        $loadMore = $request->loadMore;


        $studentHistories = StudentHistory::where([
                    'student_id' => $student_id
                ])
                 ->whereHas('video', function($q) use($request) {
                    if (!empty($request->course_id)) {
                        $q->where('course_id', $request->course_id);
                    }
                    if (!empty($request->school_id)) {
                        $q->where('school_id', $request->school_id);
                    }
                })
                ->whereNotNull('video_id')
                ->orderBy('id', 'desc')
                ->groupBy('student_history.video_id');
                // ->paginate(GLB::paginate());

        $data['loadMore'] = $loadMore;
        $data['studentHistories'] = $studentHistories;
        $data['page'] = $page;

        return $data;

        // return view('frontend.student.student_history', compact('tab', 'loadMore', 'studentHistories', 'page'));
    }

    public function removeHtudentHistory(Request $request)
    {
        if ($request->rmFrom == 'history') {
            $studentHistories = StudentHistory::find($request->id);
            if (!empty($studentHistories->id)) {
                $studentHistories->delete();
            }
        } else {
            $studentFavourites = StudentFavourites::find($request->id);
            if (!empty($studentFavourites->id)) {
                $studentFavourites->delete();
            }
        }
        return 1;
    }

    public function studentFavourites(Request $request)
    {
        $student_id = 0;
        $page = $request->input('page', 1);
        if (\Auth::check()) {
            $student_id = \Auth::user()->id;
        }

        $tab = $request->tab;
        $loadMore = $request->loadMore;


        $studentFavourites = StudentFavourites::where([
                    'student_id' => $student_id
                ])
                ->whereHas('video', function($q) use($request) {
                    if (!empty($request->course_id)) {
                        $q->where('course_id', $request->course_id);
                    }
                    if (!empty($request->school_id)) {
                        $q->where('school_id', $request->school_id);
                    }
                })
                ->whereNotNull('video_id')
                ->orderBy('id', 'desc')
                ->paginate(GLB::paginate());

            $data['loadMore'] = $loadMore;
            $data['studentFavourites'] = $loadMore;
            $data['page'] = $page;

            return $data;

        // return view('frontend.student.student_favourites', compact('tab', 'loadMore', 'studentFavourites', 'page'));
    }

    public function uploadProfile(Request $request)
    {
        $sender_id = 0;
        $status = 0;
        $messageType = '';
        $message = '';
        $watchCount = 1;
        $src = '';

        $validator = Validator::make($request->all(), [
                    //'profile_image'=>'required|image|dimensions:max_width=212,max_height=150|max:2048'
                    'profile_image' => 'required|image|max:2048'
                        ], [
                        //"required" => ""
        ]);

        if ($validator->fails()) {

            $errors = $validator->errors()->all();
            $status = 0;
            $messageType = 'error';
            $message = collect($errors)->implode('<br>');
        } else {

            $userid = \Auth::user()->id;
            $user = User::find($userid);
            $role_id = $user->role_id;
            $userRole = $user->userRole->role->slug;

            if ($request->hasFile('profile_image')) {
                $logo = $request->file('profile_image');
                $uploadPath = "uploads/student";

                if ($userRole == 'tutor') {
                    /** Below code for save tutor image * */
                    $uploadPath = "uploads/tutor";
                } else {
                    /** Below code for save student image * */
                    $uploadPath = "uploads/student";
                }

                $logoName = time() . mt_rand(10, 100) . '.' . $logo->getClientOriginalExtension();
                $location = public_path($uploadPath);
                if (!file_exists($location)) {
                    mkdir($location);
                }
                $isMoved = $logo->move($location . '/', $logoName);
                $img = Image::make($location . '/' . $logoName);

                if ($role_id == 5) {
                    $update = Student::where('user_id', $user->id)->first();
                    $update->profile_image = $uploadPath . '/' . $logoName;
                    $update->save();
                }
                if ($userRole == 'tutor') {
                    /** Below code for save tutor image * */
                    $update = Tutor::where('user_id', $user->id)->first();
                    $update->profile_image = $uploadPath . '/' . $logoName;
                    $update->save();
                } else {
                    /** Below code for save student image * */
                    $update = Student::where('user_id', $user->id)->first();
                    $update->profile_image = $uploadPath . '/' . $logoName;
                    $update->save();
                }

                $src = url($uploadPath . '/' . $logoName);
                $status = 1;
                $messageType = 'success';
                $message = "Successfully updated your profile.";
            }
        }

        $returnMsg = (object) array(
                    'status' => 200,
                    'errStatus' => $status,
                    'messageType' => $messageType,
                    'message' => $message,
                    'imgsrc' => $src
        );
        $returnData['data'] = $returnMsg;
        return response()->json($returnMsg, 200);
    }

    public function changeAvatar(Request $request)
    {
        $sender_id = 0;
        $status = 0;
        $messageType = 'error';
        $message = 'Please select avatar.';
        $watchCount = 1;
        $src     = '';
        $fileurl = '';

		$avatar = Avatar::find($request->avatar_id);

		if(!empty($avatar->file_url) && file_exists(public_path($avatar->file_url))){
			$fileurl = $avatar->file_url;
		}

        if (!empty($request->avatar_id)) {
            $userid = \Auth::user()->id;
            $user = User::find($userid);
            $role_id = $user->role_id;
            $userRole = $user->userRole->role->slug;


            if ($userRole == 'tutor') {
                /** Below code for save tutor  * */
                $update = Tutor::where('user_id', $user->id)->first();
                $update->avatar_id     = $request->avatar_id;
				$update->profile_image = $fileurl;
                $update->save();
            } else {
                /** Below code for save student  * */
                $update = Student::where('user_id', $user->id)->first();
                $update->avatar_id     = $request->avatar_id;
				$update->profile_image = $fileurl;
                $update->save();
            }

			$src = $user->userData->profile_or_avatar_image;
			//profile_or_avatar_image
            $status = 1;
            $messageType = 'success';
            $message = "Successfully updated your avatar.";
        }





        $returnMsg = (object) array(
                    'status' => 200,
                    'errStatus' => $status,
                    'messageType' => $messageType,
                    'message' => $message,
					'imgsrc' => $src
        );
        $returnData['data'] = $returnMsg;
        return response()->json($returnMsg, 200);
    }

    public function tutorLecture(Request $request)
    {


        $page = $request->input('page', 1);
        $tutor_id = \Auth::user()->id;

        $tab = $request->tab;
        $loadMore = $request->loadMore;


        $tutorVideos = Video::where([
                    'tutor_id' => $tutor_id
        ]);
        if (!empty($request->orderBy)) {
            $exp = explode('-', $request->orderBy);
            $tutorVideos = $tutorVideos->orderBy($exp[0], $exp[1]);
        } else {
            $tutorVideos = $tutorVideos->orderBy('id', 'desc');
        }

        $tutorVideos = $tutorVideos->paginate(GLB::paginate());

        return view('frontend.tutor.tutor_lecture', compact('tab', 'loadMore', 'tutorVideos', 'page'));
    }

    public function tutorPosts(Request $request)
    {
        $student_id = 0;
        $page = $request->input('page', 1);
        if (\Auth::check()) {
            $student_id = \Auth::user()->id;
        }

        $tab = $request->tab;
        $loadMore = $request->loadMore;


        $tutorPosts = StudentHistory::where('id', 0)->paginate(1);

        return view('frontend.tutor.tutor_post', compact('tab', 'loadMore', 'tutorPosts', 'page'));
    }

    public function uploadNotes(Request $request)
    {

        $validator = Validator::make($request->all(), [

                    'video_id' => 'required',
                    'notes' => 'required'
                        ], [
                    "video_id.required" => "Video not found."
        ]);
        $status = 0;
        $message = "";
        $messageType = "";
        $src = "";

        if ($validator->fails()) {

            $errors = $validator->errors()->all();
            $status = 0;
            $messageType = 'error';
            $message = collect($errors)->implode('<br>');
        } else {

            $userid = \Auth::user()->id;
            $user = User::find($userid);
            $role_id = $user->role_id;
            $userRole = $user->userRole->role->slug;

            if ($request->hasFile('notes')) {
                $notes = $request->file('notes');
                //$uploadPath = "uploads/notes";


                //$fileName = time() . mt_rand(10, 100) . '.' . $notes->getClientOriginalExtension();
                //$location = public_path($uploadPath);
                //if (!file_exists($location)) {
                //    mkdir($location);
                //}
                //$isMoved = $notes->move($location . '/', $fileName);
                //$img = Image::make($location.'/'.$fileName);
                $path = Storage::disk('s3')->put('notes', $notes, 'public');

                $insertNote = new Note();
                $insertNote->tutor_id = Auth::user()->id;
                $insertNote->file_url = $path;  //$uploadPath . '/' . $fileName;
                $insertNote->storage = 's3';
                $insertNote->save();

                if (!empty($request->video_id)) {
                    $updateVideo = Video::find($request->video_id);
                    $updateVideo->note_id = $insertNote->id;
                    $updateVideo->save();
                }


                $src = $updateVideo->videoURL();
                $status = 1;
                $messageType = 'success';
                $message = "Successfully uploaded your file.";
            }
        }

        $returnMsg = (object) array(
                    'status' => 200,
                    'errStatus' => $status,
                    'messageType' => $messageType,
                    'message' => $message,
                    'imgsrc' => $src
        );
        $returnData['data'] = $returnMsg;
        return response()->json($returnMsg, 200);
    }

    /**
     * Uplaod video files
     *
     * @param  string  $uuid
     * @return renderd view
     */
    public function uploadVideoFile($uuid)
    {
        $video = Video::uuid($uuid);
        if($video->video_type != 'file' || Auth::user()->id != $video->tutor_id){
            return redirect()->route('front');
        }

        $video_thumb = $video->getVimeoThumb();

        return view('frontend.tutor.upload_video_file', compact('video','video_thumb'));
    }

    /**
     * Get details of an school
     *
     * @return [object]
     */
    public function school_details($id)
    {
		$lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
		$schoolLogoPath = url('/') . '/uploads/schools/';
		$school = DB::table('schools')->where(['schools.status' => 1, 'schools.uuid' => $id])->whereNull('schools.deleted_at')
					->leftJoin('school_categories', 'school_categories.id', '=', 'schools.school_category')
					->select('schools.uuid as id', 'school_name as name', DB::raw('CONCAT("'.$schoolLogoPath.'", "", logo) as logo_url'), 'school_categories.uuid as institution_id' )->first();
		if(!empty($school)) {
			return $school;
		}
		return "School Not Found";
    }
}
