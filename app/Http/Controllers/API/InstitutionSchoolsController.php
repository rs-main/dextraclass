<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\SchoolCategory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class InstitutionSchoolsController extends APIController
{
    //
    private $schoolCategory;
    public function __construct(SchoolCategory $schoolCategory)
    {
        $this->schoolCategory = $schoolCategory;
    }

    public function loadInstitutionSchools()
    {
        $lang = request('lang');
		if(!$lang) {
			$lang = 1;
		}
        // $categories = DB::table('school_categories')->where(['status' => 1])->whereNull('deleted_at')->select('uuid as id', 'name')->orderBy('name', 'ASC')->get()->toArray();

        // $categories = $categories->with('schools')->get();

        $schoolCategory = $this->schoolCategory->select('uuid as uuid', 'name', 'id')->whereNull('deleted_at')->with('schools')->get();

		return $this->respond($this->successStatus, '', $schoolCategory);
    }

}
