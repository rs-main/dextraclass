<?php

namespace App\Http\Controllers\API;

use App\Models\AppVersion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class AppVersionCheckController extends APIController
{
    
    
    public function __construct(AppVersion $appVersion)
    {
        //$this->middleware('auth:api');
        $this->appVersion = $appVersion;
    }

    public function getMobileAppVersion()
    {
        $appVersion = $this->appVersion::all();
        return $this->respond($this->successStatus,'', $appVersion);
    }
    
    public function getMobileAppVersion2(Request $request)
    {
          $customer_number = $request['customer_number'];
        $appVersion = $this->appVersion::first();
        return $this->respond($this->successStatus,'', $customer_number);
    }
    
    
    
        public function make_subscription(Request $request)
    {
   
/*        
        $customer_number = $data['customer_number'];
        
        $amount = $data['amount'];
        $network = $data['network'];
        $voucher_code = $data['voucher_code'];
        $reference = $data['reference'];
        
        $SECRET_KEY  = 'iFY0Bi7VJ2/Y1jhCW3j3G00gVMuWCghsYDhM5aSQrFk592sKEuMLs4F1J06AVsL4OikbCrdKDZuBLojbdEqhlg==';
        $CLIENT_KEY = 'qrVJjCplwJielztPmjnsUtU87cVjcBY3310WNm+DIcPNMwB9LB7Y7P1C0qXR7UA0XLrwLdb64xPdWP08odMM+Q==';
        $SERVICE_ID  = '828';
        
        
        $service_url = 'https://orchard-api.anmgw.com/sendRequest';
  
        $exttrid = '';

        $callback_url = '';
        
        $timestamp = date('YYYY-MM-DD H:M:S', time());
        
        $data = array(
        'service_id' => $SERVICE_ID, 
        'trans_type' => 'CTM',
        'customer_number'=>$customer_number,
        'amount' => $amount,
        'exttrid' => $exttrid,
        'reference' => $reference,
        'nw' => $nw,
        'callback_url' => $callback_url,
        'ts' => $timestamp,
        'voucher_code' => $voucher_code,
        );
        
        $data_string = json_encode($data);
        $signature =  hash_hmac( 'sha256' , $data_string , $SECRET_KEY );
        $auth = $CLIENT_KEY.':'.$signature;
        
        $ch = curl_init($service_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: '.$auth,
        'Content-Type: application/json',
        'timeout: 180',
        'open_timeout: 180'
        )
        );
        
        
        $result = curl_exec($ch);
        curl_close($ch);*/
        
        /*
        $response = preg_replace('/\\\\\"/',"\"", $result);
        
        if(stristr($response['message'], "Success") != FALSE){
            return $this->respond($this->successStatus, 'Access Request in Progress, Call 0509962708 for further assistance');
        }
        else{
            return $this->respond($this->badRequestStatus, 'Sorry Could not Proccess, Call 0509962708 for further assistance');
        }*/
     
     
     return response()->json(["Success" => true],200);
    
       

    }


}
