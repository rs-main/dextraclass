<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\QuestionReply;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;


class QuestionReplyController extends APIController
{
    public $qreply;
    public function __construct(QuestionReply $qreply)
    {
        $this->qreply = $qreply;
    }

    public function postReply(Request $request)
    {
        $rep = $this->qreply::create($request->all());
        return $this->respond($this->successStatus, '', $rep);
    }


    public function updateReply(Request $request)
    {
        $rep = $this->qreply::find($request->id);
        $rep->update($request->all());

        return $this->respond($this->successStatus, '', $rep);
    }
}
