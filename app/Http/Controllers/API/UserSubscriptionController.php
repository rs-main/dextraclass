<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Models\Course;
use App\Models\School;
use App\Models\Classes;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Models\SchoolCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\APIController;

class UserSubscriptionController extends APIController
{
    public $user, $student;
    public function __construct(User $user, Student $student,School $school, Classes $classes, Course $courses, SchoolCategory $schoolCategory)
    {
        $this->user = $user;
        $this->student = $student;
        $this->classes = $classes;
        $this->courses = $courses;
        $this->school = $school;
        $this->schoolCategory = $schoolCategory;
        $this->middleware('auth:api');
    }
    public function checkSubscription()
    {
        $res = [];
        $user = Auth::user();
        $student = $user->student;

//        $school = $this->school::find($student->school_id);
//        $classes = $this->classes::find($student->class_id);
//        $courses = $this->courses::find($student->course_id);
//        $category = $this->schoolCategory::find($student->school_category);

//        $student['course_id'] = $courses->uuid ;
//        $student['school_id'] = $school->uuid ;
//        $student['class_id'] = $classes->uuid ;
//        $student['school_category'] = $category->uuid ;


        if ($student->subscribed) {
           $res['subscription'] = true;
           $res['student'] = $student->only(['first_name','last_name','email','mobile','school_category', 'school_id','course_id', 'class_id']);

        } else {
            $res['subscription'] = false;
            $res['student'] =  $student->only(['first_name','last_name','email','mobile','school_category', 'school_id','course_id', 'class_id']);
            // $res['student'] = $student ;
        }
        return $this->respond($this->successStatus, '', $res);

    }

    public function subScribeUser()
    {
        $user = Auth::user();
        $student = $user->student;
        Student::where(["user_id" => $user->id])->update(array('subscribed' => 1));

        return $this->respond($this->successStatus, '', $student);

    }
}
