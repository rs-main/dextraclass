<?php

namespace App\Http\Controllers\API;

use GLB;
use App\Models\Video;
use App\Models\Classes;
use App\Models\Question;
use App\Models\StudentVideo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\APIController;
use Predis\Client;

class QuestionController extends APIController
{

    public $videos, $classes;
    public function __construct(Video $videos, Classes $classes)
    {
        $this->videos = $videos;
        $this->classes = $classes;
    }
    /**
     * Show questions ajax data.
     *
     */
    public function questionsData(Request $request, $class = '', $lesson = '')
    {

        try {
            $video_builder = Video::whereId($lesson)->selectRaw("id, uuid")->first();
            $classes_builder = Classes::whereId($class)->selectRaw("id, uuid")->first();

//            $video = $video_builder->uuid;
//            $class = $classes_builder->uuid;

            $classroom_id = $classes_builder->id;
            $video_id = $video_builder->id;
            $qdata = [
                'class_id' => $classroom_id,
                'video_id' => $video_id,
                'parent_id' => 0
            ];

            if ($request->has('subject_id') && $request->subject_id > 0) {
                $qdata['subject_id'] = $request->subject_id;
                unset($qdata['video_id']);
            }

            $questions = Question::where($qdata)
                ->with(['childrenAccounts','user'])
                ->orderBy('id', 'desc')->get();

            return $questions;
//            return $this->respond($this->successStatus, '', $questions);
        }catch (\Exception $exception){
            return response()->json(["message" => $exception->getMessage()]);
        }
    }

    public function getAllChats(Request $request, $class, $lesson){
        $chat_results = $this->questionsData($request, $class, $lesson);
        $client = new Client('tcp://18.184.172.239:6380?database=0&password=password123');
        $client->publish("app:notifications", $chat_results );
        return response()->json(["message" => "success"],200);
    }

    public function postQuestions(Request $request)
    {

        $sender_id = 0;
        $status = 0;
        $messageType = '';
        $message = '';
        $watchCount = 1;


        $video = $this->videos::uuid($request->lesson_id);
        $class = $this->classes::uuid($request->class_id);

        $subject_id = $video->subject_id;

        $validator = Validator::make($request->all(), [
            'content' => 'required'
        ], [
            "content.required" => "The question field is required."
        ]);

        if ($validator->fails()) {

            $errors = $validator->errors()->all();
            $status = 0;
            $messageType = 'error';
            $message = collect($errors)->implode('<br>');
        } else {

            if (Auth::check()) {
                $sender_id = Auth::user()->id;
            }

            if ($sender_id > 0) {

                $insertQuestion = new Question;
                $insertQuestion->content = $request->input('content');
                $insertQuestion->type = $request->input('sender_type');
                $insertQuestion->parent_id = $request->input('parent_id');
                $insertQuestion->sender_id = $sender_id;
                $insertQuestion->class_id =  $class->id;
                $insertQuestion->video_id = $video->id;
                // $insertQuestion->video_id = $request->input('vimeo_id');
                $insertQuestion->subject_id =   $subject_id;

                $insertQuestion->save();

                $status = 1;
                $messageType = 'success';
                $message = 'Question posted successfully.';
            } else {

                $status = 0;
                $messageType = 'error';
                $message = 'You are not loged in.';
            }
        }

        $returnMsg = (object) array(
            'status' => 200,
            'errStatus' => $status,
            'messageType' => $messageType,
            'message' => $message
        );
        $returnData['data'] = $returnMsg;
        return response()->json($returnMsg, 200);
    }

    public function getChatParticipants(Request $request, $class = null, $lesson = null, $subject=null){

        try {
            $tab = $request->tab;
            $loadMore = $request->loadMore;
            $classroom_id = $class;
//            $video_id = $request->video_id;
//        $subject_id = $request->query("subject_id");
            $subject_id = $subject;

            //$account = Question::where('parent_id',0)->with('childrenAccounts')->paginate(5);
            $questions = Question::with("student")->where([
                'class_id' => $classroom_id,
//                    'video_id' => $video_id,
                'subject_id' => $subject_id,
                'parent_id' => 0
            ])->orderBy('id', 'desc')
                ->get();

            return $this->respond($this->successStatus, '', $questions);
        }catch (\Exception $exception){
            return response()->json(["message" => $exception->getMessage()]);
        }
    }

    public function sendChat(Request $request)
    {
        $sender_id = 0;
        $status = 0;
        $messageType = '';
        $message = '';
        $watchCount = 1;

//        $video = $this->videos::uuid($request->lesson_id);
        $video = $this->videos::whereId($request->lesson_id)->first();
//        $class = $this->classes::uuid($request->class_id);
        $class = $this->classes::whereId($request->class_id)->first();

        $sender_id = $request->user_id;
        $subject_id = $video->subject_id;
        $content    = $request->get("content");
        $type       = $request->get("sender_type");
        $parent_id  = $request->get("parent_id");

            if ($sender_id > 0) {

                $insertQuestion = new Question;
                $insertQuestion->content = $content;
                $insertQuestion->type = $type;
                $insertQuestion->parent_id = $parent_id;
                $insertQuestion->sender_id = $sender_id;
                $insertQuestion->class_id =  $class->id;
                $insertQuestion->video_id = $video->id;
                $insertQuestion->subject_id =   $subject_id;
                $insertQuestion->save();

                $status = 1;
                $messageType = 'success';
                $message = 'Question posted successfully.';
        }
        $qdata = [
            'id' => $insertQuestion->id,
            'class_id' => $insertQuestion->class_id,
            'video_id' => $insertQuestion->video_id,
            'parent_id' => 0
        ];

        $questions = Question::where($qdata)
            ->with(['childrenAccounts','user'])
            ->orderBy('id', 'desc')->get();

        $returnMsg = (object) array(
                    'status' => 200,
                    'errStatus' => $status,
                    'messageType' => $messageType,
                    'message' => $message,
                    'data'    => $questions
        );
        $returnData['data'] = $returnMsg;

        $client = new Client('tcp://18.184.172.239:6380?database=0&password=password123');
        $client->publish("app:notifications", $questions );

        return response()->json($returnMsg, 200);
    }

    //        $subject_id = array();
//        $slTable = (new StudentVideo)->getTable() . ' as sl';
//        $student_id = 0;
//        $page = $request->input('page', 1);
//        if (Auth::check()) {
//            $student_id = Auth::user()->id;
//        }
//
//        $video = $this->videos::uuid($lesson);
//        $class = $this->classes::uuid($class);
//
//        $tab = $request->tab;
//        $loadMore = $request->loadMore;
//        $classroom_id = $class->id;
//        $video_id = $video->id;
//        // $video_id = $request->vimeo_id;
//
//        $qsubject = array();
//
//        $qdata = [
//            'class_id' => $classroom_id,
//            'video_id' => $video_id,
//            'parent_id' => 0
//        ];
//
//
//        if ($request->has('subject_id') && $request->subject_id > 0) {
//            // $qsubject = array('subject_id' => $request->subject_id );
//            $qdata['subject_id'] = $request->subject_id;
//            unset($qdata['video_id']);
//        }
//               $questions = Question::where($qdata)
//                ->with('childrenAccounts')
//                ->orderBy('id', 'desc')->get();
//                // ->paginate(GLB::paginate());
//
//        return $this->respond($this->successStatus,'', $questions);

}
