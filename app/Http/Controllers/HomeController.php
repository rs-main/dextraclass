<?php

namespace App\Http\Controllers;

use App\Models\Classes;
use App\Models\SchoolCategory;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\Course;
use App\Models\Classroom;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => array('index')]);
		session()->forget('newCustomer');
    }

	/**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        session()->forget('newCustomer');
        session()->has('open') ? session()->forget('open') : '';

        // $mode = $request->has('mode') ? $request->mode : '';

		 $schools = School::where('status', '=', 1)->where('featured',1)
                           ->whereHas('latestVideo');
        $schools = $schools->orderBy('school_name', 'asc');
        $schools = $schools->paginate(10);

        $schoolCategoryBuilder = SchoolCategory::whereStatus(true);

          $openSchoolCategories = $schoolCategoryBuilder
//             ->leftJoin("schools","schools.school_category","=","school_categories.id")
            ->where("name","<>","Basic School")
            ->where("name","<>","University")
            ->where("name","<>","Professional")
            ->where("name","NOT LIKE","%Take a Course%")
            ->where("name","NOT LIKE","%Institution%")
            ->orderBy("order","asc")
            ->get();

          $schools_id_array = SchoolCategory::where("school_categories.status",1)
            ->leftJoin("schools","schools.school_category","=","school_categories.id")
            ->where("name","<>","Basic School")
            ->where("name","NOT LIKE","%Take a Course%")
            ->where("name","NOT LIKE","%Institution%")
            ->selectRaw("schools.id")
            ->pluck("id");

        $openSchoolCourses = Course::whereIn("school_id",$schools_id_array)
            ->groupBy("name")
            ->selectRaw("courses.id as courseId,courses.name as courseName")
            ->take(10)
            ->get();

        $regularSchoolCategories = SchoolCategory::whereStatus(true)
            ->where("name","LIKE","%Basic School%")
            ->orWhere("name","LIKE","%University%")
//            ->orWhere("name","LIKE","%Senior High%")
            ->where("name","NOT LIKE","%Take a Course%")
            ->where("name","NOT LIKE","%Institution%")
            ->get();

        $regular_schools = SchoolCategory::where("school_categories.status",true)
            ->where("school_categories.status",1)
            ->leftJoin("schools","schools.school_category","=","school_categories.id")
            ->where("name","LIKE","%Basic School%")
            ->where("name","NOT LIKE","%Take a Course%")
            ->where("name","NOT LIKE","%Institution%")
            ->selectRaw("schools.*")
            ->get();

        $professionalSchoolCategories = SchoolCategory::whereStatus(true)
            ->where("name","LIKE","%Take a Course%")
            ->get();

        $courses = Course::leftJoin("course_category",
            "course_category.course_id","=","courses.id")
            ->leftJoin("professional_school_categories",
                "professional_school_categories.id","=","course_category.category_id")
            ->leftJoin("industries","industries.id","=","professional_school_categories.industry_id")
            ->where("type","professional");

        if ($request->ajax()){
            $courses->where("professional_school_categories.id",$request->query("category_id"))
                                ->selectRaw("courses.id, courses.name,courses.status,courses.display_image,
                       professional_school_categories.name as category_name,
                       industries.name as industry_name")
                ->take(10)->get();

            return view('frontend.professional_courses', compact('courses'));
        }else{
            $courses = $courses->selectRaw("courses.id, courses.name,courses.status,courses.display_image,
                       professional_school_categories.name as category_name,
                       industries.name as industry_name")
                ->take(10)->get();
        };

        return view('frontend.regular.home',compact(
            'schools',
            'openSchoolCategories',
                      'openSchoolCourses',
                      'regularSchoolCategories',
                      'regular_schools',
                      'professionalSchoolCategories',
                      'courses'));

        return view('frontend.home',compact('schools'));
    }

    public function regularSchoolHome(Request $request, $mode = null)
    {
        session()->forget('newCustomer');
        //!session()->has('open') ? session()->put('open') : session()->put('open', false);
        session()->has('open') ? session()->forget('open') : '';

        $schools = School::where('status', '=', 1)->where('featured',1)
            ->whereHas('latestVideo');
        $schools = $schools->orderBy('school_name', 'asc');
        $schools = $schools->paginate(10);


        return view('frontend.home',compact('schools'));
    }
}
