<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\SiteHelpers;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

	public function login(Request $request)
	{
		/* $this->validate($request, [
			'email' => 'required|email',
			'password' => 'required|string|min:8',
		]); */

		$validator = Validator::make($request->all(), [
			'username' => 'required|string|min:4|max:255|regex:/^(?=.*[a-z]).+$/',
			'password' => 'required|min:6',
		],[
			'username.regex' => "Username contains <li>At least one lowercase</li>"
		]);

        if ($validator->fails()) {
            return redirect('login')->withErrors($validator)->withInput();
        }

		$remember_me = $request->has('remember_me') ? true : false;

		if (auth()->attempt(['username' => $request->input('username'), 'password' => $request->input('password')], $remember_me))
		{
			return $this->sendLoginResponse($request);
		}else{
			return redirect()->route('login')->with('error','your username and password are wrong.');
		}
	}

    /**
     * @param Request $request
     * @param $user
     *
     * @throws GeneralException
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function authenticated(Request $request, $user)
    {
        /*
         * Check to see if the users account is confirmed and active
         */
        //to do

        //if(!empty($user->mobile_verified_at)){
            if (!$user->status) {
                auth()->logout();
                return redirect()->route('login')->with('error', 'Currently, your account is not active. Please contact to site administrator.');
            } elseif (\Auth::user()->hasRole(['student','tutor'])) {
                return redirect()->route('front.home');
            } elseif (\Auth::user()->hasRole(['superadmin', 'admin', 'subadmin', 'school'])) {
                // && \Auth::user()->hasPermission('browse.admin')
                return redirect()->route('backend.dashboard');
            }
        //}else{
        //    auth()->logout();
        //    return redirect()->route('login')->with('error', 'Currently, your account is not active. Please Verfiy Your Email.');
        //}
        return redirect('/');
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     *
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            //create a user using socialite driver google
            $user = Socialite::driver('google')->user();
            // if the user exits, use that user and login
            $finduser = User::where('google_id', $user->id)->first();
            if($finduser){
                //if the user exists, login and show dashboard
                \Auth::login($finduser);
//                return redirect('/dashboard');
                return redirect('/existed');
            }else{
                //user is not yet created, so create first
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'password' => encrypt('')
                ]);
                //create a personal team for the user
//                $newTeam = Team::forceCreate([
//                    'user_id' => $newUser->id,
//                    'name' => explode(' ', $user->name, 2)[0]."'s Team",
//                    'personal_team' => true,
//                ]);
//                // save the team and add the team to the user.
//                $newTeam->save();
//                $newUser->current_team_id = $newTeam->id;
//                $newUser->save();
                //login as the new user
                \Auth::login($newUser);
                // go to the dashboard
                return redirect('/create-existed');
            }
            //catch exceptions
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function postMobileLogin(Request  $request)
    {
        try {
            $registered = true;
            $otp_code = mt_rand(10000, 99999);
            $phone = \request()->get("phone");
            $otp_phone = substr($phone,0,1) == "0" ? substr($phone,1) : $phone;
            $phone_code = \request()->get("country_code");
            $user = DB::table("students")
                ->leftJoin("users","users.id","=","students.user_id")
                ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
                ->leftJoin('avatars', 'students.avatar_id', '=', 'avatars.id')
                ->leftJoin('countries', 'students.country', '=', 'countries.phonecode')
                ->leftJoin('school_categories', 'school_categories.id', '=', 'students.school_category')
                ->leftJoin('schools', 'schools.id', '=', 'students.school_id')
                ->leftJoin('courses', 'courses.id', '=', 'students.course_id')
                ->leftJoin('classes', 'classes.id', '=', 'students.class_id')
                ->where("mobile", $phone)
                ->selectRaw("users.*,students.user_id, students.first_name,
                students.last_name, mobile as phone, country, profile_image, avatars.file_url,
                countries.name as country_name,school_categories.uuid as institution_id,
                schools.school_category as cat_id,schools.uuid as school_id, courses.id as co_id, courses.uuid as course_id,
                    classes.uuid as class_id")
                ->first();

            if ($user) {
                $send = SiteHelpers::sendOtpUsingCoreSms($phone_code.$otp_phone);

                $token = User::find($user->user_id)->createToken('dextraUserToken')->accessToken;
            }

            $data['username'] = $phone;
            $data['first_name'] = "";
            $data['last_name'] = "";
            $data['email'] = "$phone@email.com";
            $data['phone_code'] = $phone_code;
            $data['mobile'] = $phone;
            $data["password"] = "password";

            if (!$user) {

                $registered = false;
                $user = User::create([
                    'username' => $data['username'],
                    'email' => $data["email"],
                    'password' => $data["password"],
                ]);

                $token = $user->createToken('dextraUserToken')->accessToken;

                $role = \Illuminate\Support\Facades\DB::table('roles')
                    ->where('slug', '=', 'student')
                    ->first();  //choose the default role upon user creation.

                /* Below code for assign user role */
                $user->attachRole($role, $user->id);

                /* Below code for save student data */
                $user->insertStudent($user, $data);

            }

            $userData['uuid'] = $user->uuid;

            $request->merge(["username" =>   $data["username"] ]);
            $request->merge(["password" =>   $data["password"] ]);

            return response()->json(
                [
                    "registered" => $registered,
                    "data" => [
                        "user" => $user,
                        "access" => $token,
                    ],
                    "statusCode" => 200,
                    "message" => "successful"
                ], 200);

        }catch (\Exception $exception){
            return response()->json(["ex" => $exception->getMessage()]);
        }

    }

}
