<?php

namespace App\Http\Controllers\Dex;

use App\Models\SClass;
use App\Models\OpenCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class ClassCourseController extends APIController
{

    public $sclass, $opcourse;
    public function __construct(SClass $sclass, OpenCourse $opcourse )
    {
        $this->sclass = $sclass;
        $this->opcourse = $opcourse;
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects = $this->sclass::select(['id','name', 'category_code', 'uuid'])->with('courses')->get();
        return $this->respond($this->successStatus, '', $subjects);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return "Saving Class Courses";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = $this->sclass::find($id);
        $courses = $class->courses;

        $data['class'] = $class->only(['name', 'category_code', 'uuid']);
        $data['courses'] = $courses;

        return $this->respond($this->successStatus, '', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignClassCourse(Request $request, $class = null)
    {
        // $courseIds = \json_decode($request->course_id);
        $courseIds = $request->course_id;
        $class = $this->sclass::find($request->class_id);

        if ($class->category_code ==  $this->sclass::PMR || $class->category_code ==  $this->sclass::JHS || $class->category_code ==  $this->sclass::PROF || $class->category_code ==  $this->sclass::UNI) {
            return "Sorry Cannot Assign Course to Class";
        }
        $assign  = $class->courses()->detach($courseIds);
        $assign  = $class->courses()->attach($courseIds);

        return $this->respond($this->successStatus,'', "Saved");

    }
}
