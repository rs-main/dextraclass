<?php

namespace App\Http\Controllers\Dex;

use App\Models\OpenCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class CourseSubjectsController extends APIController
{
    public $openCourse;
    public function __construct(OpenCourse $openCourse)
    {
        $this->openCourse = $openCourse;
    }


    public function assignClassSubjects(Request $request)
    {

       // return $request;
    //    $subjectIds = \json_decode($request->subject_id);
       $subjectIds = $request->subject_id;

       $course = $this->openCourse::find($request->course_id);

       $ssubject = $course->open_subjects()->detach($subjectIds);
       $ssubject = $course->open_subjects()->attach($subjectIds);

       return $this->respond($this->successStatus,'Saved');
    }


    public function courseSubjects(Request $request, $course = null)
    {

       $course = $this->openCourse::find($course);

       $ssubjects = $course->open_subjects;

       $data['course'] = $course->only(['name','type','uuid']);
       $data['subjects'] = $ssubjects;
       return $this->respond($this->successStatus,'', $data);
    }
}
