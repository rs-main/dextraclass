<?php

namespace App\Http\Controllers\Dex;

use App\Models\SClass;
use App\Models\Classes;
use App\Models\SCategory;
use Illuminate\Http\Request;
use App\Models\SCategoryClass;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class SCategoryClassController extends APIController
{
    public $scategory, $classes, $sclass;
    public function __construct(SCategory $scategory, Classes $classes, SClass $sclass)
    {
        $this->scategory = $scategory;
        $this->classes = $classes;
        $this->sclass = $sclass;
    }

    public function index(Request $request)
    {
        $classes = $this->scategory::select(['name', 'code' ,'id','uuid'])->with('classes')->get();
        return $this->respond($this->successStatus, '', $classes);
    }



    public function getClasses(Request $request, $category = null)
    {
        $category = $this->scategory::find($category);
        $classes = $category->classes()->get();
        $data['category'] = $category->only(['id','name','code','uuid']);
        $data['classes'] = $classes;
        return $this->respond($this->successStatus, '', $data);
    }

    public function saveCategoryClass(Request $request)
    {
        $classIds = $request->class_id;

        $cateogory = $this->scategory::find($request->scategory_id);
        $scateclass = $cateogory->classes()->detach($classIds);
        $scateclass = $cateogory->classes()->attach($classIds);

        return $this->respond($this->successStatus,'Saved');

    }


    public function getCategoryClass(Request $request, $cateogory = null)
    {
        $cateogory = $this->scategory::find($request->scategory_id);
        $scateclass = $cateogory->classes()->detach($request->class_id);
        $scateclass = $cateogory->classes()->attach($request->class_id);

        return $this->respond($this->successStatus,'Saved');

    }
}
