<?php

namespace App\Http\Controllers\Dex;

use App\Models\SClass;
use App\Models\SSubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class OpenSubjectTopicController extends APIController
{
    public $openSubjects, $sclass;
    public function __construct(SSubject $openSubjects, SClass $sclass)
    {
        $this->openSubjects = $openSubjects;
        $this->sclass = $sclass;
    }

    public function getSubjectTopics(Request $request, $subject = null)
    {
        // $subject = $request->subject;
        $openSubjects = $this->openSubjects::find($subject);

        $openSubjectsTopics = $openSubjects->topics;
        return $this->respond($this->successStatus, '', $openSubjectsTopics);
    }

    public function getClassSubjectTopics(Request $request, $class = null)
    {
        $class = $this->sclass::getClassId($class);
        $openSubjectsTopics = $class->class_subject_topics($request->hashuid)->get();

        return $this->respond($this->successStatus, '', $openSubjectsTopics);
    }
}
