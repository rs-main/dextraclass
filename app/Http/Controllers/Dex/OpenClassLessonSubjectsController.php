<?php

namespace App\Http\Controllers\Dex;

use App\Models\SClass;
use App\Models\SSubject;
use App\Models\ClassLesson;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class OpenClassLessonSubjectsController extends APIController
{
    public $ssubject, $sclass;
    public function __construct(SSubject $ssubject, SClass $sclass)
    {
        $this->ssubject = $ssubject;
        $this->sclass = $sclass;
    }


    public function loadClassLessons(Request $request, $class = null)
    {
        $class = $this->sclass::getClassId($class);
        $lessonVideos = $class->class_subject_videos($request->hashuid)->get();
        // $lessonVideos = $class->class_videos;
        // return $lessonVideos;

        $classroomLessons = $lessonVideos->transform(function($item){
            $lesson = new ClassLesson;

            $lesson->tutor = $item->tutor->only(['first_name', 'last_name','profile_image']);
            $lesson->id = $item->id;
            $lesson->video_id = $item->video_id;
            $lesson->video_id = $item->video_id;
            $lesson->vimeo_id = $item->video_id;
            $lesson->description = $item->description;
            $lesson->keywords = $item->keywords;
            $lesson->note_url = $item->note_url;
            $lesson->play_on = $item->play_on;
            $lesson->lesson = $item->oplesson->title;
            $lesson->topic = $item->optopic->topic_name;
            $lesson->total_views = $item->total_views;

            return $lesson;
        });

        return $classroomLessons;
        return $this->respond($this->successStatus, '', $classroomLessons);
    }


    public function loadSubjClassLessons(Request $request, $subject = null)
    {
        $subject = $this->ssubject::getSubjectId($subject);
        $lessonVideos = $subject->subject_videos;
        // return $lessonVideos;

        $classroomLessons = $lessonVideos->transform(function($item){
            $lesson = new ClassLesson;

            $lesson->tutor = $item->tutor->only(['first_name', 'last_name','profile_image']);
            $lesson->id = $item->id;
            $lesson->video_id = $item->video_id;
            $lesson->video_id = $item->video_id;
            $lesson->vimeo_id = $item->video_id;
            $lesson->description = $item->description;
            $lesson->keywords = $item->keywords;
            $lesson->note_url = $item->note_url;
            $lesson->play_on = $item->play_on;
            $lesson->lesson = $item->oplesson->title;
            $lesson->topic = $item->optopic->topic_name;
            $lesson->total_views = $item->total_views;

            return $lesson;
        });

        return $classroomLessons;
        return $this->respond($this->successStatus, '', $classroomLessons);
    }
}
