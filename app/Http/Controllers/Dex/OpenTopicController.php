<?php

namespace App\Http\Controllers\Dex;

use App\Models\SClass;
use App\Models\OpenTopic;
use App\Models\OpenCourse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class OpenTopicController extends APIController
{
    public $openTopic, $sclass, $openCourse;
    public function __construct(OpenTopic $openTopic, SClass $sclass, OpenCourse $openCourse)
    {
        $this->openTopic = $openTopic;
        $this->openCourse = $openCourse;
        $this->sclass = $sclass;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $openTopics = $this->openTopic::all();
        return $this->respond($this->successStatus, '', $openTopics);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $openTopic =  $this->openTopic::create($request->all());
    //    $assignTopic = $this->assignTopic($openTopic);
    //    return $assignTopic;
       return $this->respond($this->successStatus, '', $openTopic);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assignTopic($data = null)
    {
        return "Complete Assigning Topic";
    }
}
