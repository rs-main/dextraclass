<?php

namespace App\Http\Controllers\Dex;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class OpenSchoolAjaxController extends Controller
{

 /**
     * Note Upload on S3
     *
     * @return void
     */
    public function openSchooldropzoneNoteStore(Request $request)
    {
        // return $request;
        // Get file extension
        $extension = $request->file('file')->getClientOriginalExtension();

        // Valid extensions
        $validextensions = array("pdf","zip","jpeg","jpg","png","doc", "docx", "ppt", "pptx");

        // Check extension
        if (in_array(strtolower($extension), $validextensions)) {

            // Rename file
            //$fileName = str_slug(time()) . rand(11111, 99999) . '.' . $extension;

            //$path = 'notes/' . $fileName ;
            $path = Storage::disk('s3')->put('notes', $request->file('file'), 'public');
        }

        return response()->json(['success' => 'success', 'savefilename' => $path]);
    }
}
