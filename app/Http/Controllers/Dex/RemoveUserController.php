<?php

namespace App\Http\Controllers\Dex;

use App\User;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Models\MobileVerification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RemoveUserController extends Controller
{

    public $user, $student, $mobile_verifications;
    public function __construct(User $user, Student $student, MobileVerification $mobile_verifications)
    {
       $this->user = $user;
       $this->student = $student;
       $this->mobile_verifications = $mobile_verifications;
       $this->middleware('auth:api');
    }


    public function removeRegisteredUser(Request $request)
    {
        $rqh = request()->header('DES-AUTH');

        if ($rqh != config('constants.DES_HEADER_AUTH')) {
           return;
        }

        $authu = Auth::user();
        $userRole = "student";
        $auser = User::find($authu->id);
        $userRole = $auser->userRole->role->slug;

        if ($userRole != 'subadmin')
        {
            return;
        }

        $mobile = $request->mobile;
        $student = $this->student::where('mobile', $mobile)->withTrashed()->first();
        $sstatus = is_null($student);

        if($sstatus)
        {
            return;
        }

        $mv = $this->mobile_verifications::where('mobile', $mobile)->first();
        $user['userid'] = $mv->user_id;

        $user = $this->user::find($user['userid']);

        $status = $student->forceDelete();
        $status = $mv->delete();
        $status = $user->delete();

        return \response()->json(['status' => $status, 'message' => 'Student Removed']);

    }
}
