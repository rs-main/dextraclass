<?php

namespace App\Http\Controllers\Dex;

use App\Models\SClass;
use App\Models\Classes;
use App\Models\Subject;
use App\Models\SSubject;
use Illuminate\Http\Request;
use App\Models\SCategoryClass;
use App\Models\SClassSubjects;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;

class ClassSubjectsController extends APIController
{
    public $scategoryClass, $subjects, $sclassSubjects, $sclass;
    public function __construct(SCategoryClass $scategoryClass, SSubject $subjects, SClassSubjects $sclassSubjects, SClass $sclass)
    {
        $this->scategoryClass = $scategoryClass;
        $this->subjects = $subjects;
        $this->sclassSubjects = $sclassSubjects;
        $this->sclass = $sclass;
    }


    public function loadSubjects()
    {
        $subjects = $this->sclass::select(['id','name', 'category_code', 'uuid'])->with('subjects')->get();
        return $this->respond($this->successStatus, '', $subjects);
    }
    
    
    


    public function store(Request $request)
    {
        return $request;
        $subjectIds = \json_decode($request->subject_id);

        $sclass = $this->sclass::find($request->class_id);

        // return  $subjectIds;
        $ssubject = $sclass->subjects()->detach($subjectIds);
        $ssubject = $sclass->subjects()->attach($subjectIds);

        return $this->respond($this->successStatus,'Saved');
    }


    public function getClassSubjects(Request $request, $class)
    {

        $sclass = $this->sclass::find($class);
        $ssubject = $sclass->open_subjects;
        // $ssubject = $sclass->subjects;

        $data['class'] = $sclass->only(['name','category_code','status','uuid']);
        $data['subjects'] = $ssubject;

        return $this->respond($this->successStatus,'', $data);
    }

    public function assignClassSubjects(Request $request)
    {

       $subjectIds = $request->subject_id;

       $sclass = $this->sclass::find($request->class_id);

       $ssubject = $sclass->open_subjects()->detach($subjectIds);
       $ssubject = $sclass->open_subjects()->attach($subjectIds);

       return $this->respond($this->successStatus,'Saved');
    }


    public function classSubjects(Request $request)
    {

       $sclass = $this->sclass::find($request->class_id);

       $ssubject = $sclass->open_subjects;
       return $this->respond($this->successStatus,'', $ssubject);
    }


    // public function saveClassSubjects(Request $request)
    // {
    //     $subject = $this->subjects::find($request->subject_id);
    //     $class = $this->classes::find($request->class_id);

    //     $checker = $this->checkifClassSubject($class->id, $subject->id);

    //    if ($checker > 0) {
    //        return;
    //    }

    //    $sclassSubject = $this->sclassSubjects::create([
    //     'class_id' => $class->id,
    //     'class_uuid' => $class->uuid,
    //     'subject_id' => $subject->id,
    //     'subject_uuid' => $subject->uuid
    //    ]);

    //    return $this->respond($this->successStatus, '', $sclassSubject);
    // }


    public function checkifClassSubject($class = null, $subject = null)
    {
        $classSub = $this->sclassSubjects::where('class_id',  $class)->where('subject_id', $subject)->get();
        return $classSub->count();
    }
}
