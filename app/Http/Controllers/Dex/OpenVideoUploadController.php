<?php

namespace App\Http\Controllers\Dex;

use App\Models\Note;
use App\Models\Video;
use App\Models\OpenVideo;
use App\Helpers\SiteHelpers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\APIController;

class OpenVideoUploadController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vdate = $request->has('data') ? $request->date : date('Y-m-d');
        $request['date'] = $vdate;

        $validator = Validator::make($request->all(), [
            // 'school' => 'required',
            // 'course' => 'required',
            'class' => 'required',
            'date' => 'required',
            'period' => 'required',
            'subject' => 'required',
            'topic' => 'required',
            'tutor' => 'required',
            'video_type' => 'required',
            'video_url' => 'required_if:video_type,url',
            //'video_url' => 'required_without:video_file',
            //'video_file' => 'required_without:video_url',
            'description' => 'required',
            //'note_file' => 'sometimes|mimes:jpeg,jpg,png,pdf,doc,docx,ppt,pptx,zip|max:20480',
        ]);


        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()->all()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {

            $video_id = '';
            if($request->video_type == 'url') {
                $video_url = $request->video_url;

                $video_data = SiteHelpers::getVimeoVideoData($video_url);

                if(!isset($video_data->video_id) || empty($video_data->video_id)){

                    $validator->errors()->add('video_url', 'Invalid Video URL');

                    return response()->json([
                        'status' => 'error',
                        'message' => $validator->errors()->all()
                    ], Response::HTTP_UNPROCESSABLE_ENTITY);
                } else {
                    $video_id = $video_data->video_id;
                }
            }


            $note = null;

            if ($request->note_file != '') {

                $path = $request->note_file;

                //Create Note
                $note = new Note();
                $note->tutor_id = $request->tutor;
                $note->file_url = $path;
                $note->storage = 's3';
                $note->status = 1;
                $note->save();
            }

            //form data is available in the request object
            $video = new OpenVideo();

            $video->school_id = $request->school;
            $video->course_id = $request->course;
            $video->class_id = $request->class;
            $video->play_on = $request->date;
            $video->period_id = $request->period;
            $video->video_id = $video_id;
            $video->video_url = $request->video_url;
            $video->video_type = $request->video_type;
            $video->description = $request->description;
            $video->subject_id = $request->subject;
            $video->topic_id = $request->topic;
            $video->keywords = $request->keywords;
            $video->tutor_id = $request->tutor;
            $video->note_id = isset($note->id)? $note->id : 0;
            // $video->user_id = Auth::user()->id;
            $video->status = ($request->input('status') !== null)? $request->input('status'):0;

            $video->save();
            $savedata['hash'] = $video->uuid;
            if($request->video_type == 'file') {
                // return redirect()->route('backend.video.upload.files',$video->uuid)->with('success', 'Video created Successfully, please uplaod video file for it. ');
                return $this->respond($this->successStatus, 'Video created Successfully, please uplaod video file for it.', $savedata);
                // return redirect()->route('backend.video.upload.files',$video->uuid)->with('success', 'Video created Successfully, please uplaod video file for it. ');
            }
            return $this->respond($this->successStatus, 'Video created Successfully',  $savedata);

            // return redirect()->route('backend.videos.index')->with('success', 'Video created Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
