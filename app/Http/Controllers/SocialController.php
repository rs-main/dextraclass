<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider){
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $user       =   User::where(['email' => $userSocial->getEmail()])->first();

        if($user){
            Auth::login($user);
            return redirect('/');
        }else{
            $data = [];

            $user = User::create([
                'name'          => $userSocial->getName(),
                'username'      => $userSocial->getEmail(),
                'email'         => $userSocial->getEmail(),
                'password'      => bcrypt("password"),
                'image'         => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
            ]);

            $split_name_array = $userSocial->getName() != null ? explode(" ", $userSocial->getName()) : "";

            $data['username'] = $userSocial->getEmail();
            $data['first_name'] = is_array($split_name_array) ? $split_name_array[0]: "";
            $data['last_name'] =  is_array($split_name_array) ? $split_name_array[1] : "";
            $data['email'] = $userSocial->getEmail();
            $data['mobile'] = "";
            $data['phone_code'] = "";

            $role = DB::table('roles')
                ->where('slug', '=', 'student')
                ->first();

            /* Below code for assign user role */
            $user->attachRole($role, $user->id);

            /* Below code for save student data */
            $user->insertStudent($user, $data);

            Auth::login($user);

//            return redirect()->route('front.home',["social" => true]);
            return redirect('/?social=true');

//            return redirect()->route('home');
        }
    }
}
