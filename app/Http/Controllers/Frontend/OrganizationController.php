<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\School;


class OrganizationController extends Controller{ 

    public function index() {

        $schools = \Illuminate\Support\Facades\DB::table('schools')
        ->leftJoin('courses', 'courses.school_id', '=', 'schools.id')
        ->leftJoin('videos', 'videos.course_id', '=', 'courses.id')
        ->leftJoin('classes', 'classes.course_id', '=', 'courses.id')
        ->get();

        

        return view('frontend.organization.index', compact('schools'));
    }

}