<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\School;


class MasterclassController extends Controller{ 

    public function index() {
        return view('frontend.masterclass.index');
    }

}