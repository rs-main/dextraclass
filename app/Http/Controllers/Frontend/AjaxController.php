<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\sendEmailtoNewuser;
use App\User;
use App\Models\Student;
use App\Models\StudentFavourites;
use App\Models\Course;
use App\Models\Tutor;
use App\Models\School;
use App\Models\Classes;
use App\Models\Department;
use App\Models\DepartmentClass;
use App\Models\SchoolCategory;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Exception;
use App\Helpers\SiteHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }


    /**
     * Send Otp code to user mobile number
     * return status true / false with error message
     */
    public function sendOtp(Request $request)
    {
        if ($request->isMethod('post')) {


            if (empty($request->input('user_id'))) {
                $res['status'] = false;
                $res['message'] = "User not found";
            } else {

                $user_id = $request->input('user_id');
                $sessionUser = User::find($request->input('user_id'));
                $userRole = $sessionUser->userRole->role->slug;

                $res['status'] = false;
                $res['message'] = null;

                $data = $request->all();

                if ($data['type'] == 'send-otp') {
                    try {
                        $otp = mt_rand(10000, 99999);

                        //change mobile number
                        if ($data['mobile_number_type'] == "new") {

                            $student_mobile_exists = Student::where(['mobile' => $data['mobile']])->count();
                            if ($student_mobile_exists > 0) {
                                $res['status'] = false;
                                $res['message'] = "This mobile number already registered.";
                                return json_encode($res);
                            } else {
                                if ($userRole == 'student') {
                                    Student::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                } else if ($userRole == 'student') {
                                    Tutor::where(["user_id" => $sessionUser->id, 'mobile' => $data['old_mobile_number']])->update(array('mobile' => $data['mobile']));
                                }
                            }
                        }

                        if ($userRole == 'student') {
                            $student = Student::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        } else if ($userRole == 'tutor') {
                            $student = Tutor::where(['user_id' => $sessionUser->id, 'mobile' => $data['mobile']])->first();
                        }
                        if ($student) {

                                $otp_mobile_prefix = substr($request->mobile,0,1);
                                $otp_mobile = $otp_mobile_prefix == "0" ? substr($data["mobile"],1) : $data["mobile"];
                                $res["sms"] = SiteHelpers::sendOtpUsingCoreSms($data['phone_code'].$otp_mobile);

//                            $vsid = SiteHelpers::sendOtpUsingTwilio($student->user_id, $data['phone_code'], $data['mobile']);

                            if($res["sms"]->responseCode == 200){
                                $res['status'] = true;
                                $res['message'] = "Verification code successfully sent on your mobile number.";
                            } else {
                                $res['status'] = false;
                                $res['message'] = "Unable to send verification code";
                            }

                        } else {
                            $res['status'] = false;
                            $res['message'] = "Wrong registered mobile number";
                        }
                    } catch (Exception $e) {
                        $res['status'] = false;
                        $res['message'] = $e->getMessage();
                    }

                    return json_encode($res);
                }
            }
        } else {
            return "Invalid request";
        }
    }

    /**
     * Verify user otp
     * return status true / false with error message
     */
    public function verifyOtp(Request $request)
    {
        if ($request->isMethod('post')) {
            $res['status'] = false;
            $res['message'] = null;

            $data = $request->all();

            if ($data['type'] == 'verify-otp') {
                try {

                    $user_id = $request->all('user_id');

                    if ($user_id) {

                        $otpdetails = DB::table("mobile_verifications")
                                            ->where('user_id', $user_id)
                                            ->where('mobile', $request->all('mobile'))
                                            ->orderBy('id','desc')->first();

//                        if(!empty($otpdetails->vsid)){

							/* $diffMinutes = SiteHelpers::createdAtdiffInMinutes($otpdetails->created_at);
							$checkOtpTime = SiteHelpers::validOtpTime($diffMinutes);

							if($checkOtpTime){ */

//                            $vsid = $otpdetails->vsid;
                            $code = $request->input('otp');

//                            $status = SiteHelpers::verifyOtpUsingTwilio($vsid, $code);
                              $otp_mobile_prefix = substr($request->mobile,0,1);
                              $otp_mobile = $otp_mobile_prefix == "0" ? substr($request->mobile,1) : $request->mobile;

                             $status = SiteHelpers::verifyOtpUsingSmsCore($request->mobile,$code);

                            if($status["responseCode"] == 200) {
                                User::where('id', $user_id)->update(['mobile_verified_at' => Carbon::now()]);
                                $res['status'] = true;
                                $res['message'] = "Verification code verified successfully.";
                            } else {
                                $res['status'] = false;
                                $res['message'] = "Wrong verification code.";
                            }
							/* }else{

								$res['status'] = false;
                                $res['message'] = "Your OTP expired.";
							} */

                    } else {
                        $res['status'] = false;
                        $res['message'] = "Wrong registered mobile number.";
                        //return redirect()->route('registerStep', 2)->withErrors($validator)->withInput();
                    }
                } catch (Exception $e) {
                    $res['status'] = false;
                    $res['message'] = $e->getMessage();
                }

                return json_encode($res);
            }
        } else {
            return "Invalide request";
        }
    }

    /**
     * Search school as per institution
     * return status true / false with error message
     */
    public function searchschool(Request $request)
    {
        $data = $request->all();
        //$res['status'] = false;
        //$res['school_data'] = null;


        //$schools = School::where('status', 1)->where('school_name', 'like', $data['keyword'] . '%');
        $schools = School::where('school_category', $data['category_id']);
        //if(isset($data['category_id']) && !empty($data['category_id'])) {
        //$schools = $schools->where('school_category', $data['category_id']);
        //}
        $schools = $schools->select('id', 'school_name')->get();
        $schoolclasses = $schools;

        // if (!$schools->isEmpty()) {

        // } else {
        //     echo "Invalid School Name!";
        // }
        return view('auth.forms.schoolclasses', compact('schoolclasses'));
        // return view('auth.forms.searchschool', compact('schools'));


        //return json_encode($res);
        //return view('auth.forms.searchschool', compact('schools'));
    }

    public function categoryschool(Request $request)
    {
        $data = $request->all();
        //$res['status'] = false;
        //$res['school_data'] = null;

        //$schools = School::where('status', 1)->where('school_name', 'like', $data['keyword'] . '%');
        $schools = School::where('school_category', $data['category_id']);
        //if(isset($data['category_id']) && !empty($data['category_id'])) {
        //$schools = $schools->where('school_category', $data['category_id']);
        //}
        $schools = $schools->select('id', 'school_name')->get();

        // if (!$schools->isEmpty()) {

        // } else {
        //     echo "Invalid School Name!";
        // }

        // return view('auth.forms.searchschool', compact('schools'));
        return view('auth.forms.schoolclasses', compact('schools'));


        //return json_encode($res);
        //return view('auth.forms.searchschool', compact('schools'));
    }

    /**
     * Search school as per institution
     * return status true / false with error message
     */
    public function schoolcourses(Request $request)
    {
        $data = $request->all();
        $optionName = "Course";

        if (!empty($request->optionName)) {
            $optionName = $request->optionName;
        }
        if ($request->listData == 'department') {
            $schoolcourses = Department::where('status', 1)->where('school_id', $data['school_id'])->select('id', 'name')->get();
        } else {
            $schoolcourses = Course::where('status', 1)->where('school_id', $data['school_id'])->select('id', 'name')->get();
        }

        return view('auth.forms.schoolcourses', compact('schoolcourses', 'optionName'));
    }

    /**
     * Classes list by ajax
     */
    public function schoolclasses(Request $request)
    {
        $data = $request->all();

        if ($data['category_id'] == School::BASIC_SCHOOL_OPEN || $data['category_id'] == School::JHS_SCHOOL_OPEN || $data['category_id'] == School::SENIOR_HIGH_OPEN || $data['category'] == School::SENIOR_HIGH_OPEN_TVET) {
                $school = School::where('school_category', $data['category_id'])->first();
                $schoolcourses = Course::where('school_id', $school->id)->first();
                if (!empty($schoolcourses->id)) {
                    $course_id = $schoolcourses->id;
                }

        }

        $schoolclasses = Classes::where('course_id', $course_id)->where('status', 1)->select('id', 'class_name')->get();

        //return $course_id;

        return view('auth.forms.schoolclasses', compact('schoolclasses'));
    }

    public function searchSchoolByName(Request $request) {
        $data = $request->school_name;

        $schools = School::where('school_name',"LIKE","%$data%")->selectRaw("school_name as text, id")->get();

        return response()->json($schools, 200);
    }

    public function signUpNewUser(Request $request){

        \Illuminate\Support\Facades\Request::validate([
            'username' => ['required','unique:users','regex:/^\S*$/u'],
            'email' => ['required','email','unique:users'],
            'name' => ['required'],
            'password' => ['required'],
            'mobile'   => ['required'],
            'termsCheckBox'   => ['required'],
        ]);

        $data['otp'] = mt_rand(10000, 99999);

        $user = User::create([
            'username' => $request->username,
            'name'     => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $rejister_as = $request->tutor == true ? "tutor" : "student";

        $role = \Illuminate\Support\Facades\DB::table('roles')->where('slug', '=',
            $rejister_as)->first();  //choose the default role upon user creation.

        $fullname = explode(" ",$request->name);

        $data['username'] = $request->username;
        $data['first_name'] = $fullname[0];
        $data['last_name'] = isset($fullname[1]) ? $fullname[1] : "";
        $data['email'] = $request->email;
        $data['mobile'] = $request->mobile;
        $data['phone_code'] = "233";

        /* Below code for assign user role */
        $user->attachRole($role, $user->id);

        if($rejister_as == 'student'){
            /* Below code for save student data */
            $user->insertStudent($user, $data);
        }

        if($rejister_as == 'tutor'){
            /* Below code for save tutor data */
            $user->insertTutor($user, $data);
            session(['userId' => $user->id]);
            session(['vsid' => $user->id]);
            session(['newCustomer' => 1]);

//            return redirect()->to("/register/step2");
        }

//        $otp_mobile_prefix = substr($request->mobile,0,1);
//        $otp_mobile = $otp_mobile_prefix == "0" ? substr($data["mobile"],1) : $data["mobile"];
//        SiteHelpers::sendOtpUsingCoreSms($data['phone_code'].$otp_mobile);

        session(['newCustomer' => 1]);

        try{
            if(!empty($user->email)) {
                Mail::to($user->email, "New registration on " . env('APP_NAME', 'Xtra Class'))->send(new sendEmailtoNewuser($user, $rejister_as));
            }
        }catch (Exception $exception){

        }

        $credentials = ["username" => $request->username,"password" => $request->password];
        Auth::attempt($credentials);

        return $user;
    }
}
