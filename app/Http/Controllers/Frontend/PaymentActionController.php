<?php

namespace App\Http\Controllers\Frontend;

use App\User;
use Carbon\Carbon;
use App\Models\Plans;
use App\Models\Student;
use App\Models\UserPlan;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\PaymentNotification;
use App\Traits\SendSMSNotification;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PaymentActionController extends Controller
{
    use PaymentNotification;

    use SendSMSNotification;

    public $plans, $user, $student, $userPlan, $uplan = '';
    public function __construct(Plans $plans, User $user, UserPlan $userPlan, Student $student)
    {
        $this->plans = $plans;
        $this->user = $user;
        $this->userPlan = $userPlan;
        $this->student = $student;
    }

    public function actionPay(Request $request)
    {

        $isUserPlan = $this->plans->userPlanCheck();



        // if ($isUserPlan != '' && $isUserPlan > 0) {
        //     return redirect()->back()->with('jsSuccess', "Subscription In Progress, You have $isUserPlan days more for next subscription");
        // //    return \response()->json(['message' => "Subscription In Progress, You have $isUserPlan days more for next subscription"]);
        // }

        // return "process payments";

        $txnRef =  Str::uuid();
        $planId = $request->plan;
        $activeMode = ($request->has('activate_mode')) ? $request->activate_mode : '';

        $myplan = $this->plans::find($planId);
        $this->uplan = $myplan;


        $duration = $myplan->duration;
        $planAmount = $myplan->amount;

        $user = Auth::user();
        $stuent = $user->student;
        $fullname = $stuent->first_name." ".$stuent->last_name;


        $userNetwork = $request->has('network') ?  $request->network : 'MTN';
        $userMomo = $request->has('momo_number') ?  $request->momo_number: "0244566965";

        $Rurl = URL::to('/');

        $clientIp = \request()->ip();
        $formParams = array();
        $formParams['tx_ref'] =  $txnRef;
        $formParams['amount'] =  $planAmount;
        $formParams['currency'] = "GHS";
        $formParams['voucher'] = "143256783";
        $formParams['network'] = $userNetwork;
        $formParams['email'] =  $user->email;
        $formParams['phone_number'] = $userMomo;
        $formParams['fullname'] = $fullname;
        $formParams['client_ip'] =  $clientIp;
        // $formParams['device_fingerprint'] = "62wd23423rq324323qew1";
        $formParams['redirect_url'] = $Rurl."/payment";

        $requestPay = $this->performPaymentRequest('POST', $formParams);

        $data = (Object)json_decode($requestPay, true);

        $redirect = $data->meta['authorization']['redirect'];

        $addPlan =  $this->addUserPlan($myplan, $txnRef, $activeMode);

        if ($addPlan) {
            return Redirect::to($redirect);
        }

    }


    public function paymentProcess(Request $request)
    {
        $presponse = $request->query();


        $data = json_decode($presponse['resp'], true);

        if ($data['status'] == 'error') {
            return redirect()->to('/profile')->with('jsError', "Could not process payment Please contact XtraClass for assistance. Thank you");
        }
        $resdata = $data['data'];

        $resPoint = $this->updatePlanTxnStatus($resdata);

        return Redirect::to('/profile');
    }


    public function addUserPlan($planData = '', $txRef = '', $activeMode = '')
    {

        $myplan = $planData;
        $payStatus = "pending";
        $carbon = new Carbon();
        $user = Auth::user();

        $startOn = $carbon->now();
        $startOn  =  date('Y-m-d H:i:s', strtotime($startOn));
        $duration = $planData->duration;

        $subsDate = strtotime("+ $duration", strtotime($startOn));
        $expDuration =  date('Y-m-d H:i:s', $subsDate);

        $userPlan = UserPlan::create([
                'user_id' => $user->id,
                'plan_id' => $myplan->id,
                'starts_on' => $startOn,
                'expired_on' => $expDuration,
                'activate_mode_type' => $activeMode,
                'payment_status' => $payStatus
            ]
        );

        $subsTransaction = $this->userPlan->insertSubscriptionTransaction($user, $userPlan, $txRef);

        return \response()->json($userPlan);
    }

    public function updatePlanTxnStatus($txnData = '')
    {
        $payStatus = "pending";
        $carbon = new Carbon();
        $status = 1;
        $user = Auth::user();
         $userPlan = collect($user->plan)->last();

        $userPlan = DB::table('user_plans')
                    ->where('id', $userPlan->id)
                    ->update([
                                'payment_status' => $payStatus,
                                'transaction_id' => $txnData['id'],
                                'transaction_cust_id' => $txnData['customerId'],
                                'transaction_acc_id' => $txnData['AccountId']
                            ]);

        $this->userPlan->updateSubscriptionTransaction($user, $txnData);
        $this->student->updatedStudentTableSubscribe($user->id, $status);
    }

    public function getUserPlan()
    {
        $user = Auth::user();
        $userPlan = collect($user->plan)->last();
        $plan = $this->plans::planDateDiff($userPlan->starts_on, $userPlan->expired_on);
        return \response()->json($plan);
    }


    public function requestAccess()
    {
        $pfx = "233";

        $user = Auth::user();

        $student = $user->student;
        $numBer = $student->mobile;

        $getNum = Str::after($numBer, "0");
        $mobileNum = "$pfx$getNum";

        $msg = "Thank you for joining XtraClass. To Subscribe,\nPurchase any of the Subscription plans below to access all lesson\n-Monthly(30 days) GHC 25.00 + Free Data, \n-Quarterly (3 months) GHC 75.00 + Free Data,\n-Yearly (12 months) GHC 265 + Free Data. \nGo to www.dextraclass.com Login and make payment or \nCall 0509962708 for Access";



        $student->request_access = 1;

        $access = $student->save();

        $vsms = $this->sendMessage($msg, $mobileNum);


        if ($access && $vsms) {
            return $this->respond($this->successStatus, 'Access Request in Progress, Call 0509962708 for further assistance');
        }
        return $this->respond($this->badRequestStatus, 'Sorry Could not Proccess, Call 0509962708 for further assistance');

    }

}
