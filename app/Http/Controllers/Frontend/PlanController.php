<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Plans;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{
    public $plan;
    public function __construct(Plans $plan)
    {
        $this->plan = $plan;
    }

    public function index()
    {
        $plans = $this->plan::all();
        return \response()->json($plans);
    }


    public function allPlans()
    {
        $plans = $this->plan::all();
        return view('frontend.student.plan', compact('plans'));
    }
}
