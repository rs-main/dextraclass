<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SchoolManager;
use App\Models\School;
use App\Models\SchoolCategory;
use App\User;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\Mail\sendEmailtoSchoolmanager;
use Illuminate\Support\Facades\Mail;
use Auth;


class SchoolManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
		if(Auth::user()->hasRole('school')){
			return redirect()->route('backend.dashboard');
		}
		$schoolmanagers = SchoolManager::orderBy('id', 'desc')->get();
		//pr($schoolmanagers); exit;
        return view('backend.schoolmanagers.index', compact('schoolmanagers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if(Auth::user()->hasRole('school')){
			return redirect()->route('backend.dashboard');
		}
		$institutes = SchoolCategory::orderBy('name')->where('status','=',1)->pluck('name','id');
		return view('backend.schoolmanagers.create', compact('institutes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$data = $request->all();
		
		$validator = Validator::make($data, [
				'institute_type' => 'required',
				'school_name' => 'required',
				'first_name' => 'required|min:2',
				'username' => 'required|string|min:4|max:255|regex:/^(?=.*[a-z]).+$/|unique:users',
				'password' => 'required|confirmed|string|min:6',
				//'password' => 'required|confirmed|string|min:8|regex:/^(?=.*[\w])(?=.*[\W])[\w\W]{6,}$/',
				'email' => 'email|max:255|unique:users',
				'mobile' => 'required|numeric|unique:school_managers',
			],[
			   'username.regex' => "Username must be contains At least one lowercase",
			  // 'password.regex' => "Password must be contains minimum 8 character with at least one lowercase, one uppercase, one digit, one special character",
			]);
			
			if ($validator->fails()) {
				return Redirect::back()
					->withErrors($validator) // send back all errors to the form
					->withInput();
			}
			
			//Persist the school manager in the database
			//form data is available in the request object
			$schoolmanager = new SchoolManager();
			
			/** Below code for save photo * */
			if ($request->hasFile('photo')) {

				$validator = Validator::make($request->all(), [
									'photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
									],[
										'photo.max' => 'The profile photo may not be greater than 2 mb.',
									]);

				if ($validator->fails()) {
					return redirect()->route('backend.managers.create')->withErrors($validator)->withInput();
				}

				$destinationPath = public_path('/uploads/schools/managers/');
				$newName = '';
				$fileName = $request->all()['photo']->getClientOriginalName();
				$file = request()->file('photo');
				$fileNameArr = explode('.', $fileName);
				$fileNameExt = end($fileNameArr);
				$newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;

				$file->move($destinationPath, $newName);

				$imagePath = 'uploads/schools/managers/' . $newName;
				$schoolmanager->profile_image = $newName;
			}
		
			$user = User::create([
			'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
			]);
			
			$role = \DB::table('roles')->where('slug', '=', 'school')->first();
			$user->attachRole($role, $user->id);
			
			$schoolmanager->user_id    = $user->id;
			$schoolmanager->first_name = $data['first_name'];
			$schoolmanager->last_name  = $data['last_name'];
			$schoolmanager->email      = $data['email'];
			$schoolmanager->mobile     = $data['mobile'];
			$schoolmanager->school_id =  $data['school_name'];
			$schoolmanager->status = 	 ($data['status'] !== null) ? $data['status'] : 0;
			$schoolmanager->country    = $data['phone_code'];
			
			$schoolmanager->save(); //persist the data
			
			$schoolmanagerdata = SchoolManager::findOrFail($schoolmanager->id);
			$schoolmanagerdata->school_name = $schoolmanagerdata->school->school_name;
			//save user other information
			$user_more_info = User::find($user->id);
			$user_more_info->name = $data['first_name'];
			$user_more_info->mobile_verified_at = date('Y-m-d H:i:s');
			$user_more_info->save(); //persist the data
			
			if(!empty($user->email))
			Mail::to($user->email, "New manager on ".env('APP_NAME', 'Xtra Class'))->send(new sendEmailtoSchoolmanager($schoolmanagerdata));
			
			return redirect()->route('backend.managers.index')->with('success','School Manager Created Successfully');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		if(Auth::user()->hasRole('school')){
			return redirect()->route('backend.dashboard');
		}
       $schoolmanager = SchoolManager::findOrFail($id);
	   return view('backend.schoolmanagers.show',compact('schoolmanager'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if(Auth::user()->hasRole('school')){
			return redirect()->route('backend.dashboard');
		}
       //Find the school manager
        $schoolmanager = SchoolManager::find($id);
	
		$institutes = SchoolCategory::orderBy('name')->where('status','=',1)->pluck('name','id');
		$schools = School::orderBy('school_name')->where('status','=',1)->where('school_category',$schoolmanager->school->school_category)->pluck('school_name','id');
		
		return view('backend.schoolmanagers.edit',compact('schoolmanager', 'institutes', 'schools'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Retrieve the school manager and update
        $schoolmanager = SchoolManager::find($id);
		$user_id = $schoolmanager->user_id;
		$data = $request->all();
		
		$validator = Validator::make($data, [
				'institute_type' => 'required',
				'school_name' => 'required',
				'first_name' => 'required|min:2',
				'password' => 'nullable|confirmed|string|min:6',
				'email' => [
						'email',
						'required',
						'max:180',
						Rule::unique('users')->where(function ($query) use($user_id) {
										return $query->where('id', '<>', $user_id);
									})
						],
				'mobile' => [
							'required',
							'numeric',
							Rule::unique('school_managers')->where(function ($query) use($user_id) {
											return $query->where('user_id', '<>', $user_id);
										})
						],
				],[
					// 'password.regex' => "Password must be contains minimum 8 character with at least one lowercase, one uppercase, one digit, one special character",
			]);
			
			if ($validator->fails()) {
				return Redirect::back()
					->withErrors($validator) // send back all errors to the form
					->withInput();
			}
			
			if ($request->hasFile('photo')) {

				$validator = Validator::make($request->all(), [
									'photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
									],[
										'photo.max' => 'The profile photo may not be greater than 2 mb.',
									]);

				if ($validator->fails()) {
					return redirect()->route('backend.managers.edit', $id)->withErrors($validator)->withInput();
				}

				$destinationPath = public_path('/uploads/schools/managers/');
				$newName = '';
				$fileName = $request->all()['photo']->getClientOriginalName();
				$file = request()->file('photo');
				$fileNameArr = explode('.', $fileName);
				$fileNameExt = end($fileNameArr);
				$newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;

				$file->move($destinationPath, $newName);
				
				$oldImage = public_path('uploads/schools/managers/' . $schoolmanager->profile_image);
				//echo $oldImage; exit;
				if (!empty($schoolmanager->profile_image) && file_exists($oldImage)) {
					unlink($oldImage);
				}

				$imagePath = 'uploads/schools/managers/' . $newName;
				$schoolmanager->profile_image = $newName;
			}
			
			$schoolmanager->first_name = $data['first_name'];
			$schoolmanager->last_name  = $data['last_name'];
			$schoolmanager->email      = $data['email'];
			$schoolmanager->mobile     = $data['mobile'];
			$schoolmanager->school_id =  $data['school_name'];
			$schoolmanager->status = 	($data['status'] !== null) ? $data['status'] : 0;
			$schoolmanager->country    = $data['phone_code'];
			
			$schoolmanager->save(); //persist the data
			
			//save user other information
			$user_more_info = User::find($schoolmanager->user_id);
			if(isset($data['password']) && !empty($data['password'])) {
				$user_more_info->password = Hash::make($data['password']);
			}
			$user_more_info->email = $data['email'];
			$user_more_info->save(); //persist the data
		
		   return redirect()->route('backend.managers.index')->with('success','School Manager Information Updated Successfully');
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$schoolmanager = SchoolManager::find($id);
		
		$user = User::find($schoolmanager->user_id);
		$user->delete();
		
        $schoolmanager->delete();
		
		
		return redirect()->route('backend.managers.index')->with('success','School Manager Deleted Successfully');
		
	}
}
