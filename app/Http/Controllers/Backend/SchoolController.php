<?php

namespace App\Http\Controllers\backend;

use App\District;
use App\Http\Controllers\Controller;
use App\Location;
use App\Region;
use App\SchoolProgrammes;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Course;
use App\Models\Department;
use App\Models\SchoolSemester;
use App\Models\Classes;
use App\Models\Subject;
use App\Models\Period;
use App\Models\Topic;
use App\Models\Video;
use App\Models\Tutor;
use App\Models\Student;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Helpers\SiteHelpers;
use Yajra\DataTables\DataTables;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = School::where('id','<>',0);
        //Check for the user profile
        if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            if(isset($profile->school_id)){
                $query = $query->where('id','=',$profile->school_id);
            }
        }

        $schools = $query->orderBy('id', 'desc')->paginate();

        return view('backend.school.index', compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$query = SchoolCategory::where('status','=',1);

		if(Auth::user()->hasRole('school')){
            return redirect()->route('backend.dashboard');
        }

        $categories = $query->orderBy('name')->get();

        return view('backend.school.create', compact('categories'));
    }

	function total_featured_school() {

		$featured_school_limit = config("constants.FEATURED_SCHOOL_LIMIT");
		$total_featured_count = School::where('status','=',1)->where('featured', 1)->where('deleted_at', NULL)->count();

		if($total_featured_count<$featured_school_limit) {
			return false;
		} else {
			return true;
		}

	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$data = $request->all();

        $featured_limit_reached = $this->total_featured_school();

		if($featured_limit_reached && $request->input('featured') !== null) {
			$featured_school_limit = config("constants.FEATURED_SCHOOL_LIMIT");
			$error_msg = "You have reached your maximum limit ".$featured_school_limit." of featured school allowed";
			return Redirect::back()
                            ->withErrors(['error'=>$error_msg]) // send back all errors to the form
                            ->withInput();
		}

		$school_category = $request->input('school_category');

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
                    'school_name' => [
                        'required',
                        'max:180',
                        Rule::unique('schools')->where(function ($query) use($school_category) {
                                    return $query->where('school_category', $school_category);
                                })
                    ],
        ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the form
                            ->withInput();
        }

        //Persist the school in the database
        //form data is available in the request object
        $school = new School();
        //input method is used to get the value of input with its
        //name specified
        $school->school_name = $request->input('school_name');
        $school->short_name = $request->input('short_name');
        $school->school_category = $request->input('school_category');
        $school->description = $request->input('description');

        $street_name = $request->street_name;
        $district = $request->district;
        $region = $request->region;
        $coordinates = $request->coordinates;
        $ghana_post_code = $request->ghana_post_code;

        if (!(Location::whereName(trim($street_name))->first())) {

            $location = new Location();
            $location->name = $street_name;
            $location->ghana_post_code = $ghana_post_code;
            $location->coordinates = $coordinates;

            //Save region id
            if ($region_object = Region::whereName(trim($region))->first()) {
                $location->region_id = $region_object->id;
            } else {
                $saved_region_object = Region::create(["name" => $region]);
                $location->region_id = $saved_region_object->id;
            }

            //save district id
            if ($district_object = District::whereName(trim($district))->first()) {
                $location->district_id = $district_object->id;
            } else {
                $saved_district_object = District::create(["name" => $district]);
                $location->district_id = $saved_district_object->id;
            }

            if ($location_id = $location->save()) {
                $school->location_id = $location_id;
            }

        }else{

            // if location is already in the database
            $location = Location::whereName(trim($street_name))->first();
            $school->location_id = $location->id;
        }

        $school->gender = $request->input("gender");
        $school->boarding_status = $request->input("boarding_status");

        $this->setSchoolSettings($request, $school, $data);

		$school->is_locked = ($request->input('is_locked') !== null) ? $request->input('is_locked') : 0;
		$school->physically_challenged = ($request->input('physically_challenged') !== null) ? $request->input('physically_challenged') : 0;

        /** Below code for save school logo * */
        if ($request->hasFile('school_logo')) {

            $validator = Validator::make($request->all(), [
                        'school_logo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
                            ], [
                        'school_logo.max' => 'The school logo may not be greater than 2 mb.',
                            ]
            );

            if ($validator->fails()) {
                return redirect()->route('backend.school.create')->withErrors($validator)->withInput();
            }

            $destinationPath = public_path('/uploads/schools/');
            $newName = '';
            $fileName = $request->all()['school_logo']->getClientOriginalName();
            $file = request()->file('school_logo');
            $fileNameArr = explode('.', $fileName);
            $fileNameExt = end($fileNameArr);
            $newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;

            $file->move($destinationPath, $newName);

            //** Below commented code for resize the image **//

            /* $user_config = json_decode(options['user'],true);

              $img = Image::make(public_path('/uploads/users/'.$newName));
              $img->resize($user_config['image']['width'], $user_config['image']['height']);
              $img->save(public_path('/uploads/users/'.$newName)); */

            $imagePath = 'uploads/schools/' . $newName;
            $school->logo = $newName;
        }



        $school->save(); //persist the data

		//add one basic education course when select basic school
		if(isset($school->id) && $school->school_category == config("constants.BASIC_SCHOOL")
            || isset($school->id) && $school->school_category == config("constants.BASIC_SCHOOL_OPEN")) {
			$course = new Course();
			$course->name = "Basic Education";
			$course->school_id = $school->id;
			$course->description = "Basic Education";
			$course->save(); //persist the data
		}

		if (isset($school->id)){
            $school_programmes = SchoolProgrammes::whereSchoolId($school->id)->first();

            $this->saveSchoolProgrammes($school_programmes, $request, $school);
        }

        return redirect()->route('backend.schools')->with('success', 'School Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!Auth::user()->hasAccessToSchool($id)){
            return redirect()->route('backend.dashboard');
        }

        $school = School::findOrFail($id);
        $semester_labels = $school->semesterNameArray();
        $school_semesters = SchoolSemester::where('school_id', $id)->get();

        $semesters = [];
        foreach ($semester_labels as $key => $name) {
            $semesters[$key] = [
                'name' => $name,
                'date_begin' => '',
                'date_end' => '',
                'status' => false
            ];
        }
        foreach ($school_semesters as $semester) {
            $semesters[$semester->semester]['date_begin'] = $semester->date_begin;
            $semesters[$semester->semester]['date_end'] = $semester->date_end;
            $semesters[$semester->semester]['status'] = true;
        }


        $courses = Course::where('school_id', $id)->orderBy('id', 'desc')->get();
        $departments = Department::where('school_id', $id)->orderBy('id', 'desc')->get();

		$department_count = SiteHelpers::dashboard_resource_count('departments', $id);
		$courses_count = SiteHelpers::dashboard_resource_count('courses', $id);
		$classes_count = SiteHelpers::dashboard_resource_count('classes', $id);
		$videos_count = SiteHelpers::dashboard_resource_count('videos', $id);
        $student_count = SiteHelpers::dashboard_resource_count('students', $id);
        $tutor_count = SiteHelpers::dashboard_resource_count('tutors', $id);

        return view('backend.school.show', compact('school', 'courses', 'departments', 'semesters', 'department_count', 'courses_count', 'classes_count', 'videos_count', 'student_count', 'tutor_count'));
    }

    /**
     * Save semester and term info.
     * @return \Illuminate\Http\Response
     */
    public function savesemester(Request $request)
    {
        $school_id = $request->school;

		if(!Auth::user()->hasAccessToSchool($school_id)){
            return redirect()->route('backend.dashboard');
        }

        $school = School::find($school_id);
        $semester_labels = $school->semesterNameArray();

        $validator = Validator::make($request->all(), [
                    'check1' => 'required',
                    'start_date1' => 'required_with:check1',
                    'end_date1' => 'nullable|required_with:check1|after:start_date1',
                    'start_date2' => 'required_with:check2',
                    'end_date2' => 'nullable|required_with:check2|after:start_date2',
                    'start_date3' => 'required_with:check3',
                    'end_date3' => 'nullable|required_with:check3|after:start_date3',
                ], [
                    'check1.required' => 'Please choose at least ' . $semester_labels[1] . '.',
                    'start_date1.required_with' => 'The Start date field is required for ' . $semester_labels[1] . '.',
                    'end_date1.required_with' => 'The End date field is required for ' . $semester_labels[1] . '.',
                    'start_date2.required_with' => 'The Start date field is required for ' . $semester_labels[2] . '.',
                    'end_date2.required_with' => 'The End date field is required for ' . $semester_labels[2] . '.',
                    'start_date3.required_with' => 'The Start date field is required for ' . $semester_labels[3] . '.',
                    'end_date3.required_with' => 'The End date field is required for ' . $semester_labels[3] . '.',
                    'end_date1.after' => 'The End date must be a date after Start date for ' . $semester_labels[1] . '.',
                    'end_date2.after' => 'The End date must be a date after Start date for ' . $semester_labels[2] . '.',
                    'end_date3.after' => 'The End date must be a date after Start date for ' . $semester_labels[3] . '.'
                ]
        );


        if ($validator->fails()) {
            return redirect()->route('backend.school.show', $request->school)->withErrors($validator)->withInput();
        }

        //delete record before update the new
        if (!empty($request->input('school'))) {
            SchoolSemester::where('school_id', $request->input('school'))->delete();
        }

        $semester_options = [];
        foreach ($semester_labels as $key => $label){
            if (!empty($request->input('check' . $key))) {
                $semester_options[] = $request->input('check' . $key);

            }
        }

        foreach ($semester_options as $semester_key) {
            $schoolsemester = new SchoolSemester();
            $schoolsemester->school_id = $request->input('school');
            $schoolsemester->category_id = $school->school_category;
            $schoolsemester->semester = $semester_key;
            $schoolsemester->date_begin = $request->input("start_date" . $semester_key);
            $schoolsemester->date_end = $request->input("end_date" . $semester_key);
            $schoolsemester->save();
        }

        return redirect()->route('backend.school.show', $request->input('school'))->with('success', 'Semester Information Saved Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if(Auth::user()->hasRole('school')){
            return redirect()->route('backend.dashboard');
        }

        //Find the school
         $school = School::leftJoin("locations","locations.id","=","schools.location_id")
            ->leftJoin("school_programmes","school_programmes.school_id","=","schools.id")
           ->where("schools.id",$id)->selectRaw("schools.*,schools.id as school_id,
           locations.id as location_id, locations.name as location_name,locations.coordinates,
           school_programmes.agriculture,school_programmes.business,school_programmes.tech,school_programmes.visual_arts,school_programmes.gen_arts,school_programmes.gen_science")->first();
        $categories = SchoolCategory::where('status', 1)->get();

        return view('backend.school.edit', compact('school', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

		$data = $request->all();

		$featured_limit_reached = $this->total_featured_school();

		if($featured_limit_reached && $request->input('featured') !== null) {
			$featured_school_limit = config("constants.FEATURED_SCHOOL_LIMIT");
			$error_msg = "You have reached your maximum limit ".$featured_school_limit." of featured school allowed";
			return Redirect::back()
                            ->withErrors(['error'=>$error_msg]) // send back all errors to the form
                            ->withInput();
		}

        $school_category = $request->input('school_category');

        $validator = Validator::make($request->all(), [
                    'school_name' => [
                        'required',
                        'max:180',
                        Rule::unique('schools')->where(function ($query) use($school_category, $id) {
                                    return $query->where('school_category', $school_category)->where('id', '<>', $id);
                                })
                    ],
        ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                            ->withErrors($validator) // send back all errors to the form
                            ->withInput();
        }

        //Retrieve the school and update
        $school = School::find($id);
        //echo "<pre>"; print_r($school); exit;
        $school->school_name = $request->input('school_name');
        $school->short_name = $request->input('short_name');
        // $school->school_category = $request->input('school_category');
        $school->description = $request->input('description');
        $school->gender = $request->input("gender");
        $school->boarding_status = $request->input("boarding_status");
        $school->ownership = $request->input("ownership");
        $school->physically_challenged = $request->input("physically_challenged") == "on";
        $school->contact_person = $request->input("contact_person");
        $school->contact_person_number = $request->input("contact_person_number");
        $school->form_one = $request->input("form_one");
        $school->form_two = $request->input("form_two");
        $school->form_three = $request->input("form_three");
        $school->track = $request->input("track");
        $school->sid_status = $request->input("sid_status");

        $street_name = $request->street_name;
        $district = $request->district;
        $region = $request->region;
        $coordinates = $request->coordinates;
        $ghana_post_code = $request->ghana_post_code;

        $location_change_status = $request->location_change_status;

        if ($location_change_status == "true") {

            if (!(Location::whereName(trim($street_name))->first())) {

                $location = new Location();
                $location->name = $street_name;
                $location->ghana_post_code = $ghana_post_code;
                $location->coordinates = $coordinates;

                if (!empty($region)) {
                    //Save region id
                    if ($region_object = Region::whereName(trim($region))->first()) {
                        $location->region_id = $region_object->id;
                    } else {
                        $saved_region_object = Region::create(["name" => $region]);
                        $location->region_id = $saved_region_object->id;
                    }
                }

                if (!empty($district)) {
                    //save district id
                    if ($district_object = District::whereName(trim($district))->first()) {
                        $location->district_id = $district_object->id;
                    } else {
                        $saved_district_object = District::create(["name" => $district]);
                        $location->district_id = $saved_district_object->id;
                    }
                }

                if ($location_id = $location->save()) {
                    $school->location_id = $location_id;
                }

            } else {

                // if location is already in the database
                $location = Location::whereName(trim($street_name))->first();
                $school->location_id = $location->id;
            }
        }

        $school->is_locked = ($request->input('is_locked') !== null) ? $request->input('is_locked') : 0;

        if ($request->hasFile('school_logo')) {

            $validator = Validator::make($request->all(), [
                        'school_logo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
                            ], [
                        'school_logo.max' => 'The school logo may not be greater than 2 mb.',
                            ]
            );

            if ($validator->fails()) {
                return redirect()->route('backend.school.edit', $id)->withErrors($validator)->withInput();
            }

            $destinationPath = public_path('/uploads/schools/');
            $newName = '';
            $fileName = $request->all()['school_logo']->getClientOriginalName();
            $file = request()->file('school_logo');
            $fileNameArr = explode('.', $fileName);
            $fileNameExt = end($fileNameArr);
            $newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;

            $file->move($destinationPath, $newName);

            $oldImage = public_path('/uploads/schools/' . $school->logo);
            if (!empty($school->logo) && file_exists($oldImage)) {
                unlink($oldImage);
            }

            $imagePath = 'uploads/schools/' . $newName;
            $school->logo = $newName;
        }

        $school->track = $request->input("track");

        $school->save(); //persist the data

        $this->saveSchoolCourses($id, $request);

//        return response()->json(["data" => $school]);

        return redirect()->route('backend.schools')->with('success', 'School Information Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if(Auth::user()->hasRole('school')){
            return redirect()->route('backend.dashboard');
        }

        $school = School::find($id);

		//delete all child records.
		//delete school
		if(isset($school->id) && !empty($school->id)) {

			$departments = Department::where('school_id', $school->id)->select('id')->get();
			//delete department
			foreach($departments as $department) {
				if(isset($department->id) && !empty($department->id))
				$department->delete();
			}

			$courses = Course::where('school_id', $school->id)->select('id')->get();
			//delete course
			foreach($courses as $course) {
				if(isset($course->id) && !empty($course->id)){

					$classes = Classes::where('course_id', $course->id)->select('id')->get();

					foreach($classes as $class) {
						if(isset($class->id) && !empty($class->id)) {
							$subjects = Subject::where('class_id', $class->id)->select('id')->get();

							foreach($subjects as $subject) {
								if(isset($subject->id) && !empty($subject->id)) {

									$topics = Topic::where('subject_id', $subject->id)->select('id')->get();

									foreach($topics as $topic) {
										if(isset($topic->id) && !empty($topic->id))
										$topic->delete();
									}

									$subject->delete();
								}
							}

							$periods = Period::where('class_id', $class->id)->select('id')->get();

							foreach($periods as $period) {
								if(isset($period->id) && !empty($period->id))
								$period->delete();
							}

							$class->delete();
						}
					}
					$course->delete();
				}
			}

			$videos = Video::where('school_id', $school->id)->select('id')->get();
			foreach($videos as $video) {
				if(isset($video->id) && !empty($video->id))
				$video->delete();
			}

			$tutors = Tutor::where('school_id', $school->id)->select('id')->get();
			foreach($tutors as $tutor) {
				if(isset($tutor->id) && !empty($tutor->id))
				$tutor->delete();
			}

			$students = Student::where('school_id', $school->id)->select('id')->get();
			foreach($students as $student) {
				if(isset($student->id) && !empty($student->id))
				$student->delete();
			}


		}

		$school->delete();
        return redirect()->route('backend.schools')->with('success', 'School Deleted Successfully');
    }

    public function getSchoolsData(){
        $schools = School::where('id','<>',0)->selectRaw("schools.*")
            ->orderBy("created_at","desc")
            ->get();

        $index = 0;
        return DataTables::of($schools)

            ->addColumn('school_name',function ($row){
                $text = $row->school_name;
                return $text;
            })

            ->addColumn('institution',function ($row){
                $btn = $row->category != null ? $row->category->name : "";
                return $btn;
            })

            ->addColumn('featured',function ($row){
                $btn = $row->featured ? 'Yes' : 'No';
                return $btn;
            })

            ->addColumn('locked',function ($row){
                $btn = $row->locked ? 'Yes' : 'No';
                return $btn;
            })->addColumn('status',function ($row){
                    $btn = $row->status ? 'Active':'Inactive';
                    return $btn;

            })->addColumn('action', function($row){
                $btn =
                    "<a style='margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;'
                       href= ".route('backend.school.show', $row['id'])."
                       class='btn btn-success btn-xs md-btn-flat article-tooltip' title='View school details'>
                            <i class='ion ion-md-eye'></i> </a>".
                    '<a style="margin-right: 5px; margin-left: 5px;padding-top: 4px; padding-bottom: 4px;"
                       href= '.route('backend.school.edit', $row['id'])."
                       class='btn btn-primary btn-xs md-btn-flat article-tooltip' title='View school details'>
                            <i class='ion ion-md-create'></i> </a>";

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);

    }

    /**
     * @param $school_programmes
     * @param Request $request
     * @param School $school
     */
    public function saveSchoolProgrammes($school_programmes, Request $request, School $school): void
    {
        if ($school_programmes) {
            $school_programmes->update([
                "agriculture" => $request->has("agriculture") && !empty($request->agriculture),
                "business" => $request->has("business") && !empty($request->business),
                "tech" => $request->has("tech") && !empty($request->tech),
                "visual_arts" => $request->has("visual_arts") && !empty($request->visual_arts),
                "gen_arts" => $request->has("gen_arts") && !empty($request->gen_arts),
                "gen_science" => $request->has("gen_science") && !empty($request->gen_science)
            ]);
        } else {
            SchoolProgrammes::create([
                "school_id" => $school->id,
                "agriculture" => $request->has("agriculture") && !empty($request->agriculture) ? true : false,
                "business" => $request->has("business") && !empty($request->business) ? true : false,
                "tech" => $request->has("tech") && !empty($request->tech) ? true : false,
                "visual_arts" => $request->has("visual_arts") && !empty($request->visual_arts) ? true : false,
                "gen_arts" => $request->has("gen_arts") && !empty($request->gen_arts) ? true : false,
                "gen_science" => $request->has("gen_science") && !empty($request->gen_science) ? true : false
            ]);
        }
    }

    /**
     * @param int $id
     * @param Request $request
     */
    public function saveSchoolCourses(int $id, Request $request): void
    {
        if (isset($id)) {
            $school_programmes = SchoolProgrammes::whereSchoolId($id)->first();

            if ($school_programmes) {
                $school_programmes->update([
                    "agriculture" => $request->has("agriculture") && !empty($request->agriculture) ? true : false,
                    "business" => $request->has("business") && !empty($request->business) ? true : false,
                    "tech" => $request->has("tech") && !empty($request->tech) ? true : false,
                    "visual_arts" => $request->has("visual_arts") && !empty($request->visual_arts) ? true : false,
                    "gen_arts" => $request->has("gen_arts") && !empty($request->gen_arts) ? true : false,
                    "gen_science" => $request->has("gen_science") && !empty($request->gen_science) ? true : false
                ]);
            } else {
                SchoolProgrammes::create([
                    "school_id" => $id,
                    "agriculture" => $request->has("agriculture") && !empty($request->agriculture) ? true : false,
                    "business" => $request->has("business") && !empty($request->business) ? true : false,
                    "tech" => $request->has("tech") && !empty($request->tech) ? true : false,
                    "visual_arts" => $request->has("visual_arts") && !empty($request->visual_arts) ? true : false,
                    "gen_arts" => $request->has("gen_arts") && !empty($request->gen_arts) ? true : false,
                    "gen_science" => $request->has("gen_science") && !empty($request->gen_science) ? true : false
                ]);
            }
        }
    }

    /**
     * @param Request $request
     * @param $school
     * @param array $data
     */
    public function setSchoolSettings(Request $request, $school, array $data): void
    {
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('subadmin')) {
            $school->theme = $request->input('theme');
            $school->status = ($request->input('status') !== null) ? $request->input('status') : 0;
            $school->featured = ($request->input('featured') !== null) ? $request->input('featured') : 0;
            $school->restrict_to_student = isset($data['restrict_to_student']) ? $data['restrict_to_student'] : 0;
            $school->student_limit = (isset($data['student_limit']) && !empty($data['student_limit']) && isset($data['restrict_to_student'])) ? $data['student_limit'] : 0;
        }
    }

}
