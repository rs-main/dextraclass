<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Department;
use App\Models\QuestionBank;
use App\Models\QuestionOptions;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Subject;
use App\PastQuestion;
use App\PastQuestionBank;
use App\PastQuestionGroup;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;


class QuestionsBankController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.questions.index3');
    }

    public function getQuestionsData(){
        $questionsbankBuilder = QuestionBank::with("school")->leftJoin("subjects","subjects.id","=","questions_bank.subject_id");

        if(isset(Auth::user()->profile)) {
            $profile = Auth::user()->profile;
            $school_details = isset($profile->school_id) ? $profile->school_id : null;

            if ($school_details) {
                $questionsbankBuilder->where("subject_name", "NOT LIKE", "%past questions%")
                    ->where("questions_bank.school_id", $school_details)
                    ->where("subject_name", "NOT LIKE", "%objectives%");
            } else {
                $questionsbankBuilder->where("subject_name", "NOT LIKE", "%past questions%")
                    ->where("subject_name", "NOT LIKE", "%objectives%");
            }
        }

        $questions_bank= $questionsbankBuilder
            ->orderBy("questions_bank.created_at","desc")
            ->selectRaw("questions_bank.* ,subjects.subject_name as name,
                subjects.id as subject_id")
            ->orderBy("questions_bank.created_at","desc")
            ->get();

        return Datatables::of($questions_bank)
            ->addColumn('question', function ($question_bank) {
                return view( 'backend.questions.partials.actions.question', compact('question_bank'));
            })
            ->addColumn('school_name', function ($question_bank) {
                return view( 'backend.questions.partials.actions.school_name', compact('question_bank'));
            })
            ->addColumn('subject', function ($question_bank) {
                return view( 'backend.questions.partials.actions.subject', compact('question_bank'));
            })
            ->addColumn('type', function ($question_bank) {
                return view( 'backend.questions.partials.actions.type', compact('question_bank'));
            })
            ->addColumn('created_by', function ($question_bank) {
                return view( 'backend.questions.partials.actions.created', compact('question_bank'));
            })
            ->addColumn('status', function ($question_bank) {
                return view( 'backend.questions.partials.actions.status', compact('question_bank'));
            })
            ->addColumn('action', function ($question_bank) {
                return view( 'backend.questions.partials.actions.action', compact('question_bank'));
            })
            ->rawColumns([ 'question', 'school_name', 'subject', 'type',
                'created_by', 'status','action' ])
            ->make(true);
    }

    public function indexOld()
    {
        $question_query = \request()->query("question");
        $subject_query = \request()->query("subject");
        $school_query = \request()->query("school");
        $questions = null;
        $has_subject = \request()->has("subject");
        $has_question = \request()->has("question");
        $has_school = \request()->has("school");

        $questionsbankBuilder = QuestionBank::leftJoin("subjects","subjects.id","=","questions_bank.subject_id");

        if(isset(Auth::user()->profile)) {
            $profile = Auth::user()->profile;
            $school_details = isset($profile->school_id) ? $profile->school_id : null;

            if ($school_details) {
                $questionsbankBuilder->where("subject_name", "NOT LIKE", "%past questions%")
                    ->where("questions_bank.school_id", $school_details)
                    ->where("subject_name", "NOT LIKE", "%objectives%")
                    ->whereNull("question_type");
            } else {
                $questionsbankBuilder->where("subject_name", "NOT LIKE", "%past questions%")
                    ->where("subject_name", "NOT LIKE", "%objectives%")
                    ->whereNull("question_type");
            }
        }


        if ( $has_subject || $has_question || $has_school){

            $questions =  QuestionBank::leftJoin("subjects","subjects.id","=","questions_bank.subject_id");

                if (!empty(\request()->query("question"))){
                    $questions->where("question","LIKE","%".$question_query."%");
                };
                if (!empty(\request()->query("subject"))){
                   $questions ->where("subjects.id",$subject_query);
                }
                if (!empty(\request()->query("school"))) {
                    $questions->where("school_id", $school_query);
                }
        }

        $count = !is_null($questions) ? $questions->count() : $questionsbankBuilder->count();
        $paginator = new LengthAwarePaginator(range(1, $count), ($count), 15, Paginator::resolveCurrentPage(), ['path' => Paginator::resolveCurrentPath()]
        );

        $questionsData = !is_null($questions) ? $questions->orderByDesc("questions_bank.created_at")
            ->selectRaw("questions_bank.* ,subjects.subject_name as name,
                subjects.id as subject_id")->paginate(15) : $questionsbankBuilder
            ->orderBy("questions_bank.created_at","desc")
            ->selectRaw("questions_bank.* ,subjects.subject_name as name,
                subjects.id as subject_id")->paginate();

        return view('backend.questions.index2', compact('questionsData','paginator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = Auth::user()->profile;
        $school_details = isset($profile->school_id)? $profile->school_id : null;

        $institutes_builder = SchoolCategory::leftJoin("schools","schools.school_category","=","school_categories.id");
        if ($school_details){
            $institutes = $institutes_builder->
            where("schools.school_name","NOT LIKE","%Take a Course")->where("school_categories.status","=",1)
                ->selectRaw("school_categories.name, school_categories.id,school_categories.status")
                ->pluck("name","id");
        }else {
            $institutes = $institutes_builder->where('school_categories.status', '=', 1)
                ->selectRaw("school_categories.name, school_categories.id,school_categories.status")
                ->pluck('name', 'id');
        }
        return view('backend.questions.create', compact('institutes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $options[] = $request->options;
        $options[] = [
            $request->answer1, $request->answer2,
            $request->answer3, $request->answer4
        ];
        //$request['suitable_word'] = \implode(",", $request->suitable_words);

        $exams_date = null;
        if ($request->has("year")) {
            $exams_date_input = $request->get($request->year);
            $exams_date = Carbon::parse($exams_date_input);
            $exams_date->format('Y-m-d');
        }

        $questionBank = QuestionBank::create([
            'type' => $request->type,
            'question' => $request->question,
            'choice_id' => $request->answer,
            'num_of_options' => $request->num_of_options,
            'active_status' => $request->active_status,
            'school_id' => $request->school_id,
            'class_id' => $request->class_id,
            'subject_id' => $request->subject_id,
            'question_type' => $request->past_question_type,
            'year'          => $exams_date,
            'has_image' => false
        ]);

        if ($request->has('image_question')) {

            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'image_question' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            ], [
                    'image_question.max' => 'The image may not be greater than 2mb.',
                ]
            );

            if ($validator->fails()) {
                return redirect()->route('backend.questions.create')->withErrors($validator)->withInput();
            }

            $destinationPath = public_path('/uploads/questions/');
            $newName = '';
            $fileName = $request->all()['image_question']->getClientOriginalName();
            $file = request()->file('image_question');
            $fileNameArr = explode('.', $fileName);
            $fileNameExt = end($fileNameArr);
            $newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;

            $file->move($destinationPath, $newName);

            //** Below commented code for resize the image **//

            /* $user_config = json_decode(options['user'],true);

              $img = Image::make(public_path('/uploads/users/'.$newName));
              $img->resize($user_config['image']['width'], $user_config['image']['height']);
              $img->save(public_path('/uploads/users/'.$newName)); */

            $imagePath = 'uploads/questions/' . $newName;

            QuestionBank::find($questionBank->id)->update([
                "image" => $imagePath,
                "has_image" => true
            ]);
        }


        foreach ($options as $value) {
            foreach ($value as $val){
                QuestionOptions::create([
                    'title' => $val,
                    'active_status' => 1,
                    'question_bank_id' => $questionBank->id,
                    'school_id' => $request->school_id
                ]);
            }
        }

        $data = array(
            'url' => URL::to('/admin/questions'),
            'success' => true);

        return $this->respond($this->successStatus,'Question addition successful',$data);
    }

    public function respond($statusCode, $message, $data = null) {
        return response()->json([
            'statusCode' => $statusCode,
            'message' => $message,
            'data' => $data ? $data : new \StdClass()
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $question = QuestionBank::findOrFail($id);

        if(!Auth::user()->hasAccessToSchool($question->school_id)){
            return redirect()->route('backend.dashboard');
        }
        $options = QuestionOptions::where('question_bank_id', $question->id)->get();

        return view('backend.questions.show', compact('question', 'options'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $question = QuestionBank::findOrFail($id);

        if(!Auth::user()->hasAccessToSchool($question->school_id)){
            return redirect()->route('backend.dashboard');
        }

        $institutes = SchoolCategory::orderBy('name')->where('status', '=', 1)->pluck('name', 'id');
        $classes_details = Classes::where("id", $question->class_id)->select('course_id')->where('status', '=', 1)->first();
        $question->course_id = $classes_details->course_id;


        $school_details = School::where("id", $question->school_id)->select('school_category')->where('status', '=', 1)->first();
        $question->category_id = $school_details->school_category;

        $schools = School::where('school_category', $question->category_id)->orderBy('school_name')->where('status', '=', 1)->pluck('school_name', 'id');
        $courses = Course::where('school_id', $question->school_id)->orderBy('name')->where('status', '=', 1)->pluck('name', 'id');
        $classes = Classes::where('course_id', $question->course_id)->orderBy('class_name')->where('status', '=', 1)->pluck('class_name', 'id');
        $subjects = Subject::where('class_id', $question->class_id)->orderBy('subject_name')->where('status', '=', 1)->pluck('subject_name', 'id');

        $options = QuestionOptions::where('question_bank_id', $question->id)->get();

        return view('backend.questions.edit', compact('question', 'institutes', 'schools', 'courses', 'classes', 'subjects', 'options'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $question = QuestionBank::findOrFail($id);

        if(!Auth::user()->hasAccessToSchool($question->school_id)){
            return redirect()->route('backend.dashboard');
        }

        $institutes = SchoolCategory::orderBy('name')->where('status', '=', 1)->pluck('name', 'id');
        $classes_details = Classes::where("id", $question->class_id)->select('course_id')->where('status', '=', 1)->first();
        $question->course_id = $classes_details->course_id;


        $school_details = School::where("id", $question->school_id)->select('school_category')->where('status', '=', 1)->first();
        $question->category_id = $school_details->school_category;

        $schools = School::where('school_category', $question->category_id)->orderBy('school_name')->where('status', '=', 1)->pluck('school_name', 'id');
        $courses = Course::where('school_id', $question->school_id)->orderBy('name')->where('status', '=', 1)->pluck('name', 'id');
        $classes = Classes::where('course_id', $question->course_id)->orderBy('class_name')->where('status', '=', 1)->pluck('class_name', 'id');
        $subjects = Subject::where('class_id', $question->class_id)->orderBy('subject_name')->where('status', '=', 1)->pluck('subject_name', 'id');

        $options = QuestionOptions::where('question_bank_id', $question->id)->get();

        return view('backend.questions.edit', compact('question', 'institutes', 'schools', 'courses', 'classes', 'subjects', 'options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = QuestionBank::find($id);
        $subject = $question->subject_id;

        //$classes_details = Classes::where('id', $class_id)->first();

        if(!Auth::user()->hasAccessToSchool($question->school_id)){
            return redirect()->route('backend.dashboard');
        }

        $validator = Validator::make($request->all(), [
            'question' => [
                'required',
                Rule::unique('questions_bank')->where(function ($query) use($subject, $id) {
                    return $query->where('subject_id', $subject)->where('id', '<>', $id);
                })
            ]
        ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the form
                ->withInput();
        }

        $question->question = $request->input('question');
        $question->active_status = ($request->input('status') !== null) ? $request->input('status') : 0;
        $question->save(); //persist the data

        if ($request->has("past-question-type")){
            return redirect()->route('backend.past-questions.index')->with('success', 'Question Updated Successfully');
        }

        return redirect()->route('backend.questions.index')->with('success', 'Question Updated Successfully');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $options = QuestionOptions::where("question_bank_id",$id)->get();

        foreach($options as $option){
            QuestionOptions::destroy($option->id);
        }
            QuestionBank::destroy($id);
            return redirect()->route('backend.past-questions.index')
                ->with('success', 'Question Deleted Successfully!');

    }

    public function getTypeAheadQuestions(){

        $query = \request()->query("q");
        $questions = QuestionBank::where("question","LIKE", "%".$query."%")
            ->with(["subject","class","school"])
//            ->forPage(\request()->query('q')?:1,15);
        ->take(15)->get();

        return $questions;
    }

    public function getTypeAheadSchools(){

        $query = \request()->query("q");
        $schools = School::with("category")
                ->orwhere("school_name","LIKE", "%".$query."%")
                ->orWhere("short_name", "LIKE","%".$query."%")
                ->selectRaw("schools.id, schools.school_name as text")
                ->take(15)
                ->get();
        return $schools;
    }

    public function getSelectSubjects(){

        $query = \request()->query("q");
        $subjects = Subject::leftJoin("classes","classes.id","=","subjects.class_id")
            ->leftJoin("courses","courses.id","=","classes.course_id")
        ->Where("subject_name","LIKE", "%".$query."%")
            ->where("subject_name","not like","%past questions%")
            ->where("subject_name","not like","%objectives%")
            ->selectRaw("subjects.*, classes.class_name, classes.id as class_id,
            courses.name as course_name, courses.id as
            course_id,courses.school_id")
            ->take(30)
            ->get();

        return $subjects;
    }

//    public function getPastQuestions()
//    {
//        $question_query = \request()->query("question");
//        $subject_query = \request()->query("subject");
//        $school_query = \request()->query("school");
//        $questions = null;
//
//        $questionsBankBuilder = QuestionBank::leftJoin("subjects",
//            "subjects.id", "=", "questions_bank.subject_id")
//            ->orWhere("subject_name", "LIKE", "%past questions%")
//            ->orWhere("subject_name", "LIKE", "%Objectives%")
//            ->orWhere("question_type", "past_question")
//            ->whereNotNull("question_type");
//
//
//        if (\request()->has("subject") || \request()->has("question") || \request()->has("school") || \request()->query("exam-year")) {
//            $questions =  $questionsBankBuilder = QuestionBank::leftJoin("subjects",
//                "subjects.id", "=", "questions_bank.subject_id");
//
//            if (!empty(\request()->query("question"))) {
//                $questions->where("question", "LIKE", "%" . $question_query . "%");
//            };
//            if (!empty(\request()->query("subject"))) {
//                $questions->where("subjects.id", $subject_query);
//            }
//            if (!empty(\request()->query("school"))) {
//                $questions->where("school_id", $school_query);
//            }
//
////            if (!empty(\request()->query("exam-year"))) {
////                $questions->whereYear("year", "LIKE","%".\request()->query("exam-year"."%"));
////            }
//        }
//
//        $count = !is_null($questions) ? $questions->count() : $questionsBankBuilder->count();
//        $paginator = new LengthAwarePaginator(range(1, $count), ($count), 15, Paginator::resolveCurrentPage(), ['path' => Paginator::resolveCurrentPath()]
//        );
//
//         $questionsData = !is_null($questions) ? $questions->orderByDesc("questions_bank.created_at")
//            ->selectRaw("questions_bank.* ,subjects.subject_name as name,
//                subjects.id as subject_id")->paginate(15) : $questionsBankBuilder
//            ->orderBy("questions_bank.created_at","desc")
//            ->selectRaw("questions_bank.* ,subjects.subject_name as name,
//                subjects.id as subject_id")->paginate();
//
//        $yearCollections = new Collection();
//        $questionYears = QuestionBank::whereNotNull("year")
//            ->groupBy("year")->distinct()->get();
//
//        foreach ($questionYears as $questionYear){
//            if (!$yearCollections->contains(Carbon::parse($questionYear->year)->year)) {
//                $yearCollections->push(Carbon::parse($questionYear->year)->year);
//            }
//        }
//
//        return view('backend.questions.past-questions.index', compact('questionsData', 'paginator',"yearCollections"));
//    }

    public function getPastQuestions()
    {
        $institutes = SchoolCategory::orderBy('name')->where('status','=',1)->pluck('name','id');

        $past_question_groups = PastQuestionGroup::with(["pastquestions","subject:id,subject_name","class:id,class_name"])
            ->leftJoin("months","months.id","=","past_questions_group.month")
//            ->leftJoin("past_questions","past_questions.past_question_group_id","=","past_questions_group.id")
            ->selectRaw("past_questions_group.*, months.id as month_id, months.month_name as month_name")
            ->orderByDesc("created_at")
            ->paginate();
        return view('backend.pastquestions.index3',compact("institutes","past_question_groups"));
    }


    public function getEditPastQuestions($id=null)
    {
        if (\request()->has("group-id")){
            $past_question_group = PastQuestionGroup::where("past_questions_group.id",\request()->query("group-id"))
                ->leftJoin("months","months.id","=","past_questions_group.month")
                ->first();
        }else{
            $past_question_group = PastQuestionGroup::where("past_questions_group.id",$id)
                ->leftJoin("months","months.id","=","past_questions_group.month")
                ->first();
            }
            $group_id = \request()->has("group-id") ? request()->query("group-id") : $id;
            $questions = PastQuestionBank::where('past_question_group_id',$group_id)->get();
            $classes = Classes::where("class_name","LIKE","%JHS%")->orWhere("class_name","LIKE","%SHS%")->pluck("id");
            $subjects = Subject::whereIn("class_id",$classes)->selectRaw("id,subject_name")->distinct("id")->get();

            return view('backend.pastquestions.edit', compact('past_question_group',
                                                    'questions','group_id','subjects'));
    }

    public function postUpdateGroupQuestions(Request $request): JsonResponse
    {
//        return response()->json(["data" => $request->get("questions_object")]);

        $question_group_id = $request->past_question_group_id;
        $questions_object = $request->get("questions_object");
        $class_id = $request->get("class_id");
        $subject_id = $request->get("subject_id");

        foreach($questions_object as $object){
            DB::table("past_question_bank")->insert([
                "question"       => $object["question"],
                "num_of_options" => $object["num_of_options"],
                "type"           => $object["type"],
                "class_id"       => $class_id,
                "subject_id"     => $subject_id,
                "answer_options" => json_encode($object["answers"]),
                "past_question_answer_id" => $object["answer"],
                "past_question_group_id"  => $question_group_id
            ]);
        }

        $data = array(
            'url' => URL::to('/admin/quiz'),
            'success' => true);

        return $this->respond($this->successStatus,'Question(s) added successful!',$data);
    }

    public function getPastQuestionsGroupCreate()
    {
        return view('backend.questions.past-question-groups.create');
    }

    public function postPastQuestionsGroupStore(Request $request)
    {
        $past_question_group = PastQuestionGroup::create($request->all());

        $has_section = $request->get("has_section") == "on";

        if ($request->has("has_section") && $has_section){
            PastQuestionGroup::find($past_question_group->id)
                ->update([
                    "has_section" => $has_section,
                    "section_counts" => $request->section_counts,
                    "section_naming_type" => $request->section_naming_type
                ]);
        }

        return redirect()->route('backend.past-questions.index')->with("Successfully created a group!");
    }

    public function getPastQuestionsCreate()
    {
        $classes = Classes::where("class_name","LIKE","%JHS%")->orWhere("class_name","LIKE","%SHS%")->pluck("id");
        $subjects = Subject::whereIn("class_id",$classes)->selectRaw("id,subject_name")->distinct("id")->get();
        $past_question_groups = PastQuestionGroup::selectRaw("id,group_name")->get();
        return view('backend.pastquestions.create',compact("subjects","past_question_groups"));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function getPastQuestionsEdit($id)
    {
//        $question = DB::table("past_questions")
//            ->leftJoin("")

        return view('backend.pastquestions.edit', compact(''));
    }

}
