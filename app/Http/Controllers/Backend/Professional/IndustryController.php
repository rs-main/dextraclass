<?php

namespace App\Http\Controllers\Backend\Professional;


use App\Helpers\SiteHelpers;
use App\Http\Controllers\Controller;
use App\Industry;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Note;
use App\Models\ReportVideo;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class IndustryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {

        $industries = DB::table('industries')
            ->leftjoin('professional_school_categories','professional_school_categories.industry_id','=','industries.id')
            ->selectRaw('COUNT(professional_school_categories.name) as category_counts, industries.*')
            ->groupBy('industries.id')
            ->paginate();

        return view('backend.professional.industries.index',compact("industries"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($school_id = ''){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Industry::create($request->all());

        return redirect()->route('backend.industries.index')->with('success', 'Industry created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Industry::find($id)->update([
            "name" => $request->name
        ]);
        return redirect()->route('backend.industries.index')->with('success', 'Item successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Industry::destroy([$id]);
        return redirect()->route('backend.industries.index')->with('success', 'Industry removed successfully!');
    }

}
