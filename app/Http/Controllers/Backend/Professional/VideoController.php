<?php

namespace App\Http\Controllers\Backend\Professional;


use App\Helpers\SiteHelpers;
use App\Http\Controllers\Controller;
use App\MainCourse;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Note;
use App\Models\ReportVideo;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Topic;
use App\Models\Video;
use App\ProfessionalCategory;
use App\ProfessionalVideoTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{

    public function listVideos(){

        $courses = array();
        $classes = array();
        $course_query = \request()->query("course");
        $search_query = \request()->query("q");


        $query = Video::leftJoin("topics","topics.id","=","videos.topic_id")
            ->where('videos.id','<>',0)->where("school_id",School::getProfessionalCourseSchoolId())
            ->selectRaw("videos.*, topics.topic_name");

        if (\request()->has("course")){
            $query->whereCourseId($course_query);
        }

        if (\request()->has("search")){
            $query->orWhere("topics.topic_name","LIKE","%$search_query%")
                  ->orWhere("videos.description","LIKE","%$search_query%");
        }

        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            if(isset($profile->school_id)){
                $query = $query->where('school_id','=',$profile->school_id);
                $courses = Course::where('school_id',$profile->school_id)->where('status',1)->orderBy('name')->select('id', 'name')
                    ->get();
                $school_details = School::where('id', $profile->school_id)->select('school_category')->first();
                if($school_details->school_category === config("constants.BASIC_SCHOOL")){

                    if(isset($courses[0]->id)) {
                        $classes = Classes::where('course_id',$courses[0]->id)->where('status',1)->orderBy('class_name')
                            ->pluck('class_name','id');
                    }
                }
            }
        }

        $videos = $query->orderBy('videos.id', 'desc')->paginate(12);

        $repoted_count = array();

        foreach($videos as $video) {
            $report_video_count = ReportVideo::where('video_id', $video->id)->count();
            $repoted_count[$video->id] = $report_video_count;
        }

        $query = School::where('status','=',1);

        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $query = $query->where('id','=',$school_id);
            }
        }

        $schools = $query->orderBy('school_name')
            ->pluck('school_name','id');

        $professional_courses = DB::table("courses")->where("type","professional")->get();

        return view('backend.professional.videos.index',
            compact('videos', 'repoted_count', 'schools', 'courses', 'classes','professional_courses'));

    }

    public function showVideo($uuid){
        $video = Video::uuid($uuid);
        $reported_video_detail = ReportVideo::where('video_id', $video->id)->get();
        if(!\Auth::user()->hasAccessToSchool($video->school_id)){
            return redirect()->route('backend.dashboard');
        }

        return view('backend.videos.regular.show', compact('video', 'reported_video_detail'));
    }

    public function createVideo(){

        $query = SchoolCategory::where('status','=',1);
        $school_id = 0;
        $category_id = 0;

        //Check for the user profile
        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $category_id = $profile->school->school_category;
                $query = $query->where('id','=',$profile->school->school_category);
            }
        }

        $institute = $query->where("name","LIKE","%Take a Course%")->first();
        $school = School::where("school_name","LIKE","%Take a Course%")->first();

         $professional_courses = ProfessionalCategory::with(
               ["courseCategory" => function($category){
            return $category->with("course")->get();}])->get()->jsonSerialize();

         $topics = DB::table("topics")->where("subject_id",NULL)->get();

        return view('backend.professional.videos.create',
            compact('institute','school_id','category_id',
                'professional_courses','school','topics'));
    }

    public function editVideo($uuid)
    {
        $school_id = 0;
        $category_id = 0;
        $video = Video::uuid($uuid);

        if(!\Auth::user()->hasAccessToSchool($video->school_id)){
            return redirect()->route('backend.dashboard');
        }

        $query = SchoolCategory::where('status','=',1);

//        if(\Auth::user()->hasRole('school')){
//            $profile = \Auth::user()->profile;
//            $query = $query->where('id','=',$profile->school->school_category);
//        }

        //Check for the user profile
        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $category_id = $profile->school->school_category;
                $query = $query->where('id','=',$profile->school->school_category);
            }
        }

        $institute = $query->where("name","LIKE","%Take a Course%")->first();

        $school = School::where("school_name","LIKE","%Take a Course%")->first();

        $professional_courses = ProfessionalCategory::with(
            ["courseCategory" => function($category){
                return $category->with("course")->get();}])->get()->jsonSerialize();

        $topics = DB::table("topics")->where("subject_id",NULL)->get();

        return view('backend.professional.videos.edit',
            compact('institute','school_id','category_id','video',
                'professional_courses','school','topics'));
    }

    public function updateVideo(Request $request, $id){

        $validator = \Validator::make($request->all(), [
            'school' => 'required',
            'course' => 'required',
            'video_type' => 'required',
            'topic_id' => 'required',
//            'video_url' => 'required_if:video_type,url',
            //'video_file' => 'required|mimes:mp4,webm,wmv,avi,flv,mov',
            'description' => 'required',
        ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the form
                ->withInput();
        } else {
            //form data is available in the request object
            $video = Video::find($id);

            $video_id = $video->video_id;
//            if ($request->video_type == 'url') {
                $video_url = $request->video_url;
//
                $video_data = SiteHelpers::getVimeoVideoData($video_url);
//
//                if (!isset($video_data->video_id) || empty($video_data->video_id)) {
//
//                    $validator->errors()->add('video_url', 'Invalid Video URL');
//
//                    return Redirect::back()
//                        ->withErrors($validator) // send back all errors to the form
//                        ->withInput();
//                } else {
//                    $video_id = $video_data->video_id;
//                }
//            }

            $note = null;
            if ($request->note_file != '') {

                $path = $request->note_file;

                //Create Note
                $note = new Note();
                $note->tutor_id = $request->tutor;
                $note->file_url = $path;
                $note->storage = 's3';
                $note->status = 1;
                $note->save();

                $video->note_id = isset($note->id) ? $note->id : 0;
            }

            if (isset($request->video_file) && $request->video_file != '') {
                $mob_url = $request->video_file;
                $video->video_file = $mob_url;
            }

            //$video->school_id = $request->school;
            $video->course_id = $request->course;
            $video->class_id = $request->class;
            $video->play_on = $request->date;
            $video->period_id = $request->period;
            $video->video_id = $video_id;
            $video->video_url = $request->video_url;
            $video->video_type = $request->video_type;
            //$video->video_file = $request->video_file;
            $video->description = $request->description;
            $video->subject_id = $request->subject;
            $video->topic_id = $request->topic_id;
            $video->keywords = $request->keywords;
            $video->tutor_id = $request->tutor;
            //$video->user_id = Auth::user()->id;
            $video->status = ($request->input('status') !== null) ? $request->input('status') : 0;

            if ( $request->has("display_image")) {

                $destinationPath = public_path('/uploads/videos/');
                $newName = '';
                $fileName = $request->all()['display_image']->getClientOriginalName();
                $file = request()->file('display_image');
                $fileNameArr = explode('.', $fileName);
                $fileNameExt = end($fileNameArr);
                $newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;
                $file->move($destinationPath, $newName);
                $imagePath = 'uploads/videos/' . $newName;
                $video->display_image = $newName;
            }

            $video->save();

//            if ($request->video_type == 'file') {
//                return redirect()->route('backend.professional.videos.index', $video->uuid)->with('success', 'Video created Successfully, please uplaod video file for it. ');
//            }

            return redirect()->route('backend.professional.videos.show',$video->uuid)->with('success', 'Video Updated Successfully');
        }

    }

    public function store(Request $request)
    {
//        $s3_base_url_plus_video_path =
//            "https://xtraclassnotes.s3-us-west-2.amazonaws.com/".$request->get("video_file");

        $validator = Validator::make($request->all(), [
            'school' => 'required',
            'course' => 'required',
            'video_type' => 'required',
            'topic_id' => 'required',
//            'video_url' => 'required_if:video_type,url',
            //'video_file' => 'required|mimes:mp4,webm,wmv,avi,flv,mov',
            'description' => 'required',
            //'note_file' => 'sometimes|mimes:jpeg,jpg,png,pdf,doc,docx,ppt,pptx,zip|max:20480',
        ]);


        // if the validator fails, redirect back to the form
        if ($validator->fails()) {

            return Redirect::back()
                ->withErrors($validator) // send back all errors to the form
                ->withInput();
        } else {

            $video_id = '';
            if($request->video_type == 'url') {
                $video_url = $request->video_url;

//                $video_data = SiteHelpers::getVimeoVideoData($video_url);

//                if(!isset($video_data->video_id) || empty($video_data->video_id)){
//
//                    $validator->errors()->add('video_url', 'Invalid Video URL');
//
//                    return Redirect::back()
//                        ->withErrors($validator) // send back all errors to the form
//                        ->withInput();
//                } else {
//                    $video_id = $video_data->video_id;
//                }
            }


            $note = null;

            if ($request->note_file != '') {

                $path = $request->note_file;

                //Create Note
                $note = new Note();
                $note->tutor_id = $request->tutor;
                $note->file_url = $path;
                $note->storage = 's3';
                $note->status = 1;
                $note->save();
            }

            //form data is available in the request object
            $video = new Video();
            $video->school_id = $request->school;
            $video->course_id = $request->course;
            $video->class_id = $request->class;
            $video->play_on = $request->date;
            $video->period_id = $request->period;
            $video->video_id = $video_id;
            $video->video_url = $request->video_url;
            $video->video_type = $request->video_type;
            $video->description = $request->description;
            $video->subject_id = $request->subject;
            $video->topic_id = $request->topic_id;
            $video->keywords = $request->keywords;
            $video->tutor_id = $request->tutor;
            $video->note_id = isset($note->id)? $note->id : 0;
            $video->user_id = Auth::user()->id;
            $video->status = ($request->input('status') !== null)? $request->input('status'):0;
//            $video->mobile_video_url = $s3_base_url_plus_video_path;

            if ( $request->has("display_image")) {

                $destinationPath = public_path('/uploads/videos/');
                $newName = '';
                $fileName = $request->all()['display_image']->getClientOriginalName();
                $file = request()->file('display_image');
                $fileNameArr = explode('.', $fileName);
                $fileNameExt = end($fileNameArr);
                $newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;
                $file->move($destinationPath, $newName);
                $imagePath = 'uploads/videos/' . $newName;
                $video->display_image = $newName;
            }

            if($video_id = $video->save()) {
                $video_topic = ProfessionalVideoTopic::whereTopicId($video->topic_id)->first();
                if (!$video_topic) {
                    ProfessionalVideoTopic::create([
                        "topic_id" => $video->topic_id,
                        "video_id" => $video_id
                    ]);
                }
            }

            $path = "no video";

            if($request->video_type == 'file') {
                return redirect()->route('backend.video.upload.files',$video->uuid)
                    ->with('success', 'Video created Successfully, please upload video file for it. '.$path);
            }

            return redirect()->route('backend.professional.videos.index')
                ->with('success', 'Video created Successfully');
        }
    }

    public function storeTopic(Request $request){

        $topic = Topic::create([
            "topic_name" => $request->topic_name,
            "description" => $request->description,
            "status"    => ($request->get('status') !== null) ? $request->input('status') : 0
        ]);

        if ($request->has("video_id")) {
            return redirect()->route('backend.professional.videos.edit',$request->video_id)->with('success', 'Topic created Successfully');
        }else{
            return redirect()->route('backend.professional.videos.create')->with('success', 'Topic created Successfully');
        }

    }

}
