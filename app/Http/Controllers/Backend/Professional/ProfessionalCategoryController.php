<?php

namespace App\Http\Controllers\Backend\Professional;


use App\Helpers\SiteHelpers;
use App\Http\Controllers\Controller;
use App\Industry;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Note;
use App\Models\ReportVideo;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Video;
use App\ProfessionalCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProfessionalCategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $industries = Industry::all();
        if (\request()->has("industry_id")){
            $categories = ProfessionalCategory::whereIndustryId(\request()->query("industry_id"))->paginate();
        }else{
            $categories = ProfessionalCategory::paginate();
        }
        return view('backend.professional.categories.index',compact("categories","industries"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($school_id = ''){

        return view('backend.course.create', compact('schools', 'institutes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        ProfessionalCategory::create($request->except("industry_modal"));

        if ($request->has("industry_modal")){
            return redirect()->route('backend.industries.index')->with('success', 'Category added Successfully!');
        }

        return redirect()->route('backend.categories.index')->with('success', 'Category created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ProfessionalCategory::find($id)->update($request->all());
        return redirect()->route('backend.professional.categories.index')->with('success', 'Category updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        ProfessionalCategory::destroy($id);

        return redirect()->route('backend.professional.categories.index')->with('success', 'Category successfully deleted!');

    }

}
