<?php

namespace App\Http\Controllers\Backend\Professional;


use App\CourseCategory;
use App\Helpers\SiteHelpers;
use App\Http\Controllers\Controller;
use App\Industry;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Note;
use App\Models\ReportVideo;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Video;
use App\ProfessionalCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CourseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
         $courses = Course::leftJoin("course_category",
             "course_category.course_id","=","courses.id")
            ->leftJoin("professional_school_categories",
                "professional_school_categories.id","=","course_category.category_id")
             ->leftJoin("industries","industries.id","=","professional_school_categories.industry_id")
            ->where("type","professional")
            ->selectRaw("courses.id, courses.name,courses.status,
            professional_school_categories.name as category_name,
            industries.name as industry_name")
            ->paginate();
        return view('backend.professional.courses.index',compact("courses"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($school_id = ''){
        $industries = Industry::all();
        $categories = ProfessionalCategory::all();
        $school = School::where("school_name","LIKE","%Take a Course%")->first();
        $school_id = $school ? $school->id : null;
        return view('backend.professional.courses.create',compact("industries","categories","school_id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $course = Course::create([
               "name" => $request->name,
               "description" => $request->description,
                "type" => $request->type,
                "status" => empty($request->status) ? false: true
           ]);

           if ( $request->has("display_image") && $request->hasFile("display_image")) {

               $destinationPath = public_path('/uploads/courses/');
               $newName = '';
               $fileName = $request->all()['display_image']->getClientOriginalName();
               $file = request()->file('display_image');
               $fileNameArr = explode('.', $fileName);
               $fileNameExt = end($fileNameArr);
               $newName = date('His') . rand() . time() . '__' . $fileNameArr[0] . '.' . $fileNameExt;
               $file->move($destinationPath, $newName);
               $imagePath = 'uploads/courses/' . $newName;

               Course::find($course->id)->update([
                   "display_image" => $newName
               ]);
           }

         if ($course){
             CourseCategory::create([
                 "course_id" => $course->id,
                 "category_id" => $request->category_id
             ]);
         }

         if ($request->has("category_modal")){
             return redirect()->route('backend.professional.categories.index')->with('success', 'Course created successfully!');
         }

        return redirect()->route('backend.professional.courses.index')->with('success', 'Course created successfully!');

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::leftJoin("course_category","course_category.course_id","=","courses.id")
            ->leftJoin("professional_school_categories",
                "professional_school_categories.id","=","course_category.category_id")
            ->leftJoin("industries","industries.id","=","professional_school_categories.industry_id")
            ->where("type","professional")
            ->where("courses.id",$id)
            ->selectRaw("courses.id as course_id, courses.description,
             courses.name,courses.status,
            professional_school_categories.id as category_id,
            professional_school_categories.name as category_name,
            industries.name as industry_name, industries.id as industry_id")
            ->first();

        $industries = Industry::all();
        $categories = ProfessionalCategory::all();
        $school = School::where("school_name","LIKE","%Take a Course%")->first();
        $school_id = $school ? $school->id : null;
        return view('backend.professional.courses.edit',
            compact("course","industries","categories","school_id"));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Course::find($id)->update($request->except(["_token","category_id","school_id"]));

            CourseCategory::whereCourseId($id)->update([
                "course_id" => $id,
                "category_id" => $request->category_id
            ]);

        return redirect()->route('backend.professional.courses.index')->with('success', 'Course updated successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (Course::find($id)){
            Course::destroy($id);
        }

        return redirect()->route('backend.professional.courses.index')->with('success', 'Course deleted successfully!');
    }

}
