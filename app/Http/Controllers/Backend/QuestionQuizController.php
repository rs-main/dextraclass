<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Question;
use App\Models\QuestionBank;
use App\Models\QuestionGroup;
use App\Models\QuestionQuiz;
use App\Models\QuizQuestion;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Subject;
use Illuminate\Http\Request;
use Auth;

class QuestionQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = School::where('status', '=', 1);
        $schools = $query->orderBy('school_name')
            ->pluck('school_name', 'id');

        $subject_query = Subject::where('status', '=', 1);
        $subject = $subject_query->orderBy('subject_name')
            ->pluck('subject_name', 'id');

        $questions = QuestionQuiz::orderBy('id', 'desc')->get();
        return view('backend.quiz.index', compact('questions', 'schools', 'subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $institutes = SchoolCategory::orderBy('name')->where('status','=',1)->pluck('name','id');
        return view('backend.quiz.create', compact('institutes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quiz = QuestionQuiz::find($id);
        $questions = QuizQuestion::where('q_quiz_id',$quiz->id)->get();

        return view('backend.quiz.show', compact('quiz','questions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quiz = QuestionQuiz::whereId($id)->first();

        $institutes = SchoolCategory::orderBy('name')->where('status', '=', 1)->pluck('name', 'id');

        $subject_details = Subject::where("id", $quiz->subject_id)->select('class_id')->first();
        $quiz->class_id = $subject_details->class_id;

        $classes_details = Classes::where("id", $quiz->class_id)->select('course_id')->where('status', '=', 1)->first();
        $quiz->course_id = $classes_details->course_id;

        $course_details = Course::where("id", $quiz->course_id)->select('school_id', 'department_id')->first();
        $quiz->school_id = $course_details->school_id;

        $school_details = School::where("id", $quiz->school_id)->select('school_category')->where('status', '=', 1)->first();
        $quiz->category_id = $school_details->school_category;

        $schools = School::where('school_category', $quiz->category_id)->orderBy('school_name')->where('status', '=', 1)->pluck('school_name', 'id');
        $courses = Course::where('school_id', $quiz->school_id)->orderBy('name')->where('status', '=', 1)->pluck('name', 'id');
        $classes = Classes::where('course_id', $quiz->course_id)->orderBy('class_name')->where('status', '=', 1)->pluck('class_name', 'id');
        $subjects = Subject::where('class_id', $quiz->class_id)->orderBy('subject_name')->where('status', '=', 1)->pluck('subject_name', 'id');

        $questions = QuizQuestion::with("questions")
            ->where('q_quiz_id',$quiz->id)->get();

        return view('backend.quiz.edit', compact('quiz', 'institutes', 'schools',
                                                        'courses', 'classes', 'subjects', 'questions',
                                                        'school_details','course_details','classes_details','subject_details'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
             $quiz = QuizQuestion::where("q_quiz_id", $id)->delete();
//            if ($quiz) {
//                $quiz->delete();

//                exit;
                $question = QuestionQuiz::find($id);
                    $question->delete();
//            }

            return response()->json(["message" => "success","question" => $question], 200);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addTopicQuiz(Request $request){
        $subject_id = $request->subject;

        $subject_details = Subject::where('id', $subject_id)->first();
        $course_id = $subject_details->subject_class->course_id;

        $course_details = Course::where('id', $course_id)->select('school_id')->first();
        if (!Auth::user()->hasAccessToSchool($course_details->school_id)) {
            return redirect()->route('backend.dashboard');
        }

        $question = QuestionQuiz::findOrFail($request->question);
        $question->lesson_id = $request->after;
        $question->save();

        return redirect()->route('backend.subjects.show', $subject_id)->with('success', 'Topic attached Successfully');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function removeTopicQuiz(Request $request){
        $subject_id = $request->subject;

        $subject_details = Subject::where('id', $subject_id)->first();
        $course_id = $subject_details->subject_class->course_id;

        $course_details = Course::where('id', $course_id)->select('school_id')->first();
        if (!Auth::user()->hasAccessToSchool($course_details->school_id)) {
            return redirect()->route('backend.dashboard');
        }

        $question = QuestionQuiz::findOrFail($request->question);
        $question->lesson_id = NULL;
        $question->save();

        return redirect()->route('backend.subjects.show', $subject_id)->with('success', 'Topic detached Successfully');

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function questionsByQuestionBankId($id): \Illuminate\Http\JsonResponse
    {
        $questions = QuestionBank::whereId($id)->get();
        return response()->json(["questions" => $questions]);
    }
}
