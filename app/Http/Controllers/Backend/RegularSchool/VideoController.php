<?php

namespace App\Http\Controllers\Backend\RegularSchool;


use App\Helpers\SiteHelpers;
use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Note;
use App\Models\ReportVideo;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class VideoController extends Controller
{

    public function listVideos(){

        $courses = array();
        $classes = array();

        $query = Video::where('id','<>',0);

        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            if(isset($profile->school_id)){
                $query = $query->where('school_id','=',$profile->school_id);
                $courses = Course::where('school_id',$profile->school_id)->where('status',1)->orderBy('name')->select('id', 'name')
                    ->get();
                $school_details = School::where('id', $profile->school_id)->select('school_category')->first();
                if($school_details->school_category === config("constants.BASIC_SCHOOL")){

                    if(isset($courses[0]->id)) {
                        $classes = Classes::where('course_id',$courses[0]->id)->where('status',1)->orderBy('class_name')
                            ->pluck('class_name','id');
                    }
                }
            }
        }

        $videos = $query->orderBy('id', 'desc')->paginate(25);

        $repoted_count = array();

        foreach($videos as $video) {
            $report_video_count = ReportVideo::where('video_id', $video->id)->count();
            $repoted_count[$video->id] = $report_video_count;
        }

        $query = School::where('status','=',1);

        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $query = $query->where('id','=',$school_id);
            }
        }

        $schools = $query->orderBy('school_name')->pluck('school_name','id');

        return view('backend.videos.regular.index', compact('videos', 'repoted_count', 'schools', 'courses', 'classes'));

    }

    public function showVideo($uuid){
        $video = Video::uuid($uuid);
        $reported_video_detail = ReportVideo::where('video_id', $video->id)->get();
        if(!\Auth::user()->hasAccessToSchool($video->school_id)){
            return redirect()->route('backend.dashboard');
        }

        return view('backend.videos.regular.show', compact('video', 'reported_video_detail'));
    }

    public function createVideo(){

        $query = SchoolCategory::where('status','=',1);
        $school_id = 0;
        $category_id = 0;

        //Check for the user profile
        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $category_id = $profile->school->school_category;
                $query = $query->where('id','=',$profile->school->school_category);
            }
        }

        $institutes = $query->orderBy('name')->pluck('name','id');

        return view('backend.videos.regular.create',  compact('institutes','school_id','category_id'));
    }

    public function editVideo($uuid)
    {
        $video = Video::uuid($uuid);

        if(!\Auth::user()->hasAccessToSchool($video->school_id)){
            return redirect()->route('backend.dashboard');
        }

        $query = SchoolCategory::where('status','=',1);

        if(\Auth::user()->hasRole('school')){
            $profile = \Auth::user()->profile;
            $query = $query->where('id','=',$profile->school->school_category);
        }

        $institutes = $query->orderBy('name')
            ->pluck('name','id');

        return view('backend.videos.regular.edit', compact('video','institutes'));
    }

    public function updateVideo(Request $request, $id){

        $validator = \Validator::make($request->all(), [
            //'school' => 'required',
            'course' => 'required',
            'class' => 'required',
            'date' => 'required',
            'period' => 'required',
            'subject' => 'required',
            'topic' => 'required',
            'video_type' => 'required',
            'video_url' => 'required_if:video_type,url',
            //'video_url' => 'required_without:video_file',
            //'video_file' => 'sometimes|mimes:mp4,webm,wmv,avi,flv,mov|max:122880',
            'description' => 'required',
            //'note_file' => 'sometimes|mimes:jpeg,png,pdf,doc,docx,ppt,pptx,zip|max:20480',
            'tutor' => 'required'
        ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the form
                ->withInput();
        } else {
            //form data is available in the request object
            $video = Video::find($id);

            $video_id = $video->video_id;
            if ($request->video_type == 'url') {
                $video_url = $request->video_url;

                $video_data = SiteHelpers::getVimeoVideoData($video_url);

//                if (!isset($video_data->video_id) || empty($video_data->video_id)) {
//
//                    $validator->errors()->add('video_url', 'Invalid Video URL');
//
//                    return Redirect::back()
//                        ->withErrors($validator) // send back all errors to the form
//                        ->withInput();
//                } else {
//                    $video_id = $video_data->video_id;
//                }
            }

            $note = null;
            if ($request->note_file != '') {

                $path = $request->note_file;

                //Create Note
                $note = new Note();
                $note->tutor_id = $request->tutor;
                $note->file_url = $path;
                $note->storage = 's3';
                $note->status = 1;
                $note->save();

                $video->note_id = isset($note->id) ? $note->id : 0;
            }

            if (isset($request->video_file) && $request->video_file != '') {
                $mob_url = $request->video_file;
                $video->video_file = $mob_url;
            }

            //$video->school_id = $request->school;
            $video->course_id = $request->course;
            $video->class_id = $request->class;
            $video->play_on = $request->date;
            $video->period_id = $request->period;
            $video->video_id = $video_id;
//            $video->video_url = $request->video_url;
            $video->video_type = $request->video_type;
            //$video->video_file = $request->video_file;
            $video->description = $request->description;
            $video->subject_id = $request->subject;
            $video->topic_id = $request->topic;
            $video->keywords = $request->keywords;
            $video->tutor_id = $request->tutor;
            //$video->user_id = Auth::user()->id;
            $video->status = ($request->input('status') !== null) ? $request->input('status') : 0;

            $video->save();

            if ($request->video_type == 'file') {
                return redirect()->route('backend.videos.show', $video->uuid)->with('success', 'Video created Successfully, please uplaod video file for it. ');
            }

            return redirect()->route('backend.regular.videos')->with('success', 'Video Updated Successfully');
        }

    }

    public function storeVideo(){

    }


    public function courseList(){

    }

    public function classesList(){

    }





}
