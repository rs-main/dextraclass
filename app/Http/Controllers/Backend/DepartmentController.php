<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Course;
use App\Models\Department;
use App\Models\Classes;
use App\Models\Subject;
use App\Models\Period;
use App\Models\Topic;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;


class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(Auth::user()->hasRole('school')){
            return redirect()->route('backend.dashboard');          
        }
		
		//Show all departments from the database and return to view
		if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            if(isset($profile->school_id)){
               $departments = Department::where('school_id', $profile->school_id)->orderBy('id', 'desc')->get();
            }            
        } else {
			$departments = Department::orderBy('id', 'desc')->get();
		}
		
        return view('backend.departments.index',compact('departments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($school_id = '')
    {
		if(Auth::user()->hasRole('school')){
            return redirect()->route('backend.dashboard');          
        }
		
		$query = SchoolCategory::where('status','=',1);
       
        //Check for the user profile      
        if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $category_id = $profile->school->school_category;
                $query = $query->where('id','=',$profile->school->school_category);
            }            
        } 
        
        $institutes = $query->where('id', config("constants.UNIVERSITY"))->orderBy('name')
                        ->pluck('name','id');
						
		return view('backend.departments.create', compact('institutes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$school_id = $request->school;
		
		 //Check for the user profile      
        if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            if($profile->school_id != $school_id){
				return redirect()->route('backend.dashboard');
			}
		}
		
		$validator = Validator::make($request->all(), [
				'school' => 'required',
				'name' => [
					'required',
					'max:180',
					Rule::unique('departments')->where(function ($query) use($school_id) {  
						return $query->where('school_id', $school_id);
					})
				],
				
			]);
			
		if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the form
                ->withInput();
        } 
		
       //Persist the employee in the database
        //form data is available in the request object
        $department = new Department();
        //input method is used to get the value of input with its
        //name specified
		$department->name = $request->input('name');
		$department->school_id = $request->input('school');
		$department->status = ($request->input('status') !== null)? $request->input('status'):0;
		$department->save(); //persist the data
		
		if(!empty($request->input('ajax_request'))) {
				return redirect()->route('backend.school.show', $department->school_id)->with('success', 'Department Created Successfully');
		} else {
			return redirect()->route('backend.departments.index')->with('success','Department Created Successfully');
		}
		
		
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$department = Department::findOrFail($id);
		
		if(!Auth::user()->hasAccessToSchool($department->school_id)){
            return redirect()->route('backend.dashboard');           
        }
		
		$courses = Course::where('department_id', $id)->orderBy('id', 'desc')->get();
		$school = School::where('id',$department->school_id)->first();
		
		return view('backend.departments.show', compact('department', 'courses', 'school'));
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       if(Auth::user()->hasRole('school')){
            return redirect()->route('backend.dashboard');          
        }
	   
	   $department = Department::findOrFail($id);
	   
	   if(!Auth::user()->hasAccessToSchool($department->school_id)){
            return redirect()->route('backend.dashboard');           
        }
	   
	   $categories = SchoolCategory::where('status',1)->get();
	   $school_details = School::where('id',$department->school_id)->select('school_category')->first();
	   
	   $schools = School::where('status',1)->where('school_category',$school_details->school_category)->get();
		
		return view('backend.departments.edit',compact('department', 'categories', 'schools', 'school_details'));

    }
	
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_ajax(Request $request)
    {
		$id = $request->input('department_id');
		$department = Department::find($id);
		
		return view('backend.departments.edit-ajax', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Retrieve the department and update
        $department = Department::find($id);
		
		$school_id = $request->school;
		
		if(!Auth::user()->hasAccessToSchool($school_id)){
            return redirect()->route('backend.dashboard');           
        }
		
		$validator = Validator::make($request->all(), [
			//	'school' => 'required',
				'name' => [
					'required',
					'max:180',
					Rule::unique('departments')->where(function ($query) use($school_id, $id) {  
						return $query->where('school_id', $school_id)->where('id','<>', $id);
					})
				],
				
			]);
			
		if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the form
                ->withInput();
        }
		
		
		//echo "<pre>"; print_r($school); exit;
       // $department->school_id = $request->input('school');
		$department->name = $request->input('name');
		$department->status = ($request->input('status') !== null)? $request->input('status'):0;
        $department->save(); //persist the data
		
		if(!empty($request->input('ajax_request'))) {
				return redirect()->route('backend.school.show', $department->school_id)->with('success', 'Department Information Updated Successfully');
		} else {
			return redirect()->route('backend.departments.index')->with('success','Department Information Updated Successfully');
		}
		
		
		
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
		$department = Department::find($id);
		
		if(!Auth::user()->hasAccessToSchool($department->school_id)){
            return redirect()->route('backend.dashboard');           
        }
		
		//delete all child records.
		if(isset($department->id) && !empty($department->id)) {
			
			$courses = Course::where('department_id',$department->id)->select('id')->get();
			//delete course
			foreach($courses as $course) {
				if(isset($course->id) && !empty($course->id)){
					$classes = Classes::where('course_id', $course->id)->select('id')->get();
					foreach($classes as $class) {
						if(isset($class->id) && !empty($class->id)) {
							$subjects = Subject::where('class_id', $class->id)->select('id')->get();
							foreach($subjects as $subject) {
								if(isset($subject->id) && !empty($subject->id)) {
									$topics = Topic::where('subject_id', $subject->id)->select('id')->get();
									foreach($topics as $topic) {
										if(isset($topic->id) && !empty($topic->id))
										$topic->delete();
									}
									
									$subject->delete();
								}
							}
							$periods = Period::where('class_id', $class->id)->select('id')->get();
							foreach($periods as $period) {
								if(isset($period->id) && !empty($period->id))
								$period->delete();
							}
				
							$class->delete();
						}
					}
					$course->delete();
				}
			}
			
		}
		
		
        $department->delete();
		
		if(!empty($request->input('ajax_request'))) {
				return redirect()->route('backend.school.show', $department->school_id)->with('success', 'Department Deleted Successfully');
		} else {
			return redirect()->route('backend.departments.index')->with('success','Department Deleted Successfully');
		}
	
	}
}
