<?php

namespace App\Http\Controllers\Backend\OpenSchool;


use App\Helpers\SiteHelpers;
use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Note;
use App\Models\ReportVideo;
use App\Models\School;
use App\Models\SchoolCategory;
use App\Models\Video;
use App\VideoDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{

    public function listVideos(){

        $courses = array();
        $classes = array();


        $query = Video::where('id','<>',0);

        if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            if(isset($profile->school_id)){
                $query = $query->where('school_id','=',$profile->school_id);
                $courses = Course::where('school_id',$profile->school_id)->where('status',1)->orderBy('name')->select('id', 'name')
                    ->get();
                $school_details = School::where('id', $profile->school_id)->select('school_category')->first();
                if($school_details->school_category === config("constants.BASIC_SCHOOL")){

                    if(isset($courses[0]->id)) {
                        $classes = Classes::where('course_id',$courses[0]->id)->where('status',1)->orderBy('class_name')
                            ->pluck('class_name','id');
                    }
                }
            }
        }

        $videos = $query->orderBy('id', 'desc')->paginate(25);

        $repoted_count = array();

        foreach($videos as $video) {
            $report_video_count = ReportVideo::where('video_id', $video->id)->count();
            $repoted_count[$video->id] = $report_video_count;
        }

        $query = School::where('status','=',1);

        if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $query = $query->where('id','=',$school_id);
            }
        }

        $schools = $query->orderBy('school_name')
            ->pluck('school_name','id');

        return view('backend.videos.open.index',
            compact('videos', 'repoted_count', 'schools', 'courses', 'classes'));
    }

    public function showVideo($uuid){
        $video = Video::uuid($uuid);
        $reported_video_detail = ReportVideo::where('video_id', $video->id)->get();
        if(!Auth::user()->hasAccessToSchool($video->school_id)){
            return redirect()->route('backend.dashboard');
        }

        return view('backend.videos.regular.show', compact('video', 'reported_video_detail'));
//        return view('backend.videos.regular.show', compact('video', 'reported_video_detail'));
    }

    public function createVideo(){

        $query = SchoolCategory::orWhere("mode","open")
            ->orWhere("mode","regular-open")->where("status",1);
        $school_id = 0;
        $category_id = 0;

        //Check for the user profile
        if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            if(isset($profile->school_id)){
                $school_id = $profile->school_id;
                $category_id = $profile->school->school_category;
                $query = $query->where('id','=',$profile->school->school_category);
            }
        }

        $institutes = $query->orderBy('name')->pluck('name','id');
         $classes_with_subjects =
            Classes::with(["course:id,school_id","subject:id,subject_name,class_id"])
            ->select("id","course_id","class_name")->get()->jsonSerialize();

//        return view('backend.videos.open.create',  compact('institutes','school_id','category_id','classes_with_subjects'));
        return view('backend.videos.create',  compact('institutes','school_id','category_id','classes_with_subjects'));
    }

    public function editVideo($uuid)
    {
        $video = Video::uuid($uuid);

        if(!Auth::user()->hasAccessToSchool($video->school_id)){
            return redirect()->route('backend.dashboard');
        }

        $query = SchoolCategory::where('status','=',1);

        if(Auth::user()->hasRole('school')){
            $profile = Auth::user()->profile;
            $query = $query->where('id','=',$profile->school->school_category);
        }

        $institutes = $query->orderBy('name')
            ->pluck('name','id');

        return view('backend.videos.regular.edit', compact('video','institutes'));
    }

    public function updateVideo(Request $request, $id){

        $validator = Validator::make($request->all(), [
            //'school' => 'required',
            'course' => 'required',
            'class' => 'required',
//            'date' => 'required',
            'subject' => 'required',
            'topic' => 'required',
            'video_type' => 'required',
//            'video_url' => 'required_if:video_type,url',
            //'video_url' => 'required_without:video_file',
            //'video_file' => 'sometimes|mimes:mp4,webm,wmv,avi,flv,mov|max:122880',
            'description' => 'required',
            //'note_file' => 'sometimes|mimes:jpeg,png,pdf,doc,docx,ppt,pptx,zip|max:20480',
            'tutor' => 'required'
        ]);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the form
                ->withInput();
        } else {
            //form data is available in the request object
            $video = Video::find($id);

            $video_id = $video->video_id;
            if ($request->video_type == 'url') {
                $video_url = $request->video_url;

                $video_data = SiteHelpers::getVimeoVideoData($video_url);

                if (!isset($video_data->video_id) || empty($video_data->video_id)) {

                    $validator->errors()->add('video_url', 'Invalid Video URL');

                    return Redirect::back()
                        ->withErrors($validator) // send back all errors to the form
                        ->withInput();
                } else {
                    $video_id = $video_data->video_id;
                }
            }

            $note = null;
            if ($request->note_file != '') {

                $path = $request->note_file;

                //Create Note
                $note = new Note();
                $note->tutor_id = $request->tutor;
                $note->file_url = $path;
                $note->storage = 's3';
                $note->status = 1;
                $note->save();

                $video->note_id = isset($note->id) ? $note->id : 0;
            }

            if (isset($request->video_file) && $request->video_file != '') {
                $mob_url = $request->video_file;
                $video->video_file = $mob_url;
            }

            //$video->school_id = $request->school;
            $video->course_id = $request->course;
            $video->class_id = $request->class;
            $video->play_on = $request->date;
//            $video->period_id = $request->period;
            $video->video_id = $video_id;
            $video->video_url = $request->video_url;
            $video->video_type = $request->video_type;
            //$video->video_file = $request->video_file;
            $video->description = $request->description;
            $video->subject_id = $request->subject;
            $video->topic_id = $request->topic;
            $video->keywords = $request->keywords;
            $video->tutor_id = $request->tutor;
            //$video->user_id = Auth::user()->id;
            $video->status = ($request->input('status') !== null) ? $request->input('status') : 0;

            $video->save();

            if ($request->video_type == 'file') {
                return redirect()->route('backend.videos.show', $video->uuid)->with('success', 'Video created Successfully, please upload video file for it. ');
            }

            return redirect()->route('backend.open.videos')->with('success', 'Video Updated Successfully');
        }

    }

    public function storeVideo(Request $request){
//        var_dump($video_data = SiteHelpers::getVideoData($query_parameter_value));

//        exit;

            $s3_base_url_plus_video_path = !empty($request->get("video_file")) ?
                "https://xtraclassnotes.s3-us-west-2.amazonaws.com/".$request->get("video_file") : "";

            $validator = Validator::make($request->all(), [
                'school' => 'required',
                'course' => 'required',
                'class' => 'required',
//                'date' => 'required',
//                'period' => 'required',
                'subject_id' => 'required',
                'topic' => 'required',
                'tutor' => 'required',
                'video_type' => 'required',
                'video_url' => 'required_if:video_type,url',
                //'video_file' => 'required|mimes:mp4,webm,wmv,avi,flv,mov',
                'description' => 'required',
                //'note_file' => 'sometimes|mimes:jpeg,jpg,png,pdf,doc,docx,ppt,pptx,zip|max:20480',
            ]);



            // if the validator fails, redirect back to the form
            if ($validator->fails()) {

                return Redirect::back()
                    ->withErrors($validator) // send back all errors to the form
                    ->withInput();
            } else {

//                $video_id = '';
//                if($request->video_type == 'url') {
//                    $video_url = $request->video_url;
//
//                    $video_data = SiteHelpers::getVideoData($video_url);
//
////                    if(!isset($video_data->video_id) || empty($video_data->video_id)){
////
////                        $validator->errors()->add('video_url', 'Invalid Video URL');
////
////                        return Redirect::back()
////                            ->withErrors($validator) // send back all errors to the form
////                            ->withInput();
////                    } else {
////                        $video_id = $video_data->video_id;
////                    }
//                }

                $url = parse_url($request->get("video_url"));
                $query_parameter = parse_url($request->get("video_url"), PHP_URL_QUERY);
                $video_id = substr($query_parameter,2,11);

                $note = null;

                if ($request->note_file != '') {

                    $path = $request->note_file;

                    //Create Note
                    $note = new Note();
                    $note->tutor_id = $request->tutor;
                    $note->file_url = $path;
                    $note->storage = 's3';
                    $note->status = 1;
                    $note->save();
                }

                //form data is available in the request object
                $video = new Video();
                $video->school_id = $request->school;
                $video->course_id = $request->course;
                $video->class_id = $request->class;
                $video->play_on = $request->date;
                $video->period_id = $request->period;
                $video->video_id = $video_id;
                $video->video_url = $request->video_url;
                $video->video_type = $request->video_type;
                $video->description = $request->description;
//                $video->subject_id = $request->subject;
                $video->subject_id = $request->subject_id;
                $video->topic_id = $request->topic;
                $video->keywords = $request->keywords;
                $video->tutor_id = $request->tutor;
                $video->note_id = isset($note->id)? $note->id : 0;
                $video->user_id = Auth::user()->id;
                $video->status = ($request->input('status') !== null)? $request->input('status'):1;
                $video->mobile_video_url = $s3_base_url_plus_video_path;

                if ($video->save()){
                    VideoDetail::create([
                        "video_id" => $video->id,
                        "details"  => SiteHelpers::getVideoData($video_id)
                    ]);
                };

                $path = "no video";

                if($request->video_type == 'file') {
                    return redirect()->route('backend.video.upload.files',$video->uuid)
                        ->with('success', 'Video created Successfully, please upload video file for it. '.$path);
                }

                return redirect()->route('backend.open.videos')->with('success', 'Video created Successfully');

        }

    }

    public function courseList(){

    }

    public function classesList(){

    }





}
