<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Stream;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StreamsController extends Controller
{
    public function index(){

        return view("backend.streams.index");
    }

    public function getStreamsData()
    {
          $streams = Stream::with(["tutor:id,first_name,last_name","class:id,class_name"])
            ->selectRaw("id,channel_id,subject,tutor_id,joined_students_total,class_id,active,created_at,recorded,streaming_url")
             ->orderBy("created_at","desc")
             ->get();

        $streams = $streams->map(function ($item, $index){
            return [
                "id"    => $index+1,
                "tutor" => $item->tutor->first_name . " ".$item->tutor->last_name,
                "class" => $item->class ? $item->class->class_name : "",
                "subject" => $item->subject,
                "channel_id" => $item->channel_id,
                "total_students" => $item->joined_students_total,
                "active" => $item->active ? "Yes" :"No",
                "recorded" => $item->recorded ? "Yes" : "No",
                "started_at" => $item->created_at,
                "streaming_url" => $item->streaming_url
            ];
        });

        return DataTables::of($streams)
            ->addColumn('action', function($row){
                if ($row["active"] == "Yes"){
                    $btn = '<a href='.$row["streaming_url"].' class="edit btn btn-primary btn-sm" target="_blank">Watch</a>';
                }else{
                    $btn = 'Ended';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
