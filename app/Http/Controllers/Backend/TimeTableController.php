<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Subject;
use App\TimeTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TimeTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("backend.timetable.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        if (!empty($request->class_id)){
            $subject = Subject::find($request->subject_id);
            $data["title"] = $subject?$subject->subject_name:"";
            $data["start"] = $request->start_date_time;
            $data["color"] = "15ca92";
        }
        $timeTable = new TimeTable();
        $timeTable->class_id = $request->class_id;
        $timeTable->subject_id = $request->subject_id;
        $timeTable->school_id = $request->school_id;
        $timeTable->period_id = $request->period_id;
        $timeTable->start_date_string  = $request->start_date_string;
        $timeTable->data = json_encode($data);
        $timeTable->start_date_time = $request->start_date_time;
        $timeTable->end_date_time = $request->end_date_time;
        $timeTable->save();

        return response()->json(["message" => "success"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function classCalendar($class_id): JsonResponse
    {
        $time_tables = TimeTable::whereClassId($class_id)->get();
        return response()->json(["data" => $time_tables]);
    }

}
