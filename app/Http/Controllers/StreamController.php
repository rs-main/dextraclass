<?php

namespace App\Http\Controllers;

use App\Events\StreamingEvent;
use App\Models\Classes;
use App\Models\Subject;
use App\Models\Tutor;
use App\Stream;
use Composer\Cache;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StreamController extends Controller
{

    public function startStream(Request $request): JsonResponse
    {
        $subject_object = DB::table("subjects")->where("id",$request->subject_id)->first();
        $class_object   = Classes::find($request->class_id);
        $class_uuid     = $class_object ? $class_object->uuid : "";
        $class_room_url = "/classroom/$class_uuid";

        if (!(Stream::where("channel_id",$request->channel_id)->first())) {

            $stream = Stream::create([
                "agora_stream_id" => $request->agora_stream_id,
                "channel_key" => $request->channel_key,
                "channel_id" => $request->channel_id,
                "tutor_id" => $request->tutor_id,
                "class_id" => $request->class_id,
                "subject" => $subject_object->subject_name,
                "subject_id" => $request->subject_id,
                "period_id" => $request->period_id,
                "topic_id" => $request->topic_id,
                "school_id" => $request->school_id,
                "active" => true,
                "joined_students_total" => 0,
                "streaming_url" => $class_room_url
            ]);
        }else{
            $stream = Stream::where("channel_id",$request->channel_id)->first();
            $stream->update([
                "streaming_url" => $class_room_url,
                "active" => true
            ]);
        }

        event(new StreamingEvent($request->class_id,$request->subject_id,$request->channel_id,'started'));
        \Illuminate\Support\Facades\Cache::forget($request->channel_key);
        \Illuminate\Support\Facades\Cache::rememberForever( $request->channel_key,function (){
            return "started";
        });

        return response()->json(["data" => $stream,"message" => "success"],200);

    }

    public function stopStream(Request $request): JsonResponse
    {

        $stream = Stream::where("channel_id",$request->channel_id)->first();
        $stream->update([
            "active" =>false
        ]);

        event(new StreamingEvent($stream->class_id,$stream->subject_id,$request->channel_id,'stopped'));

        \Illuminate\Support\Facades\Cache::rememberForever( $stream->channel_key,function (){
            return "stopped";
        });

        return response()->json(["data" => $stream,"message" => "success"],200);
    }

    public function joinStream(Request $request): JsonResponse
    {
        $stream_id = $request->agora_stream_id;
        $type = $request->joined;
        $streamObject = Stream::where("agora_stream_id",$stream_id)->first();

        if ($type == true) {
            $streamObject->update([
                "joined_students_total" => ($streamObject->joined_students_total) + 1
            ]);
        }else{
            $streamObject->update([
                "joined_students_total" => ($streamObject->joined_students_total)-1
            ]);
        }

        event(new StreamingEvent($streamObject->class_id,$streamObject->subject_id,$streamObject->channel_id,'joined'));

        return response()->json(["message" => "success"]);
    }

    public function hostStream(){
        return view("frontend.stream.host");
    }

    public function streamAudience(){
        return view("frontend.stream.audience");
    }

}
