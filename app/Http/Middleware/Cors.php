<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request)
         ->header("Access-Control-Allow-Origin", "*")
        ->header("Access-Control-Allow-Methods", "PUT, GET,POST , OPTIONS , PATCH")
        ->header("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type")
        ->header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Authorization, Origin');
        // ->header("Access-Control-Request-Headers", "X-Requested-With");
//         ->header("Access-Control-Allow-Origin", "*");
    }
}
