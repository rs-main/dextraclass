<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PastQuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "question" => $this->question,
            "num_of_options" => $this->num_of_options,
            "type" => $this->type,
            "answer_id" => $this->past_question_answer_id,
            "past_question_group_id" => $this->past_question_group_id,
            "subject_id" => $this->subject_id,
            "answer_options" => $this->type == "true-false" ? ["true","false"] : json_decode($this->answer_options)
        ];
    }
}
