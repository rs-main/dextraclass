<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionalVideoTopic extends Model
{
    protected $table = "professional_videos_topic";

    protected $guarded = [];
}
