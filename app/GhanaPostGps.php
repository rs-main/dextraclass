<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class GhanaPostGps extends Model
{
    //

    protected $table = "ghanapost_addresses";

    protected $guarded = [];

    public static function getSearchableLocationById($id)
    {

        $id = str_replace('-', '' , $id);
        $location = GhanaPostGps::where('GPSName', '=', $id)->first();

        if(!$location){
            try{

                $client = new Client();
                $response = self::guzzleRequestLocationGhanaPost($client, $id);

                $payload = $response->getBody()->getContents();

                $decoded_payload = json_decode($payload);

                if($decoded_payload && property_exists($decoded_payload, 'Table')
                    && (isset($decoded_payload->Table[0]->CenterLatitude)
                        && isset($decoded_payload->Table[0]->CenterLongitude))){

                    /** @var GhanaPostGps ghanapost_address */
                    $ghanapost_address = GhanaPostGps::firstOrCreate(['GPSName' => $id]);
                    if(!$ghanapost_address->GPSName || $ghanapost_address->GPSName != $id ||
                        !$ghanapost_address->PostCode){
                        $ghanapost_address->GPSName = $id;
                        $ghanapost_address->PostCode = $decoded_payload->Table[0]->PostCode;
                        $ghanapost_address->CenterLatitude = $decoded_payload->Table[0]->CenterLatitude;
                        $ghanapost_address->CenterLongitude = $decoded_payload->Table[0]->CenterLongitude;
                        $ghanapost_address->Area = $decoded_payload->Table[0]->Area;
                        $ghanapost_address->District = $decoded_payload->Table[0]->District;
//                        $ghanapost_address->Region = $decoded_payload->Table[0]->Region;
                        $ghanapost_address->save();

                        if (!(Location::whereName($ghanapost_address->Area)->first())){
                            $location = new Location();
                            $location->name = $ghanapost_address->Area;
                            $location->ghana_post_code = $id;
                            $location->coordinates = "{$ghanapost_address->CenterLatitude},{$ghanapost_address->CenterLongitude}";

                            if (!(District::whereName($ghanapost_address->District)->first())) {
                                $district = new District();
                                $district->name = $ghanapost_address->District;
                                if($district_id = $district->save()){
                                    $location->district_id = $district_id;
                                }
                            }else{
                                $district_object = District::whereName($ghanapost_address->District)->first();
                                $location->district_id = $district_object->id;
                            }

                            if (!(Region::whereName($decoded_payload->Table[0]->Region)->first())){
                                $region = new Region();
                                $region->name = $decoded_payload->Table[0]->Region;

                                if ($region_id = $region->save()){
                                    $location->region_id = $region_id;
                                }
                            }else{
                                $region_object = Region::whereName($decoded_payload->Table[0]->Region)->first();
                                $location->region_id = $region_object->id;
                            }

                            $location->save();
                        }

                        return $ghanapost_address;
                    }

                }else{
                    throw new \Exception("Unable to query Ghanapost location");
                }

            }catch(\Exception $e){
//                \Sentry::captureException($e);
                return response()->json(["exception" => $e->getMessage()]);
                return null;
            }
        }
        return $location;
    }

    public static function guzzleRequestLocation(Client &$client, $id){

        return $client->request('GET', "https://www.asaasegps.com/jsWebCall.aspx?Action=GetLocation&GPSName={$id}", [
            'headers' => [
                'Content-Type' => 'application/json',
                'AsaaseUser' => 'VGgxcyBJcyBvdXIgV2ViIFVzZXI=',
                'Origin' => 'https://www.asaasegps.com',
                'User-Agent'=> 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36',
                'Referer' => 'https://www.asaasegps.com/mapview.html'
            ]
        ]);
    }

    public static function guzzleRequestLocationGhanaPost(Client &$client, $id){

        $string = file_get_contents("https://ghanapostgps.com/map/");
        $position = strpos($string,"AsaaseOwner",20);
        $subString = substr($string,$position);
        $asaase_owner = substr($subString, 15,16);

        return $client->request('GET', "https://www.ghanapostgps.com/jsWebCall.aspx?Action=GetLocation&GPSName={$id}", [
            'headers' => [
                'Content-Type'=> 'application/json;charset=UTF-8',
                'AsaaseUser' => 'VGgxcyBJcyBvdXIgV2ViIFVzZXI=',
                'Origin' => 'https://www.ghanapostgps.com',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.152 Safari/537.36',
                'Referer' => 'https://www.ghanapostgps.com/map/',
                'AsaaseOwner' => 'CT%m+<C8rQOonUyT',
//                'AsaaseOwner' => "{$asaase_owner}",
                'Cookie'=> 'ASP.NET_SessionId=a2s5z3thweibxykgs5lgo0nu',
                'Accept-Charset' => 'Utf-8'
            ]
        ]);
    }


}
