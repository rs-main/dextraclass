<?php


namespace App\GraphQL\Queries;

use App\GraphQL\Middleware\ResolvePage;
use Closure;
use App\User;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\Type as GraphQLType;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class UsersQuery extends Query
{

    protected $middleware = [
//        Middleware\Logstash::class,
        ResolvePage::class,
    ];
    protected $attributes = [
        'name' => 'users',
    ];

    public function type(): GraphQLType
    {
        return Type::listOf(GraphQL::type('user'));
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'email' => ['name' => 'email', 'type' => Type::string()]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args['id'])) {
            return User::where('id' , $args['id'])->get();
        }

        if (isset($args['username'])) {
            return User::where('username', $args['username'])->get();
        }

        if (isset($args['email'])) {
            return User::where('email', $args['email'])->get();
        }

        if (isset($args['userData'])) {
            return User::where('email', $args['email'])->get();
        }

        return User::with("student")->get();
    }

    protected function getMiddleware(): array
    {
        return array_merge([
            ResolvePage::class,
        ], $this->middleware);
    }
}
