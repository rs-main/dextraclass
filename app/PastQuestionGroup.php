<?php

namespace App;

use App\Models\Classes;
use App\Models\QuestionBank;
use App\Models\Subject;
use Illuminate\Database\Eloquent\Model;

class PastQuestionGroup extends Model
{

    protected $table = "past_questions_group";

    protected $guarded = [];

    protected $casts = [
        "section_info" => "array"
    ];

    public function pastquestions(){
        return $this->hasMany(PastQuestion::class,"past_question_group_id");
    }

    public function subject(){
        return $this->belongsTo(Subject::class,"subject_id");
    }

    public function class(){
        return $this->belongsTo(Classes::class,"class_id");
    }
}
